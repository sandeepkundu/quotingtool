<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*Route::get('/login', function () {

	return view('users.user');*/
	//'csrf' => 'Illuminate\Foundation\Http\Middleware\VerifyCsrfToken';




	Route::get('/login', function () {
		return view('auth/login');
	});

	//error  404  page not
	Route::get('pagenotfound',['as'=>'notfound','uses'=>'HpUsersController@pagenotfound']);
	//error 500
	Route::get('internalerror',['as'=>'internal','uses'=>'HpUsersController@internalerror']);

	Route::get('/', 'HpUsersController@redrectRoot');

	Route::get('/QuoteClientSummary/{id}', 'DashboardController@QuoteClientSummary');

	// Route::get('/login', function () {
	// 	return view('HpLogin/hpLogin');
	// });
	Route::get('/forget', function () {
      return view('HpLogin/email');
	});

Auth::routes();

/* testing */
Route::get('/hpUser/quoteTotal','HpUsersController@getQuoteTotal');
Route::get('/Enterprises/quoteTotal','EnterprisesController@getQuoteTotal');
Route::get('/Enterprises/totalSteps', 'EnterprisesController@totalSteps');
Route::get('/hpUser/model_auto_search','HpUsersController@model_auto_search');


Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin/dashboard', 'DashboardController@dashboard');
/*=====================users =======================================*/
Route::get('/users/index','UsersController@index')->name('users');
Route::post('/usrs/insertUser','UsersController@insertUser');
Route::get('/users/addUser','UsersController@addUser');
Route::get('/users/editUser/{id}','UsersController@editUser');
Route::post('/users/updateUser','UsersController@updateUser');
Route::get('/users/Delete/{id}','UsersController@Delete');
Route::get('/users/viewProfile','UsersController@viewProfile');
Route::get('/users/editProfile','UsersController@editProfile');
Route::post('/users/updateProfile','UsersController@updateProfile');
Route::post('/users/multipleDelete','UsersController@multipleDelete');
Route::get('/users/getclients','UsersController@getclients');
Route::get('/users/resetPassword','UsersController@resetPassword');
Route::put('/users/reset','UsersController@reset');
Route::post('ClientSelect','UsersController@ClientSelect');
/*=====================users =======================================*/

/*==================================clients =======================================*/
Route::get('/clients/index' , 'ClientsController@index');
Route::post('/clients/insertClient' , 'ClientsController@insertClient');
Route::get('/clients/addClient' , 'ClientsController@addClient');
Route::get('/clients/editClient/{id}' , 'ClientsController@editClient');
Route::post('/clients/updateClient' , 'ClientsController@updateClient');
Route::get('/clients/Delete/{id}' , 'ClientsController@Delete');
Route::get('/clients/country_region', 'ClientsController@country_region');
Route::post('/clients/multipleDelete', 'ClientsController@multipleDelete');
/*=====================================clients =======================================*/

/*======================country=======================================*/
Route::get('/countries/index','CountriesController@index');
Route::post('/countries/insertCountry','CountriesController@insertCountry');
Route::get('/countries/addCountry','CountriesController@addCountry');
Route::get('/countries/deleteCountry/{id}','CountriesController@deleteCountry');
Route::get('/countries/editCountry/{id}','CountriesController@editCountry');
Route::put('/countries/updateCountry/{id}','CountriesController@updateCountry');
Route::get('/countries/showCountryRegion/{id}','CountriesController@showCountryRegion');
Route::get('/countries/deleteCountryRegion/{id}','CountriesController@deleteCountryRegion');
Route::post('/countries/multipleDelete','CountriesController@multipleDelete');
Route::post('/countries/MultipleDeleteRegion','CountriesController@MultipleDeleteRegion');
/*=============================End country=======================================*/

/*==================================== Product ===================================*/
Route::get('/products/index','ProductsController@index');
Route::get('/products/addProduct','ProductsController@addProduct');
Route::post('/products/insertProduct','ProductsController@insertProduct');
Route::get('/products/Delete/{id}','ProductsController@Delete');
Route::get('/products/editProduct/{id}','ProductsController@editProduct');
Route::post('/products/multipleDelete','ProductsController@multipleDelete');
Route::post('/products/updateProduct','ProductsController@updateProduct');
Route::get('/products/listingPage','ProductsController@listingPage');
Route::get('/products/clones/{id}','ProductsController@clones');
Route::get('/products/get-brand','ProductsController@getBrand');

/*====================================End Product ================================*/

/*======================product option group=======================================*/
Route::get('/poGroups/index','ProductOptionGroupsController@index');
Route::get('/poGroups/detailsPog/{group}','ProductOptionGroupsController@detailsPog');
Route::get('/poGroups/addPog/{group}','ProductOptionGroupsController@addPog');
Route::get('/poGroups/addNewGroups','ProductOptionGroupsController@addNewGroups');
Route::post('/poGroups/insertNewGroups','ProductOptionGroupsController@insertNewGroups');
Route::post('/poGroups/changetype','ProductOptionGroupsController@changetype');

Route::post('/poGroups/insertPog' , 'ProductOptionGroupsController@insertPog');
Route::get('/poGroups/editPog/{id}' , 'ProductOptionGroupsController@editPog');
Route::post('/poGroups/updatePog' , 'ProductOptionGroupsController@updatePog');
Route::get('/poGroups/Delete/{id}' , 'ProductOptionGroupsController@Delete');
Route::post('/poGroups/multipleDelete', 'ProductOptionGroupsController@multipleDelete');
/*=================================End product option group=========================*/


/*==================================product option =======================================*/
Route::get('/productOptions/addProductOption/{id}' , 'ProductOptionsController@addProductOption');
Route::get('/productOptions/index' , 'ProductOptionsController@index');
Route::post('/productOptions/insertProductOption' , 'ProductOptionsController@insertProductOption');
Route::get('/productOptions/editProductOptions/{id}' , 'ProductOptionsController@editProductOptions');
Route::get('/productOptions/detailProductOptions/{id}' , 'ProductOptionsController@detailProductOptions');
Route::post('/productOptions/updateProductOption' , 'ProductOptionsController@updateProductOption');
Route::get('/productOptions/updateOrder/{id}' , 'ProductOptionsController@updateOrder');
Route::get('/productOptions/validateProductOrder' , 'ProductOptionsController@validateProductOrder');
Route::get('/productOptions/deleteProductOption/{id}' , 'ProductOptionsController@deleteProductOption');
Route::post('/productOptions/multipleDelete', 'ProductOptionsController@multipleDelete');
Route::post('/productOptions/multipleDelete1', 'ProductOptionsController@multipleDelete1');

Route::post('ClientSelectProduct','ProductOptionsController@ClientSelectProduct');

/*===========================End product option =======================================*/

/*========================================Quote =======================================*/
Route::get('/Quotes/addQuote','QuotesController@addQuote');
Route::post('/Quotes/insertQuote','QuotesController@insertQuote');
Route::get('/Quotes/editQuoteStep/{id}','QuotesController@editQuoteStep');
Route::post('/Quotes/updateQuote', 'QuotesController@updateQuote');

Route::get('/Quotes/brand','QuotesController@brand');
Route::get('/Quotes/listQuoteStep','QuotesController@listQuoteStep');
Route::get('/Quotes/addQuoteStep','QuotesController@addQuoteStep');
Route::get('/Quotes/Delete/{id}' , 'QuotesController@Delete');
Route::post('/Quotes/multipleDelete', 'QuotesController@multipleDelete');
Route::get('/Quotes/listQuote/{id}' , 'QuotesController@listQuote');
Route::get('/Quotes/addQuoteItem/{id}' , 'QuotesController@addQuoteItem');
Route::post('/Quotes/insertQuoteStepItem', 'QuotesController@insertQuoteStepItem');
Route::get('/Quotes/editQuoteItem/{id}/{productIt}' , 'QuotesController@editQuoteItem');
Route::post('/Quotes/updateQuoteStepItem', 'QuotesController@updateQuoteStepItem');

Route::post('/add_quote_step_items', 'QuotesController@add_quote_step_items');

/*======================================End Quote=======================================*/

/*========================================X PRICE LOGISTICS X====================================*/
Route::get('/PriceLogistics/index','PriceLogisticsController@index');
Route::post('/PriceLogistics/multipleDelete','PriceLogisticsController@multipleDelete');
Route::get('/PriceLogistics/Delete/{id}' , 'PriceLogisticsController@Delete');
Route::get('/PriceLogistics/addPriceLogistics' , 'PriceLogisticsController@addPriceLogistics');
Route::get('/PriceLogistics/country_region' , 'PriceLogisticsController@country_region');
Route::post('/PriceLogistics/country_regions' , 'PriceLogisticsController@country_regions');
Route::post('/PriceLogistics/insertPriceLogistics','PriceLogisticsController@insertPriceLogistics');
Route::get('/PriceLogistics/editPriceLogistics/{id}' , 'PriceLogisticsController@editPriceLogistics');
Route::post('/PriceLogistics/updatePriceLogistics','PriceLogisticsController@updatePriceLogistics');
/*====================================X END PRICE LOGISTICS X====================================*/

/*======================================X Logistics Unit Numbers X=================================*/
Route::get('/LogisticsUnitNumbers/index','LogisticsUnitNumbersController@index');
Route::get('/LogisticsUnitNumbers/addLogisticsUnitNumbers','LogisticsUnitNumbersController@addLogisticsUnitNumbers');
Route::post('/LogisticsUnitNumbers/insertLogisticsUnitNumbers','LogisticsUnitNumbersController@insertLogisticsUnitNumbers');
Route::get('/LogisticsUnitNumbers/editLogisticsUnitNumbers/{id}','LogisticsUnitNumbersController@editLogisticsUnitNumbers');
Route::post('/LogisticsUnitNumbers/updateLogisticsUnitNumbers','LogisticsUnitNumbersController@updateLogisticsUnitNumbers');
Route::get('/LogisticsUnitNumbers/delete/{id}','LogisticsUnitNumbersController@delete');
Route::post('/LogisticsUnitNumbers/multipleDelete','LogisticsUnitNumbersController@multipleDelete');
/*===================================X End Logistics Unit Numbers X=================================*/


/*==================================Quote Status =======================================*/
Route::get('/quoteStatus/index' , 'QuoteStatusController@index');
Route::post('/quoteStatus/insertQuoteStatus' , 'QuoteStatusController@insertQuoteStatus');
Route::get('/quoteStatus/addQuoteStatus' , 'QuoteStatusController@addQuoteStatus');
Route::get('/quoteStatus/editQuoteStatus/{id}' , 'QuoteStatusController@editQuoteStatus');
Route::post('/quoteStatus/updateQuoteStatus' , 'QuoteStatusController@updateQuoteStatus');
Route::get('/quoteStatus/Delete/{id}' , 'QuoteStatusController@Delete');
Route::post('/quoteStatus/multipleDelete', 'QuoteStatusController@multipleDelete');
/*=====================================Quote Status =======================================*/



/*====================================X QuoteManagements X=================================*/

Route::get('/QuoteManagements/index','QuoteManagementsController@index');
Route::get('/QuoteManagements/delete/{id}','QuoteManagementsController@delete');
Route::get('/QuoteManagements/QuoteDetails/{id}','QuoteManagementsController@QuoteDetails');
Route::get('/QuoteManagements/delete_quote_item/{id}','QuoteManagementsController@delete_quote_item');
Route::post('/QuoteManagements/UploadImage','QuoteManagementsController@UploadImage');
Route::post('/QuoteManagements/updateClientFinancial','QuoteManagementsController@updateClientFinancial');

Route::post('/QuoteManagements/multipleDelete', 'QuoteManagementsController@multipleDelete');
Route::get('/Enterprises/delete/{id}','EnterprisesController@delete');

Route::get('/quote-managements/edit-price-margin', 'QuoteManagementsController@updatePriceMarginAdjustment');
Route::post('/quote-managements/save-price-margin', 'QuoteManagementsController@savePriceMarginAdjustment');
/*================================X End of QuoteManagements X===============================*/


/*==================================Price Bands=======================================*/
Route::get('/priceBands/index' , 'PriceBandsController@index');
Route::post('/priceBands/insertPriceBands' , 'PriceBandsController@insertPriceBands');
Route::get('/priceBands/addPriceBands' , 'PriceBandsController@addPriceBands');
Route::get('/priceBands/editPriceBands/{id}' , 'PriceBandsController@editPriceBands');
Route::post('/priceBands/updatePriceBands' , 'PriceBandsController@updatePriceBands');
Route::get('/priceBands/Delete/{id}' , 'PriceBandsController@Delete');
Route::post('/priceBands/multipleDelete', 'PriceBandsController@multipleDelete');
/*=====================================Price Bands =======================================*/

/*==================================Setting=======================================*/
Route::get('/QuoteValid/index' , 'QuoteValidController@index');
Route::get('/QuoteValid/addQuoteValid' , 'QuoteValidController@addQuoteValid');
Route::post('/QuoteValid/insertQuoteValid' , 'QuoteValidController@insertQuoteValid');
Route::get('/QuoteValid/deleteQuoteValid/{id}' , 'QuoteValidController@deleteQuoteValid');
Route::post('/QuoteValid/multipleDelete','QuoteValidController@multipleDelete');
Route::get('/QuoteValid/editQuoteValid/{id}','QuoteValidController@editQuoteValid');
Route::post('/QuoteValid/updateQuoteValid/{id}','QuoteValidController@updateQuoteValid');

/*=====================================End Setting=======================================*/


/*==============================X PriceMarginAdjustments X=============================*/

Route::get('/PriceMarginAdjustments/index','PriceMarginAdjustmentsController@index');
Route::get('/PriceMarginAdjustments/addPriceMarginAdjustments','PriceMarginAdjustmentsController@addPriceMarginAdjustments');

Route::post('/PriceMarginAdjustments/insertPriceMarginAdjustments','PriceMarginAdjustmentsController@insertPriceMarginAdjustments');

Route::get('/PriceMarginAdjustments/editPriceMarginAdjustments/{id}','PriceMarginAdjustmentsController@editPriceMarginAdjustments');

Route::post('/PriceMarginAdjustments/updatePriceMarginAdjustments/{id}','PriceMarginAdjustmentsController@updatePriceMarginAdjustments');

Route::post('/PriceMarginAdjustments/multipleDelete','PriceMarginAdjustmentsController@multipleDelete');
Route::get('/PriceMarginAdjustments/Delete/{id}','PriceMarginAdjustmentsController@Delete');

/*==============================X End PriceMarginAdjustments X=============================*/

/*==================================Price Margin OEM=======================================*/
Route::get('/priceMarginOEM/index' , 'PriceMarginOEMController@index');
Route::post('/priceMarginOEM/insertPriceMargin' , 'PriceMarginOEMController@insertPriceMargin');
Route::get('/priceMarginOEM/addPriceMargin' , 'PriceMarginOEMController@addPriceMargin');
Route::get('/priceMarginOEM/editPriceMargin/{id}' , 'PriceMarginOEMController@editPriceMargin');
Route::post('/priceMarginOEM/updatePriceMargin' , 'PriceMarginOEMController@updatePriceMargin');
Route::get('/priceMarginOEM/Delete/{id}' , 'PriceMarginOEMController@Delete');
Route::post('/priceMarginOEM/multipleDelete', 'PriceMarginOEMController@multipleDelete');
/*=====================================Price Margin OEM=======================================*/

/*==================================Price Margin sevices================================*/
Route::get('/priceServices/index' , 'PriceMarginServicesController@index');
Route::post('/priceServices/insertPriceServices' , 'PriceMarginServicesController@insertPriceServices');
Route::get('/priceServices/addPriceServices' , 'PriceMarginServicesController@addPriceServices');
Route::get('/priceServices/editPriceServices/{id}' , 'PriceMarginServicesController@editPriceServices');
Route::post('/priceServices/updatePriceServices' , 'PriceMarginServicesController@updatePriceServices');
Route::get('/priceServices/delete/{id}' , 'PriceMarginServicesController@delete');
Route::post('/priceServices/multipleDelete', 'PriceMarginServicesController@multipleDelete');
/*=====================================Price Margin sevices==================================*/


/*=========================================X HpUsers X========================================*/
Route::get('/EnterprisesSerialNumserSearch', 'EnterprisesController@EnterprisesSerialNumserSearch');
Route::get('store/quote/step','HpUsersController@step01');
Route::post('store/quote/step','HpUsersController@step01');
Route::post('/store/quote/step02','HpUsersController@step02');
Route::post('/store/quote/step03','HpUsersController@step03');
//Route::post('/hpUser/step04','HpUsersController@step04');
//Route::post('/hpUser/step05','HpUsersController@step05');
//Route::post('/hpUser/step06','HpUsersController@step06');
//Route::post('/hpUser/step06','HpUsersController@step06');
//Route::post('/hpUser/step07','HpUsersController@step07');
//Route::post('/hpUser/step08','HpUsersController@step08');
//Route::post('/hpUser/step09','HpUsersController@step09');
//Route::post('/hpUser/step10','HpUsersController@step10');
Route::post('/hpUser/saveStoreData','HpUsersController@saveStoreData');
Route::post('/hpUser/quoteTotal','HpUsersController@getQuoteTotal');
Route::post('/hpUser/getQuoteTotalByModel','HpUsersController@getQuoteTotalByModel');
Route::get('/hpUser/ModelFound','HpUsersController@ModelFound');
Route::get('/hpUser/SerialNumserSearch','HpUsersController@SerialNumserSearch');
Route::get('/hpUsers/allDefects','HpUsersController@allDefects');
Route::get('/hpUsers/allServicess','HpUsersController@allServicess');

Route::get('/quote/history/{id}','HpUsersController@QuoteSummary');
Route::get('/quote/history_list/{id}','HpUsersController@QuoteSummaryList');
Route::get('/hpUser/changeStatus','HpUsersController@changeStatus');
Route::get('/hpUser/downloadPDF/{id}','HpUsersController@downloadPDF');
Route::post('/hpUser/pickupRequest','HpUsersController@pickupRequest');
Route::get('/hpUser/pickupRequestDetail','HpUsersController@pickupRequestDetail');


Route::get('/page/{id}','HpUsersController@navigations');
Route::get('/hpUser/contacts','HpUsersController@contacts');
Route::get('/hpUser/info','HpUsersController@info');
Route::post('/HpUserClientSelect','HpUsersController@HpUserClientSelect');
Route::post('/store/updateName','HpUsersController@updateName');
Route::post('/store/model_auto_search','HpUsersController@model_auto_search');
Route::post('/uploadimageByWebcam','HpUsersController@uploadimageByWebcam');
Route::post('/hpUser/uploadimageByDrag','HpUsersController@uploadimageByDrag');
/*=======================================X End HpUsers X========================================*/


/*==================================Start Price Factors================================*/
Route::get('/PriceFactors/index' , 'PriceFactorsController@index');
Route::post('/PriceFactors/insertPriceFactors' , 'PriceFactorsController@insertPriceFactors');
Route::get('/PriceFactors/addPriceFactors' , 'PriceFactorsController@addPriceFactors');
Route::get('/PriceFactors/editPriceFactors/{id}' , 'PriceFactorsController@editPriceFactors');
Route::get('/PriceFactors/deletePriceFactors/{id}' , 'PriceFactorsController@deletePriceFactors');
Route::post('/PriceFactors/updatePriceFactors/{id}' , 'PriceFactorsController@updatePriceFactors');
Route::post('/PriceFactors/multipleDelete', 'PriceFactorsController@multipleDelete');
/*=====================================End Price Factors==================================*/

/*==================================Start Product Factors================================*/
Route::get('/ProductFactors/index' , 'ProductFactorsController@index');
Route::post('/ProductFactors/insertProductFactors' , 'ProductFactorsController@insertProductFactors');
Route::get('/ProductFactors/addProductFactors' , 'ProductFactorsController@addProductFactors');
Route::get('/ProductFactors/editProductFactors/{id}' , 'ProductFactorsController@editProductFactors');
Route::get('/ProductFactors/deleteProductFactors/{id}' , 'ProductFactorsController@deleteProductFactors');
Route::post('/ProductFactors/updateProductFactors/{id}' , 'ProductFactorsController@updateProductFactors');
Route::post('/ProductFactors/multipleDelete', 'ProductFactorsController@multipleDelete');
/*=====================================End Product Factors==================================*/



/*==================================Price Factor Bands================================*/
Route::get('/priceFactorBands/index' , 'PriceFactorBandsController@index');
Route::post('/priceFactorBands/insertPriceFactorBands' , 'PriceFactorBandsController@insertPriceFactorBands');
Route::get('/priceFactorBands/addPriceFactorBands' , 'PriceFactorBandsController@addPriceFactorBands');
Route::get('/priceFactorBands/editPriceFactorBands/{id}' , 'PriceFactorBandsController@editPriceFactorBands');
Route::post('/priceFactorBands/updatePriceFactorBands' , 'PriceFactorBandsController@updatePriceFactorBands');
Route::get('/priceFactorBands/delete/{id}' , 'PriceFactorBandsController@delete');
Route::post('/priceFactorBands/multipleDelete', 'PriceFactorBandsController@multipleDelete');
/*=====================================Price Factor Bands==================================*/

/*===============================Hp User Dashboard======================================*/
Route::get('/dashboard','HpUsersController@hpDashboard');
Route::get('/edit_profile','HpUsersController@editHpDashboard');
Route::post('/updatehpDashboard','HpUsersController@updatehpDashboard');
Route::post('/store/client','HpUsersController@client');
Route::post('/UploadImage','HpUsersController@UploadImage');
Route::get('/hpUser/pdf','HpUsersController@pdf');
Route::get('/add_user','HpUsersController@addUser');
Route::post('/insertUser','HpUsersController@insertUser');
Route::get('/modelSearch','HpUsersController@modelSearch');
Route::get('/edit_user/{id}','HpUsersController@editUser');
Route::post('/updateUser','HpUsersController@updateUser');
Route::get('/collection_history/{id}','HpUsersController@collection_history');
Route::post('/collection_status','HpUsersController@collection_status');
Route::get('/collection-details','HpUsersController@CollectionDetails');
/*===============================EndHp User Dashboard==================================*/




/*==================================Start QuoteCollection================================*/
Route::get('/QuoteCollection/index' , 'QuoteCollectionController@index');
Route::get('/QuoteCollection/addQuoteCollection' , 'QuoteCollectionController@addQuoteCollection');
Route::post('/QuoteCollection/insertQuoteCollection' , 'QuoteCollectionController@insertQuoteCollection');

Route::get('/QuoteCollection/editQuoteCollection/{id}' , 'QuoteCollectionController@editQuoteCollection');
Route::post('/QuoteCollection/updateQuoteCollection' , 'QuoteCollectionController@updateQuoteCollection');
Route::get('/QuoteCollection/delete/{id}' , 'QuoteCollectionController@delete');

Route::post('/QuoteCollection/multipleDelete', 'QuoteCollectionController@multipleDelete');

Route::get('/QuoteCollection/history/{id}','QuoteCollectionController@history');
Route::post('/QuoteCollection/collection_status','QuoteCollectionController@collection_status');
Route::get('/QuoteCollection/CollectionDetails','QuoteCollectionController@CollectionDetails');
Route::get('/QuoteCollection/pickupRequestDetail','QuoteCollectionController@pickupRequestDetail');
/*=====================================End QuoteCollection==================================*/


/*=====================================Start Roles==================================*/
Route::get('/Roles/index' , 'RoleController@index');
Route::get('/Roles/addRoles' , 'RoleController@addRoles');
Route::post('/Roles/insertRoles' , 'RoleController@insertRoles');
Route::get('/Roles/editRoles/{id}' , 'RoleController@editRoles');
Route::post('/Roles/updateRoles' , 'RoleController@updateRoles');
Route::get('/Roles/delete/{id}' , 'RoleController@delete');
Route::post('/Roles/multipleDelete', 'RoleController@multipleDelete');
/*=====================================End Roles==================================*/

/*=====================================Start Navigation==================================*/
Route::get('/Navigation/index' , 'NavigationController@index');
Route::get('/Navigation/addNavigation' , 'NavigationController@addNavigation');
Route::post('/Navigation/insertNavigation' , 'NavigationController@insertNavigation');
Route::get('/Navigation/editNavigation/{id}' , 'NavigationController@editNavigation');
Route::post('/Navigation/updateNavigation' , 'NavigationController@updateNavigation');
Route::get('/Navigation/delete/{id}' , 'NavigationController@delete');
Route::post('/Navigation/multipleDelete', 'NavigationController@multipleDelete');
/*=====================================End Navigation==================================*/


/*=============================PermissionsController ============================*/
Route::get('/Permissions/index' , 'PermissionsController@index');
Route::get('/Permissions/add' , 'PermissionsController@add');
Route::post('/Permissions/insertPermission' , 'PermissionsController@insertPermission');
Route::get('/Permissions/edit/{id}' , 'PermissionsController@edit');
Route::post('/Permissions/updatePermission' , 'PermissionsController@updatePermission');
Route::get('/Permissions/Delete/{id}' , 'PermissionsController@delete');
Route::post('/Permissions/multipleDelete', 'PermissionsController@multipleDelete');
/*==============================End PermissionsController==========================*/


/*=======================PermissionRolesController ==========================*/
Route::get('/PermissionRoles/index' , 'PermissionRolesController@index');
Route::get('/PermissionRoles/add' , 'PermissionRolesController@add');
Route::post('/PermissionRoles/insertPermissionRole' , 'PermissionRolesController@insertPermissionRole');
Route::get('/PermissionRoles/edit/{id}' , 'PermissionRolesController@edit');

Route::put('/PermissionRoles/updatePermissionRole' , 'PermissionRolesController@updatePermissionRole');
Route::get('/PermissionRoles/Delete/{id}' , 'PermissionRolesController@delete');
Route::post('/PermissionRoles/multipleDelete', 'PermissionRolesController@multipleDelete');
/*==================================End PermissionsController=================================*/

/*=======================QuoteStepDefaults ==========================*/
Route::get('/QuoteStepDefaults/index' , 'QuoteStepDefaultController@index');
Route::get('/QuoteStepDefaults/add' , 'QuoteStepDefaultController@add');
Route::post('/QuoteStepDefaults/insertQuoteStepDefault' , 'QuoteStepDefaultController@insertQuoteStepDefault');
Route::get('/QuoteStepDefaults/edit/{id}' , 'QuoteStepDefaultController@edit');
Route::post('/QuoteStepDefaults/updateQuoteStepDefault' , 'QuoteStepDefaultController@updateQuoteStepDefault');
Route::get('/QuoteStepDefaults/Delete/{id}' , 'QuoteStepDefaultController@Delete');
Route::post('/QuoteStepDefaults/multipleDelete', 'QuoteStepDefaultController@multipleDelete');
/*==================================End QuoteStepDefaults=================================*/


/*=================================== Enterprices ===================================*/
Route::get('/enterprises/index' , 'EnterprisesController@index');
//Route::get('/Enterprises/index2' , 'EnterprisesController@index2');
Route::get('/enterprises/quote/step' , 'EnterprisesController@EnterprisesSteps');
Route::post('/enterprises/quote/step' , 'EnterprisesController@EnterprisesSteps');
Route::post('/enterprises/step02' , 'EnterprisesController@EnterprisesSteps2');

Route::get('/enterprises/step02' , 'EnterprisesController@EnterprisesSteps2');

Route::get('/Enterprises/quantityUpdate' , 'EnterprisesController@quantityUpdate');
Route::post('EnterpriseQuote','EnterprisesController@EnterpriseQuote');
//Route::post('/Enterprises/EnterprisesSteps3' , 'EnterprisesController@EnterprisesSteps3');
//Route::post('/Enterprises/EnterprisesSteps4' , 'EnterprisesController@EnterprisesSteps4');
//Route::post('/Enterprises/EnterprisesSteps5' , 'EnterprisesController@EnterprisesSteps5');
//Route::post('/Enterprises/EnterprisesSteps6' , 'EnterprisesController@EnterprisesSteps6');
//Route::post('/Enterprises/EnterprisesSteps7' , 'EnterprisesController@EnterprisesSteps7');
//Route::post('/Enterprises/EnterprisesSteps8' , 'EnterprisesController@EnterprisesSteps8');
//Route::post('/Enterprises/EnterprisesSteps9' , 'EnterprisesController@EnterprisesSteps9');
//Route::post('/Enterprises/EnterprisesSteps10' , 'EnterprisesController@EnterprisesSteps10');
//Route::post('/Enterprises/EnterprisesSteps11' , 'EnterprisesController@EnterprisesSteps11');
Route::get('/enterprises/quote/settings','EnterprisesController@store_enterprise');
Route::post('/Enterprises/SaveEnterpriseQuote','EnterprisesController@SaveEnterpriseQuote');
Route::get('/Enterprises/delete/{id}','EnterprisesController@delete');
Route::get('/Enterprises/histry/{id}','EnterprisesController@EnterpriseSummary');
Route::get('/Enterprises/changeStatus','EnterprisesController@changeStatus');
Route::get('/Enterprises/quotestatus','EnterprisesController@updatequoteStatus');
Route::post('/Enterprises/quoteTotal','EnterprisesController@getQuoteTotal');
Route::post('/Enterprises/addnote','EnterprisesController@addNote');
Route::post('/Enterprises/stepcheck','EnterprisesController@stepCheck');
Route::get('/EnterprisesSearch','EnterprisesController@EnterprisesSearch');
Route::get('/Enterprises/allDefects','EnterprisesController@allDefects');
Route::get('/Enterprises/download/{id}','EnterprisesController@download');

Route::get('/QuoteManagements/ItemDetail','QuoteManagementsController@ItemDetail');
Route::get('/quotes/history_list/{id}','EnterprisesController@QuoteSummaryList');

Route::get('/updateQuoteDetail','QuoteManagementsController@updateQuoteDetail');
Route::post('Enterprises/quote_setting','EnterprisesController@QuoteSetting');
Route::get('enterprises/quotesettings','EnterprisesController@QuoteSettings');
Route::post('Enterprises/UpdateQuoteSettings','EnterprisesController@UpdateQuoteSettings');
Route::post('/Enterprises/model_auto_search','EnterprisesController@model_auto_search');

Route::get('Enterprises/EditItem','EnterprisesController@EditItem');
Route::post('Enterprises/editQuote','EnterprisesController@EditQuote');

Route::get('Enterprises/QuoteUpdate','EnterprisesController@QuoteUpdate');

Route::get('Enterprises/clone/{id}','EnterprisesController@copy_Item');

Route::get('Enterprises/quoteItemInfo','EnterprisesController@quoteItemInfo');
Route::get('Enterprises/csv','EnterprisesController@exportCSV');
/* testing routes to be removed */
Route::get('/Enterprises/test','EnterprisesController@test');

/*===================================End Enterprices ================================*/

/*=================================== Quotes Notes=================================*/
Route::get('/QuotesNotes/index' , 'QuotesNotesController@index');
Route::get('/QuotesNotes/add/{id}' , 'QuotesNotesController@add');
Route::post('/QuotesNotes/insertQuotesNotes' , 'QuotesNotesController@insertQuotesNotes');
Route::get('/QuotesNotes/edit/{id}' , 'QuotesNotesController@edit');
Route::post('/QuotesNotes/updateQuotesNotes' , 'QuotesNotesController@updateQuotesNotes');
Route::get('/QuotesNotes/Delete/{id}' , 'QuotesNotesController@Delete');
Route::post('/QuotesNotes/multipleDelete', 'QuotesNotesController@multipleDelete');
/*===================================End Quotes Notes ================================*/


/*=================================== General=================================*/
Route::get('/{id}' , 'GeneralController@footer');
/*=================================== General=================================*/

Route::post('/store/totalSteps', 'HpUsersController@totalSteps');

Route::post('/enterprises/totalSteps', 'EnterprisesController@totalSteps');


//=============================== Variances ===================================//
	Route::get('/Variances/BulkModelImports' , 'VariancesController@BulkModelImports');
	Route::post('/Variances/import_data', 'VariancesController@import_data');
	Route::get('/Variances/export_data', 'VariancesController@export_data');
	Route::get('/Variances/import_pup' , 'VariancesController@import_pup_list');
	Route::post('/Variances/import_pup', 'VariancesController@import_pup');
	Route::get('/Variances/check', 'HpUsersController@check');
	Route::get('/import/{id}', 'VariancesController@import');
	Route::get('/export/{id}', 'VariancesController@export');

	Route::get('/Variances/BulkDvdDefectsImports' , 'DvdDefectsController@index');
	Route::post('/Variances/BulkDvdDefectStore' , 'DvdDefectsController@BulkDvdDefectsImports_Store');

	Route::get('/Variances/hdd-ram' , 'HddRamImportController@index');
	Route::post('/Variances/BulkHddRamStore' , 'HddRamImportController@BulkHddRamStore');

	Route::post('Variances/logistic-import' , 'LogisticImportController@bulkLogisticStore');
	Route::get('/Variances/logistic-view' , 'LogisticImportController@bulkLogisticView');

	Route::post('Variances/price-margin-import-store' , 'MarginAdjustmentUploadImportController@store');
	Route::get('/Variances/price-margin-import-view' , 'MarginAdjustmentUploadImportController@index');
	
	Route::post('/Variances/services-import-upload' , 'ServicesImportController@bulkUpload');
	Route::get('/Variances/services-import-view' , 'ServicesImportController@bulkView');
//=============================================================================//



//===========================Calculation controller links=======================//

	Route::get('/pricing/check','calculationsController@checkPrice');

//========================End Calculation controller links======================//



//============================ Remove cache from server ========================//

Route::get('/cache-clear', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});

Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});

//============================ End Remove cache form server =====================//


/*=================================== Theme Management=================================*/
Route::get('/Theme/index' , 'ThemeController@index');
Route::get('/Theme/add' , 'ThemeController@add');
Route::post('/Theme/insert' , 'ThemeController@insert');
Route::get('/Theme/edit/{id}' , 'ThemeController@edit');
Route::post('/Theme/update' , 'ThemeController@update');
Route::get('/Theme/delete/{id}' , 'ThemeController@Delete');
Route::post('/Theme/multipleDelete', 'ThemeController@multipleDelete');
/*===================================Theme Management ================================*/

/*=================================== Reporting=================================*/
Route::get('/reporting/logins', 'ReportController@report_logins');
// error 404





