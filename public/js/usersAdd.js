
$(".dropdown dt a").on('click', function () {
    $(".dropdown dd ul").slideToggle('fast');
});

$(".dropdown dd ul li a").on('click', function () {
    $(".dropdown dd ul").hide();
});

function getSelectedValue(id) {
    return $("#" + id).find("dt a span.value").html();
}

$(document).bind('click', function (e) {
    var $clicked = $(e.target);
    if (!$clicked.parents().hasClass("dropdown"))
        $(".dropdown dd ul").hide();
});

$('.mutliSelect input[type="checkbox"]').on('click', function () {
    var title = $(this).closest('.mutliSelect').find('input[type="checkbox"]').val(),
    title = $(this).attr('class') + ",";

    if ($(this).is(':checked')) {
        var html = '<span title="' + title + '">' + title + '</span>';
        $('.multiSel').append(html);
        $(".hida").hide();
    } else {
        $('span[title="' + title + '"]').remove();
        var ret = $(".hida");
        $('.dropdown dt a').append(ret);
    }
});


$('#password').password({
        // custom messages
        shortPass: 'The password is too short',
        badPass: 'Weak; try combining letters & numbers',
        goodPass: 'Medium; try using special charecters',
        strongPass: 'Strong password',
        containsUsername: 'The password contains the username',
        enterPass: 'Type your password',
        // show percent
        showPercent: true,
        // show text
        showText: true,
        // enable animation
        animate: true,
        animateSpeed: 'fast',
        // link to username
        email: false,
        usernamePartialMatch: true,
        // minimum length
        minimumLength: 4

    });



$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();

});


$(document).ready(function () {
    $("#client_id").chosen();
    $('#client_id').on('change', function (evt, params) {
        var id = "";
        $('.search-choice a').each(function () {
            id = id + $(this).data('option-array-index') + ",";
        });

        $('[name="client_id"]').val(id);
    });


});
/*==============Client Heirarchy CSS==================*/
$(document).ready(function () {

    var v_token = '{{csrf_token()}}';

    $('#loadingDiv').css('display', 'block');
    $.ajax({
        type: 'post',
        url: SITE_URL + '/ClientSelect',
        data: {_token: v_token},
        dataType: 'JSON',
        success: function (obj) {
            var s = [];
            var a = [];
            var c = [];
            var i = 0;
            for (var item in obj) {
                j = 0;
                var obj1 = obj[item];
                for (var key in obj1.items) {
                    var k = 0;
                    var obj3 = obj1.items[key];
                    for (var element in obj3.items) {
                        c[k] = {id: obj3.items[element].id, text: obj3.items[element].name};
                        k++;
                    }

                    a[j] = {id: obj1.items[key].id, text: obj1.items[key].name, expanded: true, items: c};
                    j++;
                    c = [];
                }

                s[i] = {
                    id: obj[item].id, text: obj[item].name, expanded: true, items: a
                };
                i++;
                a = [];
            }


            var myDataSource = new kendo.data.HierarchicalDataSource({
                data: s
            });

            $("#multiselect").kendoMultiSelect({
                dataTextField: "text",
                dataValueField: "id"
            });

            $("#treeview").kendoTreeView({
                loadOnDemand: false,
                checkboxes: {
                    checkChildren: false
                },
                dataSource: myDataSource,
                check: onCheck,
                expand: onExpand
            });


                //  $('span').removeClass('k-checkbox-wrapper');
                var dialog = $("#dialog");
                var multiSelect = $("#multiselect").data("kendoMultiSelect");
                $("#openWindow").kendoButton();

                multiSelect.readonly(false);

                $("#openWindow").click(function () {
                    dialog.data("kendoDialog").open();
                    $(this).fadeOut();
                });

                dialog.kendoDialog({
                    width: "400px",
                    title: "Select Client",
                    visible: false,
                    actions: [
                    {
                        text: 'Cancel',
                        primary: false,
                        action: onCancelClick
                    },
                    {
                        text: 'Ok',
                        primary: true,
                        action: onOkClick
                    }
                    ],
                    close: onClose
                })//.data("kendoDialog").open();


                function onCancelClick(e) {
                    e.sender.close();
                }

                function onOkClick(e) {
                    var checkedNodes = [];
                    var treeView = $("#treeview").data("kendoTreeView");

                    getCheckedNodes(treeView.dataSource.view(), checkedNodes);
                    populateMultiSelect(checkedNodes);

                    e.sender.close();
                }

                function onClose() {
                    $("#openWindow").fadeIn();
                }

                function populateMultiSelect(checkedNodes) {
                    var multiSelect = $("#multiselect").data("kendoMultiSelect");
                    multiSelect.dataSource.data([]);

                    var multiData = multiSelect.dataSource.data();
                    if (checkedNodes.length > 0) {
                        var array = multiSelect.value().slice();
                        for (var i = 0; i < checkedNodes.length; i++) {
                            multiData.push({text: checkedNodes[i].text, id: checkedNodes[i].id});
                            array.push(checkedNodes[i].id.toString());
                        }

                        multiSelect.dataSource.data(multiData);
                        multiSelect.dataSource.filter({});
                        multiSelect.value(array);
                    }
                }

                function checkUncheckAllNodes(nodes, checked) {
                    for (var i = 0; i < nodes.length; i++) {
                        nodes[i].set("checked", checked);

                        if (nodes[i].hasChildren) {
                            checkUncheckAllNodes(nodes[i].children.view(), checked);
                        }
                    }
                }

                function chbAllOnChange() {
                    var checkedNodes = [];
                    var treeView = $("#treeview").data("kendoTreeView");
                    var isAllChecked = $('#chbAll').prop("checked");

                    checkUncheckAllNodes(treeView.dataSource.view(), isAllChecked)

                    if (isAllChecked) {
                        setMessage($('#treeview input[type="checkbox"]').length);
                    }
                    else {
                        setMessage(0);
                    }
                }

                function getCheckedNodes(nodes, checkedNodes) {
                    var node;

                    for (var i = 0; i < nodes.length; i++) {
                        node = nodes[i];

                        if (node.checked) {
                            checkedNodes.push({text: node.text, id: node.id});
                        }

                        if (node.hasChildren) {
                            getCheckedNodes(node.children.view(), checkedNodes);
                        }
                    }
                }

                function onCheck() {
                    var checkedNodes = [];
                    var treeView = $("#treeview").data("kendoTreeView");

                    getCheckedNodes(treeView.dataSource.view(), checkedNodes);
                    setMessage(checkedNodes.length);
                }

                function onExpand(e) {
                    if ($("#filterText").val() == "") {
                        $(e.node).find("li").show();
                    }
                }

                function setMessage(checkedNodes) {
                    var message;

                    if (checkedNodes > 0) {
                        message = checkedNodes + " clients selected";
                    }
                    else {
                        message = "0 clients selected";
                    }

                    $("#result").html(message);
                }

                $("#filterText").keyup(function (e) {
                    var filterText = $(this).val();

                    if (filterText !== "") {
                        $(".selectAll").css("visibility", "hidden");
                        $("#treeview .k-group .k-group .k-in").closest("li").hide();
                        $("#treeview .k-group").closest("li").hide();
                        $("#treeview .k-in:contains(" + filterText + ")").each(function () {
                            $(this).parents("ul, li").each(function () {
                                var treeView = $("#treeview").data("kendoTreeView");
                                treeView.expand($(this).parents("li"));
                                $(this).show();
                            });
                        });

                        $("#treeview .k-group .k-in:contains(" + filterText + ")").each(function () {
                            $(this).parents("ul, li").each(function () {
                                $(this).show();
                            });
                        });
                    }
                    else {
                        $("#treeview .k-group").find("li").show();
                        var nodes = $("#treeview > .k-group > li");

                        $.each(nodes, function (i, val) {
                            if (nodes[i].getAttribute("data-expanded") == null) {
                                $(nodes[i]).find("li").hide();
                            }
                        });

                        $(".selectAll").css("visibility", "visible");
                    }
                });
                $('#multiselect').attr('name', 'select[]');
                //$('#multiselect option').attr('selected');

                $("#multiselect option").each(function () {
                    $(this).attr('selected', 'true');

                });
                $('#loadingDiv').css('display', 'none');
                

            },
            error: function (error) {
                alert('error block');
            }
        });


});
function chbAllOnChange() {
    var checkedNodes = [];
    var treeView = $("#treeview").data("kendoTreeView");
    var isAllChecked = $('#chbAll').prop("checked");

    checkUncheckAllNodes(treeView.dataSource.view(), isAllChecked)

    if (isAllChecked) {
        setMessage($('#treeview input[type="checkbox"]').length);
    }
    else {
        setMessage(0);
    }
}
function checkUncheckAllNodes(nodes, checked) {
    for (var i = 0; i < nodes.length; i++) {
        nodes[i].set("checked", checked);

        if (nodes[i].hasChildren) {
            checkUncheckAllNodes(nodes[i].children.view(), checked);
        }
    }
}
function chbAllOnChange() {
    var checkedNodes = [];
    var treeView = $("#treeview").data("kendoTreeView");
    var isAllChecked = $('#chbAll').prop("checked");

    checkUncheckAllNodes(treeView.dataSource.view(), isAllChecked)

    if (isAllChecked) {
        setMessage($('#treeview input[type="checkbox"]').length);
    }
    else {
        setMessage(0);
    }
}
function onCheck() {
    var checkedNodes = [];
    var treeView = $("#treeview").data("kendoTreeView");

    getCheckedNodes(treeView.dataSource.view(), checkedNodes);
    setMessage(checkedNodes.length);
}

function onExpand(e) {
    if ($("#filterText").val() == "") {
        $(e.node).find("li").show();
    }
}

function setMessage(checkedNodes) {
    var message;

    if (checkedNodes > 0) {
        message = checkedNodes + " clients selected";
    }
    else {
        message = "0 clients selected";
    }

    $("#result").html(message);
}
/*==============Client Heirarchy CSS==================*/
