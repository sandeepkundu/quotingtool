var SITE_URL = $('#siteUrl').val();

$(function () {

    $('#example-one').hierarchySelect();
    var typeId = $('#type_id').val();
    var brandId = $('#brand_id').val();
    var brandEditId = $('#brandEditId').val();
    var brandFinalId = '';

    if (brandEditId) {

        brandFinalId = brandEditId;
        
    } else {
        
        brandFinalId = brandId;
    }

    if (typeId != '') {

        getBrands(typeId);
        typeList(typeId, brandFinalId);
    }

});

$('.product').on('change', function () {

    var typeId = $('#type_id').val();
    var brandId = $('#brand_id').val();

    typeList(typeId, brandId);

});

$('#type_id').on('change', function () {

    var typeId = $(this).val();

    if (typeId) {
        getBrands(typeId);
    } else {

        var brand = '<option value="">--Select Brand--</option>';
        $('#brand_id').html(brand);
    }

});

//Brand list
function getBrands(typeId) {
    console.log(SITE_URL + '/products/get-brand?typeId=' + typeId);

    $.ajax({
        type: 'get',
        url: SITE_URL + '/products/get-brand',
        data: {'typeId': typeId},
        dataType: 'JSON',
        success: function (obj) {

            var brand = '<option value="">--Select Brand--</option>';
            var brandEditId = $('#brandEditId').val();
            var bandOldId = $('#bandOldId').val();

            for (var i in obj) {

                if (obj[i].brand_id == brandEditId || obj[i].brand_id == bandOldId) {

                    var selected = "selected";

                } else {
                    selected = "";
                }

                brand += "<option value='" + obj[i].brand_id + "' " + selected + ">" + obj[i].brand_name + "</option>";
            }
            $('#brand_id').html(brand);

        },
        error: function (error) {
            alert('error occurred retreiving brand list');
        }
    });
}
function typeList(typeId, brandId) {

    var p = $('#processor_old').val();
    var screen_old = $('#screen_old').val();
    var dvd_old = $('#dvd_old').val();
    var hdd_old = $('#hdd_old').val();
    var ram_old = $('#ram_old').val();
    var productid = $('#id').val();
    var clientid = $('#parent_id').val();
    var processorEditId = $('#processorEditId').val();
    var screenEditId = $('#screenEditId').val();
    var dvdEditId = $('#dvdEditId').val();
    var hddEditId = $('#hddEditId').val();
    var ramEditId = $('#ramEditId').val();

    var isChecked = $('.proccessor').prop('checked');

        $.ajax({
            type: 'get',
            url: SITE_URL + '/products/listingPage',
            data: {'typeId': typeId, 'brandId': brandId, 'productid': productid, 'clientid': clientid },
            dataType: 'JSON',
            success: function (obj) {

                for (var i in obj) {
                    if (i == 'proccessor') {
                        var processor = '';
                        for (j in obj[i]) {

                            if (obj[i][j].option_id == p || obj[i][j].option_id == processorEditId) {

                                var selected = "selected";

                            } else {
                                selected = "";
                            }

                            processor += "<option value='" + obj[i][j].option_id + "' " + selected + ">" + obj[i][j].name + "</option>";
                        }
                        $('#processor_id').html(processor);
                    }
                    if (!isChecked) {
                        if (i == 'Screen') {
                            var Screen = '<option value="">--Select Screen--</option>';
                            for (j in obj[i]) {
                                if (obj[i][j].option_id == screen_old || obj[i][j].option_id == screenEditId) {
                                    var selected = "selected";

                                } else {
                                    selected = "";
                                }
                                Screen += "<option value='" + obj[i][j].option_id + "' " + selected + ">" + obj[i][j].name + "</option>";
                            }
                            $('#screen_id').html(Screen);
                        }

                        if (i == 'ram') {
                            var ram = '<option value="">--Select RAM--</option>';
                            for (j in obj[i]) {
                                if (obj[i][j].option_id == ram_old || obj[i][j].option_id == ramEditId) {
                                    var selected = "selected";

                                } else {
                                    selected = "";
                                }
                                ram += "<option value='" + obj[i][j].option_id + "' " + selected + ">" + obj[i][j].name + "</option>";
                            }
                            $('#ram_id').html(ram);
                        }

                        if (i == 'hdd') {
                            var hdd = '<option value="">--Select HDD--</option>';
                            for (j in obj[i]) {
                                if (obj[i][j].option_id == hdd_old || obj[i][j].option_id == hddEditId) {

                                    var selected = "selected";

                                } else {
                                    selected = "";
                                }
                                hdd += "<option value='" + obj[i][j].option_id + "' " + selected + ">" + obj[i][j].name + "</option>";
                            }
                            $('#hdd_id').html(hdd);
                        }

                        if (i == 'DVD') {
                            var DVD = '<option value="">--Select DVD--</option>';
                            for (j in obj[i]) {
                                if (obj[i][j].option_id == dvd_old || obj[i][j].option_id == dvdEditId) {
                                    var selected = "selected";

                                } else {
                                    selected = "";
                                }
                                DVD += "<option value='" + obj[i][j].option_id + "' " + selected + ">" + obj[i][j].name + "</option>";
                            }
                            $('#dvd_id').html(DVD);
                        }

                    }
                }
            },
            error: function (error) {
                alert('error block');
            }
        });

}
//Not in use
function typelistOld(type_id, brand_id) {
    var isChecked = $('.proccessor').prop('checked');
    $.ajax({
        type: 'get',
        url: SITE_URL + '/products/listingPage',
        data: {'type': typeId, 'brand': brandId},
        dataType: 'JSON',
        success: function (obj) {
            for (i in obj) {
                if (i == 'proccessor') {
                    var processor = '';
                    for (j in obj[i]) {
                        processor += "<option value='" + obj[i][j].option_id + "'>" + obj[i][j].name + "</option>";
                    }
                    $('#processor_id').html(processor);
                }
                if (!isChecked) {
                    if (i == 'Screen') {
                        var Screen = '<option value="">--Select Screen--</option>';
                        for (j in obj[i]) {
                            Screen += "<option value='" + obj[i][j].option_id + "'>" + obj[i][j].name + "</option>";
                        }
                        $('#screen_id').html(Screen);
                    }

                    if (i == 'ram') {
                        var ram = '<option value="">--Select RAM--</option>';
                        for (j in obj[i]) {
                            ram += "<option value='" + obj[i][j].option_id + "'>" + obj[i][j].name + "</option>";
                        }
                        $('#ram_id').html(ram);
                    }

                    if (i == 'hdd') {
                        var hdd = '<option value="">--Select HDD--</option>';
                        for (j in obj[i]) {
                            hdd += "<option value='" + obj[i][j].option_id + "'>" + obj[i][j].name + "</option>";
                        }
                        $('#hdd_id').html(hdd);
                    }

                    if (i == 'DVD') {
                        var DVD = '<option value="">--Select DVD--</option>';
                        for (j in obj[i]) {
                            DVD += "<option value='" + obj[i][j].option_id + "'>" + obj[i][j].name + "</option>";
                        }
                        $('#dvd_id').html(DVD);
                    }

                }
            }
        },
        error: function (error) {
            alert('error block');
        }
    });
}

$(document).ready(function () {
    
    $('#screen_star').hide('fast');

    if ($('#radio-type-rounded1').is(':checked')) {

        $('.processor').attr('disabled', 'disabled');
        $('#file').attr('disabled', '');
        $('.image').css('background', '#eee');

    }
    if ($('#radio-type-circle1').is(':checked')) {

        $(this).val('Model');
        var option = '<option value="" selected class="option" > --Select Pricing Type-- </option>';
        $('.model').val('Model');
        //$('#value_factor').prepend(option);
        $('.Model_star').show();
        $('.processor').removeAttr('disabled');
        $('#file').removeAttr('disabled', '');
        $('.image').css('background', 'white');
    }
    if ($('#radio-type-circle2').is(':checked')) {
        var option = '<option value="" selected class="option" > --Select Pricing Type-- </option>';
        $('.model').val('Model');
        //$('#value_factor').prepend(option);
        $('.Model_star').hide();
        $('#file').attr('disabled', '');
        $('.image').css('background', '#eee');
        $('.processor').attr('disabled', '');
        $('#processor_id').attr('disabled', '');
        $('#screen_id').removeAttr('disabled', '');
        $('#screen_star').show('fast');
    }


});

function radio(abc) {

    $("input[type='text']").val('');
    $('select').find('option').prop("selected", false);

    if (abc == 'Processor') {
        //$('#value_factor option').remove('.option');
        $('#processor').val(abc);
        $('.Model_star').hide();
        //$('.image').removeAttr('data-trigger','fileinput').css('background','#eee');
        $('#file').attr('disabled', '');
        $('.image').css('background', '#eee');
        $('.processor').attr('disabled', '');
        $('#processor_id').removeAttr('disabled', '');
        $('#screen_star').hide('fast');

    }
    if (abc == 'Model') {
        var option = '<option value="" selected class="option" > --Select Pricing Type-- </option>';
        $('.model').val('Model');
        //$('#value_factor').prepend(option);
        $('.Model_star').show();
        $('.processor').removeAttr('disabled');
        $('#processor_id').html('');
        $('#file').removeAttr('disabled', '');
        $('.image').css('background', 'white');
        $('#processor_id').removeAttr('disabled', '');
        $('#screen_star').hide('fast');
        //$('.image').attr('data-trigger','fileinput').css('background','white');
    }
    if (abc == 'Lcd') {

        var option = '<option value="" selected class="option" > --Select Pricing Type-- </option>';
        $('.model').val('Model');
        //$('#value_factor').prepend(option);
        $('.Model_star').show();
        $('#file').attr('disabled', '');
        $('.image').css('background', '#eee');
        $('.processor').attr('disabled', '');
        $('#processor_id').attr('disabled', '');
        $('#screen_id').removeAttr('disabled', '');
        $('#screen_star').show('fast');
        $(".lcd-option").attr('selected', '');

    }

    var typeId = $('#type_id').val();
    var brandId = $('#brand_id').val();

    if (typeId != '') {
        getBrands(typeId);
        typeList(typeId, brandId);
    }

}
