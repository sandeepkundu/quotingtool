 $(function () {
        $('.AcceptButton').click(function () {
   
                var QuoteId = $('.status').attr('id');
                var statusId = $('.status').attr('name');
                var SITE = $('#siteurl').val();
                if (statusId < 6) {
                   $('#loadingDiv').css('display', 'block');
                        $.ajax({
                            type: 'get',
                            url: SITE + '/hpUser/changeStatus',
                            data: {'status': statusId, 'name': QuoteId},
                            dataType: 'JSON',
                            success: function (obj) {
                                location.reload();
                            },
                            error: function (error) {
                                $('#loadingDiv').css('display', 'none');
                                alert('error block');
                            }
                        });
                      
                }

        });
    });

    $('.cancel').click(function () {
            var QuoteId = $('.status').attr('id');
            var statusId = 6;
            var SITE = $('#siteurl').val();
            if (statusId == 6) {
                if (confirm("Are you sure you want to cancel?")) {
                    $('#loadingDiv').css('display', 'block');
                    $.ajax({
                        type: 'get',
                        url: SITE + '/hpUser/changeStatus',
                        data: {'status': statusId, 'name': QuoteId},
                        dataType: 'JSON',
                        success: function (obj) {
                            location.reload();
                        },
                        error: function (error) {
                            $('#loadingDiv').css('display', 'none');
                            alert('error block');
                        }
                    });
                }
            }
        });

        $('.Upload').click(function () {
            $('#modal-bootstrap-tour').modal('show');
        });