$(document).ready(function () {
    $('#close').click(function () {
        location.reload();
    });

});
function reload() {
    location.reload();
}

function step01(productId) {
    //window.location.hash = '01';
    $('#loadingDiv').css('display', 'block');
    var val = productId;
    var SITE = $('#siteurl').val();
    var token = $("input[name=_token]").val();
    var quote_item_id = $('#quote_item_id').val();
    $.ajax({
        type: 'post',
        url: SITE + '/store/quote/step02',
        data: {'id': val, '_token': token,'quote_item_id': quote_item_id},
        dataType: 'JSON',
        success: function (obj) {
            $('.mainDiv').html(obj.html);
            $('#loadingDiv').css('display', 'none');
            window.scrollTo(0, 0);
        },
        error: function (error) {
            alert('error block');
        }
    });
}

function step02(pro1, pro2) {
    //window.location.hash = '02';
    $('#loadingDiv').css('display', 'block');
    var token = $("input[name=_token]").val();
     var quote_item_id = $('#quote_item_id').val();
    var SITE = $('#siteurl').val();
    $.ajax({
        type: 'post',
        url: SITE + '/store/totalSteps',
        data: {'pro1': pro1, '_token': token, 'pro2': pro2, 'lastStep' : 3,'quote_item_id': quote_item_id},
        dataType: 'JSON',
        success: function (obj) {
            if (obj.emptyData == 'empty') {
                $('#loadingDiv').css('display', 'none');
                $('#modal-bootstrap-tour').modal('show');
            } else {
                $('.mainDiv').html(obj.html);
                $('#loadingDiv').css('display', 'none');
                window.scrollTo(0, 0);
            }
        },
        error: function (error) {
            alert('error block');
        }
    });
}

function totalSteps() {
    $('#loadingDiv').css('display', 'block');
    var quote_item_id = $('#quote_item_id').val();
    var quote_id = $('#quote_id').val();
    var token = $("input[name=_token]").val();
    var SITE = $('#siteurl').val();
    var lastId = $('#lastId').val();
    var pro1 = $('#ProductValue').val();
    var pro2 = $('#BrandValue').val();
    var pro3 = $('#ProcessorValue').val();
    var pro4 = $('#ScreenValue').val();
    var pro5 = $('#DVDValue').val();
    var pro6 = $('#HDDValue').val();
    var pro7 = $('#RAMValue').val();
    var pro8 = $('.Defects').val();
    var pro9 = $('#ServicesValue').val();
    var modelid = $('#modelid').val();
    var pupid = $('#pupid').val();
    var pupsn = $('#pupsn').val();
    lastId++;

    console.log(SITE + '/hpUser/totalSteps?_token=' + token + '&pro1=' + pro1 + '&pro2=' + pro2 + '&pro3=' + pro3 + '&pro4=' + pro4 + '&pro5=' + pro5 + '&pro6=' + pro6 + '&pro7=' + pro7 + '&pro8=' + pro8 + '&pro9=' + pro9 + '&lastStep=' + lastId + '&quote_item_id=' + quote_item_id + '&modelid=' + modelid + '&pupid=' + pupid + '&pupsn=' + pupsn);

    $.ajax({
        type: 'post',
        url: SITE + '/store/totalSteps',
        data: {
            '_token': token,
            'pro1': pro1,
            'pro2': pro2, 
            'pro3': pro3, 
            'pro4': pro4, 
            'pro5': pro5,
            'pro6': pro6,
            'pro7': pro7,
            'pro8': pro8,
            'pro9': pro9,
            'lastStep': lastId,
            'quote_item_id': quote_item_id,
            'quote_id':quote_id,
            'modelid': modelid,
            'pupid': pupid,
            'pupsn': pupsn
        },
        dataType: 'JSON',
        success: function (obj) {
            $('#lastId').val(lastId);
            if (obj.emptyData == 'empty') {
                $('#loadingDiv').css('display', 'none');
                $('#modal-bootstrap-tour').modal('show');
            } else {
                $('.mainDiv').html(obj.html);
                $('#loadingDiv').css('display', 'none');
                window.scrollTo(0, 0);
            }
        },
        error: function (error) {
            alert('error block');
        }
    });
}

function Processor(){   
    $('.Processor').each(function () {
        if($(this).val() != 0 && $(this).val() != ''){
            var abc =  $('#ProcessorValue').val();
            abc =  $(this).val();
        }  
    })
    if($('#ProcessorValue').val() != 0 && $('#ProcessorValue').val() != ''){
        totalSteps();
    }else {
        $('#modal-body').html('Please select proccessor !!!');
        $('#modal-bootstrap-validation').modal('show');
        return false; 
    }
}

function Screen(){
    var ScreenValue = $('.Screen').val();
    if(ScreenValue != 0){        
        totalSteps();
    }else{
        $('#modal-body').html('Please select screen size !!!');
        
        $('#modal-bootstrap-validation').modal('show');
    }   
}

function Services(a){
   $('#ServicesValue').val(a);
   totalSteps();
}

function backSelection(step){
    if(step != null){
       $('#lastId').val(step-1);
       
       for(var i=0; i<9; i++){
            $("input[abc="+step+"]").val('');
            step = step+1;
       }
       totalSteps();
    }
}


$('#tags').keyup(function(){
    var query= $(this).val();
    var SITE = $('#siteurl').val();
    var token = $("input[name=_token]").val();
    var client_id = $('#clientID').val();
    if(query.length > 1){
        //console.log(SITE +'/store/model_auto_search?search='+ query );  
		//$("#search_overlay").fadeTo("fast",.8).height($(document).height()).width($(window).width());
		//$(".search-bar #tags").addClass("light-bg");
        $.ajax({
            type:'post',
            url:SITE +'/store/model_auto_search?search='+ query,
            data: {'_token': token,'client_id':client_id},
            dataType:'JSON',
            success:function(obj){
                console.log(obj.length);
                var i =  0;
                var li = '';

                for(var item in obj){ 
                    var image = '';
                    if (obj[item].image1 != '') {
                        var image = obj[item].image1;
                    } 
                    if(obj[item].table_name == 'product'){                     
                        li += '<li class="list-group-item"><a href="' + SITE + '/modelSearch?pid=' + obj[item].id + '&model=' + obj[item].model + '" class="title">';
                        li += '<div style="height:50px;float:left;" class="image"><img height="50px" width="60px" src="' + SITE + '/product/' + image + '" ></div>';
                        li += '<div style="float:left; width:52%"><span class="item-title">Model:</span> <span class="item-detail">' + obj[item].model + '</span>';
                        li += '<br><span class="item-title">Product:</span> <span class="item-detail">' + obj[item].type_name + '</span>';
                        li += '<br><span class="item-title">Brand:</span> <span class="item-detail">' + obj[item].brand_name + '</span></div>';
                        li += '<div style="float:left; width:30%"><span class="item-title">Processor:</span> <span class="item-detail">' + obj[item].processor_name + '</span>';
                        li += '<br><span class="item-title">Screen:</span> <span class="item-detail">' + obj[item].screen_name + '</span></div> </a></li>';
                    }
                    if(obj[item].table_name == 'pup_contracts'){
                        li += '<li class="list-group-item"><a href="'+SITE+'/hpUser/SerialNumserSearch?pid=' + obj[item].id + '&serial_number='+obj[item].model+'" class="title">';
                        li += '<div style="height:50px;float:left;" class="image"><img height="50px" width="60px" src="'+SITE + '/product/'+ image + '" ></div>';
                        li += '<div style="float:left; width:45%"><span class="item-title">Serial Number:</span> <span class="item-detail">' + obj[item].model + '</span>';
                        li += '<br><span class="item-title">Product:</span> <span class="item-detail">' + obj[item].type_name + '</span><br><span class="item-title">Brand:</span> <span class="item-detail">HP</span></div>';
                        li += '<div style="float:left; width:30%"><span class="item-title">PUP Contract:</span>';
                        li += '<span class="item-title">Processor:</span> <span class="item-detail">'+obj[item].processor_name+'</span>';
                        li += '<br><span class="item-title">Screen:</span> <span class="item-detail">' + obj[item].screen_name + '</span></div> </a></li>';
                    }                        
                }

                $('.search-ul').html(li).addClass("open");
                $(".close-search").css("display", "block");
            },
            error:function(error){

            }
        }); 
    } else {
        $('.search-ul').removeClass("open").html('');
        //$("#search_overlay").fadeTo("fast",0).height('0px').width('0px');
        //$(".search-bar #tags").removeClass("light-bg");
    }     
})
$( ".close-search" ).click(function() {
    $('#tags').val("").focus();
    $(this).css("display", "none");
    $('.search-ul').removeClass("open").html("");
});
