
function step01(productId){
	$('#loadingDiv').css('display','block');
	var val = productId;
	var SITE = $('#siteurl').val();
	var token = $("input[name=_token]").val();
	$.ajax({
		type:'post',
		url:SITE+'/hpUser/step02',
		data:{'id':val, '_token':token},
		dataType:'JSON',
		success:function(obj){
			$('.mainDiv').html(obj.html);
			$('#loadingDiv').css('display','none');
			window.scrollTo(0,0);
		},
		error:function(error){
			alert('error block');
		}
	});
}

function step02(brandId, productId){
	$('#loadingDiv').css('display','block');
	var token = $("input[name=_token]").val();
	var SITE = $('#siteurl').val();
	$.ajax({
		type:'post',
		url:SITE+'/hpUser/step03',
		data:{'brandId':brandId, '_token':token, 'productId':productId},
		dataType:'JSON',
		success:function(obj){
			$('.mainDiv').html(obj.html);
			$('#loadingDiv').css('display','none');
			window.scrollTo(0,0);
		},
		error:function(error){
			alert('error block');
		}
	});
}


function step03(brandId, productId, proccessorId){
	$('#loadingDiv').css('display','block');
	var token = $("input[name=_token]").val();
	var SITE = $('#siteurl').val();
	$.ajax({
		type:'post',
		url:SITE+'/hpUser/step04',
		data:{'brandId':brandId, '_token':token, 'productId':productId, 'proccessorId':proccessorId},
		dataType:'JSON',
		success:function(obj){
			$('.mainDiv').html(obj.html);
			$('#loadingDiv').css('display','none');
			window.scrollTo(0,0);
		},
		error:function(error){
			alert('error block');
		}
	});
}


function step04(brandId, productId, proccessorId, Pro1){
	$('#loadingDiv').css('display','block');
	
	var token = $("input[name=_token]").val();
	var SITE = $('#siteurl').val();
	$.ajax({
		type:'post',
		url:SITE+'/hpUser/step05',
		data:{'brandId':brandId, '_token':token, 'productId':productId, 'proccessorId':proccessorId,'Pro1':Pro1},
		dataType:'JSON',
		success:function(obj){
			$('.mainDiv').html(obj.html);
			$('#loadingDiv').css('display','none');
			window.scrollTo(0,0);
		},
		error:function(error){
			alert('error block');
		}
	});
}

function step05(brandId, productId, proccessorId, Pro1, pro2){
	$('#loadingDiv').css('display','block');
	var SITE = $('#siteurl').val();
	var token = $("input[name=_token]").val();
	$.ajax({
		type:'post',
		url:SITE+'/hpUser/step06',
		data:{'brandId':brandId, '_token':token, 'productId':productId, 'proccessorId':proccessorId,'Pro1':Pro1,'pro2':pro2},
		dataType:'JSON',
		success:function(obj){
			$('.mainDiv').html(obj.html);
			$('#loadingDiv').css('display','none');
			window.scrollTo(0,0);
		},
		error:function(error){
			alert('error block');
		}
	});
}


function step06(brandId, productId, proccessorId, Pro1, pro2,pro3){
	$('#loadingDiv').css('display','block');
	var token = $("input[name=_token]").val();
	var SITE = $('#siteurl').val();
	$.ajax({
		type:'post',
		url:SITE+'/hpUser/step07',
		data:{'brandId':brandId, '_token':token, 'productId':productId, 'proccessorId':proccessorId,'Pro1':Pro1,'pro2':pro2,'pro3':pro3},
		dataType:'JSON',
		success:function(obj){
			$('.mainDiv').html(obj.html);
			$('#loadingDiv').css('display','none');
			window.scrollTo(0,0);
		},
		error:function(error){
			alert('error block');
		}
	});
}


function step07(brandId, productId, proccessorId, Pro1, pro2,pro3,pro4){
	$('#loadingDiv').css('display','block');
	var token = $("input[name=_token]").val();
	var SITE = $('#siteurl').val();
	$.ajax({
		type:'post',
		url:SITE+'/hpUser/step08',
		data:{'brandId':brandId, '_token':token, 'productId':productId, 'proccessorId':proccessorId,'Pro1':Pro1,'pro2':pro2,'pro3':pro3,'pro4':pro4},
		dataType:'JSON',
		success:function(obj){
			$('.mainDiv').html(obj.html);
			$('#loadingDiv').css('display','none');
			window.scrollTo(0,0);
		},
		error:function(error){
			alert('error block');
		}
	});
}

function step08(brandId, productId, proccessorId, Pro1, pro2,pro3,pro4,pro5){
	$('#loadingDiv').css('display','block');
	var token = $("input[name=_token]").val();
	var SITE = $('#siteurl').val();
	$.ajax({
		type:'post',
		url:SITE+'/hpUser/step09',
		data:{'brandId':brandId, '_token':token, 'productId':productId, 'proccessorId':proccessorId,'Pro1':Pro1,'pro2':pro2,'pro3':pro3,'pro4':pro4,'pro5':pro5},
		dataType:'JSON',
		success:function(obj){
			$('.mainDiv').html(obj.html);
			$('#loadingDiv').css('display','none');
			window.scrollTo(0,0);
		},
		error:function(error){
			alert('error block');
		}
	});
}


function step09(brandId, productId, proccessorId, Pro1, pro2,pro3,pro4,pro5,pro6){
	$('#loadingDiv').css('display','block');
	var token = $("input[name=_token]").val();
	var SITE = $('#siteurl').val();
	$.ajax({
		type:'post',
		url:SITE+'/hpUser/step10',
		data:{'brandId':brandId, '_token':token, 'productId':productId, 'proccessorId':proccessorId,'Pro1':Pro1,'pro2':pro2,'pro3':pro3,'pro4':pro4,'pro5':pro5,'pro6':pro6},
		dataType:'JSON',
		success:function(obj){
			$('.mainDiv').html(obj.html);
			$('#loadingDiv').css('display','none');
			window.scrollTo(0,0);
		},
		error:function(error){
			alert('error block');
		}
	});
}