
function step01(productId) {
    $('#loadingDiv').css('display', 'block');
    var val = productId;
    var SITE = $('#siteurl').val();
    var quote_item_id = $('#quote_item_id').val();   
    var token = $("input[name=_token]").val();
    console.log(SITE + '/enterprises/step02?id='+ val+ '&_token='+ token+'&quote_item_id='+ quote_item_id);
    $.ajax({
        type: 'post',
        url: SITE + '/enterprises/step02',
        data: {'id': val, '_token': token,'quote_item_id': quote_item_id},
        dataType: 'JSON',
        success: function (obj) {
            $('.mainDiv').html(obj.html);
            $('#loadingDiv').css('display', 'none');
            window.scrollTo(0, 0);
        },
        error: function (error) {
            alert('Something went to wrong');
        }
    });
}




function step02(pro1, pro2) {
    $('#loadingDiv').css('display', 'block');
    var token = $("input[name=_token]").val();
    var quote_item_id = $('#quote_item_id').val();
    var SITE = $('#siteurl').val();

    $.ajax({
        type: 'post',
        url: SITE + '/enterprises/totalSteps',
        data: {'pro1': pro1, '_token': token, 'pro2': pro2, 'lastStep' : 3,'quote_item_id': quote_item_id},
        dataType: 'JSON',
        success: function (obj) {

            if (obj.emptyData == 'empty') {
                $('#loadingDiv').css('display', 'none');
                $('#modal-bootstrap-tour').modal('show');
            } else {

                //window.location.href = "Enterprises/index?qiid=" + quote_item_id;

                $('.mainDiv').html(obj.html);
                $('#loadingDiv').css('display', 'none');
                window.scrollTo(0, 0);
            }
        },
        error: function (error) {
            alert('an error occured getting the the list of processors');
        }
    });
}

function totalSteps(pro1, pro2, pro3, pro4, pro5, pro6, pro7, pro8, pro9, lastId, modelid) {
    $('#loadingDiv').css('display', 'block');

    var token = $("input[name=_token]").val();
    var quote_item_id = $('#quote_item_id').val();
    var SITE = $('#siteurl').val();

    console.log(SITE + '/enterprises/totalSteps?_token=' + token + '&pro1=' + pro1 + '&pro2=' + pro2 + '&pro3=' + pro3 + '&pro4=' + pro4 + '&pro5=' + pro5 + '&pro6=' + pro6 + '&pro7=' + pro7 + '&pro8=' + pro8 + '&pro9=' + pro9 + '&lastStep=' + lastId + '&quote_item_id=' + quote_item_id + '&modelid=' + modelid);

    lastId++;
    $.ajax({
        type: 'post',
        url: SITE + '/enterprises/totalSteps',
        data: {
            '_token': token,
            'pro1': pro1,
            'pro2': pro2, 
            'pro3': pro3, 
            'pro4': pro4, 
            'pro5': pro5,
            'pro6': pro6,
            'pro7': pro7,
            'pro8': pro8,
            'pro9': pro9,
            'lastStep': lastId,
            'quote_item_id': quote_item_id,
            'modelid': modelid
        },
        dataType: 'JSON',
        success: function (obj) {

            $('#lastId').val(lastId);
            if (obj.emptyData == 'empty') {
                $('#loadingDiv').css('display', 'none');
                $('#modal-bootstrap-tour').modal('show');
            } else {
                $('.mainDiv').html(obj.html);
                $('#loadingDiv').css('display', 'none');
                window.scrollTo(0, 0);
            }
        },
        error: function (error) {
            alert('an error ocurred processing this request.');
        }
    });
}

function Processor1(){
    $('.Processor').each(function () {
        if($(this).val() != 0 && $(this).val() != ''){
            var abc =  $('#ProcessorValue').val();
            abc =  $(this).val();
        }  
    })
    if($('#ProcessorValue').val() != 0 && $('#ProcessorValue').val() != ''){
        var pro1 = $('#ProductValue').val();
        var pro2 = $('#BrandValue').val();
        var pro3 = $('#ProcessorValue').val();
        var pro4 = $('#ScreenValue').val();
        var pro5 = $('#DVDValue').val();
        var pro6 = $('#HDDValue').val();
        var pro7 = $('#RAMValue').val();
        var pro8 = $('.Defects').val();
        var pro9 = $('#ServicesValue').val();
        var lastId = $('#lastId').val();
        var model = $('#modelid').val();
       totalSteps(pro1, pro2, pro3, pro4, pro5, pro6, pro7, pro8, pro9, lastId, model);

    }else {
        $('#modal-body').html('Please select proccessor !!!');
        $('#modal-bootstrap-validation').modal('show');
        return false; 
    }    
}

function Screen(){
    var ScreenValue = $('.Screen').val();

    if(ScreenValue != 0){
        var pro1 = $('#ProductValue').val();
        var pro2 = $('#BrandValue').val();
        var pro3 = $('#ProcessorValue').val();
        var pro4 = $('#ScreenValue').val();
        var pro5 = $('#DVDValue').val();
        var pro6 = $('#HDDValue').val();
        var pro7 = $('#RAMValue').val();
        var pro8 = $('.Defects').val();
        var pro9 = $('#ServicesValue').val();
        var lastId = $('#lastId').val();
        var model = $('#modelid').val();
        totalSteps(pro1, pro2, pro3, pro4, pro5, pro6, pro7, pro8, pro9, lastId, model);

    }else{
        $('#modal-body').html('Please select screen size !!!');
        
        $('#modal-bootstrap-validation').modal('show');
    }   

}
function QuoteEditSave(){
      var b = [];
      $('.activeClass').each(function () {
         b.push(this.id);
      });

    $('.Defects').val(b);

    $('#loadingDiv').css('display', 'block');
    var SITE = $('#siteurl').val();
    var QuoteItemId = $('#quote_item_id').val();
    var processor_id = $('#ProcessorValue').val();
    var screen_id = $('#ScreenValue').val();
    var dvd_id = $('#DVDValue').val();
    var hdd_id = $('#HDDValue').val();
    var ram_id = $('#RAMValue').val();
    var defects = $('.Defects').val();
    var services_id = $('#ServicesValue').val();

    console.log(SITE+'/Enterprises/QuoteUpdate?quoteItemId='+QuoteItemId+'&processor_id='+processor_id+'&screen_id='+screen_id+'&dvd_id='+dvd_id+'&hdd_id='+hdd_id+'&ram_id='+ram_id+'&defects='+defects+'&services_id='+services_id);
        
    $.ajax({
        type:'get',
        url:SITE+'/Enterprises/QuoteUpdate',
        data:{
            'quoteItemId':QuoteItemId,
                'processor_id':processor_id,
                'screen_id':screen_id,
                'dvd_id':dvd_id,
                'hdd_id':hdd_id,
                'ram_id':ram_id,
                'defects':defects,
                'services_id':services_id,
        },
        dataType:'JSON',
        success: function (obj) {
            if (obj.status == 'error') {
                if (obj.message != '') {
                    alert(obj.message);
                } else {
                    alert('An error has occured processing this request.');
                }
            } 
            window.location.href = "/enterprises/index?qiid=" + QuoteItemId;

        },
        error:function(error){
            console.log(error);
            $('#loadingDiv').css('display', 'none');
            if (error.message != '') {
                alert(error.message);
            } else {
                alert('An error has occured.');
            }
        }
    });      
}

function Services(a){
    $('#ServicesValue').val(a);
    var pro1 = $('#ProductValue').val();
    var pro2 = $('#BrandValue').val();
    var pro3 = $('#ProcessorValue').val();
    var pro4 = $('#ScreenValue').val();
    var pro5 = $('#DVDValue').val();
    var pro6 = $('#HDDValue').val();
    var pro7 = $('#RAMValue').val();
    var pro8 = $('.Defects').val();
    var pro9 = $('#ServicesValue').val();
    var lastId = $('#lastId').val();
    var model = $('#modelid').val();
    totalSteps(pro1, pro2, pro3, pro4, pro5, pro6, pro7, pro8, pro9, lastId, model)
}

function reload(){
    location.reload();
}

function backSelection(step){
    if(step != null){
       $('#lastId').val(step-1);
       
       for(var i=0; i<9; i++){
            $("input[abc="+step+"]").val('');
            step = step+1;
       }
        var pro1 = $('#ProductValue').val();
        var pro2 = $('#BrandValue').val();
        var pro3 = $('#ProcessorValue').val();
        var pro4 = $('#ScreenValue').val();
        var pro5 = $('#DVDValue').val();
        var pro6 = $('#HDDValue').val();
        var pro7 = $('#RAMValue').val();
        var pro8 = $('.Defects').val();
        var pro9 = $('#ServicesValue').val();
        var lastId = $('#lastId').val();
        var model = $('#modelid').val();
        totalSteps(pro1, pro2, pro3, pro4, pro5, pro6, pro7, pro8, pro9, lastId, model);
    }
}

$('#tags').keyup(function(){
    var query= $(this).val();
    //alert(query);
    var SITE = $('#siteurl').val();
    var token = $("input[name=_token]").val();
    var client_id = $('#clientID').val();
    if(query.length > 1){   
		//$("#search_overlay").fadeTo("fast",.7).height($(document).height()).width($(window).width());
		//$(".search-bar #tags").addClass("light-bg");
        $.ajax({
            type:'post',
            url:SITE +'/store/model_auto_search?search='+ query,
            data: {'_token': token,'client_id':client_id},
            dataType:'JSON',
            success:function(obj){
                var i =  0;
                var li = '';

                for(var item in obj){
                      
                    if(obj[item].image1 != ''){
                        var image = obj[item].image1;
                    }else if(obj[item].image2 != '') {
                        var image = obj[item].image2;
                    } 

                    if(obj[item].table_name == 'product'){                     

                        li += '<li class="list-group-item"><a href="' + SITE + '/EnterprisesSearch?pid=' + obj[item].id + '&model=' + obj[item].model + '" class="title">';
                        li += '<div style="height:50px;float:left;" class="image"><img height="50px" width="60px" src="' + SITE + '/product/' + image + '" ></div>';
                        li += '<div style="float:left; width:52%"><span class="item-title">Model:</span> <span class="item-detail">' + obj[item].model + '</span>';
                        li += '<br><span class="item-title">Product:</span> <span class="item-detail">' + obj[item].type_name + '</span>';
                        li += '<br><span class="item-title">Brand:</span> <span class="item-detail">' + obj[item].brand_name + '</span></div>';
                        li += '<div style="float:left; width:30%"><span class="item-title">Processor:</span> <span class="item-detail">' + obj[item].processor_name + '</span>';
                        li += '<br><span class="item-title">Screen:</span> <span class="item-detail">' + obj[item].screen_name + '</span></div> </a></li>';
                        
                    }

                    //if(obj[item].table_name == 'pup_contracts'){
                    //    li += '<li class="list-group-item" style="height:74px;"><a href="' + SITE + '/EnterprisesSerialNumserSearch?pid=' + obj[item].id + '&serial_number=' + obj[item].model + '" class="title"><div style="height:50px;float:left;" class="image"><img height="50px" width="60px" src="' + SITE + '/product/' + image + '" ></div>Serial Number: ' + obj[item].model + '<br>Product: ' + obj[item].type_name + '<br>Processor: ' + obj[item].processor_name + ' </a><b style="float:right;color:#767676;">PUP Contracts</b></li>';
                    //}                        
                }

                $('.search-ul').html(li).addClass("open");
                $(".close-search").css("display", "block");
            },
            error:function(error){   }
        }); 
    } else {
        $('.search-ul').removeClass("open").html("");
        //$("#search_overlay").fadeTo("fast",0).height('0px').width('0px');
        //$(".search-bar #tags").removeClass("light-bg");
    }     
});
$( ".close-search" ).click(function() {
    $('#tags').val("").focus();
    $(this).css("display", "none");
    $('.search-ul').removeClass("open").html("");
});
