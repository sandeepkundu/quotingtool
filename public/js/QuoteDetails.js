    $(document).ready(function () {
        $('.editCart').click(function () {
            $(this).closest('tr').find(".quantityUpdate").css('display', 'block');
            $(this).closest('tr').find("input").removeClass('quanitity').removeAttr('readonly');
        });
    });
    /*=======================enterprise status=====================================*/
    $(function () {
        $('.changeStatus').click(function () {
            var QuoteId = $(this).attr('id');
            var statusId = $(this).attr('name');
            var SITE = $('#siteurl').val();
            if (statusId < 6) {
                if (confirm("Are you sure to change the status")) {
                    $('#loadingDiv').css('display', 'block');
                    $.ajax({
                        type: 'get',
                        url: SITE + '/hpUser/changeStatus',
                        data: {'status': statusId, 'name': QuoteId},
                        dataType: 'JSON',
                        success: function (obj) {
                            location.reload();
                        },
                        error: function (error) {
                            alert('error block');
                        }
                    });
                }
            }
        });

        $('.scancel').click(function () {
            var QuoteId = $(this).attr('val');
            var statusId = $(this).attr('name');
            var SITE = $('#siteurl').val();
            if (statusId == 6) {
                if (confirm("Are you sure you want to cancel?")) {
                    $.ajax({
                        type: 'get',
                        url: SITE + '/hpUser/changeStatus',
                        data: {'status': statusId, 'name': QuoteId},
                        dataType: 'JSON',
                        success: function (obj) {
                            $('#loadingDiv').css('display', 'none');
                            location.reload();
                        },
                        error: function (error) {
                            alert('error block');
                        }
                    });
                }
            }
        });

        $('.Upload').click(function () {
            var a = $(this).attr('val');
            $('#hiddenQuoteId').val(a);
            $('#modal-bootstrap-tour').modal('show');
        });
    /*=======================enterprise status=====================================*/
        $('.quantityUpdate').click(function () {
            $('#loadingDiv').css('display', 'block');
            var SITE = $('#siteurl').val();
            var QuoteItemId = $(this).attr('id');
            var quantity = $('.' + QuoteItemId).val();
            $.ajax({
                type: 'get',
                url: SITE + '/Enterprises/quantityUpdate',
                data: {'quoteItemId': QuoteItemId, 'quantity': quantity},
                dataType: 'JSON',
                success: function (obj) {
                    location.reload();
                },
                error: function (error) {
                    $('#loadingDiv').css('display', 'none');
                    alert('error block');
                }
            });
        });
    });

    $('.detailItem').click(function(){
        $('#loadingDiv').css('display', 'block');
        var SITE = $('#siteurl').val();
        var QuoteItemId = $(this).attr('name');
        $.ajax({
            type: 'get',
            url: SITE + '/QuoteManagements/ItemDetail',
            data: {'quoteItemId': QuoteItemId},
            dataType: 'HTML',
            success: function (obj) {
                $('.detailPage').html(obj);
                $('#Itemdetail').modal('show');
                $('#loadingDiv').css('display', 'none');
            },
            error: function (error) {
                $('#loadingDiv').css('display', 'none');
                alert('error block');
            }
        });
    });

    /*=======================enterprise status=====================================*/
    $(function () {
            $('.AcceptButton').click(function () {

                var QuoteId = $('.status').attr('id');

                var statusId = $('.status').attr('name');
                var SITE = $('#siteurl').val();
                var quote_id = $('#quote_id').val();

                if (statusId < 6 && QuoteId != '') {
                    $('#loadingDiv').css('display', 'block');
                    $.ajax({
                        type: 'get',
                        url: SITE + '/Enterprises/changeStatus',
                        data: {'status': statusId, 'name': QuoteId, 'Quote_id':quote_id},
                        dataType: 'JSON',
                        success: function (obj) {
                            location.reload();
                        },
                        error: function (error) {
                            $('#loadingDiv').css('display', 'none');
                            alert('error block');
                        }
                    });
                }
            });


    $('.eedit').click(function () {
                var QuoteId = $('.status').attr('id');
                var statusId = $('.status').attr('name');
                var SITE = $('#siteurl').val();
                var quote_id = $('#quote_id').val();
                
                if (statusId == 4 && QuoteId != '') {

                    $('#loadingDiv').css('display', 'block');
                    $.ajax({
                        type: 'get',
                        url: SITE + '/Enterprises/changeStatus',
                        data: {'status': statusId, 'name': QuoteId, 'Quote_id':quote_id,'edit':'edit'},
                        dataType: 'JSON',
                        success: function (obj) {
                            location.reload();
                        },
                        error: function (error) {
                            $('#loadingDiv').css('display', 'none');
                            alert('error block');
                        }
                    });

                }
    });

    $('.ecancel').click(function () {
        var QuoteId = $('.status').attr('id');
        var statusId = $('.status').attr('name');
        var SITE = $('#siteurl').val();
        var quote_id = $('#quote_id').val();

        if (statusId < 7) {

            $.ajax({
                type: 'get',
                url: SITE + '/Enterprises/changeStatus',
                data: {'status': statusId, 'name': QuoteId, 'Quote_id':quote_id,'cancel':'cancel'},
                dataType: 'JSON',
                success: function (obj) {
                    location.reload();
                },
                error: function (error) {
                    $('#loadingDiv').css('display', 'none');
                    alert('error block');
                }
            });

        }
    });
})
/*=======================enterprise=====================================*/
var imageLoader = document.getElementById('filePhoto');
imageLoader.addEventListener('change', handleImage, false);

function handleImage(e) {
    var reader = new FileReader();
    reader.onload = function (event) {
        $('#scrId').val(event.target.result);
        $('.uploader img').attr('src',event.target.result);
        
    }
    console.log(e.target.files[0]);
    reader.readAsDataURL(e.target.files[0]);
}

var imageLoader2 = document.getElementById('filePhoto2');
imageLoader2.addEventListener('change', handleImage2, false);

function handleImage2(e) {
    var reader = new FileReader();
    reader.onload = function (event) {
        $('#scrId2').val(event.target.result);
        $('.uploader2 img').attr('src',event.target.result);
        
    }
    console.log(e.target.files[0]);
    reader.readAsDataURL(e.target.files[0]);
}

var imageLoader3 = document.getElementById('filePhoto3');
imageLoader3.addEventListener('change', handleImage3, false);

function handleImage3(e) {
    var reader = new FileReader();
    reader.onload = function (event) {
        $('#scrId3').val(event.target.result);
        $('.uploader3 img').attr('src',event.target.result);
        
    }
    console.log(e.target.files[0]);
    reader.readAsDataURL(e.target.files[0]);
}

    $('#uploadimageByWebcam').click(function(){
        $('#modal-bootstrap-tour').modal('hide');
        var quote_id = $('#hiddenQuoteId').val();
        $('#quoteId').val(quote_id);
        $('#modal-bootstrap-uploadImageByWebcam').modal('show');
        startWebcam();
    });

function imageuploadsubmit(){
    var serial_number = $('#serial_numberid').val();
    var scrId = $('#scrId').val();
    var scrId2 = $('#scrId2').val();
    var scrId3 = $('#scrId3').val();
    if(scrId == '' && scrId2 == '' && scrId3 == ''){
        $('.errormsgforimageupload').html('please select atleast one image');
        return false;
    }else{
         $('.errormsgforimageupload').hide();
    }
    if(serial_number == ''){
    $('.megerrorserialno').html('Serial number cant not be left blank!');
    return false;
    }

}


function save_img_webcam(){
    var serial_number = $('.serial_numberid').val();
    var image1 = $('.image1').val();
    var image2 = $('.image2').val();
    var image3 = $('.image3').val();
    //alert(image1.length);
    if(image1 == '' && image2 == '' && image3 == ''){
        $('.errormsgforimageuploadwebcam').html('please select atleast one image');
        return false;
    } else if (image1.length < 1000){
        if(image2.length < 1000){
            if(image3.length < 1000){
                $('.errormsgforimageuploadwebcam').html('please select atleast one image');
                return false;    
            }
        }
    }else{
         $('.errormsgforimageuploadwebcam').hide();
    }


    if(serial_number == ''){
        $('.megerrorserialnowebcam').html('Serial number cant not be left blank!');
        return false;
    }
}


/*$(document).ready(function(){
    $('.save-img').click(function(){
        var serial_number = $('.serial_numberid').val();
        var image1 = $('.image1').val();
        var image2 = $('.image2').val();
        var image3 = $('.image3').val();

        if(image1 == '' && image2 == '' && image3 == ''){
            $('.errormsgforimageuploadwebcam').html('please select atleast one image');
            return false;
        }else{
             $('.errormsgforimageuploadwebcam').hide();
        }
        if(serial_number == ''){
            $('.megerrorserialnowebcam').html('Serial number cant not be left blank!');
            return false;
        }
    })
})*/

$('.edit_price_margin').on('click',function(){
    var quoteItemId = $(this).attr('name');
    var SITE = $('#siteurl').val();
 $('#loadingDiv').css('display', 'block');
    if(quoteItemId){
        $.ajax({
        type: 'get',
        url: SITE + '/quote-managements/edit-price-margin',
        data: {'quoteItemId': quoteItemId},
        dataType: 'JSON',
        success: function (obj) {
            if(obj.success == 200){

                $('#brand-margin').val(obj['quoteItem']['unit_margin_brand']);
                $('#country-margin').val(obj['quoteItem']['unit_margin_country']);
                $('#screen-margin').val(obj['quoteItem']['unit_margin_screen']);
                $('#logistic-margin').val(obj['quoteItem']['logistics_margin']);
                $('#logistic-product-margin').val(obj['quoteItem']['logistics_product_margin']);
                $('#service-margin').val(obj['quoteItem']['services_margin']);
                $('#client-margin').val(obj['quoteItem']['unit_margin_customer']);

                $('#item-id').val(obj['quoteItem']['id']);

               $('#update-price-margin-adjustment').modal('show');
                $('#loadingDiv').css('display', 'none');
            }
        },
        error: function (error) {
            alert('error block');
        }
    });
    }
})

