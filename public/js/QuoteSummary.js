 $('#uploadimageByWebcam').click(function(){
            $('#modal-bootstrap-tour').modal('hide');
            var quote_id = $('#hiddenQuoteId').val();
            $('#quoteId').val(quote_id);
            $('#modal-bootstrap-uploadImageByWebcam').modal('show');
            startWebcam();
        });

        var imageLoader = document.getElementById('filePhoto');
        imageLoader.addEventListener('change', handleImage, false);

        function handleImage(e) {
            var reader = new FileReader();
            reader.onload = function (event) {
                $('#scrId').val(event.target.result);
                $('.uploader img').attr('src',event.target.result);

            }
            console.log(e.target.files[0]);
            reader.readAsDataURL(e.target.files[0]);
        }

        var imageLoader2 = document.getElementById('filePhoto2');
        imageLoader2.addEventListener('change', handleImage2, false);

        function handleImage2(e) {
            var reader = new FileReader();
            reader.onload = function (event) {
                $('#scrId2').val(event.target.result);
                $('.uploader2 img').attr('src',event.target.result);

            }
            console.log(e.target.files[0]);
            reader.readAsDataURL(e.target.files[0]);
        }

        var imageLoader3 = document.getElementById('filePhoto3');
        imageLoader3.addEventListener('change', handleImage3, false);

        function handleImage3(e) {
            var reader = new FileReader();
            reader.onload = function (event) {
                $('#scrId3').val(event.target.result);
                $('.uploader3 img').attr('src',event.target.result);

            }
            console.log(e.target.files[0]);
            reader.readAsDataURL(e.target.files[0]);
        }

        function imageuploadsubmit(){
            var serial_number = $('#serial_numberid').val();
            var scrId = $('#scrId').val();
            var scrId2 = $('#scrId2').val();
            var scrId3 = $('#scrId3').val();
            if(scrId == '' && scrId2 == '' && scrId3 == ''){
            $('.errormsgforimageupload').html('please select atleast one image');
                return false;
            }else{
             $('.errormsgforimageupload').hide();
         }
         if(serial_number == ''){
            $('.megerrorserialno').html('Serial number cant not be left blank!');
            return false;
        }

    }

    function save_img_webcam(){
        var serial_number = $('.serial_numberid').val();
        var image1 = $('.image1').val();
        var image2 = $('.image2').val();
        var image3 = $('.image3').val();
        if(image1 == '' && image2 == '' && image3 == ''){
            $('.errormsgforimageuploadwebcam').html('please select atleast one image');
            return false;
        } else if (image1.length < 1000){
            if(image2.length < 1000){
                if(image3.length < 1000){
                    $('.errormsgforimageuploadwebcam').html('please select atleast one image');
                    return false;    
                }
            }
        }else{
         $('.errormsgforimageuploadwebcam').hide();
     }

     if(serial_number == ''){
        $('.megerrorserialnowebcam').html('Serial number cant not be left blank!');
        return false;
    }
}


