//================================ First Image ================================//
(function() {
  var width = 100;    // We will scale the photo width to this
  var height = 100;     // This will be computed based on the input stream
  var streaming = false;
  var video = null;
  var canvas3 = null;
  var photo = null;
  var startbutton3 = null;

  function startup() {
    video = document.getElementById('video');
    canvas3 = document.getElementById('canvas3');
    photo = document.getElementById('photo');
    startbutton3 = document.getElementById('startbutton3');

    navigator.getMedia = ( navigator.getUserMedia ||
                           navigator.webkitGetUserMedia ||
                           navigator.mozGetUserMedia ||
                           navigator.msGetUserMedia);

    navigator.getMedia(
      {
        video: true,
        audio: false
      },
      function(stream) {
        if (navigator.mozGetUserMedia) {
          video.mozSrcObject = stream;
        } else {
          var vendorURL = window.URL || window.webkitURL;
          video.src = vendorURL.createObjectURL(stream);
        }
        video.play();
      },
      function(err) {
        console.log("An error occured! " + err);
      }
    );

    video.addEventListener('canplay', function(ev){
      if (!streaming) {
        height = video.videoHeight / (video.videoWidth/width);
		if (isNaN(height)) {
          height = width / (4/3);
        }
      
        video.setAttribute('width', width);
        video.setAttribute('height', height);
        canvas3.setAttribute('width', width);
        canvas3.setAttribute('height', height);
        streaming = true;
      }
    }, false);

    startbutton3.addEventListener('click', function(ev){
      takepicture();
      ev.preventDefault();
    }, false);
    
    clearphoto();
  }

  function clearphoto() {
    var context = canvas3.getContext('2d');
    context.fillStyle = "#AAA";
    context.fillRect(0, 0, canvas3.width, canvas3.height);
	var data = canvas3.toDataURL('image/png');
	//photo.setAttribute('src', data);
  }
  
  function takepicture() {
    var context = canvas3.getContext('2d');
    if (width && height) {
      canvas3.width = width;
      canvas3.height = height;
      context.drawImage(video, 0, 0, width, height);
    
      var data = canvas3.toDataURL('image/png');
	 // console.log(data);
	 // photo.setAttribute('src', data);
   $('.image3').val(data);
    } else {
      clearphoto();
    }
  }
	window.addEventListener('load', startup, false);
})();
