  var width = 100;    // We will scale the photo width to this
  var height = 100;  
  var photo = null;

  photo  = document.getElementById('photo');
  photo1 = document.getElementById('photo1');
  photo2 = document.getElementById('photo2');
    //--------------------
    // GET USER MEDIA CODE
    //--------------------
  navigator.getUserMedia = ( navigator.getUserMedia ||
                              navigator.webkitGetUserMedia ||
                              navigator.mozGetUserMedia ||
                              navigator.msGetUserMedia);

  var video;
  var webcamStream;

  function startWebcam() {
      if (navigator.getUserMedia) {
       navigator.getUserMedia (

            // constraints
            {
             video: true,
             audio: false
           },

            // successCallback
            function(localMediaStream) {
              video = document.querySelector('video');
              video.src = window.URL.createObjectURL(localMediaStream);
              webcamStream = localMediaStream;
            },

            // errorCallback
            function(err) {
             console.log("The following error occured: " + err);
           }
           );
      } else {
       console.log("getUserMedia not supported");
      }  
   }

   function stopWebcam() {
    webcamStream.stop();
  }
    //---------------------
    // TAKE A SNAPSHOT CODE
    //---------------------
    var canvas, ctx,canvas2;

    function init() {
      // Get the canvas and obtain a context for
      // drawing in it
      canvas = document.getElementById("myCanvas");
      ctx = canvas.getContext('2d');

      canvas2 = document.getElementById("myCanvas2");
      ctx2 = canvas2.getContext('2d'); 

      canvas3 = document.getElementById("myCanvas3");
      ctx3 = canvas3.getContext('2d');     
      }

      function snapshot() {
       // Draws current image from the video element into the canvas
          ctx.drawImage(video, 0,0, canvas.width, canvas.height);
          var data = canvas.toDataURL('image/png');

          $('.image1').val(data);
          
          
      }
      function snapshot2() {
       // Draws current image from the video element into the canvas

          ctx2.drawImage(video, 0,0, canvas2.width, canvas2.height);

          var data2 = canvas2.toDataURL('image/png');

          $('.image2').val(data2);

      }
      function snapshot3() {
       // Draws current image from the video element into the canvas
          ctx3.drawImage(video, 0,0, canvas3.width, canvas3.height);
          var data3 = canvas3.toDataURL('image/png');
          $('.image3').val(data3);      
      }

      function clearphoto() {

          ctx1.fillStyle = "#AAA";
          ctx1.fillRect(0, 0, canvas.width, canvas2.height);
          var data = canvas.toDataURL('image/png');
          //photo.setAttribute('src', data);
      }
      function clearphoto1() {
          
          ctx2.fillStyle = "#AAA";
          ctx2.fillRect(0, 0, canvas2.width, canvas2.height);
          var data = canvas2.toDataURL('image/png');
          //photo.setAttribute('src', data);
      }
      function clearphoto1() {
          
          ctx2.fillStyle = "#AAA";
          ctx2.fillRect(0, 0, canvas3.width, canvas3.height);
          var data = canvas3.toDataURL('image/png');
          //photo.setAttribute('src', data);
      }

      $(document).ready(function(){
        init();
      })