//================================ First Image ================================//
(function() {
  var width = 100;    // We will scale the photo width to this
  var height = 100;     // This will be computed based on the input stream
  var streaming = false;
  var video = null;
  var canvas2 = null;
  var photo = null;
  var startbutton2 = null;

  function startup() {
    video = document.getElementById('video');
    canvas2 = document.getElementById('canvas2');
    photo = document.getElementById('photo');
    startbutton2 = document.getElementById('startbutton2');

    navigator.getMedia = ( navigator.getUserMedia ||
                           navigator.webkitGetUserMedia ||
                           navigator.mozGetUserMedia ||
                           navigator.msGetUserMedia);

    navigator.getMedia(
      {
        video: true,
        audio: false
      },
      function(stream) {
        if (navigator.mozGetUserMedia) {
          video.mozSrcObject = stream;
        } else {
          var vendorURL = window.URL || window.webkitURL;
          video.src = vendorURL.createObjectURL(stream);
        }
        video.play();
      },
      function(err) {
        console.log("An error occured! " + err);
      }
    );

    video.addEventListener('canplay', function(ev){
      if (!streaming) {
        height = video.videoHeight / (video.videoWidth/width);
		if (isNaN(height)) {
          height = width / (4/3);
        }
      
        video.setAttribute('width', width);
        video.setAttribute('height', height);
        canvas2.setAttribute('width', width);
        canvas2.setAttribute('height', height);
        streaming = true;
      }
    }, false);

    startbutton2.addEventListener('click', function(ev){
      takepicture();
      ev.preventDefault();
    }, false);
    
    clearphoto();
  }

  function clearphoto() {
    var context = canvas2.getContext('2d');
    context.fillStyle = "#AAA";
    context.fillRect(0, 0, canvas2.width, canvas2.height);
	var data = canvas2.toDataURL('image/png');
	//photo.setAttribute('src', data);
  }
  
  function takepicture() {
    var context = canvas2.getContext('2d');
    if (width && height) {
      canvas2.width = width;
      canvas2.height = height;
      context.drawImage(video, 0, 0, width, height);
    
      var data = canvas2.toDataURL('image/png');
	 // console.log(data);
	  //photo.setAttribute('src', data);
    $('.image2').val(data);
    } else {
      clearphoto();
    }
  }
	window.addEventListener('load', startup, false);
})();
