$(document).ready(function(){
    $('.next').attr('disabled','disabled');
         $('.next').addClass('next_disabled');  
})
$('.defects').click(function () {
    var abc = $(this).attr('class');
    if (~abc.indexOf("activeClass")) {
        $(this).removeClass('activeClass');
    } else {
        $(this).addClass('activeClass');
    }
});

$('.a').click(function () {

    $(this).closest('div').find('.url_img').toggle();
    var classes = $(this).attr('class');
    if (~classes.indexOf("activeClass")) {
        $(this).removeClass('activeClass');
    } else {
        $(this).addClass('activeClass');
    }
});

$('.next').click(function () {
    var product_id = $('.Product').val();
    var brand_id = $('.Brand').val();
    var lastStep = $('#lastStep').val();

    var defects = [];
    var c = [];
    $('.activeClass').each(function () {
        defects.push(this.id);
    });

    $('.not.activeClass').each(function () {
           
            c.push($(this).attr('vals'));
    });

    if(c != ''){
        $('#modal-body').html('We are unable to progress with the quote due to the current condition of the device.');
        $('#modal-bootstrap-validation').modal('show');
    }else{
        if (defects != '') {
            
            $('.Defects').val(defects.toString());
            $.ajax({
                type: 'get',
                url: SITE_URL + '/hpUsers/allDefects',
                data: {'defects': defects,'product_id':product_id,'brand_id':brand_id,'lastStep':lastStep},
                dataType: 'JSON',
                success: function (obj) {
                    var SITE_URL = $('#site').val();
//==================================Selected defects=========================================//
                    var selectedDefects = '<h2 style="color:#767676;font-size:24px;font-weight:600;margin-top:-10px !important;margin-bottom:15px !important;">Device Condition</h2><div class="row-one" style="margin-bottom:0px;">';
                    var tot = obj.Defects;
                    for (i in tot) {
                        selectedDefects += '<div class="box-1 a width " id="' + tot[i].id + '" style="border:2px solid #01518e;">';
                        selectedDefects += '<div class="checked" style="float:right;">';
                        selectedDefects += '<img src="' + SITE_URL + '/checked-icon.png" class ="url_img" style="position: relative;">';
                        selectedDefects += '</div>';
                        selectedDefects += '<div class="Processors">' + tot[i].name + '</div>';
                        selectedDefects += '<div class="brand-title" style="margin-bottom:3px;">';
                        if (tot[i].description != '') {
                            selectedDefects += '<i class="fa fa-question-circle" name="' + tot[i].id + '"></i>'
                        }
                        selectedDefects += '</div></div>';
                    }
                    selectedDefects += '</div>';
                    $('.defects').html(selectedDefects);
//================================End Defects Listing ========================================//

//==================================Select Sirvices List =====================================//
                    if(obj.Services.name == 'Services'){
                        var ser = obj.Services.item;

                        var Services = '';
                        for (j in ser) {
                            Services += '<div class="box-1 services" onclick="abc(' + ser[j].option_id + ')" id="' + ser[j].option_id + '"><div class="Processors" >' + ser[j].name + '</div></div>';
                        }
                        $('.main').text('Next: Select your Services');
                        $('.sub').text('Please select appropriate services');
                        $('.list').html(Services);
                        $('.disable_none').remove();
                        $('.Permit').text('Services');
                        $('.Specify').remove();
                        $('.Permit_Not').remove();
                        $('.non-permit').remove();
                        $('#model_next').css('display', 'none');
                    }
                    
    //===================================End Select Sirvices List =================================//
                    window.scrollTo(0, 0);
                },
                error: function (error) {
                    alert('error block');
                }
            });
       } else {
          $('#modal-body').html('Please select atleast one defects !!!');
          $('#modal-bootstrap-validation').modal('show');
        } 
    }
});

function abc(a) {
    $('#loadingDiv').css('display', 'block');
    var token = $("input[name=_token]").val()
    var SITE = $('#siteurl').val();

    var pro1 = $('.Product').val();
    if (pro1 == 'undefined') {
        pro1 == 0;
    }

    var pro2 = $('.Brand').val();
    if (pro2 == 'undefined') {
        pro2 == 0;
    }
    var pro3 = $('.Processor').val();
    if (pro3 == 'undefined') {
        pro3 == 0;
    }
    var pro4 = $('.Screen').val();
    if (pro4 == 'undefined') {
        pro4 == 0;
    }
    var pro5 = $('.DVD').val();
    if (pro5 == 'undefined') {
        pro5 == 0;
    }
    var pro6 = $('.HDD').val();
    if (pro6 == 'undefined') {
        pro6 == 0;
    }
    var pro7 = $('.RAM').val();
    if (pro7 == 'undefined') {
        pro7 == 0;
    }
    var pro8 = $('.Defects').val();
    if (pro8 == 'undefined') {
        pro8 == 0;
    }
    var pro9 = a;

    $.ajax({
        type: 'post',
        url: SITE + '/hpUser/totalSteps',
        data: {
            '_token': token,
            'pro1': pro1,
            'pro2': pro2,
            'pro3': pro3,
            'pro4': pro4,
            'pro5': pro5,
            'pro6': pro6,
            'pro7': pro7,
            'pro8': pro8,
            'pro9': pro9,
            'lastStep': 10,
            'model_search': 'model_search'
        },
        dataType: 'JSON',
        success: function (obj) {
            if (obj.emptyData == 'empty') {
                $('#loadingDiv').css('display', 'none');
                $('#modal-bootstrap-tour').modal('show');
            } else {
                $('.mainDiv').html(obj.html);
                $('#loadingDiv').css('display','none');
                window.scrollTo(0, 0);
            }
        },
        error: function (error) {
            alert('error block');
        }
    });
}
$('.box-1.a').click(function(){
         var b = [];
          $('.activeClass').each(function () {
            b.push(this.id);
        });
      if (b != ''){

        $(this).closest('div').find('.waringblock').css('display','block').addClass('Processors');
       $(this).closest('div').find('.waringblock2').css('display','block').addClass('warninig-title');
       $(this).closest('div').find('.warningmargin').toggleClass('Processors');  
        $('.next').removeAttr('disabled','disabled');
         $('.next').removeClass('next_disabled');
         $('.next').addClass('continue');
       $('.nextarrow').css('color','#0096d6');
        }else{
        $(this).closest('.not').find('.waringblock').css('display','none');
        $(this).closest('div').find('.waringblock2').css('display','none');
        $(this).closest('div').find('.warningmargin').addClass('Processors');
         $('.next').attr('disabled','disabled');
         $('.next').addClass('next_disabled');
         $('.next').removeClass('continue'); 
       $('.nextarrow').css('color','lightgray');  
        }
});