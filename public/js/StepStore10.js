
    $(function () {
        var clientID = $('#clientID').val();
        var quote_id = $('#quote_id').val();
        var quote_item_id = $('#quote_item_id').val();
        var inprogress_url = $('.inprogress').val();
        var str = $("#formfinalstep").serialize() + '&client_id=' + clientID + '&quote_id=' + quote_id + '&quote_item_id=' + quote_item_id;
        var SITE = $('#siteurl').val();

        console.log(SITE + '/hpUser/quoteTotal?' + str);

        $.ajax({
            type: 'post',
            url: SITE + '/hpUser/quoteTotal',
            data: str,
            dataType: 'JSON',
            success: function (obj) {
                if(obj.status == 'ProductIdIssue'){
                    $('.productIdTrue').hide();
                    $('#ProductIdNotFound').modal('show');
                }else{
                    if (obj.status == 'success') {
                        $('#getTotal').html(obj.total_price);

                        $('.ProductTTGtoHP').html(obj.unitp);
                        $('.ServicesTTGtoHP').html(obj.services);
                        $('.LogisticsTTGtoHP').html(obj.logistic);
                        $('.TotalTTGtoHP').html(obj.total_price);

                        $('.ProductHPtoCustomer').html(obj.Product_Price_customer);
                        $('.ServicesHPtoCustomer').html(obj.Service_Price_customer);
                        $('.LogisticsHPtoCustomer').html(obj.Logistic_Price_customer);
                        $('.TotalHPtoCustomer').html(obj.Sub_total);

                        $('.productMarkup').html(obj.productMarkup);
                        $('.serviceMarkup').html(obj.seviceMarkup);
                        $('.logisticMarkup').html(obj.logisticMarkup);
                        $('.totalMarkup').html(obj.totalMarkup);

                        $('.ProductMargin').html(obj.product_percent);
                        $('.ServicesMargin').html(obj.service_percent);
                        $('.LogisticsMargin').html(obj.logistic_percent);
                        $('.TotalMargin').html(obj.total_percent);
                        if(obj.flag == true){
                            $('.Financial').show();
                        }else{
                            $('.Financial').hide();
                        }

                    }

                    $('.Reference').val(obj.quote_name);
                    $('.Reference1').val(obj.quote_name);
                    $('#Quote_Item_Id').val(obj.quote_item_id);
                    $('#Quote_Id').val(obj.quote_id);
                    $('#Quote_id').val(obj.quote_id);
                    $('#client_id').val(obj.client_id);
                    $('#status').val(obj.statuss);
                }
            },
            error: function (error) {
                alert('error block');
            }
        });

        $('#edit_quote').click(function () {
            $('#Quote_Edit_Form').submit();
        });

        $('.update_name').click(function(){
            var abc =  $('.Reference').val();
            $('.Reference1').val(abc);
            $('#Update_Name_Form').submit();
        });
    });
    $(document).ready(function(){
         $('[data-toggle="tooltip"]').tooltip(); 
     });
