
/*========================= users alter 16-09-17*/

ALTER TABLE  `users` ADD  `created_at` TIMESTAMP NOT NULL AFTER  `phone` ,
ADD  `updated_at` TIMESTAMP NOT NULL AFTER  `created_at` ;
ALTER TABLE  `users` CHANGE  `ip_address`  `ip_address` VARCHAR( 45 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ;
ALTER TABLE `users` CHANGE `created_on` `created_on` INT(11) UNSIGNED NULL DEFAULT NULL;


 /*============================alter by prerna 20/09/2017 ===========================*/
ALTER TABLE  `client_users` ADD  `created_at` TIMESTAMP NOT NULL AFTER  `user_id` ,
ADD  `updated_at` TIMESTAMP NOT NULL AFTER  `created_at` ;

/*=====================================================================================*/


 /*============================== alter by raj 20/09/17==================*/
ALTER TABLE `country` ADD `created_at` TIMESTAMP NOT NULL AFTER `status` ,
ADD `updated_at` TIMESTAMP NOT NULL AFTER `created_at` ;
/*=========================================================================================*/

/*============================alter by prerna 25/09/2017 ===========================*/
ALTER TABLE  `product_option_groups` ADD  `created_at` TIMESTAMP NOT NULL AFTER  `status` ,
ADD  `updated_at` TIMESTAMP NOT NULL AFTER  `created_at` ;



ALTER TABLE  `product_options` ADD  `created_at` TIMESTAMP NOT NULL AFTER  `image` ,
ADD  `updated_at` TIMESTAMP NOT NULL AFTER  `created_at` ;
/*=====================================================================================*/

ALTER TABLE  `clients` ADD  `created_at` TIMESTAMP NOT NULL AFTER  `date_created` ,
ADD  `updated_at` TIMESTAMP NOT NULL AFTER  `created_at` ;



/*======================================by raj===============================================*/
ALTER TABLE  `country` ADD  `isActive` VARCHAR( 255 ) NOT NULL AFTER  `status` ;

ALTER TABLE `country` CHANGE `status` `status` INT( 10 ) NULL DEFAULT NULL ;/*27Sept,2017 For Soft Delete*/
ALTER TABLE `country_region` CHANGE `isActive` `isActive` INT( 10 ) NOT NULL ;/*27Sept,2017 For Soft Delete*/
ALTER TABLE  `product_options` ADD  `isActive` INT( 10 ) NOT NULL AFTER  `image` ;
ALTER TABLE `product_options` CHANGE `isActive` `isActive` INT( 10 ) NOT NULL ;


/*======================================by prerna===============================================*/

ALTER TABLE  `product_option_groups` CHANGE  `id`  `id` INT( 11 ) NOT NULL AUTO_INCREMENT ;

/*======================================by prerna 04/10/17===============================================*/

ALTER TABLE  `quote_status` ADD  `created_at` TIMESTAMP NOT NULL AFTER  `source` ,
ADD  `updated_at` TIMESTAMP NOT NULL AFTER  `created_at` ;

ALTER TABLE  `price_bands` ADD  `created_at` TIMESTAMP NOT NULL AFTER  `status` ,
ADD  `updated_at` TIMESTAMP NOT NULL AFTER  `created_at` ;
ALTER TABLE  `price_bands` ADD PRIMARY KEY (  `id` ) ;
ALTER TABLE  `price_bands` CHANGE  `id`  `id` INT( 11 ) NOT NULL AUTO_INCREMENT ;

/*======================================by prerna 06/10/17===============================================*/

ALTER TABLE  `price_margin_oem` ADD  `created_at` TIMESTAMP NOT NULL AFTER  `status` ,
ADD  `updated_at` TIMESTAMP NOT NULL AFTER  `created_at` ;

ALTER TABLE  `price_margin_oem` ADD PRIMARY KEY (  `id` ) ;

ALTER TABLE  `price_margin_oem` CHANGE  `id`  `id` INT( 11 ) NOT NULL AUTO_INCREMENT ;


ALTER TABLE  `price_margin_services` ADD  `created_at` TIMESTAMP NOT NULL AFTER  `status` ,
ADD  `updated_at` TIMESTAMP NOT NULL AFTER  `created_at` ;
ALTER TABLE  `price_margin_services` ADD PRIMARY KEY (  `id` );
ALTER TABLE  `price_margin_services` CHANGE  `id`  `id` INT( 11 ) NOT NULL AUTO_INCREMENT ;

/*======================================by prerna 06/10/17===============================================*/


/*======================================by Raj 07/10/17===============================================*/

ALTER TABLE  `price_margin_adjustments` ADD  `created_at` TIMESTAMP NULL DEFAULT NULL AFTER  `status` ,
ADD  `updated_at` TIMESTAMP NULL DEFAULT NULL AFTER  `created_at` ;

ALTER TABLE  `price_margin_adjustments` ADD PRIMARY KEY (  `id` ) ;
ALTER TABLE  `price_margin_adjustments` CHANGE  `id`  `id` INT( 11 ) NOT NULL AUTO_INCREMENT ;
/*======================================End Raj 06/10/17===============================================*/

/*======================================by Raj 09/10/17===============================================*/
ALTER TABLE  `price_factors` ADD  `created_at` TIMESTAMP NULL DEFAULT NULL AFTER  `value` ,
ADD  `updated_at` TIMESTAMP NULL DEFAULT NULL AFTER  `created_at` ;
ALTER TABLE  `product_factors` ADD  `created_at` TIMESTAMP NULL DEFAULT NULL AFTER  `factor_type` ,
ADD  `updated_at` TIMESTAMP NULL DEFAULT NULL AFTER  `created_at` ;
/*======================================End Raj 09/10/17===============================================*/
=======
/*======================================by Raj 06/10/17===============================================*/

/*======================================by prerna 09/10/17===============================================*/
ALTER TABLE  `price_factors_bands` ADD PRIMARY KEY (  `id` ) ;
ALTER TABLE  `price_factors_bands` CHANGE  `id`  `id` INT( 11 ) NOT NULL AUTO_INCREMENT ;
ALTER TABLE `price_factors_bands` ADD `created_at` TIMESTAMP NOT NULL AFTER `value` , ADD `updated_at` TIMESTAMP NOT NULL AFTER `created_at`
/*======================================by prerna 09/10/17===============================================*/


/*======================================by Raj 11/10/17===============================================*/
ALTER TABLE  `client_users` ADD  `created_at` TIMESTAMP NULL DEFAULT NULL AFTER  `user_id` ,
ADD  `updated_at` TIMESTAMP NULL DEFAULT NULL AFTER  `created_at` ;
ALTER TABLE  `price_factors_bands` ADD  `created_at` TIMESTAMP NULL DEFAULT NULL AFTER  `value` ,
ADD  `updated_at` TIMESTAMP NULL DEFAULT NULL AFTER  `created_at` ;
/*======================================by Raj 11/10/17===============================================*/


/*======================================by Raj 11/10/17===============================================*/
ALTER TABLE  `clients` CHANGE  `updated_at`  `updated_at` TIMESTAMP NULL DEFAULT NULL ;
/*======================================by Raj 11/10/17===============================================*/

/*======================================by Raj 11/10/17===============================================*/

ALTER TABLE  `settings` CHANGE  `id`  `id` INT( 11 ) NOT NULL AUTO_INCREMENT ;


ALTER TABLE `price_margin_adjustments` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `logistics_unit_numbers` ADD `created_at` TIMESTAMP NOT NULL AFTER `value_to`, ADD `updated_at` TIMESTAMP NOT NULL AFTER `created_at`;
ALTER TABLE  `logistics_unit_numbers` ADD PRIMARY KEY (  `id` ) ;
ALTER TABLE  `logistics_unit_numbers` CHANGE  `id`  `id` INT( 11 ) NOT NULL AUTO_INCREMENT ;

/*=================================13-10-2017===================================*/
ALTER TABLE  `price_logistics` ADD  `created_at` TIMESTAMP NULL DEFAULT NULL AFTER  `value` ,
ADD  `updated_at` TIMESTAMP NULL DEFAULT NULL AFTER  `created_at` ;
/*=================================13-10-2017===================================*/



/*======================================by Raj 27/10/17===============================================*/
ALTER TABLE  `quote_collection` CHANGE  `date_created`  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ;
ALTER TABLE  `quote_collection` ADD  `updated_at` TIMESTAMP NULL DEFAULT NULL AFTER  `email_sent` ;
/*======================================by Raj 27/10/17===============================================*/

/*======================================by Raj 02/11/17===============================================*/
ALTER TABLE  `users` ADD  `image` VARCHAR( 255 ) NULL DEFAULT NULL AFTER  `phone` ;
/*======================================by Raj 27/10/17===============================================*/


/*======================================by Raj 05/11/17===============================================*/
ALTER TABLE  `quote_step_defaults` ADD  `created_at` TIMESTAMP NULL DEFAULT NULL AFTER  `option_id` ,
ADD  `updated_at` TIMESTAMP NULL DEFAULT NULL AFTER  `created_at` ;
/*======================================by Raj 05/11/17===============================================*/

/*======================================by Raj 12/11/17===============================================*/
ALTER TABLE `quote_notes` ADD PRIMARY KEY(`id`);
ALTER TABLE `quote_notes` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `quote_notes` ADD `created_at` TIMESTAMP NULL DEFAULT NULL AFTER `note`, ADD `updated_at` TIMESTAMP NULL DEFAULT NULL AFTER `created_at`;
ALTER TABLE `quote_collection` ADD `created_at` TIMESTAMP NULL DEFAULT NULL AFTER `email_sent`, ADD `updated_at` TIMESTAMP NULL DEFAULT NULL AFTER `created_at`;
/*======================================by Raj 12/11/17===============================================*/

/*======================================by Raj 13/11/17===============================================*/
ALTER TABLE  `product_option_groups` ADD  `type` VARCHAR( 255 ) NOT NULL AFTER  `name` ;
/*======================================by Raj 13/11/17===============================================*/

/*======================================by Raj 04/12/17===============================================*/
ALTER TABLE  `product` ADD  `clone_id` INT( 11 ) NOT NULL AFTER  `status` ;
/*======================================by Raj 04/12/17===============================================*/

/*======================================by Raj 06/12/17===============================================*/
ALTER TABLE  `navigations` ADD  `page_content` TEXT NULL DEFAULT NULL AFTER  `status` ;
/*======================================by Raj 07/12/17===============================================*/

UPDATE `clients` SET `parent_id` = '', `ave_collect_size` = '', `margin_oem_product` = '', `margin_oem_logistic` = '', `margin_oem_service` = '', `created_at` = NULL WHERE `clients`.`id` = 0;


/*======================================by Raj 09/01/18===============================================*/
ALTER TABLE  `users` ADD  `created_by` INT( 11 ) NOT NULL AFTER  `phone` ;
/*======================================by Raj 09/01/18===============================================*/

ALTER TABLE `quote_items` ADD `is_model_price` INT(11) NULL DEFAULT NULL AFTER `exchange_rate`, ADD `unit_price_defectcap` INT(11) NULL DEFAULT NULL AFTER `is_model_price`, ADD `unit_prod_price` INT(11) NULL DEFAULT NULL AFTER `unit_price_defectcap`, ADD `logistics_margin` INT(11) NULL DEFAULT NULL AFTER `unit_prod_price`, ADD `logistics_product_margin` INT(11) NULL DEFAULT NULL AFTER `logistics_margin`, ADD `logistics_oem_margin` INT(11) NULL DEFAULT NULL AFTER `logistics_product_margin`, ADD `logistics_net_price` INT(11) NULL DEFAULT NULL AFTER `logistics_oem_margin`, ADD `services_margin` INT(11) NULL DEFAULT NULL AFTER `logistics_net_price`, ADD `services_oem_margin` INT(11) NULL DEFAULT NULL AFTER `services_margin`, ADD `services_net_price` INT(11) NULL DEFAULT NULL AFTER `services_oem_margin`;

/*======================================by Raj 31/01/18===============================================*/
ALTER TABLE  `product_option_groups` ADD  `allow` VARCHAR( 255 ) NULL DEFAULT NULL AFTER  `status` ;


/*======================================by Nadeem 26/03/18===============================================*/

ALTER TABLE `product_option_groups` ADD `processor_sub_group` VARCHAR(255) NULL DEFAULT NULL AFTER `group_code`;

/*======================================by Raj 31/01/18===============================================*/
ALTER TABLE  `users` CHANGE  `theme`  `theme_id` INT( 11 ) NOT NULL ;

CREATE TABLE IF NOT EXISTS `themes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `theme_name` varchar(255) NOT NULL,
  `head_color` varchar(255) NOT NULL,
  `head_back_color` varchar(255) NOT NULL,
  `head_active_color` varchar(255) DEFAULT NULL,
  `head_hover_color` varchar(255) DEFAULT NULL,
  `foot_color` varchar(255) NOT NULL,
  `foot_back_color` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

/*=====================Missin Alter Sql======================================*/
CREATE TABLE `navigations` (
  `id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `position` int(10) NOT NULL,
  `status` varchar(255) NOT NULL,
  `section` varchar(255) NOT NULL,
  `page_content` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `navigations`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `navigations`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;COMMIT; <td style="text-align: right;">
         
                  <a href="javascript:void(0);" onclick="view_leave(1)"><i class="icon-view"></i></a>
          		        </td>
ALTER TABLE `users` ADD `remember_token` VARCHAR(255) NULL AFTER `password`;
ALTER TABLE `quote_items` ADD `use_committed_price` BOOLEAN NULL DEFAULT TRUE AFTER `image3`, ADD `logistics_base_price` INT NOT NULL DEFAULT '0' AFTER `use_committed_price`, ADD `services_base_price` INT NOT NULL DEFAULT '0' AFTER `logistics_base_price`, ADD `exchange_rate` INT NOT NULL DEFAULT '0' AFTER `services_base_price`;

ALTER TABLE `quote_items` ADD `date_modified` TIMESTAMP NOT NULL AFTER `date_created`;
ALTER TABLE `country` CHANGE `status` `status` VARCHAR(50) NULL DEFAULT NULL;
ALTER TABLE `country_region` ADD `isActive` INT(10) NOT NULL AFTER `status`, ADD `created_at` TIMESTAMP NOT NULL AFTER `isActive`, ADD `updated_at` TIMESTAMP NOT NULL AFTER `created_at`;
ALTER TABLE `product` ADD `created_at` TIMESTAMP NOT NULL AFTER `clone_id`, ADD `updated_at` TIMESTAMP NOT NULL AFTER `created_at`;
ALTER TABLE `login_success` ADD `created_at` TIMESTAMP NOT NULL AFTER `browser`, ADD `updated_at` TIMESTAMP NOT NULL AFTER `created_at`;
ALTER TABLE `login_attempts` ADD `created_at` TIMESTAMP NOT NULL AFTER `time`, ADD `updated_at` TIMESTAMP NOT NULL AFTER `created_at`;
ALTER TABLE `clients` ADD `level` int(11) DEFAULT NULL after name
UPDATE `quotingtools_new`.`clients` SET `id` = '4' WHERE `clients`.`id` = 0;
ALTER TABLE `clients` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
UPDATE `quotingtools_new`.`clients` SET `id` = '0' WHERE `clients`.`id` = 4;

ALTER TABLE `quote` ADD `name` varchar(255) DEFAULT NULL after id
ALTER TABLE `quote_collection_items` ADD `created_at` TIMESTAMP NOT NULL AFTER `status`, ADD `updated_at` TIMESTAMP NOT NULL AFTER `created_at`;
ALTER TABLE `quote` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `quote_status` ADD `created_at` TIMESTAMP NOT NULL AFTER `source`, ADD `updated_at` TIMESTAMP NOT NULL AFTER `created_at`;
ALTER TABLE `settings` ADD `created_at` TIMESTAMP NOT NULL AFTER `value`, ADD `updated_at` TIMESTAMP NOT NULL AFTER `created_at`;
/*=====================Missin Alter Sql======================================*/

ALTER TABLE `quote_items` ADD `totalmargin` DECIMAL(12,2) NULL DEFAULT NULL AFTER `services_net_price`;

CREATE TABLE IF NOT EXISTS `product_clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;



CREATE TABLE IF NOT EXISTS `client_country_rates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `expires` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `inherited` bit(1) NOT NULL,
  `client_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `exchangerate` decimal(12,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;



ALTER TABLE  `price_factors_bands` ADD  `processed` INT( 11 ) NOT NULL DEFAULT  '0' AFTER  `value` ;

/*
-------------------------------
Andy May 2018 START
*/
ALTER TABLE quote_items MODIFY COLUMN exchange_rate decimal(12,2) NOT NULL DEFAULT 0;
ALTER TABLE quote_items MODIFY COLUMN logistics_base_price decimal(12,2) NOT NULL DEFAULT 0;
ALTER TABLE quote_items MODIFY COLUMN services_base_price decimal(12,2) NOT NULL DEFAULT 0;
ALTER TABLE quote_items MODIFY COLUMN unit_price_defectcap decimal(12,2) NOT NULL DEFAULT 0;
ALTER TABLE quote_items MODIFY COLUMN unit_prod_price decimal(12,2) NOT NULL DEFAULT 0;
ALTER TABLE quote_items MODIFY COLUMN logistics_margin decimal(4,2) NOT NULL DEFAULT 0;
ALTER TABLE quote_items MODIFY COLUMN logistics_oem_margin decimal(4,2) NOT NULL DEFAULT 0;
ALTER TABLE quote_items MODIFY COLUMN logistics_net_price decimal(12,2) NOT NULL DEFAULT 0;
ALTER TABLE quote_items MODIFY COLUMN services_margin decimal(4,2) NOT NULL DEFAULT 0;
ALTER TABLE quote_items MODIFY COLUMN services_oem_margin decimal(4,2) NOT NULL DEFAULT 0;
ALTER TABLE quote_items MODIFY COLUMN services_net_price decimal(12,2) NOT NULL DEFAULT 0;

ALTER TABLE `quote_items` ADD `unit_f5_price` decimal(12,2) NOT NULL DEFAULT 0 AFTER `totalmargin` ;
ALTER TABLE quote_items ADD COLUMN unit_f5_logistics decimal(12,2) NOT NULL DEFAULT 0 AFTER `unit_f5_price` ;
ALTER TABLE quote_items ADD COLUMN unit_f5_services decimal(12,2) NOT NULL DEFAULT 0 AFTER `unit_f5_logistics` ;
ALTER TABLE `quote_items` ADD `step_id` int NOT NULL DEFAULT 0 AFTER `totalmargin` ;

CREATE TABLE IF NOT EXISTS `product_options_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

ALTER TABLE `product_options_link` ADD `order` int NOT NULL DEFAULT 0 AFTER `client_id` ;

UPDATe `price_margin_services` SET `updated_at` = now();
ALTER TABLE `price_margin_services`  MODIFY COLUMN `updated_at` timestamp NULL DEFAULT NOW();
ALTER TABLE `price_margin_services` ADD `country_id` int NOT NULL DEFAULT 0 AFTER `option_id`;

INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'HP' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'HP' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Dell' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Dell' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Lenovo' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Lenovo' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Apple' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Apple' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Microsoft' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Microsoft' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Acer' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Acer' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Asus' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Asus' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Toshiba' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Toshiba' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Fujitsu' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Fujitsu' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Other' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Other' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'HP' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'HP' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Dell' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Dell' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Lenovo' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Lenovo' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Apple' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Apple' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Acer' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Acer' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Asus' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Asus' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Toshiba' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Toshiba' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Fujitsu' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Fujitsu' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Other' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Other' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'HP' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'HP' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Dell' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Dell' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Lenovo' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Lenovo' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Other' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Other' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'HP' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'HP' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Dell' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Dell' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Lenovo' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Lenovo' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Apple' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Apple' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Acer' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Acer' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Asus' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Asus' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Toshiba' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Toshiba' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Fujitsu' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Fujitsu' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Other' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Other' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'HP' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'HP' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Dell' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Dell' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Lenovo' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Lenovo' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Acer' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Acer' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Asus' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Asus' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Other' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Other' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'HP' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'HP' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Dell' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Dell' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Lenovo' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Lenovo' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Apple' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Apple' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Acer' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Acer' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Asus' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Asus' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Toshiba' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Toshiba' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Fujitsu' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Fujitsu' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Samsung' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Samsung' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Other' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Other' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Amazon' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Amazon' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Huawei' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Huawei' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'HP' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'HP' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Canon' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Canon' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Xerox' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Xerox' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Lexmark' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Lexmark' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Ricoh' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Ricoh' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Oki' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Oki' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Konica Minolta' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Konica Minolta' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Kyocera' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Kyocera' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Sharp' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Sharp' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Toshiba' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Toshiba' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'HP' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'HP' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Dell-Wyse' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Dell-Wyse' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Lenovo' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Lenovo' AND `type` = 'Brand');
INSERT INTO `product_options` (`type`, `name`, `order`, `status`, `description`, `image`, `isactive`, `created_at`, `updated_at`) SELECT * FROM (SELECT 'Brand' as `type`, 'Other' as `name`, 20 as `order`, 'active' as `status`, '' as `description`, '' AS `image`, 1 as `isactive`, now() as `created_at`, now() as `updated_at` )tmp WHERE NOT EXISTS (   SELECT `name` FROM `product_options` WHERE `name` = 'Other' AND `type` = 'Brand');

TRUNCATE TABLE `product_options_link`;

INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'HP' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Notebook' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Dell' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Notebook' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Lenovo' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Notebook' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Apple' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Notebook' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Microsoft' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Notebook' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Acer' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Notebook' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Asus' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Notebook' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Toshiba' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Notebook' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Fujitsu' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Notebook' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Other' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Notebook' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'HP' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Desktop' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Dell' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Desktop' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Lenovo' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Desktop' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Apple' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Desktop' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Acer' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Desktop' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Asus' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Desktop' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Toshiba' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Desktop' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Fujitsu' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Desktop' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Other' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Desktop' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'HP' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Workstation' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Dell' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Workstation' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Lenovo' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Workstation' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Other' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Workstation' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'HP' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'All-in-One' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Dell' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'All-in-One' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Lenovo' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'All-in-One' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Apple' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'All-in-One' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Acer' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'All-in-One' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Asus' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'All-in-One' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Toshiba' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'All-in-One' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Fujitsu' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'All-in-One' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Other' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'All-in-One' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'HP' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'LCD' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Dell' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'LCD' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Lenovo' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'LCD' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Acer' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'LCD' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Asus' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'LCD' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Other' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'LCD' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'HP' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Tablet' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Dell' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Tablet' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Lenovo' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Tablet' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Apple' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Tablet' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Acer' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Tablet' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Asus' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Tablet' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Toshiba' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Tablet' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Fujitsu' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Tablet' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Samsung' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Tablet' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Other' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Tablet' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Amazon' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Tablet' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Huawei' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Tablet' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'HP' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Printer' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Canon' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Printer' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Xerox' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Printer' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Lexmark' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Printer' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Ricoh' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Printer' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Oki' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Printer' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Konica Minolta' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Printer' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Kyocera' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Printer' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Sharp' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Printer' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Toshiba' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Printer' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'HP' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Thin Client' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Dell-Wyse' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Thin Client' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Lenovo' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Thin Client' ) AS pid,  0, now() , now() ;
INSERT INTO `product_options_link` (`brand_id`, `type_id`, `client_id`, `created_at`, `updated_at`) SELECT  (SELECT id FROM `product_options` WHERE `type` = 'Brand' AND `name` = 'Other' ) AS bid, (SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = 'Thin Client' ) AS pid,  0, now() , now() ;

ALTER TABLE `quote_items_defects` ADD COLUMN option_id int NOT NULL DEFAULT 0;
ALTER TABLE `client_country_rates` MODIFY COLUMN exchangerate DECIMAL(12,4) NULL ;
ALTER TABLE `client_country_rates` MODIFY COLUMN inherited BIT NULL;
ALTER TABLE `client_country_rates` MODIFY COLUMN expires DATETIME NULL;
UPDATE `quote_step_items` SET group_code = REPLACE(group_code, 'Screensize' , 'Screen');
UPDATE `quote_step_items` SET group_code = REPLACE(group_code, 'ScreenSize' , 'Screen');
UPDATE `product_option_groups` SET group_code = REPLACE(group_code, 'Screensize' , 'Screen');
UPDATE `product_option_groups` SET group_code = REPLACE(group_code, 'ScreenSize' , 'Screen');
DELETE FROM `product_options` WHERE isActive = 0 ;
DELETE FROM `product_option_groups` WHERE option_id NOT IN (SELECT id FROM `product_options`);
UPDATE `product_option_groups` p JOIN `product_options` o ON p.option_id = o.id SET p.name = o.name;
UPDATE `product` SET pricing_type = 'Lcd' where type_id = 178;
DELETE FROM `product_factors` where processor_id NOT in (select id from product_options);

UPDATE `product` SET pricing_type = 'Processor' WHERE `model` IS NULL AND processor_id > 0;
UPDATE `product` SET pricing_type = 'Model' WHERE `model` IS NOT NULL AND `model` != '' AND processor_id > 0;
UPDATE `product` SET pricing_type = 'Lcd' WHERE `model` IS NULL AND processor_id IS NULL AND screen_id > 0;

ALTER TABLE `product`  MODIFY COLUMN `updated_at` timestamp NULL DEFAULT NOW();
UPDATE `product` SET `updated_at` = now();
ALTER TABLE `product` ADD COLUMN `country_id` INT NOT NULL DEFAULT 0 ;


DROP TABLE IF EXISTS `themes`;

CREATE TABLE `themes` (
  `id` int(11) NOT NULL,
  `theme_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_template` varchar(255) NOT NULL,
  `theme_folder` varchar(255) DEFAULT NULL,
  `logo` varchar(255) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB ;


INSERT INTO `themes` (`id`, `theme_name`, `email`, `email_template`, `theme_folder`, `logo`, `updated_at`, `created_at`) VALUES
(1, 'F5', 'info@f5-it.com', '', '', '', '2018-06-03 00:10:03', '2018-06-03 00:10:03'),
(5, 'HP Device Refresh', 'hp@f5-it.com', '', 'theme/hp-asia', '1522305530.png', '2018-06-03 00:11:25', '2018-06-03 00:11:25');

ALTER TABLE `themes`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `themes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

/*
-------------------------------
Andy May 2018 END
*/