<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoginSuccess extends Model
{
   protected $table = "login_success";
   protected $fillable = ['user_id', 'date_loggedin','ip_address','browser'];
}
