<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuoteManagement extends Model
{

    protected $table = 'quote';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

        protected $fillable = [
            'id',
            'client_id',
            'user_id',
            'date_created',
            'date_presented',
            'date_accepted',
            'date_validto',
            'date_modified',
            'status',
            'country',
            'region',
            'ave_collect_size',
            'currency_symbol',
            'currency_rate',
            'margin_oem_product',
            'margin_oem_logistic',
            'margin_oem_service',
            'total_value',
            'total_items',
        ];


    public function Clientname(){
        return $this->hasOne('App\Client','id','client_id');
    }
    public function Status(){
        return $this->hasOne('App\QuoteStatus','id','status');
    }
    public function Username(){
        return $this->hasOne('App\User','id','user_id');
    }
    public function QuoteItem(){
        return $this->hasOne('App\QuoteItem','quote_id','id');
    }
     public function QuoteItems(){
        return $this->hasMany('App\QuoteItem','quote_id','id');
    }
    public function Country(){
        return $this->hasOne('App\Country','id','country');
    }
}

