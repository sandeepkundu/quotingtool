<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

//use Zizaco\Entrust\Traits\EntrustUserTrait;


class QuoteStep extends Authenticatable {

    protected $table = 'quote_step';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'client_id',
        'type_id',
        'brand_id',
        'name'
    ];
    public function Client(){
        return $this->hasOne('App\Client', 'id', 'client_id');
    }
    public function Type(){
        return $this->hasOne('App\ProductOption', 'id', 'type_id');
    }
     public function Brand(){
        return $this->hasOne('App\ProductOption', 'id', 'brand_id');
    }
}
