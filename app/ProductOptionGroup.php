<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductOptionGroup extends Model
{
     public $table = "product_option_groups";
     
    protected $fillable = [
        'client_id',
        'option_id',
        'group',
        'group_code',
        'name',
        'order',
        'status'
    ];
}
