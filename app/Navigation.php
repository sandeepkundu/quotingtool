<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Navigation extends Model
{
    protected $table='navigations';
    protected $fillable = [
        'name',
        'display_name',
        'position',
        'status',
        'section',
        'page_content',
        'created_at',
        'updated_at'
    ];

}
