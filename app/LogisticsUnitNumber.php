<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogisticsUnitNumber extends Model
{

    protected $table = 'logistics_unit_numbers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

        protected $fillable = [
        'id',
        'client_id',
        'value_from',
        'value_to'
        
    ];


    public function Clientname(){
        return $this->hasOne('App\Client','id','client_id');
    }
}
