<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{
    protected $table='password_resets';
    protected $fillable = [
        'id',
        'email',
        'token',
        'created_at',
        'updated_at'
    ];
}
