<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;


class User extends Authenticatable

{
    use Notifiable;
     use EntrustUserTrait; 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ip_address', 'username', 'password','theme_id', 'salt', 'email', 'activation_code', 'forgotten_password_code', 'forgotten_password_time', 'remember_code', 'created_on', 'last_login', 'active', 'first_name', 'last_name', 'company', 'phone','created_by','remember_token','image','permission','last_url'
            ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

     public function clientuser(){
        return $this->hasMany('App\ClientUser');
    }

    public function UserRole(){
        return $this->hasOne('App\RoleUser','user_id','id');
    }
    public function theme(){
        return $this->hasOne('App\Theme','id','theme_id');
    }
	public function logins(){
        return $this->hasMany('App\LoginSuccess');
    }
}
