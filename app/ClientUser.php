<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientUser extends Model
{
     protected $table = 'client_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        	'client_id', 'user_id'
    	];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function clientss(){
        return $this->belongsTo('App\Client','client_id');
    }
     public function clients(){
        return $this->hasMany('App\Client','parent_id','client_id');
    }

  }
