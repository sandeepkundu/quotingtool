<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Product extends Authenticatable {

    protected $table = 'product';
    protected $fillable = [
        'id',
        'client_id',
        'model',
        'type_id',
        'brand_id',
        'processor_id',
        'screen_id',
        'hdd_id',
        'ram_id',
        'dvd_id',
        'image',
        'value',
        'value_factor',
        'status',
    ];

    public function Brand() {
        return $this->hasOne('App\ProductOption', 'id', 'brand_id');
    }

    public function Type() {
        return $this->hasOne('App\ProductOption', 'id', 'type_id');
    }
    public function Processor() {
        return $this->hasOne('App\ProductOption', 'id', 'processor_id');
    }
    public function Screen() {
        return $this->hasOne('App\ProductOption', 'id', 'screen_id');
    }
}
