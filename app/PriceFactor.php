<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceFactor extends Model
{
      protected $table = 'price_factors';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'client_id',
        'factor_name',
        'variance_to',
        'variance_from',
        'value',
        
    ];
    public function Clientname(){
        return $this->hasOne('App\Client','id','client_id');
    }
}
