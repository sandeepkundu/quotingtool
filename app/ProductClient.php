<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductClient extends Model
{
    protected $table = 'product_clients';
    
    protected $fillable = [
        'id',
        'client_id',
        'option_id',
    ];
    public function clientss(){
        return $this->belongsTo('App\Client','client_id');
    }
}
