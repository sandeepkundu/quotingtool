<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceMarginOEM extends Model
{
     public $table = "price_margin_oem";
	protected $fillable = [
			'client_id', 
			'oem_type',
			'value',
			'status'
			
        ];
}
