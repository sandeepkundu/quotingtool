<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceLogistic extends Model
{

    protected $table = 'price_logistics';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'client_id',
        'country_id',
        'region_id',
        'logistic_unit_id',
        'value',
        
    ];

    public function Clientname(){
        return $this->hasOne('App\Client','id','client_id');
    }

    public function countryname(){
        return $this->hasOne('App\Country','id','country_id');
    }

    public function Regionname(){
        return $this->hasOne('App\CountryRegion','id','region_id');
    }

     public function logisticname(){
        return $this->hasOne('App\LogisticsUnitNumber','id','logistic_unit_id');
    }
    
}
