<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\EntrustRole;

class IntegrationImport extends Model
{
    protected $table = "integration_imports";
    protected $fillable = [
        'date',
        'fileimported',
        'fileexported',
        'records_valid',
        'records_invalid',
        'source'
    ];

}
