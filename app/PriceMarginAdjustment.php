<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceMarginAdjustment extends Model {

    protected $table = 'price_margin_adjustments';

    protected $fillable = [
        'id',
        'client_id',
        'country_id',
        'type_id',
        'brand_id',
        'screen_id',
        'margin_type',
        'value',
        'status'
     ];

    public function Clientname(){
        return $this->hasOne('App\Client','id','client_id');
    }

    public function Countryname(){
        return $this->hasOne('App\Country','id','country_id');
    }

    public function ProductOtionType(){
        return $this->hasOne('App\ProductOption','id','type_id');
    }

    public function ProductOtionBrand(){
        return $this->hasOne('App\ProductOption','id','brand_id');
    }

    public function ProductOtionScreen(){
        return $this->hasOne('App\ProductOption','id','screen_id');
    }

    
    
}
