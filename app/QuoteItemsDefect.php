<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
//use Zizaco\Entrust\Traits\EntrustUserTrait;


class QuoteItemsDefect extends Model
{
    public $timestamps = false;   
    public function productOption(){
        return $this->hasMany('App\ProductOption', 'id', 'defect_id');
     
    }
}
