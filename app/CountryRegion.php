<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountryRegion extends Model
{
    protected $table = 'country_region';
    protected $fillable = [
			'id', 
			'country_id', 
			'name', 
			'status', 
			'isActive', 
			'created_at',
			'updated_at'
        ];
       
}
