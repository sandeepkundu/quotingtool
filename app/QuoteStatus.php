<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuoteStatus extends Model
{
    public $table = "quote_status";
	protected $fillable = [
			'name', 
			'source'
	 ];
}
