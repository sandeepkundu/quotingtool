<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuotesNotes extends Model
{
     public $table = "quote_notes";
	protected $fillable = [
			'quote_id', 
			'user_id',			
			'date_created',			
			'display_level',			
			'note'			
        ];

    public function Username(){
        return $this->hasOne('App\User','id','user_id');
    }
}
