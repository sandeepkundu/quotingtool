<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuoteStepDefault extends Model
{
    protected $table = 'quote_step_defaults';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = 
    [
		'id', 
		'client_id', 
		'step_id', 
		'group',
		'option_id'
	];

}
