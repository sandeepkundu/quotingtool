<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class ProductFactor extends Model
{
     protected $table = 'product_factors';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'client_id',
        'type_id',
        'processor_id',
        'option_id',
        'factor_type'       
    ];
    public function Clientname(){
        return $this->hasOne('App\Client','id','client_id');
    }
    public function ProductOtionType(){
        return $this->hasOne('App\ProductOption','id','type_id');
    }
    public function ProductOtionProcessor(){
        return $this->hasOne('App\ProductOption','id','processor_id');
    }
    public function ProductOtionHDD(){
        return $this->hasOne('App\ProductOption','id','option_id');
    }
}
