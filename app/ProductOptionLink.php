<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductOptionLink extends Model
{
    public $table = "product_options_link";
     
    protected $fillable = [
        'type_id',
        'brand_id',
		'client_id',
		'order'
    ];
}
