<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model{ 
	public $table = "country";
	protected $fillable = [
			'id',
			'name', 
			'currency_name', 
			'currency_symbol', 
			'currency_rate', 
			'status',
			'isActive',
			'created_at',
			'updated_at'
        ];
		public function country_regions(){
			//return $this->hasMany('App\CountryRegion', 'country_id');
			return $this->hasMany('App\CountryRegion');
		}
}
