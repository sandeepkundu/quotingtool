<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceFactorBands extends Model
{
    
    public $table = "price_factors_bands";
    
	protected $fillable = [
	        'id',
			'client_id', 
			'type_id',
			'option_id',
			'value',
			'factor_name',
			'price_band_id'
        ];

    public function Clientname(){
        return $this->hasOne('App\Client','id','client_id');
    }
    public function ProductOtionType(){
        return $this->hasOne('App\ProductOption','id','type_id');
    }
    public function ProductOtionDefect(){
        return $this->hasOne('App\ProductOption','id','option_id');
    } 
    public function PriceBands(){
        return $this->hasOne('App\PriceBands','id','price_band_id');
    }
}
