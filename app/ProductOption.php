<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductOption extends Model
{
	protected $table='product_options';

    protected $fillable = [
        'type',
        'id',
        'name',
        'value',
        'order',
        'description',
        'image',
        'status',
        'created_at',
        'updated_at'
    ];
}
