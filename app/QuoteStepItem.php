<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

//use Zizaco\Entrust\Traits\EntrustUserTrait;


class QuoteStepItem extends Model {

    protected $table = 'quote_step_items';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'quote_step_id',
        'order',
        'group_code'
    ];

}
