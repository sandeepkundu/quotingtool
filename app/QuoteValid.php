<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuoteValid extends Model
{
    protected $table = "settings";

	protected $fillable = [
			'id', 
			'client_id', 
			'setting', 
			'value'
		 ];

public function Clientname(){
        return $this->hasOne('App\Client','id','client_id');
    }
}
