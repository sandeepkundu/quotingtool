<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PupContract extends Model
{
    protected $table = "pup_contracts";
}
