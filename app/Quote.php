<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
//use Zizaco\Entrust\Traits\EntrustUserTrait;


class Quote extends Model
{
    protected $table = 'quote';
    
    const CREATED_AT = 'date_created';
    const UPDATED_AT = 'date_modified';
    
    public function quote_item(){
        return $this->hasMany('App\QuoteItem','quote_id','id');
    }
    public function QuoteItem(){
        return $this->hasOne('App\QuoteItem','quote_id','id');
    }

    public function QuoteStatus(){
        return $this->hasOne('App\QuoteStatus', 'id', 'status');
    }

    public function Clientname(){
        return $this->hasOne('App\Client','id','client_id');
    }

    public function Username(){
        return $this->hasOne('App\User','id','user_id');
    }

    public function Defects(){
        return $this->hasMany('App\QuoteItemsDefect','quote_id','id');   
    }

    public function Region(){
        return $this->hasOne('App\CountryRegion','id','region');   
    }

	public function Country(){
        return $this->hasOne('App\Country','id','country');   
    }
}
