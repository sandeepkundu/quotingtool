<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuoteCollection extends Model
{
	protected $table = 'quote_collection';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'id',
    'quote_id',
    'user_id',
    'client_id',
    'created_at',
    'date_collected',
    'status',
    'email_sent',
    'updated_at' 
    ];
    
    public function Quote() {
        return $this->hasOne('App\Quote', 'id', 'quote_id');
    }
    public function User() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    public function Clientname(){
        return $this->hasOne('App\Client', 'id', 'client_id');
    }

}
