<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model {

    protected $table = 'clients';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'client_type',
        'parent_id',
        'country',
        'region',
        'ave_collect_size',
        'margin_oem_product',
        'margin_oem_logistic',
        'margin_oem_service',
        'status'
    ];

    public function countryClient() {
        return $this->hasOne('App\Country', 'id', 'country');
    }

    public function Region() {
        return $this->hasOne('App\CountryRegion', 'id', 'region');
    }

    public function client() {
        return $this->hasMany('App\ClientUser');
    }

    public function comments() {
        //return $this->hasManyThrough("App\Client as a", "App\Client", 'parent_id', 'a.id');
        return $this->hasMany("App\Client", 'parent_id', 'id');
    }

}
