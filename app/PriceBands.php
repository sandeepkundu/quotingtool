<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceBands extends Model
{
     public $table = "price_bands";
	protected $fillable = [
			'value_from', 
			'value_to',
			'status'
			
        ];
}
