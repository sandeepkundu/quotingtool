<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

//use Zizaco\Entrust\Traits\EntrustUserTrait;


class QuoteItem extends Model {

    protected $table = 'quote_items';

    const CREATED_AT = 'date_created';
    const UPDATED_AT = 'date_modified';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id'
    ];
    public function quote_name(){
         return $this->belongsTo('App\QuoteManagement', 'quote_id','id');
    }

    public function QuoteItemsDefect(){
        return $this->hasMany('App\QuoteItemsDefect','quote_item_id','id');
    }
    public function model_image(){
        return $this->hasOne('App\Product','id','product_id');
    }
     public function option_model_image(){
        return $this->hasOne('App\ProductOption','id','type_id');
    }

}
