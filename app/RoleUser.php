<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\EntrustRole;

class RoleUser extends Model
{
    protected $table = "role_user";
    protected $fillable = [
        'user_id', 'role_id','updated_at','created_at'];
}
