<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Session;
use DateTime;
use links;
use Toast;
use App\PriceBands;
use App\PriceFactorBands;
use App\ProductOption;
use App\Clients;
use App\QuoteItem;
use App\QuoteCollection;
use App\QuoteCollectionItem;
use App\QuoteItemsDefect;
use App\QuotesNotes;
use App\Quote;
use App\Http\Controllers\ClientsController;

class PriceFactorBandsController extends Controller {

      private $permission = [1];

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index(Request $request) {
        if ($this->filter($this->permission)) {


            $ser = key($_GET);

            $PriceFactorBands = PriceFactorBands::with('ProductOtionType', 'ProductOtionDefect', 'PriceBands','Clientname')->orderBy('id', 'desc')->where('factor_name', '=', key($_GET))->get();
            
            return view('priceFactorBands.index', array('PriceFactorBands' => $PriceFactorBands, 'ser' => $ser));
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addPriceFactorBands(Request $request) {
        if ($this->filter($this->permission)) {
            $types = ProductOption::where('type', '=', 'Product')->
            orderBy('name','asc')->get();
            $option = ProductOption::where('type', '=', key($_GET))->orderBy('name','asc')->get();
            $priceBands = PriceBands::get();
           
            $clients = new ClientsController();
            $cids = Array();
            $parent_ids = $clients->getTree($cids, 0, 0, '0', '', '');
            return view('priceFactorBands/addPriceFactorBands', array('parent_ids' => $parent_ids, 'types' => $types, 'option' => $option, 'priceBands' => $priceBands));
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function insertPriceFactorBands(Request $request) {
        if ($this->filter($this->permission)) {
            Input::flash();
            $this->validate($request, array(
                'type_id' => 'required',
                'value' => 'required|numeric',
                'option_id' => 'required',
                'factor_name' => 'required',
                'price_band_id' => 'required'
            ));
            $PriceFactorBand = new PriceFactorBands();
            $PriceFactorBand->fill($request->all());
            $PriceFactorBand->client_id = $request->parent_id;
            if ($PriceFactorBand->save()) {
                Toast::success('Record has been Saved.', 'Success');
                return redirect('priceFactorBands/index?' . $request->cat);
            } else {
                Toast::error('Record not saved. Id not found.', 'Error');
                return redirect('priceFactorBands/index?' . $request->cat);
            }
        } else {
             Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editPriceFactorBands($id = Null) {
        if ($this->filter($this->permission)) {
             $arr = $array = $arr1 = array();
            if ($id != null) {
                $priceBands = PriceBands::get();
                $types = ProductOption::where('type', '=', 'Product')->orderBy('name','asc')->get();
                $option = ProductOption::where('type', '=', key($_GET))->orderBy('name','asc')->get();
                $PFBands = PriceFactorBands::find($id);
                        $clients = new ClientsController();
            $cids = Array();
            $parent_ids = $clients->getTree($cids, 0, 0, '0', '', '');
            
                if ($PFBands) {
                    return view('/priceFactorBands/editPriceFactorBands', compact('oem', 'parent_ids', 'option', 'types', 'PFBands', 'priceBands'));
                } else {
                    Toast::error('Id Not Found', 'Error');
                    return Redirect('priceFactorBands/editPriceFactorBands');
                }
            } else {
                Toast::error('Id Not Found', 'Error');
                return Redirect('priceFactorBands/editPriceFactorBands');
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatePriceFactorBands(Request $request) {
        if ($this->filter($this->permission)) {
            // dd($request->all());
            Input::flash();
            $this->validate($request, array(
                'type_id' => 'required',
                'value' => 'required|numeric',
                'option_id' => 'required',
                'factor_name' => 'required',
                'price_band_id' => 'required'
            ));
            $PFBands = PriceFactorBands::find($request->input('id'));
            if (!empty($PFBands)) {
                $PFBands->client_id = $request->get('parent_id');
                $PFBands->type_id = $request->get('type_id');
                $PFBands->value = $request->get('value');
                $PFBands->option_id = $request->get('option_id');
                $PFBands->factor_name = $request->get('factor_name');
                $PFBands->price_band_id = $request->get('price_band_id');

                if ($PFBands->save()) {
                    Toast::success('Record has been updated.', 'Success');
                    return Redirect('/priceFactorBands/index?' . $request->cat);
                } else {
                    Toast::error('Product not updated. Please try again.', 'Error');
                    return Redirect('/priceFactorBands/index?' . $request->cat);
                }
            } else {
                Toast::error('Invalid id', 'Error');
                return redirect()->route('/priceFactorBands/index?' . $request->cat)->withInput();
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id = Null) {
        if ($this->filter($this->permission)) {
            $ser = key($_GET);

            $PFBands = PriceFactorBands::find($id);
            $option_id = $PFBands->option_id;
            $QuoteItems = QuoteItem::orWhere(['dvd_id'=>$option_id])->get();
            $QuoteItemDefects = QuoteItemsDefect::orWhere(['defect_id'=>$option_id])->get();

            //dd($QuoteItemDefects);

                if(count($QuoteItems) >0 ){
                    foreach($QuoteItems as $QuoteItem){
                            $QuoteItemDelete = QuoteItem::find($QuoteItem->quote_item_id);

                            if(!empty($QuoteItemDelete)){
                                $QuoteItemDelete->delete();
                            } 

                            $QuoteDelete =  Quote::find($QuoteItem->quote_id);
                            if(!empty($QuoteDelete)){
                                $QuoteDelete->delete();
                            } 

                            $QuoteItemsDefectDelete = QuoteItemsDefect::where('quote_item_id',$QuoteItem->quote_id)->get();
                           
                            if(count($QuoteItemsDefectDelete) > 0){
                                $QuoteItemsDefectDelete->delete();
                            } 

                            $QuoteCollectionDelete = QuoteCollection::where('quote_id',$QuoteItem->quote_id)->get();
                            if(count($QuoteCollectionDelete) > 0){
                                 QuoteCollection::where('quote_id',$QuoteItem->quote_id)->delete();
                            } 

                           $QuoteCollectionItemDelete = QuoteCollectionItem::where('quote_id',$QuoteItem->quote_id)->get();
                            if(count($QuoteCollectionItemDelete) > 0){

                                QuoteCollectionItem::where('quote_id',$QuoteItem->quote_id)->delete();
                            } 


                            $QuotesNotesDelete =  QuotesNotes::where('quote_id',$QuoteItem->quote_id)->get();
                            if(count($QuotesNotesDelete) > 0){
                               QuotesNotes::where('quote_id',$QuoteItem->quote_id)->delete();
                            }
                    }
                }
                if(count($QuoteItemDefects) > 0){
                    foreach($QuoteItemDefects as $QuoteItemDefect){

                            $QuoteItemDelete = QuoteItem::find($QuoteItemDefect->quote_item_id);

                            if(!empty($QuoteItemDelete)){
                                $QuoteItemDelete->delete();
                            } 

                            $QuoteDelete =  Quote::find($QuoteItemDefect->quote_id);
                            if(count($QuoteDelete) > 0){
                                $QuoteDelete->delete();
                            } 

                            $QuoteItemsDefectDelete = QuoteItemsDefect::where('quote_item_id',$QuoteItemDefect->quote_id)->get();
                           
                            if(count($QuoteItemsDefectDelete) > 0){
                                $QuoteItemsDefectDelete->delete();
                            } 

                            $QuoteCollectionDelete = QuoteCollection::where('quote_id',$QuoteItemDefect->quote_id)->get();
                            if(count($QuoteCollectionDelete) > 0){
                                 QuoteCollection::where('quote_id',$QuoteItemDefect->quote_id)->delete();
                            } 

                           $QuoteCollectionItemDelete = QuoteCollectionItem::where('quote_id',$QuoteItemDefect->quote_id)->get();
                            if(count($QuoteCollectionItemDelete) > 0){

                                QuoteCollectionItem::where('quote_id',$QuoteItemDefect->quote_id)->delete();
                            } 


                            $QuotesNotesDelete =  QuotesNotes::where('quote_id',$QuoteItemDefect->quote_id)->get();
                            if(count($QuotesNotesDelete) > 0){
                               QuotesNotes::where('quote_id',$QuoteItemDefect->quote_id)->delete();
                            } 
                        
                    }
                }
                 
            if (!empty($PFBands)) {
                $PFBands->delete();
                Toast::success('Records has been deleted.', 'Success');
                return Redirect('/priceFactorBands/index?' . $ser);
            } else {
                Toast::error('Invalid Id. Please try Again!', 'Error');
                return Redirect('/priceFactorBands/index?' . $ser);
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function multipleDelete(Request $request) {
        if ($this->filter($this->permission)) {
            $ser = key($_GET);
            $ids = $request->get('ids');
            $arr = array();
            $PriceFactorBands = PriceFactorBands::whereIn('id',$ids)->get();
                foreach ($PriceFactorBands as $PriceFactorBand) {
                   $arr[] = $PriceFactorBand->option_id;
                }

            $QuoteItems = QuoteItem::orWhereIn('dvd_id',$arr)->get();
            $QuoteItemDefects = QuoteItemsDefect::orWhereIn('defect_id',$arr)->get();


            if(count($QuoteItems) >0 ){
                        foreach($QuoteItems as $QuoteItem){
                            $QuoteItemDelete = QuoteItem::find($QuoteItem->id);

                            if(!empty($QuoteItemDelete)){
                                $QuoteItemDelete->delete();
                            } 
                            /*$QuoteDelete =  Quote::find($QuoteItem->quote_id);
                            if(!empty($QuoteDelete)){
                                $QuoteDelete->delete();
                            } */

                            if(!empty($QuoteItem->id)){
                                QuoteItemsDefect::where('quote_item_id',$QuoteItem->id)->delete();
                            } 

                            if(!empty($QuoteItem->v) && $QuoteItem->quote_item_id != 0){
                                 $QuoteCollectionDelete = QuoteCollection::where('quote_item_id',$QuoteItem->id)->delete();
                            } 

                            if(!empty($QuoteItem->quote_item_id)){
                                QuoteCollectionItem::where('quote_item_id',$QuoteItem->id)->delete();
                            } 

                           
                            if(!empty($QuoteItem->id)){
                                $QuotesNotesDelete =  QuotesNotes::where('quote_item_id',$QuoteItem->id)->delete();
                            }
                    }
                }
                if(count($QuoteItemDefects) > 0){
                    foreach($QuoteItemDefects as $QuoteItemDefect){

                            $QuoteItemDelete = QuoteItem::find($QuoteItemDefect->quote_item_id);

                            if(!empty($QuoteItemDelete)){
                                $QuoteItemDelete->delete();
                            } 

                            /*$QuoteDelete =  Quote::find($QuoteItemDefect->quote_id);
                            if(!empty($QuoteDelete)){
                                $QuoteDelete->delete();
                            } */

                            $QuoteItemsDefectDelete = QuoteItemsDefect::find($QuoteItemDefect->id);
                           

                            if(!empty($QuoteItemsDefectDelete)){
                                $QuoteItemsDefectDelete->delete();
                            } 

                            if(!empty($QuoteItemDefect->quote_id) && $QuoteItemDefect->quote_id != 0){
                                 $QuoteCollectionDelete = QuoteCollection::where('quote_id',$QuoteItemDefect->quote_id)->delete();
                            } 

                            if(!empty($QuoteItemDefect->quote_id)){
                                QuoteCollectionItem::where('quote_id',$QuoteItemDefect->quote_id)->delete();
                            } 

                           
                            if(!empty($QuoteItemDefect->quote_id)){
                                $QuotesNotesDelete =  QuotesNotes::where('quote_id',$QuoteItemDefect->quote_id)->delete();
                            } 
                        
                    }
                }

            if (PriceFactorBands::destroy($request->get('ids'))) {
                Toast::success('Records has been deleted.', 'Success');
                return Redirect('/priceFactorBands/index?' . $ser);
            } else {
                Toast::error('Please select any record.', 'Error');
                return Redirect('/priceFactorBands/index?' . $ser);
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

}
