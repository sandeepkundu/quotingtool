<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Session;
use DateTime;
use links;
use Toast;
use App\PriceBands;
use App\PriceFactorBand;

class PriceBandsController extends Controller {

     private $permission = [1];

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        if ($this->filter($this->permission)) {
            Input::flash();
            //$txt=$request->get('search');
            $limit = $request->get('perPage');

            if (isset($limit)) {
                $lim = $limit;
            } else {
                $lim = 30;
            }
            $Pbands = DB::table('price_bands');
            $Pbands->select('price_bands.id', 'price_bands.value_from', 'price_bands.value_to', 'price_bands.status');
            /* if (isset($txt)) {
              $Pbands->Where('value_from', 'like', '%' . $txt . '%');
              $Pbands->OrWhere('value_to', 'like', '%' . $txt . '%');
              } */
            $Pbands = $Pbands->orderBy('value_from', 'asc')->paginate($lim);
            return view('priceBands/index', array('Pbands' => $Pbands, 'lim' => $lim));
        } else {
             Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addPriceBands(Request $request) {
        if ($this->filter($this->permission)) {
            return view('priceBands/addPriceBands');
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function insertPriceBands(Request $request) {
        if ($this->filter($this->permission)) {
            Input::flash();

            $this->validate($request, array(
                'value_from' => 'required|numeric',
                'value_to' => 'required|numeric'
            ));
            $value_froms = PriceBands::all();
            foreach ($value_froms as $value_from) {
                if ($value_from->value_from == $request->get('value_from')) {
                    Toast::error('Entered  value already exist.', 'Error');
                    return redirect('/priceBands/addPriceBands');
                }
                if ($value_from->value_to == $request->get('value_to')) {
                    Toast::error('Entered  value already exist', 'Error');
                    return redirect('/priceBands/addPriceBands');
                }
            }
            if ($request->get('value_to') > $request->get('value_from')) {
                if ($request->status != null && $request->status == 'on') {
                    $save = PriceBands::Create(array(
                                'value_from' => $request->value_from,
                                'value_to' => $request->value_to,
                                'status' => 'active'
                    ));
                } else {
                    $save = PriceBands::Create(array(
                                'value_from' => $request->value_from,
                                'value_to' => $request->value_to,
                                'status' => 'inactive'
                    ));
                }

                if ($save->save()) {
                    Toast::success('Record has been Saved.', 'Success');
                    return redirect('priceBands/index');
                } else {
                    Toast::error('Record not saved. Id not found.', 'Error');
                    return redirect('priceBands/index');
                }
            } else {
                Toast::error('value_from is less than value_to', 'Error');
                return redirect('/priceBands/addPriceBands');
            }
        } else {
           Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editPriceBands(Request $request, $id = Null) {
        if ($this->filter($this->permission)) {
            if (isset($id)) {
                $PriceBands = PriceBands::find($id);
                if (!empty($PriceBands)) {
                    return view('/priceBands/editPriceBands', compact('PriceBands'));
                } else {
                    Toast::error('Id Not Found', 'Error');
                    return Redirect('priceBands/editPriceBands');
                }
            }
        } else {
           Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatePriceBands(Request $request) {
        if ($this->filter($this->permission)) {
            Input::flash();
            $this->validate($request, array(
                'value_from' => 'required|numeric',
                'value_to' => 'required|numeric'
            ));

            $PriceBands = PriceBands::find($request->input('id'));
            if ($request->get('value_to') > $request->get('value_from')) {
                if (!empty($PriceBands)) {
                    if ($request->get('status') != null && $request->status == 'on') {
                        $PriceBands->status = 'active';
                    } else {
                        $PriceBands->status = 'inactive';
                    }
                    $PriceBands->value_from = $request->get('value_from');
                    $PriceBands->value_to = $request->get('value_to');

                    if ($PriceBands->save()) {
                        Toast::success('Record has been updated.', 'Success');
                        return Redirect('priceBands/index');
                    } else {
                        Toast::error('Product not updated. Please try again.', 'Error');
                        return Redirect('priceBands/index');
                    }
                }
            } else {
                Toast::error('value_from is less than value_to', 'Error');
                return redirect('/priceBands/addPriceBands/' . $request->input('id'));
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function Delete($id = Null) {
        if ($this->filter($this->permission)) {
            $priceBands = PriceBands::find($id);
            if (!empty($priceBands)) {
                $priceBands->delete();

                DB::table('price_factors_bands')->where('price_band_id', '=', $id)->delete();

                Toast::success('Records has been deleted.', 'Success');
                return Redirect('priceBands/index');
            } else {
                Toast::error('Invalid id, Please try again!', 'Error');
                return Redirect('priceBands/index');
            }
        } else {
           Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function multipleDelete(Request $request) {
        if ($this->filter($this->permission)) {
            if (PriceBands::destroy($request->get('ids'))) {

                //PriceFactorBand::destroy($request->get('ids'))->where 
                DB::table('price_factors_bands')->where('price_band_id', '=', $request->get('ids'))->delete();

                Toast::success('Records has been deleted.', 'Success');
                return Redirect('priceBands/index');
            } else {
                Toast::error('Please select any record.', 'Error');
                return Redirect('priceBands/index');
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

}
