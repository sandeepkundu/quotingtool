<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Session;
use DateTime;
use links;
use Toast;
use App\Client;
use App\User;
use App\ClientUser;
use App\Country;
use App\CountryRegion;
use App\RoleUser;
use Mail;
use App\PasswordReset;
use App\Theme;
use App\QuoteManagement;


class UsersController extends Controller {

    private $permission = [1];
   


    public function __construct() {
        $this->middleware('auth', ['except' => ['resetPassword', 'reset']]);
       
        date_default_timezone_set('Asia/Kolkata');
       
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        
      
        if ($this->filter($this->permission)) {
               
            $id = Auth::user()->id;

            $user = User::with('clientuser')->where('id', '!=', $id)->get();

            return view('users/index', array(
                'user' => $user,
            ));

        } else {

            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect('/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function getClientList() {
        $arr=array();
        $arr1=array();
        $array=array();
        $a=array();
        $parent_id = DB::table('clients')->where('parent_id', '=', 0)->where('id', '!=', '0')->get();
        foreach ($parent_id as $val) {
            $arr[] = $parent_ids = DB::table('clients')->where('parent_id', '=', $val->id)->where('id', '!=', '0')->get();
        }

        $i = 0;
        foreach ($arr as $va) {
            $j = 0;
            $arr1[$i][] = $parent_id[$i];

            foreach ($va as $ke) {
                $test = array();
                $arr1[$i][] = $ke;
                $test[] = DB::table('clients')->where('parent_id', '=', $ke->id)->get();
                foreach ($test as $a1) {
                    if (count($test) > 0) {
                        foreach ($a1 as $a2) {
                            $arr1[$i][] = $a2;
                        }
                    }
                }
            }
            $i++;
        }
        foreach ($arr1 as $va) {
            foreach ($va as $aa) {
                $array[] = $aa;
            }
        }
        $i = -1;

        $k = -1;
        foreach ($array as $key => $va) {
            if ($va->level == 1) {
                $i++;
                $a[$i] = $va;

                $j = 0;
            } else if ($va->level == 2) {
                $j++;
                $a[$i]->items[$j] = $va;

                $k = 0;
            } else if ($va->level == 3) {

                $a[$j]->items[$j]->child[$k] = $va;
                $k++;
            }
        }

        return $a;
    }
	*/

    public function addUser() {
        if ($this->filter($this->permission)) {
            //$clients = $this->getClientList();
            $Themes = Theme::orderBy('theme_name','asc')->get();
            $userRoles = DB::table('roles')->where('id', '!=', 1)->get();
            return view('users/addUser', array(
                /*'clients' => $clients,*/
                'userRoles' => $userRoles,
                'Themes'  =>$Themes
            ));

        } else {
            
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function insertUser(Request $request) {
        
        if ($this->filter($this->permission)) {
            Input::flash();

            
            $this->validate($request, array(
                'first_name' => 'required|min:3',
                'last_name' => 'required|min:3',
                'email' => 'required|min:3|unique:users',
                'password' => 'required|min:10',
                'company' => 'required',
                'phone' => 'nullable|regex:/^([0-9\-\+\(\)]*)$/|min:6|max:14',
                //'userrole' => 'required',
                'theme' => 'required',
            ));

           /* if($request->userrole == 3){
                $this->validate($request, array(
                'permission'=>'required'
            ));
            }*/

            if ($request->input('active') != null) {
                $status = 1;
            } else {
                $status = 0;
            }
            $arr = $request->permission;

            if(!empty($arr)){
                $permission = implode(",",$arr);
            }else{
                $permission = 0; 
            }

           // dd($request->all());

            $save = User::Create(array(
                        'first_name' => $request->input('first_name'),
                        'last_name' => $request->input('last_name'),
                        'email' => $request->input('email'),
                        'password' => bcrypt($request->input('password')),
                        'company' => $request->input('company'),
                        'phone' => $request->input('phone'),
                        'active' => $status,
                        'theme_id' => $request->input('theme'),
                        'permission' => $permission, 
            ));

            //dd($save);

            if ($request->input('select') != null) {
                foreach ($request->input('select') as $id) {
                    if ($id != null) {
                        $ClientUser = new ClientUser();
                        $ClientUser->user_id = $save->id;
                        $ClientUser->client_id = $id;
                        $ClientUser->save();
                    }
                }
            } else {
                $ClientUser = new ClientUser();
                $ClientUser->user_id = $save->id;
                $ClientUser->client_id = 0;
                $ClientUser->save();
            }
            if($request->input('userrole')){
               $store = RoleUser::Create(array(
                        'user_id' => $save->id,
                        'role_id' => $request->input('userrole'),
            ));   
            }
          

            if ($save->save() || $store->save()) {
                if (!empty($request->SendUserNotification)) {
                    $userid= $save->id;
                   //$getUser = DB::table('users')->where('id', $userid)->first();
                    $getUser = User::find($userid);

                    $PasswordReset = new PasswordReset();
                    $PasswordReset->email = $save->email;
                    $PasswordReset->token = bcrypt(uniqid());
                    $PasswordReset->save();
                    $Theme = Theme::find($save->theme_id);
                    //return view('users.mail',compact('PasswordReset','save','getUser','Theme'));
            dd($save);
                    Mail::send('users.mail', compact('PasswordReset','save','getUser','Theme'), function ($message) use ($save,$Theme) {

                        $message->from('support@f5-it.com', "$Theme->theme_name/ New user registration");
                        $message->subject($Theme->theme_name.'/ New user registration');
                        $message->to($save->email, $save->name);
                    });

                    //return view('users.mail',compact('PasswordReset','save','getUser','Theme'));
                }

                Toast::success('Record has been saved.', 'Success');
                return redirect('/users/index');
            } else {
                Toast::error('Record not saved . Id not found', 'Error');
                return redirect('/users/index');
            }
        } else {
            
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect('/login');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function editUser(Request $request, $id = null) {
        if ($this->filter($this->permission)) {
            if ($id != null) {
                $user = User::with('UserRole')->where('id', '=', $id)->first();
                 $Themes = Theme::orderBy('theme_name','asc')->get();
                if ($user->UserRole != null) {
                    //$clients = $this->getClientList();

                    $userRoles = DB::table('roles')->where('id', '!=', 1)->get();
                    $clientuser = DB::table('client_users')->where('user_id', '=', $id)->get();
                    $CU = array();
                    foreach ($clientuser as $val) {
                        $CU[] = $val->client_id;
                    }
					
                    if (!empty($user)) {
                        return view('/users/editUser', compact('CU', 'user', 'clientuser', /* 'clients',*/ 'userRoles','Themes'));
                    } else {
                        toast::Error('Id Not Found ', 'Error');
                        return Redirect('users/index');
                    }
                } else {
                    toast::Error('Id Not Found ', 'Error');
                    return Redirect('users/index');
                }
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function editProfile() {
        if ($this->filter($this->permission)) {
            if (Auth::user()->id != null) {
                $user = User::find(Auth::user()->id);
                $clients = Client::get();
                $clientuser = DB::table('client_users')->where('user_id', '=', Auth::user()->id)->get();
                $CU = array();
                foreach ($clientuser as $val) {
                    $CU[] = $val->client_id;
                }
                if (!empty($user)) {
                    return view('/users/editProfile', compact('CU', 'user', 'clientuser', 'clients'));
                } else {
                    toast::Error('Id Not Found ', 'Error');
                    return Redirect('users/editProfile');
                }
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function updateProfile(Request $request) {
        //dd('asdf');
        if ($this->filter($this->permission)) {
            Input::flash();

            $this->validate($request, array(
                'first_name' => 'required|min:3',
                'username' => 'required|min:3',
                'last_name' => 'required|min:3',
                'company' => 'required',
                'phone' => 'required|regex:/^[0-9+-]/'
            ));

            if (Auth::user()->id != null) {
                $user = User::with('clientuser')->find(Auth::user()->id);

                if (!empty($user)) {
                    if (isset($request->password) && !empty($request->password)) {
                        $this->validate($request, array(
                            'password' => 'required|min:3',
                            'confirm_password' => 'required|same:password'
                        ));
                        $user->password = bcrypt($request->get('password'));
                    } else {
                        unset($user->password);
                    }

                    if ($request->input('active') != null) {
                        $active = '1';
                    } else {
                        $active = '0';
                    }
                    if (isset($request->image) && !empty($request->image)) {
                        $file = array('image' => $request->image);
                        $destinationPath = public_path() . '/images/';
                        $extension = Input::file('image')->getClientOriginalExtension();
                        $now = time();
                        $fileName = $now . '.' . $extension;
                        Input::file('image')->move($destinationPath, $fileName);
                        $user->image = $fileName;
                    } else {
                        unset($user->image);
                    }

                    $user->first_name = $request->get('first_name');
                    $user->username = $request->get('username');
                    $user->last_name = $request->get('last_name');
                    $user->company = $request->get('company');
                    $user->phone = $request->get('phone');
                    $user->active = $active;
                    if ($user->save()) {
                        Toast::success('Record has been updated.', 'Success');
                        return Redirect('/');
                    } else {
                        Toast::error('User not updated, Please try again.', 'Error');
                        return Redirect('users/index');
                    }
                }
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function viewProfile() {
        if ($this->filter($this->permission)) {
            return view('/users/viewProfile');
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
                }
    }

    /**
     * ,
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function updateUser(Request $request, $id = null) {
        Input::flash();
                //dd('asasasa');
        $this->validate($request, array(
            'first_name' => 'required|min:3',
            //'username' => 'required|min:3',
            'last_name' => 'required|min:3',
            'company' => 'required',
            //'phone' => 'required|regex:/^[0-9+-]/'
            //'userrole' => 'required',
            'theme' => 'required',
        ));

        $user = User::with('clientuser')->find($request->input('id'));
        if (!empty($user)) {
            if (isset($request->password) && !empty($request->password)) {
                $this->validate($request, array(
                    'password' => 'required|min:10',
                        //'confirm_password' => 'required|same:password'
                ));
                $user->password = bcrypt($request->get('password'));
            } else {
                unset($user->password);
            }
            if($request->input('userrole')){
             DB::table('role_user')->where('user_id', $request->input('id'))->update(['role_id' => $request->input('userrole')]);   
            }
            

            if ($request->input('active') != null) {
                $status = 1;
            } else {
                $status = 0;
            }

/*            if($request->userrole == 3){
                $this->validate($request, array(
                'permission'=>'required'
                ));
            }*/

            $arr = $request->permission;

            if(!empty($arr)){
                $permission = implode(",",$arr);
            }else{
                $permission = 0; 
            }

            $user->first_name = $request->get('first_name');
            $user->last_name = $request->get('last_name');
            $user->company = $request->get('company');
            $user->phone = $request->get('phone');
            $user->theme_id = $request->get('theme');
            $user->permission = $permission;
            $user->active = $status;

            if ($user->save()) {
                $id = $user->id;
                ClientUser::where('user_id', $id)->delete();
                if ($request->input('select') != null) {
                    foreach ($request->get('select') as $key => $val) {
                        ClientUser::create(['client_id' => $val, 'user_id' => $user->id]);
                    }
                } else {
                    ClientUser::create(['client_id' => 0, 'user_id' => $user->id]);
                }

                Toast::success('Record has been updated.', 'Success');
                return Redirect('users/editUser/' . $user->id);
            } else {
                Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
            }
        }
    }

    public function multipleDelete(Request $request) {
        if ($this->filter($this->permission)) {
            $a = 0;
            foreach ($request->ids as $key => $id) {
                $user = User::find($id);
                $QuoteManagement = QuoteManagement::where('user_id', '=', $id)->get();
                if (count($QuoteManagement) == 0) {
                    if (!empty($user)) {
                        $user->delete();
                    }
                } else {
                    $a++;
                }
            }
            Toast::success("Only ($a) users deleted ,rest has quote built angainst them.So,theycan't be deleted", "success");

            return Redirect('users/index');
        } else {
           Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function Delete($id = Null) {
        if ($this->filter($this->permission)) {

            $user = User::find($id);
            $QuoteManagement = QuoteManagement::where('user_id', '=', $id)->get();

            if (count($QuoteManagement) == 0) {
                if (!empty($user)) {
                    $user->delete();
                    Toast::success('Record has been deleted.', 'success');
                    return Redirect('users/index');
                } else {
                    Toast::error('Invalid id, Please try again!', 'error');
                    return Redirect('users/index');
                }
            } else {
                Toast::error("This user has quote genereted.So,This can't be delete!", 'error');
                return Redirect('users/index');
            }
        } else {
           Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function getclients(Request $request) {
        if ($request->get('userId') != '') {
            $clientuser = ClientUser::with('clientss')->where('user_id', '=', $request->get('userId'))->get();
        }
        echo json_encode(array('clientuser' => $clientuser));
    }

    public function resetPassword() {

        if (isset($_GET['token'])) {
            $PasswordReset = PasswordReset::select('email')->where('token', '=', $_GET['token'])->first();

            if (!empty($PasswordReset)) {
                $user = User::select('email')->where('email', '=', $PasswordReset->email)->first();
                if (isset($user->email)) {
                    return view('users.reset', compact('user'));
                } else {
                    return redirect('/login');
                }
            } else {
                 Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
            }
        } else {
            return redirect('/login');
        }
    }

    public function reset(Request $request) {

        Input::flash();
        $email = $request->email;
        $this->validate($request, array(
            'password' => 'required',
            'password_confirmation' => 'required|same:password|min:10'
        ));
        $password = bcrypt($request->input('password_confirmation'));
        if (!empty($email)) {
            $user = DB::table('users')->where('email', $email)->update(['password' => $password]);
            if ($user) {
                Toast::success('Your password has been changed.', 'Success');
                return redirect('/login');
            }
        } else {
            return redirect('/users/resetPassword');
        }
    }

    public function ClientSelect(Request $request) {
		$clients = new ClientsController();
		$cids = array();
		// retreive an indent recurise list of all clients
		$cids = $clients->getTreeIndent($cids, 0, 0, '0', '', '');
		//retreive the list of clients that the current user is already assigned to
		$cids['ClientUser'] = ClientUser::with('clientss')->where('user_id', $request->id)->get()->toArray();
        $cids = array_filter($cids);
		//output the results as JSON object
		return response()->json($cids);
    }

}
