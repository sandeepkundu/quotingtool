<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use DateTime;
use links;
use App\User;
use Toast;
use App\ProductOption;
use App\Client;
use App\QuoteStep;
use App\ProductOptionGroup;
use App\QuoteStepDefault;

class QuoteStepDefaultController extends Controller {

   private $permission = [1];

    public function __construct() {
        
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        
        if ($this->filter($this->permission)) {
            $QuoteStepDefaults = QuoteStepDefault::select('clients.fullpath as fullpath','quote_step_defaults.id as id', 'quote_step_defaults.group as group', 'quote_step_defaults.step_id', 'step.name as step_name', 'clients.name as client_name', 'quote_step_defaults.client_id', 'quote_step_defaults.option_id', 'option.name as product');
            $QuoteStepDefaults->leftjoin('clients', 'quote_step_defaults.client_id', '=', 'clients.id');
            $QuoteStepDefaults->leftjoin('product_options as option', 'quote_step_defaults.option_id', '=', 'option.id');
            $QuoteStepDefaults->leftjoin('quote_step as step', 'quote_step_defaults.step_id', '=', 'step.id');

            $QuoteStepDefaults = $QuoteStepDefaults->orderBy('id','desc')->get();
    
            return view('QuoteStepDefaults.index', array('QuoteStepDefaults' => $QuoteStepDefaults));
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add() {
        
        if ($this->filter($this->permission)) {
            $QuoteSteps = QuoteStep::all();
            $pog = DB::table('product_option_groups')->select('group')->groupBy('group')->get();
            $product_options = DB::table('product_options')->get();
            $parent_id = DB::table('clients')->where('parent_id', '=', 0)->where('id', '!=', '0')->get();
            foreach ($parent_id as $val) {
                $arr[] = $parent_ids = DB::table('clients')->where('parent_id', '=', $val->id)->where('id', '!=', '0')->get();
            }
            $i = 0;
            foreach ($arr as $va) {
                $j = 0;
                $arr1[$i][] = $parent_id[$i];

                foreach ($va as $ke) {
                    $test = array();
                    $arr1[$i][] = $ke;
                    $test[] = DB::table('clients')->where('parent_id', '=', $ke->id)->get();
                    foreach ($test as $a1) {
                        if (count($test) > 0) {
                            foreach ($a1 as $a2) {
                                $arr1[$i][] = $a2;
                            }
                        }
                    }
                }
                $i++;
            }
            foreach ($arr1 as $va) {
                foreach ($va as $aa) {
                    $array[] = $aa;
                }
            }

            $parent_ids = $array;
            return view('QuoteStepDefaults.add', compact('product_options', 'parent_ids', 'QuoteSteps', 'pog'));
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function insertQuoteStepDefault(Request $request) {
        
        if ($this->filter($this->permission)) {
            Input::flash();
            $this->validate($request, array(
                'step_id' => 'required',
                'group' => 'required',
                'option_id' => 'required',
            ));
            $QuoteStepDefaults = new QuoteStepDefault();
            $QuoteStepDefaults->fill($request->all());
            $QuoteStepDefaults->client_id = $request->parent_id;
            if ($QuoteStepDefaults->save()) {
                Toast::success('Record has been saved.', 'Success');
                return redirect('/QuoteStepDefaults/index');
            } else {
                Toast::error('Record not saved', 'Error');
                return redirect('/QuoteStepDefaults/index');
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function edit($id = null) {
        
        if ($this->filter($this->permission)) {
            if ($id != null) {
                $QuoteStepDefault = QuoteStepDefault::find($id);
                if (!empty($QuoteStepDefault)) {
                    $pog = DB::table('product_option_groups')->select('group')->groupBy('group')->get();
                    $QuoteSteps = QuoteStep::all();
                    $product_options = DB::table('product_options')->get();
                    $parent_id = DB::table('clients')->where('parent_id', '=', 0)->where('id', '!=', '0')->get();
                    foreach ($parent_id as $val) {
                        $arr[] = $parent_ids = DB::table('clients')->where('parent_id', '=', $val->id)->where('id', '!=', '0')->get();
                    }
                    $i = 0;
                    foreach ($arr as $va) {
                        $j = 0;
                        $arr1[$i][] = $parent_id[$i];

                        foreach ($va as $ke) {
                            $test = array();
                            $arr1[$i][] = $ke;
                            $test[] = DB::table('clients')->where('parent_id', '=', $ke->id)->get();
                            foreach ($test as $a1) {
                                if (count($test) > 0) {
                                    foreach ($a1 as $a2) {
                                        $arr1[$i][] = $a2;
                                    }
                                }
                            }
                        }
                        $i++;
                    }
                    foreach ($arr1 as $va) {
                        foreach ($va as $aa) {
                            $array[] = $aa;
                        }
                    }
                    $parent_ids = $array;
                    return view('QuoteStepDefaults.edit', compact('QuoteStepDefault', 'parent_ids', 'product_options', 'QuoteSteps', 'pog'));
                } else {
                    Toast::error('Invalid id !!', 'Error');
                    return redirect('/QuoteStepDefaults/index');
                }
            } else {
                Toast::error('Invalid id !!', 'Error');
                return redirect('/QuoteStepDefaults/index');
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateQuoteStepDefault(Request $request) {
        
        if ($this->filter($this->permission)) {
            Input::flash();
            $this->validate($request, array(
                'step_id' => 'required',
                'group' => 'required',
                'option_id' => 'required',
            ));
            if (isset($request->id)) {
                $QuoteStepDefault = QuoteStepDefault::find($request->id);
                $QuoteStepDefault->fill($request->all());
                $QuoteStepDefault->client_id = $request->parent_id;
                $QuoteStepDefault->save();
                Toast::success('Record has been updated.', 'Success');
                return redirect('/QuoteStepDefaults/index');
            } else {
                Toast::error('Record not saved', 'Error');
                return redirect()->route('/QuoteStepDefaults/index?' . $request->cat);
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function Delete($id = null) {
        
        if ($this->filter($this->permission)) {
            if ($id != null) {
                $QuoteStepDefault = QuoteStepDefault::find($id);
                if ($QuoteStepDefault->delete()) {
                    Toast::success('Records has been deleted.', 'Success');
                    return redirect('/QuoteStepDefaults/index');
                } else {
                    Toast::error('Invalid id, Please try again!', 'Error');
                    return redirect('/QuoteStepDefaults/index');
                }
            } else {
                Toast::error('Please provide id !!', 'Error');
                return redirect('/QuoteStepDefaults/index');
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function multipleDelete(Request $request) {

        if ($this->filter($this->permission)) {
            if (QuoteStepDefault::destroy($request->get('ids'))) {
                Toast::success('Records has been deleted.', 'Success');
                return redirect('/QuoteStepDefaults/index');
            } else {
                Toast::error('Please select any record.', 'Error');
                return redirect('/QuoteStepDefaults/index');
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

}
