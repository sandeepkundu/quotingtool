<?php
namespace App\Http\Controllers;

ini_set('max_execution_time', 600);

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Session;
use DateTime;
use links;
use Toast;
use App\ProductOption;
use View;
use App\IntegrationImport;
use App\PriceBands;
use App\PriceFactorBands;
use App\ProductFactor;
use App\CountryRegion;
use App\LogisticsUnitNumber;
use App\PriceLogistic;
use App\Country;
use App\Quote;
use App\QuoteItem;
use App\QuoteItemsDefect;
use App\QuotesNotes;
use App\Http\Controllers\CalculationsController;

class LogisticImportController extends Controller
{

	 private $permission = [1];
	public function __construct() {
		$this->middleware('auth');
		date_default_timezone_set('Asia/Kolkata');
	}
	public function bulkLogisticView(){
		if (!$this->filter($this->permission)) {
                   
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
		$IntegrationImports = IntegrationImport::where('source','=','logistics')->orderBy('date','DESC')->get();
		//dd($IntegrationImports);

		return view('Variances/LogisticImport',array(
			'IntegrationImports'=> $IntegrationImports,
			));
		
	}
	public function bulkLogisticStore(Request $request){

		$debug = false;
		$folder         =  public_path().'/i/';
		$datestring 	=  date_format(new DateTime, 'Y-m-d-H-i-s');
		$importfilename = 'logistics-import-' . $datestring . '.csv';
		$exportfilename = 'logistics-export-' . $datestring . '.csv';

		/*
			1. Save uploaded file to server 
		*/	
			if (! isset($_FILES["fileToUpload"])) {
				return  'A csv file must be selected for upload';
			}
			if ($_FILES["fileToUpload"]["error"] > 0) {
				Toast::error('A error was encountered uploading this file(' . $_FILES["fileToUpload"]["error"] . ')', 'Error');
				return redirect('/Variances/logistic-view');
			}

			$ftype = explode(".",$_FILES['fileToUpload']['name']);
			if(strtolower(end($ftype)) !== 'csv'){

				Toast::error('only a csv can be uploaded', 'Error');
				return redirect('/Variances/logistic-view');
			}
			
			if(move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $folder. $importfilename) === FALSE){

				Toast::error('File could not be saved', 'Error');
				return redirect('/Variances/logistic-view');
			}

			/*
			2. Create a new import record 
			*/
			$IntegrationImport               = new IntegrationImport();
			$IntegrationImport->date         = date('Y-m-d-H-i-s');
			$IntegrationImport->fileimported = $importfilename;
			$IntegrationImport->source       = 'logistics';
			$IntegrationImport->save();

			$import_id = $IntegrationImport->id;

			//reset all records so that none are flagged as being updated
			DB::update(DB::raw("UPDATE `price_logistics` SET processed = 0"));

			/*
			3. Process the file 
			*/
			$row = 0;
			$columncount = 0;
			$processrecords = array();
			$columnames = array();
			$isValid = FALSE;
			$num_invalid = 0;
			$num_valid = 0;

			if (($handle = fopen($folder. $importfilename, "r")) !== FALSE) {

				while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
					$data = array_map("utf8_encode", $data); 
					$row ++;
					if($row == 1){
						$columncount = count($data);
						$columnames = $data;
					} else if (count($data) != 6){
					// incorrect number of columns for the row
						array_unshift($data, "[record has incorrect number of columns (".  count($data) . ")]");				
						array_push($processrecords, $data);
						$num_invalid ++;
					} else {				
					$country	= trim($data[0]);		// Country				e.g. New Zealand	
					$region		= trim($data[1]);		// Country Region		e.g. South Island		
					$ufrom		= $data[2];				// Unit From			e.g. 100	
					$uto		= $data[3];				// Unit To				e.g. 200
					$client		= trim($data[4]);		// Client				e.g. HP India store A		
					$value		= $data[5];				// Price				e.g. 20

					// validate the data
					$validationmessage = '';
					$isValid = FALSE; 

					if(empty($country) || $country === ''){
						$validationmessage .= "[a country is required]";
					}	
					if(empty($ufrom) || !is_numeric($ufrom) || $ufrom < 0){
						$validationmessage .= "[Unit From is invalid]";
					}	
					if(empty($uto) || !is_numeric($uto) || $uto <= $ufrom){
						$validationmessage .= "[Unit To is invalid]";
					}		
					if(empty($value) || !is_numeric($value)){
						$validationmessage .= "[Price is invalid]";
					}	

					if(isset($status) && $status !== 'active'){
						$status = 'disabled'; 
					}

					if($debug){
						echo '<pre>';
						print_r($data);
						echo '</pre>';
						echo "SELECT 
						IFNULL((SELECT id FROM clients WHERE name = '". $client . "'), 0) AS client,
						IFNULL((SELECT id FROM country WHERE name = '". $country . "'), 0) AS country,
						IFNULL((SELECT id FROM country_region WHERE name = '". $region . "' AND country_id IN ((SELECT id FROM country WHERE name = '". $country . "'))), 0) AS region,
						IFNULL((SELECT id FROM logistics_unit_numbers WHERE value_from = ". $ufrom . " AND value_to = ". $uto . "), 0) AS unit<br><BR>";
					}
						
					// vailidate model and device components
					$args = array(
						'client' => $client, 
						'country' => $country, 
						'region' => $region,
						'country2' => $country, 
						'unitfrom' => $ufrom,
						'unitto' => $uto
						);

					$resultset = DB::select(DB::raw("SELECT 
						IFNULL((SELECT id FROM clients WHERE name = :client), 0) AS client,
						IFNULL((SELECT id FROM country WHERE name = :country), 0) AS country,
						IFNULL((SELECT id FROM country_region WHERE name = :region AND country_id IN ((SELECT id FROM country WHERE name = :country2))), 0) AS region,
						IFNULL((SELECT id FROM logistics_unit_numbers WHERE value_from = :unitfrom AND value_to = :unitto), 0) AS unit"),$args);

					if(!empty($resultset)){

						if($debug){
							echo '<pre>';
							print_r($resultset);
							echo '</pre>';
						}

						$country_id = $resultset[0]->country;
						$region_id = $resultset[0]->region;
						$unit_id = $resultset[0]->unit;
						$client_id = $resultset[0]->client;

						if($country_id === 0) {
							$validationmessage .= "[country is invalid]";
						}
						if($client_id === 0 && strtolower($client) !== 'default') {
							$validationmessage .= "[client not found]";
						}

						if($debug){ echo "message = $validationmessage<br>"; }
							
						//don't continue if there are any validation errors
						if($validationmessage === '') {
							if($region_id == 0){
								if($debug){ echo "message2<br>"; }
								$CountryRegion = new CountryRegion();
								$CountryRegion->name = $region;
								$CountryRegion->country_id = $country_id;
								$CountryRegion->status = 'active';
								$CountryRegion->isActive = '1';
								$CountryRegion->save();	
								$region_id = $CountryRegion->id;
								$validationmessage .= "[added region]";
							}

							// add logistic unit numbers
							if($unit_id == 0){
								if($debug){ echo "message3<br>"; }
								$LogisticsUnitNumber = new LogisticsUnitNumber();
								$LogisticsUnitNumber->client_id = $client_id;
								$LogisticsUnitNumber->value_from = $ufrom;
								$LogisticsUnitNumber->value_to = $uto;
								$LogisticsUnitNumber->save();
								$unit_id = $LogisticsUnitNumber->id;
								$validationmessage .= "[added logistics units]";
							}

							$resultset1 = DB::select(DB::raw("SELECT * 
								FROM `price_logistics` 
								WHERE `country_id` = $country_id 
								AND `region_id` = $region_id 
								AND `client_id` = $client_id 
								AND `logistic_unit_id` = $unit_id 
								ORDER BY `id` DESC LIMIT 1"));
																
							if(count($resultset1)  > 0){
								if($debug){ echo "message4<br>"; }
								//exist so only update processed
								$id = $resultset1[0]->id;
								$PriceLogistic = PriceLogistic::find($id);
								$PriceLogistic->processed =	1;
								$PriceLogistic->value =	 $value;
								$PriceLogistic->save();
								$validationmessage .= "[updated]";
								$isValid = TRUE; 									
							}else{
								if($debug){ echo "message5<br>"; }
								//Add
								$PriceLogistic = new PriceLogistic();
								$PriceLogistic->client_id = $client_id;
								$PriceLogistic->country_id = $country_id;
								$PriceLogistic->region_id = $region_id;
								$PriceLogistic->logistic_unit_id = $unit_id;
								$PriceLogistic->value = $value;
								$PriceLogistic->processed = 1;
								$PriceLogistic->save();
								$validationmessage .= "[added]";
								$isValid = TRUE; 
							}
						}
					} else {
						$validationmessage .= "[error occurred validating model]";
					}

					if($isValid) { 
						$num_valid ++;
					} else {
						$num_invalid ++;
					}

					array_unshift($data, $validationmessage);				
					array_push($processrecords, $data);
				}
			}
				
			$resultset5 = DB::select(DB::raw("SELECT 
				'[deleted]' AS `message`,
				cty.name AS `country`,
				r.name AS `region`,
				uv.value_from AS `from`,
				uv.value_to as `to`,
				c.name AS `client`,
				pl.value as `value`
				FROM `price_logistics` pl
				INNER JOIN `country` cty ON cty.id = pl.country_id
				LEFT JOIN `country_region` r ON r.id = pl.region_id
				LEFT JOIN `logistics_unit_numbers` uv ON uv.id = pl.logistic_unit_id
				LEFT JOIN `clients` c ON c.id = pl.client_id
				WHERE processed = 0"));

			if(!empty($resultset5)){
				foreach ($resultset5 as $row) {
					//add each record to array for export in csv
					array_push($processrecords, 
						array (
							$row->message,
							$row->country,
							$row->region,
							$row->from,
							$row->to,
							$row->client,
							$row->value
							)
						);						
				}
			}

			//remove all records not updated
			DB::delete(DB::raw("DELETE FROM `price_logistics` WHERE processed = 0"));

			//save the processed records to file.
			$exportfile = fopen($folder.$exportfilename, "w");
            fprintf($exportfile, chr(0xEF).chr(0xBB).chr(0xBF));
            
			array_unshift($columnames, 'message');
			fputcsv($exportfile, $columnames);
			foreach ($processrecords as $line){
				fputcsv($exportfile, $line);
			}

			/*
			4. Update import record will export file name andretrun results to 
			*/	
			$IntegrationImport                = IntegrationImport::find($import_id);
			$IntegrationImport->fileexported  = $exportfilename;
			$IntegrationImport->records_valid = $num_valid;
			$IntegrationImport->records_invalid = $num_invalid;
			$IntegrationImport->save();

			//need to recalulate all inprogress quotes.
			$Quotes = Quote::where('status', '=', '1')->get();
			if(count($Quotes) >0 ){
				foreach($Quotes as $Quote){
					$calculate = new CalculationsController();
					$calculate->Recalculate($Quote->id);

					$QuotesNotes = new QuotesNotes();
					$QuotesNotes->quote_id = $Quote->id;
					$QuotesNotes->user_id = Auth::user()->id;
					$QuotesNotes->display_level = 1;
					$QuotesNotes->note = 'Logistics pricing bulk import - recalculation of in-progress quotes.';
					$QuotesNotes->save();
				}
			}

			Toast::Success('iImport has been completed', 'Success');
			return redirect('/Variances/logistic-view');

		} else {

			Toast::error('the imported file couldn\'t be processed', 'Error');
			return redirect('/Variances/logistic-view');
		}
	}
}
