<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use DateTime;
use links;
use App\Theme;
use Toast;

class ThemeController extends Controller
{
	private $permission = [1];

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request){
    	if ($this->filter($this->permission)) {
            Input::flash();
            
            $Themes = Theme::orderBy('id', 'desc')->paginate(30);
            
            return view('Theme/index', compact('Themes'));
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }
    public function add(){
    	 if ($this->filter($this->permission)) {
            return view('Theme/add');
        } else {
           Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    	
    }
    public function insert(Request $request) {
    	Input::flash();
    	//dd($request->logo);
        	//dd(Input::file('logo'));
        if ($this->filter($this->permission)) {
            
            $this->validate($request, array(
                'theme_name' => 'required',
                'email' => 'required',
                'css_file' => 'required',
                'logo' => 'required|max:100',
                'js_file' => 'required',
                'email_template' => 'required'
            ));
           	$theme = new Theme();

           	$theme->fill($request->all());

           	if (!empty($request->logo)) {
                $file = array('logo' => $request->logo);
                $destinationPath = public_path() . '/images/';
                $extension = Input::file('logo')->getClientOriginalExtension();
                $now = time();
                $fileName = $now . '.' . $extension;
                Input::file('logo')->move($destinationPath, $fileName); //  
                $image = $fileName;
            } else {
                $image = '';
            }

            if (!empty($request->js_file)) {
                $file = array('js_file' => $request->js_file);
                $destinationPath = public_path() . '/js/';
                $extension = Input::file('js_file')->getClientOriginalExtension();
                $now = time();
                $fileName = $now . '.' . $extension;
                Input::file('js_file')->move($destinationPath, $fileName); //  
                $js_file = $fileName;
            } else {
                $js_file = '';
            }

            if (!empty($request->css_file)) {
                $file = array('css_file' => $request->css_file);
                $destinationPath = public_path() . '/css/';
                $extension = Input::file('css_file')->getClientOriginalExtension();
                $now = time();
                $fileName = $now . '.' . $extension;
                Input::file('css_file')->move($destinationPath, $fileName); //  
                $css_file = $fileName;
            } else {
                $css_file = '';
            }

            $theme->logo = $image;
            $theme->email = $request->email;
            $theme->css_file = $css_file;
            $theme->js_file = $js_file;
           	$theme->email_template = $request->email_template;

           	$theme->save();

            if ($theme->save()) {
                    Toast::success('Record has been saved.', 'Success');
                    return redirect('/Theme/index');
            } else {
                    Toast::error('Id not found', 'Error');
                    return redirect('/Theme/add');
            }

        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }
    public function edit($id = null){
    	if($id != null){
    		$Theme = Theme::find($id);
    		return view('Theme/edit',compact('Theme'));
    	}else{
    		Toast::error('Id not found', 'Error');
            return redirect('/Theme/index');
    	}
    	
    }
    public function update(Request $request) {
    	Input::flash();

        if ($this->filter($this->permission)) {
            //dd($request->logo);

	        $this->validate($request, array(
                'theme_name' => 'required',
                'email' => 'required',
                //'css_file' => 'required',
                //'logo' => 'required|max:100',
                //'js_file' => 'required',
                'email_template' => 'required'
            ));

	        $theme = Theme::find($request->id);

	        $theme->fill($request->all());

            if (!empty($theme)) {


                if (isset($request->logo) && !empty($request->logo)) {
                    $file = array('image' => $request->image);
                    $destinationPath = public_path() . '/images/';
                    $extension = Input::file('logo')->getClientOriginalExtension();
                    $now = time();
                    $fileName = $now . '.' . $extension;
                    Input::file('logo')->move($destinationPath, $fileName);
                    $theme->logo = $fileName;
		         } else {
		        	unset($theme->logo);
		        }

                if (isset($request->css_file) && !empty($request->css_file)) {
                    $file = array('css_file' => $request->css_file);
                    $destinationPath = public_path() . '/css/';
                    $extension = Input::file('css_file')->getClientOriginalExtension();
                    $now = time();
                    $fileName = $now . '.' . $extension;
                    Input::file('css_file')->move($destinationPath, $fileName);
                    $theme->css_file = $fileName;
                 } else {
                    unset($theme->css_file);
                }

                if (isset($request->js_file) && !empty($request->js_file)) {
                    $file = array('js_file' => $request->image);
                    $destinationPath = public_path() . '/js/';
                    $extension = Input::file('js_file')->getClientOriginalExtension();
                    $now = time();
                    $fileName = $now . '.' . $extension;
                    Input::file('js_file')->move($destinationPath, $fileName);
                    $theme->js_file = $fileName;
                 } else {
                    unset($theme->js_file);
                }

                $theme->email = $request->email;
                $theme->email_template = $request->email_template;
	           	$theme->save();

	            if ($theme->save()) {
	                    Toast::success('Record has been updated.', 'Success');
	                    return redirect('/Theme/index');
	            } else {
	                    Toast::error('Id not found', 'Error');
	                    return redirect()->back();
	            }

	        } else {
	            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
	        }
    	}
	}

	public function Delete($id = Null) {
        if ($this->filter($this->permission)) {
            $Theme = Theme::find($id);
            if (!empty($Theme)) {
                $Theme->delete();
                Toast::success('Record has been deleted.', 'Success');
                return Redirect('Theme/index');
            } else {
                Toast::error('Invalid id, Please try again!', 'Error');
                return Redirect('Theme/index');
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }
    public function multipleDelete(Request $request) {
	    if ($this->filter($this->permission)) {

	        if (Theme::destroy($request->get('ids'))) {
	            Toast::success('Record has been deleted.', 'Success');
	            return Redirect('Theme/index');
	        } else {
	            Toast::error('Invalid id, Please try again!', 'Error');
	            return Redirect('Theme/index');
	        }
	    } else {
	        Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
	    }
    }
}
