<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Session;
use DateTime;
use Toast;
use links;
use App\Client;
use App\User;
use App\ProductOption;
use App\QuoteManagement;
use App\ProductOptionGroup;
use App\QuoteCollection;
use App\QuoteSteps;
use App\Quote;
use App\QuoteItem;
use App\QuoteCollectionItem;
use Mail;


class QuoteCollectionController extends Controller {

    private $permission = [1];

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index(Request $request) {
        if ($this->filter($this->permission)) {

            

        $Quotes = Quote::with('Clientname','Username')->where('status',4)->orderBy('date_modified','desc')->get();
           
                
           

            return view('QuoteCollection/index', compact('Quotes'));
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function history($id = null){
        $ClientName = Client::find($id);
        $Quotes = Quote::where('client_id',$id)->where('status',4)->get();

        foreach ($Quotes as $Quote) {
           $QuoteId[] = $Quote->id;
        }

        $QuoteItem = QuoteItem::select('quote_items.serial_number as serial_number','quote_items.type_name as type_name','quote_items.screen_name as screen_name','quote_items.processor_name as processor_name','quote_items.brand_name as brand_name','quote_items.id as QI_ID','product.model as model','product.image as m_image','product_options.image as productImage')
        ->leftJoin('product', 'product.id', '=', 'quote_items.model_id')
        ->leftJoin('product_options', 'product_options.id', '=', 'quote_items.type_id');
        
        $QuoteItems = $QuoteItem->whereIn('quote_id', $QuoteId)->get();

        return view('QuoteCollection.history',compact('QuoteItems','id'));
    }

    public function addQuoteCollection() {
        if ($this->filter($this->permission)) {
            $parent_id = DB::table('clients')->where('parent_id', '=', 0)->where('id', '!=', '0')->get();
            foreach ($parent_id as $val) {
                $arr[] = $parent_ids = DB::table('clients')->where('parent_id', '=', $val->id)->where('id', '!=', '0')->get();
            }
            $i = 0;
            foreach ($arr as $va) {
                $j = 0;
                $arr1[$i][] = $parent_id[$i];

                foreach ($va as $ke) {
                    $test = array();
                    $arr1[$i][] = $ke;
                    $test[] = DB::table('clients')->where('parent_id', '=', $ke->id)->get();
                    foreach ($test as $a1) {
                        if (count($test) > 0) {
                            foreach ($a1 as $a2) {
                                $arr1[$i][] = $a2;
                            }
                        }
                    }
                }
                $i++;
            }
            foreach ($arr1 as $va) {
                foreach ($va as $aa) {
                    $array[] = $aa;
                }
            }
            $parent_ids = $array;
            $User = User::all();
            $quote_status = DB::table('quote_status')->get();
            $Quotes = QuoteManagement::all();
            return view('QuoteCollection/addQuoteCollection', compact('parent_ids', 'User', 'Quotes', 'quote_status'));
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function insertQuoteCollection(Request $request) {
        if ($this->filter($this->permission)) {
            Input::flash();

            $this->validate($request, array(
                'quote_id' => 'required',
                'user_id' => 'required',
            ));
            $QuoteCollection = new QuoteCollection();
            $QuoteCollection->fill($request->all());
            $QuoteCollection->client_id = $request->parent_id;
            if ($QuoteCollection->save()) {
                Toast::success('Record has been saved.', 'Success');
                return redirect('QuoteCollection/index');
            } else {
                Toast::error('Id not found', 'Error');
                return redirect('QuoteCollection/index');
            }
        } else {
           
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editQuoteCollection($id = null) {
        if ($this->filter($this->permission)) {
            if ($id != null) {
                $parent_id = DB::table('clients')->where('parent_id', '=', 0)->where('id', '!=', '0')->get();
                foreach ($parent_id as $val) {
                    $arr[] = $parent_ids = DB::table('clients')->where('parent_id', '=', $val->id)->where('id', '!=', '0')->get();
                }
                $i = 0;
                foreach ($arr as $va) {
                    $j = 0;
                    $arr1[$i][] = $parent_id[$i];

                    foreach ($va as $ke) {
                        $test = array();
                        $arr1[$i][] = $ke;
                        $test[] = DB::table('clients')->where('parent_id', '=', $ke->id)->get();
                        foreach ($test as $a1) {
                            if (count($test) > 0) {
                                foreach ($a1 as $a2) {
                                    $arr1[$i][] = $a2;
                                }
                            }
                        }
                    }
                    $i++;
                }
                foreach ($arr1 as $va) {
                    foreach ($va as $aa) {
                        $array[] = $aa;
                    }
                }
                $parent_ids = $array;
                $User = User::all();
                $quote_status = DB::table('quote_status')->get();
                $Quotes = QuoteManagement::all();
                $QuoteCollection = QuoteCollection::find($id);
                return view('QuoteCollection/editQuoteCollection', compact('parent_ids', 'User', 'Quotes', 'quote_status', 'QuoteCollection'));
            } else {
                Toast::error('Id not found', 'Error');
                return redirect('QuoteCollection/index');
            }
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateQuoteCollection(Request $request) {
        if ($this->filter($this->permission)) {
            Input::flash();
            $this->validate($request, array(
                'quote_id' => 'required',
                'user_id' => 'required'
            ));
            if (isset($request->id)) {
                $QuoteCollection = QuoteCollection::find($request->id);
                $QuoteCollection->fill($request->all());
                $QuoteCollection->client_id = $request->parent_id;
                if ($QuoteCollection->save()) {
                    Toast::success('Record has been Updated.', 'Success');
                    return redirect('QuoteCollection/index');
                } else {
                    Toast::error('Id not found', 'Error');
                    return redirect('QuoteCollection/index');
                }
            } else {
                Toast::error('Id not found', 'Error');
                return redirect('QuoteCollection/index');
            }
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id = null) {
        if ($this->filter($this->permission)) {
            if ($id != null) {
                $QuoteCollection = QuoteCollection::find($id);
                if ($QuoteCollection->delete()) {
                    Toast::success('Records has been deleted.', 'Success');
                    return redirect('/QuoteCollection/index');
                } else {
                    Toast::error('Invalid id, Please try again!', 'Error');
                    return redirect('/QuoteCollection/index');
                }
            } else {
                Toast::error('Id not found', 'Error');
                return redirect('/QuoteCollection/index');
            }
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function multipleDelete(Request $request) {
        if ($this->filter($this->permission)) {
            if (QuoteCollection::destroy($request->get('ids'))) {
                Toast::success('Records has been deleted.', 'Success');
                return redirect('/QuoteCollection/index');
            } else {
                Toast::error('Invalid id, Please try again!', 'Error');
                return redirect('/QuoteCollection/index');
            }
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }
    /*=========================================================*/
    public function collection_status(Request $request){
          if ($this->filter($this->permission)) {
        Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');

        $client_id = $request->client_id;

        if (isset($request->ids)) {

            $user = Auth::user()->id;

            foreach ($request->ids as  $id) {
                if($id != null){
                    
                    $QuoteItem = QuoteItem::find($id);
                    $Quote = Quote::find($QuoteItem->quote_id);
                    $Quote ->status = '5';
                    $Quote->save();
                    $QuoteCollection = new QuoteCollection();
                    $QuoteCollection->quote_id = $Quote->id;
                    $QuoteCollection->user_id = $Quote->user_id;
                    $QuoteCollection->client_id = $Quote->client_id;
                    $QuoteCollection->date_collected = date('Y-m-d H:i:s');
                    $QuoteCollection->email_sent = 'yes';
                    $QuoteCollection->save();

                    //===================== Collection Items ==========================//

                    $QuoteCollectionItem = new QuoteCollectionItem();
                    $QuoteCollectionItem->collection_id = $QuoteCollection->id;
                    $QuoteCollectionItem->quote_id = $Quote->id;
                    $QuoteCollectionItem->quote_item_id = $QuoteItem->id;
                    $QuoteCollectionItem->save();
                }               
            }

           /* $quotes = Quote::with('QuoteItem', 'Clientname', 'Username')->where('status', '=',5)->orderBy('date_modified','desc')->get();
            $save = array();
            //dd($quotes);

            Mail::send('hpUser.pickup', array('quotes'=>$quotes), function ($message) use ($save) {
                $message->subject('pickup Request');
                $message->to('prerna.yadav@webnyxa.com', 'Admin');
            });*/
          
           return redirect('QuoteCollection/CollectionDetails');
           }     
        }
    }
    public function CollectionDetails(Request $request){
        if (!$this->filter($this->permission)) {
           Toast::error("Authorization  failed:You  don't have permission.", "Error");
                   Auth::logout();
                  return redirect ('/login');
        }

        $txt = trim($request->get('search'));
        $limit = $request->get('perPage');
        if (isset($limit)) {
        $lim = $limit;
        } else {
            $lim = 30;
        }

        $page = ($request->page);
        $i = 0;
        if (isset($page) && $page > 1) {
        $pages = ($page - 1) * $lim;
        } else {
        $pages = 0;
    }

        $QuoteCollection = QuoteCollection::select('quote_collection.*','users.first_name as Username','clients.name as ClientName','clients.id as client_id','quote.status as quote_status');
        if(!empty($txt)){
            $QuoteCollection ->where('Username', 'like', '%' . $txt . '%');
            $QuoteCollection ->OrWhere('clients.name', 'like', '%' . $txt . '%');
        }
        $QuoteCollection->leftJoin('users', 'users.id', '=', 'quote_collection.user_id');
        $QuoteCollection->leftJoin('clients', 'clients.id', '=', 'quote_collection.client_id');
        $QuoteCollection->leftJoin('quote', 'quote.id', '=', 'quote_collection.quote_id');

        $QuoteCollections = $QuoteCollection->where('clients.client_type','store')->orderBy('date_collected','desc')->paginate($lim);
        return view('/QuoteCollection.details',compact('lim','id','ClientName','pages','QuoteCollections'));
    } 

    public function pickupRequestDetail(){

    $quotes = Quote::with('QuoteItem', 'Clientname', 'Username')->where('client_id', '=',$_GET['client_id'])->where('status', '=',5)->orderBy('date_modified','desc')->get();

    return view('QuoteCollection/pickupRequestDetail', array('quotes' => $quotes));
    }
}
