<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\LoginSuccess;
use App\LoginAttempt;
use Auth;
use App\RoleUser;
use App\User;

class LoginController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() 
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm() {

      return view('auth.login');

  /*  if(isset($_COOKIE['theme_name']))
      {
          if($_COOKIE['theme_name']=="HP"){
            return view('auth.login');
          } else{
            return view('auth.hplogin');
          }
      }else{
        return view('auth.hplogin');
      }*/
    }

    protected function authenticated() {
      
        $user1 = auth()->user();
        $user1->last_login = time();
        $user1->ip_address = $_SERVER["REMOTE_ADDR"];
        $user1->save();

        $user = LoginSuccess::Create([
                    'user_id' => Auth::user()->id,
                    'date_loggedin' => date('Y-m-d H:i:s'),
                    'ip_address' => $_SERVER["REMOTE_ADDR"],
                    'browser' => $_SERVER["HTTP_USER_AGENT"]
        ]);

        $user = LoginAttempt::Create([
                    'ip_address' => $_SERVER["REMOTE_ADDR"],
                    'login' => '1',
                    'time' => time(),
        ]);
         
        $theme_name = $user1['theme_id'];

        setcookie('theme_name',$theme_name); // 86400 = 1 day
      
        $role_id = RoleUser::select('role_id')->where('user_id', '=',Auth::user()->id)->first()->role_id;
         
        $lastUrl = Auth::user()->last_url;
           //($last_url);
           
        if($role_id == 1){
          if(isset($lastUrl) && !empty($lastUrl)){
             return redirect($lastUrl);
          }else{
            return redirect('/admin/dashboard');
          }
          
        } else if($role_id == 2 || $role_id == 3){
         
          if(isset($lastUrl) && !empty($lastUrl)){
             return redirect($lastUrl);
          }else{
         
           return redirect('/dashboard');
          }
           
       }
    }
    public function login(Request $request) {
       
        $this->validateLogin($request);
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        if ($this->guard()->validate($this->credentials($request))) {
            $user = $this->guard()->getLastAttempted();
            if ($user->active && $this->attemptLogin($request)) {
                return $this->sendLoginResponse($request);
            } else {
             
                // Increment the failed login attempts and redirect back to the
                // login form with an error message.
                $this->incrementLoginAttempts($request);
                return redirect()
                                ->back()
                                ->withInput($request->only($this->username(), 'remember'))
                                ->withErrors(['active' => 'You must be active to login.']);
            }
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    

}
