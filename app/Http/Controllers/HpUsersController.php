<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Session;
use DateTime;
use links;
use Toast;
use App\Client;
use App\User;
use App\ClientUser;
use App\Country;
use App\CountryRegion;
use App\RoleUser;
use App\ProductOption;
use App\Product;
use App\PupContract;
use App\ProductOptionGroup;
use App\QuoteManagement;
use View;
use Response;
use App\QuoteItem;
use App\Quote;
use App\QuoteStatus;
use Mail;
use PDF;
use URL;
use App\QuoteCollection;
use App\QuoteItemsDefect;
use App\QuoteCollectionItem;
use App\QuoteValid;
use App\Navigation;
use App\PasswordReset;
use App\Theme;
use App\QuoteStepDefault;
use App\ProductClient;
use App\ClientCountryRate;
use App\Http\Controllers\CalculationsController;
use Cookie;

class HpUsersController extends Controller {



    private $permission = [2,3];


    /*
     * |--------------------------------------------------------------------------
     * | HpUsersController
     * |--------------------------------------------------------------------------
     */

    public function __construct() {
        $this->middleware('auth');
        $this->middleware('checkRole');
        date_default_timezone_set('Asia/Kolkata');
    }


    private function getSteps($productId = null, $brandId = null) {
        /* internally called method so user is already authenticated */
        if(Session::has('client_id')){
            $client_id = Session::get('client_id');

        }else{
            $client_id = 0;
        }

        $calculate = new CalculationsController();
        $step =  $calculate->getQuoteSteps($productId, $brandId, $client_id);
        return $step;
    }

    private function getStepsWithProcessorName($product = null, $brand = null, $order = '') {
        /* internally called method so user is already authenticated */
        if(Session::has('client_id')){
            $client_id = Session::get('client_id');
        }else{
            $client_id = 0;
        }
        $step = $this->getSteps($product, $brand);

        if(Session::has('step_id')){
            // check if step process is the same when brand is changed
        } else {
            Session::put('step_id', $step[0]->id);
        }

        $proccessor = array();
        $Screen = array();
        $ram = array();
        $hdd = array();
        $DVD = array();
        $pro = '';
        $arr = array();

        foreach ($step as $key => $value) {

            if ($value->order == $order) {
                $a = substr($value->group_code, 0, -1);

                if (strpos($a, 'Processor') !== false) {
                    $pro = $value->group_code;
                    if ($pro != '') {
                        $productogns = ProductOptionGroup::where('group_code',$pro)->groupBy('processor_sub_group')->orderBy('order')->get();

                        foreach ($productogns as $productogn) {

                            $args = array(
                                'client'    => $client_id,
                                'subgroup'  => $productogn->processor_sub_group,
                                'group'     => $pro,
                                'brand'     => $brand,
                                'type'      => $product
                            );

                            $sql = "SELECT PO.description as description,p.*
                            FROM `product_option_groups` p
                            JOIN(
                            SELECT t.`id` AS client_id, @pv:=t.`parent_id` AS parent, @rownum := @rownum + 1 AS rownum
                            FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
                            JOIN (SELECT @pv:= :client, @rownum:=0)tmp
                            WHERE t.`id`= @pv
                            )c on c.`client_id` = p.`client_id`
                            LEFT JOIN `product_options` as PO ON `p`.`option_id` = `PO`.`id`
                            WHERE p.`processor_sub_group` = :subgroup 
                            AND p.`group_code` = :group 
                            AND p.`status` = 'active'
                            AND p.`option_id` IN (
                            SELECT processor_id 
                            FROM `product` 
                            WHERE `type_id` = :type 
                            AND (`brand_id` = :brand OR `brand_id` IS NULL OR `brand_id` = 0) 
                            AND `value_factor` = 'explicit price' 
                            AND `value` IS NOT NULL 
                            AND `status` = 'active'
                            )
                            ORDER BY p.`order` ASC, c.`rownum`";

                            $proccessor = DB::select(DB::raw($sql), $args);

                            Session::put('Processor', $order);

                            if(count($proccessor) > 0){
                                $arr[] = array('name' => 'Processor', 'item' => $proccessor, 'subgroup' => $productogn->processor_sub_group);
                            }
                        }

                        return $arr;
                    }
                } else if (strpos($a, 'Screen') !== false || strpos($a, 'HDD') !== false || strpos($a, 'RAM') !== false || strpos($a, 'DVD') !== false || strpos($a, 'Processor') !== false) {
                    $pro = $value->group_code;
                    $exs = explode(',',$pro);                      

                    foreach ($exs as  $ex) {

                        $group = $ex;
                        if(count($exs) > 1){
                            $name = 'group';
                        }else{
                            $name = $group;
                        }

                        if ($group != '') {

                            $Screen = DB::select(DB::raw('SELECT PO.description as description,p.*
                                FROM `product_option_groups` p
                                JOIN(
                                SELECT t.`id` AS client_id
                                , @pv:=t.`parent_id` AS parent
                                , @rownum := @rownum + 1 AS rownum
                                FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
                                JOIN (SELECT @pv:= '.$client_id.', @rownum:=0)tmp
                                WHERE t.`id`=@pv
                            )c on c.`client_id` = p.`client_id`
                            LEFT JOIN `product_options` as PO ON `p`.`option_id` = `PO`.`id`
                            WHERE (`group_code` = "' . $group . '" AND `p`.`status`="active")
                            ORDER BY p.`order` ASC, c.`rownum`'));
                            $count = count($Screen);
                            $sessiong = preg_replace('/[0-9]+/', '', $group);

                            Session::put($sessiong, $order);
                                //Session::put('Screen', $order);
                            $arr[] = array('name' => $group, 'item' => $Screen, 'count' => $count);                             
                        }

                    }
                    return $arr;
                        //echo "<pre>";print_r($arr);die;

                } else if (strpos($a, 'Defects') !== false) {
                    $pro = $value->group_code;
                    if ($pro != '') {
                        $Defects = DB::select(DB::raw('SELECT PO.description as description,p.*
                            FROM `product_option_groups` p
                            JOIN(
                            SELECT t.`id` AS client_id
                            , @pv:=t.`parent_id` AS parent
                            , @rownum := @rownum + 1 AS rownum
                            FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
                            JOIN (SELECT @pv:= '.$client_id.', @rownum:=0)tmp
                            WHERE t.`id`=@pv
                        )c on c.`client_id` = p.`client_id`
                        LEFT JOIN `product_options` as PO ON `p`.`option_id` = `PO`.`id`
                        WHERE (`group_code` = "' . $pro . '" AND `p`.`status`="active")
                        ORDER BY p.`order` ASC, c.`rownum`'));

                        $count = count($Defects);
                        Session::put('Defects', $order);
                        $arr = array('name' => 'Defects', 'item' => $Defects, 'count' => $count);
                        return $arr;
                    }
                } else if (strpos($a, 'Services') !== false) {

                    $client_id = Session::get('client_id');

                    $pro = $value->group_code;
                    if ($pro != '') {
                        $Services = DB::select(DB::raw('SELECT PO.description as description,p.*
                            FROM `product_option_groups` p
                            JOIN(
                            SELECT t.`id` AS client_id
                            , @pv:=t.`parent_id` AS parent
                            , @rownum := @rownum + 1 AS rownum
                            FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
                            JOIN (SELECT @pv:= '.$client_id.', @rownum:=0)tmp
                            WHERE t.`id`=@pv
                        )c on c.`client_id` = p.`client_id`
                        LEFT JOIN `product_options` as PO ON `p`.`option_id` = `PO`.`id`
                        WHERE (`group_code` = "' . $pro . '" AND `p`.`status`="active")
                        ORDER BY p.`order` ASC, c.`rownum`'));

                        $count = count($Services);
                        $arr = array('name' => 'Services', 'item' => $Services, 'count' => $count);
                        return $arr;
                    }   
                }               
            }
        }
        return $arr;
    }

    public function modelSearch(Request $request) {
        if ($this->filter($this->permission)) {
            //validate inputs
            if(!isset($request->pid) || !is_numeric($request->pid)){
                Toast::error('invalid model specified.', 'Error');
                return redirect('/dashboard');
            }

            $client_id = Session::get('client_id');
            $clinetname = Client::find($client_id);
            $proModal = Product::where('model',$request->model)->first();

            //validate objects and user permission 
            if(null === $proModal || null === $clinetname || !$this->isUserValid($client_id)){
                Toast::error('You do not have permission.', 'Error');
                return redirect('/dashboard');
            }

            $data = array(
                'model_id' => $request->pid
            );

            return $this->Step01($request, $data);

        }else {
            Toast::error('You do not have permission.', 'Error');
            return redirect('/admin/ashboard');
        }
    }
    public function SerialNumserSearch(Request $request) {
        // is user logged in
        if (!$this->filter($this->permission)) {
            Toast::error('You do not have the correct permissions.', 'Error');
            return redirect('/dashboard');
        }

        //value inputs
        if(!isset($request->pid) || !is_numeric($request->pid) || $request->pid <= 0 || !isset($request->serial_number) || $request->serial_number == ''){
            Toast::error('The serial number is invalid.', 'Error');
            return redirect('/quote/step');
        }

        $client_id = 0;
        if(Session::has('client_id')){
            $client_id = Session::get('client_id');
        } 
        if($client_id == 0 && Session::has('enterprise_client_id')){
            $client_id = Session::get('enterprise_client_id');
        } 

        //validate user has permission against this client
        if(!$this->isUserValid($client_id)) { 
            Toast::error('You do not have the correct permissions.', 'Error');
            return redirect('/dashboard');
        }

        //validate and retreive PUP details
        $pup = PupContract::where('serial_number', '=', $request->serial_number)->where('id', '=', $request->pid)->first();

        if(null === $pup){
            Toast::error('The serial number is invalid.', 'Error');
            return redirect('/quote/step');
        }


        if(strtolower($pup->status) !== 'valid'){
            Toast::error('The PUP contract is no longer valid.', 'Error');
            return redirect('/quote/step');
        }

        //compare date to now to get number of months.
        $date_pup = (new DateTime($pup->date))->format('Y-m-d H:i:s');
        $date_now = (new DateTime('now'))->format('Y-m-d H:i:s');

        $months = (int)abs((strtotime($date_pup) - strtotime($date_now))/(60*60*24*30));

        $args = Array(
            'serial' => $request->serial_number,
            'mfrom' => $months,
            'mto'   => $months
        );
        $sql = "SELECT * FROM `pup_contracts_months` WHERE `serial_number` = :serial AND month_from <= :mfrom AND month_to >= :mto";
        $results = DB::select(DB::raw($sql), $args);
        if(empty($results)){
            Toast::error('PUP Trade-in eligibility time expired!', 'Error');
            return redirect('/quote/step');         
        }       

        $pup_id = $pup->id;
        $pup_month_id = $results[0]->id;

        $data = array(
            'pup_id' => $pup_id,
            'pup_month_id' => $pup_month_id,
            'serial_number' => $request->serial_number
        );

        $model_id       = $pup->model_id;
        $modelname      = $pup->model;
        $type_id        = $pup->type_id;
        $brand_id       = ProductOption::where('type', '=', 'Brand')->where('name', '=', 'HP')->first()->id;
        $processor_id   = $pup->processor_id;
        $screen_id      = $pup->screen_id;
        $dvd_id         = $pup->dvd_id;
        $ram_id         = $pup->ram_id;
        $hdd_id         = $pup->hdd_id;

        $PO = array('type'=>'Defects');

        $EditSteps = $this->getSteps($type_id, $brand_id);
        $EditlastStep = '';
        if(!empty($EditSteps)){
            foreach ($EditSteps as $key => $value) {
                if(strpos($value->group_code,$PO['type']) !== false){
                    $EditlastStep = $value->order - 1;
                }else if (strpos($value->group_code, 'Screen') !== false || strpos($value->group_code, 'HDD') !== false && strpos($value->group_code, 'RAM') !== false ||  strpos($value->grop_code, 'DVD') !== false ){
                    $EditlastStep = $value->order - 1;
                }
            }
        }   

        $arr = array(
            'type_id'       => $type_id,
            'brand_id'      => $brand_id,
            'EditlastStep'  => $EditlastStep,
            'SearchType'    => $PO['type'],
            'editMode'      => 'editMode',
            'processor_id'  => $processor_id,
            'screen_id'     => $screen_id,
            'dvd_id'        => $dvd_id,
            'hdd_id'        => $hdd_id,
            'ram_id'        => $ram_id,
            'services_id'   => 0,
            'pup_id'        => $pup_id,
            'pup_sn'        => $request->serial_number,
            'modelname'     => $modelname,
            'model_id'      => $model_id
        );

        $json = json_encode($arr);
        setcookie('clientdata', $json, time() + (86400), "/");

        $Products = array();
        $clinetname = Client::find($client_id);
        return view('hpUser/step-01', Array(
            'products'          => $Products,
            'clinetname'        => $clinetname,
            'modal'             => '',
            'client_id'         => $client_id,
            'QuoteItemDetail'   => ''
        ));
    }

    public function step01(Request $request, $searcharray = null) {
               
        if ($this->filter($this->permission)) {
            $modal = $request->get('modal');
            $client_id = (isset($request->client_id) && is_numeric($request->client_id) && $request->client_id > 0 ) ? $request->client_id : 0;;
              //dd($client_id);
            if(Session::has('client_id') && is_numeric(Session::get('client_id'))){
                $client_id = Session::get('client_id');
            }

            if(!$this->isUserValid($client_id)){
                Toast::error('You do not have permission for this client.', 'Error');
                return redirect('/dashboard');
            }

            if (isset($request->Quote_Item_Id) && $request->Quote_Item_Id != 0) {
                $quoteItemId = $request->Quote_Item_Id;
                if ($quoteItemId != '') {
                    $QuoteItemDetail = QuoteItem::find($quoteItemId);
                } else {
                    $quoteItemId = '';
                }
            } else {
                $quoteItemId = '';
            }

            //=========================== Code for Modeal Search ==================================//
            if(isset($searcharray) &&  $searcharray['model_id'] > 0){
                //retreive product detail
                $productitem = Product::find($searcharray['model_id']);
                if(null !== $productitem){
                    $modelname = $productitem->model;
                    $type_id = $productitem->type_id;
                    $brand_id = $productitem->brand_id;
                    $processor_id = $productitem->processor_id;
                    $screen_id = $productitem->screen_id;
                    $dvd_id = $productitem->dvd_id;
                    $ram_id = $productitem->ram_id;
                    $hdd_id = $productitem->hdd_id;

                    $EditSteps = $this->getSteps($productitem->type_id, $productitem->brand_id);

                    $EditlastStep = '';
                    $missingdata = 'Services';
                    if(!empty($EditSteps)){
                        $num = count($EditSteps);
                        while($num > 0){                        
                            $value = $EditSteps[$num - 1];                      
                            $num --;

                            $arrstep = explode(',', strtolower($value->group_code));
                            $arrstep = array_reverse($arrstep);
                            foreach ($arrstep as $val) {
                                if (strpos($val, 'processor') !== false && $processor_id == 0){                             
                                    $EditlastStep = $value->order - 1;
                                    $missingdata = "Processor";
                                } else if (strpos($val, 'screen') !== false && $screen_id == 0){
                                    $EditlastStep = $value->order - 1;
                                    $missingdata = "Screen Size";
                                } else if (strpos($val, 'hdd') !== false && $hdd_id == 0) {
                                    $EditlastStep = $value->order - 1;
                                    $missingdata = "HDD";
                                } else if (strpos($val, 'ram') !== false && $ram_id == 0){
                                    $EditlastStep = $value->order - 1;
                                    $missingdata = "RAM";
                                } if (strpos($val, 'dvd') !== false && $dvd_id == 0){   
                                    $EditlastStep = $value->order - 1;
                                    $missingdata = "DVD";
                                } else if (strpos($val, 'defects') !== false && $missingdata == 'Services'){
                                    $EditlastStep = $value->order - 1;
                                    $missingdata = "Defects";
                                } else if (strpos($val, 'services') !== false && $missingdata == 'Services'){
                                    $EditlastStep = $value->order - 1;
                                }
                            }
                        }
                    }

                    $PO = array('type' => $missingdata);

                    $arr = array(
                        'type_id'       => $type_id,
                        'brand_id'      => $brand_id,
                        'SearchType'    => $PO['type'],
                        'SearchId'      => '',
                        'SearchName'    => '',
                        'EditlastStep'  => $EditlastStep,
                        'editMode'      => 'editMode',
                        'processor_id'  => $processor_id,
                        'screen_id'     => $screen_id,
                        'dvd_id'        => $dvd_id,
                        'hdd_id'        => $hdd_id,
                        'ram_id'        => $ram_id,
                        'services_id'   => '',
                        'Defects'       => '',
                        'modelname'     => $modelname,
                        'model_id'      => $searcharray['model_id']
                    );

                    $json = json_encode($arr);
                    setcookie('clientdata', $json, time() + (86400), "/");
                }
            } else {
                setcookie('clientdata', '', -10, "/");
                setcookie('brand_id', '', -10, "/");
                setcookie('SearchType', '', -10, "/");
                setcookie('SearchId', '', -10, "/");
                setcookie('SearchName', '', -10, "/");
                setcookie('EditlastStep', '', -10, "/");
                setcookie('editMode', '',-10, "/");
            }
            //=========================== Code for Edit Quote ==================================//

            if (isset($client_id)) {

                $clinetname = Client::find($client_id);
                if (!empty($clinetname)) {

                    $Products = array();
                    $resultset = DB::select(DB::raw("SELECT p.id, p.`type`, p.`name`, gp.`order`, p.`description`, p.`image`, gp.`status`
                        FROM `product_options` p
                        JOIN `product_option_groups` gp on gp.`option_id` = `p`.`id`
                        JOIN(
                        SELECT t.`id` AS client_id, @pv:=t.`parent_id` AS parent, @rownum := @rownum
                        + 1 AS rownum
                        FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
                        JOIN (SELECT @pv:= '$client_id', @rownum:=0)tmp
                        WHERE t.`id`=@pv
                        )c on c.`client_id` = gp.`client_id`
                        WHERE p.`type` =  'Product' AND p.`status` = 'active'
                        ORDER BY p.`id`, c.`rownum`"));

                    $previd = 0;
                    foreach($resultset as $brand){
                        if($brand->id != $previd){
                            if($brand->status == 'active'){
                                array_push($Products, $brand);
                            }
                        }
                        $previd = $brand->id;
                    }
                    usort($Products, function ($a, $b) {
                        return $a->order - $b->order;
                    });

                    return view('hpUser/step-01', array(
                        'products' => $Products,
                        'clinetname' => $clinetname,
                        'modal' => $modal,
                        'client_id' => $client_id,
                        'QuoteItemDetail' => @$QuoteItemDetail
                    ));

                } else {
                    Toast::error('Invalid client Id !!!.', 'Error');
                    return redirect('/dashboard');
                }
            } else {
                Toast::error('Invalid client Id !!!.', 'Error');
                return redirect('/dashboard');
            }
        }else {
            return  redirect()->back();
           
        }    
    }

    public function step02(Request $request) {
        if ($this->filter($this->permission)) {

            $client_id = 0;
            if(Session::has('client_id') && is_numeric(Session::get('client_id'))){
                $client_id = Session::get('client_id');
            }

            if(!$this->isUserValid($client_id)){
                Toast::error('You do not have permission for this client.', 'Error');
                return redirect('/dashboard');
            }

            $id = $request->get('id');
            $brand_id = array();

            if ($request->get('id') != null) {
                $clinetname = Client::find($client_id);

                $product = ProductOption::find($id);

                $args = array(
                    'client'    => $client_id,
                    'product'   => $id
                );

                $results = array();
                $resultset = DB::select(DB::raw("SELECT p.id, p.`type`, p.`name`, gp.`order`, p.`description`, p.`image`, gp.`status`
                    FROM `product_options` p
                    JOIN `product_option_groups` gp on gp.`option_id` = `p`.`id`
                    JOIN `product_options_link` pol on pol.`brand_id` = `p`.`id`
                    JOIN(
                    SELECT t.`id` AS client_id, @pv:=t.`parent_id` AS parent, @rownum := @rownum
                    + 1 AS rownum
                    FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
                    JOIN (SELECT @pv:= :client, @rownum:=0)tmp
                    WHERE t.`id`=@pv
                    )c on c.`client_id` = gp.`client_id`
                    WHERE p.`type`= 'Brand' AND p.`status` = 'active' AND pol.type_id = :product
                    ORDER BY p.`id`, c.`rownum`"), $args);

                $previd = 0;
                foreach($resultset as $brand){
                    if($brand->id != $previd){
                        if($brand->status == 'active'){
                            array_push($results, $brand);
                        }
                    }
                    $previd = $brand->id;
                }

                usort($results, function ($a, $b) {
                    return $a->order - $b->order;
                });

                if (isset($request->quote_item_id) && $request->quote_item_id != 0) {
                    $quoteItemId = $request->quote_item_id;
                    if ($quoteItemId != '') {
                        $QuoteItemDetail = QuoteItem::find($quoteItemId);
                    } else {
                        $quoteItemId = '';
                    }
                } else {
                    $quoteItemId = '';
                }
                //echo "<pre>";print_r($results);die;
                $Product_id = $id;
                $html = View::make('/hpUser/step-02', compact('product', 'results', 'Product_id', 'QuoteItemDetail', 'quoteItemId'))->render();
                return Response::json(['html' => $html]);

            } else {
                $products = ProductOption::where('type', '=', 'Product')->where('status','active')->get();
                return view('hpUser/step-01', array(
                    'products' => $products,
                ));
            }
        }else {
            Toast::error('You do not have permission.', 'Error');
            return redirect('/dashboard');
        }    
    }

    public function totalSteps(Request $request) {
        // check if user logged in
        if (!$this->filter($this->permission)) {
            return '';
        } 

        $client_id = 0;
        if(Session::has('client_id') && is_numeric(Session::get('client_id'))){
            $client_id = Session::get('client_id');
        }

        // check if user has permission for this client and/or quote
        if(!$this->isUserValid(Session::get('client_id'))){
            return '';
        }

        $pro1 = $request->get('pro1');
        $pro2 = $request->get('pro2');
        $pro3 = $request->get('pro3');
        $pro4 = $request->get('pro4');
        $pro5 = $request->get('pro5');
        $pro6 = $request->get('pro6');
        $pro7 = $request->get('pro7');
        $pro8 = $request->get('pro8');
        $pro9 = $request->get('pro9');
        $lastStep = $request->get('lastStep');
        $modelid = $request->get('modelid');
        $pupid = (isset($request->pupid) && is_numeric($request->get('pupid'))) ? $request->get('pupid') : '';
        $pupsn = strip_tags($request->get('pupsn'));

        if($pro2 == null){
            $stepPro = $this->getStepsWithProcessorName($pro1, '1', $lastStep);
        }else{
            $stepPro = $this->getStepsWithProcessorName($pro1, $pro2, $lastStep);    
        }

        $Product = $Brand = $Processor = $Screen = $DVD = $HDD = $RAM = $Services = $ServiceDefault = '';
        $Defects = array();
        $Model = (object)array('id' => 0, 'name' => '');
        $PUP = (object)array('id' => $pupid, 'serialnumber' => $pupsn);

        if (isset($modelid) && is_numeric($modelid) && $modelid > 0) {
            $Model = Product::find($modelid);
        }

        if (isset($pro1) && $pro1 != 0) {
            $Product = ProductOption::find($pro1);
        }
        if (isset($pro2) && $pro2 != 0) {
            $Brand = ProductOption::find($pro2);
        }

        if($pro2 == null){
            $stepsProgress = $this->getSteps($pro1,1);
        }else{
            $stepsProgress = $this->getSteps($pro1, $pro2);    
        }

        if (isset($pro3) && $pro3 != 0) {
            $Processor = ProductOption::find($pro3);
            $Processor->current = Session::get('Processor');
            Session::flash('Processor_old',$Processor->id);
        }
        if (isset($pro4) && $pro4 != 0) {
            $Screen = ProductOption::find($pro4);
            $Screen->current = Session::get('Screen');
            Session::flash('Screen_old',$Screen->id);
        }
        if (isset($pro5) && $pro5 != 0) {
            $DVD = ProductOption::find($pro5);
            $DVD->current = Session::get('DVD');
            Session::flash('DVD_old',$DVD->id);
        }
        if (isset($pro6) && $pro6 != 0) {
            $HDD = ProductOption::find($pro6);
            $HDD->current = Session::get('HDD');
            Session::flash('HDD_old',$HDD->id);
        }
        if (isset($pro7) && $pro7 != 0) {
            $RAM = ProductOption::find($pro7);
            $RAM->current = Session::get('RAM');
            Session::flash('RAM_old',$RAM->id);
        }
        if (isset($pro8) && $pro8 != 0) {

            $ids = explode(",", $pro8);

            $Defects = ProductOption::whereIn('id', $ids)->get();
            $Defects->current = Session::get('Defects');

            Session::flash('Defects_Old',$Defects);
        }

        if (isset($pro9) && is_numeric($pro9) && $pro9 > 0) {
            $Services = ProductOption::find($pro9);
            Session::flash('services_old', $Services->id);
        } else {
            // check for default service fee (need product type and brand to get step)
            // support only eixsts for services default 
            if (isset($pro1) && is_numeric($pro1) && $pro1 > 0 && isset($pro2) && is_numeric($pro2) && $pro2 > 0) {
                if(Session::has('ServiceDefault') && is_numeric(Session::get('ServiceDefault'))){
                    // retreive the default service fee
                    $ServiceDefault = Session::get('ServiceDefault');
                } else {
                    if(null !== $stepsProgress && count($stepsProgress) > 0){
                        //check if there is a default service fee

                        $args = array(
                            'client'    => $client_id,
                            'stepid'    => $stepsProgress[0]->id
                        );                      

                        $resultset = DB::select(DB::raw('SELECT * 
                            FROM `quote_step_defaults` sd
                            JOIN(
                            SELECT t.`id` AS client_id
                            , @pv:=t.`parent_id` AS parent
                            , @rownum := @rownum + 1 AS rownum
                            FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
                            JOIN (SELECT @pv:= :client, @rownum:=0)tmp
                            WHERE t.`id`=@pv
                            )c ON sd.client_id = c.client_id
                            WHERE sd.step_id = :stepid
                            ORDER BY rownum LIMIT 1'), $args);
                        if(!empty($resultset)){
                            $ServiceDefault = $resultset[0]->option_id;
                            Session::put('ServiceDefault', $ServiceDefault);
                        }
                    }
                }
            }

            if(is_numeric($ServiceDefault) && $ServiceDefault > 0){
                $Services = ProductOption::find($ServiceDefault);
                Session::flash('services_old', $Services->id);
            }
        }

        $QuoteItemDetail = '';
        $QID = array();

        if (isset($request->quote_item_id) && $request->quote_item_id != 0) {
            $quoteItemId = $request->quote_item_id;
            if ($quoteItemId != '') {
                $QuoteItemDetail = QuoteItem::find($quoteItemId);
                $QuoteItemDefects = QuoteItemsDefect::where('quote_item_id', $quoteItemId)->get();
                foreach ($QuoteItemDefects as $QuoteItemDefect) {
                    $QID[] = $QuoteItemDefect->option_id;
                }
            } else {
                $quoteItemId = '';
            }
        } else {
            $quoteItemId = '';
        }

        $lastStep = isset($stepPro['order']) ? $stepPro['order'] : $request->get('lastStep');
        @$role = RoleUser::where('user_id',Auth::user()->id)->first()->role_id;
        @$permissions = Auth::user()->permission;
        @$permission =  explode(',', @$permissions);
        $args = array(
            'Product' => $Product,
            'Brand' => $Brand,
            'Processor' => $Processor,
            'Screen' => $Screen,
            'DVD' => $DVD,
            'HDD' => $HDD,
            'RAM' => $RAM,
            'Defects' => $Defects,
            'Services' => $Services,
            'stepPro' => $stepPro,
            'stepsProgress' => $stepsProgress,
            'lastStep' => $lastStep,
            'QuoteItemDetail' => $QuoteItemDetail,
            'QID' => $QID,
            'permission' =>$permission,
            'role'=>$role,            
            'Model' => $Model,
            'isServiceDefault' => (Session::has('ServiceDefault') && is_numeric(Session::get('ServiceDefault'))) ? Session::get('ServiceDefault') : 0,
            'PUP' => $PUP
        );

        if (count($stepPro) > 0) {
            $html = View::make('/hpUser/step-03', $args)->render();            
        } else {
            $html = View::make('/hpUser/step-10', $args)->render();            
        }
        return Response::json(['html' => $html]);
    }

    public function hpDashboard(Request $request) {
       if (!$this::filter($this->permission)){
            
            return  redirect()->back();
            
        }

        $RoleUser = RoleUser::where('user_id',Auth::user()->id)->first();

        if($RoleUser->role_id == 1){

            return redirect('/admin/dashboard');
        }else{
            date_default_timezone_set('Asia/Kolkata');

            if (Auth::user()->id != '') {
                $id = Auth::user()->id;         

                // the current user and list of users created by the current user
                $a = User::where('created_by',$id)->orWhere('id', $id)->pluck('id')->toArray();

                $users = array();
                $quote = array();

                // retrieve the current users profile
                $user = User::find($id);                
                // retrieve the current users role
                $RoleUser = RoleUser::select('role_id')->where('user_id', '=', $id)->first();

                //returns a list of clients the user has permissions for
                $ClientUsers = ClientUser::with('clients', 'clientss')
                ->where('user_id', $id)
                ->leftjoin('clients', 'client_users.client_id', '=', 'clients.id')
                ->orderBy('clients.fullpath', 'asc')                        
                ->get();

                //returns an array  of client id the user has permissions for to filter quotes by
                $clientids = ClientUser::where('user_id', $id)->pluck('client_id')->toArray();      

                $sqlbit = "";

                if ($RoleUser->role_id == 3) {
                    // role is just a public user therefore they can only see quote they have made.
                    $quote = QuoteManagement::with('Clientname')->where('user_id', '=', $id)->groupBy('client_id')->select('client_id', DB::raw('count(*) as total'))->get();
                    $sqlbit = " AND q.user_id = " . $id;
                } else {
                    // role is manager or admin therefore can see everything they have access to
                    $users = User::where('created_by', '=', Auth::user()->id)->paginate(5);
                    $quote = QuoteManagement::with('Clientname')->whereIn('client_id', $clientids)->groupBy('client_id')->select('client_id', DB::raw('count(*) as total'))->get();
                    $sqlbit = " AND q.client_id IN (SELECT client_id FROM client_users WHERE user_id = " . $id . ")";
                }

                $sql = "SELECT SUM(total_value) AS `total_value`, SUM(total_items) AS `total_items`, `month`, ss AS `status`
                FROM (
                SELECT 
                CASE WHEN q.date_created > NOW() - INTERVAL 30 DAY THEN 'current' ELSE 'last' END AS 'month',
                CASE WHEN c.client_type = 'store' THEN 2 ELSE q.status END AS ss,                   
                q.status,
                c.client_type,
                q.total_value,
                q.total_items
                FROM `quote` q
                INNER JOIN `clients` c ON q.client_id = c.id
                WHERE q.date_created BETWEEN NOW() - INTERVAL 60 DAY AND NOW() + INTERVAL 1 DAY
                AND q.`status` != 7
                " . $sqlbit . " 
            )tmp
            GROUP BY `month`, `ss` ORDER BY `ss`, `month`";
            $results = DB::select(DB::raw($sql));
            $status_1_current = 0;
            $status_1_last = 0;
            $status_2_current = 0;
            $status_2_last = 0;
            $status_6_current = 0;
            $status_6_last = 0;
            if(!empty($results) && count($results) > 0){
                foreach($results AS $qv){
                    if($qv->status == 1 && $qv->month == 'current') { $status_1_current = $qv->total_value;}
                    if($qv->status == 1 && $qv->month == 'last')    { $status_1_last    = $qv->total_value;}
                    if($qv->status == 2 && $qv->month == 'current') { $status_2_current = $qv->total_value;}
                    if($qv->status == 2 && $qv->month == 'last')    { $status_2_last    = $qv->total_value;}
                    if($qv->status == 6 && $qv->month == 'current') { $status_3_current = $qv->total_value;}
                    if($qv->status == 6 && $qv->month == 'last')    { $status_3_last    = $qv->total_value;}
                }
            }
            $quotevalues = array();
            $quotevalues[] = array('status' => 'In Progress', 'CurrentMonth' => $status_1_current, 'LastMonth' => $status_1_last);
            $quotevalues[] = array('status' => 'Completed', 'CurrentMonth' => $status_6_current, 'LastMonth' => $status_6_last);
            $quotevalues[] = array('status' => 'Accepted', 'CurrentMonth' => $status_2_current, 'LastMonth' => $status_2_last);
            
            /*===============================Permission==========================*/
            $role_id = RoleUser::where('user_id',Auth::user()->id)->first()->role_id;
            @$permissions = Auth::user()->permission;
            $permission =  explode(',', @$permissions);
            /*===============================Permission==========================*/         

            /*===============================Collection Request===================*/
            $Quotes = Quote::with('Clientname')->whereIn('client_id', $clientids)->where('status', 4)->groupBy('client_id')->select('client_id', DB::raw('count(*) as total'))->paginate(10);
            /*===============================Collection Request===================*/

            return view('/hpUser.hpDashboard', compact('user', 'totalQuote', 'users', 'role', 'ClientUsers', 'quote', 'RoleUser', 'Quotes', 'role_id', 'permissions', 'permission', 'quotevalues'));
        } else {

            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect('/login');
        }
    }    
}

public static function client(Request $Request) {

    //dd($Request);
    $client = Client::find($Request->client_name);
    

    if ($client->client_type == 'store') {
        Session::put('client_id', $client->id);
        return redirect('/store/quote/step');
    } else {
            //dd($client);
        Session::flash('enterprise_client_id', $client->id);
        return redirect('/enterprises/quote/settings');

    } 

    if (!$this::filter($this->permission)){
        Toast::success("Authentication failed:You don't have permissions.", 'Error');
        Auth::logout();
        return redirect('/login');
    } 

}

/*retreive the current users profie details for them to edeit*/
public function editHpDashboard() {


    if ($this->filter($this->permission)) {

        $user = User::find(Auth::user()->id);
        if(null === $user){

            Toast::error('Unable to retrieve you profile information.', 'Error');
            return redirect('/dashboard');
        }
        return view('/hpUser.editHpDashboard', compact('user'));
    }else{

       
        return redirect()->back();
   }

}

/* update the details for the logged in users */
public function updatehpDashboard(Request $request) {
    
    //check that the current user has permissions to edit user id specified
    $id = $request->id;
    if(empty($id) || !is_numeric($id) || $id <= 0){
        Toast::error('Invalid user specified.', 'Error');
        return redirect('/dashboard');
    }
        // get currect user id and check that the updated user can be edited by the loggedin user 
        // i.e. must be a manger who create the user or who profile is assigned to a user the manager has permissions on
    $user_id = Auth::user()->id;
    if($user_id != $id){
        Toast::error('Invalid user specified.', 'Error');
        return redirect('/dashboard');
    }

    $User = User::find($id);
    if (null === $User || empty($User)) {
        Toast::error('Invalid user specified.', 'Error');
        return redirect('/dashboard');
    }   

   
    if (isset($request->password) && !empty($request->password)) {
        $this->validate($request, array(
            'password' => 'required|min:3',
            'confirm_password' => 'required|same:password'
        ));
        $User->password = bcrypt($request->get('password'));
    }

    if ($User->save()) {
        Session::flash('message', 'Record has been updated.');
        return Redirect('/edit_profile');
    } else {
        Toast::error('User not updated, Please try again.', 'Error');
        return Redirect('/dashboard');
    }        
}

public function getQuoteTotal(Request $request) {
        //validate  user is logged in
    if (!$this->filter($this->permission)) {
        Toast::error('You do not have permission.', 'Error');
        return redirect('/dashboard');  
    }

    $calculate = new CalculationsController();
    $userID = Auth::user()->id;
    $clntid = (isset($request['client_id']) && is_numeric($request['client_id']) && $request['client_id'] > 0) ? $request['client_id'] : 0;

        //permission check
    if($clntid == 0 || !$this->isUserValid($clntid)){
        $response = array('status'=>'ProductIdIssue', 'message'=>'We are unable to progress with your request at this time.');
        echo json_encode($response);
        die;
    }

    $isPUP = FALSE;
    $pup = null;
        //is it a pup quote? if so then validate pup details
    if(isset($request['pupid']) && is_numeric($request['pupid']) && $request['pupid'] > 0){
            //validate and retreive PUP details
        $pup = PupContract::where('serial_number', '=', $request->pupsn)->where('id', '=', $request->pupid)->where('status', '=', 'Valid')->first();

        if(null === $pup){
            $response = array('status'=>'ProductIdIssue', 'message'=>'The Serial Number for the PUP contract is no longer valid! Please contact the administrator');
            echo json_encode($response);die;
        }

            //compare date to now to get number of months.
        $date_pup = (new DateTime($pup->date))->format('Y-m-d H:i:s');
        $date_now = (new DateTime('now'))->format('Y-m-d H:i:s');

        $months = (int)abs((strtotime($date_pup) - strtotime($date_now))/(60*60*24*30));
        
        $args = Array(
            'serial' => $request->pupsn,
            'mfrom' => $months,
            'mto'   => $months
        );
        $sql = "SELECT * FROM `pup_contracts_months` WHERE `serial_number` = :serial AND month_from <= :mfrom AND month_to >= :mto";
        $results = DB::select(DB::raw($sql), $args);
        if(empty($results)){
            $response = array('status'=>'ProductIdIssue', 'message'=>'The PUP contract is no longer valid for redemption! Please contact the administrator');
            echo json_encode($response);die;        
        }   
        $isPUP = TRUE;
    }

        // validate and populate varaibles from request object 
    $obj = $this->validateOption($request['Product'], 'Product');
    $type_id = $obj["id"];
    $type_name = $obj["name"];
    $obj = $this->validateOption($request['Brand'], 'Brand');
    $brand_id = $obj["id"];
    $brand_name = $obj["name"];
    $obj = $this->validateOption($request['Processor'], 'Processor');
    $processor_id = $obj["id"];
    $processor_name = $obj["name"];
    $obj = $this->validateOption($request['Screen'], 'Screen Size');
    $screen_id = $obj["id"];
    $screen_name = $obj["name"];
    $obj = $this->validateOption($request['DVD'], 'DVD');
    $dvd_id = $obj["id"];
    $dvd_name = $obj["name"];
    $obj = $this->validateOption($request['HDD'], 'HDD');
    $hdd_id = $obj["id"];
    $hdd_name = $obj["name"];
    $obj = $this->validateOption($request['RAM'], 'RAM');
    $ram_id = $obj["id"];
    $ram_name = $obj["name"];
    $obj = $this->validateOption($request['Services'], 'Services');
    $services_id = $obj["id"];
    $services_name = $obj["name"];
    $model_id = 0;
    $model = '';
    $product_id = 0;

    if(isset($request['modelid']) && is_numeric($request['modelid']) && $request['modelid'] > 0){
        $mobj = Product::find($request['modelid']);
        if(null !== $mobj){
            $model_id = $mobj->id;
            $model = $mobj->model;
        }
    }

        // return step information
    $stepsProgress = $this->getSteps($type_id, $brand_id);
    $step_id = 0;
    if(null !== $stepsProgress && count($stepsProgress) > 0){
        $step_id = $stepsProgress[0]->id; 
    }

    $clientsData = Client::find($clntid);

        // find the product matching the specified configuration  
    $re = $calculate->productId($clntid, $type_id, $brand_id, $processor_id, $screen_id, $hdd_id, $dvd_id, $ram_id, 1, $model_id, isset($clientsData->country) ? $clientsData->country : 0);
    if($re){
        $product_id = $re[0]->id;
        if(!empty($re[0]->model) && $model_id == 0){
            $model = $re[0]->model;
            $model_id = $product_id;
        }
    }else{
            //if the quote is for a PUP item then proceed even if not product was found
        if(!$isPUP){
            $response = array('status'=>'ProductIdIssue', 'message'=>'You can not create quote for this client due to no relavent data found in product table !!!');
            echo json_encode($response);die;
        }
    }

    $client_collect = $calculate->getClientData($clntid, 'ave_collect_size');
    $client_oemp    = $calculate->getClientData($clntid, 'margin_oem_product');
    $client_oeml    = $calculate->getClientData($clntid, 'margin_oem_logistic');
    $client_oems    = $calculate->getClientData($clntid, 'margin_oem_service');

    $ExcRate = 1;
    $ExcSymbol = '$';

    if(isset($clientsData->country)){
        $Country = Country::find($clientsData->country);
        if(null !== $Country){           

            $ExcSymbol = $Country->currency_symbol;
                //check if there a cursomter speciic exchange rate
            $args = array(
                'client'    => $clntid,
                'country'   => $Country->id
            );
            
            $results = DB::select(DB::raw("SELECT * 
                FROM `client_country_rates` ccr
                JOIN(
                SELECT t.`id` AS client_id, @pv:=t.`parent_id` AS parent, @rownum := @rownum + 1 AS rownum
                FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
                JOIN (SELECT @pv:= :client, @rownum:=0)tmp
                WHERE t.`id`= @pv
                )c on c.`client_id` = ccr.`client_id`
                WHERE country_id = :country AND (expires IS NULL OR expires > now())
                ORDER BY c.`rownum`
                LIMIT 1"), $args);
            if(!empty($results) && isset($results[0]->exchangerate)){
                $ExcRate = $results[0]->exchangerate;
            } else {
                $ExcRate =  $Country->currency_rate;
            }
        }
    }

    $quote = new Quote;
    $quote->client_id = $clntid;
    $quote->user_id = $userID;
    $quote->country = $clientsData->country;
    $quote->currency_symbol = $ExcSymbol;
    $quote->currency_rate = $ExcRate;

    if($isPUP){
        $quote->margin_oem_product = $pup->margin;
        $quote->margin_oem_logistic = $pup->margin;
        $quote->margin_oem_service = $pup->margin;
    }else{
        $quote->margin_oem_product = $client_oemp;
        $quote->margin_oem_logistic = $client_oeml;
        $quote->margin_oem_service = $client_oems;
    }

    $quote->region              = $clientsData->region;
    $quote->ave_collect_size    = $client_collect;    
    $quote->total_value         = 0;
    $quote->total_items         = 0;
    $quote->status              = 0;
    $name = 'Quote_';

    if (isset($request['proccessor_name'])) {
        $name .= $request['proccessor_name'];
    }

    if (isset($request['brand_name'])) {
        $name .= $request['brand_name'];
    }

    if (isset($request['screen_name'])) {
        $name .= $request['screen_name'];
    }

    $quote->name = $name;
    $quote->save();
    $q_id = $quote->id;
    $q_name = $quote->name;
    $statuss = $quote->status;

    $quoteItem = new QuoteItem;
    $quoteItem->quote_id        = $quote->id;
    $quoteItem->status          = 8;
    $quoteItem->quantity        = 1;
    $quoteItem->type_id         = $type_id;
    $quoteItem->type_name       = $type_name;
    $quoteItem->brand_id        = $brand_id;
    $quoteItem->brand_name      = $brand_name;
    $quoteItem->processor_id    = $processor_id;
    $quoteItem->processor_name  = $processor_name;
    $quoteItem->screen_id       = $screen_id;
    $quoteItem->screen_name     = $screen_name;
    $quoteItem->dvd_id          = $dvd_id;
    $quoteItem->dvd_name        = $dvd_name;
    $quoteItem->hdd_id          = $hdd_id;
    $quoteItem->hdd_name        = $hdd_name;
    $quoteItem->ram_id          = $ram_id;
    $quoteItem->ram_name        = $ram_name;
    $quoteItem->services_id     = $services_id;
    $quoteItem->services_name   = $services_name;
    $quoteItem->product_id      = $product_id;
    $quoteItem->model_id        = $model_id;
    $quoteItem->model_name      = $model;
    $quoteItem->step_id         = $step_id;
    $quoteItem->use_committed_price     = 0;        
    $quoteItem->is_model_price          = ($product_id == $model_id) ? 1 : 0;
    if($isPUP){
        $quoteItem->isPUP                   = $pup->id;
        $quoteItem->serial_number           = $pup->serial_number;
        $quoteItem->logistics_oem_margin    = $pup->margin;
        $quoteItem->services_oem_margin     = $pup->margin;
        $quoteItem->unit_margin_oem_product = $pup->margin;
        $quoteItem->unit_margin_committed   = $pup->margin;
    } else {
        $quoteItem->isPUP                   = 0;
        $quoteItem->unit_margin_committed   = 0;
        $quoteItem->logistics_oem_margin    = $client_oeml;
        $quoteItem->services_oem_margin     = $client_oems;
        $quoteItem->unit_margin_oem_product = $client_oemp;
    }
    $quoteItem->save(); 

    if($isPUP){
            // set the pup item to no longer be valid
            //$pup->status = 'Redeemed';
            //$pup->save();
    }

    $quote_item_id = $quoteItem->id;

    QuoteItemsDefect::where('quote_item_id', $quote_item_id)->where('quote_id', $quote->id)->delete();

    if (count($request['Defects']) > 0) {
        foreach ($request['Defects'] as $key => $value) {
            $obj = $this->validateOption($value, 'Defects');
            if($obj["id"] > 0){
                $quoteItemDefect = new QuoteItemsDefect;
                $quoteItemDefect->quote_id      = $quote->id;
                $quoteItemDefect->defect_id     = 0;
                $quoteItemDefect->defect_name   = $obj["name"];
                $quoteItemDefect->defect_value  = 0;
                $quoteItemDefect->quote_item_id = $quoteItem->id;
                $quoteItemDefect->option_id     = $obj["id"];
                $quoteItemDefect->save();            
            }               
        }
    }

    $calculate->Recalculate($q_id);

        //reload item to get margin detail for output
    $quotes = QuoteItem::find($quoteItem->id);
    $quoteTable = Quote::find($quote->id);

    if (isset($results['total_price'])) {
        if ($results['total_price'] < 0) {
            $total = $results['total_price'] * (-1);
        } else {
            $total = $results['total_price'];
        }
    } else {
        $total = '$0';
    }

    $lastClient = $clientsData->name;
    $client_id  = $clientsData->id;

    if (isset($results['unit_price'])) {
        if ($results['unit_price'] < 0) {
            $ttgToHp = $results['unit_price'] * (-1);
        } else {
            $ttgToHp = $results['unit_price'];
        }
    } else {
        $ttgToHp = 0;
    }

        //=============TTG TO HP ==============//
    $qty                    = $quotes->quantity;
    $unitp                  = $quotes->unit_prod_price;
    $services               = $quotes->unit_f5_services;
    $logistic               = $quotes->unit_f5_logistics;
    $net_price              = $quotes->unit_prod_price - $quotes->unit_f5_services - $quotes->unit_f5_logistics;
    $total_price            = $net_price;        
    $margin_oem_product     = $quoteTable['margin_oem_product'];
    $margin_oem_logistic    = $quoteTable['margin_oem_logistic'];
    $margin_oem_service     = $quoteTable['margin_oem_service'];
        //============ TTG TO HP  =============//

        //============ Hp to Customer =============//
    $Product_Price_customer     = $quotes->unit_f5_price;
    $Service_Price_customer     = $quotes->services_net_price;
    $Logistic_Price_customer    = $quotes->logistics_net_price;
    $Sub_total                  = $quotes->total_price;
    $total_percent              = ($total_price != 0) ? ($total_price - $Sub_total) * 100.00 / $total_price : 0;
    $product_percent            = $quotes->unit_margin_oem_product;
    $logistic_percent           = $quotes->logistics_oem_margin;
    $service_percent            = $quotes->services_oem_margin;
    $totalmargin                = $total_price - $Sub_total;
        //============ Hp to Customer =============//

    /*===============================Permission==========================*/
    @$role = RoleUser::where('user_id',Auth::user()->id)->first()->role_id;
    @$permissions = Auth::user()->permission;
    @$permission =  explode(',', @$permissions);
    $flag = '';

    if(@$role == 2 || (@$role == 3 && @in_array(1,$permission)) ){
        $flag = true;
    }else{
        $flag = false;
    }
    /*===============================Permission==========================*/

        //clear any session varaible set for this quote
    Session::forget('step_id');
    Session::forget('ServiceDefault');


    return Response::json([
        'client_id'                 => $client_id, 
        'status'                    => 'success', 
        'total'                     => $total, 
        'ttgToHp'                   => $ttgToHp, 
        'lastClient'                => $lastClient, 
        'margin'                    => $total_percent, 
        'quote_id'                  => $q_id, 
        'quote_item_id'             => $quote_item_id, 
        'quote_name'                => $q_name,
        'statuss'                   => $statuss,
        'qty'                       => @$qty,
        'unitp'                     => $ExcSymbol . number_format($unitp * $ExcRate, 0, '.', ','),
        'services'                  => $ExcSymbol . number_format($services * $ExcRate, 0, '.', ','),
        'logistic'                  => $ExcSymbol . number_format($logistic * $ExcRate, 0, '.', ','),
        'net_price'                 => $ExcSymbol . number_format($net_price * $ExcRate, 0, '.', ','),
        'total_price'               => $ExcSymbol . number_format($total_price * $ExcRate, 0, '.', ','),
        'totalmargin'               => $ExcSymbol . number_format($totalmargin * $ExcRate, 0, '.', ','),
        'Product_Price_customer'    => $ExcSymbol . number_format($Product_Price_customer * $ExcRate, 0, '.', ','),
        'Logistic_Price_customer'   => $ExcSymbol . number_format($Logistic_Price_customer * $ExcRate, 0, '.', ','),
        'Service_Price_customer'    => $ExcSymbol . number_format($Service_Price_customer * $ExcRate, 0, '.', ','),
        'Sub_total'                 => $ExcSymbol . number_format($Sub_total * $ExcRate, 0, '.', ','),
        'productMarkup'             => $ExcSymbol . number_format(($unitp - $Product_Price_customer) * $ExcRate, 0, '.', ','),
        'seviceMarkup'              => $ExcSymbol . number_format(($Service_Price_customer - $services) * $ExcRate, 0, '.', ','),
        'logisticMarkup'            => $ExcSymbol . number_format(($Logistic_Price_customer - $logistic) * $ExcRate, 0, '.', ','),
        'totalMarkup'               => $ExcSymbol . number_format(($total_price - $Sub_total) * $ExcRate, 0, '.', ','),
        'total_percent'             => number_format($total_percent, 0) . '%',
        'product_percent'           => number_format($product_percent, 0) . '%',
        'logistic_percent'          => number_format($logistic_percent, 0) . '%',
        'service_percent'           => number_format($service_percent, 0) . '%',
        'flag' => $flag
    ]);
}

public function QuoteSummary($id = null) {
        //check if user has permissions
    if (!$this->filter($this->permission)) {
       return redirect()->back();
    }

    /* validate quote id ($id) is a number greater than zero */
    if ($id != null && !is_numeric($id) && $id <= 0) {
        Toast::error('Request is invalid', 'Error');
        return redirect('/dashboard');
    }

    $Quote = Quote::with('Clientname')->where('id', '=', $id)->first();

    if(null === $Quote || !$this->isUserValid($Quote->client_id)){
        Toast::error('Request is invalid', 'Error');
        return redirect('/dashboard');
    }

    if (isset($Quote->Clientname->client_type) && ($Quote->Clientname->client_type == 'enterprise' || $Quote->total_items > 1)) {
        Session::put('enterprise_client_id', $Quote->client_id);
        Session::put('quote_id', $Quote->id);
        return redirect('/enterprises/index');
    }

    $QuoteItem = QuoteItem::with('quote_name')->where('quote_id', '=', $id)->first();
    $status=$Quote->status;

    if($Quote->status > 3){
            //set default values for image varaible to a empty string*/
        $image1 = $image2 = $image3 = '';

        if(!empty($QuoteItem->image1)) {
            $path=public_path().$QuoteItem->image1;
            if(file_exists($path)){
                $image1=$QuoteItem->image1;
            }                        
        }

        if(!empty($QuoteItem->image2)) {
            $path=public_path().$QuoteItem->image2;
            if(file_exists($path)){
                $image2=$QuoteItem->image2;
            }
        }

        if(!empty($QuoteItem->image3)) {
            $path=public_path().$QuoteItem->image3;
            if(file_exists($path)){
                $image3=$QuoteItem->image3;
            }
        }
    }

    if (!empty($QuoteItem)) {
        $type = ProductOption::where('id', '=', $QuoteItem->type_id)->first();
        $brand = ProductOption::where('id', '=', $QuoteItem->brand_id)->first();
        $processor = ProductOption::where('id', '=', $QuoteItem->processor_id)->first();
        $screen = ProductOption::where('id', '=', $QuoteItem->screen_id)->first();
        $dvd = ProductOption::where('id', '=', $QuoteItem->dvd_id)->first();
        $ram = ProductOption::where('id', '=', $QuoteItem->ram_id)->first();
        $services = ProductOption::where('id', '=', $QuoteItem->services_id)->first();
        $hdd = ProductOption::where('id', '=', $QuoteItem->hdd_id)->first();
        $Quote = QuoteManagement::find($id);
        $Client = Client::find($Quote->client_id);
        $quote_status = QuoteStatus::find($Quote->status);

            //check if service was default or selected
        $isServiceDefault = 0;
        if($QuoteItem->step_id > 0){
            $args = array(
                'client'    => $Quote->client_id,
                'stepid'    => $QuoteItem->step_id
            );                      

            $resultset = DB::select(DB::raw('SELECT * 
                FROM `quote_step_defaults` sd
                JOIN(
                SELECT t.`id` AS client_id
                , @pv:=t.`parent_id` AS parent
                , @rownum := @rownum + 1 AS rownum
                FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
                JOIN (SELECT @pv:= :client, @rownum:=0)tmp
                WHERE t.`id`=@pv
                )c ON sd.client_id = c.client_id
                WHERE sd.step_id = :stepid
                ORDER BY rownum LIMIT 1'), $args);
            if(!empty($resultset)){
                $isServiceDefault = $resultset[0]->option_id;
            }
        }

        $QuoteItemsDefect = QuoteItemsDefect::with('productOption')->where('quote_id', $id)->where('quote_item_id', $QuoteItem->id)->get();
        
        /* F5 price to partner */
        $qty                        =  $QuoteItem->quantity;
        $unitp                      =  $QuoteItem->unit_prod_price;
        $service                    =  $QuoteItem->unit_f5_services;
        $logistic                   =  $QuoteItem->unit_f5_logistics;
        $net_price                  =  $QuoteItem->unit_prod_price - $QuoteItem->unit_f5_services - $QuoteItem->unit_f5_logistics;
        $total_price                =  $net_price;
        $totalmargin                =  $QuoteItem->totalmargin;
        /* margin detail */
        $margin_oem_product         = $QuoteItem->unit_margin_oem_product;
        $margin_oem_logistic        = $QuoteItem->logistic_oem_margin;
        $margin_oem_service         = $QuoteItem->service_oem_margin;
        /* partner price to customer */
        $Product_Price_customer     = $QuoteItem->unit_f5_price;
        $Service_Price_customer     = $QuoteItem->services_net_price;
        $Logistic_Price_customer    = $QuoteItem->logistics_net_price;
        $Sub_total                  = $QuoteItem->total_price;
        $total_percent              = ($total_price > 0) ? ($total_price - $Sub_total) * 100.00 / $total_price : 0;
        $product_percent            = $QuoteItem->unit_margin_oem_product;
        $logistic_percent           = $QuoteItem->logistics_oem_margin;
        $service_percent            = $QuoteItem->services_oem_margin;

        /*===============================Permission==========================*/
        @$role = RoleUser::where('user_id',Auth::user()->id)->first()->role_id;
        @$permissions = Auth::user()->permission;
        @$permission =  explode(',', @$permissions);
        /*===============================Permission==========================*/

        return view('hpUser.QuoteSummary', 
            compact('type',
                'brand',
                'processor',
                'screen',
                'dvd',
                'ram',
                'services',
                'hdd',
                'quote_status',
                'Client',               
                'Quote',
                'QuoteItem', 
                'QuoteItemsDefect',
                'margin', 
                'permission',
                'role',
                'image1',
                'image2',
                'image3',
                'status',
                'unitp',
                'service',
                'logistic',
                'net_price',
                'total_price',
                'totalmargin',
                'role',
                'permission',
                'Product_Price_customer',
                'Logistic_Price_customer',
                'Service_Price_customer',
                'Sub_total',
                'total_percent',
                'product_percent',
                'logistic_percent',
                'service_percent',
                'isServiceDefault'
            ));
    } else {
        Toast::error('Please provide valid id !!!', 'Error');
        return redirect('/dashboard');
    }           
}

/* returns a list of store quotes against the specified client ($id) */
public function QuoteSummaryList(Request $request, $id = null) {
        //validate  user is logged in
    if (!$this->filter($this->permission)) {
        return redirect()->back();  
    }

        //validate $id => client id 
    if(empty($id) || !is_numeric($id) || $id <= 0){
        Toast::error('Client ID is not valid.', 'Error');
        return redirect('/dashboard');  
    }

        //validate user has permissions for client
    if(!$this->isUserValid($id)){ 
        Toast::error('You do not have permission.', 'Error');
        return redirect('/dashboard');
    }

    if (Auth::user()->id != null) {
        Input::flash();

        $txt = strip_tags(trim($request->get('search')));

        $statusarr = Array("1", "2", "3", "4", "6", "7");
        $status = (isset($request->status) && ($request->status == "1" || $request->status == "2" || $request->status == "3" || $request->status == "4" || $request->status == "6" || $request->status == "7")) ? $request->status : '';
        if($status != ''){
            $statusarr = Array($status);
        }
        $lim = (isset($request->perPage) && is_numeric($request->perPage) &&  $request->perPage > 0) ? $request->perPage : 30 ;
        $page = (isset($request->page) && is_numeric($request->page) && $request->page > 0) ? $request->page : 1;
        $i = 0;

        if ($page > 1) {
            $pages = ($page - 1) * $lim;
        } else {
            $pages = 0;
        }

        $client = Client::find($id);
        $user_id = Auth::user()->id;

            // retrieve the current users role
        $role_id = 3;
        $RoleUser = RoleUser::select('role_id')->where('user_id', '=', $user_id)->first();
        if(null !== $RoleUser){
            $role_id = $RoleUser->role_id;
        }

        if (!empty($client->name)) {
            if (!empty($txt)) {
                $Quote = QuoteManagement::with('Status', 'Clientname', 'QuoteItem','Username')
                ->whereIn('status', $statusarr)
                ->where(function ($query) use($txt){
                    $query->whereHas('Username', function ($q) use($txt) {
                        $q->where('first_name', 'like', "%$txt%");
                    })
                    ->orwhere('quote.id', 'like', "%$txt%")
                    ->orwhere('quote.name', 'like', "%$txt%");
                });

                if ($role_id == 3) {
                    $Quotes = $Quote->where('client_id', '=', $id)->where('user_id', '=', $user_id)->orderBy('quote.id', 'DESC')->paginate($lim);
                } else {
                    $Quotes = $Quote->where('client_id', '=', $id)->orderBy('quote.id', 'DESC')->paginate($lim);
                }
            }else{
                $Quote = QuoteManagement::with('Status', 'Clientname', 'QuoteItem','Username')->whereIn('status', $statusarr);
                if ($role_id == 3) {
                    $Quotes = $Quote->where('client_id', '=', $id)->where('user_id', '=', $user_id)->orderBy('quote.id', 'DESC')->paginate($lim);
                } else {
                    $Quotes = $Quote->where('client_id', '=', $id)->orderBy('quote.id', 'DESC')->paginate($lim);
                }                                                   
            }
                //===============================Permission==========================/
            @$role = RoleUser::where('user_id',Auth::user()->id)->first()->role_id;
            @$permissions = Auth::user()->permission;
            @$permission =  explode(',', @$permissions);
                //===============================Permission==========================/

            return view('hpUser.QuoteSummaryList', compact('Quotes','txt', 'status', 'pages', 'lim', 'id', 'client','role','permission'));
        } else {
            Toast::error('Please provide valid id  !!!', 'Error');
            return redirect('/dashboard');
        }
    } else {
        Toast::error('Please provide  valid id!!!', 'Error');
        return redirect('/admin/dashboard');
    }

}

public function UploadImage1(Request $request) {
    if ($this->filter($this->permission)) {
        if ((isset($request->image) && !empty($request->image)) || isset($request->image2) && !empty($request->image2) || isset($request->image3) && !empty($request->image3) || isset($request->serial_number) && !empty($request->serial_number)) {

            $Quote = Quote::find($request->get('quote_id'));
            $QuoteItem1 = DB::table('quote_items')->where('quote_id', '=', $request->get('quote_id'))->first();
            $QuoteItem = QuoteItem::find($QuoteItem1->id);


            if (isset($request->image) && !empty($request->image)) {
                $file = array('image' => $request->image);
                $destinationPath = public_path() . '/product/';
                $extension = Input::file('image')->getClientOriginalExtension();
                $now = time();
                $fileName1 = $now . '111.' . $extension;
                Input::file('image')->move($destinationPath, $fileName1);
                $QuoteItem->image1 = $fileName1;
            } else {
                unset($QuoteItem->image1);
            }

            if (isset($request->image2) && !empty($request->image2)) {
                $file = array('image2' => $request->image2);
                $destinationPath = public_path() . '/product/';
                $extension = Input::file('image2')->getClientOriginalExtension();
                $now = time();
                $fileName2 = $now . '222.' . $extension;
                Input::file('image2')->move($destinationPath, $fileName2);
                $QuoteItem->image2 = $fileName2;
            } else {
                unset($QuoteItem->image2);
            }

            if (isset($request->image3) && !empty($request->image3)) {
                $file = array('image3' => $request->image3);
                $destinationPath = public_path() . '/product/';
                $extension = Input::file('image3')->getClientOriginalExtension();
                $now = time();
                $fileName3 = $now . '333.' . $extension;
                Input::file('image3')->move($destinationPath, $fileName3);
                $QuoteItem->image3 = $fileName3;
            } else {
                unset($QuoteItem->image3);
            }

            $Quote->status = ($Quote->status) + 1;
            $QuoteItem->status = $Quote->status;
            $QuoteItem->serial_number = $request->serial_number;
            $Quote->save();
            if ($QuoteItem->save()) {
                Toast::success('successfully uploaded.', 'Success');
                return Redirect::back();
            }
        } else {
            Toast::error('Please select atleast 1 picture Or serial number !!!.', 'Error');
            return Redirect::back();
        }
    } else {
        Toast::error('You have not permission.', 'Error');
        return redirect('/dashboard');
    }    
}


public function UploadImage(Request $request) {
    if ($this->filter($this->permission)) {
        if ((isset($request->image) && !empty($request->image)) || isset($request->image2) && !empty($request->image2) || isset($request->image3) && !empty($request->image3) || isset($request->serial_number) && !empty($request->serial_number)) {
                    //dd($request->all());
            if(isset($request->image)){
                list($type, $data) = explode(';', $request->image);
                list(, $data) = explode(',', $data);
                $data = base64_decode($data);
                $image_name1 = "/product/image" . time() . '1.png';
                $path = public_path() . $image_name1;
                file_put_contents($path, $data);
            }else{
                $image_name1 = '';
            }

            if(isset($request->image2)){
                list($type, $data2) = explode(';', $request->image2);
                list(, $data2) = explode(',', $data2);
                $data2 = base64_decode($data2);
                $image_name2 = "/product/image" . time() . '2.png';
                $path = public_path() . $image_name2;
                file_put_contents($path, $data2);
            }else{
                $image_name2 = '';
            }

            if(isset($request->image3)){
                list($type, $data3) = explode(';', $request->image3);
                list(, $data3) = explode(',', $data3);
                $data3 = base64_decode($data3);
                $image_name3 = "/product/image" . time() . '3.png';
                $path = public_path() . $image_name3;
                file_put_contents($path, $data3);
            }else{
                $image_name3 = '';
            }

            $Quote = Quote::find($request->quote_id);
            $QuoteItem1 = DB::table('quote_items')->where('quote_id', '=', $request->get('quote_id'))->first();
            $QuoteItem = QuoteItem::find($QuoteItem1->id);

            $Quote->status = $Quote->status+1;
            $Quote->save();

            $QuoteItem->image1 = $image_name1;
            $QuoteItem->image2 = $image_name2;
            $QuoteItem->image3 = $image_name3;
            $QuoteItem->serial_number = $request->serial_number;

            if ($QuoteItem->save()) {
                Toast::success('successfully uploaded.', 'Success');
                return Redirect::back();
            }

        } else {
            Toast::error('Please select atleast 1 picture Or serial number !!!.', 'Error');
            return Redirect::back();
        }
    } else {
        Toast::error('You have not permission.', 'Error');
        return redirect('/dashboard');
    }    
}

/* returns CMS page links for the header section */
public static function Navigation() {
    $cms = Navigation::where('section', '=', 'Header')->where('status', '=', 'Active')->orderBy('position', 'ASC')->get();
    return $cms;
}

/* returns CMS page links for the header section */
public static function Footer() {
    $cms = Navigation::where('section', '=', 'Footer')->where('status', '=', 'Active')->orderBy('position', 'ASC')->get();
    return $cms;
}

public static function Theme() {
    if(!empty(Auth::user()->id)){
        $User = User::with('theme')->where('id',Auth::user()->id)->first();
    }else{
        $User = '';
    }
    return $User;
}

public static function Theme_Cookie() {
    if(isset($_COOKIE['theme_name'])){
        $theme_id = $_COOKIE['theme_name'];
        $Theme = Theme::find($theme_id);
    }else{
        $Theme = '';
    }
    return $Theme;
}

/* TODO : Need to review this method */
public function changeStatus(Request $request) {
        //validate  user is logged in
    if (!$this->filter($this->permission)) {
        Toast::error('You do not have permission.', 'Error');
        return redirect('/dashboard');  
    }

    $quote_id = $request->get('name');

        //validate quote id 
    if(!isset($quote_id) || !is_numeric($quote_id) || $quote_id <= 0){
        Toast::error('Quote ID is not valid.', 'Error');
        return redirect('/dashboard');  
    }

    $Quote = Quote::find($quote_id);

        //validate user has permissions for client
    if(null === $Quote || !$this->isUserValid($Quote->client_id)){ 
        Toast::error('You do not have permission.', 'Error');
        return redirect('/dashboard');
    }  

    $step = $request->get('status') + 1;
    $status1 = QuoteStatus::find($step);
    $status = json_decode(json_encode($status1));        
    $QuoteItem1 = DB::table('quote_items')->where('quote_id', '=', $request->get('name'))->first();
    $QuoteItem = QuoteItem::find(@$QuoteItem1->id);
    if ($request->get('status') == 1) {
        $Quote->date_presented = date('Y-m-d H:i:s');
    }

    if ($request->get('status') == 3) {
        $client = DB::table('settings')->where('client_id', '=', $Quote->client_id)->first();
        $Date = date('Y-m-d');
        if (isset($client->value)) {
            @$Quote->date_validto = date('Y-m-d H:i:s', strtotime($Date . ' + ' . $client->value . ' days'));
        } else {
            $client = DB::table('settings')->where('client_id', '=', '0')->first();
            @$Quote->date_validto = date('Y-m-d H:i:s', strtotime($Date . ' + ' . $client->value . ' days'));
        }
    }

    if ($request->get('status') == 4) {
        $Quote->date_accepted = date('Y-m-d H:i:s');

        $QuoteCollection = new QuoteCollection();
        $QuoteCollection->quote_id = $request->get('name');
        $QuoteCollection->user_id = $Quote->user_id;
        $QuoteCollection->client_id = $Quote->client_id;
        $QuoteCollection->date_collected = date('Y-m-d H:i:s');
        $QuoteCollection->email_sent = 'yes';
        $QuoteCollection->save();

                //===================== Collection Items ==========================//

        $QuoteCollectionItem = new QuoteCollectionItem();
        $QuoteCollectionItem->collection_id = $QuoteCollection->id;
        $QuoteCollectionItem->quote_id = $Quote->id;
        $QuoteCollectionItem->quote_item_id = $QuoteItem->id;
        $QuoteCollectionItem->save();
    }

    $Quote->status = $status1->id;
    $Quote->save();

    if($request->get('status') == 5){
        $QuoteCollection = QuoteCollection::where('quote_id',$Quote->id)->delete();
        QuoteCollectionItem::where('quote_id',$Quote->id)->delete();
    }

    $QuoteItem->status = $status1->id;
    $QuoteItem->save();

    if (!empty($status)) {
        return Response::json(array('status' => 'status', 'data' => $status));
    } else {
        return Response::json(array('status' => 'error'));
    }
}

public function downloadPDF($id = null) {
        //validate  user is logged in
    if (!$this->filter($this->permission)) {
        Toast::error('You do not have permission.', 'Error');
        return redirect('/dashboard');  
    }

        //validate $id => client id 
    if(!isset($id) || !is_numeric($id) || $id <= 0){
        Toast::error('Quote ID is not valid.', 'Error');
        return redirect('/dashboard');  
    }

    $Quote = Quote::find($id);

        //validate user has permissions for client
    if(null === $Quote || !$this->isUserValid($Quote->client_id)){ 
        Toast::error('You do not have permission.', 'Error');
        return redirect('/dashboard');
    } 

    $QuoteItem = QuoteItem::with('quote_name')->where('quote_id', '=', $id)->first();

    if (!empty($QuoteItem)) {
        $type = ProductOption::where('id', '=', $QuoteItem->type_id)->first();
        $brand = ProductOption::where('id', '=', $QuoteItem->brand_id)->first();
        $processor = ProductOption::where('id', '=', $QuoteItem->processor_id)->first();
        $screen = ProductOption::where('id', '=', $QuoteItem->screen_id)->first();
        $dvd = ProductOption::where('id', '=', $QuoteItem->dvd_id)->first();
        $ram = ProductOption::where('id', '=', $QuoteItem->ram_id)->first();
        $services = ProductOption::where('id', '=', $QuoteItem->services_id)->first();
        $hdd = ProductOption::where('id', '=', $QuoteItem->hdd_id)->first();
        $Client = Client::find($Quote->client_id);
        $quote_status = QuoteStatus::find($Quote->status);
        $QuoteItemsDefect = QuoteItemsDefect::with('productOption')->where('quote_id', $id)->where('quote_item_id', $QuoteItem->id)->get();

        $productsfee = $QuoteItem->unit_margin_oem_product;
        $servicesfee = $QuoteItem->unit_margin_oem_services;
        $logisticsfee = $QuoteItem->unit_margin_oem_logistics;

        $margin = $servicesfee + $logisticsfee - $productsfee;

        $ttgToHp = $QuoteItem->unit_price;
        $hpToClient = $QuoteItem->total_price - $margin;
        $total = $QuoteItem->total_price;
        $url = URL::to('/');
        $pdf = PDF::loadView('pdfview', compact('type', 'brand', 'processor', 'screen', 'dvd', 'ram', 'services', 'hdd', 'quote_status', 'Client', 'QuoteItem', 'QuoteItemsDefect', 'margin', 'ttgToHp', 'hpToClient', 'url'));

        return $pdf->download('QuoteSummary.pdf');
    }  
}

public function addUser() {
        //validate  user is logged in
    if (!$this->filter($this->permission)) {
        
       return redirect()->back();
                      }

    $user_id=Auth::User()->id;

        //validate that the user is a mnager or admin
        // retrieve the current users role
    $RoleUser = RoleUser::select('role_id')->where('user_id', '=', Auth::user()->id)->first();


    if(null === $RoleUser || $RoleUser->role_id === 3){
        Toast::error('You do not have permission.', 'Error');
        return redirect('/dashboard');  
    }

    $ClientUsers = ClientUser::where('user_id', Auth::user()->id)->get();
    $c = array();

    foreach ($ClientUsers as $Client) {
        $c[] = $Client->client_id;
    }

    $Clientsss = Client::whereIn('id', $c)->get();

    $userRoles = DB::table('roles')->where('id', '=', 4)->get();

    return view('hpUser/addUser', array(
        'userRoles' => $userRoles,
        'Clients' => $Clients
    ));
}

public function insertUser(Request $request) {
        //validate  user is logged in
    if (!$this->filter($this->permission)) {
        Toast::error('You do not have permission.', 'Error');
        return redirect('/dashboard');  
    }

        //get current user id
    $user_id = Auth::user()->id; 

        //validate that the user is a mnager or admin
        // retrieve the current users role
    $RoleUser = RoleUser::select('role_id')->where('user_id', '=', $user_id)->first();
    if(null === $RoleUser || $RoleUser->role_id === 3){
        Toast::error('You do not have permission.', 'Error');
        return redirect('/dashboard');  
    }

    Input::flash();
    $this->validate($request, array(
        'first_name' => 'required|min:2',
        'last_name' => 'required|min:3',
        'email' => 'required|min:3|unique:users',
        'password' => 'required|min:8',
        'company' => 'required',
        'phone' => 'nullable|regex:/^([0-9\-\+\(\)]*)$/|min:6|max:14',
    ));

    if ($request->input('active') != null) {
        $status = 1;
    } else {
        $status = 0;
    }
    $arr = $request->permission;

    if(!empty($arr)){
        $permission = implode(",",$arr);
    }else{
        $permission = 0; 
    }

    $save = User::Create(array(
        'first_name'    => strip_tags($request->input('first_name')),
        'last_name'     => strip_tags($request->input('last_name')),
        'email'         => strip_tags($request->input('email')),
        'password'      => bcrypt($request->input('password')),
        'company'       => strip_tags($request->input('company')),
        'phone'         => strip_tags($request->input('phone')),
        'active'        => $status,
        'created_by'    => Auth::user()->id,
        'permission'    => $permission,
        'theme_id'      => Auth::user()->theme_id,
    ));

    if ($request->input('select') != null) {
        foreach ($request->input('select') as $id) {
            if ($id != null && is_numeric($id)) {
                $ClientUser = new ClientUser();
                $ClientUser->user_id = $save->id;
                $ClientUser->client_id = $id;
                $ClientUser->save();
            }
        }
        } /*else {
            $ClientUser = new ClientUser();
            $ClientUser->user_id = $save->id;
            $ClientUser->client_id = 0;
            $ClientUser->save();
        }*/

        $store = RoleUser::Create(array(
            'user_id' => $save->id,
            'role_id' => '3',
        ));

        if ($save->save() || $store->save()) {
            $Theme = Theme::find($save->theme_id);
            $getUser = User::find($save->id);
            $PasswordReset = new PasswordReset();
            $PasswordReset->email = $save->email;
            $PasswordReset->token = bcrypt(uniqid());
            $PasswordReset->save();

            Mail::send('users.mail', compact('PasswordReset','save','getUser','Theme'), function ($message) use ($save,$Theme) {

                $message->from('support@f5-it.com', "$Theme->theme_name/ New user registration");
                $message->subject($Theme->theme_name.'/ New user registration');
                $message->to($save->email, $save->name);
            });
            Toast::success('User '. strip_tags($request->input('first_name')) . ' has been added.', 'Success');
            return redirect('/dashboard');
        } else {
            Toast::error('New user not saved. Unknown error occurred', 'Error');
            return redirect('/dashboard');
        }
    }

    public function editUser(Request $request, $id = null) {
        //validate  user is logged in
        if (!$this->filter($this->permission)) {
            Toast::error('You do not have permission.', 'Error');
            return redirect('/dashboard');  
        }

        //get current user id
        $user_id = Auth::user()->id; 

        //validate that the user is a mnager or admin
        // retrieve the current users role
        $RoleUser = RoleUser::select('role_id')->where('user_id', '=', $user_id)->first();
        if(null === $RoleUser || $RoleUser->role_id === 3){
            Toast::error('You do not have permission.', 'Error');
            return redirect('/dashboard');  
        }

        //check that current user can manage the specified user ($id)

        if ($id != null && is_numeric($id)) {
            // retrieve the user object for the user being updated
            $user = User::with('UserRole')->where('id', '=', $id)->first();

            if(null === $user){
                toast::Error('User not Found ', 'Error');
                return Redirect('/dashboard');
            }

            // check the role of the user getting updat eto ensure they are only public (3)
            $RoleUser = RoleUser::select('role_id')->where('user_id', '=', $id)->first();
            if(null === $RoleUser || $RoleUser->role_id !== 3){
                toast::Error('User not Found ', 'Error');
                return Redirect('/dashboard');
            }


            $Client_Users = ClientUser::with('clientss')->where('user_id', $id)->get();
            foreach ($Client_Users as $ClientUser) {
                $abc[] = $ClientUser->client_id;
            }

            $ClientUsers = ClientUser::where('user_id', $user_id)->get();
            $c = array();

            $hasPermission = FALSE;
            foreach ($ClientUsers as $key => $Client) {
                $c[] = $Client->client_id;
                //check that the user is assigned to a client that th manager (current user) has permission for
                if(in_array($Client->client_id, $abc)){
                    $hasPermission = TRUE;
                }
            }

            if(!$hasPermission && $user->create_by !== $user_id){
                toast::Error('User not Found ', 'Error');
                return Redirect('/dashboard');
            }

            $Clients = Client::whereIn('id', $c)->get();

            $userRoles = DB::table('roles')->where('id', '=', 4)->get();
            $clientuser = DB::table('client_users')->where('user_id', '=', $id)->get();
            $CU = array();
            foreach ($clientuser as $val) {
                $CU[] = $val->client_id;
            }

            return view('hpUser/editUser', compact('abc', 'CU', 'user', 'clientuser', 'clients', 'userRoles', 'Clients'));
        }  else {
            toast::Error('User not Found ', 'Error');
            return Redirect('/dashboard');
        }      

    }

    public function updateUser(Request $request, $id = null) {
        //validate  user is logged in
        if (!$this->filter($this->permission)) {
            Toast::error('You do not have permission.', 'Error');
            return redirect('/dashboard');  
        }

        //get current user id
        $user_id = Auth::user()->id; 

        //validate that the user is a mnager or admin
        // retrieve the current users role
        $RoleUser = RoleUser::select('role_id')->where('user_id', '=', $user_id)->first();
        if(null === $RoleUser || $RoleUser->role_id === 3){
            Toast::error('You do not have permission.', 'Error');
            return redirect('/dashboard');  
        }

        Input::flash();

        $this->validate($request, array(
            'first_name' => 'required|min:2',
            'last_name' => 'required|min:2',
            'company' => 'required',
        ));

        $uid = $request->input('id');

        if(!isset($uid) || !is_numeric($uid) || $uid <= 0) {
            toast::Error('User not Found ', 'Error');
            return Redirect('/dashboard');
        }      

        $user = User::with('clientuser')->find($uid);
        if(null === $user || empty($user)) {
            toast::Error('User not Found ', 'Error');
            return Redirect('/dashboard');
        } 

        // check the role of the user getting updat eto ensure they are only public (3)
        $RoleUser = RoleUser::select('role_id')->where('user_id', '=', $uid)->first();
        if(null === $RoleUser || $RoleUser->role_id !== 3){
            toast::Error('User not Found ', 'Error');
            return Redirect('/dashboard');
        }

        // check current user has permissions
        $resultsets = DB::select(DB::raw("SELECT 1 FROM `client_users` WHERE user_id = " . $uid . " AND client_id IN (SELECT client_id FROM `client_users` WHERE `user_id` = " . $user_id . ") GROUP by 1"));
        if($user->created_by !== $user_id && empty($resultsets)){
            toast::Error('User not Found ', 'Error');
            return Redirect('/dashboard');
        }

        if (isset($request->password) && !empty($request->password)) {
            $this->validate($request, array(
                'password' => 'required|min:8',
            ));
            $user->password = bcrypt($request->get('password'));
        } else {
            unset($user->password);
        }
            //DB::table('role_user')->where('user_id', $request->input('id'))->update(['role_id' => $request->input('userrole')]);

        if ($request->input('active') != null) {
            $status = 1;
        } else {
            $status = 0;
        }
        $arr = $request->permission;

        if(!empty($arr)){
            $permission = implode(",",$arr);
        }else{
            $permission = 0; 
        }

        $user->first_name = strip_tags($request->get('first_name'));
        $user->last_name = strip_tags($request->get('last_name'));
        $user->company = strip_tags($request->get('company'));
        $user->phone = strip_tags($request->get('phone'));
        $user->theme_id = Auth::user()->theme_id;
        $user->active = $status;
        $user->created_by = Auth::user()->id;
        $user->permission = $permission;
        if ($user->save()) {
            $id = $user->id;
            ClientUser::where('user_id', $id)->delete();
            if ($request->input('select') != null) {
                foreach ($request->get('select') as $key => $val) {
                    if(!empty($val) && is_numeric($val) && $val > 0){
                        ClientUser::create(['client_id' => $val, 'user_id' => $user->id]);
                    }
                }
            } else {
                //ClientUser::create(['client_id' => 0, 'user_id' => $user->id]);
            }


            Toast::success('User ' . strip_tags($request->get('first_name')) . ' has been updated.', 'Success');
            return Redirect('/dashboard');

        } else {
            Toast::error('User not updated, Please try again.', 'Error');
            return Redirect('/dashboard');
        }
    }

    /* ================----End Client selection--================== */

    public function HpUserClientSelect(Request $request) {
        $ClientUsers = ClientUser::where('user_id', Auth::user()->id)->get();
        $c = array();

        foreach ($ClientUsers as $Client) {
            $c[] = $Client->client_id;
        }

        $Clients = Client::whereIn('id', $c)->get();

            //$level1 = Client::where('level', '=', 1)->get()->toArray();
        $level1 = Client::whereIn('id', $c)->where('level', '=', 1)->get()->toArray();
        $i = 0;

        foreach ($level1 as $val) {
            $level1[$i]['items'] = Client::whereIn('id', $c)->where('parent_id', '=', $val['id'])->get()->toArray();

            if (!empty($level1[$i]['items'])) {
                $j = 0;
                foreach ($level1[$i]['items'] as $abc) {
                    if ($abc != null) {
                        $child = Client::whereIn('id', $c)->where('parent_id', '=', $abc ['id'])->get()->toArray();
                        if (count($child) > 0) {
                            $level1[$i]['items'][$j]['items'] = $child;
                        }
                        $j++;
                    }
                }
            }$i++;
        }

        $level1['ClientUser'] = ClientUser::with('clientss')->where('user_id', $request->id)->get()->toArray();
        $level1['Clients'] = Client::whereIn('id', $c)->get();
        $level1 = array_filter($level1);

        return response()->json($level1);
    }
    /* ================----End Client selection--================== */

    public function pickupRequest(Request $request) {
        if ($this->filter($this->permission)) {
            if (isset($request->client_names)) {
                $user = Auth::user()->id;
                $client_id = $request->client_names;

                $Quotes = Quote::with('QuoteItem')->where('user_id', '=', $user)->where('client_id', '=',$client_id )->where('status', '=', 4)->get();

                foreach ($Quotes as  $Quote) {
                    if($Quote->id != null){

                        $Quote = Quote::find($Quote->id);
                        $QuoteItem = QuoteItem::find($Quote->QuoteItem->id);

                        $Quote ->status = '5';
                        $Quote->save();
                        $QuoteCollection = new QuoteCollection();
                        $QuoteCollection->quote_id = $Quote->id;
                        $QuoteCollection->user_id = $Quote->user_id;
                        $QuoteCollection->client_id = $Quote->client_id;
                        $QuoteCollection->date_collected = date('Y-m-d H:i:s');
                        $QuoteCollection->email_sent = 'yes';
                        $QuoteCollection->save();

                                //===================== Collection Items ==========================//

                        $QuoteCollectionItem = new QuoteCollectionItem();
                        $QuoteCollectionItem->collection_id = $QuoteCollection->id;
                        $QuoteCollectionItem->quote_id = $Quote->id;
                        $QuoteCollectionItem->quote_item_id = $QuoteItem->id;
                        $QuoteCollectionItem->status = '';
                        $QuoteCollectionItem->save();
                    }

                }
                $quotes = Quote::with('QuoteItem', 'Clientname', 'Username')->where('user_id', '=', $user)->where('client_id', '=',$client_id )->where('status', '=', 5)->get();

                if (count($quotes) > 0) {
                    $save = array();

                    Mail::send('hpUser.pickup', array('quotes' => $quotes), function ($message) use ($save) {
                        $message->subject('pickup Request');

                        $message->to('rajkishor.gupta@webnyxa.com', 'Admin');
                    });

                    return redirect('hpUser/pickupRequestDetail?client_id='.$client_id);
                } else {
                    Toast::error('No record found in database', 'Error');
                    return redirect('/dashboard');
                }
            } else {
                Toast::error('Please select any client.', 'Error');
                return redirect('/dashboard');
            }
        } else {
            Toast::error('You have not permission.', 'Error');
            return redirect('/dashboard');
        }    
    }

    public function pickupRequestDetail(){

        $quotes = Quote::with('QuoteItem', 'Clientname', 'Username')->where('user_id', '=',Auth::user()->id)->where('client_id', '=',$_GET['client_id'])->where('status', '=',5)->orderBy('date_modified','desc')->get();

        return view('hpUser/pickupRequestDetail', array('quotes' => $quotes));
    }

    public function navigations($id = null) {
        if ($id != null) {
            $Navigation = Navigation::where('name', $id)->where('status', 'Active')->first();
            if (!empty($Navigation)) {
                return view('hpUser.nav_page', compact('Navigation'));
            } else {
                return redirect()->back();
            }
        }
    }

    public function contacts() {
        return view('hpUser.contacts');
    }

    public function info(Request $request) {
        if ($request->id != '') {
            $description = ProductOption::find($request->id);
            return Response::json(['description' => $description->description]);
        }
    }

    public static function hpDashboardQuote() {
        @$id=Auth::user()->id;
        @$ClientUsers = ClientUser::with('clients', 'clientss')->where('user_id', $id)->get();
        return $ClientUsers ;
    }

    public function updateName(Request $request){

        if(isset($request->Quote_id)){
            $Quote = Quote::find($request->Quote_id);
            $QuoteItem1 = DB::table('quote_items')->where('quote_id', '=', $request->Quote_id)->first();
            $QuoteItem = QuoteItem::find($QuoteItem1->id);

            if(!isset($request->status) || null === $Quote || null === $QuoteItem){ 
                return redirect('quote/history/'.$Quote->id);
            }

            $step = ($request->status == 0 && $Quote->status == 0) ? 3 : $request->status + 1;
            $status1 = QuoteStatus::find($step);
            if(null === $status1){
                return redirect('quote/history/'.$Quote->id);
            }

            $Quote->name = strip_tags($request->reference);            
            $QuoteItem->status = $status1->id;
            $QuoteItem->save();  

            if($request->status == 0 && $Quote->status == 0 && $QuoteItem->isPUP > 0){
                $pup = PupContract::find($QuoteItem->isPUP);
                if(null !== $pup){
                    $pup->status = 'Redeemed';
                    $pup->save();
                }
            }

            $Quote->status = $status1->id;
            if($Quote->save()){ 
                return redirect('quote/history/'.$Quote->id);
            }
        }

    }

    public function model_auto_search(Request $request){
        $query = $request->search;        

        $client_id = 0;
        if(Session::has('client_id')){
            $client_id = Session::get('client_id');
        } 
        if($client_id == 0 && Session::has('enterprise_client_id')){
            $client_id = Session::get('enterprise_client_id');
        } 

        // validate client 
        //only perform search if 3 or more charater are entered
        if( $client_id == 0 || strlen($query) < 3){ return; }

        //validate user has permission against this client
        if(!$this->isUserValid($client_id)) { return; }

        //get list of countries for the client
        $calculate = new CalculationsController();
        $countries = $calculate->getCountriesforClient($client_id);
        //loop though and get array of ids
        //preceed the array with the default country id of zero
        $countryids = array(0);
        foreach($countries as $country){
            $countryids[] = $country->id;
        }

        $args = array(
            'client'    => $client_id,
            'query'     => '%' . $query . '%'
        );

        $results = array();
        //Model search

    	$resultset = DB::select(DB::raw("SELECT `po1`.`name` as type_name
    	, `po1`.`image` as image1
    	, `po2`.`name` as brand_name
    	, `product`.`image` as image2
    	, `po3`.`name` as processor_name
    	, `product`.`id` AS id
    	, `product`.`model` AS model
    	, 'product' as `table_name` 
		, IFNULL(`po5`.`name`, '-') as screen_name
    	FROM product AS product
    	LEFT JOIN product_options AS po1 ON po1.id = product.type_id 
    	LEFT JOIN product_options AS po2 ON po2.id = product.brand_id 
    	LEFT JOIN product_options AS po3 ON po3.id = product.processor_id 
		LEFT JOIN product_options AS po5 ON po5.id = product.screen_id 
    	JOIN(
    		SELECT t.`id` AS client_id, @pv:=t.`parent_id` AS parent,@rownum := @rownum+ 1 AS rownum
    		FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
    		JOIN (SELECT @pv:= :client, @rownum:=0)tmp
    		WHERE t.`id`=@pv
    	)c on c.`client_id` = product.`client_id`            
    	WHERE `product`.`model` LIKE :query 
    	AND `product`.`status` = 'active' 
    	AND (`product`.`value_factor` IS NULL OR `product`.`value_factor` = 'explicit price')
    	AND `product`.country_id IN (". implode(',', $countryids) .")
    	ORDER BY `product`.`model`, c.rownum"), $args);

    	$prevmodel = '';
    	foreach($resultset as $model){
    		if($model->model != $prevmodel){
    			array_push($results, $model);
    		}
    		$prevmodel = $model->model;
    	}

    	usort($results, function ($a, $b) {
    		return strcmp($a->model, $b->model);
    	});

		//PUP Search
		//only applies to store clients
    	$client_type = Client::find($client_id)->client_type;
    	if(isset($client_type) && 'store' == $client_type){
    		$sql = "SELECT `po4`.`name` as type_name
    		, `po4`.`image` as image1					
    		, `po5`.`name` as brand_name
    		, '' as image2
    		, `po6`.`name` as processor_name
    		, `pup_contracts`.`id` AS id
    		, `pup_contracts`.`serial_number` AS model
    		, 'pup_contracts' as table_name 
			, IFNULL(`po5`.`name`, '-') as screen_name
    		FROM pup_contracts AS pup_contracts
    		LEFT JOIN product_options AS po4 ON po4.id = pup_contracts.type_id 
    		LEFT JOIN product_options AS po5 ON po5.id = pup_contracts.screen_id 
    		LEFT JOIN product_options AS po6 ON po6.id = pup_contracts.processor_id 
    		WHERE `pup_contracts`.`serial_number` LIKE :query AND  `pup_contracts`.`status` = 'Valid' ";

            $resultset = \DB::select(DB::raw($sql), array('query' => '%' . $query . '%'));
            foreach($resultset as $model){
                array_push($results, $model);
            }
        }
        if (count($results) > 0) {
            echo json_encode($results); 
        }
    }

    public function uploadimageByWebcam(Request $request){
        if ((isset($request->image1) && !empty($request->image1)) || isset($request->image2) && !empty($request->image2) || isset($request->image3) && !empty($request->image3) || isset($request->serial_number) && !empty($request->serial_number)) {

            if(isset($request->image1)){
                list($type, $data) = explode(';', $request->image1);
                list(, $data) = explode(',', $data);
                $data = base64_decode($data);
                $image_name1 = "/product/image" . time() . '1.png';
                $path = public_path() . $image_name1;
                file_put_contents($path, $data);
            }else{
                $image_name1 = '';
            }

            if(isset($request->image2)){
                list($type, $data2) = explode(';', $request->image2);
                list(, $data2) = explode(',', $data2);
                $data2 = base64_decode($data2);
                $image_name2 = "/product/image" . time() . '2.png';
                $path = public_path() . $image_name2;
                file_put_contents($path, $data2);
            }else{
                $image_name2 = '';
            }

            if(isset($request->image3)){
                list($type, $data3) = explode(';', $request->image3);
                list(, $data3) = explode(',', $data3);
                $data3 = base64_decode($data3);
                $image_name3 = "/product/image" . time() . '3.png';
                $path = public_path() . $image_name3;
                file_put_contents($path, $data3);
            }else{
                $image_name3 = '';
            }

            $Quote = Quote::find($request->get('quote_id'));
            $QuoteItem1 = DB::table('quote_items')->where('quote_id', '=', $request->get('quote_id'))->first();
            $QuoteItem = QuoteItem::find($QuoteItem1->id);

            $Quote->status = $Quote->status+1;
            $Quote->save();

            $QuoteItem->image1 = $image_name1;
            $QuoteItem->image2 = $image_name2;
            $QuoteItem->image3 = $image_name3;
            $QuoteItem->serial_number = $request->serial_number;

            if ($QuoteItem->save()) {
                Toast::success('successfully uploaded.', 'Success');
                return Redirect::back();
            }
        } else {
            Toast::error('Please select atleast 1 picture Or serial number !!!.', 'Error');
            return Redirect::back();
        }
    }


    public function collection_history(Request $request, $id = null){

        $role = RoleUser::where('user_id',Auth::user()->id)->first()->role_id;
        @$permissions = Auth::user()->permission;
        $permission =  explode(',', @$permissions);

        if($role == 2 || ($role == 3 && @in_array(3,$permission)) ){

            if($id != null){
                $QuoteId = array();

                $txt = trim($request->get('search'));
                $limit = $request->get('perPage');
                if (isset($limit)) {
                    $lim = $limit;
                } else {
                    $lim = 30;
                }
                $page = ($request->page);
                $i = 0;
                if (isset($page) && $page > 1) {
                    $pages = ($page - 1) * $lim;
                } else {
                    $pages = 0;
                }
                $ClientName = Client::find($id);
                $Quotes = Quote::where('client_id',$id)->where('status',4)->get();

                foreach ($Quotes as $Quote) {
                    $QuoteId[] = $Quote->id;
                }

                $QuoteItem = QuoteItem::select('quote_items.serial_number as serial_number',

                    'quote_items.type_name as type_name',
                    'quote_items.screen_name as screen_name',
                    'quote_items.processor_name as processor_name',
                    'quote_items.brand_name as brand_name',
                    'quote_items.id as QI_ID',
                    'product.model as model',
                    'product.image as m_image',
                    'product_options.image as productImage')
                ->whereIn('quote_id', $QuoteId)
                ->where(function($query) use ($txt){
                    $query->where('model', 'like', '%' . $txt . '%');
                    $query->OrWhere('type_name', 'like', '%' . $txt . '%');
                    $query->OrWhere('serial_number', 'like', '%' . $txt . '%');
                    $query->OrWhere('brand_name', 'like', '%' . $txt . '%');
                })
                ->leftJoin('product', 'product.id', '=', 'quote_items.model_id')
                ->leftJoin('product_options', 'product_options.id', '=', 'quote_items.type_id');

                $QuoteItems = $QuoteItem->whereIn('quote_id', $QuoteId)->paginate($lim);

                return view('hpUser/collection_history',compact('QuoteItems','lim','id','ClientName','pages'));  
            }

        }else{
            return redirect()->back();
        }
    }
    public function collection_status(Request $request){
        $client_id = $request->client_id;

        if (isset($request->ids)) {

            $user = Auth::user()->id;

            foreach ($request->ids as  $id) {
                if($id != null){

                    $QuoteItem = QuoteItem::find($id);
                    $Quote = Quote::find($QuoteItem->quote_id);
                    $Quote ->status = '5';
                    $Quote->save();
                    $QuoteCollection = new QuoteCollection();
                    $QuoteCollection->quote_id = $Quote->id;
                    $QuoteCollection->user_id = $Quote->user_id;
                    $QuoteCollection->client_id = $Quote->client_id;
                    $QuoteCollection->date_collected = date('Y-m-d H:i:s');
                    $QuoteCollection->email_sent = 'yes';
                    $QuoteCollection->save();

                        //===================== Collection Items ==========================//

                    $QuoteCollectionItem = new QuoteCollectionItem();
                    $QuoteCollectionItem->collection_id = $QuoteCollection->id;
                    $QuoteCollectionItem->quote_id = $Quote->id;
                    $QuoteCollectionItem->quote_item_id = $QuoteItem->id;
                    $QuoteCollectionItem->save();
                }
            }

            $quotes = Quote::with('QuoteItem', 'Clientname', 'Username')->where('user_id', '=',Auth::user()->id)->where('client_id', '=',$client_id)->where('status', '=',5)->orderBy('date_modified','desc')->get();
            $save = array();

            Mail::send('hpUser.pickup', array('quotes'=>$quotes), function ($message) use ($save) {
                $message->subject('pickup Request');
                $message->to('prerna.yadav@webnyxa.com', 'Admin');
            });

            return redirect('/collection_details');

        }
    }
    public function CollectionDetails(Request $request){

 
        if (!$this->filter($this->permission))
         //   dd($this);
        {

            
            return redirect()->back();  
         }

        $txt = trim($request->get('search'));
        $limit = $request->get('perPage');
        if (isset($limit)) {
            $lim = $limit;
        } else {
            $lim = 30;
        }
        $page = ($request->page);
        $i = 0;
        if (isset($page) && $page > 1) {
            $pages = ($page - 1) * $lim;
        } else {
            $pages = 0;
        }

        $QuoteCollection = QuoteCollection::select('quote_collection.*','users.first_name as Username','clients.name as ClientName','clients.id as client_id','quote.status as quote_status');
        if(!empty($txt)){
            $QuoteCollection ->where('Username', 'like', '%' . $txt . '%');
            $QuoteCollection ->OrWhere('clients.name', 'like', '%' . $txt . '%');
        }
        $QuoteCollection->leftJoin('users', 'users.id', '=', 'quote_collection.user_id');
        $QuoteCollection->leftJoin('clients', 'clients.id', '=', 'quote_collection.client_id');
        $QuoteCollection->leftJoin('quote', 'quote.id', '=', 'quote_collection.quote_id');

        $QuoteCollections = $QuoteCollection->where('clients.client_type','store')->where('quote_collection.user_id',Auth::user()->id)->orderBy('date_collected','desc')->paginate($lim);
       // dd($QuoteCollections);
        return view('/hpUser.collection_details',compact('lim','id','ClientName','pages','QuoteCollections'));

    }    

    /* 
    Determines if the current logged-in user has the appropriate permisions for the specified client
    if so the True is returned else false  
    */
    private function isUserValid($client_id, $quote_id = 0){    
        //validate client id
        if (!empty($client_id) && is_numeric($client_id) && $client_id > 0) {
            // get the user id  
            $userID = Auth::user()->id;
            // check if user can do action for the client           
            $cu = ClientUser::where([['client_id', '=', $client_id], ['user_id', '=', $userID]])->first();

            if(null !== $cu){ 
                if($quote_id === 0){
                    return true; 

                //validate that user can access the quote if specified
                // i.e. have public role (3) but is the user who created the quote or role of 1 (admin) or 2 (manager)
                } else if (!empty($quote_id) && is_numeric($quote_id) && $quote_id > 0) {
                    $role = RoleUser::where('user_id', Auth::user()->id)->first()->role_id; 

                    if(null !== $role && $role != 3){
                        return true;
                    }

                    $quote = Quote::where('id', '=', $quote_id)->where('user_id', '=', $userID)->first();   

                    if(null !== $role && null !== $quote && $role === 3){
                        return true;
                    }   
                }
            }
        }
        return false;
    }

        /*
        returns an array of the id, name and product option object  give an id and the product type.
        This is used to validate that the id is numeric and matches a product_option of the  type specified
    */
        private function validateOption($id, $type){
            if(isset($id) && is_numeric($id) && $id > 0){
                $option = ProductOption::where([['type', '=', $type], ['id', '=', $id]])->first();
                if(null !== $option){
                    return array('id' => $option->id, 'name' => $option->name, 'option' =>$option);
                }
            }
            return array('id' => 0, 'name' => '', 'option' => null);
        }

       
        // page not found exceptions
        public function pagenotfound(){
            $role_id = RoleUser::select('role_id')->where('user_id', '=',Auth::user()->id)->first()->role_id;
             if($role_id == 1){
        
               return view('errors.admincustom');
            }else{
            
                return view('errors.custom');

            }
           
       }


    //internal server error 500
        public function internalerror(){

            if ($this->filter($this->permission)){
                return view('errors.uinternalerror');
            }else{

                return view('errors.ainternalerror');
            }
       }
       // redirection 
       public function redrectRoot(){
            $lastUrl = Auth::user()->last_url;
            if ($this->filter($this->permission)){

                if(isset($lastUrl) && !empty($lastUrl)){
                    return redirect($lastUrl);
                }else{
                    return redirect('/dashboard');
                }

            }else{

                if(isset($lastUrl) && !empty($lastUrl)){
                    return redirect($lastUrl);
                }else{
                    return redirect('/admin/dashboard');
                }

               
            }   
              
       }
    }

