<?php
namespace App\Http\Controllers;

ini_set('max_execution_time', 600);

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Session;
use DateTime;
use links;
use Toast;
use App\ProductOption;
use View;
use App\IntegrationImport;
use App\PriceMarginServices;
use App\Country;
use App\Quote;
use App\QuoteItem;
use App\QuoteItemsDefect;
use App\QuotesNotes;
use App\Http\Controllers\CalculationsController;

class ServicesImportController extends Controller
{
	public function __construct() {
		$this->middleware('auth');
		date_default_timezone_set('Asia/Kolkata');
	}
	public function bulkView(){
		$IntegrationImports = IntegrationImport::where('source','=','services')->orderBy('date','DESC')->get();
		//dd($IntegrationImports);

		return view('Variances/ServicesImport',array(
			'IntegrationImports'=> $IntegrationImports,
			));
		
	}
	public function bulkUpload(Request $request){

		$debug = false;
		$folder         =  public_path().'/i/';
		$datestring 	=  date_format(new DateTime, 'Y-m-d-H-i-s');
		$importfilename = 'services-import-' . $datestring . '.csv';
		$exportfilename = 'services-export-' . $datestring . '.csv';

		/*
			1. Save uploaded file to server 
		*/	
			if (! isset($_FILES["fileToUpload"])) {
				return  'A csv file must be selected for upload';
			}
			if ($_FILES["fileToUpload"]["error"] > 0) {
				Toast::error('A error was encountered uploading this file(' . $_FILES["fileToUpload"]["error"] . ')', 'Error');
				return redirect('/Variances/services-import-view');
			}

			$ftype = explode(".",$_FILES['fileToUpload']['name']);
			if(strtolower(end($ftype)) !== 'csv'){

				Toast::error('only a csv can be uploaded', 'Error');
				return redirect('/Variances/services-import-view');
			}
			
			if(move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $folder. $importfilename) === FALSE){

				Toast::error('File could not be saved', 'Error');
				return redirect('/Variances/services-import-view');
			}

			/*
			2. Create a new import record 
			*/
			$IntegrationImport               = new IntegrationImport();
			$IntegrationImport->date         = date('Y-m-d-H-i-s');
			$IntegrationImport->fileimported = $importfilename;
			$IntegrationImport->source       = 'services';
			$IntegrationImport->save();

			$import_id = $IntegrationImport->id;

			//reset all records so that none are flagged as being updated
			DB::update(DB::raw("UPDATE `price_margin_services` SET processed = 0"));

			/*
			3. Process the file 
			*/
			$row = 0;
			$columncount = 0;
			$processrecords = array();
			$columnames = array();
			$isValid = FALSE;
			$num_invalid = 0;
			$num_valid = 0;

			if (($handle = fopen($folder. $importfilename, "r")) !== FALSE) {

				while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
					$row ++;
					if($row == 1){
						$columncount = count($data);
						$columnames = $data;
					} else if (count($data) != 6){
					// incorrect number of columns for the row
						array_unshift($data, "[record has incorrect number of columns (".  count($data) . ")]");				
						array_push($processrecords, $data);
						$num_invalid ++;
					} else {	
					$type		= trim($data[0]);		// product type		e.g. notebook	
					$service	= trim($data[1]);		// service			e.g. Disk Wipe	
					$country	= trim($data[2]);		// Country			e.g. New Zealand	
					$client		= trim($data[3]);		// Client			e.g. HP India store A		
					$value		= $data[4];				// Price			e.g. 4.50
					$status		= trim($data[5]);		// status			e.g. active

					// validate the data
					$validationmessage = '';
					$isValid = FALSE; 

					if(empty($type) || trim($type) === ''){
						$validationmessage .= "[a product type is required]";
					}	
					if(empty($service) || trim($service) === ''){
						$validationmessage .= "[a service type is required]";
					}		
					if(empty($value) || !is_numeric($value)){
						$validationmessage .= "[Price is invalid]";
					}	

					if(isset($status) && strtolower(trim($status)) !== 'active'){
						$status = 'disabled'; 
					} else {
					    $status = 'active';   
					}

					if($debug){
						echo '<pre>';
						print_r($data);
						echo '</pre>';
						echo $status.'<br>';
					}
						
					// vailidate model and device components
					$args = array(
						'client'	=> $client, 
						'country'	=> $country, 
						'type'		=> $type,
						'service'	=> $service
						);

					$resultset = DB::select(DB::raw("SELECT 
						IFNULL((SELECT id FROM clients WHERE name = :client), 0) AS client,
						IFNULL((SELECT id FROM country WHERE name = :country), 0) AS country,
						IFNULL((SELECT id FROM product_options WHERE type = 'product' AND name = :type LIMIT 1), 0) AS type,
						IFNULL((SELECT id FROM product_options WHERE type = 'services' AND name = :service LIMIT 1), 0) AS service"),$args);

					if(!empty($resultset)){

						if($debug){
							echo '<pre>';
							print_r($resultset);
							echo '</pre>';
						}

						$country_id = $resultset[0]->country;
						$service_id = $resultset[0]->service;
						$type_id = $resultset[0]->type;
						$client_id = $resultset[0]->client;

						if($country_id === 0 && trim($country) !== '') {
							$validationmessage .= "[country is invalid]";
						}
						if($client_id === 0 && strtolower($client) !== 'default') {
							$validationmessage .= "[client not found]";
						}
						if($type_id === 0) {
							$validationmessage .= "[product type is invalid]";
						}
						if($service_id === 0) {
							$validationmessage .= "[service is invalid]";
						}

						if($debug){ echo "message = $validationmessage<br>"; }
							
						//don't continue if there are any validation errors
						if($validationmessage === '') {

							$pms = PriceMarginServices::where('country_id', '=', $country_id)->where('type_id', '=', $type_id)->where('option_id', '=', $service_id)->where('client_id', '=', $client_id)->first();
							if(null === $pms){
								$pms =  new PriceMarginServices(); 
								$pms->type_id = $type_id;
								$pms->option_id = $service_id;
								$pms->country_id = $country_id;
								$pms->client_id = $client_id;
								$validationmessage .= "[added]";
							} else {
								$validationmessage .= "[updated]";
							}
							$pms->value = $value;
							$pms->processed = 1;
							$pms->status = $status;
							$pms->save();
							$isValid = TRUE; 
								
						}
					} else {
						$validationmessage .= "[error occurred validating information]";
					}

					if($isValid) { 
						$num_valid ++;
					} else {
						$num_invalid ++;
					}

					array_unshift($data, $validationmessage);				
					array_push($processrecords, $data);
				}
			}
				
			$resultset5 = DB::select(DB::raw("SELECT 
				'[deleted]' AS `message`,
				typ.name AS `type`,
				service.name AS `service`,
				cty.name AS `country`,
				c.name AS `client`,
				pl.value as `value`,
				pl.status as `status`
				FROM `price_margin_services` pl				
				INNER JOIN `product_options` typ ON pl.type_id = typ.id 
				LEFT JOIN `product_options` service ON pl.option_id = service.id 
				LEFT JOIN `clients` c ON c.id = pl.client_id
				LEFT JOIN `country` cty ON cty.id = pl.country_id
				WHERE processed = 0"));

			if(!empty($resultset5)){
				foreach ($resultset5 as $row) {
					//add each record to array for export in csv
					array_push($processrecords, 
						array (
							$row->message,
							$row->type,
							$row->service,
							$row->country,
							$row->client,
							$row->value,
							$row->status
							)
						);						
				}
			}

			//remove all records not updated
			DB::delete(DB::raw("DELETE FROM `price_margin_services` WHERE processed = 0"));

			//save the processed records to file.
			$exportfile = fopen($folder.$exportfilename, "w");

			array_unshift($columnames, 'message');
			fputcsv($exportfile, $columnames);
			foreach ($processrecords as $line){
				fputcsv($exportfile, $line);
			}

			/*
			4. Update import record will export file name andretrun results to 
			*/	
			$IntegrationImport                = IntegrationImport::find($import_id);
			$IntegrationImport->fileexported  = $exportfilename;
			$IntegrationImport->records_valid = $num_valid;
			$IntegrationImport->records_invalid = $num_invalid;
			$IntegrationImport->save();

			//need to recalulate all inprogress quotes.
			$Quotes = Quote::where('status', '=', '1')->get();
			if(count($Quotes) >0 ){
				foreach($Quotes as $Quote){
					$calculate = new CalculationsController();
					$calculate->Recalculate($Quote->id);

					$QuotesNotes = new QuotesNotes();
					$QuotesNotes->quote_id = $Quote->id;
					$QuotesNotes->user_id = Auth::user()->id;
					$QuotesNotes->display_level = 1;
					$QuotesNotes->note = 'Services pricing bulk import - recalculation of in-progress quotes.';
					$QuotesNotes->save();
				}
			}

			Toast::Success('Import has been completed', 'Success');
			return redirect('/Variances/services-import-view');

		} else {

			Toast::error('The imported file couldn\'t be processed', 'Error');
			return redirect('/Variances/services-import-view');
		}
	}
}
