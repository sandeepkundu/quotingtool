<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Session;
use DateTime;
use links;
use Toast;
use App\Client;
use App\User;
use App\ClientUser;
use App\ProductOption;
use App\ProductOptionGroup;

class ProductOptionGroupsController extends Controller {

      private $permission = [1];

    public function __construct() {
        $this->middleware('auth');
    }

    public function index(Request $request) {
        if ($this->filter($this->permission)) {
            $pog = ProductOptionGroup::select('group', 'group_code', DB::raw('count(*) as group_count'))
                            ->groupBy('group')->orderBy('group', 'asc')->get();
            return view('poGroups.index', array('pog' => $pog));
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function detailsPog(Request $request, $group = null) {
        if ($this->filter($this->permission)) {
            Input::flash();
            
            if ($group != null) {

                $pog = ProductOptionGroup::where('group', '=', $group)
						->leftjoin('clients', 'product_option_groups.client_id', '=', 'clients.id')
						->select('product_option_groups.*', 'clients.fullpath')
						->orderBy('product_option_groups.order', 'desc')						
						->get();

                return view('poGroups.detailsPog', compact('pog', 'group'));
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function addNewGroups() {
        if ($this->filter($this->permission)) {
            $arr = $array = $arr1 = array();
            $types = ProductOption::select('type')->groupBy('type')->get();

			$clients = new ClientsController();
			$cids = Array();
			$parent_ids = $clients->getTree($cids, 0, 0, '0', '', '');
            $pog = ProductOptionGroup::all();

            return view('poGroups/addNewGroups', array(
                'types' => $types,
                'parent_ids' => $parent_ids,
                'pog' => $pog,
            ));
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function insertNewGroups(Request $request) {
        if ($this->filter($this->permission)) {
            $option_id = $request->option_name;
            Input::flash();


            $this->validate($request, array(
                'group' => 'required',
                'group_code' => 'required',
                'type' => 'required',
                'option_name' => 'required',
                'order' => 'numeric|between:0,2147483646'
            ));

            if($request->type == "Processor"){
                $this->validate($request, array(
                'processor_sub_group'=>'required'
            ));
            }

            if ($request->status == 'on') {
                $status = 'active';
            } else {
                $status = 'disabled';
            }
            //dd($request->all());
            $productOptions = ProductOption::find($option_id);
            $ProductOptionGroup = new ProductOptionGroup();
            $ProductOptionGroup->fill($request->all());
            $ProductOptionGroup->option_id = $option_id;
            $ProductOptionGroup->client_id = $request->parent_id;
            $ProductOptionGroup->type = $request->type;
            $ProductOptionGroup->processor_sub_group = $request->processor_sub_group;
            $ProductOptionGroup->name = $productOptions->name;
            $ProductOptionGroup->status = $status;

            if ($ProductOptionGroup->save()) {
                Toast::success('Record has been saved.', 'Success');
                return redirect('poGroups/index');
            } else {
                Toast::error('Record not saved . Id not found', 'Error');
                return redirect('poGroups/index/');
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function addPog(Request $request, $group = null) {
        if ($this->filter($this->permission)) {

            $pog = ProductOptionGroup::select('group_code', 'client_id', 'type','processor_sub_group')->where('group', '=', $group)->groupBy('group')->first();
            $productOptions = ProductOption::select('id', 'name')->where('type', '=', $pog->type)->where('isActive', '=' , '1')->orderBy('name', 'asc')->get();
            $client_name = Client::find($pog->client_id);

            return view('poGroups/addPog', array(
                'productOptions' => $productOptions,               
                'pog' => $pog,
                'group' => $group,
                'client_name' => $client_name
            ));
        } else {
        Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function insertPog(Request $request) {
        if ($this->filter($this->permission)) {
            $option_id = $request->option_name;
            Input::flash();

            $ProductOptionGroups = ProductOptionGroup::select('order')->where('group',$request->group)->get();
            foreach ($ProductOptionGroups as $orders) {
                 if($orders->order == $request->order){
                    return redirect()->back()->withErrors(['order' => 'Order has been already taken']);
                 }   
            }
            $this->validate($request, array(
                'group_code' => 'required',
                'option_name' => 'required',
                'type' => 'required',
                'order' => 'numeric|between:0,2147483646'
            ));
           if($request->type == "Processor"){
                $this->validate($request, array(
                'processor_sub_group'=>'required'
            ));
            }
            if ($request->status == 'on') {
                $status = 'active';
            } else {
                $status = 'disabled';
            }
            $productOptions = ProductOption::find($option_id);
            $ProductOptionGroup = new ProductOptionGroup();
            $ProductOptionGroup->fill($request->all());
            $ProductOptionGroup->client_id = $request->parent_id;
            $ProductOptionGroup->option_id = $option_id;
            $ProductOptionGroup->type = $request->type;
            $ProductOptionGroup->processor_sub_group = $request->processor_sub_group;
            $ProductOptionGroup->name = $productOptions->name;
            $ProductOptionGroup->status = $status;
            $ProductOptionGroup->allow = 'allow';
            if ($ProductOptionGroup->save()) {
                Toast::success('Record has been saved.', 'Success');
                return redirect('poGroups/detailsPog/' . $request->group);
            } else {
                Toast::error('Record not saved . Id not found', 'Error');
                return redirect('poGroups/detailsPog/' . $request->group);
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function editPog($id = null) {
        if ($this->filter($this->permission)) {
             $arr = $array = $arr1 = array();
            Input::flash();
            if ($id != null) {
                $ProductOptionGroup = ProductOptionGroup::find($id);

                $client_name = Client::find($ProductOptionGroup->client_id);
                $ProductOptions = ProductOption::select('id', 'name')->where('type', '=', $ProductOptionGroup->type)->where('isActive', '=' , '1')->orderBy('name', 'asc')->get();
				
                return view('poGroups/editPog', array('ProductOptionGroup' => $ProductOptionGroup, /*'parent_ids' => $parent_ids, */ 'ProductOptions' => $ProductOptions, 'client_name' => $client_name));
            } else {
                Toast::error('Record not Saved. Id not found', 'Error');
                return redirect('/poGroups/addPog');
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function updatePog(Request $request) {
        if ($this->filter($this->permission)) {
          
            Input::flash();
            $option_id = $request->option_name;
            $this->validate($request, array(
                'group_code' => 'required',
                'option_name' => 'required',
                'type' => 'required',
                'order' => 'numeric|between:0,2147483646'
            ));
            if($request->type == "Processor"){
                $this->validate($request, array(
                'processor_sub_group'=>'required'
            ));
            }

            $ProductOptionGroups = ProductOptionGroup::select('order')->where('id','!=',$request->id)->where('group',$request->group)->get();
            foreach ($ProductOptionGroups as $orders) {
                 if($orders->order == $request->order){
                    return redirect()->back()->withErrors(['order' => 'Order has been already taken']);
                 }   
            }
            if ($request->status == 'on') {
                $status = 'active';
            } else {
                $status = 'disabled';
            }
            if($request->allow == 'on' ){
                $allow = 'not_allow';
            }else{
                 $allow = 'allow';
            }
            $productOptions = ProductOption::find($option_id);
            $ProductOptionGroup = ProductOptionGroup::find($request->id);
            $ProductOptionGroup->client_id = $request->parent_id;
            $ProductOptionGroup->group_code = $request->group_code;
            $ProductOptionGroup->order = $request->order;
            $ProductOptionGroup->type = $request->type;
            $ProductOptionGroup->processor_sub_group = $request->processor_sub_group;
            $ProductOptionGroup->option_id = $option_id;
            $ProductOptionGroup->name = $productOptions->name;
            $ProductOptionGroup->status = $status;
            $ProductOptionGroup->allow = $allow;
            if ($ProductOptionGroup->save()) {
                Toast::success('Record has been Updated.', 'Success');
                return redirect('poGroups/detailsPog/' . $ProductOptionGroup->group);
            } else {
                Toast::error('Record not saved . Id not found', 'Error');
                return redirect('/poGroups/detailsPog/' . $ProductOptionGroup->group);
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function multipleDelete(Request $request) {
        if ($this->filter($this->permission)) {
            if (ProductOptionGroup::destroy($request->get('ids'))) {
                Toast::success('Record has been deleted.', 'Success');
                return Redirect('poGroups/detailsPog/' . $request->group);
            } else {
                Toast::error('Invalid id, Please try again!', 'Error');
                return Redirect('poGroups/detailsPog/' . $request->group);
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function Delete($id = null) {
        if ($this->filter($this->permission)) {
            if ($id != null) {
                $ProductOptionGroup = ProductOptionGroup::find($id);
                if ($ProductOptionGroup->delete()) {
                    Toast::success('Record has been deleted.', 'Success');
                    return redirect('poGroups/detailsPog/' . $ProductOptionGroup->group);
                } else {
                    Toast::error('Invalid id, Please try again!', 'Error');
                    return Redirect('poGroups/detailsPog/' . $ProductOptionGroup->group);
                }
            } else {
                Toast::error('Invalid id, Please try again!', 'Error');
                return Redirect('poGroups/ProductOptionGroup' . $ProductOptionGroup->group);
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function changetype(Request $request) {
        if ($this->filter($this->permission)) {
            $type = $request->type;
            $data = ProductOption::select('id', 'name')->where('type', '=', $type)->where('isActive', '=' , '1')->orderBy('name', 'asc')->get();
            return response()->json($data);
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

}
