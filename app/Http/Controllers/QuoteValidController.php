<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Session;
use Toast;
use DateTime;
use links;
use App\QuoteValid;
use App\Client;
use App\Http\Controllers\ClientsController;

class QuoteValidController extends Controller {

  private $permission = [1];

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    public function index(Request $request) {
        if ($this->filter($this->permission)) {
            $settings = QuoteValid::with('Clientname')->orderBy('id', 'desc')->get();      
            return view('/QuoteValid.index', compact('settings'));
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addQuoteValid() {
        if ($this->filter($this->permission)) {
            $parent_id = Client::where('parent_id', '=', 0)->where('id', '!=', '0')->get();
            $clients = new ClientsController();
            $cids = Array();
            $parent_ids = $clients->getTree($cids, 0, 0, '0', '', '');
           
            return view('/QuoteValid.addQuoteValid', compact('parent_ids'));
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function insertQuoteValid(Request $request) {
        if ($this->filter($this->permission)) {
            Input::flash();
            $this->validate($request, array(
                'Quote_valid_period' => 'required',
                'value' => 'required|numeric'
            ));
            $saveInsert = QuoteValid::Create(array(
                        'client_id' => $request->parent_id,
                        'setting' => $request->Quote_valid_period,
                        'value' => $request->value
            ));
            if ($saveInsert) {
                Toast::success('Record has been saved.', 'Success');
                return Redirect('/QuoteValid/index');
            } else {
                Toast::error('Please try again', 'Error');
                return Redirect('/QuoteValid/index');
            }
        } else {
           
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function editQuoteValid($id = null) {
        if ($this->filter($this->permission)) {
            if ($id != null) {
                $parent_id = Client::where('parent_id', '=', 0)->where('id', '!=', '0')->get();
                $clients = new ClientsController();
            $cids = Array();
            $parent_ids = $clients->getTree($cids, 0, 0, '0', '', '');
               

                $editQuoteValid = QuoteValid::find($id);
                if (!empty($editQuoteValid)) {
                    return view('/QuoteValid.editQuoteValid', compact('editQuoteValid', 'parent_ids'));
                } else {
                    Toast::error('Id not found', 'Error');
                    return Redirect('/QuoteValid/index');
                }
            } else {
                Toast::error('Id not found', 'Error');
                return Redirect('/QuoteValid/index');
            }
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateQuoteValid(Request $request, $id = null) {
        if ($this->filter($this->permission)) {
            Input::flash();
            $this->validate($request, array(
                'setting' => 'required',
                'value' => 'required|numeric'
            ));
            if ($id != null) {
                $saveupdate = QuoteValid::find($id);
                $saveupdate->fill($request->all());
                $saveupdate->client_id = $request->parent_id;

                if ($saveupdate->save()) {
                    Toast::success('Record has been updated.', 'Success');
                    return Redirect('/QuoteValid/index');
                }
            } else {
                Toast::error('Please try again', 'Error');
                return Redirect('/QuoteValid/index');
            }
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteQuoteValid($id = null) {
        if ($this->filter($this->permission)) {
            if ($id != null) {
                $delete = QuoteValid::find($id);
                if ($delete->delete()) {
                    Toast::success('Record has been deleted.', 'Success');
                    return Redirect('/QuoteValid/index');
                } else {
                    Toast::error('Id not found', 'Error');
                    return Redirect('/QuoteValid/index');
                }
            }
        } else {
           
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function multipleDelete(Request $request) {
        if ($this->filter($this->permission)) {
            $id = $request->get('ids');
            if (isset($id)) {
                QuoteValid::destroy($id);
                Toast::success('Record has been deleted.', 'Success');
                return Redirect('/QuoteValid/index');
            } else {
                Toast::error('Id not found', 'Error');
                return Redirect('/QuoteValid/index');
            }
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

}
