<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Session;
use DateTime;
use links;
use Toast;
use App\Client;
use App\User;
use App\ClientUser;
use App\Country;
use App\CountryRegion;
use App\RoleUser;
use App\ProductOption;
use App\Product;
use App\PupContract;
use App\ProductOptionGroup;
use App\QuoteManagement;
use PDF;
use App\Quote;
use App\QuoteStatus;
use App\QuoteItem;
use App\QuoteItemsDefect;
use App\QuoteCollection;
use App\QuoteCollectionItem;
use App\QuoteStepDefault;
use App\QuotesNotes;
use View;
use Response;
use Mail;
use Cookie;
use App\ClientCountryRate;
use App\Http\Controllers\CalculationsController;

class EnterprisesController extends Controller {

    private $permission = [2,3];

    public function __construct() {
		$this->middleware('auth');    
    }

    public function index(Request $request, $returnType = 0) {
		//validate user has permission
        if (!$this->filter($this->permission)) {
		    Toast::error('Authentication Failed:', 'Error');
		    //changes if  any  unauthenticazion  person  try  then reach  on error page  
            return redirect()->back();
		}

		$client_id = 0;
		$quote_id = 0;

		//check client id is set
		if (!Session::has('enterprise_client_id') || !is_numeric(Session::get('enterprise_client_id'))) {
			return redirect('/dashboard');
		} else {
			$client_id = Session::get('enterprise_client_id');
		}
		//check quote id is set
        if (!Session::has('quote_id') || !is_numeric(Session::get('quote_id'))) {
			return redirect('/dashboard');           
        } else {
            $quote_id = Session::get('quote_id');
        }

		//validate client and quote 
		if($client_id <= 0 || $quote_id <= 0){
			return redirect('/dashboard');   
		}

		//check user has permission for the quote
		if(!$this->isUserValid($client_id, $quote_id)){
		    Toast::error('You do not have the correct permissions to access this quote.', 'Error');
            return redirect('/dashboard');
		}
		
		//declare local varaibles;            
        $Sub_totat = $product = $logistic = $service = $total = $product1 = $logistic1 = $service1 = $total1 = $QuoteItems = array();
        $qty = $unitp = $services = $total_price = $net_price = $totalmargin = $ItemCount = array();
		$unit_tp = $services_tp = $logistics_tp = $total_tp = $unit_oemtp = $services_oemtp = $logistics_oemtp = $total_oemtp = array();    
    
		//retreive client object      
        $clientDetails = Client::find($client_id); 
       
		//retreive quote object
		$Quotes = Quote::with('Clientname', 'Username', 'quote_item','Defects','Region', 'Country')
			->where('id', '=', $quote_id,'')
			->orderBy('date_modified', 'desc')
			->first();

		//retreive quote item objects
        $Quote_Items = QuoteItem::where('quote_id',$quote_id)->orderBy('total_price', 'desc')->get();                
        foreach($Quote_Items as $Quote_Item){
            $items=QuoteItem::with('quote_name','model_image','option_model_image','QuoteItemsDefect')->where('id',$Quote_Item->id)->get()->toArray();
            $QuoteItems[$Quote_Item->type_name][] = $items;
        }

		$i=0;
        if(count($QuoteItems) > 0 ){
            foreach($QuoteItems as $key1 => $quotes){
                foreach($quotes as $Quote){
                    $ItemCount[$i]			= count($quotes);
					$i++;
                    $qty[]					= $Quote[0]['quantity'];
                    $unitp[]				= $Quote[0]['unit_prod_price'];
                    $net_price[]			= $Quote[0]['unit_price'];
                    $total_price[]			= $Quote[0]['total_price'];
					$unit_tp[]				= $Quote[0]['unit_prod_price'] * $Quote[0]['quantity'];
					$unit_oemtp[]			= $Quote[0]['unit_f5_price'] * $Quote[0]['quantity'];
					$services_tp[]			= $Quote[0]['unit_f5_services'] * $Quote[0]['quantity'];
					$services_oemtp[]		= $Quote[0]['services_net_price'] * $Quote[0]['quantity'];
					$logistics_tp[]			= $Quote[0]['unit_f5_logistics'] * $Quote[0]['quantity'];
					$logistics_oemtp[]		= $Quote[0]['logistics_net_price'] * $Quote[0]['quantity'];
					$total_tp[]				= ($Quote[0]['unit_prod_price'] - $Quote[0]['unit_f5_services'] - $Quote[0]['unit_f5_logistics']) * $Quote[0]['quantity'];
					$total_oemtp[]			= $Quote[0]['total_price'];
                }
            }
        }

        /*----------------Permission----------------*/
        $role = RoleUser::where('user_id',Auth::user()->id)->first()->role_id;
        @$permissions = Auth::user()->permission;
        $permission =  explode(',', @$permissions);
        /*----------------Permission----------------*/

		$outputarray = array(
            'Quotes'		=> $Quotes,
            'clientDetails' => $clientDetails,
            'Quote_Items'	=> $QuoteItems,
            'role'			=> $role,
            'qty'			=> @$qty,
            'unitp'			=> @$unitp,
            'services'		=> @$services,
            'logistic'		=> @$logistic,
            'net_price'		=> @$net_price,
            'total_price'	=> @$total_price,        
            'permission'	=> @$permission,
            'ItemCount'			=> $ItemCount,
			'debugDisplay'		=> (@$request->dd == 'true') ? 'true' : 'false',
			'itemSelected'		=> (isset($request->qiid) && $request->qiid != '') ? $request->qiid : '',
			'unit_tp'			=> (count($unit_tp) > 0) ? array_sum($unit_tp) : 0,
            'unit_oemtp'		=> (count($unit_oemtp) > 0) ? array_sum($unit_oemtp) : 0,
			'services_tp'		=> (count($services_tp) > 0) ? array_sum($services_tp) : 0,
			'services_oemtp'	=> (count($services_oemtp) > 0) ? array_sum($services_oemtp) : 0,
			'logistic_tp'		=> (count($logistics_tp) > 0) ? array_sum($logistics_tp) : 0,
			'logistic_oemtp'	=> (count($logistics_oemtp) > 0) ? array_sum($logistics_oemtp) : 0,
			'total_tp'			=> (count($total_tp) > 0) ? array_sum($total_tp) : 0,
			'total_oemtp'		=> (count($total_oemtp) > 0) ? array_sum($total_oemtp) : 0
        );

		/*return the result based of the specified $returnType
			$returnType = 1 return raw array
			$returnType <> 1 return raw array
		*/
		if($returnType == 1){
			return $outputarray;
		} else {
			return view('Enterprises.index2', $outputarray );
		}
    }

    private function getSteps($productId = null, $brandId = null) {
		/* internally called method so user is already authenticated */
		$client_id = 0;
        if(Session::has('enterprise_client_id')){
            $id = Session::get('enterprise_client_id');
			$client_id = (is_numeric($id) && $id > 0 ) ? $id : 0;
        }   

        $calculate = new CalculationsController();
		$step =  $calculate->getQuoteSteps($productId, $brandId, $client_id);
		return $step;
    }

    private function getStepsWithProcessorName($product = null, $brand = null, $order = ''){
		/* internally called method so user is already authenticated */

        if(Session::has('enterprise_client_id')){
            $client_id = Session::get('enterprise_client_id');
        }else{
            $client_id = 0;
        }

        $step = $this->getSteps($product, $brand);

        $proccessor = array();
        $Screen = array();
        $ram = array();
        $hdd = array();
        $DVD = array();
        $pro = '';
        $arr = array();

        foreach ($step as $key => $value) {
            if ($value->order == $order) {
                $a = substr($value->group_code, 0, -1);
                    //echo $a;die;
                if (strpos($a, 'Processor') !== false) {
                    $pro = $value->group_code;
                    if ($pro != '') {

                        $productogns = ProductOptionGroup::where('group_code',$pro)->groupBy('processor_sub_group')->orderBy('order')->get();
                            //echo '<pre>';print_r($productogns);die;
                        foreach ($productogns as $productogn) {
							$args = array(
								'client'	=> $client_id,
								'subgroup'	=> $productogn->processor_sub_group,
								'group'		=> $pro,
								'brand'		=> $brand,
								'type'		=> $product
							);

                            $proccessor = DB::select(DB::raw("SELECT PO.description as description,p.*
										FROM `product_option_groups` p
										JOIN(
											SELECT t.`id` AS client_id, @pv:=t.`parent_id` AS parent, @rownum := @rownum + 1 AS rownum
												FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
												JOIN (SELECT @pv:= :client, @rownum:=0)tmp
												WHERE t.`id`= @pv
										)c on c.`client_id` = p.`client_id`
										LEFT JOIN `product_options` as PO ON `p`.`option_id` = `PO`.`id`
										WHERE p.`processor_sub_group` = :subgroup 
										AND p.`group_code` = :group 
										AND p.`status` = 'active'
										AND p.`option_id` IN (
											SELECT processor_id 
											FROM `product` 
											WHERE `type_id` = :type 
											AND (`brand_id` = :brand OR `brand_id` IS NULL OR `brand_id` = 0) 
											AND `value_factor` = 'explicit price' 
											AND `value` IS NOT NULL 
											AND `status` = 'active'
										)
										ORDER BY p.`order` ASC, c.`rownum`"), $args);

                             Session::put('Processor', $order);

							if(count($proccessor) > 0){
								$arr[] = array('name' => 'Processor', 'item' => $proccessor, 'subgroup' => $productogn->processor_sub_group);
							}
                        }
                        //$count = count($proccessor);
                        return $arr;
                    }
                } else if (strpos($a, 'Screen') !== false || strpos($a, 'HDD') !== false || strpos($a, 'RAM') !== false || strpos($a, 'DVD') !== false || strpos($a, 'Processor') !== false) {

                    $pro = $value->group_code;
                    $exs = explode(',',$pro);                      

                    foreach ($exs as  $ex) {
						$group = $ex;
                          
						if(count($exs) > 1){
							$name = 'group';
						}else{
							$name = $group;
						}

						if ($group != '') {

							$Screen = DB::select(DB::raw('SELECT PO.description as description,p.*
								FROM `product_option_groups` p
								JOIN(
								SELECT t.`id` AS client_id
								, @pv:=t.`parent_id` AS parent
								, @rownum := @rownum + 1 AS rownum
								FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
								JOIN (SELECT @pv:= '.$client_id.', @rownum:=0)tmp
								WHERE t.`id`=@pv
								)c on c.`client_id` = p.`client_id`
								LEFT JOIN `product_options` as PO ON `p`.`option_id` = `PO`.`id`
								WHERE (`group_code` = "' . $group . '" AND `p`.`status`="active")
								ORDER BY p.`order` ASC, c.`rownum`'));

							$count = count($Screen);

							$sessiong = preg_replace('/[0-9]+/', '', $group);

							Session::put($sessiong, $order);

							$arr[] = array('name' => $group, 'item' => $Screen, 'count' => $count);

						}
					}
					return $arr;
							//echo "<pre>";print_r($arr);die;
				} else if (strpos($a, 'Defects') !== false) {
					$pro = $value->group_code;
					//echo $pro;die;
					if ($pro != '') {
						$Defects = DB::select(DB::raw('SELECT PO.description as description,p.*
							FROM `product_option_groups` p
							JOIN(
							SELECT t.`id` AS client_id
							, @pv:=t.`parent_id` AS parent
							, @rownum := @rownum + 1 AS rownum
							FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
							JOIN (SELECT @pv:= '.$client_id.', @rownum:=0)tmp
							WHERE t.`id`=@pv
							)c on c.`client_id` = p.`client_id`
							LEFT JOIN `product_options` as PO ON `p`.`option_id` = `PO`.`id`
							WHERE (`group_code` = "' . $pro . '" AND `p`.`status`="active")
							ORDER BY p.`order` ASC, c.`rownum`'));

						$count = count($Defects);
						Session::put('Defects', $order);
						$arr = array('name' => 'Defects', 'item' => $Defects, 'count' => $count);
						return $arr;
					}
				} else if (strpos($a, 'Services') !== false) {
					$client_id = Session::get('enterprise_client_id');
					
					$role = RoleUser::where('user_id',Auth::user()->id)->first()->role_id;            
					@$permissions = Auth::user()->permission;            
					$permission =  explode(',', @$permissions);

					$pro = $value->group_code;
					if ($pro != '') {
						$Services = DB::select(DB::raw('SELECT PO.description as description,p.*
							FROM `product_option_groups` p
							JOIN(
							SELECT t.`id` AS client_id
							, @pv:=t.`parent_id` AS parent
							, @rownum := @rownum + 1 AS rownum
							FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
							JOIN (SELECT @pv:= '.$client_id.', @rownum:=0)tmp
							WHERE t.`id`=@pv
							)c on c.`client_id` = p.`client_id`
							LEFT JOIN `product_options` as PO ON `p`.`option_id` = `PO`.`id`
							WHERE (`group_code` = "' . $pro . '" AND `p`.`status`="active")
							ORDER BY p.`order` ASC, c.`rownum`'));
						$count = count($Services);
						$arr = array('name' => 'Services', 'item' => $Services, 'count' => $count, 'role' => $role, 'permission' => $permission);
						return $arr;
					}					
				}
			}
		}
		return $arr;
    }

    public function EnterprisesSearch(Request $request) {
		$client_id = 0;
		$quote_id = 0;

		//check client id is set
		if (!Session::has('enterprise_client_id') || !is_numeric(Session::get('enterprise_client_id'))) {
			Toast::error('You do not have permission for this action.', 'Error');
			return redirect('/dashboard');
		} else {
			$client_id = Session::get('enterprise_client_id');
		}
		//check quote id is set
        if (Session::has('quote_id') && is_numeric(Session::get('quote_id'))) {
            $quote_id = Session::get('quote_id');
        }

		//validate client and quote 
		if($client_id <= 0 || $quote_id < 0){
			Toast::error('You do not have permission for this action.', 'Error');
			return redirect('/');
		}

		//check user has permission for the quote
		if(!$this->isUserValid($client_id, $quote_id)){
			Toast::error('You do not have permission for this action.', 'Error');
			return redirect('/');
		}

		//validate inputs
		if(!isset($request->pid) || !is_numeric($request->pid)){
			Toast::error('You do not have permission for this action.', 'Error');
			return redirect('/');
		}

        $clinetname = Client::find($client_id);
		$proModal = Product::where('model', $request->model)->first();

		//validate objects and user permission 
		if(null === $proModal || null === $clinetname || !$this->isUserValid($client_id)){
			Toast::error('You do not have permission for this action.', 'Error');
			return redirect('/');
		}

		$data = array(
			'model_id' => $request->pid
		);

		return $this->EnterprisesSteps($request, $data);
    }

    public function EnterprisesSteps(Request $request, $searcharray = null) {
          
        if ($this->filter($this->permission)) {

            $modal = $request->get('modal');
            $client_id = Session::get('enterprise_client_id');
            $QuoteItem = array();
            $Quote = array();
            $QuoteItemDetail = array();

            if (Session::has('quote_id')) {
                $quote_id = Session::get('quote_id');
                if ($quote_id != '') {
                    $QuoteItem = QuoteItem::with('model_image','option_model_image')->where('quote_id', '=', $quote_id)->get()->toArray();
                    $Quote = Quote::find($quote_id)->toArray();
                } else {
                    $quote_id = '';
                }
            } else {
                $quote_id = '';
            }

			if(!$this->isUserValid($client_id, ($quote_id == '') ? 0 : $quote_id)){
				Toast::error('You do not have permission for this client.', 'Error');
				return redirect('/dashboard');
			}

            $product = false;
            $brand = false;
            $other = false;

			//=========================== Code for Modeal Search ==================================//
			if(isset($searcharray) &&  $searcharray['model_id'] > 0){
				//retreive product detail
                $productitem = Product::find($searcharray['model_id']);
				if(null !== $productitem){
					$modelname = $productitem->model;
					$type_id = $productitem->type_id;
					$brand_id = $productitem->brand_id;
					$processor_id = $productitem->processor_id;
					$screen_id = $productitem->screen_id;
					$dvd_id = $productitem->dvd_id;
					$ram_id = $productitem->ram_id;
					$hdd_id = $productitem->hdd_id;
					
					 $EditSteps = $this->getSteps($productitem->type_id, $productitem->brand_id);

					 $EditlastStep = '';
					 $missingdata = 'Services';
					 if(!empty($EditSteps)){
						$num = count($EditSteps);
						while($num > 0){						
							$value = $EditSteps[$num - 1];						
							$num --;

							$arrstep = explode(',', strtolower($value->group_code));
							$arrstep = array_reverse($arrstep);
							foreach ($arrstep as $val) {
								if (strpos($val, 'processor') !== false && $processor_id == 0){								
									$EditlastStep = $value->order - 1;
									$missingdata = "Processor";
								} else if (strpos($val, 'screen') !== false && $screen_id == 0){
									$EditlastStep = $value->order - 1;
									$missingdata = "Screen Size";
								} else if (strpos($val, 'hdd') !== false && $hdd_id == 0) {
									$EditlastStep = $value->order - 1;
									$missingdata = "HDD";
								} else if (strpos($val, 'ram') !== false && $ram_id == 0){
									$EditlastStep = $value->order - 1;
									$missingdata = "RAM";
								} if (strpos($val, 'dvd') !== false && $dvd_id == 0){	
									$EditlastStep = $value->order - 1;
									$missingdata = "DVD";
								} else if (strpos($val, 'defects') !== false && $missingdata == 'Services'){
									$EditlastStep = $value->order - 1;
									$missingdata = "Defects";
								} else if (strpos($val, 'services') !== false && $missingdata == 'Services'){
									$EditlastStep = $value->order - 1;
								}
							}
						}
					}

					$PO = array('type' => $missingdata);

					$arr = array(
						'type_id'		=> $type_id,
						'brand_id'		=> $brand_id,
						'SearchType'	=> $PO['type'],
						'SearchId'		=> '',
						'SearchName'	=> '',
						'EditlastStep'	=> $EditlastStep,
						'editMode'		=> 'editMode',
						'productIfTrue'	=> $product,
						'brandIfTrue'	=> $brand,
						'processor_id'	=> $processor_id,
						'screen_id'		=> $screen_id,
						'dvd_id'		=> $dvd_id,
						'hdd_id'		=> $hdd_id,
						'ram_id'		=> $ram_id,
						'services_id'	=> '',
						'Defects'		=> '',
						'modelname'		=> $modelname,
						'model_id'		=> $searcharray['model_id']
					);

				   $json = json_encode($arr);

				   setcookie('data',$json, time() + (86400), "/");
				}
			}

			//=========================== Code for Edit Quote ==================================//
            else if(isset($request) && !empty($request->quote_edit_items) && $request->quote_edit_items != ''){
	
                if($request->quantity != null){
					$this->quantityUpdate($request);
					if($request->quote_edit_items == -1){
						return redirect('/enterprises/index?qiid='.$request->quoteItemId);
					}
					Session::put('quantity',$request->quantity);
					Session::put('oem_product',$request->product_oem);
					Session::put('oem_logistic',$request->logistic_oem);
					Session::put('oem_service',$request->service_oem);
                }

                $EditQuoteItemDetail = QuoteItem::find($request->quote_item_id);
               
                if($request->quote_edit_items == 'Defects'){
                    $PO = array('type'=>'Defects');
                }else{
                    $PO = ProductOption::find($request->quote_edit_items);
                }

				if(null !== $EditQuoteItemDetail){
					$type_id		= $EditQuoteItemDetail->type_id;
					$brand_id		= $EditQuoteItemDetail->brand_id;
					$processor_id	= $EditQuoteItemDetail->processor_id;
					$screen_id		= $EditQuoteItemDetail->screen_id;
					$dvd_id			= $EditQuoteItemDetail->dvd_id;
					$ram_id			= $EditQuoteItemDetail->ram_id;
					$hdd_id			= $EditQuoteItemDetail->hdd_id;
					$services_id	= $EditQuoteItemDetail->services_id;				

					if($PO['type'] == "Product"){
						$product = true;
					}else if($PO['type'] == "Brand"){
						$brand = true;
					}else if($PO['type'] == "DVD" || $PO['type'] == "HDD" || $PO['type'] == "Processor" || $PO['type'] == "RAM" || $PO['type'] == "Screen Size" || $PO['type'] == "Services" || $PO['type'] == "Defects"){

						$defect_id = array();
						$Defects = QuoteItemsDefect::where('quote_item_id',$request->quote_item_id)->get();
						foreach ($Defects as $Defect) {
							$defect_id[] = $Defect->defect_id;
						 }
						$defects = implode(',',$defect_id);
					}

					 $EditSteps = $this->getSteps($EditQuoteItemDetail->type_id, $EditQuoteItemDetail->brand_id);

					 $EditlastStep = '';

					 if(!empty($EditSteps)){
						foreach ($EditSteps as $key => $value) {

							if(strpos($value->group_code,$PO['type']) !== false){

								$EditlastStep = $value->order - 1;

							}else if (strpos($value->group_code, 'Screen') !== false || strpos($value->group_code, 'HDD') !== false && strpos($value->group_code, 'RAM') !== false ||  strpos($value->group_code, 'DVD') !== false ){

							   $EditlastStep = $value->order-1;
							}
						}
					}
				}

				$SearchType = @$PO['type'];
				$SearchId = @$PO['id'];
				$SearchName = @$PO['name'];

			   $arr = array(
				'type_id' => $type_id,
				'brand_id' => $brand_id,
				'SearchType' => $SearchType,
				'SearchId' => $SearchId,
				'SearchName' => $SearchName,
				'EditlastStep' => $EditlastStep,
				'editMode' => 'editMode',
				'productIfTrue'=>$product,
				'brandIfTrue' => $brand,
				'processor_id' =>  $processor_id,
				'screen_id' =>  $screen_id,
				'dvd_id' =>  $dvd_id,
				'hdd_id' =>  $hdd_id,
				'ram_id' =>  $ram_id,
				'services_id' =>  $services_id,
				'Defects' => @$defects,
				'model_id' => 0
				);

			   $json = json_encode($arr);

			   setcookie('data',$json, time() + (86400), "/");
			}else{
				setcookie('data', '', -10, "/");
				setcookie('brand_id', '', -10, "/");
				setcookie('SearchType', '', -10, "/");
				setcookie('SearchId', '', -10, "/");
				setcookie('SearchName', '', -10, "/");
				setcookie('EditlastStep', '', -10, "/");
				setcookie('editMode', '',-10, "/");
				setcookie('model_id', '',-10, "/");
			}
			//========================End code for EditQuote =========================// 

			if (isset($request) && isset($request->quote_item_id) && $request->quote_item_id != 0) {
				$quoteItemId = $request->quote_item_id;
				if ($quoteItemId != '') {
					$QuoteItemDetail = QuoteItem::find($quoteItemId);
				} else {
					$quoteItemId = '';
				}
			} else {
				$quoteItemId = '';
				Session::forget('quantity');									
				Session::forget('oem_product');
				Session::forget('oem_logistic');
				Session::forget('oem_service');
			}

			if (!empty($client_id)) {
				$clinetname = Client::find($client_id);

				if (!empty($clinetname)) {

					$Products = array();
					$resultset = DB::select(DB::raw("SELECT p.id, p.`type`, p.`name`, gp.`order`, p.`description`, p.`image`, gp.`status`
									FROM `product_options` p
									JOIN `product_option_groups` gp on gp.`option_id` = `p`.`id`
									JOIN(
									SELECT t.`id` AS client_id, @pv:=t.`parent_id` AS parent, @rownum := @rownum
									+ 1 AS rownum
									FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
									JOIN (SELECT @pv:= '$client_id', @rownum:=0)tmp
									WHERE t.`id`=@pv
									)c on c.`client_id` = gp.`client_id`
									WHERE p.`type` =  'Product' AND p.`status` = 'active'
									ORDER BY p.`id`, c.`rownum`"));
                
					$previd = 0;
					foreach($resultset as $brand){
						if($brand->id != $previd){
							if($brand->status == 'active'){
								array_push($Products, $brand);
							}
						}
						$previd = $brand->id;
					}
					usort($Products, function ($a, $b) {
						return $a->order - $b->order;
					});

					return view('Enterprises/EnterprisesSteps', array(
						'Products' => $Products,
						'clinetname' => $clinetname,
						'client_id' => $client_id,
						'quote_id' => $quote_id,
						'QuoteItem' => $QuoteItem,
						'Quote' => $Quote,
						'quoteItemId' => $quoteItemId,
						'QuoteItemDetail' => $QuoteItemDetail,
						'brand'=>$brand,
						'product' => $product
						));
				} else {
					Toast::error('Invalid client Id !!!.', 'Error');
					return redirect('/dashboard');
				}
			} else {
				return redirect('/dashboard');
			}
		}else {
			
			return redirect()->back();
		}     
    }

    public function EnterprisesSteps2(Request $request) {

        if ($this->filter($this->permission)) {
			$quote_id = 0;
			$client_id = 0;
		    if (Session::has('quote_id') && is_numeric(Session::get('quote_id'))) {
                $quote_id = Session::get('quote_id');
            }
            if(Session::has('enterprise_client_id')){
                $client_id = Session::get('enterprise_client_id');
            }
			
			if(!$this->isUserValid($client_id, $quote_id)){
				Toast::error('You do not have permission for this client.', 'Error');
				return redirect('/dashboard');
			}


            $id = $request->get('id');
            $brand_id = array();

            if ($request->get('id') != null) {
                $product = ProductOption::find($id);

				$args = array(
					'client'	=> $client_id,
					'product'	=> $id
				);

				$results = array();
                $resultset = DB::select(DB::raw("SELECT p.id, p.`type`, p.`name`, gp.`order`, p.`description`, p.`image`, gp.`status`
                    FROM `product_options` p
                    JOIN `product_option_groups` gp on gp.`option_id` = `p`.`id`
					JOIN `product_options_link` pol on pol.`brand_id` = `p`.`id`
                    JOIN(
						SELECT t.`id` AS client_id, @pv:=t.`parent_id` AS parent, @rownum := @rownum
						+ 1 AS rownum
						FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
						JOIN (SELECT @pv:= :client, @rownum:=0)tmp
						WHERE t.`id`=@pv
                    )c on c.`client_id` = gp.`client_id`
                    WHERE p.`type`= 'Brand' AND p.`status` = 'active' AND pol.type_id = :product
                    ORDER BY p.`id`, c.`rownum`"), $args);
                
				$previd = 0;
				foreach($resultset as $brand){
					if($brand->id != $previd){
						if($brand->status == 'active'){
							array_push($results, $brand);
						}
					}
					$previd = $brand->id;
				}

				usort($results, function ($a, $b) {
					return $a->order - $b->order;
				});

                if (isset($request->quote_item_id) && $request->quote_item_id != 0) {
                    $quoteItemId = $request->quote_item_id;
                    if ($quoteItemId != '') {
                        $QuoteItemDetail = QuoteItem::find($quoteItemId);
                    } else {
                        $quoteItemId = '';
                    }
                } else {
                    $quoteItemId = '';
                }

                $Product_id = $id;

                $html = View::make('/Enterprises/EnterprisesSteps2', compact('product', 'results', 'Product_id', 'QuoteItemDetail', 'quoteItemId'))->render();
                return Response::json(['html' => $html]);
            } else {
                $products = ProductOption::where('type', '=', 'Product')->where('status','active')->get();
                return view('Enterprises/EnterprisesSteps', array(
                    'products' => $products,
                    ));
            }
        }else {
            Toast::error('You do not have permission fo this client.', 'Error');
            return redirect('/dashboard');
        }    
    }

	/* 
	this function is to validate the step sequence for an existing enterprise item if the brand 
	if changed. 
	The step process can be configured for product type and/or brand therefore if the step process doesn't change 
	then update brand and progress 
	*/
	public function stepCheck(Request $request){
		$client_id = 0;
		$quote_id = 0;

		//check client id is set
		if (!Session::has('enterprise_client_id') || !is_numeric(Session::get('enterprise_client_id'))) {
			$response = array('status' => 'error', 'message' => 'You do not have permission !!!');
			return json_encode($response);
		} else {
			$client_id = Session::get('enterprise_client_id');
		}
		//check quote id is set
        if (!Session::has('quote_id') || !is_numeric(Session::get('quote_id'))) {
			$response = array('status' => 'error', 'message' => 'You do not have permission !!!');
			return json_encode($response);         
        } else {
            $quote_id = Session::get('quote_id');
        }

		//validate client and quote 
		if($client_id <= 0 || $quote_id <= 0){
			$response = array('status' => 'error', 'message' => 'You do not have permission !!!');
			return json_encode($response); 
		}

		//check user has permission for the quote
		if(!$this->isUserValid($client_id, $quote_id)){
			$response = array('status' => 'error', 'message' => 'You do not have permission !!!');
			return json_encode($response);
		}


	    $type_id = (isset($request->pro1) && is_numeric($request->get('pro1'))) ? $request->get('pro1') : 0;
        $brand_id =(isset($request->pro2) && is_numeric($request->get('pro2'))) ?  $request->get('pro2') : 0;
		$item_id = (isset($request->quote_item_id) && is_numeric($request->get('quote_item_id'))) ? $request->get('quote_item_id') : 0;

		//return an error if the inputs are invalid
		if($type_id == 0 || $brand_id == 0 || $item_id == 0){
			return Response::json(array('status' => 'error')); 
		}

		// validate and populate varaibles from request object 
		$obj = $this->validateOption($type_id, 'Product');
		$type_id = $obj["id"];
		$type_name = $obj["name"];
		$obj = $this->validateOption($brand_id, 'Brand');
        $brand_id = $obj["id"];
        $brand_name = $obj["name"];

		//return an error if the inputs are invalid
		if($type_id == 0 || $brand_id == 0){
			return Response::json(array('status' => 'error')); 
		}

		// return an error if not step information is returned
		$stepsProgress = $this->getSteps($type_id, $brand_id);
		if(null === $stepsProgress && count($stepsProgress) == 0){
			return Response::json(array('status' => 'error')); 
		}

		//load the quote item and return an error if it doesn't exit
		$quoteitem = QuoteItem::find($item_id);
		if(null === $quoteitem){
			return Response::json(array('status' => 'error'));
		}

		// check is the user is valid for the client
		$quote = Quote::find($quoteitem->quote_id);
		if(null === $quote || !$this->isUserValid($quote->client_id)){
			return Response::json(array('status' => 'error'));
		}
		
		//find the step id based on the brand and product type selection
		//check if there is an item is being edited and if the step id is the same.
		$step_id = $stepsProgress[0]->id;
		$item_step_id = $quoteitem->step_id;
		
		//step process is the same therefore update brand, reacalculate, and return a response to jump t the summary page
		if($step_id == $item_step_id){
			$quoteitem->brand_id = $brand_id;
			$quoteitem->brand_name = $brand_name;
			$model = 0;
			$model_id = '';

			$calculate = new CalculationsController();
			//check product id
			$re = $calculate->productId($quote->client_id, $quoteitem->type_id, $brand_id, $quoteitem->processor_id, $quoteitem->screen_id, $quoteitem->hdd_id, $quoteitem->dvd_id, $quoteitem->ram_id, 1, 0);
			if($re){
				$product_id = $re[0]->id;
				if(!empty($re[0]->model) && $model_id == 0){
					$model = $re[0]->model;
					$model_id = $product_id;
				}
			}else{
				$response = array('status'=>'error', 'message'=>'You can not create quote for this client due to no relavent data found in product table !!!');
				return json_encode($response);
			}
			$quoteitem->product_id = $product_id;
			$quoteitem->model_id = $model_id;
			$quoteitem->model_name = $model;
			$quoteitem->save();

			$calculate->Recalculate($quoteitem->quote_id, $quoteitem->id);
			return Response::json(array('status' => 'equal'));
		} else {
			return Response::json(array('status' => 'new'));
		}
	}

    public function totalSteps(Request $request) {
		// check if user logged in
		if (!$this->filter($this->permission)) {
			return '';
		} 

		$quote_id = 0;
		if (Session::has('quote_id') && is_numeric(Session::get('quote_id'))) {
            $quote_id = Session::get('quote_id');
        }
		// check if user has permission for this client and/or quote
		if(!$this->isUserValid(Session::get('enterprise_client_id'), $quote_id)){
			return '';
		}

        $pro1 = $request->get('pro1');
        $pro2 = $request->get('pro2');
        $pro3 = $request->get('pro3');
        $pro4 = $request->get('pro4');
        $pro5 = $request->get('pro5');
        $pro6 = $request->get('pro6');
        $pro7 = $request->get('pro7');
        $pro8 = $request->get('pro8');
        $pro9 = $request->get('pro9');
        $lastStep = $request->get('lastStep');
		$modelid = $request->get('modelid');

        $stepPro = $this->getStepsWithProcessorName($pro1, $pro2, $lastStep);
        $Product = $Brand = $Processor = $Screen = $DVD = $HDD = $RAM = $Services = $ServiceDefault = '';
        $Defects = array();
		$Model = (object)array('id' => 0, 'name' => '');

		if (isset($modelid) && is_numeric($modelid) && $modelid > 0) {
            $Model = Product::find($modelid);
        }

        if (isset($pro1) && is_numeric($pro1) && $pro1 > 0) {
            $Product = ProductOption::find($pro1);
        }
        if (isset($pro2) && is_numeric($pro2) && $pro2 > 0) {
            $Brand = ProductOption::find($pro2);
        }

		$stepsProgress = $this->getSteps($pro1, $pro2);

        if (isset($pro3) && $pro3 != 0) {
            $Processor = ProductOption::find($pro3);
            $Processor->current = Session::get('Processor');
            Session::flash('Processor_old',$Processor->id);
        }
        if (isset($pro4) && $pro4 != 0) {
            $Screen = ProductOption::find($pro4);
            $Screen->current = Session::get('Screen');
            Session::flash('Screen_old',$Screen->id);
        }
        if (isset($pro5) && $pro5 != 0) {
            $DVD = ProductOption::find($pro5);
            $DVD->current = Session::get('DVD');
            Session::flash('DVD_old',$DVD->id);

        }
        if (isset($pro6) && $pro6 != 0) {
            $HDD = ProductOption::find($pro6);
            $HDD->current = Session::get('HDD');
            Session::flash('HDD_old',$HDD->id);

        }
        if (isset($pro7) && $pro7 != 0) {
            $RAM = ProductOption::find($pro7);
            $RAM->current = Session::get('RAM');
            Session::flash('RAM_old',$RAM->id);
        }
        if (isset($pro8) && $pro8 != 0) {

            $ids = explode(",", $pro8);

            $Defects = ProductOption::whereIn('id', $ids)->get();
            $Defects->current = Session::get('Defects');

            Session::flash('Defects_Old',$Defects);
        }

        if (isset($pro9) && is_numeric($pro9) && $pro9 > 0) {
            $Services = ProductOption::find($pro9);
            Session::flash('services_old', $Services->id);
        } else {
			// check for default service fee (need product type and brand to get step)
			// support only eixsts for services default 
			if (isset($pro1) && is_numeric($pro1) && $pro1 > 0 && isset($pro2) && is_numeric($pro2) && $pro2 > 0) {
				if(Session::has('ServiceDefault') && is_numeric(Session::get('ServiceDefault'))){
					// retreive the default service fee
					$ServiceDefault = Session::get('ServiceDefault');
				} else {
					if(null !== $stepsProgress && count($stepsProgress) > 0){
						//check if ther eis a default service fee
						$client_id = Session::get('enterprise_client_id');
						$args = array(
							'client'	=> $client_id,
							'stepid'	=> $stepsProgress[0]->id
						);						

						$resultset = DB::select(DB::raw('SELECT * 
									FROM `quote_step_defaults` sd
									JOIN(
										SELECT t.`id` AS client_id
											, @pv:=t.`parent_id` AS parent
											, @rownum := @rownum + 1 AS rownum
										FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
										JOIN (SELECT @pv:= :client, @rownum:=0)tmp
										WHERE t.`id`=@pv
									)c ON sd.client_id = c.client_id
									WHERE sd.step_id = :stepid
									ORDER BY rownum LIMIT 1'), $args);
						if(!empty($resultset)){
							$ServiceDefault = $resultset[0]->option_id;
							Session::put('ServiceDefault', $ServiceDefault);
						}
					}
				}
			}

			if(is_numeric($ServiceDefault) && $ServiceDefault > 0){
				$Services = ProductOption::find($ServiceDefault);
				Session::flash('services_old', $Services->id);
			}
		}

        $QuoteItemDetail = '';
        $QID = array();
        if (isset($request->quote_item_id) && $request->quote_item_id != 0) {
            $quoteItemId = $request->quote_item_id;
                
            if ($quoteItemId != '') {
                $QuoteItemDetail = QuoteItem::find($quoteItemId);
                $QuoteItemDefects = QuoteItemsDefect::where('quote_item_id', $quoteItemId)->get();
                foreach ($QuoteItemDefects as $QuoteItemDefect) {
                    $QID[] = $QuoteItemDefect->option_id;
                }
            } else {
                $quoteItemId = '';
            }
        } else {
            $quoteItemId = '';
        }

        
        $lastStep = isset($stepPro['order']) ? $stepPro['order'] : $request->get('lastStep');
		$args = array(
                'Product' => $Product,
                'Brand' => $Brand,
                'Processor' => $Processor,
                'Screen' => $Screen,
                'DVD' => $DVD,
                'HDD' => $HDD,
                'RAM' => $RAM,
                'Defects' => $Defects,
                'Services' => $Services,
                'stepPro' => $stepPro,
                'stepsProgress' => $stepsProgress,
                'lastStep' => $lastStep,
                'QuoteItemDetail' => $QuoteItemDetail,
                'QID' => $QID,
				'Model' => $Model,
				'ServiceDefault' => $ServiceDefault,
				'isServiceDefault' => (Session::has('ServiceDefault') && is_numeric(Session::get('ServiceDefault'))) ? Session::get('ServiceDefault') : 0
        );

        if (count($stepPro) > 0) {
            $html = View::make('/Enterprises/EnterprisesSteps3', $args)->render();
        } else {
            $html = View::make('/Enterprises/EnterprisesSteps10', $args)->render();            
        }
		return Response::json(['html' => $html]);
    }

    public function quantityUpdate(Request $request) {        

		//check user have permissions
		$role = RoleUser::where('user_id',Auth::user()->id)->first()->role_id;            
		@$permissions = Auth::user()->permission;            
		$permission =  explode(',', @$permissions);
		if(!($role == 2 || ($role == 3 && @in_array(2,$permission)))){
			return Response::json(array('status' => 'error', 'message' => 'invalid permission'));
		}

		//validate quote item exists
		$QuoteItem = QuoteItem::find($request->quoteItemId);
		if(null === $QuoteItem){
			return Response::json(array('status' => 'error', 'message' => 'invalid item'));
		}

		//validate input quote id match quote of the item being updated
		if(!isset($request->quote_id) || !is_numeric($request->quote_id) || $request->quote_id != $QuoteItem->quote_id){
			return Response::json(array('status' => 'error', 'message' => 'invalid quote for item'));
		}

		//validate that the user has permission for this client.
		$Quote = Quote::find($QuoteItem->quote_id);
		if(null === $Quote || !$this->isUserValid($Quote->client_id, $Quote->id)){
			return Response::json(array('status' => 'error', 'message' => 'invalid user'));
		}

		//validate margin and quantity information
		$quantity = (isset($request->quantity) && is_numeric($request->quantity) && $request->quantity > 0) ? $request->quantity : $QuoteItem->quantity;
		$oem_p =  (isset($request->product_oem) && is_numeric($request->product_oem)) ? $request->product_oem : $QuoteItem->unit_margin_oem_product;
		$oem_s = (isset($request->service_oem) && is_numeric($request->service_oem)) ? $request->service_oem : $QuoteItem->services_oem_margin;
		$oem_l = (isset($request->logistic_oem) && is_numeric($request->logistic_oem)) ? $request->logistic_oem : $QuoteItem->logistics_oem_margin;

		//apply values to quote item
		$QuoteItem->quantity				= $quantity;
		$QuoteItem->unit_margin_oem_product = $oem_p;
		$QuoteItem->services_oem_margin		= $oem_s;
		$QuoteItem->logistics_oem_margin	= $oem_l;
		
		//QuoteItem::where('id', '=', $request->quoteItemId)->update(['quantity' => $quantity], ['unit_margin_oem_product' => $oem_p], ['services_oem_margin' => $oem_s], ['logistics_oem_margin' => $oem_l]);

		if ($QuoteItem->save()) {	
			// recalulate the item.	
			$calculate = new CalculationsController();            
			$calculate->Recalculate($QuoteItem->quote_id, $QuoteItem->id);

			return Response::json(array('status' => 'success'));
		} else {
			return Response::json(array('status' => 'error'));
		}
		
    }

	//retreive the clients  details to display for the quote setting page for a new 
    public function store_enterprise() {

        if ($this->filter($this->permission)) {

            //===========================Permission============================================//
            $role = RoleUser::where('user_id',Auth::user()->id)->first()->role_id;
            @$permissions = Auth::user()->permission;
            $permission =  explode(',', @$permissions);
            //==========================Permission============================================//

            if (Session::has('enterprise_client_id')) {
				$client = Client::find(Session::get('enterprise_client_id'));

				if(null === $client || !$this->isUserValid($client->id)){
				    Toast::error('You do not have permission for this action.', 'Error');
					return redirect('/dashboard');
				}

				$calculate = new CalculationsController();  

				//if margin values are null then retreive from parent
				$collection_size	= (!empty($client->ave_collect_size) && is_numeric($client->ave_collect_size)) ? $client->ave_collect_size : $calculate->getClientData($client, 'ave_collect_size');
				$product_margin		= (!empty($client->margin_oem_product) && is_numeric($client->margin_oem_product)) ? $client->margin_oem_product : $calculate->getClientData($client, 'margin_oem_product');
				$service_margin		= (!empty($client->margin_oem_service) && is_numeric($client->margin_oem_service)) ? $client->margin_oem_service : $calculate->getClientData($client, 'margin_oem_service');
				$logistic_fee		= (!empty($client->margin_oem_logistic) && is_numeric($client->margin_oem_logistic)) ? $client->margin_oem_logistic : $calculate->getClientData($client, 'margin_oem_logistic');

                if (!empty($client)) {
					          
					$Countries = $calculate->getCountriesforClient($client->id);
                    $currency = Country::find($client->country);
                    $Regions = CountryRegion::where('country_id', '=', $client->country)->get();
                    return view('/Enterprises.clientIndex', compact('client', 'Countries', 'Regions', 'currency','role','permission', 'collection_size', 'product_margin', 'service_margin', 'logistic_fee'));
                } else {
                    Toast::error('Invalid client Id !!!.', 'Error');
                    return redirect('/dashboard');
                }
            } else {
                //return redirect('/dashboard');
                Auth::logout();
            	return redirect('/login');
            }
        } else {
            
            return redirect()->back();
        }    
    }		

	// the methos is called when a new enterprise quote is created 
	// when the settings page is submitted the details are saved to the session
	// and the nthe user is redirected to configure and create and item
    public function SaveEnterpriseQuote(Request $request) {
		//check if user logged in
		if (!$this->filter($this->permission)) {
            Toast::success('You do not have permissions for this action.', 'Error');
            return redirect('/dashboard');
		} 

		//check client id is set
		$client_id = 0;
		if (isset($request->client_id) || is_numeric($request->client_id)) {
			$client_id = $request->client_id;
		}

		//check if user has permission for the client 
		if(!$this->isUserValid($client_id)){
			Toast::success('You do not have permissions for this action.', 'Error');
			return redirect('/dashboard');
		}

        Session::put(array(
            'name'				=> $request->name,
            'country'			=> $request->country,
            'region'			=> $request->region,
            'collection_size'	=> $request->collection_size,
            'currency'			=> $request->currency,
            'product_margin'	=> $request->product_margin,
            'service_margin'	=> $request->service_margin,
            'logistic_fee'		=> $request->logistic_fee,
            'note'				=> $request->note,
            'display_level'		=> $request->display_level,
            )
        );
        Session::put('enterprise_client_id', $client_id);
        Session::put('quote_id', '');

        return redirect('/enterprises/quote/step');
    }

	//deletes and item from a quote
    public function delete($id = null) {
		//check if user logged in
		if (!$this->filter($this->permission)) {
            Toast::success('You do not have permissions for this action.', 'Error');
            return redirect('/dashboard');
		}

        if ($id != null && is_numeric($id)) {
			$quoteitem = QuoteItem::find($id);
			//check quote item exists
			if(null === $quoteitem){
				Toast::success('Unable to delete item.', 'Error');
				return redirect('/enterprises/index');
			}

			//check if user has permission for the client and/or quote
			$quote = Quote::find($quoteitem->quote_id);
			if(null === $quote || !$this->isUserValid($quote->client_id, $quote->id)){
				Toast::success('Unable to delete item.', 'Error');
				return redirect('/enterprises/index');
			}
			
			//delete the item and related defects
            QuoteItem::destroy($id);
            QuoteItemsDefect::where('quote_id', '=', $id)->delete();
            Toast::success('Item has been deleted.', 'Success');
            return redirect('/enterprises/index');

        } else {
			Toast::success('Unable to delete item.', 'Error');
			return redirect('/enterprises/index');
		}
    }

	public function download($id = null) {
		if (!$this->filter($this->permission)) {
			return;
		}
        if ($id != null && is_numeric($id)) {
			$Quote = QuoteManagement::find($id)->toArray();

			if(null === $Quote || !$this->isUserValid($Quote['client_id'], $id)){
				return;
			}
            
            $QuoteItem = QuoteItem::with('QuoteItemsDefect')->where('quote_id', '=', $id)->get()->toArray();
            $client = Client::find($Quote['client_id'])->toArray();
            $quote_status = QuoteStatus::find($Quote['status']);
            $pdf = PDF::loadView('/Enterprises/download', compact('Quote', 'QuoteItem', 'quote_status', 'client'));
            return $pdf->download('QuoteSummary.pdf');
        }
    }

	/*
	returns an array of the id, name and product option object  give an id and the product type.
	This is used to validate that the id is numeric and matches a product_option of the  type specified
	*/
	private function validateOption($id, $type){
		if(isset($id) && is_numeric($id) && $id > 0){
			$option = ProductOption::where([['type', '=', $type], ['id', '=', $id]])->first();
			if(null !== $option){
				return array('id' => $option->id, 'name' => $option->name, 'option' =>$option);
			}
		}
		return array('id' => 0, 'name' => '', 'option' => null);
	}

	/* 
	Determines if the current logged-in user has the appropriate permisions for the specified client
	if so the True is returned else false  
	*/
	private function isUserValid($client_id, $quote_id = 0){	
		//validate client id
		if (!empty($client_id) && is_numeric($client_id) && $client_id > 0) {
			// get the user id	
			$userID = Auth::user()->id;
			// check if user can do action for the client 			
			$cu = ClientUser::where([['client_id', '=', $client_id], ['user_id', '=', $userID]])->first();
			if(null !== $cu){ 
				if($quote_id === 0){
					return true; 

				//validate that user can access the quote if specified
				// i.e. have public role (3) but is the user who created the quote or role of 1 (admin) or 2 (manager)
				} else if (!empty($quote_id) && is_numeric($quote_id) && $quote_id > 0) {
					$role = RoleUser::where('user_id', Auth::user()->id)->first()->role_id; 
					if(null !== $role && $role != 3){
						return true;
					}

					$quote = Quote::where('id', '=', $quote_id)->where('user_id', '=', $userID)->first();	
					if(null !== $role && null !== $quote && $role === 3){
						return true;
					}	
				}
			}
		}
		return false;
	}

	public function test(Request $request){
		$calculate = new CalculationsController();

		//print_r($calculate->productId(27, 175, 1, 107, 0, 0, 0, 0, 1, 126, 3));
		//$calculate->productId(1, 175, 1, 121, 0, 0, 0, 0, 1, 0);
		//echo ($calculate->getClientData(1, 'margin_oem_logistic'));
		//echo ($calculate->getClientData(1, 'ave_collect_size'));
		//$calculate = new CalculationsController();
		$calculate->Recalculate(602);

		//print_r($this->validateOption(null, 'Product'));
		die();
	}

    public function getQuoteTotal(Request $request) {
		//validate  user is logged in
		if (!$this->filter($this->permission)) {
			Toast::error('You do not have permission.', 'Error');
			return redirect('/dashboard');	
		}

        $calculate = new CalculationsController();
        $userID = Auth::user()->id;
        $clntid = (isset($request['client_id']) && is_numeric($request['client_id']) && $request['client_id'] > 0) ? $request['client_id'] : 0;

		//permission check
        if(!$this->isUserValid($clntid)){
            $response = array('status'=>'ProductIdIssue', 'message'=>'We are unable to progress with your request at this time.');
            echo json_encode($response);
			die;
        }

        // validate and populate varaibles from request object 
		$obj = $this->validateOption($request['Product'], 'Product');
		$type_id = $obj["id"];
		$type_name = $obj["name"];
		$obj = $this->validateOption($request['Brand'], 'Brand');
        $brand_id = $obj["id"];
        $brand_name = $obj["name"];
		$obj = $this->validateOption($request['Processor'], 'Processor');
        $processor_id = $obj["id"];
        $processor_name = $obj["name"];
		$obj = $this->validateOption($request['Screen'], 'Screen Size');
        $screen_id = $obj["id"];
        $screen_name = $obj["name"];
		$obj = $this->validateOption($request['DVD'], 'DVD');
        $dvd_id = $obj["id"];
        $dvd_name = $obj["name"];
		$obj = $this->validateOption($request['HDD'], 'HDD');
        $hdd_id = $obj["id"];
        $hdd_name = $obj["name"];
		$obj = $this->validateOption($request['RAM'], 'RAM');
        $ram_id = $obj["id"];
        $ram_name = $obj["name"];
		$obj = $this->validateOption($request['Services'], 'Services');
        $services_id = $obj["id"];
        $services_name = $obj["name"];
		$model_id = 0;
		$model = '';
		$product_id = 0;

		if(isset($request['modelid']) && is_numeric($request['modelid']) && $request['modelid'] > 0){
			$mobj = Product::find($request['modelid']);
			if(null !== $mobj){
				$model_id = $mobj->id;
				$model = $mobj->model;
			}
		}
		
		// return step information
		$stepsProgress = $this->getSteps($type_id, $brand_id);
		$step_id = 0;
		if(null !== $stepsProgress && count($stepsProgress) > 0){
			$step_id = $stepsProgress[0]->id; 
		}

        $clientsData = Client::find($clntid);
        $product_margin	= 0;
		$service_margin	= 0;
		$logistic_fee	= 0;
		$country        = $clientsData->country;

        if (isset($request['quote_id']) && is_numeric($request['quote_id']) && $request['quote_id'] > 0) {
            $quote = Quote::find($request['quote_id']);
            
            $product_margin	= $quote->margin_oem_product;
			$service_margin	= $quote->margin_oem_service;
			$logistic_fee	= $quote->margin_oem_logistic;
        } else {
            $quote = new Quote;		

			$name				= Session::has('name') ? Session::get('name'):  $clientsData->name;
			$country			= Session::has('country') ? Session::get('country'):  $clientsData->country;
			$region				= Session::has('region')? Session::get('region'):  $clientsData->region;
			$collection_size	= Session::has('collection_size')?Session::get('collection_size') : $calculate->getClientData($clntid, 'ave_collect_size');
			$currency			= Session::has('currency')? Session::get('currency'): $quote->currency_symbol;
			$product_margin		= Session::has('product_margin') && is_numeric(Session::get('product_margin')) ? Session::get('product_margin') : $calculate->getClientData($clntid, 'margin_oem_product');
			$service_margin		= Session::has('service_margin') && is_numeric(Session::get('service_margin'))  ? Session::get('service_margin') : $calculate->getClientData($clntid, 'margin_oem_service');
			$logistic_fee		= Session::has('logistic_fee') && is_numeric(Session::get('logistic_fee'))  ? Session::get('logistic_fee') : $calculate->getClientData($clntid, 'margin_oem_logistic');

			//check if there a cursomter speciic exchange rate
			$args = array(
				'client'	=> $clntid,
				'country'	=> $country
			);

			$ExcRate = 0;
			$results = DB::select(DB::raw("SELECT * 
				FROM `client_country_rates` ccr
				JOIN(
					SELECT t.`id` AS client_id, @pv:=t.`parent_id` AS parent, @rownum := @rownum + 1 AS rownum
					FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
					JOIN (SELECT @pv:= :client, @rownum:=0)tmp
					WHERE t.`id`=@pv
				)c on c.`client_id` = ccr.`client_id`
				WHERE country_id = :country AND (expires IS NULL OR expires > now())
				ORDER BY c.`rownum`
				LIMIT 1"), $args);
			if(!empty($results) && isset($results[0]->exchangerate)){
				$ExcRate = $results[0]->exchangerate;
			} else {
				$ExcRate =  Country::find($country)->currency_rate;
			}
 
			if($currency == 'US$' || $ExcRate == 0){
				$currency_symbol = '$';
				$ExcRate = 1;
			}else{
				$currency_symbol = Country::find($country)->currency_symbol;
			}

			$quote->client_id = $clntid;
			$quote->user_id = $userID;
			$quote->country = $country;
			$quote->region = $region;
			$quote->currency_symbol = @$currency_symbol;
			$quote->currency_rate = $ExcRate;
			$quote->status = 1;
			$quote->ave_collect_size = $collection_size;
			$quote->margin_oem_product = $product_margin;
			$quote->margin_oem_logistic = $logistic_fee;
			$quote->margin_oem_service = $service_margin; 
			$quote->total_value = 0;
			$quote->total_items = 0;
			$quote->name = $name;
			$quote->save();
		}

		// find the product matching the specified configuration  
        $re = $calculate->productId($clntid, $type_id, $brand_id, $processor_id, $screen_id, $hdd_id, $dvd_id, $ram_id, 1, $model_id, $country);
        if($re){
            $product_id = $re[0]->id;
			if(!empty($re[0]->model) && $model_id == 0){
				$model = $re[0]->model;
				$model_id = $product_id;
			}
        }else{
            $response = array('status'=>'ProductIdIssue', 'message'=>'You can not create quote for this client due to no relavent data found in product table !!!');
            echo json_encode($response);
			die;
        }

        $q_id = $quote->id;
        $q_name = $quote->name;
        if (isset($request['quote_item_id']) && $request['quote_item_id'] != '' && $request['quote_item_id']!='undefined') {
            $quoteItem = QuoteItem::find($request['quote_item_id']);
        } else {
            $quoteItem = new QuoteItem;
        }    

        if(Session::has('quantity')){
            $quantity =  Session::get('quantity');
        }else{
            $quantity = 1;
        }

		$quoteItem->quote_id		= $quote->id;
        $quoteItem->status			= 1;
        $quoteItem->quantity		= $quantity;
        $quoteItem->type_id			= $type_id;
        $quoteItem->type_name		= $type_name;
        $quoteItem->brand_id		= $brand_id;
        $quoteItem->brand_name		= $brand_name;
        $quoteItem->processor_id	= $processor_id;
        $quoteItem->processor_name	= $processor_name;
        $quoteItem->screen_id		= $screen_id;
        $quoteItem->screen_name		= $screen_name;
        $quoteItem->dvd_id			= $dvd_id;
        $quoteItem->dvd_name		= $dvd_name;
        $quoteItem->hdd_id			= $hdd_id;
        $quoteItem->hdd_name		= $hdd_name;
        $quoteItem->ram_id			= $ram_id;
        $quoteItem->ram_name		= $ram_name;
        $quoteItem->services_id		= $services_id;
        $quoteItem->services_name	= $services_name;
        $quoteItem->product_id		= $product_id;
        $quoteItem->model_id		= $model_id;
		$quoteItem->model_name		= $model;
		$quoteItem->step_id			= $step_id;
		$quoteItem->use_committed_price		= 0;
		$quoteItem->isPUP					= 0;
		$quoteItem->is_model_price			= ($product_id == $model_id) ? 1 : 0;
		$quoteItem->logistics_oem_margin	= Session::has('oem_logistic') && is_numeric(Session::get('oem_logistic')) ? Session::get('oem_logistic') : $logistic_fee;
		$quoteItem->services_oem_margin		= Session::has('oem_service') && is_numeric(Session::get('oem_service')) ? Session::get('oem_service') : $service_margin;
		$quoteItem->unit_margin_oem_product	= Session::has('oem_product') && is_numeric(Session::get('oem_product')) ? Session::get('oem_product') : $product_margin;
        $quoteItem->save();	

		//clear any session varaible set for this quote
		Session::forget('quantity');									
		Session::forget('oem_product');
		Session::forget('oem_logistic');
		Session::forget('oem_service');				
		Session::forget('step_id');
		Session::forget('ServiceDefault');

        $QuoteItemID = $quoteItem->id; //$request->QuoteItemDetail;
        QuoteItemsDefect::where('quote_item_id', $QuoteItemID )->delete();
       
        if (count($request['Defects']) > 0) {
            foreach ($request['Defects'] as $key => $value) {
				$obj = $this->validateOption($value, 'Defects');
				if($obj["id"] > 0){
                    $quoteItemDefect = new QuoteItemsDefect;
                    $quoteItemDefect->quote_id		= $quote->id;
                    $quoteItemDefect->defect_id		= 0;
                    $quoteItemDefect->defect_name	= $obj["name"];
                    $quoteItemDefect->defect_value	= 0;
                    $quoteItemDefect->quote_item_id = $quoteItem->id;
					$quoteItemDefect->option_id		= $obj["id"];
                    $quoteItemDefect->save();            
				}               
            }
        }

        $calculate->Recalculate($quote->id, $QuoteItemID);
		//reload item to get margin detail for output
		$quoteItem = QuoteItem::find($QuoteItemID);

        $total = 0;
        $productsfee = 0;
        $servicesfee = 0;
        $logisticsfee = 0;

        $lastClient = $clientsData->name;
        $margin = $servicesfee + $logisticsfee - $productsfee;

        $ttgToHp = 0;
        $hpToClient = 0;
        Session::put('quote_id', $quote->id);
        Session::put('enterprise_client_id', $clientsData->id);

        return Response::json(['status' => 'success', 
				'total' => $total, 
				'ttgToHp' => $ttgToHp, 
				'hpToClient' => $hpToClient, 
				'lastClient' => $lastClient, 
				'margin' => $margin, 
				'quote_id' => $q_id, 
				'quote_name' => $q_name,
				'Quote_Item_Id'=>$quoteItem->id,
				'quantity'=>$quoteItem->quantity,
				'product_oem'=>$quoteItem->unit_margin_oem_product,
				'service_oem'=>$quoteItem->services_oem_margin,
				'logistic_oem'=>$quoteItem->logistics_oem_margin]);
    }

	/* returns a list of enterprise quotes against the specified client ($id) */
    public function QuoteSummaryList(Request $request, $id = null) {
		//validate  user is logged in
		if (!$this->filter($this->permission)) {
			Toast::error('You do not have permission.', 'Error');
			return redirect('/dashboard');	
		}

		//validate $id => client id	
		if(empty($id) || !is_numeric($id) || $id <= 0){
			Toast::error('Client ID is not valid.', 'Error');
			return redirect('/dashboard');	
		}

		//validate user has permissions for client
		if(!$this->isUserValid($id)){ 
			Toast::error('You do not have permission.', 'Error');
			return redirect('/dashboard');
		}      
            
        Input::flash();
        $txt = strip_tags(trim($request->get('search')));

		$statusarr = Array("1", "2", "6", "7");
		$status = (isset($request->status) && ($request->status == "1" || $request->status == "2" || $request->status == "6" || $request->status == "7")) ? $request->status : '';
		if($status != ''){
			$statusarr = Array($status);
		}
        $lim = (isset($request->perPage) && is_numeric($request->perPage) &&  $request->perPage > 0) ? $request->perPage : 30 ;
        $page = (isset($request->page) && is_numeric($request->page) && $request->page > 0) ? $request->page : 1;
        $i = 0;

        if ($page > 1) {
            $pages = ($page - 1) * $lim;
        } else {
            $pages = 0;
        }

        $client = Client::find($id);
        $user_id = Auth::user()->id;

		// retrieve the current users role
		$role_id = 3;
		$RoleUser = RoleUser::select('role_id')->where('user_id', '=', $user_id)->first();
		if(null !== $RoleUser){
			$role_id = $RoleUser->role_id;
		}
       
        if (!empty($txt)) {
            $Quote = QuoteManagement::with('Status', 'Clientname', 'Username', 'Country')
				->whereIn('status', $statusarr)
				->where(function ($query) use($txt){
					$query->whereHas('Username', function ($q) use($txt) {
						$q->where('first_name', 'like', "%$txt%");
					})
                ->orwhere('quote.id', 'like', "%$txt%")
                ->orwhere('quote.name', 'like', "%$txt%");
            });

			if ($role_id == 3) {
				$Quotes = $Quote->where('client_id', '=', $id)->where('user_id', '=', $user_id)->orderBy('quote.id', 'DESC')->paginate($lim);
			} else {
				$Quotes = $Quote->where('client_id', '=', $id)->orderBy('quote.id', 'DESC')->paginate($lim);
			}
        }else{
            $Quote = QuoteManagement::with('Status', 'Clientname', 'Username', 'Country')->whereIn('status', $statusarr);
			if ($role_id == 3) {
				$Quotes = $Quote->where('client_id', '=', $id)->where('user_id', '=', $user_id)->orderBy('quote.id', 'DESC')->paginate($lim);
			} else {
				$Quotes = $Quote->where('client_id', '=', $id)->orderBy('quote.id', 'DESC')->paginate($lim);
			}
        }
        return view('Enterprises/quote_history_list', compact('Quotes','txt', 'status', 'pages', 'lim', 'id', 'client')); 
    }

    public function QuoteSetting(Request $request){
		//is users authenticated
        if (!$this->filter($this->permission)) {
			return redirect()->back();
		}

		//validate inputs
		if(empty($request->client_id) || !is_numeric($request->client_id) || $request->client_id <= 0){
			return redirect()->back();
		}

		if(empty($request->quote_id) || !is_numeric($request->quote_id) || $request->quote_id <= 0){
			return redirect()->back();
		}

		//validate user has permission for the client
		if(!$this->isUserValid($request->client_id, $request->quote_id)){
			return redirect()->back();
		}

		//redirect to the settings page
        Session::put(['enterprise_client_id' => $request->client_id, 'quote_id' => $request->quote_id]);
        return redirect('enterprises/quotesettings');    
    }

    public function QuoteSettings(){
        if ($this->filter($this->permission)) {

            $role = RoleUser::where('user_id',Auth::user()->id)->first()->role_id;
            @$permissions = Auth::user()->permission;
            $permission =  explode(',', @$permissions);

			$client_id = Session::get('enterprise_client_id');
			$Quote_Id = Session::get('quote_id');

			if($this->isUserValid($client_id, $Quote_Id)){                
                
                $client = Client::find($client_id);
                $Quote = Quote::find($Quote_Id);
                $quote_notes = QuotesNotes::where(['quote_id'=>$Quote->id,'display_level'=>2])->get();
            
                if (!empty($client)) {  
					$calculate = new CalculationsController();            
					$Countries = $calculate->getCountriesforClient($client_id);
					$currency = Country::find($Quote->country);
                    $Regions = CountryRegion::where('country_id', '=', $Quote->country)->get();
                    return view('/Enterprises.QuoteSetting', compact('client', 'Countries', 'Regions', 'currency','Quote','quote_notes','role','permission'));
                } else {
                    Toast::error('Invalid client Id !!!.', 'Error');
                    return redirect('/dashboard');
                }
                
            }else{
                return redirect()->back();
            }
        } else {
            Toast::error("You don'thave not permission.", 'Error');
            return redirect('/dashboard');
        }
    }

    public function UpdateQuoteSettings(Request $request){
        if ($this->filter($this->permission)) {

            $quote_id = $request->quote_id;
            $Quote = Quote::find($quote_id);
            $country = Country::find($request->country);
			$region = CountryRegion::where('id', '=', $request->region)->where('country_id', '=', $request->country);

			//validate info
			if(null === $Quote || null === $country || null === $region){
				return redirect('/enterprises/index');
			} 
			if(!$this->isUserValid($Quote->client_id, $Quote->id)){
				return redirect('/enterprises/index');
			}

			$currency_symbol = $country->currency_symbol;
			$currency_rate = $country->currency_rate;

            if($request->currency == 'US$'){                
				$currency_symbol = '$';
				$currency_rate = 1;
            } else {

				//check if there a cursomter speciic exchange rate
				$args = array(
					'client'	=> $Quote->client_id,
					'country'	=> $country->id
				);

				$results = DB::select(DB::raw("SELECT * 
					FROM `client_country_rates` ccr
					JOIN(
						SELECT t.`id` AS client_id, @pv:=t.`parent_id` AS parent, @rownum := @rownum + 1 AS rownum
						FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
						JOIN (SELECT @pv:= :client, @rownum:=0)tmp
						WHERE t.`id`=@pv
					)c on c.`client_id` = ccr.`client_id`
					WHERE country_id = :country AND (expires IS NULL OR expires > now())
					ORDER BY c.`rownum`
					LIMIT 1"), $args);
				if(!empty($results) && isset($results[0]->exchangerate) && !empty($results[0]->exchangerate) && $results[0]->exchangerate != 0){					
					$currency_rate = $results[0]->exchangerate;
				}
			}

			$org_country = $Quote->country;
            if($Quote->status == 1){
				$oem_p = $Quote->margin_oem_product;
				$oem_s = $Quote->margin_oem_service;
				$oem_l = $Quote->margin_oem_logistic;

                $Quote->name				= strip_tags($request->name);  
                $Quote->country				= $country->id;  
                $Quote->region				= $request->region;  
                $Quote->ave_collect_size	= (isset($request->ave_collect_size) && is_numeric($request->ave_collect_size) && $request->ave_collect_size > 0) ? $request->ave_collect_size : $Quote->ave_collect_size;  
                $Quote->currency_symbol		= $currency_symbol; 
                $Quote->currency_rate		= $currency_rate;  
								
				/* TODO: check user has permissions to update margin info */
				if(isset($request->margin_oem_product) && is_numeric($request->margin_oem_product)){
					$Quote->margin_oem_product	= $request->margin_oem_product; 
					if(isset($request->margin_apply_product) && $request->margin_apply_product == '1'){
						QuoteItem::where('quote_id', '=', $Quote->id)->update(['unit_margin_oem_product' => $request->margin_oem_product]);
					}
				} 
				if(isset($request->margin_oem_service) && is_numeric($request->margin_oem_service)){
					$Quote->margin_oem_service	= $request->margin_oem_service; 
					if(isset($request->margin_apply_service) && $request->margin_apply_service == '1'){
						QuoteItem::where('quote_id', '=', $Quote->id)->update(['services_oem_margin' => $request->margin_oem_service]); 
					}
					
				}
				if(isset($request->margin_oem_logistic) && is_numeric($request->margin_oem_logistic)){
					$Quote->margin_oem_logistic	= $request->margin_oem_logistic; 
					if(isset($request->margin_apply_logistic) && $request->margin_apply_logistic == '1'){
						QuoteItem::where('quote_id', '=', $Quote->id)->update(['logistics_oem_margin' => $request->margin_oem_logistic]);
					}
				}		 

                $Quote->save();

				$calculate = new CalculationsController();
				//if the country changes then need to review all produt id for items incase they have  changed
				//then recalculate
				if($org_country != $request->country){
					$items = QuoteItem::where('quote_id', '=', $Quote->id)->get();
					if(!empty($items) && count($items) > 0){
						foreach($items as $item){
							$product = $calculate->productId($Quote->client_id, $item->type_id, $item->brand_id, $item->processor_id, $item->screen_id, $item->hdd_id, $item->dvd_id, $item->ram_id, 1, 0, $country->id);
							if(!empty($product) && $product[0]->id != $item->product_id){
								$item->product_id = $product[0]->id;
								$item->save();
							}
						}
					}
				}
                
				$calculate->Recalculate($Quote->id);
            }    
			//================================End Update Quote =============================//
            return redirect('/enterprises/index');
        }else {
            Toast::error('You have not permission.', 'Error');
            return redirect('/dashboard');
        }
    }

    public function EditItem(Request $request){
		//check if user logged in
		if (!$this->filter($this->permission)) {
            Toast::success('You do not have permissions for this action.', 'Error');
            return redirect('/dashboard');
		}

       $item_id = $request->QuoteItem;

		if(!empty($item_id) && is_numeric($item_id) && $item_id > 0){
			$QuoteItem = QuoteItem::with('QuoteItemsDefect')->where('id', $item_id)->first();			

			//check quote item exists
			if(null !== $QuoteItem){
				//check if user has permission for the client and/or quote
				$quote = Quote::find($QuoteItem->quote_id);
				if(null !== $quote && $this->isUserValid($quote->client_id, $quote->id)){
			
					$Steps = $this->getSteps($QuoteItem->type_id, $QuoteItem->brand_id);
					echo json_encode(array('QuoteItem' => $QuoteItem, 'Steps' => $Steps));

				}
			}
		}
		die;
    }
    
	public function QuoteUpdate(Request $request){
		//validate  quote item id
		if(!isset($request->quoteItemId) || !is_numeric($request->quoteItemId)){
			return Response::json(array('status' => 'error', 'message' => 'Item not found')); 		
		}

		//retreive the quote item object
		$QuoteItem = QuoteItem::find($request->quoteItemId);
		
		//exist is there is not quote item found 
		if(null === $QuoteItem){
			return Response::json(array('status' => 'error', 'message' => 'Item not found')); 		
		}

		// check that the current logged in user has permissions agains tthe quote and item
		$Quote = Quote::find($QuoteItem->quote_id);
		if(null === $Quote || !$this->isUserValid($Quote->client_id, $Quote->id)){ 
			return Response::json(array('status' => 'error', 'message' => 'Quote not found')); 
		}

		$type_id = $brand_id = $processor_id = $screen_id = $hdd_id = $dvd_id = $ram_id = $model_id = $product_id = 0;
		$model = '';
		
		$obj = $this->validateOption($request->processor_id, 'Processor');
		if($obj["id"] > 0){
			$processor_id = $obj["id"];
			$QuoteItem->processor_id = $obj["id"];
			$QuoteItem->processor_name = $obj["name"];
		}
				
		$obj = $this->validateOption($request->screen_id, 'Screen Size');
		if($obj["id"] > 0){
			$screen_id = $obj["id"];
			$QuoteItem->screen_id = $obj["id"];
			$QuoteItem->screen_name = $obj["name"];
		}

		$obj = $this->validateOption($request->dvd_id, 'DVD');
		if($obj["id"] > 0){
			$dvd_id = $obj["id"];
			$QuoteItem->dvd_id = $obj["id"];
			$QuoteItem->dvd_name = $obj["name"];
		}

		$obj = $this->validateOption($request->hdd_id, 'HDD');
		if($obj["id"] > 0){
			$hdd_id = $obj["id"];
			$QuoteItem->hdd_id = $obj["id"];
			$QuoteItem->hdd_name = $obj["name"];
		}

		$obj = $this->validateOption($request->ram_id, 'RAM');
		if($obj["id"] > 0){
			$ram_id = $obj["id"];
			$QuoteItem->ram_id = $obj["id"];
			$QuoteItem->ram_name = $obj["name"];
		}

		$obj = $this->validateOption($request->services_id, 'Services');
		if($obj["id"] > 0){
			$QuoteItem->services_id = $obj["id"];
			$QuoteItem->services_name = $obj["name"];
		}
				
		
		// find the product matching the specified configuration  
		$calculate = new CalculationsController();
        $re = $calculate->productId($Quote->client_id, $QuoteItem->type_id, $QuoteItem->brand_id, $processor_id, $screen_id, $hdd_id, $dvd_id, $ram_id, 1, 0);
        if($re){
            $product_id = $re[0]->id;
			if(!empty($re[0]->model) && $model_id == 0){
				$model = $re[0]->model;
				$model_id = $product_id;
			}
        }else{
            $response = array('status'=>'error', 'message'=>'You can not create quote for this client due to no relavent data found in product table !!!');
            return json_encode($response);
        }

		$QuoteItem->product_id = $product_id;
		$QuoteItem->model_id = $model_id;
		$QuoteItem->model_name = $model_id;
		$QuoteItem->save();

		if(isset($request->defects) && (@$request->defects != 0) && !empty($request->defects)){
		    QuoteItemsDefect::where('quote_item_id', $QuoteItem->id )->delete();
     
			$defects = explode(',', $request->defects);
			if (count($defects) > 0) {
				foreach ($defects as $key => $value) {
					$obj = $this->validateOption($value, 'Defects');
					if($obj["id"] > 0){
						$quoteItemDefect = new QuoteItemsDefect;
						$quoteItemDefect->quote_id		= $QuoteItem->quote_id;
						$quoteItemDefect->defect_id		= 0;
						$quoteItemDefect->defect_name	= $obj["name"];
						$quoteItemDefect->defect_value	= 0;
						$quoteItemDefect->quote_item_id = $QuoteItem->id;
						$quoteItemDefect->option_id		= $obj["id"];
						$quoteItemDefect->save();            
					}               
				}
			}
		}		
		
		$calculate->Recalculate($QuoteItem->quote_id, $QuoteItem->id);

		echo json_encode($QuoteItem);
    }
	
	//clones and creates a new item against a quote with the same details from the cloned item.
    public function copy_Item($id = Null){
		//validate inputs
		if($id == null || !is_numeric($id)){
			return redirect('/enterprises/index');  
		}
		      
		//validate an item exists with the given id
        $Quote_Item = QuoteItem::find($id);
		if(null === $Quote_Item){
			return redirect('/enterprises/index');
		}

		//validate the quote is at the right status and that the user has permissions
		$Quote = Quote::find($Quote_Item->quote_id);
		if(null === $Quote || $Quote->status != 1 || !$this->isUserValid($Quote->client_id, $Quote->id)){
			return redirect('/enterprises/index?qiid=' . $id);
		}

		//clone the item
        $Quote_Item_Save = $Quote_Item->replicate();
        $Quote_Item_Save->save();

		//clone defect and add to new item
		$defects = QuoteItemsDefect::where('quote_item_id', $id )->get();
		if (count($defects) > 0) {
			foreach ($defects as $defect) {
				$newdefect = $defect->replicate();
				$newdefect->quote_item_id = $Quote_Item_Save->id;
				$newdefect->save();             
			}
		}

        return redirect('/enterprises/index?qiid=' . $Quote_Item_Save->id);   
    }

	//update the quote status  
	public function updatequoteStatus(Request $request){
		if ($this->filter($this->permission)) {

            $quote_id = $request->get('Quote_id');
            $Quote = Quote::find($quote_id);

			if(null === $Quote  || !$this->isUserValid($Quote->client_id, $quote_id)){ 
				return Response::json(array('status' => 'error')); 
			}

			$old_status = $Quote->status;
			$new_status = $request->get('status');

			if(!is_numeric($new_status) || $new_status <= 0 || $old_status == $new_status){
				return Response::json(array('status' => 'error')); 
			}

			$note = "Quote status changed from ";
			switch($old_status){
				case 1; $note .= "in-progress to "; break;
				case 2; $note .= "accepted to "; break;
				case 6; $note .= "complete to "; break;
				case 7; $note .= "cancelled to "; break;
			}
			
			$isUpdated = FALSE;

			/* 
			status can go from 
			- in progress (1) -> complete (6) or cancel (7)
			- complete (1) -> in progress (1) or accepted (2) or cancel (7)
			- accepted (1) -> in progress (1) or cancel (7)
			- cancel (7) -> in progress (1)
			*/

			if($new_status == 1 && ($old_status == 2 || $old_status == 6 || $old_status == 7)){
				/* quote being set back to in-progress */
				$note .= "in-progress";
                $Quote->status = $new_status;  
                $Quote->date_modified = date('Y-m-d H:i:s');
				$Quote->save();
				$isUpdated = TRUE;

				$calculate = new CalculationsController();            
				$calculate->Recalculate($quote_id);

			} else if ($new_status == 6 && ($old_status == 1)){
				/* quote being set back to complete from in-pogress */
				$note .= "completed";
				$Quote->date_presented = date('Y-m-d H:i:s');
                $Quote->status = $new_status;  
				$Quote->date_modified = date('Y-m-d H:i:s');                  
                $client = DB::table('settings')->where('client_id', '=', $Quote->client_id)->first();
                $Date = date('Y-m-d');
                if (isset($client->value)) {
                    $Quote->date_validto = date('Y-m-d H:i:s', strtotime($Date . ' + ' . $client->value . ' days'));
                } else {
                    $client = DB::table('settings')->where('client_id', '=', '0')->first();
                    @$Quote->date_validto = date('Y-m-d H:i:s', strtotime($Date . ' + ' . $client->value . ' days'));
                }
				$Quote->save();
				$isUpdated = TRUE;

			} else if ($new_status == 7){
				/* quote being set back to cancelled */
				$note .= "cancelled";
				$Quote->status = $new_status;  
				$Quote->date_modified = date('Y-m-d H:i:s');
				$Quote->save();
				$isUpdated = TRUE;

			} else if ($new_status == 2 && $old_status == 6){
				/* quote being set back to accepted from compelete */
				$note .= "accepted";
				$Quote->status = $new_status;  
                $Quote->date_accepted  = date('Y-m-d H:i:s');
				$Quote->date_modified = date('Y-m-d H:i:s');
				$Quote->save();
				$isUpdated = TRUE;
			} 

            if ($isUpdated) {
				/* add a note */
				$QuotesNotes = new QuotesNotes();
				$QuotesNotes->note = $note;
				$QuotesNotes->display_level = 1;
				$QuotesNotes->user_id = Auth::user()->id;
				$QuotesNotes->quote_id = $Quote->id;
				$QuotesNotes->save();

                return Response::json(array('status' => 'status', 'data' => $new_status));
            } else {
                return Response::json(array('status' => 'error'));
            }
        }
	}

	//anables a user to add a note against an enterprise quote from the QuoteSettings page
	public function addNote(Request $request){
		if (!$this->filter($this->permission)) {
			return redirect('enterprises/quotesettings');
		}

        $quote_id = $request->get('quote_id');		

		//check quote exists and that the user can access this quote
		$Quote = Quote::find($quote_id);
		if (null === $Quote || !$this->isUserValid($Quote->client_id, $quote_id)) {
			return redirect('enterprises/quotesettings');
		}

		$note = $request->get('note');
		if(strlen(trim($note)) > 0){
			/* add a note */
			$QuotesNotes = new QuotesNotes();
			$QuotesNotes->note = strip_tags($note);
			$QuotesNotes->display_level = 2;
			$QuotesNotes->user_id = Auth::user()->id;
			$QuotesNotes->quote_id = $Quote->id;
			$QuotesNotes->save();
		}

		return redirect('enterprises/quotesettings');
	}

	public function exportCSV(Request $request){
		if (!$this->filter($this->permission)) {
			return null;
		}

		//user permission check agains tthe client and/or quote is performed in the index() method
		//get quote detail
		$result = $this->index($request, 1);
		
		@$permissions = Auth::user()->permission;
        $permission =  explode(',', @$permissions);
		//print_r($permission);
		//die();

		if(!isset($result) || empty($result)){
			return null;
		}

		//get quote items 
		$list = Array();

		$statustext = '';
        switch($result['Quotes']['status']){
        	case "1": $statustext = "In Progress"; break;
        	case "6": $statustext = "Completed"; break;
        	case "2": $statustext = "Accepted"; break;
        	case "7": $statustext = "Canceled"; break;
        }

		$list[] = Array('Quote', $result['Quotes']['id']);
		$list[] = Array('Reference', $result['Quotes']['name']);
		$list[] = Array('Status', $statustext);

		if(($result['Quotes']['currency_symbol'] == "$" || $result['Quotes']['currency_symbol'] == 'US$') && $result['Quotes']['currency_rate'] == 1){	
			$list[] = Array('Currency', 'US$');
		} else {
			$list[] = Array('Currency', $result['Quotes']['Country']['currency_name']);
		}
		$list[] = Array('Client', $result['clientDetails']['name']);
			
		if($result['role'] == 2 || ($result['role'] == 3 && in_array(1,$result['permission'])) ){
			$list[] = Array('');
			$list[] = Array('Country', $result['Quotes']['Country']['name']);
			$list[] = Array('Region', $result['Quotes']['Region']['name']);
			$list[] = Array('Qty Per Pickup', number_format($result['Quotes']['ave_collect_size']));
			$list[] = Array('Base Product Margin', number_format($result['Quotes']['margin_oem_product'],0).'%');
			$list[] = Array('Base Services Markup', number_format($result['Quotes']['margin_oem_service'],0).'%');
			$list[] = Array('Base Logistics Markup', number_format($result['Quotes']['margin_oem_logistic'],0).'%');

			$list[] = Array('');
			$list[] = Array('Financial Summary', 'Product Value', 'Services Fee', 'Logistics Fee', 'Net Value'); 
			$list[] = Array('Quote from Partner', 
						$this->displayQuoteValues($result['unit_tp'], $result['Quotes']['currency_symbol'], $result['Quotes']['currency_rate'] ),
						$this->displayQuoteValues(@$result['services_tp'], $result['Quotes']['currency_symbol'], $result['Quotes']['currency_rate'] ),
						$this->displayQuoteValues(@$result['logistic_tp'], $result['Quotes']['currency_symbol'], $result['Quotes']['currency_rate']),
						$this->displayQuoteValues(@$result['total_tp'], $result['Quotes']['currency_symbol'], $result['Quotes']['currency_rate'] )
					);
			$list[] = Array('Quote to Customer',
						$this->displayQuoteValues($result['unit_oemtp'], $result['Quotes']['currency_symbol'], $result['Quotes']['currency_rate'] ),
						$this->displayQuoteValues(@$result['services_oemtp'], $result['Quotes']['currency_symbol'], $result['Quotes']['currency_rate'] ),
						$this->displayQuoteValues(@$result['logistic_oemtp'], $result['Quotes']['currency_symbol'], $result['Quotes']['currency_rate'] ),
						$this->displayQuoteValues(array_sum(@$result['total_price']), $result['Quotes']['currency_symbol'], $result['Quotes']['currency_rate'] )
					);
			$list[] = Array('Margin / Markup $',
						$this->displayQuoteValues(($result['unit_tp'] - $result['unit_oemtp']), $result['Quotes']['currency_symbol'], $result['Quotes']['currency_rate'] ),
						$this->displayQuoteValues(($result['services_oemtp'] - $result['services_tp']), $result['Quotes']['currency_symbol'], $result['Quotes']['currency_rate'] ),
						$this->displayQuoteValues(($result['logistic_oemtp'] - $result['logistic_tp']), $result['Quotes']['currency_symbol'], $result['Quotes']['currency_rate'] ),
						$this->displayQuoteValues((@$result['total_tp'] - $result['total_oemtp']), $result['Quotes']['currency_symbol'], $result['Quotes']['currency_rate'] )
					);
			$list[] = Array('Margin / Markup %',
						($result['unit_tp'] > 0)	? number_format( ceil((($result['unit_tp'] - $result['unit_oemtp']) / $result['unit_tp']) * 100), 0).'%' : '',
						($result['services_tp'] > 0) ? number_format( ceil((($result['services_oemtp'] - $result['services_tp']) / $result['services_tp']) * 100), 0).'%' : '',
						($result['logistic_tp'] > 0) ? number_format( ceil((($result['logistic_oemtp'] - $result['logistic_tp']) / $result['logistic_tp']) * 100), 0).'%' : '',
						($result['total_tp'] > 0)	? number_format( ceil((($result['total_tp'] -  $result['total_oemtp']) / $result['total_tp']) * 100), 0).'%' : ''
					);
		}
		$list[] = Array('');
		//$heading = Array('Product Details', '# Items', 'QTY', 'Product Value (per unit)', 'Services Fee (per unit)',  'Logistics Fee (per unit)', 'Net Value (per unit)', 'Total Net Value', 'Margin %');
		//$list[] = $heading;

		$heading = Array('Model', 'Type', 'Brand', 'Processor', 'Screen', 'DVD', 'HDD', 'RAM', 'Services', 'Defects', 'QTY', 'Product Value (per unit)', 'Services Fee (per unit)'
			,  'Logistics Fee (per unit)', 'Net Value (per unit)', 'Total Net Value', 
			($result['role'] == 2 || ($result['role'] == 3 && in_array(1,$result['permission'])) ) ? 'Product Margin' : '',
			($result['role'] == 2 || ($result['role'] == 3 && in_array(1,$result['permission'])) ) ? 'Services Margin' : '',
			($result['role'] == 2 || ($result['role'] == 3 && in_array(1,$result['permission'])) ) ? 'Logistics Margin' : '',
			($result['role'] == 2 || ($result['role'] == 3 && in_array(1,$result['permission'])) ) ? 'Margin %' : ''
		);
		$list[] = $heading;   

		if(count($result['Quote_Items']) > 0) {
			foreach($result['Quote_Items'] as $key1 => $quotes1) {
				$qty2 = $unitp2 = $services1 = $logistic1 = $net_price1 = $total_price1 = $total_price1 = $totalmargin1 = $quotes2 = $p_unit = $p_logistics = $p_services = array(); 
				foreach($quotes1 as $Quote1)  {
	                $qty2[]			= $Quote1[0]['quantity'];
	                $unitp2[]		= $Quote1[0]['unit_f5_price'] * $Quote1[0]['quantity'];
	                $services1[]	= $Quote1[0]['services_net_price'] * $Quote1[0]['quantity'];
	                $logistic1[]	= $Quote1[0]['logistics_net_price'] * $Quote1[0]['quantity'];
	                $net_price1[]	= $Quote1[0]['unit_price'];
	                $total_price1[] = $Quote1[0]['total_price'];
	                $totalmargin1[] = $Quote1[0]['totalmargin'];
					$p_unit[]		= $Quote1[0]['unit_prod_price'] * $Quote1[0]['quantity'];
					$p_logistics[]	= $Quote1[0]['unit_f5_logistics'] * $Quote1[0]['quantity'];
					$p_services[]	= $Quote1[0]['unit_f5_services'] * $Quote1[0]['quantity'];  
                }

				if(array_sum($qty2) != 0 && array_sum($p_unit) != 0) {																	
					$net_price = array_sum($p_unit) - array_sum($p_services) - array_sum($p_logistics);
					$ave_margin = ceil( (1 - (array_sum($total_price1) / $net_price)) * 100); 																	
							
					$summary = Array($key1, 
									count($quotes1), number_format(array_sum($qty2), 0, '.', ','), 
									$this->displayQuoteValues(array_sum($unitp2) / array_sum($qty2), $result['Quotes']['currency_symbol'], $result['Quotes']['currency_rate'] ), 
									$this->displayQuoteValues(array_sum($services1) / array_sum($qty2), $result['Quotes']['currency_symbol'], $result['Quotes']['currency_rate'] ),  
									$this->displayQuoteValues(array_sum($logistic1) / array_sum($qty2), $result['Quotes']['currency_symbol'], $result['Quotes']['currency_rate'] ), 
									$this->displayQuoteValues(array_sum($total_price1) / array_sum($qty2) , $result['Quotes']['currency_symbol'], $result['Quotes']['currency_rate'] ), 
									$this->displayQuoteValues(array_sum($total_price1), $result['Quotes']['currency_symbol'], $result['Quotes']['currency_rate'] ), 
									number_format($ave_margin,0)
								);
					
					//$list[] = $summary;  
		
					foreach($quotes1 as $Quote) {
						$defects = '';
						if(isset($Quote[0]['quote_items_defect']) && count($Quote[0]['quote_items_defect'])>0) {
							$sep = '';
    						foreach($Quote[0]['quote_items_defect'] as $val){
								$defects .= $sep . $val['defect_name'];
								$sep = ';';
    						}
    					}									
						//get the pre oem pricing for products, logistics and services to work out the actual OEM margin 
						$item_p_total		= ($Quote[0]['unit_prod_price'] - $Quote[0]['unit_f5_logistics'] - $Quote[0]['unit_f5_services']) * $Quote[0]['quantity'];
						$item_m				= ($item_p_total != 0) ? ceil( (1 - ($Quote[0]['total_price'] / $item_p_total)) * 100) : 0;
					
						$line = Array(
							(!empty($Quote[0]['model_name'])) ? $Quote[0]['model_name'] : '',
							(!empty($Quote[0]['type_name'])) ? $Quote[0]['type_name'] : '',
							(!empty($Quote[0]['brand_name'])) ? $Quote[0]['brand_name'] : '',
							(!empty($Quote[0]['processor_name'])) ? $Quote[0]['processor_name'] : '',
							(!empty($Quote[0]['screen_name'])) ? $Quote[0]['screen_name'] : '',
							(!empty($Quote[0]['dvd_name'])) ?  $Quote[0]['dvd_name'] : '',
							(!empty($Quote[0]['hdd_name'])) ? $Quote[0]['hdd_name']: '',
							(!empty($Quote[0]['ram_name'])) ? $Quote[0]['ram_name'] : '',    											
							(!empty($Quote[0]['services_name'])) ? $Quote[0]['services_name'] : '',
							$defects,
							number_format($Quote[0]['quantity'], 0, '.', ','),
							$this->displayQuoteValues($Quote[0]['unit_f5_price'], $result['Quotes']['currency_symbol'], $result['Quotes']['currency_rate'] ),
							$this->displayQuoteValues($Quote[0]['services_net_price'], $result['Quotes']['currency_symbol'], $result['Quotes']['currency_rate'] ),
							$this->displayQuoteValues($Quote[0]['logistics_net_price'], $result['Quotes']['currency_symbol'], $result['Quotes']['currency_rate']),
							$this->displayQuoteValues($Quote[0]['unit_price'], $result['Quotes']['currency_symbol'], $result['Quotes']['currency_rate']),
							$this->displayQuoteValues($Quote[0]['total_price'], $result['Quotes']['currency_symbol'], $result['Quotes']['currency_rate']),
							($result['role'] == 2 || ($result['role'] == 3 && in_array(1,$result['permission'])) ) ? number_format($Quote[0]['unit_margin_oem_product'], 0 ).'%' : '',
							($result['role'] == 2 || ($result['role'] == 3 && in_array(1,$result['permission'])) ) ? number_format($Quote[0]['services_oem_margin'], 0 ) . '%' : '',
							($result['role'] == 2 || ($result['role'] == 3 && in_array(1,$result['permission'])) ) ? number_format($Quote[0]['logistics_oem_margin'], 0 ) . '%' : '',
							($result['role'] == 2 || ($result['role'] == 3 && in_array(1,$result['permission'])) ) ? number_format($item_m, 0) . '%' : ''
						);	
						$list[] = $line;							
					}				
				}
			}
		}

		//get notes 
		$list[] = Array('');
		$list[] = Array(''); 
		$quote_notes = QuotesNotes::where(['quote_id' => $result['Quotes']['id'], 'display_level' => 2])->orderBy('date_created', 'DESC')->get();
		if(!empty($quote_notes)){
			foreach($quote_notes as $note){
				$list[] = Array($note->username->first_name , ' ' . $note->username->last_name, $note->note);
			}
		}

		$headers = [
				'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
			,   'Content-type'        => 'text/csv'
			,   'Content-Disposition' => 'attachment; filename=f5-quote-' . $result['Quotes']['id'] . '.csv'
			,   'Expires'             => '0'
			,   'Pragma'              => 'public'
		];

		$callback = function() use ($list) 
		{
			$FH = fopen('php://output', 'w');
			foreach ($list as $row) { 
				fputcsv($FH, $row);
			}
			fclose($FH);
		};

		return response()->stream($callback, 200, $headers);
	}

	//supporting function to format the money  values for the method exportCSV()
	function displayQuoteValues($value, $symbol, $rate){
		$rate = ($rate > 0 && strlen($symbol) > 0) ?  $rate : 1;
		$symbol = ''; // (strlen($symbol) > 0) ? $symbol : '$';
		return $symbol . '' . number_format($value * $rate, 0, '.', ',');
	}

	//returns a list of region for a specified country
	// this is used via an ajax called from the enterprise quote summary page
    public function EnterpriseQuote(Request $request) {
        if (!empty($request->input('id')) && is_numeric($request->input('id'))) {
			$data = DB::table('country')
					->select('country.id', 'country.currency_symbol', 'country_region.name as region', 'country_region.id as region_id')
					->join('country_region', 'country_region.country_id', '=', 'country.id')
					->where('country.id', '=', $request->input('id'))
					->orderby('country_region.name', 'asc')
					->get();
			return response()->json($data);
		}        
    }
}
