<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use DateTime;
use links;
use App\User;
use Toast;
use App\Client;
use App\Product;
use App\ProductOption;
use App\Http\Controllers\CalculationsController;
use App\Quote;
use App\QuoteStatus;
use App\QuoteItem;
use App\QuoteItemsDefect;
use App\QuotesNotes;
use App\Country;

class ProductsController extends Controller {

    private $permission = [1];

    public function __construct() {
        $this->middleware('auth');
    }

    public function index(Request $request) {
        if ($this->filter($this->permission)) {
			$lim = (isset($request->perPage) && is_numeric($request->perPage) &&  $request->perPage > 0) ? $request->perPage : 30 ;
			$page = (isset($request->page) && is_numeric($request->page) && $request->page > 0) ? $request->page : 1;
			$order = (isset($request->order) && !empty($request->order) && $request->order !== '') ? $request->order : 'client_f';
			$orderfield = 'clients.fullpath';
			$orderby = 'asc';


			switch (trim(strtolower($order))){
				case "client_r": $orderby = 'desc'; break;
				case "country_f": $orderfield = 'country.name'; break; 
				case "country_r": $orderfield = 'country.name'; $orderby = 'desc'; break; 
				case "model_f": $orderfield = 'product.model'; break; 
				case "model_r": $orderfield = 'product.model'; $orderby = 'desc'; break; 
				case "brand_f": $orderfield = 'brd.name'; break; 
				case "brand_r": $orderfield = 'brd.name'; $orderby = 'desc'; break; 
				case "proc_f": $orderfield = 'proc.name'; break; 
				case "proc_r": $orderfield = 'proc.name'; $orderby = 'desc'; break; 
				case "scr_f": $orderfield = 'scr.name'; break; 
				case "scr_r": $orderfield = 'scr.name'; $orderby = 'desc'; break; 
				case "prod_f": $orderfield = 'typ.name'; break; 
				case "prod_r": $orderfield = 'typ.name'; $orderby = 'desc'; break; 
			}

			$product = Product::with('Brand', 'Type', 'Processor', 'Screen')
						->leftjoin('clients', 'product.client_id', '=', 'clients.id')
						->leftjoin('country', 'product.country_id', '=', 'country.id')
						->leftjoin('product_options as brd', 'product.brand_id', '=', 'brd.id')
						->leftjoin('product_options as typ', 'product.type_id', '=', 'typ.id')
						->leftjoin('product_options as scr', 'product.screen_id', '=', 'scr.id')
						->leftjoin('product_options as proc', 'product.processor_id', '=', 'proc.id')
						->select('product.*', 'clients.fullpath', 'country.name AS ctry') 
						->orderBy($orderfield, $orderby)					
						->paginate($lim);

            return view('Products/index', array('products' => $product, 'page' => $page, 'lim' => $lim, 'order' => $order));
         } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function addProduct() {
        if ($this->filter($this->permission)) {

			$clients = new ClientsController();
			$cids = Array();
			$parent_ids = $clients->getTree($cids, 0, 0, '0', '', '');
			$countries = Country::where('status', '=' , 'active')->orderBy('name', 'asc')->get();

            $product_options = ProductOption::orderBy('name', 'asc')->get();
            return view('Products/add', compact('parent_ids', 'product_options', 'countries' ));
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function listingPage(Request $request) {
		$typeid = (!empty($request->typeId) && is_numeric($request->typeId) && $request->typeId > 0) ? $request->typeId : 0;  
		$brandid = (!empty($request->brandId) && is_numeric($request->brandId) && $request->brandId > 0) ? $request->brandId : 0; 
		$clientid = (!empty($request->clientid) && is_numeric($request->clientid) && $request->clientid > 0) ? $request->clientid : 0; 

		if ($typeid == 0 && $brandid == 0) {
            $data = array(
                'proccessor' => array(),
                'Screen' => array(),
                'ram' => array(),
                'hdd' => array(),
                'DVD' => array()
            );
            echo json_encode($data);
		}

		$calculate = new CalculationsController();
		$step = $calculate->getQuoteSteps($typeid, $brandid, $clientid);      

        $proccessor = array();
        $Screen = array();
        $ram = array();
        $hdd = array();
        $DVD = array();
        $pro = '';
             
		if(isset($step) && count($step) > 0){

            foreach ($step as $key => $value) { 
				$steps = explode(',', $value->group_code);              

				foreach ($steps as $pro) {
					$a = preg_replace('/[0-9]+/', '', $pro);

					if (strpos($a, 'Processor') !== false) {                    
				
						$proccessor = DB::select(DB::raw('SELECT *, CONCAT(p.`group`, " - ", p.`name`) as name
							FROM `product_option_groups` p
							WHERE `group_code` = "' . $pro . '"
							ORDER BY  CONCAT(p.`group`, " - ", p.`name`) '));
				
					} else if (strpos($a, 'Screen') !== false) {
						
						$Screen = DB::select(DB::raw('SELECT *, CONCAT(p.`group`, " - ", p.`name`) as name
							FROM `product_option_groups` p
							WHERE `group_code` = "' . $pro . '"
							ORDER BY CONCAT(p.`group`, " - ", p.`name`)'));
						
					} else if (strpos($a, 'RAM') !== false) {
						
						$ram = DB::select(DB::raw('SELECT *, CONCAT(p.`group`, " - ", p.`name`) as name
							FROM `product_option_groups` p
							WHERE `group_code` = "' . $pro . '"
							ORDER BY CONCAT(p.`group`, " - ", p.`name`)'));
					
					} else if (strpos($a, 'HDD') !== false) {
						
						$hdd = DB::select(DB::raw('SELECT *, CONCAT(p.`group`, " - ", p.`name`) as name
							FROM `product_option_groups` p
							WHERE `group_code` = "' . $pro . '"
							ORDER BY CONCAT(p.`group`, " - ", p.`name`)'));
				
					} else if (strpos($a, 'DVD') !== false) {
						
						$DVD = DB::select(DB::raw('SELECT *, CONCAT(p.`group`, " - ", p.`name`) as name
							FROM `product_option_groups` p
							WHERE `group_code` = "' . $pro . '"
							ORDER BY CONCAT(p.`group`, " - ", p.`name`)'));				
					}
				}
			}

            $proccessor1 = json_decode(json_encode($proccessor));
            $Screen1 = json_decode(json_encode($Screen));
            $ram1 = json_decode(json_encode($ram));
            $hdd1 = json_decode(json_encode($hdd));
            $DVD1 = json_decode(json_encode($DVD));

            $data = array(
                'proccessor' => $proccessor1,
                'Screen' => $Screen1,
                'ram' => $ram1,
                'hdd' => $hdd1,
                'DVD' => $DVD1
            );
            echo json_encode($data);
        } else {

            $proccessor = DB::select(DB::raw('SELECT *, CONCAT(p.`group`, " - ", p.`name`) as name
                FROM `product_option_groups` p
                WHERE p.`type` = "Processor"
                ORDER BY CONCAT(p.`group`, " - ", p.`name`)'));
            $Screen = DB::select(DB::raw('SELECT *, CONCAT(p.`group`, " - ", p.`name`) as name
                FROM `product_option_groups` p
                WHERE `type` = "Screen Size"
                ORDER BY CONCAT(p.`group`, " - ", p.`name`)'));
            $ram = DB::select(DB::raw('SELECT *, CONCAT(p.`group`, " - ", p.`name`) as name
                FROM `product_option_groups` p
                WHERE `type` = "RAM"
                ORDER BY CONCAT(p.`group`, " - ", p.`name`)'));
            $DVD = DB::select(DB::raw('SELECT *, CONCAT(p.`group`, " - ", p.`name`) as name
                FROM `product_option_groups` p
                WHERE `type` = "DVD"
                ORDER BY CONCAT(p.`group`, " - ", p.`name`)'));
            $hdd = DB::select(DB::raw('SELECT *, CONCAT(p.`group`, " - ", p.`name`) as name
                FROM `product_option_groups` p
                WHERE `type` = "HDD"
                ORDER BY CONCAT(p.`group`, " - ", p.`name`)'));


            $proccessor1 = json_decode(json_encode($proccessor));
            $Screen1 = json_decode(json_encode($Screen));
            $ram1 = json_decode(json_encode($ram));
            $hdd1 = json_decode(json_encode($hdd));
            $DVD1 = json_decode(json_encode($DVD));

            $data = array(
                'proccessor' => $proccessor1,
                'Screen' => $Screen1,
                'ram' => $ram1,
                'hdd' => $hdd1,
                'DVD' => $DVD1
            );
            echo json_encode($data);
        }
    }

    public function insertProduct(Request $request) {
        if ($this->filter($this->permission)) {
            Input::flash();
            //dd($request->all());
            if ($request->get('status') != null) {
                $status = 'active';
            } else {
                $status = 'inactive';
            }
            if (!empty($request->image)) {
                $file = array('image' => $request->image);
                $destinationPath = public_path() . '/product/';
                $extension = Input::file('image')->getClientOriginalExtension();
                $now = time();
                $fileName = $now . '.' . $extension;
                Input::file('image')->move($destinationPath, $fileName); //  
                $image = $fileName;
            } else {
                $image = '';
            }

            if ($request->parent_id == '') {
                 $client_id = 0;
                } else {
                    $client_id = $request->parent_id;
            }
            
            if ($request->radiotype1 == 'Processor') {
                $this->validate($request, array(
                    'type_id' => 'required',
                    'value' => 'required|numeric'
                ));
            }
            if ($request->radiotype1 == 'Model') {
                $this->validate($request, array(
                    'type_id' => 'required',
                    'model' => 'required',
                    'value' => 'nullable|numeric',
                    'value_factor' => 'required',
                    'image' => 'mimes:jpeg,png|max:100'
                ));
            }

            if ($request->radiotype1 == 'lcd') {
                $this->validate($request, array(
                    'type_id' => 'required',
                    'value' => 'required|numeric',
                    'value_factor' => 'required',
                    'screen_id'=>'required'
                ));
            }

            $products = Product::where('client_id',$client_id)  
                        ->where('model',$request->model)
                        ->where('type_id',$request->type_id)
                        ->where('brand_id',$request->brand_id)
                        ->where('processor_id',$request->processor_id)
                        ->where('screen_id',$request->screen_id)
                        ->where('hdd_id',$request->hdd_id)
                        ->where('ram_id',$request->ram_id)
                        ->where('dvd_id',$request->dvd_id)
                        ->where('value',$request->value)
                        ->where('value_factor',$request->value_factor)
                        ->where('status',$status)
                        ->where('image',$image)
						->where('country_id', $request->country_id)
                        ->get();

            if(count($products) > 0 ){
				return redirect()->back()->withErrors(['Duplicate' => 'You can not create duplicate product for same client']);
            }else{
                $Product = new Product();
                
                $Product->fill($request->all());
                $Product->status=$status;
                $Product->client_id = $client_id;
                $Product->pricing_type = $request->radiotype1;
				$Product->country_id = $request->country_id;
                $Product->image = $image;

                if ($Product->save()) {
                    Toast::success('Record has been saved.', 'Success');
                    return redirect('/products/index');
                } else {
                    Toast::error('Record not saved', 'Error');
                    return redirect('/products/index');
                }
            }            
            
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function editProduct($id = null) {
        if ($this->filter($this->permission)) {

            if ($id != null) {

                $clients = new ClientsController();
				$cids = Array();
				$parent_ids = $clients->getTree($cids, 0, 0, '0', '', '');
				$countries = Country::where('status', '=' , 'active')->orderBy('name', 'asc')->get();

                $product_options = ProductOption::orderBy('name', 'asc')->get();
                $product = Product::find($id);

                if (!empty($product)) {
                    
                    return view('Products/editProduct', compact(
                                    'product', 'parent_ids', 'product_options', 'countries'
                    ));

                } else {
                    Toast::error('Id not found, Please try again.', 'Error');
                    return Redirect('products/index');
                }
            } else {
                Toast::error('Id not found, Please try again.', 'Error');
                return Redirect('products/index');
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
       
    }

    public function updateProduct(Request $request, $id = null) {
        if ($this->filter($this->permission)) {

            Input::flash();
            if ($request->radiotype1 == 'Processor') {
                $this->validate($request, array(
                    'type_id' => 'required',
                    'value' => 'required|numeric'
                ));
            }
            if ($request->radiotype1 == 'Model') {
                $this->validate($request, array(
                    'type_id' => 'required',
                    'brand_id' => 'required|numeric',
                    'model' => 'required',
                    'value' => 'nullable|numeric',
                    'value_factor' => 'required',
                    'image' => 'mimes:jpeg,png|max:100'
                ));
            }
             if ($request->radiotype1 == 'Lcd') {
                $this->validate($request, array(
                    'type_id' => 'required',
                    'value' => 'required|numeric',
                    'value_factor' => 'required',
                    'screen_id'=>'required'
                ));
            }
            if ($request->parent_id == '') {
                 $client_id = 0;
                } else {
                    $client_id = $request->parent_id;
            }
             if ($request->get('status') != null) {
                    $status = 'active';
                } else {
                    $status = 'inactive';
                }
            
            $products = Product::where('client_id',$client_id)  
                        ->where('model',$request->model)
                        ->where('type_id',$request->type_id)
                        ->where('brand_id',$request->brand_id)
                        ->where('processor_id',$request->processor_id)
                        ->where('screen_id',$request->screen_id)
                        ->where('hdd_id',$request->hdd_id)
                        ->where('ram_id',$request->ram_id)
                        ->where('dvd_id',$request->dvd_id)
                        ->where('value',$request->value)
                        ->where('value_factor',$request->value_factor)
                        ->where('status',$status)
                        ->where('country_id',$request->country_id)
                        ->where('id','!=',$request->input('id'))
                        ->get();
                      
            if(count($products) >0 ){

                return redirect()->back()->withErrors(['Duplicate' => 'You can not create duplicate product for same client']);
            }

            $Product = Product::find($request->input('id'));

            if (!empty($Product)) {
                if (isset($request->image) && !empty($request->image)) {
                    $file = array('image' => $request->image);
                    $destinationPath = public_path() . '/product/';
                    $extension = Input::file('image')->getClientOriginalExtension();
                    $now = time();
                    $fileName = $now . '.' . $extension;
                    Input::file('image')->move($destinationPath, $fileName);
                    $Product->image = $fileName;
                } else {
                    unset($Product->image);
                }

               
                $Product->client_id =$client_id;
                $Product->model = $request->get('model');
                $Product->processor_id = $request->get('processor_id');
                $Product->type_id = $request->get('type_id');
                $Product->brand_id = $request->get('brand_id');
                $Product->screen_id = $request->get('screen_id');
                $Product->hdd_id = $request->get('hdd_id');
                $Product->ram_id = $request->get('ram_id');
                $Product->dvd_id = $request->get('dvd_id');
                $Product->value = $request->get('value');
                $Product->status = $status;
                $Product->value_factor = $request->get('value_factor');
				$Product->country_id = $request->get('country_id');
                $Product->pricing_type = $request->radiotype1;
                
                if ($Product->save()) {
                    Toast::success('Record has been updated.', 'Success');
                    return Redirect('products/index');
                } else {
                    Toast::error('Product not updated, Please try again.', 'Error');
                    return Redirect('products/index');
                }
            } else {
                Toast::error('Invalid product Id.', 'Error');
                return Redirect('products/index');
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function multipleDelete(Request $request) {
        if ($this->filter($this->permission)) {

			$ids = $request->get('ids');

			$Quotes = Quote::Where('status', '=', '1')->get();
			if(count($Quotes) >0 ){
				foreach($Quotes as $Quote){
					$delItem = FALSE;

					//$QuoteItems = QuoteItem::where('quote_id', '=', $Quote->id)->WhereIn('product_id', $request->get('ids'))->get();
					$QuoteItems = QuoteItem::where('quote_id', '=', $Quote->id)->where(function ($query) use ($ids) {
										$query->whereIn('product_id', $ids)
												->orWhereIn('model_id', $ids);
									})->get();

					if(count($QuoteItems) >0 ){
						foreach($QuoteItems as $QuoteItem){
							QuoteItemsDefect::where('quote_item_id',$QuoteItem->id)->delete();
							$QuoteItem->delete();
							$delItem = TRUE;
						}
					}

					if($delItem){
						$calculate = new CalculationsController();
						$calculate->Recalculate($Quote->id);

						$QuotesNotes = new QuotesNotes();
						$QuotesNotes->quote_id = $Quote->id;
						$QuotesNotes->user_id = Auth::user()->id;
						$QuotesNotes->display_level = 1;
						$QuotesNotes->note = 'product deleted by admin';
						$QuotesNotes->save();
					}
				}
			}

            if (Product::destroy($request->get('ids'))) {
                Toast::success('Record has been deleted.', 'Success');
                return Redirect('products/index');
            } else {
                Toast::error('Invalid id, Please try again!', 'Error');
                return Redirect('products/index');
            }

        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');

        }

    }

    public function Delete($id = Null) {
        if ($this->filter($this->permission)) {
            $product = Product::find($id);
			if (!empty($product)) {

				$Quotes = Quote::Where('status', '=', '1')->get();
				if(count($Quotes) >0 ){
					foreach($Quotes as $Quote){
						$delItem = FALSE;

						$QuoteItems = QuoteItem::where('quote_id', '=', $Quote->id)->where(function ($query) use ($id) {
											$query->where('product_id', '=', $id)
												  ->orWhere('model_id', '=', $id);
										})->get();

						if(count($QuoteItems) >0 ){
							foreach($QuoteItems as $QuoteItem){
								QuoteItemsDefect::where('quote_item_id',$QuoteItem->id)->delete();
								$QuoteItem->delete();
								$delItem = TRUE;
							}
						}

						if($delItem){
							$calculate = new CalculationsController();
							$calculate->Recalculate($Quote->id);

							$QuotesNotes = new QuotesNotes();
							$QuotesNotes->quote_id = $Quote->id;
							$QuotesNotes->user_id = Auth::user()->id;
							$QuotesNotes->display_level = 1;
							$QuotesNotes->note = 'product deleted by admin';
							$QuotesNotes->save();
						}
					}
				}
         
                $product->delete();
                Toast::success('Record has been deleted.', 'Success');
                return Redirect('products/index');
            } else {
                Toast::error('Invalid id, Please try again!', 'Error');
                return Redirect('products/index');
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function clones($id = null) {
        if ($this->filter($this->permission)) {
            if ($id != null) {
                $product = Product::find($id);
                if (!empty($product)) {

					$newProduct = $product->replicate();
                    $newProduct->clone_id = $product->id;

                    if ($newProduct->save()) {
                        Toast::success('Record has been cloned.', 'Success');
                        return Redirect('products/editProduct/' . $newProduct->id);
                    } else {
                        Toast::error('Invalid id, Please try again!', 'Error');
                        return Redirect('products/index');
                    }
                } else {
                    Toast::error('Invalid id, Please try again!', 'Error');
                    return Redirect('products/index');
                }
            } else {
                Toast::error('Invalid id, Please try again!', 'Error');
                return Redirect('products/index');
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

	//get brand according to product type
    public function getBrand(Request $request) {
        $typeId = $request->typeId;

        if ($typeId) {

            $brands = ProductOption::select('product_options.name as brand_name', 'product_options_link.brand_id')
                            ->join('product_options_link', 'product_options_link.brand_id', '=', 'product_options.id')
                            ->where('product_options_link.type_id', '=', $typeId)
							->orderBy('product_options.name', 'asc')
                            ->get()
							->toArray();
            echo json_encode($brands);
        }
    }
}
