<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use DateTime;
use links;
use App\User;
use Toast;
use App\Country;
use App\Client;
use App\CountryRegion;
use App\PriceLogistic;
use App\LogisticsUnitNumber;
use App\PriceMarginAdjustment;
use App\Quote;
use App\QuoteItem;
use App\QuoteItemsDefect;
use App\QuotesNotes;
use App\Http\Controllers\CalculationsController;
use App\Http\Controllers\ClientsController;

class PriceMarginAdjustmentsController extends Controller {

    private $permission = [1];

    public function __construct() {
        $this->middleware('auth');
    }

    
    public function index(Request $request) {
        if ($this->filter($this->permission)) {
            
            $PriceMarginAdjustments = PriceMarginAdjustment::with('Clientname', 'Countryname', 'ProductOtionType', 'ProductOtionBrand', 'ProductOtionScreen')->orderBy('id', 'desc')->get();
           
             
            return view('PriceMarginAdjustments/index', array('PriceMarginAdjustments' => $PriceMarginAdjustments));
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function addPriceMarginAdjustments() {
        if ($this->filter($this->permission)) {
            
            $clients = new ClientsController();
            $cids = Array();
            $parent_ids = $clients->getTree($cids, 0, 0, '0', '', '');
            $country = DB::table('country')->get();
            $product_options = DB::table('product_options')->get();

            return view('PriceMarginAdjustments/addPriceMarginAdjustments', array(
                'parent_ids' => $parent_ids,
                'country' => $country,
                'product_options' => $product_options
            ));
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function insertPriceMarginAdjustments(Request $request) {
        if ($this->filter($this->permission)) {
        Input::flash();
        
        $this->validate($request, array(
            'margin_type' => 'required',
            'value' => 'required|numeric|between:-9999999999.99,9999999999.99'
        ));
        $status = $request->status;
        if ($status == 'on') {
            $status = 'active';
        } else {
            $status = 'Inactive';
        }
         
        $PriceMarginAdjustment = new PriceMarginAdjustment();
        $PriceMarginAdjustment->fill($request->all());
        $PriceMarginAdjustment->status = $status;
        $PriceMarginAdjustment->client_id = $request->parent_id;
           
        if ($PriceMarginAdjustment->save()) {
           

            $this->recalcInProgresQuotes();
            

            Toast::success('Record has been saved .', 'Success');
            return redirect('/PriceMarginAdjustments/index');
        } else {
            Toast::error('Record not saved', 'Error');
            return redirect()->route('/PriceMarginAdjustments/index')->withInput();
        }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function editPriceMarginAdjustments($id = null) {
        if ($this->filter($this->permission)) {
        if ($id != null) {
            $PriceMarginAdjustments = PriceMarginAdjustment::find($id);
            $country = DB::table('country')->get();
            $product_options = DB::table('product_options')->get();
           
            $clients = new ClientsController();
            $cids = Array();
            $parent_ids = $clients->getTree($cids, 0, 0, '0', '', '');
            return view('PriceMarginAdjustments/editPriceMarginAdjustments', array(
                'parent_ids' => $parent_ids,
                'country' => $country,
                'product_options' => $product_options,
                'PriceMarginAdjustments' => $PriceMarginAdjustments
            ));
        } else {
            Toast::error('Invalid id !!!', 'Error');
            return redirect()->route('/PriceMarginAdjustments/index')->withInput();
        }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function updatePriceMarginAdjustments(Request $request, $id = null) {
          if ($this->filter($this->permission)) {
        Input::flash();
        $this->validate($request, array(
            'margin_type' => 'required',
            'value' => 'required|numeric|between:-9999999999.99,9999999999.99'
        ));
        $status = $request->status;
        if ($status == 'on') {
            $status = 'active';
        } else {
            $status = 'Inactive';
        }
        if ($id != null) {
            $PriceMarginAdjustment = PriceMarginAdjustment::find($id);
            $PriceMarginAdjustment->fill($request->all());
            $PriceMarginAdjustment->status = $status;
            $PriceMarginAdjustment->client_id = $request->parent_id;
            $PriceMarginAdjustment->save();
			$this->recalcInProgresQuotes();

            Toast::success('Record has been updated.', 'Success');
            return redirect('/PriceMarginAdjustments/index');
        } else {
            Toast::error('Record not saved', 'Error');
            return redirect('/PriceMarginAdjustments/index');
        }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function multipleDelete(Request $request) {
         if ($this->filter($this->permission)) {
        if (PriceMarginAdjustment::destroy($request->get('ids'))) {
			$this->recalcInProgresQuotes();

            Toast::success('Records has been deleted.', 'Success');
            return redirect('/PriceMarginAdjustments/index');
        } else {
            Toast::error('Please select any record.', 'Error');
            return redirect('/PriceMarginAdjustments/index');
        }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function delete($id = Null) {
        if ($this->filter($this->permission)) {
        if ($id != null) {
            $PriceMarginAdjustment = PriceMarginAdjustment::find($id);
            if (!empty($PriceMarginAdjustment)) {
                $PriceMarginAdjustment->delete();
				$this->recalcInProgresQuotes();

                Toast::success('Records has been deleted.', 'Success');
                return redirect('/PriceMarginAdjustments/index');
            } else {
                Toast::error('Invalid id, Please try again!', 'Error');
                return redirect('/PriceMarginAdjustments/index');
            }
        } else {
            Toast::error('Please provide id !!', 'Error');
            return redirect('/PriceMarginAdjustments/index');
        }
        } else {
           Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

	private function recalcInProgresQuotes(){
		//need to recalulate all inprogress quotes.			
		$Quotes = Quote::where('status', '=', '1')->get();
           
		if(count($Quotes) >0 ){
			foreach($Quotes as $Quote){
                 
				$calculate = new CalculationsController();
                  
				$calculate->Recalculate($Quote->id);
                
				$QuotesNotes = new QuotesNotes();
                 
				$QuotesNotes->quote_id = $Quote->id;
                
				$QuotesNotes->user_id = Auth::user()->id;

				$QuotesNotes->display_level = 1;

				$QuotesNotes->note = 'Admin margin adjustment change - recalculation of in-progress quote.';
               
				$QuotesNotes->save();
                  

			}
		}
	}

}
