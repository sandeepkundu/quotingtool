<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use DateTime;
use links;
use App\User;
use Toast;
use App\Client;
use App\ProductOption;
use App\ProductFactor;
use App\Quote;
use App\QuoteItem;
use App\QuoteItemsDefect;
use App\QuotesNotes;
use App\Http\Controllers\CalculationsController;
use App\Http\Controllers\ClientsController;

class ProductFactorsController extends Controller {

     private $permission = [1];

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    public function index(Request $request) {
        if ($this->filter($this->permission)) {
                   
            $ser = key($_GET);

            $ProductFactors = ProductFactor::with('ProductOtionType','ProductOtionProcessor','ProductOtionHDD','Clientname')->where('factor_type', '=', key($_GET))->get();
            
            return view('ProductFactors.index', array('ProductFactors' => $ProductFactors,'ser' => $ser));
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addProductFactors() {
        if ($this->filter($this->permission)) {
            // get clients
            $clients = new ClientsController();
            $cids = Array();
            $parent_ids = $clients->getTree($cids, 0, 0, '0', '', '');
            $product_options = ProductOption::orderBy('name','asc')->get();
            $products = ProductOption::orderBy('order','asc')->where('type','HDD')->get();
         
            return view('ProductFactors.addProductFactors', compact('product_options', 'parent_ids','products'));
        } else {
             Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function insertProductFactors(Request $request) {
        if ($this->filter($this->permission)) {

            Input::flash();
            $this->validate($request, array(
                'type_id' => 'required',
                'processor_id' => 'required',
                'option_id' => 'required',
                    //'factor_type' => 'required' 
            ));
            $ProductFactor = new ProductFactor();
            $ProductFactor->fill($request->all());
            $ProductFactor->client_id = $request->parent_id;
            if ($ProductFactor->save()) {

				/* recalulate in-progress quote */
				if(isset($request->option_id) && is_numeric($request->option_id)){
					$this->recalcInProgresQuotes($request->option_id);
				}

                Toast::success('Record has been saved successfully.', 'Success');
                return redirect('/ProductFactors/index?' . $request->cat);
            } else {
                Toast::error('Record not saved', 'Error');
                return redirect()->route('/ProductFactors/index?' . $request->cat)->withInput();
            }
        } else {
             Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');

        }
    }

    public function editProductFactors($id = null) {
        if ($this->filter($this->permission)) {
            $arr = $array = $arr1 = array();
            if ($id != null) {
               
                $ProductFactor = ProductFactor::find($id);
               
                $product_options = ProductOption::orderBy('name','asc')->get();
                $products = ProductOption::orderBy('order','asc')->where('type',key($_GET))->get();

                 $parent_id = Client::where('parent_id', '=', 0)->where('id', '!=', '0')->where('id', '!=', $id)->get();
                $clients = new ClientsController();
                $cids = Array();
                $parent_ids = $clients->getTree($cids, 0, 0, '0', '', '');
                return view('ProductFactors/editProductFactors', array(
                    'parent_ids' => $parent_ids,
                    'product_options' => $product_options,
                    'ProductFactor' => $ProductFactor,
                    'products'=>$products
                ));
            } else {
                Toast::error('Invalid id !!', 'Error');
                return redirect()->route('/ProductFactors/index');
            }
        } else {
             Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateProductFactors(Request $request, $id) {
        if ($this->filter($this->permission)) {
            Input::flash();
            $this->validate($request, array(
                'type_id' => 'required',
                'processor_id' => 'required',
                'option_id' => 'required',
                'factor_type' => 'required'
            ));
            if ($id != null) {
                $ProductFactor = ProductFactor::find($id);
				$old_option_id = $ProductFactor->option_id;
                $ProductFactor->fill($request->all());
                $ProductFactor->client_id = $request->parent_id;
                $ProductFactor->save();

				/* recalulate in-progress quote */

				if(isset($request->option_id) && is_numeric($request->option_id)){
                   
					$this->recalcInProgresQuotes($request->option_id);
                    
				}

                Toast::success('Record has been updated successfully.', 'Success');
                return redirect('/ProductFactors/index?' . $request->cat);
            } else {
                Toast::error('Record not saved', 'Error');
                return redirect()->route('/ProductFactors/index?' . $request->cat);
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteProductFactors($id = null) {
        if ($this->filter($this->permission)) {
            $ser = key($_GET);
            if ($id != null) {
                $ProductFactor = ProductFactor::find($id);
                $option_id = $ProductFactor->option_id;

				$this->recalcInProgresQuotes($option_id);
                
                if ($ProductFactor->delete()) {
                    Toast::success('Records has been deleted.', 'Success');
                    return redirect('/ProductFactors/index?' . $ser);
                } else {
                    Toast::error('Invalid id, Please try again!', 'Error');
                    return redirect('/ProductFactors/index?' . $ser);
                }
            } else {
                Toast::error('Please provide id !!', 'Error');
                return redirect('/ProductFactors/index?' . $ser);
            }
        } else {
             Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');

        }
    }

	private function recalcInProgresQuotes($option_id = 0){
		//need to recalulate all inprogress quotes.
		$Quotes = Quote::where('status', '=', '1')->get();
		if(count($Quotes) > 0 ){
			foreach($Quotes as $Quote){
				//check for quotes with either the ram or dvd matching the option id
				$QuoteItems = QuoteItem::where('id' , '=', $Quote->id)
									->where(function($q) use ($option_id) {
										$q->where('ram_id', $option_id)
										->orWhere('hdd_id', $option_id);
									})
									->get();

				//$QuoteItems = QuoteItem::orWhere(['ram_id'=>$option_id,'hdd_id'=>$option_id])->get();
				if(count($QuoteItems) >0 ){
					//items exists for the therfore recalc the quote
					$calculate = new CalculationsController();
					$calculate->Recalculate($Quote->id);

					$QuotesNotes = new QuotesNotes();
					$QuotesNotes->quote_id = $Quote->id;
					$QuotesNotes->user_id = Auth::user()->id;
					$QuotesNotes->display_level = 1;
					$QuotesNotes->note = 'DVD/Defect admin change - recalculation of in-progress quote.';
					$QuotesNotes->save();
				}
			}
		}
	}

    public function multipleDelete(Request $request) {
        if ($this->filter($this->permission)) {
            $ser = key($_GET);
            $ids = $request->get('ids');
            $arr = array();

            $ProductFactors = ProductFactor::whereIn('id',$ids)->get();
            foreach ($ProductFactors as $ProductFactor) {
                $arr[] = $ProductFactor->option_id;
				$this->recalcInProgresQuotes($ProductFactor->option_id);
            }

            if (ProductFactor::destroy($request->get('ids'))) {
                Toast::success('Records has been deleted.', 'Success');
                return redirect('/ProductFactors/index?' . $ser);
            } else {
                Toast::error('Please select any record.', 'Error');
                return redirect('/ProductFactors/index?' . $ser);
            }
        } else {
             Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');

        }
    }

}
