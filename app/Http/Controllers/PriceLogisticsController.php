<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use DateTime;
use links;
use App\User;
use Toast;
use App\Country;
use App\Client;
use App\CountryRegion;
use App\PriceLogistic;
use App\LogisticsUnitNumber;
use App\Http\Controllers\ClientsController;

class PriceLogisticsController extends Controller {

     private $permission = [1];

    public function __construct() {
        $this->middleware('auth');
    }

   
    public function index(Request $request) {

        if ($this->filter($this->permission)) {
            
            $prices  = PriceLogistic::with('Clientname','countryname','Regionname','logisticname')->orderBy('id','desc')->get();
         
//            $PriceLogistic = PriceLogistic::select('price_logistics.id as id', 'clients.name as client_name', 'price_logistics.client_id', 'price_logistics.value as value', 'price_logistics.country_id', 'price_logistics.region_id', 'price_logistics.logistic_unit_id', 'country.name as countryname', 'country_region.name as region', 'logistics_unit_numbers.value_from as f_value', 'logistics_unit_numbers.value_to as t_value');
//            
//            $PriceLogistic->leftjoin('country', 'price_logistics.country_id', '=', 'country.id');
//            $PriceLogistic->leftjoin('country_region', 'price_logistics.region_id', '=', 'country_region.id');
//            $PriceLogistic->leftjoin('clients', 'price_logistics.client_id', '=', 'clients.id');
//            $PriceLogistic->leftjoin('logistics_unit_numbers', 'price_logistics.logistic_unit_id', '=', 'logistics_unit_numbers.id');
//
//            $prices = $PriceLogistic->orderBy('countryname', 'asc')->orderBy('region', 'asc')->orderBy('f_value', 'asc')->orderBy('clients.name', 'asc')->paginate($lim);

         
            return view('PriceLogistics/index', array('prices' => $prices));
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function addPriceLogistics() {

        if ($this->filter($this->permission)) {
            $countries = Country::orderBy('name', 'asc')->get();
            $logistics_unit_numbers = LogisticsUnitNumber::orderBy('value_from', 'asc')->get();
            $parent_id = Client::where('parent_id', '=', 0)->where('id', '!=', '0')->get();
            $clients = new ClientsController();
            $cids = Array();
            $parent_ids = $clients->getTree($cids, 0, 0, '0', '', '');
           
            return view('PriceLogistics/addPriceLogistics', array(
                'countries' => $countries,
                'parent_ids' => $parent_ids,
                'logistics_unit_numbers' => $logistics_unit_numbers
            ));
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function insertPriceLogistics(Request $request) {
        if ($this->filter($this->permission)) {
            Input::flash();
            $this->validate($request, array(
                'country_id' => 'required',
                'logistic_unit_id' => 'required',
                'region_id' => 'required',
                'value' => 'required|numeric|between:0,9999999999.99',
            ));

            $PriceLogistic = new PriceLogistic();

            $PriceLogistic->fill($request->all());
            $PriceLogistic->client_id = $request->parent_id;

            if ($PriceLogistic->save()) {
                Toast::success('Record has been saved successfully.', 'Success');
                return redirect('/PriceLogistics/index');
            } else {
                Toast::error('Record not saved', 'Error');
                return redirect()->route('/PriceLogistics/index')->withInput();
            }
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function editPriceLogistics($id = null) {
        if ($this->filter($this->permission)) {
            $arr = $array = $arr1 = array();
            $priceLogistic = PriceLogistic::find($id);
            $priceLogistic->country_id;
            $countries = Country::orderBy('name', 'asc')->get();

            $country_region = CountryRegion::where('country_id', '=', $priceLogistic->country_id)->orderBy('name', 'asc')->get();

            $parent_id = Client::where('parent_id', '=', 0)->where('id', '!=', '0')->where('id', '!=', $id)->get();

             $clients = new ClientsController();
            $cids = Array();
            $parent_ids = $clients->getTree($cids, 0, 0, '0', '', '');
            $logistics_unit_numbers = LogisticsUnitNumber::orderBy('value_from', 'asc')->get();

            return view('PriceLogistics/editPriceLogistics', array(
                'countries' => $countries,
                'parent_ids' => $parent_ids,
                'logistics_unit_numbers' => $logistics_unit_numbers,
                'priceLogistic' => $priceLogistic,
                'country_region' => $country_region
            ));
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function updatePriceLogistics(Request $request, $id = null) {
        if ($this->filter($this->permission)) {
            Input::flash();
            $this->validate($request, array(
                'country_id' => 'required',
                'logistic_unit_id' => 'required',
                'region_id' => 'required',
                'value' => 'required|numeric|between:0,9999999999.99',
            ));
            $PriceLogistic = PriceLogistic::find($request->input('id'));
            if (!empty($PriceLogistic)) {
                $PriceLogistic->client_id = $request->get('parent_id');
                $PriceLogistic->country_id = $request->get('country_id');
                $PriceLogistic->logistic_unit_id = $request->get('logistic_unit_id');
                $PriceLogistic->region_id = $request->get('region_id');
                $PriceLogistic->value = $request->get('value');

                if ($PriceLogistic->save()) {
                    Toast::success('Record has been updates successfully.', 'Success');
                    return redirect('/PriceLogistics/index');
                } else {
                    Toast::error('Record not udpated', 'Error');
                    return redirect()->route('/PriceLogistics/index')->withInput();
                }
            } else {
                Toast::error('Invalid id', 'Error');
                return redirect()->route('/PriceLogistics/index')->withInput();
            }
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function country_region(Request $request) {
        if ($this->filter($this->permission)) {
            if (!empty($request->input('id'))) {
                $country_region = DB::table('country_region')->where('country_id', '=', $request->input('id'))->orderBy('name', 'asc')->get();
                echo json_encode(array('country_region' => $country_region));
                die;
            }
        } else {
            Toast::error('You have not permission.', 'Error');
            return redirect('/');
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function multipleDelete(Request $request) {
        if ($this->filter($this->permission)) {
            if (PriceLogistic::destroy($request->get('ids'))) {
                Toast::success('Records has been deleted.', 'Success');
                return Redirect('PriceLogistics/index');
            } else {
                Toast::error('Please select any record.', 'Error');
                return Redirect('PriceLogistics/index');
            }
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function Delete($id = Null) {
        if ($this->filter($this->permission)) {
            if ($id != null) {
                $PriceLogistic = PriceLogistic::find($id);
                if (!empty($PriceLogistic)) {
                    $PriceLogistic->delete();
                    Toast::success('Records has been deleted.', 'Success');
                    return Redirect('PriceLogistics/index');
                } else {
                    Toast::error('Invalid id, Please try again!', 'Error');
                    return Redirect('PriceLogistics/index');
                }
            } else {
                Toast::error('Please provide id !!', 'Error');
                return Redirect('PriceLogistics/index');
            }
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function country_regions(Request $request) {
        if ($this->filter($this->permission)) {
            if (!empty($request->input('id'))) {
                $data = CountryRegion::where('country_id', '=', $request->input('id'))->orderBy('name', 'asc')->get();
                return response()->json($data);
            }
        } else {
           
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

}
