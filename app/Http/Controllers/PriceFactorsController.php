<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use DateTime;
use links;
use App\User;
use Toast;
use App\Client;
use App\PriceFactor;
use App\ProductOption;

use App\Http\Controllers\CalculationsController;

use App\Http\Controllers\ClientsController;


class PriceFactorsController extends Controller {

    private $permission = [1];

    public function __construct() {
        $this->middleware('auth');
    }


    public function index(Request $request) {
        if ($this->filter($this->permission)) {

            $ser = key($_GET);
			//can only be HDD or RAM
			$ser = ($ser === 'HDD') ? 'HDD' : 'RAM';

            $PriceFactors = PriceFactor::Where('factor_name', '=', $ser)
				->leftjoin('clients', 'price_factors.client_id', '=', 'clients.id')
				->leftjoin('product_options', 'price_factors.type_id', '=', 'product_options.id')
				->select('price_factors.*', 'clients.fullpath AS fullPath', 'product_options.name AS type')
				->get();
            return view('PriceFactors.index', compact('PriceFactors', 'lim', 'ser'));
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addPriceFactors() {
        if ($this->filter($this->permission)) {

            $productOptions = ProductOption::select(DB::raw('count(*) as type_count, type'))->groupBy('type')->get();
			$types = ProductOption::where('type', '=', 'Product')->orderby('name', 'asc')->get();            
			
            $clients = new ClientsController();
            $cids = Array();
            $parent_ids = $clients->getTree($cids, 0, 0, '0', '', '');
          
  
            return view('PriceFactors/addPriceFactors', compact('productOptions', 'parent_ids', 'types'));

        } else {
           Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function insertPriceFactors(Request $request) {
        if ($this->filter($this->permission)) {
            Input::flash();
            $this->validate($request, array(
                'factor_name' => 'required',
                'variance_to' => 'required|numeric',
                'variance_from' => 'required|numeric',
                'value' => 'required|numeric',
				'type_id' => 'nullable|numeric'
            ));
            $PriceFactor = new PriceFactor();
            $PriceFactor->fill($request->all());
			$PriceFactor->type_id = $request->type_id;
            $PriceFactor->client_id = $request->parent_id;
            if ($PriceFactor->save()) {

				//recalculate all in progress quotes
				$calculate = new CalculationsController();
				$calculate->RecalculateInProgressQuotes();

                Toast::success('Record has been saved successfully.', 'Success');
                return redirect('/PriceFactors/index?' . $request->cat);
            } else {
                Toast::error('Record not saved', 'Error');
                return redirect()->route('/PriceFactors/index?' . $request->cat)->withInput();
            }
        } else {
           Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editPriceFactors($id = null) {
        if ($this->filter($this->permission)) {
             $arr = $array = $arr1 = array();
            if ($id != null) {
                $PriceFactor = PriceFactor::find($id);
                $productOptions = DB::table('product_options')->select(DB::raw('count(*) as type_count, type'))->groupBy('type')->get();

				$types = ProductOption::where('type', '=', 'Product')->orderby('name', 'asc')->get();            
				$clients = new ClientsController();
				$cids = Array();
				$parent_ids = $clients->getTree($cids, 0, 0, '0', '', '');
                return view('PriceFactors/editPriceFactors', array(
                    'parent_ids' => $parent_ids,
                    'productOptions' => $productOptions,
                    'PriceFactor' => $PriceFactor,
					'types' => $types
                ));
            } else {
                Toast::error('Invalid id !!', 'Error');
                return redirect()->route('/PriceFactors/index');
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatePriceFactors(Request $request, $id) {
        if ($this->filter($this->permission)) {
            Input::flash();
            $this->validate($request, array(
                'factor_name' => 'required',
                'variance_to' => 'required|numeric',
                'variance_from' => 'required|numeric',
                'value' => 'required|numeric',
				'type_id' => 'nullable|numeric'
            ));
              
            if (isset($id) && is_numeric($id) && $id > 0) {

                $PriceFactor = PriceFactor::find($id);
                $PriceFactor->fill($request->all());
				$PriceFactor->type_id = $request->type_id;
                $PriceFactor->client_id = $request->parent_id;
                $PriceFactor->save();
                  
				//recalculate all in progress quotes
				$calculate = new CalculationsController();
				$calculate->RecalculateInProgressQuotes();

                Toast::success('Record has been saved successfully.', 'Success');
                return redirect('/PriceFactors/index?' . $request->cat);
            } else {
                Toast::error('Record not saved', 'Error');
                return redirect()->route('/PriceFactors/index?' . $request->cat)->withInput();
            }
        } else {
           Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deletePriceFactors($id = null) {
        if ($this->filter($this->permission)) {
            if ($id != null) {
                $ser = key($_GET);

                $PriceFactor = PriceFactor::find($id);
                if ($PriceFactor->delete()) {
					//recalculate all in progress quotes
					$calculate = new CalculationsController();
					$calculate->RecalculateInProgressQuotes();

                    Toast::success('Records has been deleted.', 'Success');
                    return redirect('/PriceFactors/index?' . $ser);
                } else {
                    Toast::error('Invalid id, Please try again!', 'Error');
                    return redirect('/PriceFactors/index?' . $ser);
                }
            } else {
                Toast::error('Please provide id !!', 'Error');
                return redirect('/PriceFactors/index?' . $ser);
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function multipleDelete(Request $request) {
        if ($this->filter($this->permission)) {
            $ser = key($_GET);
            if (PriceFactor::destroy($request->get('ids'))) {
				//recalculate all in progress quotes
				$calculate = new CalculationsController();
				$calculate->RecalculateInProgressQuotes();

                Toast::success('Records has been deleted.', 'Success');
                return redirect('/PriceFactors/index?' . $ser);
            } else {
                Toast::error('Please select any record.', 'Error');
                return redirect('/PriceFactors/index?' . $ser);
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

}
