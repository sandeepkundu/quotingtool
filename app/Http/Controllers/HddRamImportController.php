<?php
namespace App\Http\Controllers;

ini_set('max_execution_time', 600);

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Session;
use DateTime;
use links;
use Toast;
use App\ProductOption;
use View;
use App\IntegrationImport;
use App\PriceBands;
use App\PriceFactorBands;
use App\ProductFactor;
use App\Quote;
use App\QuoteItem;
use App\QuoteItemsDefect;
use App\QuotesNotes;
use App\Http\Controllers\CalculationsController;


class HddRamImportController extends Controller
{
	 private $permission = [1];
	public function __construct() {
		$this->middleware('auth');
		date_default_timezone_set('Asia/Kolkata');
	}
	public function index(){
          
          if (!$this->filter($this->permission)) {
                   
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
		$IntegrationImports = IntegrationImport::where('source','=','hddram')->orderBy('date','DESC')->get();
		//dd($IntegrationImports);
		return view('Variances/BulkHddRamImports',array(
			'IntegrationImports'=> $IntegrationImports,
			));

	}
	public function BulkHddRamStore(Request $request){

		$debug = false;
		$folder         =  public_path().'/i/';
		$datestring = date_format(new DateTime, 'Y-m-d-H-i-s');
		$importfilename = 'hddram-import-' . $datestring . '.csv';
		$exportfilename = 'hddram-export-' . $datestring . '.csv';

		/*
			1. Save uploaded file to server 
		*/	
			if (! isset($_FILES["fileToUpload"])) {
				return  'A csv file must be selected for upload';

				Toast::error('A csv file must be selected for upload', 'Error');
				return redirect('/Variances/hdd-ram');
			}
			if ($_FILES["fileToUpload"]["error"] > 0) {

				Toast::error('A error was encountered uploading this file(' . $_FILES["fileToUpload"]["error"] . ')', 'Error');
				return redirect('/Variances/hdd-ram');
			}

			$ftype = explode(".",$_FILES['fileToUpload']['name']);

			if(strtolower(end($ftype)) !== 'csv'){
				Toast::error('only a csv can be uploaded', 'Error');
				return redirect('/Variances/hdd-ram');

			}
			if(move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $folder. $importfilename) === FALSE){

				Toast::error('File could not be saved', 'Error');
				return redirect('/Variances/hdd-ram');
			}


			/*--2. ----Create a new import record------*/

			$IntegrationImport               = new IntegrationImport();
			$IntegrationImport->date         = date('Y-m-d-H-i-s');
			$IntegrationImport->fileimported = $importfilename;
			$IntegrationImport->source       = 'hddram';
			$IntegrationImport->save();

			$import_id = $IntegrationImport->id;

		//reset all records so that none are flagged as being updated

			DB::update(DB::raw("UPDATE `price_factors_bands` SET processed = 0"));

		/*
			3. Process the file 
		*/
			$row = 0;
			$columncount = 0;
			$processrecords = array();
			$columnames = array();
			$isValid = FALSE;
			$num_invalid = 0;
			$num_valid = 0;

			if (($handle = fopen($folder. $importfilename, "r")) !== FALSE) {

				while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
					$row ++;
					if($row == 1){
						$columncount = count($data);
					//dd($data);

						$columnames = $data;

					} else if (count($data) != 5){

						Toast::error('Record has incorrect number of columns ', 'Error');
						return redirect('/Variances/hdd-ram');

					// incorrect number of columns for the row
						array_unshift($data, "[record has incorrect number of columns (".  count($data) . ")]");			
					//dd($data);	
						array_push($processrecords, $data);
						$num_invalid ++;

					} else {


					$factor		= $data[0];		// Factor				e.g. HDD or RAM
					$type		= $data[1];		// Product Type			e.g. Notebook				
					$processor	= $data[2];		// Processor			e.g. Core i3 1st gen							
					$hddram		= $data[3];		// HDD					e.g. 500GB	
					$client		= $data[4];		// Client				e.g. HP India store A			

					// validate the data
					$validationmessage = '';
					$isValid = FALSE;

					if(!isset($factor) || ($factor !== 'HDD' && $factor !== 'RAM')){
						$validationmessage .= "[the factor can only be HDD or RAM]";

					}			

					if(!isset($type) || $type === '' || !isset($processor) || $processor === ''){
						$validationmessage .= "[both processor and product type are required]";
						//dd($validationmessage);
					}		

					if($debug){
						echo '<pre>';
						print_r($data);
						echo '</pre>';
						echo "type = $type<br>";
						echo "processor = $processor<br>";
						echo "hddram = $hddram<br>";
						echo "factor = $factor<br>";
						echo "name = $client<br>";
					}

					// vailidate model and device components
					$args = array(
						'client' => $client, 
						'type' => $type, 
						'processor' => $processor, 
						'hdd' => $hddram, 
						'ram' => $hddram
					);
					$resultset = DB::select(DB::raw("SELECT 
						IFNULL((SELECT id FROM `clients` WHERE `name` = :client), 0) AS client,
						IFNULL((SELECT id FROM `product_options` WHERE `type` = 'Product' AND `name` = :type), 0) AS type,
						IFNULL((SELECT id FROM `product_options` WHERE `type` = 'Processor' AND `name` = :processor), 0) AS processor,
						IFNULL((SELECT id FROM `product_options` WHERE `type` = 'HDD' AND `name` = :hdd), 0) AS hdd,
						IFNULL((SELECT id FROM `product_options` WHERE 'type' = 'RAM' AND `name` = :ram), 0) AS ram"), $args);

					if(count($resultset) > 0){

						if($debug){
							echo '<pre>';
							print_r($resultset);
							echo '</pre>';
						}
						//continue;
						
						$type_id = $resultset[0]->type;
						$hdd_id = $resultset[0]->hdd;
						$ram_id = $resultset[0]->ram;
						$processor_id = $resultset[0]->processor;
						$client_id = $resultset[0]->client;

						if($type_id === 0) {
							$validationmessage .= "[product type lookup not found]";
						}
						if($processor_id === 0) {
							$validationmessage .= "[processor lookup not found]";

						}
						if($client_id === 0 && strtolower($client) !== 'default') {
							$validationmessage .= "[client not found]";
						}

						if($debug){ echo "message = $validationmessage<br>"; }

						//don't continue if there are any validation errors
						if($validationmessage === '') {
							// retreive step id
							$step_id = 0;

							$resultset2 = DB::select(DB::raw("SELECT id FROM `quote_step` WHERE `type_id` = $type_id AND (brand_id = 0 OR brand_id is null) AND (client_id = $client_id OR client_id = 0) ORDER BY client_id DESC, brand_id DESC LIMIT 1"));
							if(count($resultset2) > 0){
								if(!empty($resultset2)){ $step_id = $resultset2[0]->id; }
							}
							if($debug){ echo "step id = $step_id<br>"; }

							if(isset($hdd_id) && $hdd_id <= 0 && $factor == 'HDD'){
								//ADD HDD
								
								$ProductOption = new ProductOption();
								$ProductOption->type = 'HDD';
								$ProductOption->name = $hddram;
								$ProductOption->order = 0;
								$ProductOption->status = 'active';
								$ProductOption->isActive = 1;
								$ProductOption->description = '';
								$ProductOption->image = '';
								$ProductOption->save();

								$hdd_id = $ProductOption->id;
								$hdd = $ProductOption->name;

								//add to group
								$resultset3 = DB::insert(DB::raw("INSERT INTO product_option_groups (`group`, `group_code`, `name`, `option_id`, `order`, `status`, `client_id`) SELECT DISTINCT pgo.`group`, pgo.`group_code`, '1TB', 228, 0, 'active', 0 FROM `product_option_groups` pgo INNER JOIN `product_options` po ON pgo.option_id = po.id INNER JOIN `quote_step_items` qsi ON qsi.group_code = pgo.group_code WHERE po.type = 'HDD' AND qsi.quote_step_id = 0 "));

								$validationmessage .= "[added product part HDD]";
								$isValid = TRUE;
							}

							//dd('ss');
							if(isset($ram_id) && $ram_id <= 0 && $factor == 'RAM'){
								//ADD RAM								
								$ProductOption = new ProductOption();
								$ProductOption->type = 'HDD';
								$ProductOption->name = $hddram;
								$ProductOption->order = 0;
								$ProductOption->status = 'active';
								$ProductOption->isActive = 1;
								$ProductOption->description = '';
								$ProductOption->image = '';
								$ProductOption->save();

								$ram_id = $ProductOption->id;
								$ram = $ProductOption->name;
								
								//add to group
								DB::select(DB::raw("INSERT INTO product_option_groups (`group`, `group_code`, `name`, `option_id`, `order`, `status`, `client_id`) SELECT DISTINCT pgo.`group`, pgo.`group_code`, '".$ram."', '.$ram_id.', 0, 'active', '".$client_id."'  FROM `product_option_groups` pgo INNER JOIN `product_options` po ON pgo.option_id = po.id INNER JOIN `quote_step_items` qsi ON qsi.group_code = pgo.group_code WHERE po.type = 'HDD' AND qsi.quote_step_id = $step_id "));

								$validationmessage .= "[added product part RAM]";
								$isValid = TRUE;
							}

							$option_id = ($factor == 'RAM') ? $ram_id : $hdd_id;
							
							$arg= array(
								'client' => $client_id,
								'type' => $type_id,
								'processor' => $processor_id, 
								'option' => $option_id,
								'factor' => $factor
								);

							$resultset4 = DB::select(DB::raw("SELECT id FROM `product_factors` WHERE client_id = :client AND type_id = :type AND processor_id = :processor AND option_id = :option AND factor_type = :factor LIMIT 1"),$arg);
							
							if(count($resultset4) > 0){ 
								//exist so only update processed
								$id = $resultset4[0]->id; 
								$ProductFactor = ProductFactor::find($id);
								$ProductFactor->processed = 1;
								$ProductFactor->save();
								$validationmessage .= "[factor exists]";
								
							}else{
								//add product factor
								$ProductFactor = new ProductFactor();
								$ProductFactor->client_id = $client_id;
								$ProductFactor->type_id = $type_id;
								$ProductFactor->processor_id = $processor_id;
								$ProductFactor->option_id = $option_id;
								$ProductFactor->factor_type = $factor;
								$ProductFactor->processed = 1;

								$ProductFactor->save();
								$validationmessage .= "[factor added]";
							}
						}

					} else {
						$validationmessage .= "[error occurred validating model]";
					}
					
					if($isValid) { 						 
						$num_valid++;
					} else {
						$num_invalid ++;

					}

					array_unshift($data, $validationmessage);				
					array_push($processrecords, $data);	
				}

			}

			$resultset5 = DB::select(DB::raw("SELECT 
				'[deleted]' AS message,
				pf.factor_type AS factor,
				typ.name AS type,
				CASE WHEN pf.factor_type = 'HDD' THEN hdd.name ELSE ram.name END AS hddram,
				c.name AS client 
				FROM `product_factors` pf
				INNER JOIN `product_options` typ ON pf.type_id = typ.id 
				LEFT JOIN `product_options` hdd ON pf.option_id = hdd.id AND hdd.type = 'HDD' AND pf.factor_type = 'HDD'
				LEFT JOIN `product_options` ram ON pf.option_id = ram.id AND ram.type = 'RAM' AND pf.factor_type = 'RAM'
				LEFT JOIN `clients` c ON c.id = pf.client_id
				WHERE processed = 0"));


			if(!empty($resultset5) && count($resultset5) > 0){
				foreach ($resultset5 as $row) {

					//add each record to array for export in csv
					array_push($processrecords, 
						array (
							$row->message,
							$row->factor,
							$row->type,
							$row->hddram,
							$row->client
							)
						);	
				}
			}

			//remove all records not updated
			$resultset6 = DB::delete(DB::raw("DELETE FROM `product_factors` WHERE processed = 0"));

			//save the processed records to file.
			$exportfile = fopen($folder.$exportfilename, "w");

			array_unshift($columnames, 'message');
			fputcsv($exportfile, $columnames);
			foreach ($processrecords as $line){
				fputcsv($exportfile, $line);
			}

			/*
			4. Update import record will export file name andretrun results to 
			*/	
			$IntegrationImport					= IntegrationImport::find($import_id);
			$IntegrationImport->fileexported	= $exportfilename;
			$IntegrationImport->records_valid	= $num_valid;
			$IntegrationImport->records_invalid = $num_invalid;
			$IntegrationImport->save();

			//need to recalulate all inprogress quotes.
			$Quotes = Quote::where('status', '=', '1')->get();
			if(count($Quotes) >0 ){
				foreach($Quotes as $Quote){
					$calculate = new CalculationsController();
					$calculate->Recalculate($Quote->id);

					$QuotesNotes = new QuotesNotes();
					$QuotesNotes->quote_id = $Quote->id;
					$QuotesNotes->user_id = Auth::user()->id;
					$QuotesNotes->display_level = 1;
					$QuotesNotes->note = 'HDD/RAM bulk import - recalculation of in-progress quotes.';
					$QuotesNotes->save();
				}
			}

			//return 'import has been completed';
			Toast::Success('Import has been completed', 'Success');
			return redirect('/Variances/hdd-ram');

		} else {

			Toast::error('the imported file couldn\'t be processed', 'Error');
			return redirect('/Variances/hdd-ram');
		}
	}
}
