<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use DateTime;
use links;
use App\User;
use Toast;
use App\ProductOption;
use App\QuoteManagement;
use App\ProductOptionGroup;
use App\QuotesNotes;
use App\QuoteSteps;
use App\Quote;

class QuotesNotesController extends Controller
{
      private $permission = [1];
      
    public function __construct(){
        $this->middleware('auth');
    }
    public function index(Request $request){
    	$txt= $request->get('search');
        $limit = $request->get('perPage');
        if(isset($limit)){
            $lim=$limit;
        }else{
            $lim = 30;
        }
        $QuotesNotes=QuotesNotes::select('quote_notes.id as id','quote_notes.quote_id','quote_notes.user_id','quote_notes.date_created','quote_notes.display_level','quote_notes.note','quote.id as quote_name','u.first_name as u_firstname','users.last_name as u_lastname');        
        if (isset($txt)) {

            $QuotesNotes->Where('quote_notes.note', 'like', '%' . $txt . '%');                    
            $QuotesNotes->orWhere('u.first_name', 'like', '%' . $txt . '%');                    
        }
        $QuotesNotes->leftjoin('quote', 'quote_notes.quote_id', '=', 'quote.id');
        $QuotesNotes->leftjoin('users as u', 'quote_notes.user_id', '=', 'u.id');
        $QuotesNotes->leftjoin('users', 'quote_notes.user_id', '=', 'users.id');

        $QuotesNotes =$QuotesNotes->orderBy('quote_notes.id','asc')->paginate($lim);

        return view('QuotesNotes.index',compact('QuotesNotes','lim'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add($id=null){
        if($id != null){
            $User=User::all();
            $Quotes=QuoteManagement::all();
            return view('QuotesNotes.add',compact('User','Quotes','id'));
        }else{
            Toast::error('Please provide valid id.', 'Error');
            return Redirect::back();
        }
    	
    }
    public function insertQuotesNotes(Request $request){
    	Input::flash();       
        $this->validate($request, array(
            //'quote_id' => 'required',
            //'user_id' => 'required', 
            'display_level' => 'required|numeric',    
            'note' => 'required',   
        ));

        $QuotesNotes =new QuotesNotes();
        $QuotesNotes->fill($request->all());
        $Quote = Quote::find($request->quote_id);
        $QuotesNotes->quote_id=$Quote->id;
        $QuotesNotes->user_id=$Quote->user_id;

        if($QuotesNotes->save()){
            Toast::success('Record has been saved.', 'Success');
            return redirect('/QuoteManagements/QuoteDetails/'.$request->quote_id);
        }else{
            Toast::error('Record not saved', 'Error');
            return redirect('/QuoteManagements/QuoteDetails/'.$request->quote_id);
        }
    }

    public function edit($id=null){
        if($id!=null){
            $QuotesNotes=QuotesNotes::find($id);
            if (!empty($QuotesNotes)) {
                return view('QuotesNotes.edit', compact('QuotesNotes','id'));
            }else{
                Toast::error('Invalid id !!', 'Error');
                return redirect('/QuoteManagements/QuoteDetails/'.$id);
            }
        }else{
            Toast::error('Invalid id !!', 'Error');
            return Redirect::back();
        }
    }

    public function updateQuotesNotes(Request $request){
       Input::flash();           
       $this->validate($request, array(
            //'quote_id' => 'required',
            //'user_id' => 'required', 
            'display_level' => 'required|numeric',    
            'note' => 'required',   
        ));

       if (isset($request->id)){
            $QuotesNote= QuotesNotes::find($request->id);
            $QuotesNote->fill($request->all());
            $QuotesNote->save();
            Toast::success('Record has been updated.', 'Success');
            return redirect('/QuoteManagements/QuoteDetails/'.$request->quote_id);
        }else{
            Toast::error('Record not saved', 'Error');
            return redirect('/QuoteManagements/QuoteDetails/'.$request->quote_id);
        }
    }

    public function Delete($id=null){
        if($id!=null){
            $QuotesNotes = QuotesNotes::find($id);
            if ( $QuotesNotes->delete()) {
                Toast::success('Records has been deleted.', 'Success');
               return Redirect::back();
            }else{
                Toast::error('Invalid id, Please try again!', 'Error');
                return Redirect::back();
            }
        }else{
           Toast::error('Please provide id !!', 'Error');
           return Redirect::back();
       } 
   }

   public function multipleDelete(Request $request){
		//dd($request->all());
       if (QuotesNotes::destroy($request->get('ids'))){
           Toast::success('Records has been deleted.', 'Success');
           return redirect('/QuotesNotes/index');
       }else{
           Toast::error('Please select any record.', 'Error');
           return redirect('/QuotesNotes/index');
       }
   }
}
