<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Session;
use DateTime;
use links;
use Toast;
use App\Client;
use App\User;
use App\ClientUser;
use App\Country;
use App\CountryRegion;
use App\RoleUser;
use App\QuoteManagement;
use App\LoginSuccess;

//require_once "Mobile_Detect.php";

class ReportController extends Controller
{
    private $permission = [1];
    /*
     * |--------------------------------------------------------------------------
     * | DashboardController
     * |--------------------------------------------------------------------------
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

	//show the last and previous logins for users
    public function report_logins(Request $requet){

		
		$lim = (isset($request->perPage) && is_numeric($request->perPage) &&  $request->perPage > 0) ? $request->perPage : 30 ;
        $page = (isset($request->page) && is_numeric($request->page) && $request->page > 0) ? $request->page : 1;
         
		//retreive all users
		$date = date("Y-m-d", strtotime("-14 days")).' 00:00:00';
		$users = User::with(['logins' => function($query) use($date){
						return $query								
								->where('date_loggedin', '>=', $date)
								->orderby('date_loggedin', 'DESC')
								->get();
					}])										
					->orderby('last_login', 'DESC')
					->paginate($lim);

		$totalusers = User::get()->count();

		//dd($users);
		//print_r($users);
		//die();

		return view('Reporting/report_logins', array(
            'lim' => $lim,
			'page' => $page,
			'totalusers' => $totalusers,
            'users' => $users
        ));
        
    }
    
	public function getOS($user_agent) { 
		$os_platform    =   "Unknown OS Platform";
		$os_array       =   array(
								'/windows nt 10/i'     =>  'Windows 10',
								'/windows nt 6.3/i'     =>  'Windows 8.1',
								'/windows nt 6.2/i'     =>  'Windows 8',
								'/windows nt 6.1/i'     =>  'Windows 7',
								'/windows nt 6.0/i'     =>  'Windows Vista',
								'/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
								'/windows nt 5.1/i'     =>  'Windows XP',
								'/windows xp/i'         =>  'Windows XP',
								'/windows nt 5.0/i'     =>  'Windows 2000',
								'/windows me/i'         =>  'Windows ME',
								'/win98/i'              =>  'Windows 98',
								'/win95/i'              =>  'Windows 95',
								'/win16/i'              =>  'Windows 3.11',
								'/macintosh|mac os x/i' =>  'Mac OS X',
								'/mac_powerpc/i'        =>  'Mac OS 9',
								'/linux/i'              =>  'Linux',
								'/ubuntu/i'             =>  'Ubuntu',
								'/iphone/i'             =>  'iPhone',
								'/ipod/i'               =>  'iPod',
								'/ipad/i'               =>  'iPad',
								'/android/i'            =>  'Android',
								'/blackberry/i'         =>  'BlackBerry',
								'/webos/i'              =>  'Mobile'
							);

		foreach ($os_array as $regex => $value) { 
			if (preg_match($regex, $user_agent)) {
				$os_platform    =   $value;
			}
		}  
		return $os_platform;
	}

	public function getBrowser($user_agent) {

		$browser        =   "Unknown Browser";
		$browser_array  =   array(
								'/msie(.*)/i'   =>  'I.E.',
								'/firefox/i'    =>  'Firefox',
								'/safari/i'     =>  'Safari',
								'/chrome(.*)/i' =>  'Chrome',
								'/edge/i'       =>  'Edge',
								'/opera/i'      =>  'Opera',
								'/netscape/i'   =>  'Netscape',
								'/maxthon/i'    =>  'Maxthon',
								'/konqueror/i'  =>  'Konqueror',
								'/mobile/i'     =>  'Handheld'
							);

		foreach ($browser_array as $regex => $value) { 
			if (preg_match($regex, $user_agent)) {
				$browser    =   $value . ' ' . $this->getVersion($value, $regex, $user_agent);
			}
		}
		return $browser;

	}

	public function getVersion($browser, $search, $string){
		$version = "";
		$browser = strtolower($browser);
		preg_match_all($search,$string,$match);
	
		$m = (isset($match[1][0])) ? $match[1][0] : $match[0][0];

		switch($browser){
			case "firefox": $version = str_replace("/","",$m); break;
			case "i.e.": $version = substr($m,0,4); break;
			case "opera": $version = str_replace("/","",substr($m,0,5)); break;
			case "navigator": $version = substr($m,1,7); break;
			case "maxthon": $version = str_replace(")","",$m); break;
			case "chrome": $version = substr($m,1,10);
		}
		return $version;
	}

	// Function to get the client IP address
	public function get_client_ip() {
		$ipaddress = '';
		if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
		else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
		else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		else if(getenv('HTTP_FORWARDED'))
		   $ipaddress = getenv('HTTP_FORWARDED');
		else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
		else
			$ipaddress = 'UNKNOWN';
		return $ipaddress;
	}
  
}
