<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Session;
use DateTime;
use links;
use Toast;
use App\Client;
use App\User;
use App\ClientUser;
use App\Country;
use App\CountryRegion;
use App\RoleUser;
use App\ProductOption;
use App\Product;
use App\ProductOptionGroup;
use App\QuoteManagement;
use View;
use Response;
use Mail;
use PDF;
use URL;
use App\QuoteCollection;
use App\QuoteCollectionItem;
use App\QuoteValid;
use App\Navigation;
use App\PasswordReset;
use App\Quote;
use App\QuoteStatus;
use App\QuoteItem;
use App\QuoteItemsDefect;

class CalculationsController extends Controller{
    
    public function checkPrice(){
        $data = $this->calculate(174,224);
        //echo "<pre>";print_r($data);die;
    }

	public function calculate($quoteid, $itemid) {
                /*
                  the calculation steps array()e as follows
                  1. determiine base price
                  2. for all-in-one get screen adjustment value
                  3. add 1 and 2 together to get price for price band lookup
                  4. calc dvd adjustment is applicable
                  5. calc ram adjustment is applicable
                  6. calc hdd adjustment is applicable
                  7. add 3, 4, 5, and 6 together to get hardware price
                  8. calc margin adjustments
                  9. calc services
                  10. calc logistics
                  11. subtract 8, 9 and 10 from 7 to get the price before defects
                  12. calculate defects
                  13. subtract 12 from 11 to get the unit price
                  14. multiple the unit price by the quantity to get the total item price method returns an array of all pricing elements
                 */

                // 1. retrieve data against quote.
                //$quote_data = QuoteManagement::find($quoteid);

                $resultArray = DB::select(DB::raw("SELECT * FROM quote WHERE id = $quoteid"));
                if ($resultArray)
                    $quote_data = json_decode(json_encode($resultArray[0]), true);

                if (!isset($quote_data["client_id"]) || $quote_data["client_id"] <= 0) {
                    return null;
                }

                //echo "<pre>";print_r($resultArray);die;
                // 2. retrieve data against quote item.
                //$item_data = QuoteItem::find($itemid);
                $resultSet = DB::select(DB::raw("SELECT * FROM quote_items WHERE id = $itemid"));

                if ($resultSet){
                    $item_data = json_decode(json_encode($resultSet[0]), true);
                }

                //echo "<pre>";print_r($item_data);die;
                //dd($resultSet);

                if (!isset($item_data["type_id"]) || $item_data["type_id"] <= 0) {
                    return null;
                }

				//price locking flag
				$flag = 0;
				if(isset($quote_data['flag']) && $quote_data['flag'] > 0){
					$flag = $quote_data['flag'];
				}
				//if flag = 4 then exits as no changes to be applied aginst quote.
				if($flag == 4){
					return null;
				}

                $product_id = isset($item_data["product_id"]) && ($item_data["product_id"] > 0) ? $item_data["product_id"] : 0;
                $model_id = 0;
                if (isset($item_data["model_id"]) && is_numeric($item_data["model_id"])) {
                    $model_id = $item_data["model_id"];
                }

                /*
                  2. calc base price and return calulation step return value includes an array comprising
                  the base price and hardware config  used for it to  identify if additional hardware adjustments are required
                */
                $base_price_data = array();

				$client_id = ($quote_data["client_id"] > 0) ? $quote_data["client_id"] : 0;
				$type_id = ($item_data["type_id"] > 0) ? $item_data["type_id"] : 0;
                $brand_id = ($item_data["brand_id"] > 0) ? $item_data["brand_id"] : 0;
                $processor_id = ($item_data["processor_id"] > 0) ? $item_data["processor_id"] : 0;
                $screen_id = ($item_data["screen_id"] > 0) ? $item_data["screen_id"] : 0;
                $hdd_id = ($item_data["hdd_id"] > 0) ? $item_data["hdd_id"] : 0;
                $dvd_id = ($item_data["dvd_id"] > 0) ? $item_data["dvd_id"] : 0;
                $ram_id = ($item_data["ram_id"] > 0) ? $item_data["ram_id"] : 0;
				$margin_country = $margin_brand	= $margin_customer = $margin_screen = 0;

				if ($product_id > 0) {
                    $base_price_data = $this->calculate_base_price($product_id, $hdd_id, $dvd_id,$ram_id);
                } else { 
                    $base_price_data = $this->calculate_base_price_without_product($itemid, $client_id, $type_id, $brand_id, $processor_id, $screen_id, $hdd_id, $dvd_id, $ram_id);
                }

                $base_price = $band_price = $price = $base_price_data["value"];
                $screen_price = 0;
                $hdd_price = 0;
                $ram_price = 0;
                $model_price = 0;
                $model_margin = 0;
                $dvd_price = 0;
                $isModelPrice = false;

                // if the $model_id and $product_id are the same then this is an explicit price against the model
                // therefore don't apply any further margins or adjustments
                if ($model_id !== $product_id) {

					$margins = $this->calculate_margin_adj($client_id, $quote_data["country"], $brand_id, $type_id, $screen_id);                  

                    /* all-in-screen price ($type_id = 469 => All-in-One) */
                    if($type_id == 469 && isset($screen_id) && $screen_id > 0 && isset($brand_id) && $brand_id > 0){
                        $screen_price = $this->calculate_hw_adj($client_id, $screen_id, 'aio', 0, 0, 0, $brand_id);
                        
                        $band_price += $screen_price;
                    }

                    $resultsets = $this->productId($client_id, $type_id, $brand_id, $processor_id, $screen_id, $hdd_id, $dvd_id, $ram_id, 2, 0, $quote_data["country"]);
                    if ($resultsets) {
                        $resultset = json_decode(json_encode($resultsets[0]), true);

						if (isset($resultset["value"]) && $resultset["value"] > 0 && isset($resultset["value_factor"]) && $resultset["value_factor"] === "margin price") {
							$model_price = $resultset["value"];
							$band_price = $base_price + $model_price;
						} elseif (isset($resultset["value"]) && $resultset["value"] > 0 && isset($resultset["value_factor"]) && $resultset["value_factor"] === "margin percent") {
							$model_margin = $resultset["value"];
							$band_price = $base_price * (1 + ($model_margin / 100));
						}
					}  

                    /* if band price = 0 the return error */
                    if ($band_price === 0) {
                        //return /* error */;
                    }
                    /* dvd hardware 
                    adjustment price */
                    if ($base_price_data["dvd_required"] && isset($dvd_id) && $dvd_id > 0){
                        $dvd_price = $this->calculate_hw_adj($client_id, $dvd_id, 'dvd', $band_price, 0, 0);
                    }
                    //echo $dvd_price;die;
                    if (isset($processor_id) && $processor_id > 0) {
                        /* hdd hardware adjustment price */
                        if ($base_price_data["hdd_required"] && isset($hdd_id) && $hdd_id > 0) {

                            $hdd_price = $this->calculate_hw_adj($client_id, $hdd_id, 'hdd', $band_price, $type_id, $processor_id);
                        }
                        /* ram hardware adjustment price */
                        if ($base_price_data["ram_required"] && isset($ram_id) && $ram_id > 0) {
                            $ram_price = $this->calculate_hw_adj($quote_data["client_id"], $ram_id, 'ram', $band_price, $type_id, $processor_id);
                        }
                    }
                    
                    $price = $band_price + $dvd_price + $hdd_price + $ram_price;

					/* margin calculation */
					//pricing flag is 2 or 3 then margin are set and should not be recaluated so just populate from quote item as is
					if($flag == 2 || $flag == 3){
						$margin_country = $item_data["unit_margin_country"];
						$margin_brand	= $item_data["unit_margin_brand"];
						$margin_customer= $item_data["unit_margin_customer"];
						$margin_screen	= $item_data["unit_margin_screen"];
					} else {
						$margin_country = (isset($margins["country"])	&& is_numeric($margins["country"])	&& $margins["country"] != 0)	? $margins["country"] : 0 ;
						$margin_brand	= (isset($margins["brand"])		&& is_numeric($margins["brand"])	&& $margins["brand"] != 0)		? + $margins["brand"] 	: 0;
						$margin_customer = (isset($margins["customer"])	&& is_numeric($margins["customer"])	&& $margins["customer"] != 0)	? $margins["customer"]	: 0;		
						$margin_screen	= (isset($margins["screen"])	&& is_numeric($margins["screen"])	&& $margins["screen"] != 0)		? $margins["screen"]	: 0;
					}
					
					$margin_country_adj = ($margin_country != 0) ? (100 + $margin_country) / 100.0	: 1;
					$margin_brand_adj	= ($margin_brand != 0) ? (100 + $margin_brand) / 100.0		: 1;
					$margin_customer_adj = ($margin_customer != 0)	? (100 + $margin_customer) / 100.0	: 1;		
					$margin_screen_adj	= ($margin_screen != 0)	? (100 + $margin_screen) / 100.0	: 1;
					
                    $price =  $price *  $margin_country_adj * $margin_customer_adj * $margin_brand_adj * $margin_screen_adj;

                } else {
                    $isModelPrice = true;
                }
                

                $defect_cap = 0;
                if (isset($margins["defectcap"]) && $margins["defectcap"] > 0) {
                    $defect_cap = $margins["defectcap"];
                }

                // calcuate defects - get the sum of all defects selected against the item                
                $defect_price = 0;
                $defect_price_net = 0;
				$defects = array();

				/* loop through each selected defect */
				$QuoteItemDefects = QuoteItemsDefect::where('quote_item_id', $itemid)->get();
                foreach ($QuoteItemDefects as $QuoteItemDefect) {
                    $option_id = $QuoteItemDefect->option_id;
					$d_id = $QuoteItemDefect->id;
					
					$args = array(
						'type'		=> $type_id,
						'client'	=> $client_id,
						'price'		=> $price,
						'price2'	=> $price,
						'option'	=> $QuoteItemDefect->option_id
					);
					
					$resultsets = DB::select(DB::raw("SELECT p.*
						FROM `price_factors_bands` p
						INNER JOIN `price_bands` b ON b.id = p.price_band_id
						JOIN(
							SELECT t.`id` AS client_id, @pv:=t.`parent_id` AS parent, @rownum := @rownum + 1 AS rownum 
							FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
							JOIN (SELECT @pv:= :client, @rownum:=0) tmp WHERE t.`id`=@pv
						)c on c.`client_id` = p.`client_id`
						WHERE b.value_from <= :price 
						AND b.value_to >= :price2 
						AND b.`status` = 'active' 
						AND p.type_id = :type
						AND p.option_id = :option
						ORDER BY c.`rownum`, p.value DESC 
						LIMIT 0, 1"), $args);
					foreach($resultsets as $defect){
						if(isset($defect->value)) {
							$defect_price = $defect_price + $defect->value;
						}
						array_push($defects, array('defect_item_id' => $d_id, 'defect_id' => $defect->id, 'value' => $defect->value));
					}
                }
				$defect_price_net = $defect_price;
            
                // check to see if sem of defects is less then the maximum defect value based on the cap                
                $defect_cap_price = 0;

                if ($defect_cap > 0) {
                    $defect_cap_price = $price * ($defect_cap / 100.0);
                    
                    if ($defect_cap_price < $defect_price) {
                        $defect_price_net = $defect_cap_price;
                    }
                }
				//pricing flag is 2 or 3 then margin are set and should not be recaluated so just populate from quote item as is
				if($flag == 2 || $flag == 3){
					$logistics_margin = $item_data["logistics_margin"];
					$logistics_product_margin = $item_data["logistics_product_margin"];
					$services_margin = $item_data["services_margin"];
				} else {
					$logistics_margin = (isset($margins["logistics"])) ? $margins["logistics"] : 0;
					$logistics_product_margin = (isset($margins["logistic product"])) ? $margins["logistic product"] : 0;
					$services_margin = (isset($margins["service"])) ? $margins["service"] : 0;
				}

				$oem_product = (isset($quote_data["margin_oem_product"])) ? $quote_data["margin_oem_product"] : 0;
				$oem_services = (isset($quote_data["margin_oem_service"])) ? $quote_data["margin_oem_service"] : 0;
				$oem_logistics = (isset($quote_data["margin_oem_logistic"])) ? $quote_data["margin_oem_logistic"] : 0;

				if(isset($item_data["unit_margin_oem_product"])) {
				    $oem_product = $item_data["unit_margin_oem_product"];
				}
				if(isset($item_data["services_oem_margin"])){
				    $oem_services = $item_data["services_oem_margin"];
				}
				if(isset($item_data["logistics_oem_margin"])) {
				    $oem_logistics = $item_data["logistics_oem_margin"];
				}

				// calcuate services 
                $service_price = 0;
                $service_price_net = 0;    
				$countryID = $quote_data["country"];           

                if (isset($item_data["services_id"]) && $item_data["services_id"] > 0) {

                    $services_id = $item_data["services_id"];

					$args = array(
								'country'	=> $countryID,
								'client'	=> $client_id,
								'services'	=> $services_id,
								'type'		=> $type_id
							);

                    $resultsets = DB::select(DB::raw("SELECT p.* FROM `price_margin_services` p
                        JOIN(
							SELECT t.`id` AS client_id, @pv:=t.`parent_id` AS parent, @rownum := @rownum + 1 AS rownum 
							FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
							JOIN (SELECT @pv:= :client, @rownum:=0) tmp WHERE t.`id`=@pv
						)c on c.`client_id` = p.`client_id`
                        WHERE p.option_id = :services 
						AND p.`status` = 'active' 
						AND (country_id  = 0 OR country_id = :country )
						AND (type_id = 0 OR type_id = :type )
                        ORDER BY c.`rownum`, p.`type_id` DESC, p.`country_id` DESC LIMIT 0, 1"), $args);

                    if (!empty($resultsets)) {
                        $quote_data19 = json_decode(json_encode($resultsets[0]), true);
                        if (isset($quote_data19["value"]) && is_numeric($quote_data19["value"])) {
                            $service_price = $service_price_net = $quote_data19["value"];
                            if ($services_margin != 0) {
                                //$service_price_net = $service_price - ($service_price * ($services_margin / 100.0));
								$service_price_net = $service_price * (1 + ($services_margin / 100.0));
                            }
                        }
                    }
                }

                // calcuate logistic
                $logistics_price = 0;
                $logistics_price_net = 0;                
                $regionID = !empty($quote_data["region"]) ? $quote_data["region"] : '0';

                $collectionQuantity = $quote_data["ave_collect_size"];
                $resultsets = DB::select(DB::raw("SELECT p.* FROM `price_logistics` p
                    INNER JOIN `logistics_unit_numbers` un ON p.logistic_unit_id = un.id
                    JOIN(SELECT t.`id` AS client_id, @pv:=t.`parent_id` AS parent, @rownum := @rownum + 1 AS rownum 
                    FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t 
                    JOIN (SELECT @pv:=$client_id, @rownum:=0) tmp WHERE t.`id`=@pv)c on c.`client_id` = p.`client_id`
                    WHERE un.value_from <= $collectionQuantity
                    AND un.value_to >= $collectionQuantity
                    AND p.country_id = $countryID
                    AND p.region_id = $regionID ORDER BY c.`rownum`
                    LIMIT 0, 1"));

                if (!empty($resultsets)) {
                    $quote_data20 = json_decode(json_encode($resultsets[0]), true);
                    if (isset($quote_data20["value"]) && is_numeric($quote_data20["value"])) {
                        $logistics_price = $logistics_price_net = $quote_data20["value"];
						if($logistics_margin != 0 || $logistics_product_margin != 0){
							if($logistics_product_margin != 0 && $price != 0){
								$logistics_price_net = $logistics_price_net * (1 + ($logistics_product_margin / 100.0));
							}
							if($logistics_margin != 0 && $price != 0){
								$logistics_price_net = $logistics_price_net * (1 + ($logistics_margin / 100.0));
							}
						}
                    }
                }

                // this is for HP PUP stores there may also be a specified margin would would over ride and replace all oem margin values
                if (isset($item_data["isPUP"]) && $item_data["isPUP"] == 1 && isset($item_data["unit_margin_committed"])) {
                    $oem_product = $oem_services = $oem_logistics = $item_data["unit_margin_committed"];
                }
             
				// price before defect = this is compared against the committed price to determine which should be used.
				// this is for HP PUP stores
				$use_committed = false;

				$committed_price = 0;
				if (isset($item_data["isPUP"]) && $item_data["isPUP"] > 0 && isset($item_data["unit_margin_committed"])) {
					//get price
					$sql = "SELECT 
						p.price AS 'purchase_price',
						m.margin,
						(period_diff(date_format(now(), '%Y%m'), date_format(p.`date`, '%Y%m')) + 1) AS 'months',
						p.`price` * m.`margin`/ 100.0 AS 'calculated_price' 
						FROM `pup_contracts` p
						INNER JOIN `pup_contracts_months` m ON m.serial_number = p.serial_number
						WHERE 
						p.id = " . $item_data["isPUP"] . "	
						AND m.month_from <= period_diff(date_format(now(), '%Y%m'), date_format(p.`date`, '%Y%m')) + 1
						AND m.month_to >= period_diff(date_format(now(), '%Y%m'), date_format(p.`date`, '%Y%m')) + 1
						AND m.import = 0	
						AND p.import = 0
						LIMIT 1";
					$resultsets = DB::select(DB::raw($sql));	
					if(!empty($resultsets)){
						$committed_price = $resultsets[0]->calculated_price;
					}	
				
					if($committed_price > ($price - $service_price_net - $logistics_price_net)){
						$use_committed = true;
						$price = $committed_price;
						$service_price_net = $logistics_price_net = 0;
					}
				}

				$logistics_f5_price = $logistics_price_net;
                if ($oem_logistics != 0 && $logistics_price_net != 0) {
                    $logistics_price_net = $logistics_price_net * (1 + ($oem_logistics / 100.0));
                }

                $service_f5_price = $service_price_net;
                if ($oem_services != 0 && $service_price_net != 0) {
                    $service_price_net = $service_price_net * (1 + ($oem_services / 100.0));
                }

				//Product Price - Pre OEM Margin 
				$price = $product_price = $price - $defect_price_net;

				if($oem_product != 0){
					$price =  $price - ($price * ($oem_product / 100.0));
				}

                $price = $price - $service_price_net - $logistics_price_net;
               
                return array(
                    "unit_price_base" => $base_price
                    , "is_model_price" => $isModelPrice
                    , "unit_price_screen" => $screen_price
                    , "unit_price_dvd" => $dvd_price
                    , "unit_price_ram" => $ram_price
                    , "unit_price_hdd" => $hdd_price
                    , "unit_price_services" => $service_price_net
                    , "unit_price_model" => $model_price
                    , "unit_margin_model" => $model_margin
                    , "unit_price_logistics" => $logistics_price_net
                    , "unit_price_defects" => $defect_price
                    , "unit_margin_defectcap" => $defect_cap
                    , "unit_price_defectcap" => $defect_cap_price
                    , "use_committed_price" => $use_committed
                    , "unit_price_committed" => $committed_price
                    , "unit_margin_oem_product" => $oem_product
                    , "unit_margin_brand" => $margin_brand
                    , "unit_margin_customer" => $margin_customer
                    , "unit_margin_screen" => $margin_screen
                    , "unit_margin_country" => $margin_country
                    , "unit_prod_price" => $product_price
					, "unit_f5_price" =>  $product_price - ($product_price * ($oem_product / 100.0))
                    , "unit_price" => $price
                    , "total_price" => $price * $item_data["quantity"]
                    , "exchange_rate" => $quote_data["currency_rate"]
                    , "currency" => $quote_data["currency_symbol"]
                    , "quantity" => $item_data["quantity"]
                    , "country" => $quote_data["country"]
                    , "totalmargin" => $margin_brand + $margin_customer + $margin_screen + $margin_country
					, "logistics" => array(
						"logistics_base_price" => $logistics_price
						, "logistics_margin" => $logistics_margin
						, "logistics_product_margin" => $logistics_product_margin
						, "logistics_oem_margin" => $oem_logistics
						, "logistics_net_price" => $logistics_price_net
						, "unit_f5_logistics" => $logistics_f5_price
						, "logistics_price_OEM" => $logistics_price_net - $logistics_f5_price
						)
					, "services" => array(
						"services_base_price" => $service_price
						, "services_margin" => $services_margin
						, "services_oem_margin" => $oem_services
						, "services_net_price" => $service_price_net
						, "unit_f5_services" => $service_f5_price
						, "services_price_OEM" => $service_price_net - $service_f5_price
						)
					, "defects" => $defects
                    );
            }

		public function calculate_base_price($product_id, $hdd_id, $dvd_id, $ram_id) {
            /*
              returns an array value => decimal (can be either a negative or posistive value)
              hdd_required => boolean (true,  false)
              dvd_required => boolean (true,  false)
              ram_required => boolean (true,  false)
             */
              $value = 0;
              $hdd = false;
              $dvd = false;
              $ram = false;

              $resultsets = DB::select(DB::raw("SELECT * FROM `product` p WHERE p.id= $product_id AND `status` = 'active' "));
           // echo "<pre>";print_r($resultsets[0]);die('dddddd');
              if ($resultsets){
                $resultset = json_decode(json_encode($resultsets[0]), true);
            }
            if (isset($resultset["value"]) && is_numeric($resultset["value"])) {
                $value = $resultset["value"];
            }
            if (isset($hdd_id) && $hdd_id > 0) {
                if (!(isset($resultset["hdd_id"]) && $resultset["hdd_id"] > 0 && $hdd_id == $resultset["hdd_id"])) {
                    $hdd = true;
                }
            } else {
                $hdd = true;
            }
            if (isset($dvd_id) && $dvd_id > 0) {
                if (!(isset($resultset["dvd_id"]) && $resultset["dvd_id"] > 0 && $dvd_id == $resultset["dvd_id"])) {
                    $dvd = true;
                }
            } else {
                $dvd = true;
            }
            if (isset($ram_id) && $ram_id > 0) {
                if (!(isset($resultset["ram_id"]) && $resultset["ram_id"] > 0 && $ram_id == $resultset["ram_id"])) {
                    $ram = true;
                }
            } else {
                $ram = true;
            }

            return array("value" => $value, "hdd_required" => $hdd, "dvd_required" => $dvd, "ram_required" => $ram);
        }

        public function calculate_base_price_without_product($itemid, $client_id, $type_id = 0, $brand_id = 0, $processor_id = 0, $screen_id = 0, $hdd_id = 0, $dvd_id = 0, $ram_id = 0) {
            $value = 0;
            $hdd = false;
            $dvd = false;
            $ram = false;

            $resultsets = DB::select(DB::raw("SELECT * FROM `product` p
                JOIN(SELECT t.`id` AS client_id, @pv:=t.`parent_id` AS parent,@rownum := @rownum+ 1 AS rownum
                FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
                JOIN (SELECT @pv:=$client_id, @rownum:=0)tmp
                WHERE t.`id`=@pv)c on c.`client_id` = p.`client_id`
                WHERE p.type_id =$type_id
                AND (p.processor_id =$processor_id OR p.processor_id IS NULL)
                AND (p.screen_id = $screen_id OR p.screen_id IS NULL)
                AND (p.brand_id = $brand_id OR p.brand_id IS NULL OR p.brand_id = 0)
                AND (p.hdd_id = $hdd_id OR p.hdd_id IS NULL)
                AND (p.ram_id = $ram_id OR p.ram_id IS NULL)
                AND (p.dvd_id = $dvd_id OR p.dvd_id IS NULL)
                AND p.`value` > 0 
                AND p.`value_factor` = 'explicit price'
                AND p.`status` = 'active'
                ORDER BY c.`rownum`, p.ram_id DESC, p.hdd_id DESC, p.dvd_id DESC, p.screen_id DESC, p.processor_id DESC
                LIMIT 0, 1"));

            //echo "<pre>";print_r($resultsets);die('dddddd');
            if ($resultsets) {
                $resultset = json_decode(json_encode($resultsets[0]), true);
                $quoteitm = QuoteItem::find($itemid);
                $quoteitm->product_id = @$resultset["id"];
                $quoteitm->model_id = ($resultset["model"] > 0) ? $resultset["model"] : 0;
                $quoteitm->save();
            } else {
                $resultset = 0;
                //return 'error this product is not ready to be send';
            }
            if (isset($resultset["value"]) && is_numeric($resultset["value"])) {
                $value = $resultset["value"];
            }
            if (isset($hdd_id) && $hdd_id > 0) {
                if (!(isset($resultset["hdd_id"]) && $resultset["hdd_id"] > 0 && $hdd_id == $resultset["hdd_id"])) {
                    $hdd = true;
                }
            } else {
                $hdd = true;
            }
            if (isset($dvd_id) && $dvd_id > 0) {
                if (!(isset($resultset["dvd_id"]) && $resultset["dvd_id"] > 0 && $dvd_id == $resultset["dvd_id"])) {
                    $dvd = true;
                }
            } else {
                $dvd = true;
            }
            if (isset($ram_id) && $ram_id > 0) {
                if (!(isset($resultset["ram_id"]) && $resultset["ram_id"] > 0 && $ram_id == $resultset["ram_id"])) {
                    $ram = true;
                }
            } else {
                $ram = true;
            }
            return array("value" => $value, "hdd_required" => $hdd, "dvd_required" => $dvd, "ram_required" => $ram);
        }

        public function calculate_hw_adj($client_id, $id, $hardwaretype, $price, $type_id, $processor_id = 0, $brand_id = 0) {
            /* returns a decimal value 
              calcuate adjustment factor for hdd, ram or dvd
              $hardwaretype => string ("aio", "hdd", "ram" or "dvd")
             */
              $value = 0;
              $spec_value = 0;
              $actual_value = 0;
              $variance_value = 0;

              if ($hardwaretype === "aio") {
                $resultsets = DB::select(DB::raw("SELECT * FROM `product` p
                    JOIN(
						SELECT t.`id` AS client_id, @pv:=t.`parent_id` AS parent, @rownum := @rownum + 1 AS rownum 
						FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
						JOIN (SELECT @pv:=$client_id, @rownum:=0) tmp 
						WHERE t.`id`=@pv
					) c on c.`client_id` = p.`client_id`
                    WHERE p.type_id IN (SELECT id FROM `product_options` WHERE type = 'Product' AND name = 'LCD')
                    AND (p.screen_id = $id)
                    AND (p.brand_id = $brand_id OR p.brand_id IS NULL OR p.brand_id = 0)
                    AND (p.processor_id IS NULL)
                    AND (p.hdd_id IS NULL)
                    AND (p.ram_id IS NULL)
                    AND (p.dvd_id IS NULL)
                    AND p.`value` > 0 
                    AND p.`value_factor` = 'explicit price'
                    AND p.`status` = 'active'
                    ORDER BY c.`rownum`
                    LIMIT 0, 1"));
                if (!empty($resultsets)) {
                    $resultset = json_decode(json_encode($resultsets[0]), true);
                    if (isset($resultset["value"]) && is_numeric($resultset["value"])) {
                        $value = $resultset["value"];
                    }
                }
            } elseif ($hardwaretype === "dvd") {

                $resultsets27 = DB::select(DB::raw("SELECT p.* FROM  `price_factors_bands` p
                    JOIN `price_bands` b ON p.price_band_id = b.id 
                    JOIN(SELECT t.`id` AS client_id, @pv:=t.`parent_id` AS parent, @rownum := @rownum + 1 AS rownum
                    FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
                    JOIN (SELECT @pv:=$client_id, @rownum:=0)tmp WHERE t.`id`=@pv)c on c.`client_id` = p.`client_id`
                    WHERE p.factor_name = 'DVD'
                    AND p.option_id = $id
                    AND b.`value_from` <= $price
                    AND b.`value_to` >= $price
                    AND b.`status` = 'active'
                    "));

                if (!empty($resultsets27)) {
                    $resultset27 = json_decode(json_encode($resultsets27[0]), true);
                    //$resultset; /* = get resultset from QUERY SAMPLE 11 */
                    if (isset($resultset27["value"]) && is_numeric($resultset27["value"])) {
                        $value = $resultset27["value"];
                    }
                }
            } elseif ($hardwaretype === "ram" || $hardwaretype === "hdd") {

				$args = Array(
					'type'		=> $type_id,
					'processor'	=> $processor_id,
					'hwtype'	=> $hardwaretype
				);

                $resultsets = DB::select(DB::raw("SELECT p.`value` FROM `product_options` p
                    JOIN `product_factors` f ON p.id = f.option_id
                    WHERE f.type_id = :type
                    AND f.processor_id= :processor
                    AND f.factor_type = :hwtype"), $args);

                if (!empty($resultsets)) {
                    $resultset34 = json_decode(json_encode($resultsets[0]), true);
                    if (isset($resultset34["value"]) && is_numeric($resultset34["value"])) {
                        $spec_value = $resultset34["value"];
                    }
                }

				$args = Array(
					'id'		=> $id,
					'hwtype'	=> $hardwaretype
				);
                $resultsets = DB::select(DB::raw("SELECT `value` FROM `product_options` WHERE type = :hwtype AND id = :id"), $args);

                if (!empty($resultsets)) {
                    $resultset13 = json_decode(json_encode($resultsets[0]), true);
                    if (isset($resultset13["value"]) && is_numeric($resultset13["value"])) {
                        $actual_value = $resultset13["value"];
                    }
                }
                if ($spec_value > 0 && $actual_value - $spec_value != 0) {

                    $variance = $actual_value - $spec_value;

					$args = Array(
						'type'		=> $type_id,
						'client'	=> $client_id,
						'hwtype'	=> $hardwaretype,
						'vfrom'		=> $variance,
						'vto'		=> $variance
					);
					
                    $resultsets = DB::select(DB::raw("SELECT * FROM `price_factors` p
                        JOIN(SELECT t.`id` AS client_id, @pv:=t.`parent_id` AS parent, @rownum := @rownum + 1 AS rownum
                        FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
                        JOIN (SELECT @pv:= :client, @rownum:=0)tmp 
                        WHERE t.`id`=@pv) c on c.`client_id` = p.`client_id`
                        WHERE p.variance_from >= :vfrom
                        AND p.variance_to <= :vto
                        AND p.factor_name = :hwtype
						AND (p.type_id = 0 OR p.type_id IS NULL OR p.type_id = :type)
                        ORDER BY c.`rownum`, p.type_id DESC
                        LIMIT 0, 1"), $args);
						
                    if (!empty($resultsets)) {
                        $resultset14 = json_decode(json_encode($resultsets[0]), true);
                  
                        if (isset($resultset14["value"]) && is_numeric($resultset14["value"])) {
                            $value = $resultset14["value"];
                        }
                    }
                }
            }
            return $value;
        }

	public function calculate_margin_adj($client_id, $country_id, $brand_id, $type_id, $screen_id) {
        /* returns an array of margin adjustments to be made */
        $returnresult = array();
        $totalmargin = 0;
        $screen_id = !empty($screen_id) ? $screen_id : 0;

		/* margins for  logistics product; logistics ans services */
		$args = array('logistic product', 'logistics', 'service', 'defectcap');
		foreach ($args as $arg) {
			$resultsets = DB::select(DB::raw("SELECT * 
					FROM `price_margin_adjustments` p
					JOIN(
						SELECT t.`id` AS client_id, @pv:=t.`parent_id` AS parent, @rownum := @rownum + 1 AS rownum
						FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
						JOIN (SELECT @pv:= $client_id, @rownum:=0)tmp
						WHERE t.`id`=@pv
					)c on c.`client_id` = p.`client_id`
					WHERE p.margin_type = '". $arg . "'
					AND (p.type_id = $type_id OR p.type_id IS NULL)
					ANd p.`status` = 'active'
					ORDER BY c.`rownum`, p.`type_id` DESC, p.`value` LIMIT 1"));

			if (!empty($resultsets)) {
				$resultset4 = json_decode(json_encode($resultsets), true);
				foreach ($resultset4 as $key => $data) {					
					$returnresult[$arg] = $data['value'];
				}
			}
		}

		/* margin details for country */
		$resultsets = DB::select(DB::raw("SELECT * 
				FROM `price_margin_adjustments` p
				JOIN(
					SELECT t.`id` AS client_id, @pv:=t.`parent_id` AS parent, @rownum := @rownum + 1 AS rownum
					FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
					JOIN (SELECT @pv:= $client_id, @rownum:=0)tmp
					WHERE t.`id`=@pv
				)c on c.`client_id` = p.`client_id`
				WHERE p.margin_type = 'country'
				AND (p.country_id = $country_id OR p.country_id IS NULL)
				ANd p.`status` = 'active'
				ORDER BY c.`rownum`, p.`country_id` DESC, p.`value` LIMIT 1"));

		if (!empty($resultsets)) {
            $resultset4 = json_decode(json_encode($resultsets), true);
            foreach ($resultset4 as $key => $data) {				
				$returnresult['country'] = $data['value'];
				$totalmargin += $data['value'];
			}
		}

		/* margin detail for client */
		$resultsets = DB::select(DB::raw("SELECT * 
				FROM `price_margin_adjustments` p
				JOIN(
					SELECT t.`id` AS client_id, @pv:=t.`parent_id` AS parent, @rownum := @rownum + 1 AS rownum
					FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
					JOIN (SELECT @pv:= $client_id, @rownum:=0)tmp
					WHERE t.`id`=@pv
				)c on c.`client_id` = p.`client_id`
				WHERE p.margin_type = 'client'
				AND (p.client_id = $client_id OR p.client_id IS NULL)
				ANd p.`status` = 'active'
				ORDER BY c.`rownum`, p.`value` LIMIT 1"));

		if (!empty($resultsets)) {
            $resultset4 = json_decode(json_encode($resultsets), true);
            foreach ($resultset4 as $key => $data) {				
				$returnresult['customer'] = $data['value'];
				$totalmargin += $data['value'];
			}
		}

		/* margin detail for brand */
		$resultsets = DB::select(DB::raw("SELECT * 
				FROM `price_margin_adjustments` p
				JOIN(
					SELECT t.`id` AS client_id, @pv:=t.`parent_id` AS parent, @rownum := @rownum + 1 AS rownum
					FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
					JOIN (SELECT @pv:= $client_id, @rownum:=0)tmp
					WHERE t.`id`=@pv
				)c on c.`client_id` = p.`client_id`
				WHERE p.margin_type = 'brand'
				AND (p.brand_id = $brand_id OR p.brand_id IS NULL)
				AND (p.type_id = $type_id OR p.type_id IS NULL)
				ANd p.`status` = 'active'
				ORDER BY c.`rownum`, p.brand_id DESC, p.type_id DESC, p.`value` LIMIT 1"));

		if (!empty($resultsets)) {
            $resultset4 = json_decode(json_encode($resultsets), true);
            foreach ($resultset4 as $key => $data) {			
				$returnresult['brand'] = $data['value'];
				$totalmargin += $data['value'];
			}
		}

		/* margin detail for screen */
		$resultsets = DB::select(DB::raw("SELECT * 
				FROM `price_margin_adjustments` p
				JOIN(
					SELECT t.`id` AS client_id, @pv:=t.`parent_id` AS parent, @rownum := @rownum + 1 AS rownum
					FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
					JOIN (SELECT @pv:= $client_id, @rownum:=0)tmp
					WHERE t.`id`=@pv
				)c on c.`client_id` = p.`client_id`
				WHERE p.margin_type = 'screen'
				AND (p.screen_id = $screen_id AND p.type_id = $type_id) 
				AND (p.type_id = $type_id OR p.type_id IS NULL)
				ANd p.`status` = 'active'
				ORDER BY c.`rownum`, p.screen_id DESC, p.type_id DESC, p.`value` LIMIT 1"));

		if (!empty($resultsets)) {
            $resultset4 = json_decode(json_encode($resultsets), true);
            foreach ($resultset4 as $key => $data) {					
				$returnresult['screen'] = $data['value'];
				$totalmargin += $data['value'];
			}
		}

        $returnresult["totalmargin"] = $totalmargin;
        return $returnresult;		
    }

    public function productId($client_id, $type_id = 0, $brand_id = 0, $processor_id = 0, $screen_id = 0, $hdd_id = 0, $dvd_id = 0, $ram_id = 0, $pricetpe = 1, $model_id = 0, $country_id = 0) {

		if(empty($client_id) || !is_numeric($client_id) || $client_id < 0) { $client_id = 0; }
				
		$countryids = array(0);
		//get list of countries for the client
		if($country_id == 0){
			$countries = $this->getCountriesforClient($client_id);
			//loop though and get array of ids
			//preceed the array with the default country id of zero
			foreach($countries as $country){
				$countryids[] = $country->id;
			}
		} else {
			$countryids[] = $country_id;
		}

		$sqlbit = '';
		if($pricetpe == 1){
			$sqlbit = " AND p.`value` IS NOT NULL AND p.`value_factor` = 'explicit price'";
		} else {
			$sqlbit = " AND p.`value` IS NOT NULL AND (p.`value_factor` = 'margin price' OR p.`value_factor` = 'margin percent') ";
		}

		// if model is specified check there is pricing and that
		// it's the right one for the client
		if($model_id > 0){
			$sql = "SELECT * FROM `product` p  WHERE p.`status` = 'active' AND p.id = " . $model_id . $sqlbit;
			$resultsets = DB::select(DB::raw($sql));        
			if ($resultsets) {
				return $resultsets;
			}
		}

		$sql = "SELECT * FROM `product` p
            JOIN(SELECT t.`id` AS client_id, @pv:=t.`parent_id` AS parent,@rownum := @rownum+ 1 AS rownum
            FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
            JOIN (SELECT @pv:=$client_id, @rownum:=0)tmp
            WHERE t.`id`=@pv)c on c.`client_id` = p.`client_id`
            WHERE p.type_id =$type_id
            AND (p.processor_id = $processor_id OR p.processor_id IS NULL OR p.processor_id = 0)
            AND (p.screen_id = $screen_id OR p.screen_id IS NULL OR p.screen_id = 0)
            AND (p.brand_id = $brand_id OR p.brand_id IS NULL OR p.brand_id = 0)
            AND (p.hdd_id = $hdd_id OR p.hdd_id IS NULL OR p.hdd_id = 0)
            AND (p.ram_id = $ram_id OR p.ram_id IS NULL OR p.ram_id = 0)
            AND (p.dvd_id = $dvd_id OR p.dvd_id IS NULL OR p.dvd_id = 0)
			AND p.country_id IN (". implode(',', $countryids) .")
            ". $sqlbit . "
            AND p.`status` = 'active'
            ORDER BY c.`rownum`, p.country_id DESC, p.ram_id DESC, p.hdd_id DESC, p.dvd_id DESC, p.screen_id DESC, p.processor_id DESC, p.model DESC, p.`value` DESC
            LIMIT 0, 1";

        $resultsets = DB::select(DB::raw($sql));
        
        if ($resultsets) {
            return $resultsets;
        } else {
            return false;
        }
    }

	public function getQuoteSteps($productId = null, $brandId = null, $client_id = null) {

		$step = array();

	    if(null === $client_id || !is_numeric($client_id) || $client_id < 0){
			//client not valid
			$client_id = 0;
		}     

		if(null === $productId || !is_numeric($productId) || $productId < 0){
			//product id is not valid
			return $step;
		}  

		if(null !== $brandId && (!is_numeric($brandId) || $brandId < 0)){
			// brand id is not valid
			$brandId = null;
		} 

        $step = DB::select(DB::raw('SELECT tmp.`id`, qsi.`order`, group_concat(qsi.`group_code`) as group_code
                FROM `quote_step_items` qsi
                JOIN (
                SELECT qs.id 
				FROM `quote_step` qs
                JOIN(
                SELECT t.`id` AS client_id
                , @pv:=t.`parent_id` AS parent
                , @rownum := @rownum + 1 AS rownum
                FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
                JOIN (SELECT @pv:= '.$client_id.', @rownum:=0)tmp
                WHERE t.`id`=@pv
                )c on c.`client_id` = qs.`client_id`
                WHERE qs.type_id =' . $productId . '
                AND (qs.brand_id = ' . $brandId . ' OR qs.brand_id IS NULL)
                ORDER BY qs.brand_id DESC, c.`rownum` ASC
                LIMIT 1
                ) tmp ON qsi.quote_step_id = tmp.id
                group by qsi.`order`
                ORDER BY qsi.`order`'));

		return $step;
	}

	public function Recalculate($quote_id = null, $quoteitem_id = null){
		//check quote exists
        
		$Quote = Quote::find($quote_id);

		if(null === $Quote){
			return FALSE;
            
		}
       
		//enterprise client status must be  
		//store client status must be 2

		$Client = Client::find($Quote->client_id);
		if(null === $Client || (($Quote->status != 1 && $Client->client_type = 'enterprise') && ($Quote->status != 0 && $Client->client_type = 'store')))

        {
			return FALSE;
            


		}


		if($quoteitem_id != null){
			$QuoteItems = QuoteItem::where('id', '=', $quoteitem_id)->get();
		}else{
			$QuoteItems = QuoteItem::where('quote_id', '=', $quote_id)->get();
		}
                  
        if (count($QuoteItems) > 0) {

            foreach ($QuoteItems as $QuoteItem) {
				/* TODO check if update are locked or whether values can be recalc due to override values */

                $results = $this->calculate($QuoteItem->quote_id, $QuoteItem->id);
                  
				if(null !== $results){
					$QuoteItem = QuoteItem::find($QuoteItem->id);

					$QuoteItem->unit_price_base				= $results['unit_price_base'];
					$QuoteItem->unit_price_screen			= $results['unit_price_screen'];
					$QuoteItem->unit_price_dvd				= $results['unit_price_dvd'];
					$QuoteItem->unit_price_ram				= $results['unit_price_ram'];
					$QuoteItem->unit_price_hdd				= $results['unit_price_hdd'];  
              
					$QuoteItem->unit_price_model			= $results['unit_price_model'];      
					$QuoteItem->unit_margin_model			= $results['unit_margin_model'];  
        
					$QuoteItem->unit_price_defects			= $results['unit_price_defects'];
					$QuoteItem->unit_margin_defectcap		= $results['unit_margin_defectcap'];
					$QuoteItem->unit_price_committed		= $results['unit_price_committed'];
					$QuoteItem->use_committed_price			= ($results['use_committed_price']) ? 1 : 0;
					$QuoteItem->unit_margin_oem_product		= $results['unit_margin_oem_product'];
					$QuoteItem->unit_margin_oem_services	= 0;
					$QuoteItem->unit_margin_brand			= $results['unit_margin_brand'];
					$QuoteItem->unit_margin_customer		= $results['unit_margin_customer'];
					$QuoteItem->unit_margin_screen			= $results['unit_margin_screen'];
					$QuoteItem->unit_margin_country			= $results['unit_margin_country'];
                
					$QuoteItem->unit_price					= $results['unit_price'];
					$QuoteItem->total_price					= $results['total_price'];
					$QuoteItem->is_model_price				= $results['is_model_price'];
					$QuoteItem->unit_price_defectcap		= $results['unit_price_defectcap'];
					$QuoteItem->unit_prod_price				= $results['unit_prod_price'];
					$QuoteItem->unit_f5_price				= $results['unit_f5_price'];

					/* logistic values */
					$QuoteItem->unit_price_logistics		= $results['unit_price_logistics'] ;
					$QuoteItem->logistics_base_price		= $results['logistics']['logistics_base_price'] ;
					$QuoteItem->logistics_margin			= $results['logistics']['logistics_margin'];
					$QuoteItem->logistics_product_margin	= $results['logistics']['logistics_product_margin'];
					//$QuoteItem->logistics_oem_margin		= $results['logistics']['logistics_oem_margin'];
					$QuoteItem->logistics_net_price			= $results['logistics']['logistics_net_price'];
					$QuoteItem->unit_f5_logistics			= $results['logistics']['unit_f5_logistics'];

					/* services values */
					$QuoteItem->unit_price_services			= $results['unit_price_services'];
					$QuoteItem->services_base_price			= $results['services']['services_base_price'];
					$QuoteItem->services_margin				= $results['services']['services_margin'];
					//$QuoteItem->services_oem_margin			= $results['services']['services_oem_margin'];
					$QuoteItem->services_net_price			= $results['services']['services_net_price'];
					$QuoteItem->unit_f5_services			= $results['services']['unit_f5_services'];

					$QuoteItem->save();

					//update al defect pricing to zero iniitally
					QuoteItemsDefect::where('quote_item_id', '=', $QuoteItem->id)->update(['defect_value' => 0, 'defect_id' => 0] );
					//update pricing for defect from calculation
					if(count($results["defects"]) > 0){
						foreach ($results["defects"] as $defect) {
							QuoteItemsDefect::where('id', '=', $defect["defect_item_id"])->update(['defect_value' => $defect["value"], 'defect_id' => $defect["defect_id"]] );
						}
					}
				}
            }

            $total_price = array();
			$total_qty = array();
            $TotalItems = QuoteItem::where('quote_id', '=', $QuoteItem->quote_id)->get();
            foreach ($TotalItems as $item) {
                $total_price[]	= $item->total_price;
				$total_qty[]	= $item->quantity;
            }

            $quoteTable = Quote::find($QuoteItem->quote_id);
            $quoteTable->total_value = array_sum($total_price);
            $quoteTable->total_items = array_sum($total_qty);;
            $quoteTable->save();
        }
    }

	public function getCountriesforClient($client_id){
		$Countries = Country::where('status', '=', 'active')->orderBy('name')->get();


		$results = array();
		$clienttree = array();
		$clientcontroller = new ClientsController();
		$clienttree = $clientcontroller->getParent($clienttree, 0, $client_id);
		$hasCountries = false;
		
		foreach($clienttree as $client){
		    
		    if($hasCountries) { continue; }
		    
    		$args = array(
    			'client'	=> $client["id"]
    		);	
    		
    		$resultset = DB::select(DB::raw("SELECT cr.*
    			FROM `country` cr
    			INNER JOIN `client_country_rates` ccr ON cr.id = ccr.country_id
    			WHERE ccr.`client_id` = :client
    			ORDER BY cr.`name`"), $args);
    
    		$previd = 0;
    		if(!empty($resultset)){
    			foreach($resultset as $ctry){
    				if($ctry->id != $previd){
    					if($ctry->status == 'active'){
    					    $hasCountries = true;
    						array_push($results, $ctry);
    					}
    				}
    				$previd = $ctry->id;
    			}
    	
    			$Countries = $results;					
    			usort($Countries, function ($a, $b) {
    				return strcmp($a->name, $b->name);
    			});
    		}
    		
		}
		return $Countries;
	}

	public function getClientData($clientid, $data){
		if(!isset($clientid) || !is_numeric($clientid) || $clientid < 0){ return 0; }

		$infoitem = "id";
		switch ($data){
			case 'country': $infoitem = 'country'; break;
			case 'region': $infoitem = 'region'; break;
			case 'ave_collect_size': $infoitem = 'ave_collect_size'; break;
			case 'margin_oem_product': $infoitem = 'margin_oem_product'; break;
			case 'margin_oem_logistic': $infoitem = 'margin_oem_logistic'; break;
			case 'margin_oem_service': $infoitem = 'margin_oem_service'; break;
		}

		$args = array(
			'client'	=> $clientid,
		);

		$sql = "SELECT $infoitem AS `data`
			FROM `clients` ccr
			JOIN(
				SELECT t.`id` AS client_id, @pv:=t.`parent_id` AS parent, @rownum := @rownum + 1 AS rownum
				FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
				JOIN (SELECT @pv:= :client, @rownum:=0)tmp
				WHERE t.`id`=@pv
			)c on c.`client_id` = ccr.`id`
			WHERE $infoitem IS NOT NULL
			ORDER BY c.`rownum`
			LIMIT 1";
			
		$results = DB::select(DB::raw($sql), $args);
		if(!empty($results)){
			return $results[0]->data;
		}

		return 0;
	}

	public function RecalculateInProgressQuotes(){
	    $Quotes = Quote::where('status', '=', 1)->get();
        if (count($Quotes) > 0) {
            foreach ($Quotes as $Quote) {
				$this->Recalculate($Quote->id);
			}
        }
	}
}
