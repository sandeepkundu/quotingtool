<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Session;
use DateTime;
use Toast;
use links;
use App\Client;
use App\User;
use App\ProductOption;
use App\Product;
use App\ProductOptionGroup;
use URL;
use App\ProductClient;
use App\ProductFactor;
use App\PriceFactors;
use App\PriceFactorBands;
use App\Http\Controllers\CalculationsController;
use App\Quote;
use App\QuoteStatus;
use App\QuoteItem;
use App\QuoteItemsDefect;
use App\QuotesNotes;
use App\ProductOptionLink;

class ProductOptionsController extends Controller {

    private $permission = [1];

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        if ($this->filter($this->permission)) {

            $productOption = ProductOption::select(DB::raw('count(*) as type_count, type'))->where('isActive', '=', 1)->groupBy('type')->orderBy('type', 'ASC')->get();

            return view('productOptions/index', array('productOption' => $productOption));
        } else {
             Toast::error("Authorization  failed:You  don't have .", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addProductOption($type) {
        if ($this->filter($this->permission)) {
            $names = ProductOption::all();
            return view('productOptions/addProductOption', compact('names', 'type'));
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function insertProductOption(Request $request) {
        if ($this->filter($this->permission)) {
            Input::flash();
            $this->validate($request, array(
                'name' => 'required',
                'value' => 'numeric',
                // 'description' => 'required',
                //'image' => 'required|max:1000',
                'image' => 'nullable|mimes:jpeg,png|max:100'
            ));
            if ($request->image != null) {
                $file = array('image' => $request->image);
                $destinationPath = public_path() . '/product/';
                $extension = Input::file('image')->getClientOriginalExtension();
                $now = time();
                $fileName = $now . '.' . $extension;
                Input::file('image')->move($destinationPath, $fileName);
            } else {
                $fileName = '';
            }
            $productsOptionStatus = $request->status;
            if ($productsOptionStatus == 'on') {
                $status = 'active';
            } else {
                $status = 'Inactive';
            }
            $desc = strip_tags($request->input('description'));
            $description = $request->input('description');

            if (!empty($desc)) {
                $dom = new \DomDocument();
                $dom->loadHtml($description, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                $images = $dom->getElementsByTagName('img');
                foreach ($images as $k => $img) {
                    $data = $img->getAttribute('src');
                    list($type, $data) = explode(';', $data);
                    list(, $data) = explode(',', $data);
                    $data = base64_decode($data);
                    $image_name = "/product/" . time() . $k . '.png';
                    $path = public_path() . $image_name;
                    file_put_contents($path, $data);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', URL::to('/') . $image_name);
                }
                $description = $dom->saveHTML();
            } else {
                $description = '';
            }


            $productOption = new ProductOption();
            $productOption->fill($request->all());
            $productOption->description = $description;
            $productOption->image = $fileName;
            $productOption->status = $status;
            $productOption->isActive = 1;
            

            if ($productOption->save()) {
			/*
                if($productOption->type == 'Product' || $productOption->type == 'Brand'){
                    if ($request->input('select') != null) {
                        foreach ($request->input('select') as $id) {
                            if ($id != null) {
                                $ProductClient = new ProductClient();
                                $ProductClient->option_id = $productOption->id;
                                $ProductClient->client_id = $id;
                                $ProductClient->save();
                            }
                        }
                    } else {
                        $ProductClient = new ProductClient();
                        $ProductClient->option_id = $productOption->id;
                        $ProductClient->client_id = 0;
                        $ProductClient->save();
                    }
                }
			*/
            
                Toast::success('Record has been saved.', 'Success');
                return redirect('/productOptions/detailProductOptions/' . $productOption->type);
            } else {
                Toast::error('Id not found', 'Error');
                return redirect('/productOptions/detailProductOptions');
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detailProductOptions(Request $request, $type = null) {
        if ($this->filter($this->permission)) {
            Input::flash();
            if ($type != null) {
                $productOptions = ProductOption::where('type', '=', $type)->where('isActive', '=', 1)->get();
                return view('productOptions.detailProductOptions', compact('productOptions', 'type'));
            }
        } else {
             Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editProductOptions($id = null) {
        if ($this->filter($this->permission)) {
            if ($id != null && is_numeric($id)) {
                $names = ProductOption::all();
                $productOptions = ProductOption::find($id);

				$brands = ProductOption::where('type', '=', 'brand')->orderBy('name', 'asc')->get();
				$processor_brands = array();

				if($productOptions->type == 'Product'){
					$processor_brands = DB::select(DB::raw("SELECT o.* 
						FROM `product_options_link` k
						INNER JOIN `product_options` o ON k.brand_id = o.id
						WHERE k.type_id = " . $id));
				}

                if (!empty($productOptions)) {
                    return view('/productOptions.editProductOptions', compact('productOptions', 'names', 'brands', 'processor_brands'));
                } else {
                    Toast::error('Id not found', 'Error');
                    return Redirect('productOptions/index');
                }
            } else {
                Toast::error('Id not found', 'Error');
                return Redirect('productOptions/index');
            }
        } else {
             Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateProductOption(Request $request) {   //dd($request->all());
        if ($this->filter($this->permission)) {
            Input::flash();
            $this->validate($request, array(
                'name' => 'required',
                'value' => 'numeric',
                'ids' => 'required|numeric',
                //'description' => 'required'
                'image' => 'nullable|mimes:jpeg,png|max:100'
            ));

            $id = $request->ids;
            $productOption = ProductOption::find($id);

			$name = $productOption->name;

            $productOption->fill($request->all());
            $desc = strip_tags($request->input('description'));
            $description = $request->input('description');
            if (!empty($desc)) {
                $description = $request->input('description');
                $dom = new \DomDocument();
                $dom->loadHtml($description, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                $images = $dom->getElementsByTagName('img');
                foreach ($images as $k => $img) {
                    $data = $img->getAttribute('src');
                    if (strpos($data, 'data:image') !== false) {
                        list($type, $data) = explode(';', $data);
                        list(, $data) = explode(',', $data);
                        $data = base64_decode($data);
                        $image_name = "/product/" . time() . $k . '.png';
                        $path = public_path() . $image_name;
                        file_put_contents($path, $data);
                        $img->removeAttribute('src');
                        $img->setAttribute('src', URL::to('/') . $image_name);
                    }
                }
                $description = $dom->saveHTML();
            } else {
                $description = '';
            }

            if (isset($request->image) && !empty($request->image)) {
                $file = array('image' => $request->image);
                $destinationPath = public_path() . '/product/';
                $extension = Input::file('image')->getClientOriginalExtension();
                $now = time();
                $fileName = $now . '.' . $extension;
                Input::file('image')->move($destinationPath, $fileName);
                $productOption->image = $fileName;
            } else {
                unset($productOption->image);
            }
            $productsOptionStatus = $request->status;
            if ($productsOptionStatus == 'on') {
                $status = 'active';
            } else {
                $status = 'Inactive';
            }
            $productOption->status = $status;
            $productOption->isActive = 1;
            $productOption->description = $description;


            if ($productOption->save()) {

				if($productOption->type == 'Product'){
					$brands = $request->proc_brand;     
					ProductOptionLink::where('type_id', $id)->delete();          
      
					if(isset($brands)  && count($brands) > 0){
						foreach ($brands as $key => $brand) {  
							$plink = new ProductOptionLink();
							$plink->brand_id = $brand;
							$plink->type_id = $productOption->id;
							$plink->client_id = 0;
							$plink->order = 0;
							$plink->save();
						}
					}
				}
				
				/* name has changed therefore update agsint the product option group table */
				if($name !== $productOption->name){

					$groups = ProductOptionGroup::select('*')->where('option_id', '=', $id)->get();
					if(count($groups) >0 ){
						foreach($groups as $item){
							$item->name = $productOption->name;
							$item->save();
						}
					}
				}

                $id = $productOption->id;

                Toast::success('Record has been saved.', 'Success');
                return redirect('/productOptions/detailProductOptions/' . $productOption->type);
            } else {
                Toast::error('Id not found', 'Error');
                return redirect('/productOptions/detailProductOptions');
            }
        } else {
             Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteProductOption($id) {
        if ($this->filter($this->permission)) {
			if($this->deleteSingleItem($id)){
                Toast::success('Record has been deleted.', 'Success');
                return redirect('/productOptions/index');
            } else {
                Toast::error('Id not found', 'Error');
                return redirect('/productOptions/index');
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

	public function deleteSingleItem($id){
		$ProductOption = ProductOption::find($id);

		if(null === $ProductOption){
			return false;
		}

		$type = $ProductOption->type;
		$name = $ProductOption->name;
        $ProductOption->isActive = 0;

        if (!$ProductOption->save()) {
			return false;
		}

		/* remove the product optiont from each product option group binding */
		ProductOptionGroup::where('option_id',$id)->delete();

		if($type == 'Brand'){
			/* delete products */
			Product::where('brand_id',$id)->delete();
			/* delete brand link values */
			DB::select(DB::raw("DELETE FROM `product_options_link` WHERE brand_id = :brandid"), array('brandid' => $id));
			/* delete steps with this brand */
			DB::select(DB::raw("DELETE FROM `quote_step_items` WHERE quote_step_id IN (SELECT id FROM `quote_step` WHERE brand_id = :brandid)"), array('brandid' => $id));
			DB::select(DB::raw("DELETE FROM `quote_step` WHERE brand_id = :brandid"), array('brandid' => $id));						
		}else if($type == 'Product'){
			/* delete products */
			Product::where('type_id',$id)->delete();
			/* delete brand link values */
			$resultset = DB::select(DB::raw("DELETE FROM `product_options_link` WHERE type_id = :typeid"), array('typeid' => $id));
			/* delete steps with this brand */
			DB::select(DB::raw("DELETE FROM `quote_step_items` WHERE quote_step_id IN (SELECT id FROM `quote_step` WHERE type_id = :typeid)"), array('typeid' => $id));
			DB::select(DB::raw("DELETE FROM `quote_step` WHERE type_id = :typeid"), array('typeid' => $id));
			PriceFactors::where('type_id',$id)->delete();
			PriceFactorBands::where('type_id',$id)->delete();
		}else if($type == 'Processor'){
			/* delete products */
			Product::where('processor_id',$id)->delete();
			PriceFactors::where('processor_id',$id)->delete();
		}else if($type == 'HDD'){
			/* delete products */
			Product::where('hdd_id',$id)->delete();
		}else if($type == 'RAM'){
			/* delete products */
			Product::where('ram_id',$id)->delete();
		}else if($type == 'DVD'){
			/* delete products */
			Product::where('dvd_id',$id)->delete();
		}

		PriceFactorBands::where('option_id',$id)->delete();
		ProductFactor::where('option_id',$id)->orWhere('type_id',$id)->orWhere('processor_id',$id)->delete();
				
		$Quotes = Quote::where('status', '=', '1')->get();
		if(count($Quotes) >0 ){
			foreach($Quotes as $Quote){
				$delItem = FALSE;
				if($type == 'Brand'){
					$QuoteItems = QuoteItem::where('quote_id', '=', $Quote->id)->where('brand_id', $id)->get();
					if(count($QuoteItems) > 0) {
						QuoteItem::where('quote_id', '=', $Quote->id)->where('brand_id', $id)->delete();
						$delItem = TRUE;
					}
				}else if($type == 'Product'){
					$QuoteItems = QuoteItem::where('quote_id', '=', $Quote->id)->where('type_id', $id)->get();
					if(count($QuoteItems) > 0) {
						QuoteItem::where('quote_id', '=', $Quote->id)->where('type_id', $id)->delete();
						$delItem = TRUE;
					}
				}else if($type == 'Processor'){
					$QuoteItems = QuoteItem::where('quote_id', '=', $Quote->id)->where('processor_id', $id)->get();
					if(count($QuoteItems) > 0) {
						QuoteItem::where('quote_id', '=', $Quote->id)->where('processor_id', $id)->delete();
						$delItem = TRUE;
					}
				}else if($type == 'Screen Size'){
					$QuoteItems = QuoteItem::where('quote_id', '=', $Quote->id)->where('screen_id', $id)->get();
					if(count($QuoteItems) > 0) {
						QuoteItem::where('quote_id', '=', $Quote->id)->where('screen_id', $id)->delete();
						$delItem = TRUE;
					}
				}else if($type == 'RAM'){
					$QuoteItems = QuoteItem::where('quote_id', '=', $Quote->id)->where('ram_id', $id)->get();
					if(count($QuoteItems) > 0) {
						QuoteItem::where('quote_id', '=', $Quote->id)->where('ram_id', $id)->delete();
						$delItem = TRUE;
					}
				}else if($type == 'DVD'){
					$QuoteItems = QuoteItem::where('quote_id', '=', $Quote->id)->where('dvd_id', $id)->get();
					if(count($QuoteItems) > 0) {
						QuoteItem::where('quote_id', '=', $Quote->id)->where('dvd_id', $id)->delete();
						$delItem = TRUE;
					}
				}else if($type == 'HDD'){
					$QuoteItems = QuoteItem::where('quote_id', '=', $Quote->id)->where('hdd_id', $id)->get();
					if(count($QuoteItems) > 0) {
						QuoteItem::where('quote_id', '=', $Quote->id)->where('hdd_id', $id)->delete();
						$delItem = TRUE;
					}
				}else if($type == 'Services'){
					$QuoteItems = QuoteItem::where('quote_id', '=', $Quote->id)->where('services_id', $id)->get();
					if(count($QuoteItems) > 0) {
						QuoteItem::where('quote_id', '=', $Quote->id)->where('services_id', $id)->delete();
						$delItem = TRUE;
					}						
				}else if($type == 'Defects'){
					$defectItems = QuoteItemsDefect::where('quote_id',$Quote->id)->where('defect_id', $id)->get();	
					if(count($defectItems) > 0) {
						QuoteItemsDefect::where('quote_id',$Quote->id)->where('defect_id', $id)->delete();	
						$delItem = TRUE;
					}					
				}

				if($delItem){
					$calculate = new CalculationsController();
					$calculate->Recalculate($Quote->id);

					$QuotesNotes = new QuotesNotes();
					$QuotesNotes->quote_id = $Quote->id;
					$QuotesNotes->user_id = Auth::user()->id;
					$QuotesNotes->display_level = 1;
					$QuotesNotes->note = 'product part (' . $type .' - '. $name . ') deleted by admin and removed from quote items.';
					$QuotesNotes->save();
				}
			}
		}
		return true;
	}

    public function multipleDelete(Request $request) {
        if ($this->filter($this->permission)) {

            foreach($request->get('ids') as $id){
				$this->deleteSingleItem($id);
            }
            Toast::error('Record has been deleted', 'Success');
            return Redirect()->back();

        } else {
             Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }
}
