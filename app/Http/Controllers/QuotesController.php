<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use DateTime;
use links;
use App\User;
use Session;
use Toast;
use App\Client;
use App\Product;
use App\QuoteStep;
use App\QuoteStepItem;
use App\ProductOptionGroup;
use App\ProductOption;
use App\Http\Controllers\ClientsController;

class QuotesController extends Controller {

     private $permission = [1];

    public function __construct() {
        $this->middleware('auth');
    }

   

    public function listQuoteStep(Request $request) {
        if ($this->filter($this->permission)) {
            $QuoteType=QuoteStep::with('Type','Brand','Client')->orderBy('id','desc')->get();

            
            
//            $QuoteType=QuoteStep::select('quote_step.id', 'clients.name as cname', 'p.name as p_type', 'b.name as brand', 'quote_step.client_id', 'quote_step.type_id', 'quote_step.brand_id', 'quote_step.name');
//            $QuoteType->leftjoin('clients', 'quote_step.client_id', '=', 'clients.id');
//            $QuoteType->leftjoin('product_options as p', 'quote_step.type_id', '=', 'p.id');
//            $QuoteType->leftjoin('product_options as b', 'quote_step.brand_id', '=', 'b.id');
//            $QuoteType = $QuoteType->orderBy('quote_step.id', 'desc')->get();

           
            return view('Quotes/listQuoteStep', compact('QuoteType'));
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');

        }
    }

    public function addQuoteStep() {
        if ($this->filter($this->permission)) {
            $type_id = DB::table('product_options')->where('type', '=', 'Product')->orderBy('name', 'asc')->get();
            $brand_id = DB::table('product_options')->where('type', '=', 'Brand')->orderBy('name', 'asc')->get();
            $group_codes = ProductOptionGroup::select('group', 'group_code')->distinct()->orderBy('group', 'asc')->get();
           
           
            $clients = new ClientsController();
            $cids = Array();
            $parent_ids = $clients->getTree($cids, 0, 0, '0', '', '');
            return view('Quotes/addQuoteStep', compact('parent_ids', 'type_id', 'brand_id', 'group_codes'));
        } else {
           Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');

        }
    }

    public function insertQuote(Request $request) {
        if ($this->filter($this->permission)) {
            Input::flash();
            $this->validate($request, array(
                //'brand_id' => 'required',
                'name' => 'required',
                'type_id' => 'required',
                'group_code' => 'required',
                'order' => 'required'
            ));
            $orders = $request->order;
            $group_codes = $request->group_code;

            $QuoteStep = new QuoteStep();
            $QuoteStep->client_id = $request->parent_id;
            $QuoteStep->name = $request->name;
            $QuoteStep->brand_id = $request->brand_id;
            $QuoteStep->type_id = $request->type_id;
            $group_code = '';
            if ($QuoteStep->save()) {
                $i = 0;
                foreach ($orders as $key => $order) {
                    $group_code = $group_codes[$i];
                    $i++;
                    $QuoteStepItem = new QuoteStepItem();
                    $QuoteStepItem->quote_step_id = $QuoteStep->id;
                    $QuoteStepItem->order = $order;
                    $QuoteStepItem->group_code = $group_code;
                    $QuoteStepItem->save();
                }
                Toast::success('Record has been saved.', 'Success');
                return redirect('/Quotes/listQuoteStep');
            } else {
                Toast::error('Record not saved', 'Error');
                return redirect('/Quotes/listQuoteStep');
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');

        }
    }

    public function editQuoteStep($id = null) {
        if ($this->filter($this->permission)) {
            $type_id = ProductOption::where('Type', '=', 'Product')->get();
            $brand_id = ProductOption::where('Type', '=', 'Brand')->get();
            $QuoteStepItem = QuoteStepItem::select('*')->where('quote_step_id', '=', $id)->orderBy('order', 'asc')->get();
            $QuoateStep = QuoteStep::find($id);
            $group_codes = ProductOptionGroup::select('group', 'group_code')->distinct()->orderBy('group_code', 'asc')->get();
            $clients = new ClientsController();
            $cids = Array();
            $parent_ids = $clients->getTree($cids, 0, 0, '0', '', '');
            
           
            return view('Quotes/editQuoteStep', compact('parent_ids', 'type_id', 'brand_id', 'QuoateStep', 'QuoteStepItem', 'group_codes'));
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');

        }
    }

    public function updateQuote(Request $request) {

        if ($this->filter($this->permission)) {
            Input::flash();
            $this->validate($request, array(
                'name' => 'required',
                'type_id' => 'required',
                'group_code' => 'required',
                'order' => 'required'
            ));
            $QuoteStep = QuoteStep::find($request->input('id'));
            if (!empty($QuoteStep)) {

                $QuoteStep->client_id = $request->input('parent_id');
                $QuoteStep->brand_id = $request->input('brand_id');
                $QuoteStep->name = $request->input('name');
                $QuoteStep->type_id = $request->input('type_id');
                $id = $request->input('id');
                $orders = $request->order;
                $group_codes = $request->group_code;
                QuoteStepItem::where('quote_step_id', $id)->delete();
                $group_code = '';
                $i = 0;
                
                foreach ($orders as $key => $order) {
                    $group_code = $group_codes[$key];
                    $i++;

                    $QuoteStepItem = new QuoteStepItem();
                    $QuoteStepItem->quote_step_id = $id;
                    $QuoteStepItem->order = $order;
                    $QuoteStepItem->group_code = $group_code;

                    $QuoteStepItem->save();
                    $QuoteStep->save();
                }
                Toast::success('Record has been saved.', 'Success');
                return redirect('/Quotes/listQuoteStep');
            } else {
                Toast::error('Id not found.', 'Error');
                return redirect('/Quotes/listQuoteStep');
            }
        } else {
           Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');

        }
    }

    public function multipleDelete(Request $request) {
        if ($this->filter($this->permission)) {
            if (QuoteStep::destroy($request->get('ids'))) {
                QuoteStepItem::where('quote_step_id', '=', $request->get('ids'))->delete();
                Toast::success('Records has been deleted.', 'Success');
                return Redirect('Quotes/listQuoteStep');
            } else {
                Toast::error('Please select any record.', 'Error');
                return Redirect('Quotes/listQuoteStep');
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');

        }
    }

    public function Delete($id = Null) {
        if ($this->filter($this->permission)) {
            $product = QuoteStep::find($id);

            if (!empty($product)) {
                $product->delete();
                QuoteStepItem::where('quote_step_id', '=', $id)->delete();
                Toast::success('Record has been deleted.', 'Success');
                return Redirect('Quotes/listQuoteStep');
            } else {
                Toast::error('Invalid id, Please try again!', 'Error');
                return Redirect('Quotes/listQuoteStep');
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');

        }
    }

    public function add_quote_step_items(Request $request) {
        if ($this->filter($this->permission)) {

            if (!empty($request->input('id'))) {
                $data = DB::table('product_option_groups')->where('option_id', '=', $request->input('id'))->get();
                //echo"<pre>"; print_r($data);die;
                return response()->json($data);
            }
        } else {
           Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');

        }
    }

    public function country_region(Request $request) {
        if ($this->filter($this->permission)) {
            if (!empty($request->input('id'))) {
                $country_region = DB::table('country_region')->where('country_id', '=', $request->input('id'))->orderBy('name', 'asc')->get();

                echo json_encode(array('country_region' => $country_region));
                die;
            }
        } else {
          Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');

        }
    }

}
