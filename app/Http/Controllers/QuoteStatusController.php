<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Session;
use DateTime;
use links;
use Toast;
use App\Client;
use App\User;
use App\QuoteStatus;

class QuoteStatusController extends Controller {

    private $permission = [1];

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        if ($this->filter($this->permission)) {
            Input::flash();
            $txt = $request->get('search');
            $limit = $request->get('perPage');


            if (isset($limit)) {
                $lim = $limit;
            } else {
                $lim = 30;
            }
            $Qstatus = DB::table('quote_status');
            $Qstatus->select('quote_status.id', 'quote_status.name', 'quote_status.source');
            if (isset($txt)) {
                $Qstatus->Where('name', 'like', '%' . $txt . '%');
                $Qstatus->OrWhere('source', 'like', '%' . $txt . '%');
            }

            $Qstatus = $Qstatus->orderBy('id', 'asc')->paginate($lim);
            return view('quoteStatus/index', array('Qstatus' => $Qstatus, 'lim' => $lim));
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addQuoteStatus(Request $request) {
        if ($this->filter($this->permission)) {
            return view('quoteStatus/addQuoteStatus');
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function insertQuoteStatus(Request $request) {
        if ($this->filter($this->permission)) {
            Input::flash();

            $this->validate($request, array(
                'name' => 'required',
                'source' => 'required'
            ));

            $save = QuoteStatus::Create(array(
                        'name' => $request->input('name'),
                        'source' => $request->input('source')
            ));
            if ($save->save()) {
                Toast::success('Record has been saved.', 'Success');
                return redirect('quoteStatus/index');
            } else {
                Toast::error('Record not saved . Id not found', 'Error');
                return redirect('quoteStatus/index');
            }
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editQuoteStatus(Request $request, $id = Null) {
        if ($this->filter($this->permission)) {
            if ($id != null) {
                $QuoteStatus = QuoteStatus::find($id);

                if (!empty($QuoteStatus)) {
                    return view('/quoteStatus/editQuoteStatus', compact('QuoteStatus'));
                } else {
                    Toast::error('Id Not Found', 'Error');
                    return Redirect('quoteStatus/editQuoteStatus');
                }
            }
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateQuoteStatus(Request $request) {
        if ($this->filter($this->permission)) {
            Input::flash();
            $this->validate($request, array(
                'name' => 'required',
                'source' => 'required '
            ));
            $QuoteStatus = QuoteStatus::find($request->input('id'));
            $QuoteStatus->name = $request->get('name');
            $QuoteStatus->source = $request->get('source');

            if ($QuoteStatus->save()) {
                Toast::success('Record has been updated.', 'Success');
                return Redirect('quoteStatus/index');
            } else {
                Toast::error('Product not updated, Please try again.', 'Error');
                return Redirect('quoteStatus/index');
            }
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function Delete($id = Null) {
        if ($this->filter($this->permission)) {
            $Qstatus = QuoteStatus::find($id);
            //echo "<pre>";print_r($user);die;
            if (!empty($Qstatus)) {
                $Qstatus->delete();
                Toast::success('Records has been deleted.', 'Success');
                return Redirect('quoteStatus/index');
            } else {
                Toast::error('Invalid id, Please try again!', 'Error');
                return Redirect('quoteStatus/index');
            }
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function multipleDelete(Request $request) {
        if ($this->filter($this->permission)) {
            if (QuoteStatus::destroy($request->get('ids'))) {
                Toast::success('Records has been deleted.', 'Success');
                return Redirect('quoteStatus/index');
            } else {
                Toast::error('Please select any record.', 'Error');
                return Redirect('quoteStatus/index');
            }
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

}
