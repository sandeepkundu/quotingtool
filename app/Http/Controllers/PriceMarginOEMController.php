<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Session;
use DateTime;
use links;
use Toast;
use App\PriceMarginOEM;
use App\PriceMarginServices;


class PriceMarginOEMController extends Controller
{
      public function __construct(){
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){   
        Input::flash();
        $clients=DB::table('clients')->get();
        $txt=$request->get('search');
        $limit = $request->get('perPage');
       
        if(isset($limit)){
            $lim=$limit;
        }else{
            $lim = 30;
        }
        $oem = DB::table('price_margin_oem');
        $oem->select('price_margin_oem.id','clients.name', 'price_margin_oem.client_id', 'price_margin_oem.oem_type' , 'price_margin_oem.status' , 'price_margin_oem.value');
        if (isset($txt)) {
            $oem->Where('oem_type', 'like', '%' . $txt . '%');
            $oem->OrWhere('value', 'like', '%' . $txt . '%');
        }
        $oem->leftjoin('clients', 'price_margin_oem.client_id', '=', 'clients.id');
        $oem =$oem->orderBy('price_margin_oem.id', 'desc')->paginate($lim); 
        return view('/priceMarginOEM/index', array('oem'=>$oem, 'lim'=>$lim, 'clients'=>$clients));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addPriceMargin(Request $request){

    	$client = DB::table('clients')->get();
        return view('priceMarginOEM/addPriceMargin', array('client'=>$client));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function insertPriceMargin(Request $request)
    { //dd($request->all());
        Input::flash();
        
        $this->validate($request, array(
            'client_id' => 'required',
            'oem_type' => 'required',
            'value' => 'required|numeric',

        ));
        
        if($request->status != null && $request->status == 'on'){
            $save = PriceMarginOEM::Create(array(
                'client_id' => $request->client_id,
                'oem_type' => $request->oem_type,
                'value' => $request->value,
                'status' => 'active'
            ));
        } else {
            $save = PriceMarginOEM::Create(array(
                'client_id' => $request->client_id,
                'oem_type' => $request->oem_type,
                'value' => $request->value,
                'status' => 'inactive'
            ));
        }

        if ($save->save()) {
            Toast::success('Record has been Saved.', 'Success');
            return redirect('priceMarginOEM/index');
        } else {
            Toast::error('Record not saved. Id not found.', 'Error');  
            return redirect('priceMarginOEM/index');
        }  
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  
    public function editPriceMargin(Request $request,$id=Null ){ 
        if(isset($id)){
        	$client = DB::table('clients')->get();
            $oem = PriceMarginOEM::find($id);
            if(!empty($oem)){
                return view('/priceMarginOEM/editPriceMargin', compact('oem', 'client'));
            }else{
                Toast::error('Id Not Found', 'Error'); 
                return Redirect('priceMarginOEM/editPriceMargin');
            }
         }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function updatePriceMargin(Request $request){
        Input::flash();
        $this->validate($request, array(
            'client_id' => 'required',
            'oem_type' => 'required',
            'value' => 'required|numeric'
        ));

        $oem = PriceMarginOEM::find($request->input('id'));

        if(!empty($oem)) {
			if($request->get('status')!=null && $request->status == 'on'){
				$oem->status='active';
			}else{
				$oem->status='inactive';
			}
            $oem->client_id = $request->get('client_id');
            $oem->oem_type = $request->get('oem_type');
            $oem->value = $request->get('value');
               
               
            if($oem->save()){
                Toast::success('Record has been updated.', 'Success');
                return Redirect('/priceMarginOEM/index');
            }else{
                Toast::error('Product not updated. Please try again.', 'Error');
                return Redirect('/priceMarginOEM/index');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function Delete($id = Null){   

       $oem = PriceMarginOEM::find($id);
       if (!empty($oem)) {
            $oem->delete();
            Toast::success('Records has been deleted.', 'Success');
            return Redirect('/priceMarginOEM/index');
       } else {
            Toast::error('Invalid id, Please try again!', 'Error');
            return Redirect('/priceMarginOEM/index');
       }
    }

     public function multipleDelete(Request $request) {
        if (PriceMarginOEM::destroy($request->get('ids'))) {
            Toast::success('Records has been deleted.', 'Success');
            return Redirect('/priceMarginOEM/index');
        } else {
            Toast::error('Please select any record.', 'Error');
            return Redirect('/priceMarginOEM/index');
        }
    }




}
