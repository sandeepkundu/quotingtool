<?php
namespace App\Http\Controllers;

ini_set('max_execution_time', 600);

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Session;
use DateTime;
use links;
use Toast;
use App\Client;
use App\User;
use App\ClientUser;
use App\Country;
use App\CountryRegion;
use App\RoleUser;
use App\ProductOption;
use App\Product;
use App\ProductOptionGroup;
use App\QuoteManagement;
use View;
use Response;
use App\QuoteItem;
use App\Quote;
use App\QuoteStatus;
use Mail;
use PDF;
use URL;
use App\QuoteCollection;
use App\QuoteItemsDefect;
use App\QuoteCollectionItem;
use App\QuoteValid;
use App\Navigation;
use App\PasswordReset;
use App\IntegrationImport;
use App\QuotesNotes;
use App\Http\Controllers\CalculationsController;


class VariancesController extends Controller {
    
     private $permission = [1];

    public function __construct() {
        $this->middleware('auth', ['except' => ['resetPassword', 'reset']]);
         date_default_timezone_set('Asia/Kolkata');
    }

    public function BulkModelImports(){
        if (!$this->filter($this->permission)) {
                   
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
         
        $IntegrationImports = IntegrationImport::where('source','=','model')->orderBy('date','DESC')->paginate();
        return view('Variances/BulkModelImports',array(
                'IntegrationImports'=> $IntegrationImports,

            ));
    }
    
    public function export_data(){
        $folder         = 'i/';
        $datestring     = date_format(new DateTime, 'Y-m-d-H-i-s');
        $exportfilename = 'model-export-' . $datestring . '.csv';

        $processrecords = array();
        $columnames     = array('model','product type','brand','processor','screen','hdd','ram','dvd','price','margin $','margin %','client','status', 'confgitype', 'country');

        $sql = "SELECT 
            model,
            ptype.name AS `type`,
            pbrand.name AS `brand`,
            proc.name AS `processor`,
            screen.name AS `screen`,
            hdd.name AS `hdd`,
            ram.name AS `ram`,
            dvd.name AS `dvd`,
            CASE WHEN p.value_factor = 'explicit price' THEN p.`value` ELSE '' END AS `price`,
            CASE WHEN p.value_factor = 'margin price' THEN p.`value` ELSE '' END AS `margind`,
            CASE WHEN p.value_factor = 'margin percent' THEN p.`value` ELSE '' END AS `marginp`,
            CASE WHEN p.client_id > 0 THEN `clients`.name ELSE '' END AS  `client`,
            p.`status`,
			p.`pricing_type` AS configtype,
			CASE WHEN p.country_id > 0 THEN `country`.name ELSE '' END AS  `country`
            FROM `product` p
            INNER JOIN `product_options` ptype ON p.type_id = ptype.id
            LEFT JOIN `product_options` proc ON p.processor_id = proc.id
            LEFT JOIN `product_options` pbrand ON p.brand_id = pbrand.id
            LEFT JOIN `product_options` screen ON p.screen_id = screen.id
            LEFT JOIN `product_options` hdd ON p.hdd_id = hdd.id
            LEFT JOIN `product_options` ram ON p.ram_id = ram.id
            LEFT JOIN `product_options` dvd ON p.dvd_id = dvd.id
            LEFT JOIN `clients` ON p.client_id = clients.id
			LEFT JOIN `country` ON p.country_id = country.id";
        $resultset = DB::select(DB::raw($sql));

        if (!empty($resultset)) {
            foreach($resultset as $product){
                $processrecords[] = array(
                            $product->model,
                            $product->type,
                            $product->brand,
                            $product->processor,
                            $product->screen,
                            $product->hdd,
                            $product->ram,
                            $product->dvd,
                            $product->price,
                            $product->margind,
                            $product->marginp,
                            $product->client,
                            $product->status,
                            $product->configtype,
                            $product->country
                        );
            }
        }
                    
        //save the processed records to file.
        $exportfile = fopen($folder . $exportfilename, "w");
        
        //array_unshift($columnames, 'message');
        fputcsv($exportfile, $columnames);
        foreach ($processrecords as $line) {
            fputcsv($exportfile, $line);
        }
        
        $IntegrationImport               = new IntegrationImport;
        $IntegrationImport->date         = date('Y-m-d-H-i-s');
        $IntegrationImport->fileimported = '';
        $IntegrationImport->source       = 'model';
        $IntegrationImport->fileexported  = $exportfilename;
        $IntegrationImport->records_valid = count($processrecords);
        $IntegrationImport->records_invalid = 0;
        $IntegrationImport->save();
        $import_id = $IntegrationImport->id;

        Toast::success('export has been completed.', 'Success');
        //echo 'ajeet';die;
        return redirect('/Variances/BulkModelImports');
    }
    
    public function import_data(){
        if (!$this->filter($this->permission)) {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
        
        $folder         = 'i/';
        $debug          = false;
        $datestring     = date_format(new DateTime, 'Y-m-d-H-i-s');
        $importfilename = 'model-import-' . $datestring . '.csv';
        $exportfilename = 'model-export-' . $datestring . '.csv';
        
        
        //============== 1. Save uploaded file to server =======================// 
        
        if (!isset($_FILES["fileToUpload"])) {
            return 'A csv file must be selected for upload';
        }
        ///echo "<pre>";print_r();die;
        if (!isset($_FILES['fileToUpload']['name'])) {
            Toast::error('A error was encountered uploading this file (' . $_FILES["fileToUpload"]["error"] . ')!!!', 'Error');
            return redirect('/Variances/BulkModelImports');
            //return 'A error was encountered uploading this file (' . $_FILES["file"]["error"] . ')';
        }
        
        $ftype = explode(".", $_FILES['fileToUpload']['name']);
        
        if (strtolower(end($ftype)) !== 'csv') {
            Toast::error('only a csv can be uploaded', 'Error');
            return redirect('/Variances/BulkModelImports');
        }
        
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $folder . $importfilename) === FALSE) {
            Toast::error('File could not be saved', 'Error');
            return redirect('/Variances/BulkModelImports');
        }
        
        //============================== 2. Create a new import record ======================//
        $IntegrationImport               = new IntegrationImport;
        $IntegrationImport->date         = date('Y-m-d-H-i-s');
        $IntegrationImport->fileimported = $importfilename;
        $IntegrationImport->source       = 'model';
        $IntegrationImport->save();
        $import_id = $IntegrationImport->id;
        
        //============================= 3. Process the file ===========================//
        
        $row            = 0;
        $columncount    = 0;
        $processrecords = array();
        $columnames     = array();
        $isValid        = FALSE;
        $num_invalid    = 0;
        $num_valid      = 0;
 		$recalc_ids		= array(); //collection of all changed product so quote can be recalculated
 		$delete_ids		= array(); //collection of all inactive product so quote be delete from in-progss quotes and be recalculated
       
        if (($handle = fopen($folder . $importfilename, "r")) !== FALSE) {
            
            while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
                $row++;
                if ($row == 1) {
                    $columncount = count($data);
                    $columnames  = $data;
                } else if (count($data) != 15) {
                    array_unshift($data, "[record has incorrect number of columns (" . count($data) . ")]");
                    array_push($processrecords, $data);
                    $num_invalid++;
                } else {
                    $model     = $data[0]; // Model                e.g. Latitude E7240
                    $type      = $data[1]; // Product Type         e.g. Notebook
                    $brand     = $data[2]; // Product Type         e.g. Notebook
                    $processor = $data[3]; // Processor            e.g. Core i3 1st gen    
                    $screen    = $data[4]; // Secreen Size         e.g. 15"            
                    $hdd       = $data[5]; // HDD                  e.g. 500GB
                    $ram       = $data[6]; // RAM                  e.g. 8
                    $dvd       = $data[7]; // DVD                  e.g. Yes
                    $price     = $data[8]; // Price                e.g. 1080.00
                    $margind   = $data[9]; // dollar margin        e.g. 10.00
                    $marginp   = $data[10]; // percent margin       e.g. 10
                    $client    = $data[11]; // client               e.g. HP World Stores
                    $status    = $data[12]; // status               e.g. active
					$pt		   = $data[13]; // product type i.e. Lcd/Processor/Model   e.g. Lcd
					$country   = $data[14]; // country				e.g. New Zealand
                    
                    // validate the data
                    $validationmessage = '';
					//this flag is used to chack if there is processor price for a model that doesn't have a price
					$toCheckPrice = FALSE;
					$isValid = FALSE;
                    
                    if (!isset($status) || $status != 'active') {
                        $status = 'disabled';
                    }
                    
                    //validate price
                    if (isset($price) && $price != '' && !is_numeric($price)) {
                        $validationmessage .= "[price is not valid]";
                    }
                    if (isset($margind) && $margind != '' && !is_numeric($margind)) {
                        $validationmessage .= "[margin value is not valid]";
                    }
                    if (isset($marginp) && $marginp != '' && !is_numeric($marginp)) {
                        $validationmessage .= "[margin percent is not valid]";
                    }
                    
                    $pcount  = 0;
                    $pfactor = 'explicit price';
                    if (isset($price) && is_numeric($price)) {
                        $pcount++;
                    }
                    
                    if (isset($marginp) && is_numeric($marginp) && $marginp != 0) {
                        $pcount++;
                        $pfactor = 'margin percent';
                        $price   = $marginp;
                    }
                    
                    if (isset($margind) && is_numeric($margind) && $margind != 0) {
                        $pcount++;
                        $pfactor = 'margin price';
                        $price   = $margind;
                    }

                    if ($pcount != 1) {
                        $validationmessage .= "[invalid pricing details]";
                    }

					//final check is that price doesn't need to exists for a model
					if(strtolower($pt) == 'model' && $price == '' && $pfactor == 'explicit price'){		
						$validationmessage = '';
						//set price check falg so check can be made once type and processor ids are returned
						$toCheckPrice = TRUE;
					}
                    
                    if (!isset($type) || $type === '' ) {
                        $validationmessage .= "[product type is required]";
                    }

					if(strtolower($pt) != 'model' && strtolower($pt) != 'processor' && strtolower($pt) != 'lcd'){
						$validationmessage .= "[product configuration is required]";
					}

					if(strtolower($pt) == 'model' && trim($model) == ''){
						$validationmessage .= "[model required]";
					}
 					if(strtolower($pt) == 'processor' && trim($processor) == ''){
						$validationmessage .= "[processor required]";
					}
 					if(strtolower($pt) == 'lcd' && trim($screen) == ''){
						$validationmessage .= "[screen required]";
					}
                   
                    //retreive client id
                    $client_id = 0;
                    $client    = Client::where('name', '=', $client)->first();
                    if (count($client) > 0) {
                        if (!isset($client->id)) {
                            $client_id = $client->id;
                        }
                    }                    

                    // vailidate model and device components
					$model_id		= 0;
					$brand_id		= 0;
					$type_id		= 0;
					$dvd_id			= 0;
					$hdd_id			= 0;
					$ram_id			= 0;
					$screen_id		= 0;  
					$processor_id	= 0;
					$country_id		= 0;

					if(trim($processor) !== ''){
						$processor_obj = ProductOption::where('type', 'processor' )->where('name', $processor)->first();
						if(null !== $processor_obj) { $processor_id = $processor_obj->id; }
					}

					if(trim($type) !== ''){
						$type_obj = ProductOption::where('type', 'product' )->where('name', $type)->first();
						if(null !== $type_obj) { $type_id = $type_obj->id; }
					}

					if(trim($screen) !== ''){
						$screen_obj = ProductOption::where('type', 'screen size' )->where('name', $screen)->first();
						if(null !== $screen_obj) { $screen_id = $screen_obj->id; }
					}

					if(trim($hdd) !== ''){
						$hdd_obj = ProductOption::where('type', 'hdd' )->where('name', $hdd)->first();
						if(null !== $hdd_obj) { $hdd_id = $hdd_obj->id; }
					}

					if(trim($ram) !== ''){
						$ram_obj = ProductOption::where('type', 'ram' )->where('name', $ram)->first();
						if(null === $ram_obj){
							$ram_obj = ProductOption::where('type', 'ram' )->where('value', $ram)->first();
						}
						if(null !== $ram_obj){ $ram_id = $ram_obj->id; }
					}

					if(trim($dvd) !== ''){
						$dvd_obj = ProductOption::where('type', 'dvd' )->where('name', $dvd)->first();
						if(null !== $dvd_obj) { $dvd_id = $dvd_obj->id; }
					}

					if(trim($brand) !== ''){
						$brand_obj = ProductOption::where('type', 'brand' )->where('name', $brand)->first();
						if(null !== $brand_obj) { $brand_id = $brand_obj->id; }
					}  
					
					if(isset($country) && $country !== ''){
						$country_obj = Country::where('name', '=', $country)->first();
						if(null !== $country_obj) { $country_id = $country_obj->id; }
					} 



					/* find client specifi model processor or lcd combinations */
					$client_id = 0;
					if(trim($client) !== '' ){
						$client_obj = Client::where('name', $client);
						$client_id = $client->id;
					}

 					if($toCheckPrice){
						$prod = Product::where('type_id', '=', $type_id)
									->where('processor_id', '=', $processor_id)
									->where('value_factor', '=', 'explicit price')
									->where('pricing_type', '=', 'Processor')
									->first();
						if(null === $prod){
							$validationmessage .= "[no processor pricing for this model]";
						}
					}    
            
                    if ($type_id > 0) {
						$args = array();
						$args['type'] = $type_id;
						$args['configtype'] = $pt;

						$args2 = array();
                        $sqlbit = '';          
                        if ($brand_id > 0) {
                            $sqlbit .= " AND brand_id = :brand";
                            $args['brand'] = $brand_id;
                        }
                        if ($client_id > 0) {
                            $sqlbit .= " AND client_id = :client";
                            $args['client'] = $client_id;
                        }
                        if ($country_id > 0) {
                            $sqlbit .= " AND country_id = :country";
                            $args['country'] = $country_id;
                        } 
   
						$query = "SELECT id 
								FROM product 
								WHERE pricing_type = :configtype
								AND type_id = :type " . $sqlbit;

						if($processor_id > 0 && strtolower($pt) == 'processor'){
							$args2 = $args;  
							$args2['processor'] = $processor_id;
							$query .= "	AND processor_id = :processor 
								AND screen_id IS NULL AND ram_id IS NULL AND dvd_id IS NULL AND hdd_id IS NULL AND (model = '' OR model IS NULL) ";								
						} else if($screen_id > 0 && strtolower($pt) == 'lcd'){
							$args2 = $args;  
							$args2['screen'] = $screen_id;
							$query .= "	AND screen_id = :screen
								AND processor_id IS NUll AND ram_id IS NULL AND dvd_id IS NULL AND hdd_id IS NULL AND (model = '' OR model IS NULL)";
						} else if($model != '' && strtolower($pt) == 'model'){
							$args2 = $args;  
							$args2['model'] = $model;
							$args2['processor'] = $processor_id;
							$query .= "	AND model = :model
								AND processor_id = :processor";
						}

						if ($debug) {
							echo "<br>". str_replace(':type', $type_id, str_replace(':processor', $processor_id, str_replace(':configtype', "'". $pt ."'", str_replace(':client', $client_id, $query))));
							echo "<br>";
							print_r($args2);	
						}
					
						$model_id = 0;
						if(count($args2) > 0){
							$resultset = DB::select(DB::raw($query), $args2);
							if (!empty($resultset)) {
								$model_id = $resultset[0]->id;
							}
						}  
                 
						if ($debug) {
							echo '<pre>';
							print_r($data);
							echo '</pre>';
							 echo "ID = $model_id<br>";
							echo "model = $model<br>";
							echo "type = $type / $type_id<br>";
							echo "processor = $processor / $screen_id<br>";
							echo "screen = $screen / $screen_id<br>";
							echo "hdd = $hdd / $hdd_id<br>";
							echo "ram = $ram / $ram_id<br>";
							echo "dvd = $dvd / $dvd_id<br>";
							echo "brand = $brand / $brand_id<br>";
							echo "client = $client<br>";
							echo "client id = $client_id<br>";
							echo "price = $price<br>";
							echo "price factor = $pfactor<br>";
							echo "status = $status<br>";
							echo "price count = $pcount<br>";
						}    
                    
                        if ($brand_id === 0 && trim($brand) !== '') {
                            $validationmessage .= "[brand lookup not found]";
                        }
                        if ($type_id === 0) {
                            $validationmessage .= "[product type lookup not found]";
                        }
                        if ($dvd_id === 0 && isset($dvd) && $dvd !== '') {
                            $validationmessage .= "[dvd lookup not found]";
                        }
                        
                        if ($client_id === 0 && isset($client) && $client !== '' && strtolower(trim($client)) !== 'default') {
                            $validationmessage .= "[client not found]";
                        }    
   
                        if ($country_id === 0 && $country !== '') {
                            $validationmessage .= "[country not found]";
                        }                    

                        //don't continue if there are any validation errors
                        if ($validationmessage === '') {
                            $step_id = 0;   

                            $resultset = DB::select(DB::raw("SELECT id FROM `quote_step` WHERE type_id = '" . $type_id . "' AND (brand_id = '" . $brand_id . "' OR brand_id is null) AND (client_id = '" . $client_id . "' OR client_id = 0) ORDER BY client_id DESC, brand_id DESC LIMIT 1"));
                                                        
                            if (count($resultset) > 0) {
                                $step_id = $resultset[0]->id;
                            }
                            
                            //RAM                       
                            if ($ram_id === 0 && isset($ram) && $ram !== '' && $step_id > 0) {
                                //add ram
                                $ProductOption              = new ProductOption;
                                $ProductOption->type        = 'RAM';
                                $ProductOption->name        = $ram;
                                $ProductOption->order       = 0;
                                $ProductOption->status      = 'active';
								$ProductOption->isActive	= 1;
                                $ProductOption->description = '';
                                $ProductOption->image       = '';
                                $ProductOption->save();
                                
                                $ram_id = $ProductOption->id;

								$args = array(
									'val'	=> $ram,
									'id'	=> $ram_id,
									'cid'	=> $client_id,
									'type'	=> 'RAM',
									'step'	=> $step_id
								);
                                
                                $resultset = DB::select(DB::raw("INSERT INTO product_option_groups (`group`, `group_code`, `name`, `option_id`, `order`, `status`, `client_id`, `updated_at`, `type`) 
									SELECT pgo.`group`, pgo.`group_code`, :val, :id, 0, 'active', :cid, now(), po.type  
									FROM `product_option_groups` pgo 
									INNER JOIN `product_options` po ON pgo.option_id = po.id 
									INNER JOIN `quote_step_items` qsi ON qsi.group_code = pgo.group_code 
									WHERE po.type = :type AND qsi.quote_step_id = :step
									LIMIT 1;"), $args);
                            }                            

                            //HDD
                            if ($hdd_id === 0 && isset($hdd) && $hdd !== '' && $step_id > 0) {
                                //add hdd
                                $ProductOption              = new ProductOption;
                                $ProductOption->type        = 'HDD';
                                $ProductOption->name        = $hdd;
                                $ProductOption->order       = 0;
                                $ProductOption->status      = 'active';
								$ProductOption->isActive	= 1;
                                $ProductOption->description = '';
                                $ProductOption->image       = '';
                                $ProductOption->save();
                                
                                $hdd_id = $ProductOption->id;
                                
								$args = array(
									'val'	=> $hdd,
									'id'	=> $hdd_id,
									'cid'	=> $client_id,
									'type'	=> 'HDD',
									'step'	=> $step_id
								);
                                
                                $resultset = DB::select(DB::raw("INSERT INTO product_option_groups (`group`, `group_code`, `name`, `option_id`, `order`, `status`, `client_id`, `updated_at`, `type`) 
									SELECT pgo.`group`, pgo.`group_code`, :val, :id, 0, 'active', :cid, now(), po.type  
									FROM `product_option_groups` pgo 
									INNER JOIN `product_options` po ON pgo.option_id = po.id 
									INNER JOIN `quote_step_items` qsi ON qsi.group_code = pgo.group_code 
									WHERE po.type = :type AND qsi.quote_step_id = :step
									LIMIT 1;"), $args);                           }
                                
                            //SCREEN
                            if ($screen_id === 0 && isset($screen) && $screen !== '' && $step_id > 0) {
                                //add screen
                                $ProductOption              = new ProductOption;
                                $ProductOption->type        = 'Screen Size';
                                $ProductOption->name        = $screen;
                                $ProductOption->order       = 0;
                                $ProductOption->status      = 'active';
								$ProductOption->isActive	= 1;
                                $ProductOption->description = '';
                                $ProductOption->image       = '';
                                $ProductOption->save();
                                
                                $screen_id = $ProductOption->id;
                                
								$args = array(
									'val'	=> $screen,
									'id'	=> $screen_id,
									'cid'	=> $client_id,
									'type'	=> 'Screen Size',
									'step'	=> $step_id
								);
                                
                                $resultset = DB::select(DB::raw("INSERT INTO product_option_groups (`group`, `group_code`, `name`, `option_id`, `order`, `status`, `client_id`, `updated_at`, `type`) 
									SELECT pgo.`group`, pgo.`group_code`, :val, :id, 0, 'active', :cid, now(), po.type  
									FROM `product_option_groups` pgo 
									INNER JOIN `product_options` po ON pgo.option_id = po.id 
									INNER JOIN `quote_step_items` qsi ON qsi.group_code = pgo.group_code 
									WHERE po.type = :type AND qsi.quote_step_id = :step
									LIMIT 1;"), $args);                            }

							//PROCESSOR
							if ($processor_id === 0 && isset($processor) && $processor !== '') {							
                                $ProductOption = new ProductOption;
                                $ProductOption->type = 'Processor';
                                $ProductOption->name = $processor;
                                $ProductOptionorder	= 0;
                                $ProductOption->status = 'active';
								$ProductOption->isActive = 1;
                                $ProductOption->description = '';
                                $ProductOption->image = '';
                                        
                                $ProductOption->save();
                                $processor_id = $ProductOption->id;

                                //add to group
								if($step_id > 0){
									$args = array(
										'val'	=> $processor,
										'id'	=> $processor_id,
										'cid'	=> $client_id,
										'type'	=> 'Processor',
										'step'	=> $step_id
									);
                                
									$resultset = DB::select(DB::raw("INSERT INTO product_option_groups (`group`, `group_code`, `name`, `option_id`, `order`, `status`, `client_id`, `updated_at`, `type`) 
										SELECT pgo.`group`, pgo.`group_code`, :val, :id, 0, 'active', :cid, now(), po.type  
										FROM `product_option_groups` pgo 
										INNER JOIN `product_options` po ON pgo.option_id = po.id 
										INNER JOIN `quote_step_items` qsi ON qsi.group_code = pgo.group_code 
										WHERE po.type = :type AND qsi.quote_step_id = :step
										LIMIT 1;"), $args);								
								} else {
									$validationmessage .= "[unable to determine matching process step]";
								}
							}
                                  
                            //if the model id is greater than zero there it is an existing product and can be updated.
							if($model_id > 0){
								$Product               = Product::find($model_id);
                                $Product->processor_id = (strtolower($pt) == 'lcd') ? null : $processor_id;
                                $Product->brand_id     = $brand_id;
                                $Product->screen_id    = (strtolower($pt) == 'processor') ? null : $screen_id;
                                $Product->hdd_id       = (strtolower($pt) == 'lcd' || strtolower($pt) == 'processor') ? null : $hdd_id;
                                $Product->ram_id       = (strtolower($pt) == 'lcd' || strtolower($pt) == 'processor') ? null : $ram_id;
                                $Product->dvd_id       = (strtolower($pt) == 'lcd' || strtolower($pt) == 'processor') ? null : $dvd_id;
                                $Product->client_id    = $client_id;
                                $Product->status       = $status;
                                $Product->value        = ($price == '' && strtolower($pt) == 'model') ? null : $price;;
                                $Product->value_factor = $pfactor;
								$Product->pricing_type = $pt;
								$Product->country_id   = $country_id;
                                $Product->save();
								$validationmessage .= "[". $pt . " updated]";
								$isValid = TRUE;

								if('active' === $status){
									$recalc_ids[] = $model_id;
								} else {
									$delete_ids[] = $model_id;
								}
							} else {                            
								
                                $Product               = new Product;                                
								$Product->type_id      = $type_id;                               
                                $Product->brand_id     = $brand_id;                                
                                $Product->client_id    = $client_id;
                                $Product->status       = $status;
                                $Product->value        = ($price == '' && strtolower($pt) == 'model') ? null : $price;
                                $Product->value_factor = $pfactor;
                                $Product->pricing_type = $pt;
								$Product->country_id   = $country_id;

								if(strtolower($pt) == 'model'){
									$Product->model		= $model;
									$Product->hdd_id	= $hdd_id;
									$Product->ram_id	= $ram_id;
									$Product->dvd_id	= $dvd_id;
								}
								if(strtolower($pt) != 'lcd') {
									$Product->processor_id = $processor_id;
								}
								if(strtolower($pt) != 'processor') {
									$Product->screen_id = $screen_id;
								}                                

                                $Product->save();
								$validationmessage .= "[". $pt . " added]";
								$isValid = TRUE;  
								
							}                            
                        }
                    } else {
                        $validationmessage .= "[error occurred validating product type]";
                    }
                            
                    if ($isValid) {                        
                        $num_valid++;
                    } else {
                        $num_invalid++;
                    }
                            
                    array_unshift($data, $validationmessage);
                    array_push($processrecords, $data);
                }
            }
                    
            //save the records to file.
            $exportfile = fopen($folder . $exportfilename, "w");
            
            array_unshift($columnames, 'message');
            fputcsv($exportfile, $columnames);
            foreach ($processrecords as $line) {
                fputcsv($exportfile, $line);
            }

            
            $IntegrationImport                = IntegrationImport::find($import_id);
            $IntegrationImport->fileexported  = $exportfilename;
            $IntegrationImport->records_valid = $num_valid;
            $IntegrationImport->records_invalid = $num_invalid;

            $IntegrationImport->save();

			//need to recalulate all inprogress quotes.
			$Quotes = Quote::where('status', '=', '1')->get();
			if(count($Quotes) >0 ){
				foreach($Quotes as $Quote){
					$needRecalc = FALSE;

					$id = $Quote->id;
					$Quoteitems = QuoteItem::where('quote_id', '=', $id)->get();
					if(count($Quoteitems) > 0){
						foreach($Quoteitems as $Quoteitem){
							// product changed therefore need to recalc
							if( in_array($Quoteitem->product_id, $recalc_ids) || in_array($Quoteitem->model_id, $recalc_ids)){
								$needRecalc = TRUE;
							}

							//product set to inactive - therefore remove from quote items
							if( in_array($Quoteitem->product_id, $delete_ids)){
								QuoteItem::where('id', '=', $Quoteitem->id).delete();
								$needRecalc = TRUE;
							}

							//product set to inactive and assigned as the model id - requires recalc  
							if($Quoteitem->product_id !=  $Quoteitem->model_id &&  in_array($Quoteitem->model_id, $delete_ids)){
								$needRecalc = TRUE;
							}
						}
					}

					if($needRecalc){
						$calculate = new CalculationsController();
						$calculate->Recalculate($Quote->id);

						$QuotesNotes = new QuotesNotes();
						$QuotesNotes->quote_id = $Quote->id;
						$QuotesNotes->user_id = Auth::user()->id;
						$QuotesNotes->display_level = 1;
						$QuotesNotes->note = 'Model bulk import - recalculation of in-progress quote.';
						$QuotesNotes->save();
					}
				}
			}    
            Toast::success('import has been completed.', 'Success');
            //echo 'ajeet';die;
            return redirect('/Variances/BulkModelImports');
        }
    }

    public function import_pup_list(){
if (!$this->filter($this->permission)) {
    Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        
}
        $IntegrationImports = IntegrationImport::where('source','=','pup')->orderBy('date','DESC')->paginate();
        return view('Variances/import_pup',array(
                'IntegrationImports'=> $IntegrationImports,

            ));
    }


    function import_pup(Request $request) {
        //dd($request);
         if (!$this->filter($this->permission)) {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
         }

        $folder         = 'i/';
        $datestring = date_format(new DateTime, 'Y-m-d-H-i-s');
        $importfilename = 'pup-import-' . $datestring . '.csv';
        $exportfilename = 'pup-export-' . $datestring . '.csv';

        //=====================1. Save uploaded file to server ==============//

        if (!isset($_FILES["fileToUpload"])) {
            return 'A csv file must be selected for upload';
        }

        ///echo "<pre>";print_r();die;
        if (!isset($_FILES['fileToUpload']['name'])) {
            Toast::error('A error was encountered uploading this file (' . $_FILES["fileToUpload"]["error"] . ')!!!', 'Error');
            return redirect('/Variances/BulkModelImports');
            //return 'A error was encountered uploading this file (' . $_FILES["file"]["error"] . ')';
        }
        
        $ftype = explode(".", $_FILES['fileToUpload']['name']);
        
        if (strtolower(end($ftype)) !== 'csv') {
            Toast::error('only a csv can be uploaded', 'Error');
            return redirect('/Variances/BulkModelImports');
        }
        
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $folder . $importfilename) === FALSE) {
            Toast::error('File could not be saved', 'Error');
            return redirect('/Variances/BulkModelImports');
        }

        
        
        //===================2.Create a new import record======================// 
        $IntegrationImport               = new IntegrationImport;
        $IntegrationImport->date         = date('Y-m-d-H-i-s');
        $IntegrationImport->fileimported = $importfilename;
        $IntegrationImport->source       = 'pup';
        $IntegrationImport->save();
        $import_id = $IntegrationImport->id;


        // ===================== 3. Process the file ============================== //
        
        $row = 0;
        $columncount = 0;
        $processrecords = array();
        $columnames = array();
        $isValid = FALSE;
        $num_invalid = 0;
        $num_valid = 0;

        //update all records to have import field set to 0

        //$resultset = DB::select(DB::raw("UPDATE pup_contracts SET import = 0"));
        //$resultset = DB::select(DB::raw("UPDATE pup_contracts_months SET import = 0"));


        if (($handle = fopen($folder. $importfilename, "r")) !== FALSE) {
           
            while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
                $row ++;
                //echo "<pre>";print_r($data);die;
                if($row == 1){
                    $columncount = count($data);
                    $columnames = $data;
                } else if (count($data) != $columncount){
                    // incorrect number of columns for the row
                    array_unshift($data, "[record has incorrect number of columns (".  count($data) . ")]");
                    array_push($processrecords, $data);
                    $num_invalid++;
                } else {
                    $serial     = $data[0];     // Serial Number        e.g. 123456
                    $date       = $data[1];     // Date (dd/mm/yyyy)    e.g. 13/11/2017
                    $status     = $data[2];     // Status               e.g. Valid
                    $model      = $data[3];     // Model                e.g. Latitude E7240
                    $type       = $data[4];     // Product Type         e.g. Notebook
                    $processor  = $data[5];     // Processor            e.g. Core i3 1st gen    
                    $screen     = $data[6];     // Secreen Size         e.g. 15"            
                    $hdd        = $data[7];     // HDD                  e.g. 500GB
                    $ram        = $data[8];     // RAM                  e.g. 8
                    $dvd        = $data[9];     // DVD                  e.g. Yes
                    $price      = $data[10];    // Price                e.g. 1080.00
                    $margin     = $data[11];    // HP Margin            e.g. 10                    
                    
                    $validationmessage = '';

                    //validate price
                    if(!isset($price) || !is_numeric($price) || $price <= 0){
                        $validationmessage .= "[price is not valid]";
                    }                    

                    //validate date
                    if(!isset($date)){
                        $validationmessage .= "[price is not valid]";
                    } else{
                        $date_now = new DateTime();
                        try{
                            $dateTime = DateTime::createFromFormat('d/m/Y', $date);
                            if ($date_now <= $dateTime) {
                                $validationmessage .= "[price is not valid]";
                            }
                        }catch (Exception $e){
                            $validationmessage .= "[price is not valid]";
                        }
                    }                    

                    //validate margin
                    if(!isset($margin) || !is_numeric($margin) || $margin < 0){
                        $validationmessage .= "[margin is not valid]";
                    }                    

                    //validate status
                    if(!isset($status) || ($status !=  'Valid' && $status != 'Invalid')){
                        $validationmessage .= "[status is not valid]";
                    }                   

                    // vailidate model and device components
                    //$model = str_replace('"','\"',$model);
                    //$screen = str_replace('"','\"',$screen);
                    
                    //echo "IFNULL((SELECT '' FROM product_options WHERE type = 'product' AND name = '$type'), '[product type match not found]' ) AS type , 
                      //  ";die();
						
                    $sql = "SELECT IFNULL((SELECT '' FROM product WHERE model = :model limit 1), '[model match not found]') AS model , 
                        IFNULL((SELECT '' FROM product_options WHERE type = 'product' AND name = :type limit 1), '[product type match not found]' ) AS type , 
                        IFNULL((SELECT '' FROM product_options WHERE type = 'processor' AND name = :processor limit 1), '[processor match not found]' ) AS processor , 
                        IFNULL((SELECT '' FROM product_options WHERE type = 'screen size' AND name = :screen limit 1), '[screen match not found]' ) AS screen , 
                        IFNULL((SELECT '' FROM product_options WHERE type = 'hdd' AND name = :hdd limit 1), '[HDD match not found]' ) AS hdd , 
                        IFNULL((SELECT '' FROM product_options WHERE type = 'ram' AND value = :ram limit 1), '[RAM match not found]' ) AS ram , 
                        IFNULL((SELECT '' FROM product_options WHERE type = 'dvd' AND name = :dvd limit 1), '[DVD match not found]' ) AS dvd , 
                        IFNULL((SELECT '' FROM pup_contracts WHERE import= 1 AND serial_number = :serial limit 1), '[Duplicate record]' ) AS duplicate";
					
					$args = array(
                                    'type'		=> $type,
                                    'screen'	=> $screen,
									'model'		=> $model,
									'processor' => $processor,
									'hdd'		=> $hdd,
									'ram'		=> $ram,
									'dvd'		=> $dvd,
									'serial'	=> $serial
                                );

                    $resultset = DB::select(DB::raw($sql), $args);

                    if(isset($resultset)){
                        $validationmessage .= @$resultset[0]->model . @$resultset[0]->type . @$resultset[0]->processor . @$resultset[0]->screen . @$resultset[0]->hdd . @$resultset[0]->ram . @$resultset[0]->dvd;
                    } else {
                        $validationmessage .= "[error occurred validating device]";
                    }

                    if($validationmessage === '') { 
                        $isValid = TRUE; 
                        $validationmessage = 'OK';
                        $num_valid ++;
                    } else {
                        $num_invalid ++;
                    }

                    //var_dump($validationmessage);die;
                    // insert the data if valid
                    if($isValid){
                        //echo "sdfsdf";die;
                        $da =  date('Y-m-d', strtotime($date));
						$da1 =  date('Y-m-d H:i:s', time());
						
						$sql = "INSERT INTO `pup_contracts`  
                        (`serial_number`, `date`, `status`, `price`, `margin`, `model`, `model_id`, `type`, `type_id`, `processor`,`processor_id`, `screen`, `screen_id`, `hdd`, `hdd_id`, `ram`, `ram_id`, `dvd`, `dvd_id`, `import`, `created_at`, `updated_at`) 
                        SELECT :serial, '$da', '$status', '$price', '$margin',
                        :model, (SELECT id FROM product WHERE model = :model2 limit 1)
                        , '$type', (SELECT id FROM product_options WHERE type = 'product' AND name = '$type' limit 1)
                        , '$processor', (SELECT id FROM product_options WHERE type = 'processor' AND name = '$processor' limit 1)
                        , '$screen', (SELECT id FROM product_options WHERE type = 'screen size' AND name = '$screen' limit 1)
                        , '$hdd', (SELECT id FROM product_options WHERE type = 'hdd' AND name = '$hdd' limit 1)
                        , '$ram', (SELECT id FROM product_options WHERE type = 'ram' AND value = '$ram' limit 1)
                        , '$dvd', (SELECT id FROM product_options WHERE type = 'dvd' AND name = '$dvd' limit 1), 1, '$da1', '$da1'";
					
						$args = array(
									'model'		=> $model,
									'model2'		=> $model,
									'serial'	=> $serial
                                );

                        $resultset = DB::update(DB::raw($sql), $args);
						
                        $rowid = DB::getPdo()->lastInsertId();
                    }

                    // only add monthly break point and margins if all data is valid and record has been added to database
                    if($isValid){

                        for($i = 0; $i < $columncount; $i++){   
                            //echo "<pre>";print_r($columnames);die;
                            $key    = $columnames[$i];
                            $value  = $data[$i];

                            // check that thecolumn strat with "months " -> if so then determine the monthly break point and associated margin
                            if($this->startsWith($key, 'months')){
                                //echo $key;die;   
                                if(isset($value) && is_numeric($value) && $value >= 0){
                                    //get month dates
                                    $month = str_replace('months ', '', $key);
                                    $months = explode (' - ', $month);
                                    if(isset($months) && count($months) == 2 && is_numeric($months[0]) && $months[0] >= 0 && is_numeric($months[1]) && $months[1] >= 0 ){

                                        // add item to database 
                                        $resultset = DB::update(DB::raw("INSERT INTO pup_contracts_months (`serial_number`, `month_from`, `month_to`, `margin`, `import` ) VALUES  ('".$serial."', '".$months[0]."', '".$months[1]."' , '".$value."', 1)"));
                                    }
                                }
                            }
                        }
                    } 
                        
                    array_unshift($data, $validationmessage);               
                    array_push($processrecords, $data);
                }
            }

            // remove old records			
            $resultset = DB::delete(DB::raw("DELETE FROM `pup_contracts` WHERE `import` = 0 limit 1000"));
            $resultset = DB::delete(DB::raw("DELETE FROM `pup_contracts_months` WHERE `import` = 0 limit 1000"));
            $resultset = DB::update(DB::raw("UPDATE `pup_contracts` SET `import` = 0 limit 1000"));
            $resultset = DB::update(DB::raw("UPDATE `pup_contracts_months` SET `import` = 0 limit 1000"));

            //save the processed records to file.
            $exportfile = fopen($folder.$exportfilename, "w");

            array_unshift($columnames, 'message');
            fputcsv($exportfile, $columnames);
            foreach ($processrecords as $line){
                fputcsv($exportfile, $line);
            }

            /*
            4. Update import record will export file name andretrun results to 
            */  
            //echo $num_invalid;die;
            $sql = "UPDATE `integration_imports` SET `fileexported` = '".$exportfilename."', `records_valid` = $num_valid, `records_invalid` ='".$num_invalid."'  WHERE `id` = $import_id";
            //echo $sql;die;
             $resultset = DB::update(DB::raw($sql));

            Toast::success('import has been completed.', 'Success');
            return redirect('/Variances/import_pup');
         
        } else {
            Toast::error('the imported file couldn\'t be processed!!!', 'Error');
            return redirect('/Variances/import_pup');
        }
    }


    function startsWith($haystack, $needle){
         $length = strlen($needle);
         return (substr($haystack, 0, $length) === $needle);
    }

    function endsWith($haystack, $needle){
        $length = strlen($needle);
            return $length === 0 || (substr($haystack, -$length) === $needle);
    }


    public function import($id=null){
        $IntegrationImport = IntegrationImport::find($id);
        $file_path = public_path() .'/i/'. $IntegrationImport->fileimported;
        if (file_exists($file_path)){
            return Response::download($file_path, $IntegrationImport->fileimported, [
                'Content-Length: '. filesize($file_path)
            ]);
        }else{
            Toast::error('Requested file does not exist on our server!', 'Error');
            return Redirect::back();
        }
    }

    public function export($id=null){
        $IntegrationImport = IntegrationImport::find($id);
        $file_path = public_path() .'/i/'. $IntegrationImport->fileexported;

        if (file_exists($file_path)){
            return Response::download($file_path, $IntegrationImport->fileexported, [
                'Content-Length: '. filesize($file_path)
            ]);
        }else{
            Toast::error('Requested file does not exist on our server!', 'Error');
            return Redirect::back();
        }
    }


}