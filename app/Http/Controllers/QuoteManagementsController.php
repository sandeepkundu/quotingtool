<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use DateTime;
use links;
use App\User;
use Toast;
use App\Country;
use App\Client;
use App\CountryRegion;
use App\PriceLogistic;
use App\LogisticsUnitNumber;
use App\QuoteManagement;
use App\Quote;
use App\QuoteStatus;
use App\QuoteItem;
use App\QuoteItemsDefect;
use App\ProductOption;
use App\QuotesNotes;
use View;
use App\QuoteCollectionItem;
use App\QuoteCollection;

use App\Http\Controllers\CalculationsController;

class QuoteManagementsController extends Controller {
    
    private $permission = [1];

    public function __construct() {
        $this->middleware('auth', ['except' => ['updateQuoteDetail']]);
    }

    public function index(Request $request) {

        if (!$this->filter($this->permission)) {
                   
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }

        $txt = $request->get('search');
        $limit = $request->get('perPage');
        $status = $request->get('status');
        $user_id = $request->get('user_id');
        $client_id = $request->get('client_id');
        $lim = isset($limit) ? $limit : 30;

        $QuoteManagements = Quote::select('quote.id as id', 'quote.name as name', 'quote.status', 'quote.name as ref_name', 'quote.total_value as value', 'quote.date_created as date_created', 'clients.name as client_name', 'clients.client_type as client_type', 'quote.client_id as client_id', 'users.first_name as user_name', 'users.id as user_id', 'quote_status.name as status_name', 'quote_status.id as qs_id');
        if (isset($txt)) {

            $QuoteManagements->where('quote.id', '=', $txt);
            $QuoteManagements->orwhere('clients.name', 'like', '%' . $txt . '%');
            $QuoteManagements->orWhere('quote.name', 'like', '%' . $txt . '%');
            $QuoteManagements->orWhere('users.first_name', 'like', '%' . $txt . '%');
        } elseif (isset($status)) {

            $QuoteManagements->where('quote_status.id', '=', $status);
        } elseif (isset($user_id)) {

            $QuoteManagements->where('quote.user_id', '=', $user_id);
        } elseif (isset($client_id)) {

            $QuoteManagements->where('quote.client_id', '=', $client_id);
        }

        $QuoteManagements->leftjoin('quote_status', 'quote.status', '=', 'quote_status.id');
        $QuoteManagements->leftjoin('clients', 'quote.client_id', '=', 'clients.id');
        $QuoteManagements->leftjoin('users', 'quote.user_id', '=', 'users.id');

        $QuoteManagements = $QuoteManagements->orderBy('date_created', 'desc')->paginate($lim);
        //dd($QuoteManagements);
        $QuoteStatus = QuoteStatus::where('source', '=', 'quote')->get();
        return view('QuoteManagements/index', array(
            'lim' => $lim,
            'QuoteManagements' => $QuoteManagements,
            'QuoteStatus' => $QuoteStatus
        ));

    }

    public function QuoteDetails($id = null) {

        if ($id != null) {
            $QuoteItem = QuoteItem::with('quote_name')->where('quote_id', '=', $id)->first();

            if (!empty($QuoteItem)) {

                $Quote = QuoteManagement::find($id);

                $QuoteFinacial = Quote::with('quote_item')
                        ->where('id', '=', $id)
                        ->orderBy('date_modified', 'desc')
                        ->first();
                //================= Code Showing image for uplad image================//
                $status=$QuoteFinacial->status;
                if($QuoteFinacial->status > 3){

                    if(!empty($QuoteItem->image1)) {
                        $path=public_path().$QuoteItem->image1;
                        if(file_exists($path)){
                           $image1=$QuoteItem->image1;
                        }else{
                            $image1='';
                        }
                       
                    }else{
                       $image1=''; 
                    }
                    if(!empty($QuoteItem->image2)) {
                        $path=public_path().$QuoteItem->image2;
                        if(file_exists($path)){
                            $image2=$QuoteItem->image2;
                        }else{
                             $image2='';
                        }
                       
                    }else{
                        $image2='';
                    }
                    if(!empty($QuoteItem->image3)) {
                        $path=public_path().$QuoteItem->image3;
                        if(file_exists($path)){
                            $image3=$QuoteItem->image3;
                        }else{
                            $image3='';
                        }
                       
                    }else{
                        $image3='';
                    }
                    
                }
                //================= End code Showing image for uplad image================//

				$unit_tp = $services_tp = $logistics_tp = $total_tp = $unit_oemtp = $services_oemtp = $logistics_oemtp = $total_oemtp = array(); 
				$q_unit_tp = $q_services_tp = $q_logistics_tp = $q_total_tp = $q_unit_oemtp = $q_services_oemtp = $q_logistics_oemtp = $q_total_oemtp = array(); 
				$qty = $unitp = $services = $logistic = $total_price = $net_price = $totalmargin = $ItemCount =  array();
				$QuoteItems = array();

				//retreive quote object
				$Quotes = Quote::with('Clientname', 'Username', 'quote_item','Defects','Region', 'Country')
					->where('id', '=', $id,'')
					->orderBy('date_modified', 'desc')
					->first();

				//retreive quote item objects
				$Quote_Items = QuoteItem::where('quote_id',$id)->orderBy('total_price', 'desc')->get();                
				foreach($Quote_Items as $Quote_Item){
					$items=QuoteItem::with('quote_name','model_image','option_model_image','QuoteItemsDefect')->where('id',$Quote_Item->id)->get()->toArray();
					$QuoteItems[$Quote_Item->type_name][] = $items;
				}

				$i=0;
				if(count($QuoteItems) > 0 ){
					foreach($QuoteItems as $key1 => $quotes){
						foreach($quotes as $quote){
							$ItemCount[$i]			= count($quotes);
							$i++;
							$qty[]					= $quote[0]['quantity'];
							$unitp[]				= $quote[0]['unit_prod_price'];
							$net_price[]			= $quote[0]['unit_price'];
							$total_price[]			= $quote[0]['total_price'];
							$unit_tp[]				= $quote[0]['unit_prod_price'] * $quote[0]['quantity'];
							$unit_oemtp[]			= $quote[0]['unit_f5_price'] * $quote[0]['quantity'];
							$services_tp[]			= $quote[0]['unit_f5_services'] * $quote[0]['quantity'];
							$services_oemtp[]		= $quote[0]['services_net_price'] * $quote[0]['quantity'];
							$logistics_tp[]			= $quote[0]['unit_f5_logistics'] * $quote[0]['quantity'];
							$logistics_oemtp[]		= $quote[0]['logistics_net_price'] * $quote[0]['quantity'];
							$total_tp[]				= ($quote[0]['unit_prod_price'] - $quote[0]['unit_f5_services'] - $quote[0]['unit_f5_logistics']) * $quote[0]['quantity'];
							$total_oemtp[]			= $quote[0]['total_price'];
						}
					}
				}

				$q_unit_tp = (count($unit_tp) > 0) ? array_sum($unit_tp) : 0;
				$q_unit_oemtp = (count($unit_oemtp) > 0) ? array_sum($unit_oemtp) : 0;
				$q_services_tp = (count($services_tp) > 0) ? array_sum($services_tp) : 0;
				$q_services_oemtp = (count($services_oemtp) > 0) ? array_sum($services_oemtp) : 0;
				$q_logistic_tp = (count($logistics_tp) > 0) ? array_sum($logistics_tp) : 0;
				$q_logistic_oemtp = (count($logistics_oemtp) > 0) ? array_sum($logistics_oemtp) : 0;
				$q_total_tp = (count($total_tp) > 0) ? array_sum($total_tp) : 0;
				$q_total_oemtp = (count($total_oemtp) > 0) ? array_sum($total_oemtp) : 0;

                $currency = Country::find($Quote->country);
                $Clients = Client::all();
                $countries = Country::all();
                $regions =  CountryRegion::where('country_id', '=', $Quote->country)->get();
                $Users = User::all();
                $QuotesNotes = QuotesNotes::where('quote_id', '=', $Quote->id)->get();
                $quote_status = QuoteStatus::find($Quote->status);
                $QuoteItemsDefect = QuoteItemsDefect::with('productOption')->where('quote_id', $id)->where('quote_item_id', $QuoteItem->id)->get();
  
                return view('QuoteManagements/QuoteDetails', compact(
                                'quote_status', 
								'Clients', 
								'QuoteItem', 
								'QuoteItemsDefect', 
								'Users', 
								'Quote', 
								'QuotesNotes', 
								'QuoteItems', 
								'countries', 
								'regions',
								'image1',
								'image2',
								'image3',
								'status',
								'qty',
								'unitp',
								'net_price',
								'total_price',
								'currency',
								'ItemCount',
								'Quotes',
								'q_unit_tp',
								'q_unit_oemtp',
								'q_services_tp',
								'q_services_oemtp',
								'q_logistic_tp',
								'q_logistic_oemtp',
								'q_total_tp',
								'q_total_oemtp'
                ));
            } else {
                Toast::error('No relavent data !!!.', 'Error');
                return Redirect::back();
            }
        }
    }

	public function UploadImage(Request $request) {
        if ($this->filter($this->permission)) {
            if ((isset($request->image) && !empty($request->image)) || isset($request->image2) && !empty($request->image2) || isset($request->image3) && !empty($request->image3) || isset($request->serial_number) && !empty($request->serial_number)) {
                //dd($request->all());
                if(isset($request->image)){
                    list($type, $data) = explode(';', $request->image);
                    list(, $data) = explode(',', $data);
                    $data = base64_decode($data);
                    $image_name1 = "/product/image" . time() . '1.png';
                    $path = public_path() . $image_name1;
                    file_put_contents($path, $data);
                }else{
                    $image_name1 = '';
                }

                if(isset($request->image2)){
                    list($type, $data2) = explode(';', $request->image2);
                    list(, $data2) = explode(',', $data2);
                    $data2 = base64_decode($data2);
                    $image_name2 = "/product/image" . time() . '2.png';
                    $path = public_path() . $image_name2;
                    file_put_contents($path, $data2);
                }else{
                    $image_name2 = '';
                }

                if(isset($request->image3)){
                    list($type, $data3) = explode(';', $request->image3);
                    list(, $data3) = explode(',', $data3);
                    $data3 = base64_decode($data3);
                    $image_name3 = "/product/image" . time() . '3.png';
                    $path = public_path() . $image_name3;
                    file_put_contents($path, $data3);
                }else{
                    $image_name3 = '';
                }

                $Quote = Quote::find($request->quote_id);

                $QuoteItem1 = DB::table('quote_items')->where('quote_id', '=', $request->get('quote_id'))->first();
                $QuoteItem = QuoteItem::find($QuoteItem1->id);

                $Quote->status = $Quote->status+1;
                $Quote->save();

                $QuoteItem->image1 = $image_name1;
                $QuoteItem->image2 = $image_name2;
                $QuoteItem->image3 = $image_name3;
                $QuoteItem->serial_number = $request->serial_number;
                
                if ($QuoteItem->save()) {
                    Toast::success('successfully uploaded.', 'Success');
                    return Redirect::back();
                }

            } else {
                Toast::error('Please select atleast 1 picture Or serial number !!!.', 'Error');
                return Redirect::back();
            }
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }    
    }
    

    public function delete($id = Null) {
        if ($id != null) {

            Quote::destroy($id);
            QuoteItem::where('quote_id', '=', $id)->delete();
            QuoteItemsDefect::where('quote_id', '=', $id)->delete();
            QuoteCollection::where('quote_id', '=', $id)->delete();
            QuoteCollectionItem::where('quote_id', '=', $id)->delete();
            Toast::success('Record has been deleted.', 'Success');
            return redirect('QuoteManagements/index');
        }
    }

    public function delete_quote_item($id = Null) {
        if ($id != null) {

            QuoteItem::destroy($id);
            QuoteItemsDefect::where('quote_id', '=', $id)->delete();

            QuoteCollection::where('quote_id', '=', $id)->delete();
            QuoteCollectionItem::where('quote_id', '=', $id)->delete();

            Toast::success('Record has been deleted.', 'Success');
            return redirect('QuoteManagements/index');
        }
    }

    public function multipleDelete(Request $request) {
        $a = 0;
        foreach ($request->ids as $key => $id) {
            $Quote = Quote::find($id);

            if (!empty($Quote)) {
                $Quote->delete();
                QuoteItem::where('quote_id', '=', $id)->delete();
                QuoteItemsDefect::where('quote_id', '=', $id)->delete();
                QuoteCollection::where('quote_id', '=', $id)->delete();
                QuoteCollectionItem::where('quote_id', '=', $id)->delete();
            }
            @$a++;
        }
        Toast::success('Record has been deleted. =>' . $a, 'success');
        return Redirect('QuoteManagements/index');
    }

    public function formatOutput($value, $rate = 1, $symbol = '$') {

		$rate = ($rate > 0 && strlen($symbol) > 0 && is_numeric($rate)) ?  $rate : 1;
		$symbol =  (strlen($symbol) > 0 && $rate != 1) ? $symbol : '$';

		return $symbol . number_format($value * $rate, 2, ".", ",");
    }

    public function ItemDetail(Request $request) {
        $calculate = new CalculationsController();
        $quoteItem = quoteItem::find($request->quoteItemId);
		
//$calculate->Recalculate($quoteItem->quote_id);
//echo '<pre>';
//print_r($a);
//echo '</pre>';	       

		$a = Quote::find($quoteItem->quote_id);
        $QuoteItemsDefects = QuoteItemsDefect::where('quote_id',$quoteItem->quote_id)->where('quote_item_id',$request->quoteItemId)->get()->toArray();

        $html = View::make('QuoteManagements/ItemDetail1', array(
                    'a'					=> $a,
                    'QuoteItemsDefects'	=> $QuoteItemsDefects,
					'quoteitem'			=> $quoteItem
                        )
                )->render();

        echo $html;
        die;
    }



    public function updateQuoteDetail(Request $request) {
        $Quotes = Quote::where('status', '=', 1)->get();

        if (count($Quotes) > 0) {
			$calculate = new CalculationsController();

            foreach ($Quotes as $Quote) {
				$calculate->Recalculate($Quote->id);
			}

            /*
            $arr = array(
                'status' => 'success',
                'message' => 'All inprogress quote has been updated '
            );
            echo "success fully updated.";
            */
            exit();
        }
    }

    public function updateClientFinancial(Request $request){
        $Quote = Quote::find($request->id);
		if(null === $Quote){ return; }

        $Quote->name = $request->name;        

		$country = Country::find($request->country);
		if(null !== $country){
			$currency_symbol = $country->currency_symbol;			 

            if($request->currency_symbol == 'US$'){                
				$currency_symbol = '$';
            } 

			$Quote->country = $request->country;
			$Quote->currency_symbol = $currency_symbol; 
		}
		
		if(isset($request->currency_rate) && is_numeric($request->currency_rate) && $request->currency_rate != $Quote->currency_rate){
			$Quote->currency_rate = $request->currency_rate; 
		}
		 
		/* 
		flag == 1 => Lock currency only
		flag == 2 => Lock item margins only
		flag == 3 => Lock all adjustable margins
		flag == 4 => Lock everything
		*/
		if(isset($request->lockpricing) && is_numeric($request->lockpricing) && $request->lockpricing > 0){
			$Quote->flag = $request->lockpricing; 
		} else {
			$Quote->flag = 0;
		}

		//get the current oem margin detail
        $oemp = $Quote->margin_oem_product; 
        $oeml = $Quote->margin_oem_logistic; 
        $oems = $Quote->margin_oem_servicee; 

		//check if the submitted oem values are different from current oem value and only update if they have changed
		//also apply updated oem information to all items
		if(isset($request->margin_oem_product) && is_numeric($request->margin_oem_product) && $request->margin_oem_product != $oemp){
			$Quote->margin_oem_product = $request->margin_oem_product; 
			QuoteItem::where('quote_id', '=', $Quote->id)->update(['unit_margin_oem_product' => $request->margin_oem_product]);
		}
		
		if(isset($request->margin_oem_logistic) && is_numeric($request->margin_oem_logistic) && $request->margin_oem_logistic != $oeml){
			$Quote->margin_oem_logistic = $request->margin_oem_logistic; 
			QuoteItem::where('quote_id', '=', $Quote->id)->update(['logistics_oem_margin' => $request->margin_oem_logistic]);
		}

		if(isset($request->margin_oem_service) && is_numeric($request->margin_oem_service) && $request->margin_oem_service != $oems){
			$Quote->margin_oem_service = $request->margin_oem_service; 
			QuoteItem::where('quote_id', '=', $Quote->id)->update(['services_oem_margin' => $request->margin_oem_service]);
		}	

		if(isset($request->margin_brand) && is_numeric($request->margin_brand)){
			QuoteItem::where('quote_id', '=', $Quote->id)->update(['unit_margin_brand' => $request->margin_brand]);
		}	
		if(isset($request->margin_country) && is_numeric($request->margin_country)){
			QuoteItem::where('quote_id', '=', $Quote->id)->update(['unit_margin_country' => $request->margin_country]);
		}	
		if(isset($request->margin_screen) && is_numeric($request->margin_screen)){
			QuoteItem::where('quote_id', '=', $Quote->id)->update(['unit_margin_screen' => $request->margin_screen]);
		}	
		if(isset($request->margin_customer) && is_numeric($request->margin_customer)){		
			QuoteItem::where('quote_id', '=', $Quote->id)->update(['unit_margin_customer' => $request->margin_customer]);
		}	
		if(isset($request->margin_Service) && is_numeric($request->margin_Service)){		
			QuoteItem::where('quote_id', '=', $Quote->id)->update(['unit_margin_services' => $request->margin_Service]);
			QuoteItem::where('quote_id', '=', $Quote->id)->update(['services_margin' => $request->margin_Service]);
		}	
		if(isset($request->margin_logistics) && is_numeric($request->margin_logistics)){		
			QuoteItem::where('quote_id', '=', $Quote->id)->update(['logistics_margin' => $request->margin_logistics]);
			QuoteItem::where('quote_id', '=', $Quote->id)->update(['unit_margin_logistics' => $request->margin_logistics]);
		}
		if(isset($request->margin_logisticsp) && is_numeric($request->margin_logisticsp)){		
			QuoteItem::where('quote_id', '=', $Quote->id)->update(['logistics_product_margin' => $request->margin_logisticsp]);
		}
		
        if($Quote->save()){
			$calculate = new CalculationsController();
			$calculate->Recalculate($Quote->id);

            Toast::success('Record has been updated.', 'Success');
            return redirect('QuoteManagements/QuoteDetails/'.$Quote->id);
        }
    }


	public function updatePriceMarginAdjustment(Request $request){
		$quoteItemId = $request->quoteItemId;

		if(isset($quoteItemId) && !empty($quoteItemId)){
			$quoteItem = QuoteItem::select('id','logistics_margin','services_margin','unit_margin_brand','unit_margin_country','unit_margin_screen','logistics_product_margin','unit_margin_customer')->where('id',$quoteItemId)->first()->toArray();
			return response()->json(array('success'=>200,'quoteItem'=>$quoteItem));
		}
	}

	public function savePriceMarginAdjustment(Request $request){
		$quoteItemId = $request->item_id;
		if(isset($quoteItemId) && !empty($quoteItemId) && is_numeric($quoteItemId)){
			$QuoteItem = QuoteItem::find($quoteItemId);

			if(null !== $QuoteItem){

				$QuoteItem->unit_margin_brand = (!empty($request->brand_margin) && is_numeric($request->brand_margin)) ? $request->brand_margin : $QuoteItem->unit_margin_brand; 
				$QuoteItem->unit_margin_country = (!empty($request->country_margin) && is_numeric($request->country_margin)) ? $request->country_margin : $QuoteItem->unit_margin_country;
				$QuoteItem->unit_margin_screen = (!empty($request->screen_margin) && is_numeric($request->screen_margin)) ? $request->screen_margin : $QuoteItem->unit_margin_screen;
				$QuoteItem->unit_margin_logistics = (!empty($request->logistic_margin) && is_numeric($request->logistic_margin)) ? $request->logistic_margin : $QuoteItem->unit_margin_logistics;
				$QuoteItem->logistics_margin = (!empty($request->logistic_margin) && is_numeric($request->logistic_margin)) ? $request->logistic_margin : $QuoteItem->logistics_margin;
				$QuoteItem->logistics_product_margin = (!empty($request->logistic_product_margin) && is_numeric($request->logistic_product_margin)) ? $request->logistic_product_margin : $QuoteItem->logistics_product_margin;
				$QuoteItem->unit_margin_services = (!empty($request->service_margin) && is_numeric($request->service_margin)) ? $request->service_margin : $QuoteItem->unit_margin_services;
				$QuoteItem->services_margin = (!empty($request->service_margin) && is_numeric($request->service_margin)) ? $request->service_margin : $QuoteItem->services_margin;
				$QuoteItem->unit_margin_customer = (!empty($request->client_margin) && is_numeric($request->client_margin)) ? $request->client_margin : $QuoteItem->unit_margin_customer;
				
				if($QuoteItem->save()){
					//if quote flag is set to no recal then need to remove it recaluate the item and then set it again
					/*
					$quote = Quote::find($QuoteItem->quote_id);
					$flag = $quote->flag;
					if($flag > 1){
						$quote->flag = 0;
						$quote->save(); 
					} 
					*/

					//recalculate the item 
					$calculate = new CalculationsController();
					$calculate->Recalculate($QuoteItem->quote_id, $QuoteItem->id);

					//reset the flag
					/*
					if($flag > 1){
						$quote->flag = $flag;
						$quote->save(); 
					} 	
					*/			

					Toast::success('Successfully updated.', 'Success');
					return redirect('QuoteManagements/QuoteDetails/'.$QuoteItem->quote_id);
				}				
			} else {			
				Toast::success('SQuote item id is invalid.', 'Error');
				return redirect('QuoteManagements/QuoteDetails/'.$QuoteItem->quote_id);
			}
		} else {			
			Toast::success('Quote item id is invalid.', 'Error');
			return redirect('QuoteManagements/QuoteDetails/'.$QuoteItem->quote_id);
		}
	}

}
