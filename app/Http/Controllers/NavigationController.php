<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Session;
use Toast;
use DateTime;
use links;
use App\Navigation;
use URL;

class NavigationController extends Controller {

     private $permission = [1];

    public function __construct() {
        $this->middleware('auth');
    }

    public function index(Request $request) {
        if ($this->filter($this->permission)) {
            Input::flash();
            $txt = $request->get('search');
            $limit = $request->get('perPage');
            if (isset($limit)) {
                $lim = $limit;
            } else {
                $lim = 30;
            }

            $Navigations = Navigation::select('*');
            if (isset($txt)) {
                $Navigations->where('name', 'like', '%' . $txt . '%');
                $Navigations->orWhere('display_name', 'like', '%' . $txt . '%');
            }
            $Navigations = $Navigations->orderBy('position', 'asc')->paginate($lim);
            return view('Navigation.index', compact('Navigations', 'lim'));
        } else {
           Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');

        }
    }

    public function addNavigation() {
        if ($this->filter($this->permission)) {
            return view('Navigation.addNavigation');
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');

        }
    }

    public function insertNavigation(Request $request) {
        
        if ($this->filter($this->permission)) {
            $this->validate($request, array(
                'name' => 'required|min:3',
                'display_name' => 'required|min:3',
                //'position' => 'required|numeric|unique:navigations,position',
                'position' => 'required|numeric',
                'section' => 'required'
            ));
            $Navigation = new Navigation();
            if ($request->status == 'on') {
                $status = 'Active';
            } else {
                $status = 'Disabled';
            }
            $description = $request->input('page_content');
            if (!empty($description)) {
                $dom = new \DomDocument();
                $dom->loadHtml($description, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                $images = $dom->getElementsByTagName('img');
                foreach ($images as $k => $img) {
                    $data = $img->getAttribute('src');
                    list($type, $data) = explode(';', $data);
                    list(, $data) = explode(',', $data);
                    $data = base64_decode($data);
                    $image_name = "/product/" . time() . $k . '.png';
                    $path = public_path() . $image_name;
                    file_put_contents($path, $data);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', URL::to('/') . $image_name);
                }
                $description = $dom->saveHTML();
            }

            $old_name = $request->name;
            $name = str_replace(" ", "_", $old_name);
            $Navigation->fill($request->all());
            $Navigation->name = $name;
            $Navigation->status = $status;
            $Navigation->page_content = $description;

            if ($Navigation->save()) {
                Toast::success('Record has been saved.', 'Success');
                return redirect('/Navigation/index');
            } else {
                Toast::error('Record not saved', 'Error');
                return redirect()->route('/Navigation/index');
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');

        }
    }

    public function editNavigation($id = Null) {
        if ($this->filter($this->permission)) {
            if ($id != Null) {

                $Navigation = Navigation::find($id);
                if (!empty($Navigation)) {
                    return view('Navigation.editNavigation', compact('Navigation'));
                } else {
                    Toast::error('Invalid id, Please try again!', 'Error');
                    return redirect('Navigation/index');
                }
            } else {
                Toast::error('Invalid id, Please try again!', 'Error');
                return redirect()->route('Navigation/index');
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');

        }
    }

    public function updateNavigation(Request $request) {
        if ($this->filter($this->permission)) {
            $Navigation = Navigation::find($request->id);
            $this->validate($request, array(
                'name' => 'required|min:3',
                'display_name' => 'required|min:3',
                //'position' => 'required|numeric|unique:navigations,position,' . $Navigation->id,
                'section' => 'required'
            ));

            if ($request->status == 'on') {
                $status = 'Active';
            } else {
                $status = 'Disabled';
            }
            $old_name = $request->name;
            $name = str_replace(" ", "_", $old_name);

            $Navigation->fill($request->all());
            if (!empty($request->page_content)) {
                $description = $request->input('page_content');
                $dom = new \DomDocument();
                $dom->loadHtml($description, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                $images = $dom->getElementsByTagName('img');
                foreach ($images as $k => $img) {
                    $data = $img->getAttribute('src');
                    if (strpos($data, 'data:image') !== false) {
                        list($type, $data) = explode(';', $data);
                        list(, $data) = explode(',', $data);
                        $data = base64_decode($data);
                        $image_name = "/product/" . time() . $k . '.png';
                        $path = public_path() . $image_name;
                        file_put_contents($path, $data);
                        $img->removeAttribute('src');
                        $img->setAttribute('src', URL::to('/') . $image_name);
                    }
                }
                $description = $dom->saveHTML();
            } else {
                $description = '';
            }
            $Navigation->status = $status;
            $Navigation->name = $name;
            $Navigation->page_content = $description;
            $Navigation->section = $request->section;

            if ($Navigation->save()) {
                Toast::success('Record has been updated.', 'Success');
                return redirect('/Navigation/index');
            } else {
                Toast::error('Record not saved', 'Error');
                return redirect()->route('/Navigation/index');
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');

        }
    }

    public function multipleDelete(Request $request) {
        if ($this->filter($this->permission)) {
            if (Navigation::destroy($request->get('ids'))) {
                Toast::success('Records has been deleted.', 'Success');
                return Redirect('Navigation/index');
            } else {
                Toast::error('Please select any record.', 'Error');
                return Redirect('Navigation/index');
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id = Null) {
        if ($this->filter($this->permission)) {
            $Navigation = Navigation::find($id);
            if (!empty($Navigation)) {
                $Navigation->delete();
                Toast::success('Records has been deleted.', 'Success');
                return Redirect('Navigation/index');
            } else {
                Toast::error('Invalid id, Please try again!', 'Error');
                return Redirect('Navigation/index');
            }
        } else {
           Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');

        }
    }

}
