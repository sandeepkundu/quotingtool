<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Session;
use DateTime;
use links;
use Toast;
use App\PriceMarginOEM;
use App\PriceMarginServices;
use App\ProductOption;
use App\Clients;
use App\Country;
use App\Quote;
use App\QuoteItem;
use App\QuoteItemsDefect;
use App\QuotesNotes;
use App\Http\Controllers\CalculationsController;
use App\Http\Controllers\ClientsController;


class PriceMarginServicesController extends Controller {

    private $permission = [1];

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request) {
        if ($this->filter($this->permission)) {
            Input::flash();
           
            $services = PriceMarginServices::with('ProductOtionType','ProductOtionServices', 'CountryServices','Clientname')->orderBy('id','desc')->get();

            return view('/priceServices/index', array('services' => $services));
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addPriceServices(Request $request) {

        if ($this->filter($this->permission)) {
             
            $types = ProductOption::where('type', '=', 'Product')->get();
            $option = ProductOption::where('type', '=', 'Services')->get();
            $countries = Country::where('status', '=', 'active')->get();
            
            $clients = new ClientsController();
            $cids = Array();
            $parent_ids = $clients->getTree($cids, 0, 0, '0', '', '');
             
            return view('priceServices/addPriceServices', array('parent_ids' => $parent_ids, 'types' => $types, 'option' => $option, 'countries' => $countries));
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }
   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function insertPriceServices(Request $request) {
        if ($this->filter($this->permission)) {
            Input::flash();
            $this->validate($request, array(
                'type_id' => 'required',
                'value' => 'required|numeric',
                'option_id' => 'required'
            ));
            if ($request->status == 'on') {
                $status = 'active';
            } else {
                $status = 'inactive';
            }

            $save = PriceMarginServices::Create(array(
                        'client_id' => $request->parent_id,
                        'type_id' => $request->type_id,
                        'value' => $request->value,
                        'option_id' => $request->option_id,
                        'status' => $status,
                        'country_id' => (isset($request->country_id) && is_numeric($request->country_id) && $request->country_id > 0) ? $request->country_id : 0
            ));

            if ($save) {
                $this->recalcInProgresQuotes();

                Toast::success('Record has been Saved.', 'Success');
                return redirect('priceServices/index');
            } else {
                Toast::error('Record not saved. Id not found.', 'Error');
                return redirect('priceServices/index');
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editPriceServices($id = Null) {
        if ($this->filter($this->permission)) {
            if ($id != null) {
                //$types = ProductOption::distinct()->get(['type']);
                $types = ProductOption::where('type', '=', 'Product')->get();
                $option = ProductOption::where('type', '=', 'Services')->get();
				$countries = Country::where('status', '=', 'active')->get();
                
                $clients = new ClientsController();
            $cids = Array();
            $parent_ids = $clients->getTree($cids, 0, 0, '0', '', '');
                $services = PriceMarginServices::find($id);
                if ($services) {
                    return view('/priceServices/editPriceServices', compact('oem', 'parent_ids', 'option', 'types', 'services', 'countries'));
                } else {
                    Toast::error('Id Not Found', 'Error');
                    return Redirect('priceServices/editPriceServices');
                }
            } else {
                Toast::error('Id Not Found', 'Error');
                return Redirect('priceServices/editPriceServices');
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatePriceServices(Request $request) {
        if ($this->filter($this->permission)) {
            Input::flash();
            $this->validate($request, array(
                'type_id' => 'required',
                'value' => 'required|numeric',
                'option_id' => 'required'
            ));
            $services = PriceMarginServices::find($request->input('id'));
            if (!empty($services)) {
                if ($request->get('status') != null) {
                    $services->status = 'active';
                } else {
                    $services->status = 'inactive';
                }
                $services->client_id = $request->parent_id;
                $services->type_id = $request->get('type_id');
                $services->value = $request->get('value');
                $services->option_id = $request->get('option_id');
				$services->country_id = (isset($request->country_id) && is_numeric($request->country_id) && $request->country_id > 0) ? $request->country_id : 0;

                if ($services->save()) {
					$this->recalcInProgresQuotes();
                    
                    Toast::success('Record has been updated.', 'Success');
                    return Redirect('/priceServices/index');
                } else {
                    Toast::error('Product not updated. Please try again.', 'Error');
                    return Redirect('/priceServices/index');
                }
            }
        } else {
           Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id = Null) {
        if ($this->filter($this->permission)) {
            $services = PriceMarginServices::find($id);
            if (!empty($services)) {
                $services->delete();
				$this->recalcInProgresQuotes();

                Toast::success('Records has been deleted.', 'Success');
                return Redirect('/priceServices/index');
            } else {
                Toast::error('Invalid Id. Please try Again!', 'Error');
                return Redirect('/priceServices/index');
            }
        } else {
           Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function multipleDelete(Request $request) {
        if ($this->filter($this->permission)) {
            if (PriceMarginServices::destroy($request->get('ids'))) {

				$this->recalcInProgresQuotes();

                Toast::success('Records has been deleted.', 'Success');
                return Redirect('/priceServices/index');
            } else {
                Toast::error('Please select any record.', 'Error');
                return Redirect('/priceServices/index');
            }
        } else {
           Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

	private function recalcInProgresQuotes($service_id = 0){
		//need to recalulate all inprogress quotes.			dd
		$Quotes = Quote::where('status', '=', '1')->get();
		if(count($Quotes) >0 ){
			foreach($Quotes as $Quote){
               
				$calculate = new CalculationsController();
				$calculate->Recalculate($Quote->id);
 
				$QuotesNotes = new QuotesNotes();
				$QuotesNotes->quote_id = $Quote->id;
				$QuotesNotes->user_id = Auth::user()->id;
				$QuotesNotes->display_level = 1;
				$QuotesNotes->note = 'Admin margin adjustment change - recalculation of in-progress quote.';
				$QuotesNotes->save();

			}
		}
	}

}
