<?php
namespace App\Http\Controllers;

ini_set('max_execution_time', 600);

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Session;
use DateTime;
use links;
use Toast;
use App\ProductOption;
use View;
use App\IntegrationImport;
use App\PriceBands;
use App\PriceFactorBands;
use App\ProductFactor;
use App\CountryRegion;
use App\LogisticsUnitNumber;
use App\PriceLogistic;
use App\PriceMarginAdjustment;
use App\Quote;
use App\QuoteItem;
use App\QuoteItemsDefect;
use App\QuotesNotes;
use App\Http\Controllers\CalculationsController;

class MarginAdjustmentUploadImportController extends Controller
{
	private $permission = [1];
	public function __construct() {
		$this->middleware('auth');
		date_default_timezone_set('Asia/Kolkata');
	}
	public function index(){
		if (!$this->filter($this->permission)) {
                   
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }

		$IntegrationImports = IntegrationImport::where('source','=','priceadjustments')->orderBy('date','DESC')->get();
		//dd($IntegrationImports);
		return view('Variances/MarginAdjustmentsImport',array(
			'IntegrationImports'=> $IntegrationImports,
			));

	}
	public function store(Request $request){

		$debug = false;
		$folder         =  public_path().'/i/';
		$datestring = date_format(new DateTime, 'Y-m-d-H-i-s');
		$importfilename = 'priceadjustments-import-' . $datestring . '.csv';
		$exportfilename = 'priceadjustments-export-' . $datestring . '.csv';

		/*
			1. Save uploaded file to server 
		*/	
		if (!isset($_FILES["fileToUpload"])) {

			Toast::error('A csv file must be selected for upload', 'Error');
			return redirect('/Variances/price-margin-import-view');
		}
		if ($_FILES["fileToUpload"]["error"] > 0) {

			Toast::error('A error was encountered uploading this file(' . $_FILES["fileToUpload"]["error"] . ')', 'Error');
			return redirect('/Variances/price-margin-import-view');
		}
		$ftype = explode(".",$_FILES['fileToUpload']['name']);
		if(strtolower(end($ftype)) !== 'csv'){

			Toast::error('only a csv can be uploaded', 'Error');
			return redirect('/Variances/price-margin-import-view');
		}
		if(move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $folder. $importfilename) === FALSE){

			Toast::error('File could not be saved', 'Error');
			return redirect('/Variances/price-margin-import-view');
		}

		/*
			2. Create a new import record 
		*/
		$IntegrationImport               = new IntegrationImport();
		$IntegrationImport->date         = date('Y-m-d-H-i-s');
		$IntegrationImport->fileimported = $importfilename;
		$IntegrationImport->source       = 'priceadjustments';
		$IntegrationImport->save();

		$import_id 						= $IntegrationImport->id;

		//reset all records so that none are flagged as being updated
		DB::update(DB::raw("UPDATE `price_margin_adjustments` SET processed = 0"));

		/*
			3. Process the file 
		*/
		$row = 0;
		$columncount = 0;
		$processrecords = array();
		$columnames = array();
		$isValid = FALSE;
		$num_invalid = 0;
		$num_valid = 0;

		if (($handle = fopen($folder. $importfilename, "r")) !== FALSE) {

			while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
				$row ++;
				if($row == 1){
					$columncount = count($data);
					$columnames = $data;
				} else if (count($data)  !=  8){
				// incorrect number of columns for the row
					Toast::error('Record has incorrect number of columns ', 'Error');
					return redirect('/Variances/price-margin-import-view');

					array_unshift($data, "[record has incorrect number of columns (".  count($data) . ")]");				
					array_push($processrecords, $data);
					$num_invalid ++;
				} else {
				$adj		= $data[0];		// Adjustment Type		e.g. country
				$type		= $data[1];		// Product Type			e.g. Notebook				
				$country	= $data[2];		// Country				e.g. New Zealand							
				$brand		= $data[3];		// Brand				e.g. Apple	
				$screen		= $data[4];		// Screen Size			e.g. 15"
				$client		= $data[5];		// Client				e.g. HP India store A		
				$value		= $data[6];		// Margin Amount %		e.g. 20
				$status		= $data[7];		// Status				e.g. active	

				// validate the data
				$validationmessage = '';
				$isValid = FALSE;

				if(!isset($adj) || ($adj !== 'country' && $adj !== 'client' && $adj !== 'logistics' && $adj !== 'logistic product' && $adj !== 'defectcap' && $adj !== 'service' && $adj !== 'brand'  && $adj !== 'screen' )){
					$validationmessage .= "[the adjustment type is not valid]";
				}	
				if($adj === 'country' && (!isset($country) || $country === '')){
					$validationmessage .= "[a country is required for the adjustment type of country]";
				}	
				if($adj === 'brand' && (!isset($brand) || $brand === '')){
					$validationmessage .= "[a brand is required for the adjustment type of brand]";
				}	
				if($adj === 'screen' && (!isset($screen) || $screen === '')){
					$validationmessage .= "[a country is required for the adjustment type of country]";
				}		
				if($adj === 'client' && (!isset($client) || $client === '')){
					$validationmessage .= "[a client name is required for the adjustment type of client]";
				}	
				if($adj === 'logistic product' && (!isset($type) || $type === '')){
					$validationmessage .= "[a product type is required for the adjustment type of logistics product]";
				}
				if(!is_numeric($value)){
					$validationmessage .= "[value must be a number]";
				}

				if($status !== 'active'){ $status = 'disabled'; }

				if($debug){
					echo '<pre>';
					print_r($data);
					echo '</pre>';
				}
				
				// vailidate model and device components

				$args = array(
					'client' => (isset($client) and strlen($client) > 0) ? $client : 'default', 
					'type' => $type, 
					'screen' => $screen,
					'country' => $country,
					'brand' => $brand
					);

					// vailidate model and device components

				$resultset = DB::select(DB::raw("SELECT 
					IFNULL((SELECT id FROM clients WHERE `name` = :client), 0) AS client,
					IFNULL((SELECT id FROM product_options WHERE type = 'product' AND name = :type), 0) AS type,
					IFNULL((SELECT id FROM product_options WHERE type = 'screen size' AND name = :screen), 0) AS screen,
					IFNULL((SELECT id FROM country WHERE name = :country), 0) AS country,
					IFNULL((SELECT id FROM product_options WHERE type = 'brand' AND `name` = :brand), 0) AS brand"), $args);
				
				if(!empty($resultset)){

					if($debug){
						echo '<pre>';
						print_r($resultset);
						echo '</pre>';
					}
					//continue;

					$type_id = $resultset[0]->type;
					$brand_id = $resultset[0]->brand;
					$screen_id = $resultset[0]->screen;
					$country_id = $resultset[0]->country;
					$client_id = $resultset[0]->client;

					if($adj === 'logistic product' && $type_id === 0) {
						$validationmessage .= "[product type lookup not found]";
					}
					if($adj === 'screen' && $screen_id === 0) {
						$validationmessage .= "[screen lookup not found]";
					}
					if($adj === 'brand' && $brand_id === 0) {
						$validationmessage .= "[brand lookup not found]";
					}
					if($adj === 'country' && $country_id === 0) {
						$validationmessage .= "[country lookup not found]";
					}

					if($client_id === 0 && $client !== 'default') {
						$validationmessage .= "[client not found]";
					}
					
					if($debug){ echo "message = $validationmessage<br>"; }

					//don't continue if there are any validation errors
					if($validationmessage === '') {

						$args = array(
							'type'		=> $type_id,
							'county'	=> $country_id,
							'adj'		=> $adj,
							'client'	=> $client_id,
							'brand'		=> $brand_id,
							'screen'	=> $screen_id
						);

						$resultset1 = DB::select(DB::raw("SELECT * 
							FROM `price_margin_adjustments` 
							WHERE type_id = :type 
							AND client_id = :client 
							AND country_id = :county 
							AND margin_type = :adj 
							AND brand_id = :brand
							AND screen_id = :screen
							ORDER BY `id` DESC LIMIT 1"), $args);						

						if(count($resultset1) > 0){
						
							//exist so only update processed
							$id = $resultset1[0]->id; 
							$PriceMarginAdjustment = PriceMarginAdjustment::find($id);
							$PriceMarginAdjustment->value = $value;
							$PriceMarginAdjustment->processed = 1;
							$PriceMarginAdjustment->status = $status;
							$PriceMarginAdjustment->save();
							$validationmessage .= "[updated]";
							$isValid = TRUE; 						
							
						}else{
							//add
							$PriceMarginAdjustment = new PriceMarginAdjustment();
							$PriceMarginAdjustment->client_id = $client_id;
							$PriceMarginAdjustment->country_id = $country_id;
							$PriceMarginAdjustment->type_id = $type_id;
							$PriceMarginAdjustment->brand_id = $brand_id;
							$PriceMarginAdjustment->screen_id = $screen_id;
							$PriceMarginAdjustment->margin_type = $adj;
							$PriceMarginAdjustment->value = $value;
							$PriceMarginAdjustment->status = $status;
							$PriceMarginAdjustment->processed = 1;
							$PriceMarginAdjustment->save();

							$validationmessage .= "[added]";
							$isValid = TRUE; 
						}
					}

				} else {
					$validationmessage .= "[error occurred validating model]";
				}

				if($isValid){
					$num_valid ++;
				} else {
					$num_invalid ++;
				}

				array_unshift($data, $validationmessage);				
				array_push($processrecords, $data);
			}
		}


		$resultset2 = DB::select(DB::raw("SELECT 
			'[deleted]' AS message,
			pf.margin_type AS margin,
			typ.name AS type,
			cty.name AS `country`,
			brand.name AS `brand`,
			sc.name AS `screen`,
			c.name AS `client`,
			pf.value AS `value`,
			pf.`status` AS `status`
			FROM `price_margin_adjustments` pf
			LEFT JOIN `country` cty ON cty.id = pf.country_id
			LEFT JOIN `product_options` typ ON pf.type_id = typ.id 
			LEFT JOIN `product_options` brand ON pf.brand_id = brand.id AND brand.type = 'Brand' 
			LEFT JOIN `product_options` sc ON pf.screen_id = sc.id AND sc.type = 'Screen Size'
			LEFT JOIN `clients` c ON c.id = pf.client_id
			WHERE processed = 0"));

			if(!empty($resultset2)){
				foreach ($resultset2 as $row) {
					//add each record to array for export in csv
					array_push($processrecords, 
						array (
							$row->message,
							$row->margin,
							$row->type,
							$row->country,
							$row->brand,
							$row->screen,
							$row->client,
							$row->value,
							$row->status,
							)
						);					
				}
			}

			//remove all records not updated
			DB::delete(DB::raw("DELETE FROM `price_margin_adjustments` WHERE processed = 0"));

			//save the processed records to file.
			$exportfile = fopen($folder.$exportfilename, "w");

			array_unshift($columnames, 'message');
			fputcsv($exportfile, $columnames);
			foreach ($processrecords as $line){
				fputcsv($exportfile, $line);
			}

			/*
			4. Update import record will export file name andretrun results to 
			*/
			$IntegrationImport                	= IntegrationImport::find($import_id);
			$IntegrationImport->fileexported  	= $exportfilename;
			$IntegrationImport->records_valid 	= $num_valid;
			$IntegrationImport->records_invalid = $num_invalid;
			$IntegrationImport->save();

			//need to recalulate all inprogress quotes.
			$Quotes = Quote::where('status', '=', '1')->get();
			if(count($Quotes) >0 ){
				foreach($Quotes as $Quote){
					$calculate = new CalculationsController();
					$calculate->Recalculate($Quote->id);

					$QuotesNotes = new QuotesNotes();
					$QuotesNotes->quote_id = $Quote->id;
					$QuotesNotes->user_id = Auth::user()->id;
					$QuotesNotes->display_level = 1;
					$QuotesNotes->note = 'Margin Adjustment bulk import - recalculation of in-progress quotes.';
					$QuotesNotes->save();
				}
			}

			Toast::Success('Import has been completed', 'Success');
			return redirect('/Variances/price-margin-import-view');

		} else {
			Toast::error('The imported file couldn\'t be processed', 'Error');
			return redirect('/Variances/price-margin-import-view');
		}
	}

}
