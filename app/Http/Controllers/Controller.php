<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use Auth;
use App\PermissionRole;
use App\RoleUser;
use App\Permission;
use App\User;
use Toast;

class Controller extends BaseController {

	use AuthorizesRequests,
	DispatchesJobs,
	ValidatesRequests;


	public function filter($permision) {
		try{
			
			$id = Auth::user()->id;
			$user = User::find($id);
		
		$user->last_url = url()->current();
		//dd($user);
		$user->save();
		


			$role_id = RoleUser::select('role_id')->where('user_id', '=', $id)->first();
		
			$PermissionRoles = PermissionRole::whereIn('permission_id',$permision)->where('role_id',$role_id->role_id)->get();

			if(count($PermissionRoles) > 0){
				foreach ($PermissionRoles as $PermissionRole) {
					if (!empty($PermissionRole->id)) {
						$User = User::find($id);
						$User->last_url = url()->current();
						//$user->save();

						
						if($User->save()){
							return true;
						}
						
					} else {

						//dd($PermissionRoles);

						return false;
					}
				}

			}else{
				return false;		
			}

			dd($User);
		}catch (Exception $e ){
			return false;
		}
	}

	public function checkRoleManager($id=null) {
        try{
    		if (Auth::user()->id != null && Auth::check()) {		
    
    			$role_id = RoleUser::select('role_id')->where('user_id', '=', $id)->first();
    
    			if ($role_id != null) {
    
    				$permission_roles = PermissionRole::where('role_id', '=', $role_id->role_id)->get();
    
    				$Permissions = Permission::all();
    
    				$perm = array();
    				$roles = array();
    
    				foreach ($permission_roles as $permission_role) {
    					$roles[] = $permission_role->permission_id;
    				}
    
    				foreach ($Permissions as $Permission) {
    					$perm[] = $Permission->id;
    				}
    
    				foreach ($roles as $role) {
    					if (in_array($role, $perm)) {
    						return true;
    					} else {
    						return false;
    					}
    				}
    			} else {
    				return false;
    			}
    		} else {
    			return false;
    		}
        }catch (Exception $e ){
			return false;
		}
	}

}
