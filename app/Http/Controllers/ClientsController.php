<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use DateTime;
use links;
use App\User;
use Toast;
use App\Country;
use App\Client;
use App\CountryRegion;
use App\QuoteManagement;
use App\ClientUser;
use App\ClientCountryRate;
use App\Quote;

class ClientsController extends Controller {

      private $permission = [1];

    public function __construct() {
        $this->middleware('auth');
       // $this->middleware('checkRole');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        if ($this->filter($this->permission)) {
            $Client = Client::with('countryClient', 'Region')->where('id', '!=', 0)->orderBy('fullpath', 'ASC')->get();
            return view('clients/index', array('clients' => $Client));
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addClient() {
        if ($this->filter($this->permission)) {
            $countries = Country::all();
			$parent_ids = array();
			$parent_ids = $this->getTree($parent_ids, 0, 0, '0', '', '');
            return view('clients/addClient', compact('countries', 'parent_ids'));
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function insertClient(Request $request) {
        if ($this->filter($this->permission)) {
            Input::flash();
            $this->validate($request, array(
                'name' => 'required|min:3',
                'client_type' => 'required',
                'country' => 'required',
                'region' => 'required',
                'ave_collect_size' => 'nullable|numeric|between:0,2147483646',
                'margin_oem_product' => 'nullable|numeric|between:0,9999999999.99',
                'margin_oem_logistic' => 'nullable|numeric|between:0,9999999999.99',
                'margin_oem_service' => 'nullable|numeric|between:0,9999999999.99'
            ));

            $Client = new Client();
            $Client->fill($request->all());
            if ($request->get('status') != null) {
                $Client->status = 'active';
            } else {
                $Client->status = 'inactive';
            }

			if (isset($request->ave_collect_size) && is_numeric($request->ave_collect_size)) {
                $Client->ave_collect_size = $request->get('ave_collect_size');
            } else {
				$Client->ave_collect_size = null;
			}
			if (isset($request->margin_oem_product) && is_numeric($request->margin_oem_product)) {
                $Client->margin_oem_product = $request->get('margin_oem_product');
            } else {
				$Client->margin_oem_product = null;
			}
			if (isset($request->margin_oem_logistic) && is_numeric($request->margin_oem_logistic)) {
                $Client->margin_oem_logistic = $request->get('margin_oem_logistic');
            } else {
				$Client->margin_oem_logistic = null;
			}
			if (isset($request->margin_oem_service) && is_numeric($request->margin_oem_service)) {
                $Client->margin_oem_service = $request->get('margin_oem_service');
            } else {
				$Client->margin_oem_service = null;
			}


            if ($Client->save()) {
				//resort all clients
				$this->updateOrder();
				
                Toast::success('Record has been saved.', 'Success');
                return redirect('/clients/index');
            } else {
                Toast::error('Record not saved', 'Error');
                return redirect()->route('/clients/index')->withInput();
            }
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function country_region(Request $request) {
        if ($this->filter($this->permission)) {
            if (!empty($request->input('id'))) {
                $country_region = DB::table('country_region')->where('country_id', '=', $request->input('id'))->get();
                echo json_encode(array('country_region' => $country_region));
                die;
            }
        } else {
          
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function editClient(Request $request, $id = null) {
        if ($this->filter($this->permission)) {
            if ($id != null) {
                 $arr = $array = $arr1 = array();
                $clients = Client::with('countryClient')->find($id);
                $countries = Country::all();

				$parent_ids = array();
				$parent_ids = $this->getTree($parent_ids, 0, 0, '0', '', '');

                if (isset($clients->countryClient->id)) {
                    $country_region = CountryRegion::where('country_id', '=', $clients->countryClient->id)->get();
                } else {
                    $country_region = array();
                }
                $ClientCountryRates = ClientCountryRate::select('country.name as country_name','client_country_rates.*')
                ->join('country','country.id','=','client_country_rates.country_id')
                ->where('client_id',$clients->id)
				->orderBy('country.name', 'ASC')
                ->get();

                if (!empty($clients)) {
                    return view('/clients/editClient', compact('clients', 'countries', 'parent_ids', 'country_region','ClientCountryRates'));
                } else {
                    Toast::error('Invalid id, Please try again!', 'Error');
                    return Redirect('clients/editClient/' . $id);
                }
            }
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function updateClient(Request $request, $id = null) {
        if ($this->filter($this->permission)) {

            $this->validate($request, array(
                'name' => 'required|min:3',
                'client_type' => 'required',
                'country' => 'required',
                'region' => 'required',
                'ave_collect_size' => 'nullable|numeric|between:0,2147483646',
                'margin_oem_product' => 'nullable|numeric|between:0,9999999999.99',
                'margin_oem_logistic' => 'nullable|numeric|between:0,9999999999.99',
                'margin_oem_service' => 'nullable|numeric|between:0,9999999999.99',
            ));
			
            $user = Client::find($request->input('id'));

            if (!empty($user)) {
                if ($request->get('status') != null) {
                    $user->status = 'active';
                } else {
                    $user->status = 'inactive';
                }

				$org_size = $user->ave_collect_size;
				if (isset($request->ave_collect_size) && is_numeric($request->ave_collect_size)) {
                    $user->ave_collect_size = $request->get('ave_collect_size');
                } else {
					$user->ave_collect_size = null;
				}

				$org_oemp = $user->margin_oem_product;
				if (isset($request->margin_oem_product) && is_numeric($request->margin_oem_product)) {
                    $user->margin_oem_product = $request->get('margin_oem_product');
                } else {
					 $user->margin_oem_product = null;
				}

				$org_oeml = $user->margin_oem_logistic;
				if (isset($request->margin_oem_logistic) && is_numeric($request->margin_oem_logistic)) {
                    $user->margin_oem_logistic = $request->get('margin_oem_logistic');
                } else {
					$user->margin_oem_logistic = null;
				}

				$org_oems = $user->margin_oem_service;
				if (isset($request->margin_oem_service) && is_numeric($request->margin_oem_service)) {
                    $user->margin_oem_service = $request->get('margin_oem_service');
                } else {
					$user->margin_oem_service = null;
				}

				$parentid = $user->parent_id;
				$name =  $user->name;

                $user->name = strip_tags($request->get('name'));
                $user->client_type = $request->get('client_type');
				if($request->input('id') != $request->get('parent_id')){
					$user->parent_id = $request->get('parent_id');
				}
                $user->country = $request->get('country');
                $user->region = $request->get('region');

                $countries = $request->country_exchange;
                $exchangerates = $request->value_ex;  
                $dates = $request->date_ex;
     
				$org_countrylist = ClientCountryRate::where('client_id', '=', $user->id)->get()->toArray();
				$dif_countrylist = Array();
                ClientCountryRate::where('client_id', $user->id)->delete();
  
                if(isset($countries)  && count($countries) > 0){
                       foreach ($countries as $key => $country) {

                        $expires = '';

						if(isset($dates[$key])){
							$dt = DateTime::createFromFormat("Y-m-d", $dates[$key]);
							if($dt !== false && !array_sum($dt->getLastErrors())){
								$expires = $dates[$key];
							}
						}           
                        $exchangerate = (isset($exchangerates[$key]) && is_numeric($exchangerates[$key]) && $exchangerates[$key] > 0) ? $exchangerates[$key] : 0;

                        $ClientCountryRate = new ClientCountryRate();
                        $ClientCountryRate->country_id = $country;
                        $ClientCountryRate->expires = ($expires == '') ? null : $expires;
                        $ClientCountryRate->client_id = $user->id;
                        $ClientCountryRate->inherited = 1;
                        $ClientCountryRate->exchangerate = ($exchangerate == 0) ? null : $exchangerate;
                        $ClientCountryRate->save();

						//check if was in the orginal list and if so whether the exchange rate has changed
						$doesExist = false;
						foreach($org_countrylist as $cty){
							if($cty['country_id'] == $country && $cty['exchangerate'] != $exchangerate && $exchangerate > 0){
							echo $country." AA<br>";
								$dif_countrylist[] = $country;
								$doesExist = true;
								break;
							} else if($cty['country_id'] == $country){
								$doesExist = true;
							}
						}
						if(!$doesExist && $exchangerate > 0){ $dif_countrylist[] = $country; echo $country." bb<br>";}
                    }
                }

				foreach($org_countrylist as $cty){
					$country = ClientCountryRate::where('client_id', '=', $user->id)->where('country_id', '=', $cty['country_id'])->first();

					if((null === $country || empty($country)) && !empty($cty['exchangerate']) && $cty['exchangerate'] > 0){ 
						$dif_countrylist[] = $cty['country_id']; 

					}
				}

                if ($user->save()) {
					//resort all clients
					if($parentid != $request->get('parent_id') || $name != strip_tags($request->get('name'))){
						$this->updateOrder();
					}

					$clientlist = Array();
					$clientlist = $this->getTree($clientlist, 0, 0, '0', '', '');
					/*
					$cids = array_map(function($client) {
						return $client['id'];
					}, $clientlist);
					print_r($cids);
					*/
					
					// need to loop through each client and update quotes assigned to them.
					// only do this if there is they are inheriting details from this client
					foreach($clientlist as $client){

echo '<br>'.$client['id']." - ".$client['path']."<br>";

						//find out if values propagated from this client
						$obj =  $this->getClientData($client['id'], 'ave_collect_size');
						$u_size = ($obj->client_id == $parentid || $org_size != $user->ave_collect_size) ? true : false;
						$obj =  $this->getClientData($client['id'], 'margin_oem_product');
						$u_oemp = ($obj->client_id == $parentid || $org_oemp != $user->margin_oem_product) ? true : false;
						$obj =  $this->getClientData($client['id'], 'margin_oem_logistic');
						$u_oeml = ($obj->client_id == $parentid || $org_oeml != $user->margin_oem_logistic) ? true : false;						
						$obj =  $this->getClientData($client['id'], 'margin_oem_service');
						$u_oems = ($obj->client_id == $parentid || $org_oems != $user->margin_oem_service) ? true : false;	
				
						if($u_size || $u_oemp || $u_oeml || $u_oems || count($dif_countrylist) > 0){

							$quotes = Quote::where('status', '=', 1)->where('client_id', '=', $client['id'])->get();
							foreach($quotes as $quote){
echo $quote->id."<br>";
								$doUpdate = false;
								$flag = empty($quote->flag) ? 0 : $quote->flag;

								//don't reset currency rate is quote in US$ or $flag has currency locked
								if($quote->currency_rate != 1 || $flag == 0 || $flag == 2){
									$quote->currency_rate = $request->currency_rate;
									$doUpdate = true;
								}
								if($u_size && $flag != 4) {
									$quote->ave_collect_size = $user->ave_collect_size;
									$doUpdate = true;
								}
								if($u_oemp && $flag != 4 && $flag != 2 && $flag != 3) {
									$quote->margin_oem_product = $user->margin_oem_product;
									$doUpdate = true;
								}
								if($u_oeml && $flag != 4 && $flag != 2 && $flag != 3) {			
									$quote->margin_oem_logistic = $user->margin_oem_logistic;
									$doUpdate = true;
								}
								if($u_oeml && $flag != 4 && $flag != 2 && $flag != 3) {	
									$quote->margin_oem_service = $user->margin_oem_service; 
									$doUpdate = true;
								}

								if(count($dif_countrylist) > 0 && in_array($quote->country, $dif_countrylist) && $flag != 4 && $flag != 1){
									// get the rate from the country table
									$rate = Country::find($quote->country)->currency_rate;

									//check if there is an override rate or whether it was removed
									$args = array(
										'client'	=> $quote->client_id,
										'country'	=> $quote->country
									);							
									$results = DB::select(DB::raw("SELECT * 
										FROM `client_country_rates` ccr
										JOIN(
											SELECT t.`id` AS client_id, @pv:=t.`parent_id` AS parent, @rownum := @rownum + 1 AS rownum
											FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
											JOIN (SELECT @pv:= :client, @rownum:=0)tmp
											WHERE t.`id`= @pv
										)c on c.`client_id` = ccr.`client_id`
										WHERE country_id = :country AND (expires IS NULL OR expires > now())
										ORDER BY c.`rownum`
										LIMIT 1"), $args);
									if(empty($results) || !isset($results[0]->exchangerate)){	
										$rate = $results[0]->exchangerate;
									}

									//check if quote rate is different to that caclulated and then apply if it is.
									if($rate != $quote->currency_rate){
										$quote->currency_rate = $rate;
										$doUpdate = true;
									}
								}

								if($doUpdate){
									$quote->save();

									//recalculate the quote 
									$calculate = new CalculationsController();
									$calculate->Recalculate($quote->id);
								}
							}
						}
					}
					
                       //data  show  effected  by  here
                    Toast::success('Record has been updated.', 'Success');
                    //return Redirect('clients/editClient/' . $user->id);
                } else {
                    Toast::error('Client not updated, Please try again.', 'Error');
                    return Redirect('clients/editClient/' . $user->id);
                }
            } else {

                Toast::error('Invalid Client id.', 'Error');
                return Redirect('clients/index');
            }
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

	public function getClientData($clientid, $data){
		if(!isset($clientid) || !is_numeric($clientid) || $clientid < 0){ return 0; }

		$infoitem = "id";
		switch ($data){
			case 'country': $infoitem = 'country'; break;
			case 'region': $infoitem = 'region'; break;
			case 'ave_collect_size': $infoitem = 'ave_collect_size'; break;
			case 'margin_oem_product': $infoitem = 'margin_oem_product'; break;
			case 'margin_oem_logistic': $infoitem = 'margin_oem_logistic'; break;
			case 'margin_oem_service': $infoitem = 'margin_oem_service'; break;
		}

		$args = array(
			'client'	=> $clientid,
		);

		$sql = "SELECT $infoitem AS `data`, c.client_id
			FROM `clients` ccr
			JOIN(
				SELECT t.`id` AS client_id, @pv:=t.`parent_id` AS parent, @rownum := @rownum + 1 AS rownum
				FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
				JOIN (SELECT @pv:= :client, @rownum:=0)tmp
				WHERE t.`id`= @pv
			)c on c.`client_id` = ccr.`id`
			WHERE $infoitem IS NOT NULL
			ORDER BY c.`rownum`
			LIMIT 1";
			
		$results = DB::select(DB::raw($sql), $args);
		if(!empty($results)){
			return (object)Array('client_id' => $results[0]->client_id, 'value' => $results[0]->data, 'date_lement' => $data);
		} else {
			return (object)Array('client_id' => $clientid, 'value' => null, 'date_lement' => $data);
		}
	}

    public function multipleDelete(Request $request) {
        if ($this->filter($this->permission)) {
            $a = '';
            $allIds=array();
            foreach ($request->ids as $key => $id) {
                $user = Client::find($id);
				if(null !== $user && !empty($user)){
					$user->delete();
				}			
			}

			Toast::success('Clients deleted successfully', 'success');

            $this->updateOrder();
            return Redirect('clients/index');
        } else {
           
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function Delete($id = Null) {
        if ($this->filter($this->permission)) {

            $user = Client::find($id);
			if(null !== $user && !empty($user)){				
				$user->delete();
				$this->updateOrder();
				Toast::success('Record has been deleted.', 'success');
				return Redirect('clients/index');
			} else {
                Toast::error('Invalid id, Please try again!', 'error');
                return Redirect('clients/index');
            }

        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

	public function getParent($arr, $level, $id){
		$c = Client::find($id);
		$level ++;

		if($c === null || $id == -1){
			$id = -1;
		} else {
			$id = $c->parent_id;
			$arr[] = array('id' => $c->id, 'name' => $c->name,  'level' => $level, 'parent' => $id);		
			if($c->id > 0){
				$arr = $this->getParent($arr, $level, $id);
			}
		}
		return $arr;
	}

	public function getTree($arr, $level, $id, $path, $order, $fullpath){
		$clients = Client::where('id' , '>', '0')->where('parent_id', '=', $id)->orderBy('name', 'asc')->get();
		$level ++;
		$i = 0;

		if($clients !== null || count($clients) > 0){
			foreach($clients as $client){
				$i++;
				$c_order = strval($order).strval($i);
				$c_path = $path.'/'.$client->id;
				$c_fpath = ($fullpath == '') ? $client->name : $fullpath . ' > ' . $client->name;

				$arr[] = array('id' => $client->id, 'name' => $client->name,  'level' => $level, 'parent' => $id, 'path' => $c_path, 'order' => $c_order, 'fullpath' => $c_fpath);
				$arr = $this->getTree($arr, $level, $client->id, $c_path, $c_order, $c_fpath);
			}
		}
		return $arr;
	}

	public function getTreeIndent($arr, $level, $id, $path, $order, $fullpath){
		$clients = Client::where('id' , '>', '0')->where('parent_id', '=', $id)->orderBy('name', 'asc')->get();
		$level ++;
		$i = 0;

		if($clients !== null || count($clients) > 0){
			foreach($clients as $client){
				$i++;
				$c_order = strval($order).strval($i);
				$c_path = $path.'/'.$client->id;
				$c_fpath = ($fullpath == '') ? $client->name : $fullpath . ' > ' . $client->name;

				$arritem = array();
				$arrtemp = $this->getTreeIndent($arritem, $level, $client->id, $c_path, $c_order, $c_fpath);
				$arritem = array('id' => $client->id, 'name' => $client->name,  'level' => $level, 'parent' => $id, 'path' => $c_path, 'order' => $c_order, 'fullpath' => $c_fpath );
				if(isset($arrtemp) && count($arrtemp) > 0){
					$arritem['items'] = $arrtemp;
				}

				$arr[] = $arritem;								
			}

		}
		return $arr;
	}

	public function updateOrder(){
		$cids = array();
		$cids = $this->getTree($cids, 0, 0, '0', '', '');

		foreach($cids AS $c){
			$client = Client::find($c["id"]);
			$client->path = $c["path"];
			$client->fullpath = $c["fullpath"];
			$client->order = $c["order"];
			$client->level = $c["level"];
			$client->save();
		}
	}

}
