<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Session;
use Toast;
use DateTime;
use links;
use App\Country;
use App\CountryRegion;
use App\Quote;

class CountriesController extends Controller {

     private $permission = [1];

    public function __construct() {
        $this->middleware('auth');
    }

    public function index(Request $request) {
        if ($this->filter($this->permission)) {
            Input::flash();
           
            $countries = Country::where('isActive', '=', 1)->orderBy('name', 'desc')->get();
            
            return view('countries/index', compact('countries'));
        } else {
             Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function addCountry() {
        if ($this->filter($this->permission)) {
            return view('countries/addCountry');
        } else {
             Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function insertCountry(Request $request) {

        if ($this->filter($this->permission)) {
            Input::flash();
            $this->validate($request, array(
                'name' => 'required',
                'currency_name' => 'required',
                'currency_symbol' => 'required',
                'currency_rate' => 'required|numeric',
                'country_region_name' => 'required',
            ));
            $country_status = $request->input('country_status');
            if ($country_status == 'on') {
                $status = 'active';
            } else {
                $status = 'Inactive';
            }
            $save = Country::Create(array(
                        'name' => $request->input('name'),
                        'currency_name' => $request->input('currency_name'),
                        'currency_symbol' => $request->input('currency_symbol'),
                        'currency_rate' => $request->input('currency_rate'),
                        'status' => $status,
                        'isActive' => 1,
            ));
            if (isset($save->id)) {
                $country_id = $save->id;
                $rname = $request->input('country_region_name');
                $r_status = $request->input('check');
                foreach ($rname as $key => $name) {
                    if ($r_status[$key] == '1') {
                        $statuss = 'active';
                    } else {
                        $statuss = 'Inactive';
                    }
                    $save_region = CountryRegion::Create(array(
                                'country_id' => $save->id,
                                'name' => $name,
                                'status' => $statuss,
                                'isActive' => 1,
                    ));
                }
                if (isset($save_region->id)) {
                    Toast::success('Record has been saved.', 'Success');
                    return redirect('/countries/index');
                } else {
                    Toast::error('Id not found', 'Error');
                    return redirect('/countries/index');
                }
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function multipleDelete(Request $request) {

        if ($this->filter($this->permission)) {
            $id = $request->get('ids');
            if (isset($id)) {
                Country::destroy($id);
                Toast::success('Record has been deleted.', 'Success');
                return Redirect('countries/index');
            } else {
                Toast::error('Id not found', 'Error');
                return Redirect('countries/index');
            }
        } else {
             Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function MultipleDeleteRegion(Request $request) {

        if ($this->filter($this->permission)) {
            $id = $request->get('ids');
            if (isset($id)) {
                CountryRegion::destroy($id);
                Toast::success('Record has been deleted.', 'Success');
                return Redirect('countries/index');
            } else {
                Toast::error('Id not found', 'Error');
                return Redirect('countries/index');
            }
        } else {
             Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function deleteCountry($id) {

        if ($this->filter($this->permission)) {
            $country = Country::find($id);
            $country->isActive = 0;
            DB::table('country_region')->where('country_id', $id)->update(['isActive' => 0]);
            if ($country->save()) {
                Toast::success('Record has been deleted.', 'Success');
                return redirect('/countries/index');
            } else {
                Toast::error('Id not found', 'Error');
                return redirect('/countries/index');
            }
        } else {
             Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function editCountry($id) {
        if ($this->filter($this->permission)) {
            $country_region = CountryRegion::where('country_id', '=', $id)
                ->orderby('name', 'asc')
                ->get();
            
            $country = Country::with('country_regions')
                ->find($id);
            return view('countries.editCountry', compact('country_region', 'country'));
        } else {
             Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function updateCountry(Request $request, $id = null) {

        if ($this->filter($this->permission)) {
            Input::flash();
            $this->validate($request, array(
                'name' => 'required',
                'currency_name' => 'required',
                'currency_symbol' => 'required',
                'currency_rate' => 'required|numeric',
                'Country_region' => 'required'
            ));

			if ($id != null && is_numeric($id)) {
				$country_status = $request->input('country_status');
				if ($country_status == 'on') {
					$status = 'active';
				} else {
					$status = 'Inactive';
				}
				$country_insert = Country::find($id);

				$current_rate = $country_insert->currency_rate;

				$country_insert->name = $request->name;
				$country_insert->currency_name = $request->currency_name;
				$country_insert->currency_symbol = $request->currency_symbol;
				$country_insert->currency_rate = $request->currency_rate;
				$country_insert->status = $status;
				$country_insert->isActive = 1;
            
                $r_name		= $request->input('Country_region');
                $r_status	= $request->input('check');
				$r_ids		= $request->input('Country_region_id');       
				$r_new_ids = array();

                if (isset($r_name) AND isset($r_status)) {
                    foreach ($r_name as $key => $name) {
                        if ($r_status[$key] == '1') {
                            $statuss = 'active';
                        } else {
                            $statuss = 'Inactive';
                        }

						$rid = (!empty($r_ids[$key]) && is_numeric($r_ids[$key]) && $r_ids[$key] > 0) ? $r_ids[$key] : 0;						
						$region = CountryRegion::find($rid);

						if(null === $region || $rid == 0){
							$region = CountryRegion::Create(array(
										'country_id' => $id,
										'name' => $name,
										'status' => $statuss,
										'isActive' => '1',
						
									));
						} else {
							$region->name = $name;
							$region->status = $statuss;
							$region->save();
						}
						$r_new_ids[] = $region->id;
                    }

					//delete any regions no longer required
					CountryRegion::where('country_id', $id)->whereNotIn('id', $r_new_ids)->delete();
					DB::select(DB::raw("DELETE FROM price_logistics WHERE region_id NOT IN (SELECT id FROM country_region)"));

                } else {
                    Toast::error('You must add at least 1 region', 'Error');
                    return redirect("/countries/editCountry/$id");
                }

				if ($country_insert->save()) {

					//if the exchange rate has changed agains the country then need to recalculate the quotes
					// check that quotess allow currency changes i.e. quote falg cannot be 1, 3, or 4
					if($current_rate != $request->currency_rate){

						$quotes = Quote::where('status', '=', 1)->where('country', '=', $id)->whereNotIn('flag', [1, 3, 4])->orWhereNull('flag')->get();

						foreach($quotes as $quote){
							//don't reset rate is quote in US$
							if($quote->currency_rate != 1 || empty($quote->flag) || $quote->flag == 0 || $quote->flag == 2){
								//then need to check there isn't a country rate override for tis quote										
								$args = array(
									'client'	=> $quote->client_id,
									'country'	=> $id
								);							
								$results = DB::select(DB::raw("SELECT * 
									FROM `client_country_rates` ccr
									JOIN(
										SELECT t.`id` AS client_id, @pv:=t.`parent_id` AS parent, @rownum := @rownum + 1 AS rownum
										FROM (SELECT * FROM `clients` ORDER BY `order` DESC) t
										JOIN (SELECT @pv:= :client, @rownum:=0)tmp
										WHERE t.`id`= @pv
									)c on c.`client_id` = ccr.`client_id`
									WHERE country_id = :country AND (expires IS NULL OR expires > now())
									ORDER BY c.`rownum`
									LIMIT 1"), $args);
								if(empty($results) || !isset($results[0]->exchangerate)){					
									$quote->currency_rate = $request->currency_rate;
									$quote->save();
								}
							}
						}
					}

                    Toast::success('Record has been updated.', 'Success');
					return redirect("/countries/editCountry/$id");
                } else {
                    Toast::error('country not found', 'Error');
                    return redirect('/countries/index');
                }
            }
        } else {
            Toast::error('You have not permission.', 'Error');
            return redirect('/');
        }
    }

    public function showCountryRegion(Request $request, $id) {
        if ($this->filter($this->permission)) {
            Input::flash();
            $country_name = Country::find($id);
            $txt = $request->get('search');
            $limit = $request->get('perPage');
            if (isset($limit)) {
                $lim = $limit;
            } else {
                $lim = 30;
            }
            $country_region = CountryRegion::where('country_id', '=', $id);
            ;
            if (isset($txt)) {
                $country_region->where('name', 'like', '%' . $txt . '%');
            }
            $country_regions = $country_region->where('isActive', '=', 1)->paginate($lim);
            return view('countries/showCountryRegion', compact('country_regions', 'country_name', 'lim'));
        } else {
             Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function deleteCountryRegion($id = null) {
        if ($this->filter($this->permission)) {
            if ($id != null) {
                $country_regions = CountryRegion::find($id);
                $country_regions->isActive = 0;
                if ($country_regions->save()) {
                    Toast::success('Record has been deleted.', 'Success');
                    return redirect('/countries/index');
                } else {
                    Toast::error('Id not found', 'Error');
                    return redirect('/countries/index');
                }
            }
        } else {
            Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

}
