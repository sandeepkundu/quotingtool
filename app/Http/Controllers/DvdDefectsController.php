<?php
namespace App\Http\Controllers;

ini_set('max_execution_time', 600);

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Session;
use DateTime;
use links;
use Toast;
use App\ProductOption;
use View;
use App\IntegrationImport;
use App\PriceBands;
use App\PriceFactorBands;
use App\Quote;
use App\QuoteItem;
use App\QuoteItemsDefect;
use App\QuotesNotes;
use App\Http\Controllers\CalculationsController;

class DvdDefectsController extends Controller
{
	 private $permission = [1];

	public function __construct() {
		$this->middleware('auth');
		date_default_timezone_set('Asia/Kolkata');
	}
	public function index(){
		 if (!$this->filter($this->permission)) {
                   
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
         
		$IntegrationImports = IntegrationImport::where('source','=','defectsdvd')->orderBy('date','DESC')->get();
		return view('Variances/BulkDvdDefectsImports',array(
			'IntegrationImports'=> $IntegrationImports,

			));
	}
	public function BulkDvdDefectsImports_Store(Request $request){

		$debug = false;
		$folder         =  public_path().'/i/';
		$datestring = date_format(new DateTime, 'Y-m-d-H-i-s');
		$importfilename = 'defectsdvd-import-' . $datestring . '.csv';
		$exportfilename = 'defectsdvd-export-' . $datestring . '.csv';
		/*
		1. Save uploaded file to server 
		*/	
		if (Input::file("fileToUpload") == null) {
			Toast::error('A csv file must be selected for upload', 'Error');
            return redirect('/Variances/BulkDvdDefectsImports');

		}
		if ($_FILES["fileToUpload"]["error"] > 0) {

			Toast::error('A error was encountered uploading this file(' . $_FILES["file"]["error"] . ')', 'Error');
            return redirect('/Variances/BulkDvdDefectsImports');
		}
		$ftype = explode(".",$_FILES['fileToUpload']['name']);

		if(strtolower(end($ftype)) !== 'csv'){

			Toast::error('only a csv can be uploaded', 'Error');
            return redirect('/Variances/BulkDvdDefectsImports');
		}

		if(move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $folder. $importfilename) === FALSE){
			Toast::error('File could not be saved', 'Error');
            return redirect('/Variances/BulkDvdDefectsImports');
		}

		//============================== 2. Create a new import record ======================//
		$IntegrationImport               = new IntegrationImport();
		$IntegrationImport->date         = date('Y-m-d-H-i-s');
		$IntegrationImport->fileimported = $importfilename;
		$IntegrationImport->source       = 'defectsdvd';
		$IntegrationImport->save();

		$import_id = $IntegrationImport->id;

        //reset all records so that none are flagged as being updated
		$a = DB::select(DB::raw("UPDATE `price_factors_bands` SET processed = 0"));

        /*
		3. Process the file 
		*/
		$row = 0;
		$columncount = 0;
		$processrecords = array();
		$columnames = array();
		$isValid = FALSE;
		$num_invalid = 0;
		$num_valid = 0;

		if (($handle = fopen($folder. $importfilename, "r")) !== FALSE) {

			while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
				$row++;
				$isValid = FALSE;

				if($row == 1){
					$columncount = count($data);
					$columnames = $data;

				} else if (count($data) != $columncount){
					// incorrect number of columns for the row
					array_unshift($data, "[record has incorrect number of columns (".  count($data) . ")]");				
					array_push($processrecords, $data);
					$num_invalid ++;
				} else {
					$factor		= $data[0];		// Factor				e.g. Defects or DVD
					$type		= $data[1];		// Product Type			e.g. Notebook				
					$band		= $data[2];		// Price band			e.g. $0.00 - $10.00							
					$value		= $data[3];		// Defect or DVD name	e.g. Back Panel Cover Missing
					$client		= $data[4];		// Client				e.g. HP India store A
					$price		= $data[5];		// Price				e.g. 10
					$band_from	= '';
					$band_to	= '';

					// validate the data
					$validationmessage = '';

					if(!isset($factor) || ($factor !== 'Defects' && $factor !== 'DVD')){
						$validationmessage .= "[the factor can only be Defects or DVD]";
					}			

					if(!isset($type) || $type === '' || !isset($band) || $band === '' || !isset($value) || $value === '' ){
						$validationmessage .= "[product type, price band and value are required]";
					}	

					if(strpos($band,'-') === false) {

						$validationmessage .= "[price format invalid]";
					} else {
						$bandpart = explode('-',$band);

						$band_from = (is_numeric((int)$bandpart[0])) ? $bandpart[0] : '';
						$band_to = (is_numeric((int)$bandpart[1]))?$bandpart[1] : '';						
					}	
	
					if($band_from === '' || $band_to === ''){
						$validationmessage .= "[price format invalid]";
					}

					$args = array(
						'client' => $client, 
						'type' => $type, 
						'value' => $value, 
						'value2' => $value, 
						'bfrom' => $band_from, 
						'bto' => $band_to, 
					);

					if($debug){
						echo '$client = ' . $client .'<br>';
						echo '$type = ' . $type .'<br>';
						echo '$value = ' . $value .'<br>';
						echo '$band_from = ' . $band_from .'<br>';
						echo '$band_to = ' . $band_to .'<br>';
						echo "SELECT 
						IFNULL((SELECT id FROM clients WHERE `name` = '". $client ."'), 0) AS client,
						IFNULL((SELECT id FROM product_options WHERE type = 'product' AND name = '". $type ."' LIMIT 1), 0) AS type,
						IFNULL((SELECT id FROM product_options WHERE type = 'dvd' AND name = '". $value ."' LIMIT 1), 0) AS dvd,
						IFNULL((SELECT id FROM product_options WHERE type = 'defects' AND name = '". $value ."' LIMIT 1), 0) AS defect,
						IFNULL((SELECT id FROM price_bands WHERE `value_from` = " . $band_from . " AND `value_to` = " . $band_to . " LIMIT 1), 0) AS band<br><br>";
					}

					$resultset = DB::select(DB::raw("SELECT 
						IFNULL((SELECT id FROM clients WHERE `name` = :client), 0) AS client,
						IFNULL((SELECT id FROM product_options WHERE type = 'product' AND name = :type LIMIT 1), 0) AS type,
						IFNULL((SELECT id FROM product_options WHERE type = 'dvd' AND name = :value LIMIT 1), 0) AS dvd,
						IFNULL((SELECT id FROM product_options WHERE type = 'defects' AND name = :value2 LIMIT 1), 0) AS defect,
						IFNULL((SELECT id FROM price_bands WHERE `value_from` = :bfrom AND `value_to` = :bto LIMIT 1), 0) AS band"), $args);

					if(!empty($resultset)){

						$type_id = $resultset[0]->type;
						$defect_id = $resultset[0]->defect;
						$dvd_id = $resultset[0]->dvd;
						$band_id = $resultset[0]->band;
						$client_id = $resultset[0]->client;
						
						if($type_id === 0) {
							$validationmessage .= "[product type lookup not found]";
						}

						if(empty($client) && $client == '') {
							$validationmessage .= "[client not found]";
						}

						if($debug){
						 echo "message = $validationmessage<br>"; 
						}
						
						//don't continue if there are any validation errors
						if($validationmessage === '') {

							// retreive step id
							$step_id = 0;
							$brand_id = '';

							$resultset1 = DB::select(DB::raw("SELECT id 
								FROM `quote_step` 
								WHERE type_id = '".$type_id."' 
								AND (brand_id = '".$brand_id."' OR brand_id is null) 
								AND (client_id = '".$brand_id."' OR client_id = 0) 
								ORDER BY client_id DESC, brand_id DESC LIMIT 1"));

							if(count($resultset1) > 0){
								if(!empty($resultset)){
									$step_id = $resultset1[0]->id;
								}
							}

							if($band_id <= 0){
								$PriceBands = new PriceBands();
								$PriceBands->value_from = $band_from;
								$PriceBands->value_to = $band_to;
								$PriceBands->save();
								$band_id = $PriceBands->id;
							}

							if(isset($defect_id) && $defect_id <= 0 && $factor == 'Defects'){
								$ProductOption = new ProductOption();
								$ProductOption->type = 'Defects';
								$ProductOption->name = $value;
								$ProductOption->order = 0;
								$ProductOption->status = 'active';
								$ProductOption->isActive = 1;
								$ProductOption->description = '';
								$ProductOption->image = '';
								$ProductOption->save();

								$defect_id = $ProductOption->id;
								
								//add to group
								/*$resultset2 = DB::select(DB::raw("INSERT INTO product_option_groups 
									(`group`, `group_code`, `name`, `option_id`,`type`,`order`, `status`, `client_id`) 
									SELECT DISTINCT pgo.`group`, pgo.`group_code`, po.name, po.id, '".$factor."',0, 'active','".$client_id."'  
									FROM `product_option_groups` pgo 
									INNER JOIN `product_options` po ON pgo.option_id = po.id 
									INNER JOIN `quote_step_items` qsi ON qsi.group_code = pgo.group_code 
									WHERE po.type = 'Defects' AND qsi.quote_step_id = '".$step_id."'"));
								*/

								$validationmessage .= "[added defect]";
							}

							if(isset($dvd_id) && $dvd_id <= 0 && $factor == 'DVD'){
								//ADD RAM

								$ProductOption = new ProductOption();
								$ProductOption->type = 'DVD';
								$ProductOption->name = $value;
								$ProductOption->order = 0;
								$ProductOption->status = 'active';
								$ProductOption->isActive = 1;
								$ProductOption->description = '';
								$ProductOption->image = '';
								$ProductOption->save();
								$dvd_id = $ProductOption->id;

								//add to group
								//$resultset3 = DB::select(DB::raw("INSERT INTO product_option_groups (`group`, `group_code`, `name`, `option_id`,`type`,`order`, `status`, `client_id`) SELECT DISTINCT pgo.`group`, pgo.`group_code`, '".$type."', '".$defect_id."','".$factor."',0, 'active','".$client_id."'  FROM `product_option_groups` pgo INNER JOIN `product_options` po ON pgo.option_id = po.id INNER JOIN `quote_step_items` qsi ON qsi.group_code = pgo.group_code WHERE po.type = 'DVD' AND qsi.quote_step_id = '".$step_id."'"));

								$validationmessage .= "[added dvd]";
							}

							$option_id = ($factor == 'Defects') ? $defect_id : $dvd_id;

							$args = array(
								'client'	=> $client_id,
								'type'		=> $type_id,
								'band'		=> $band_id,
								'option'	=> $option_id, 
								'factor'	=> $factor
							);

							$resultset4 = DB::select(DB::raw("SELECT id 
								FROM `price_factors_bands` 
								WHERE client_id = :client 
								AND type_id = :type
								AND price_band_id = :band
								AND option_id = :option
								AND factor_name = :factor LIMIT 1"), $args);

							//if(count($resultset4) > 0){
								if(!empty($resultset4[0]->id)){ 
									//exist so only update processed
									
									$id = $resultset4[0]->id; 
									$PriceFactorBand = PriceFactorBands::find($id);
									$PriceFactorBand->value = $price;
									$PriceFactorBand->processed = 1;

									$PriceFactorBand->save();
									$validationmessage .= "[updated]";
									$isValid = TRUE; 
								} else {
									//add
									$PriceFactorBands = new PriceFactorBands();
									$PriceFactorBands->client_id = $client_id;
									$PriceFactorBands->type_id = $type_id;
									$PriceFactorBands->price_band_id = $band_id;
									$PriceFactorBands->option_id = $option_id;
									$PriceFactorBands->factor_name = $client_id;
									$PriceFactorBands->factor_name = $factor;
									$PriceFactorBands->value = $price;
									$PriceFactorBands->processed = 1;
									$PriceFactorBands->save();
									$validationmessage .= "[added]";
									$isValid = TRUE; 
								}
							//}
						}

					} else {
						$validationmessage .= "[error occurred validating model]";
					}

					if($isValid) { 						
						//$validationmessage = 'OK';
						$num_valid ++;
					} else {
						$num_invalid ++;
					}

					array_unshift($data, $validationmessage);				
					array_push($processrecords, $data);
				}
			}
		
			$resultset5 = DB::select(DB::raw("SELECT 
				'[deleted]' AS message,
				pf.factor_name AS factor,
				typ.name AS type,
				concat(pb.value_from, ' - ', pb.value_to) AS band,
				CASE WHEN pf.factor_name = 'Defects' THEN defect.name ELSE dvd.name END AS ddname,
				c.name AS client ,
				pf.value AS price
				FROM `price_factors_bands` pf
				INNER JOIN `product_options` typ ON pf.type_id = typ.id 
				LEFT JOIN `product_options` defect ON pf.option_id = defect.id AND defect.type = 'Defects' AND pf.factor_name = 'Defects'
				LEFT JOIN `product_options` dvd ON pf.option_id = dvd.id AND dvd.type = 'DVD' AND pf.factor_name = 'DVD'
				LEFT JOIN `clients` c ON c.id = pf.client_id
				LEFT JOIN `price_bands` pb ON pf.price_band_id = pb.id
				WHERE processed = 0"));

			if(!empty($resultset5) && count($resultset5) > 0){

				foreach ($resultset5 as $row) {
					//add each record to array for export in csv
					array_push($processrecords, 
						array (
							$row->message,
							$row->factor,
							$row->type,
							$row->ddname,
							$row->client,
							$row->price
							)
						);
					
				}
			}

			//remove all records not updated
			$resultset6 = DB::delete(DB::raw("DELETE FROM `price_factors_bands` WHERE processed = 0"));

			//save the processed records to file.
			$exportfile = fopen($folder.$exportfilename, "w");

			array_unshift($columnames, 'message');
			fputcsv($exportfile, $columnames);
			foreach ($processrecords as $line){
				fputcsv($exportfile, $line);
			}

			/*
			4. Update import record will export file name andretrun results to 
			*/	
			$resultset7 = DB::update(DB::raw("UPDATE `integration_imports` SET `fileexported` = '".$exportfilename."', `records_valid` = '".$num_valid."' WHERE `id` = '".$import_id."' "));

			//need to recalulate all inprogress quotes.
			$Quotes = Quote::where('status', '=', '1')->get();
			if(count($Quotes) >0 ){
				foreach($Quotes as $Quote){
					$calculate = new CalculationsController();
					$calculate->Recalculate($Quote->id);

					$QuotesNotes = new QuotesNotes();
					$QuotesNotes->quote_id = $Quote->id;
					$QuotesNotes->user_id = Auth::user()->id;
					$QuotesNotes->display_level = 1;
					$QuotesNotes->note = 'DVD/Defect bulk import - recalculation of in-progress quote.';
					$QuotesNotes->save();
				}
			}

			Toast::Success('Import has been completed', 'Success');
            return redirect('/Variances/BulkDvdDefectsImports');

		} else {

			Toast::error('the imported file couldn\'t be processed', 'Error');
            return redirect('/Variances/BulkDvdDefectsImports');
		}
	}
}
