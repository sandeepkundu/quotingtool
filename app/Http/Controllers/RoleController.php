<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Session;
use Toast;
use DateTime;
use links;
use App\Role;
use App\PermissionRole;
use URL;

class RoleController extends Controller {

     private $permission = [1];
     
    public function __construct() {
        $this->middleware('auth');
    }

    public function index(Request $request) {
        if ($this->filter($this->permission)) {
            Input::flash();
            $txt = $request->get('search');
            $limit = $request->get('perPage');
            if (isset($limit)) {
                $lim = $limit;
            } else {
                $lim = 30;
            }
            $Roles = DB::table('roles');
            $Roles->select('id', 'name', 'display_name', 'description');
            if (isset($txt)) {
                $Roles->where('name', 'like', '%' . $txt . '%');
                $Roles->orWhere('display_name', 'like', '%' . $txt . '%');
            }
            $Roles = $Roles->orderBy('name', 'asc')->paginate($lim);
            return view('Roles.index', compact('Roles', 'lim'));
        } else {
            Toast::error('You have not permission.', 'Error');
            return redirect('/');
        }
    }

    public function addRoles() {
        if ($this->filter($this->permission)) {
            $permissions = DB::table('permissions')->get();
            return view('Roles.addRoles', compact('permissions'));
        } else {
            Toast::error('You have not permission.', 'Error');
            return redirect('/');
        }
    }

    public function insertRoles(Request $request) {

        if ($this->filter($this->permission)) {
            $this->validate($request, array(
                'name' => 'required|unique:roles'
            ));
            $description = $request->input('description');
            $dom = new \DomDocument();
            $dom->loadHtml($description, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
            $images = $dom->getElementsByTagName('img');
            foreach ($images as $k => $img) {
                $data = $img->getAttribute('src');
                list($type, $data) = explode(';', $data);
                list(, $data) = explode(',', $data);
                $data = base64_decode($data);
                $image_name = "/product/" . time() . $k . '.png';
                $path = public_path() . $image_name;
                file_put_contents($path, $data);
                $img->removeAttribute('src');
                $img->setAttribute('src', URL::to('/') . $image_name);
            }
            $description = $dom->saveHTML();
            $Role = new Role();

            $Role->fill($request->all());
            $Role->description = $description;
            if ($Role->save()) {
                //dd($Role->id);
                if ($request->Manage_User != null) {
                    $PermissionRole = new PermissionRole();
                    $PermissionRole->permission_id = 1;
                    $PermissionRole->role_id = $Role->id;
                    $PermissionRole->save();
                }
                if ($request->Manage_Quotes != null) {
                    $PermissionRole = new PermissionRole();
                    $PermissionRole->permission_id = 2;
                    $PermissionRole->role_id = $Role->id;
                    $PermissionRole->save();
                }
                if ($request->Financials != null) {
                    $PermissionRole = new PermissionRole();
                    $PermissionRole->permission_id = 3;
                    $PermissionRole->role_id = $Role->id;
                    $PermissionRole->save();
                }
                if ($request->Collection_Request != null) {
                    $PermissionRole = new PermissionRole();
                    $PermissionRole->permission_id = 4;
                    $PermissionRole->role_id = $Role->id;
                    $PermissionRole->save();
                }

                Toast::success('Record has been saved.', 'Success');
                return redirect('/Roles/index');
            } else {
                Toast::error('Record not saved', 'Error');
                return redirect()->route('/Roles/index');
            }
        } else {
            Toast::error('You have not permission.', 'Error');
            return redirect('/');
        }
    }

    public function editRoles($id) {
        if ($this->filter($this->permission)) {
            $Role = Role::find($id);
            $PermissionRole = DB::table('permission_role')->where('role_id', '=', $Role->id)->get();
            if (!empty($PermissionRole)) {
                $a = array();
                foreach ($PermissionRole as $key => $value) {
                    $a[] = $value->permission_id;
                }
            }

            $permissions = DB::table('permissions')->get();
            return view('Roles.editRoles', compact('Role', 'permissions', 'a'));
        } else {
            Toast::error('You have not permission.', 'Error');
            return redirect('/');
        }
    }

    public function updateRoles(Request $request) {
        if ($this->filter($this->permission)) {
            $this->validate($request, array(
                'name' => 'required'
            ));
            if ($request->hidden_name != $request->name) {
                $this->validate($request, array(
                    'name' => 'required|unique:roles'
                ));
            }
            $Role = Role::find($request->id);
            $Role->fill($request->all());

            if (!empty($request->description)) {
                $description = $request->input('description');
                $dom = new \DomDocument();
                $dom->loadHtml($description, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                $images = $dom->getElementsByTagName('img');
                foreach ($images as $k => $img) {
                    $data = $img->getAttribute('src');
                    if (strpos($data, 'data:image') !== false) {
                        list($type, $data) = explode(';', $data);
                        list(, $data) = explode(',', $data);
                        $data = base64_decode($data);
                        $image_name = "/product/" . time() . $k . '.png';
                        $path = public_path() . $image_name;
                        file_put_contents($path, $data);
                        $img->removeAttribute('src');
                        $img->setAttribute('src', URL::to('/') . $image_name);
                    }
                }
                $description = $dom->saveHTML();
            }
            $Role->description = $description;
            if ($Role->save()) {
                DB::table('permission_role')->where('role_id', '=', $Role->id)->delete();
                if ($request->Manage_User != null) {
                    $PermissionRole = new PermissionRole();
                    $PermissionRole->permission_id = 1;
                    $PermissionRole->role_id = $Role->id;
                    $PermissionRole->save();
                }
                if ($request->Manage_Quotes != null) {
                    $PermissionRole = new PermissionRole();
                    $PermissionRole->permission_id = 2;
                    $PermissionRole->role_id = $Role->id;
                    $PermissionRole->save();
                }
                if ($request->Financials != null) {
                    $PermissionRole = new PermissionRole();
                    $PermissionRole->permission_id = 3;
                    $PermissionRole->role_id = $Role->id;
                    $PermissionRole->save();
                }
                if ($request->Collection_Request != null) {
                    $PermissionRole = new PermissionRole();
                    $PermissionRole->permission_id = 4;
                    $PermissionRole->role_id = $Role->id;
                    $PermissionRole->save();
                }


                Toast::success('Record has been updated.', 'Success');
                return redirect('/Roles/index');
            } else {
                Toast::error('Record not saved', 'Error');
                return redirect()->route('/Roles/index');
            }
        } else {
            Toast::error('You have not permission.', 'Error');
            return redirect('/');
        }
    }

    public function multipleDelete(Request $request) {

        if ($this->filter($this->permission)) {
            if (Role::destroy($request->get('ids'))) {
                Toast::success('Records has been deleted.', 'Success');
                return Redirect('Roles/index');
            } else {
                Toast::error('Please select any record.', 'Error');
                return Redirect('Roles/index');
            }
        } else {
            Toast::error('You have not permission.', 'Error');
            return redirect('/');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id = Null) {

        if ($this->filter($this->permission)) {
            $Role = Role::find($id);
            if (!empty($Role)) {

                ($Role->delete());
                Toast::success('Records has been deleted.', 'Success');
                return Redirect('Roles/index');
            } else {
                Toast::error('Invalid id, Please try again!', 'Error');
                return Redirect('Roles/index');
            }
        } else {
            Toast::error('You have not permission.', 'Error');
            return redirect('/');
        }
    }

}
