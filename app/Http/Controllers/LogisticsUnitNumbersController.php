<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use DateTime;
use links;
use App\User;
use Toast;
use App\Country;
use App\Client;
use App\CountryRegion;
use App\PriceLogistic;
use App\LogisticsUnitNumber;

class LogisticsUnitNumbersController extends Controller {

      private $permission = [1];

    public function __construct() {
        $this->middleware('auth');
    }

    public function index(Request $request) {
        if ($this->filter($this->permission)) {
            
            $logistics_unit_numbers = LogisticsUnitNumber::with('Clientname')->orderBy('value_from', 'asc')->get();
            return view('LogisticsUnitNumbers/index', array('logistics_unit_numbers' => $logistics_unit_numbers));
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function addLogisticsUnitNumbers() {
        if ($this->filter($this->permission)) {
            $clients = DB::table('clients')->get();
            return view('LogisticsUnitNumbers/addLogisticsUnitNumbers', array(
                'clients' => $clients,
            ));
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    public function insertLogisticsUnitNumbers(Request $request) {
        if ($this->filter($this->permission)) {
            Input::flash();
            $this->validate($request, array(
                'value_from' => 'required|numeric',
                'value_to' => 'required|numeric|',
            ));

            if ($request->get('value_to') > $request->get('value_from')) {
                $LogisticsUnitNumber = new LogisticsUnitNumber();
                $LogisticsUnitNumber->fill($request->all());
                $LogisticsUnitNumber->client_id = '0';
                if ($LogisticsUnitNumber->save()) {
                    Toast::success('Record has been saved.', 'Success');
                    return redirect('/LogisticsUnitNumbers/index');
                } else {
                    Toast::error('Record not saved', 'Error');
                    return redirect()->route('/LogisticsUnitNumbers/index')->withInput();
                }
            } else {
                Toast::error('Value From should be less than Value To', 'Error');
                return redirect('/LogisticsUnitNumbers/addLogisticsUnitNumbers');
            }
        } else {
          
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

    /* public function editLogisticsUnitNumbers($id=null){

      if($id!=null){
      $clients= DB::table('clients')->get();
      $LogisticsUnitNumber= LogisticsUnitNumber::find($id);

      return view('LogisticsUnitNumbers/editLogisticsUnitNumbers', array(
      'clients'=>$clients,
      'LogisticsUnitNumber'=>$LogisticsUnitNumber
      ));
      }
      } */
    /* public function updateLogisticsUnitNumbers(Request $request, $id = null){   
      Input::flash();
      $this->validate($request, array(
      'value_from' => 'required',
      'value_to' => 'required',
      ));

      $LogisticsUnitNumber = LogisticsUnitNumber::find($request->input('id'));

      if($request->get('value_to') > $request->get('value_from')){
      if(!empty($LogisticsUnitNumber)) {
      $LogisticsUnitNumber->client_id =0;
      $LogisticsUnitNumber->value_from = $request->get('value_from');
      $LogisticsUnitNumber->value_to = $request->get('value_to');
      if($LogisticsUnitNumber->save()){
      Toast::success('Record has been updated.', 'Success');
      return redirect('/LogisticsUnitNumbers/index');
      }else{
      Toast::error('Record not udpated', 'Error');
      return redirect('/LogisticsUnitNumbers/index')->withInput();
      }
      }else{
      Toast::error('Invalid id', 'Error');
      return redirect('/LogisticsUnitNumbers/index')->withInput();
      }
      }else{
      //dd($request->input('id'));
      Toast::error('Value From should be less than Value To', 'Error');
      return redirect('/LogisticsUnitNumbers/editLogisticsUnitNumbers/'.$request->input('id'));
      }
      } */

    public function multipleDelete(Request $request) {
        if ($this->filter($this->permission)) {
            if (LogisticsUnitNumber::destroy($request->get('ids'))) {
                Toast::success('Records has been deleted.', 'Success');
                return redirect('/LogisticsUnitNumbers/index');
            } else {
                Toast::error('Please select any record.', 'Error');
                return redirect('/LogisticsUnitNumbers/index');
            }
        } else {
           
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');;
        }
    }

    public function delete($id = Null) {
        if ($this->filter($this->permission)) {
            if ($id != null) {
                $LogisticsUnitNumber = LogisticsUnitNumber::find($id);
                if (!empty($LogisticsUnitNumber)) {
                    $LogisticsUnitNumber->delete();
                    Toast::success('Records has been deleted.', 'Success');
                    return redirect('/LogisticsUnitNumbers/index');
                } else {
                    Toast::error('Invalid id, Please try again!', 'Error');
                    return redirect('/LogisticsUnitNumbers/index');
                }
            } else {
                Toast::error('Please provide id !!', 'Error');
                return redirect('/LogisticsUnitNumbers/index');
            }
        } else {
            
      Toast::error("Authorization  failed:You  don't have permission.", "Error");
            Auth::logout();
            return redirect ('/login');
        }
    }

}
