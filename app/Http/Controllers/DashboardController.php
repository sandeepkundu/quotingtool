<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Session;
use DateTime;
use links;
use Toast;
use App\Client;
use App\User;
use App\ClientUser;
use App\Country;
use App\CountryRegion;
use App\RoleUser;
use App\QuoteManagement;
use App\LoginSuccess;

class DashboardController extends Controller
{
    private $permission = [1];
    /*
     * |--------------------------------------------------------------------------
     * | DashboardController
     * |--------------------------------------------------------------------------
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    //admin dashboard method
    public function dashboard(){

        if (!$this->filter($this->permission)) {
            
            /*return redirect('/errors/admincustom');*/
        } 

        date_default_timezone_set('Asia/Kolkata');
       // $date=date('Y-m-d', strtotime('-7 days')); 
       // $date1=date("Y-m-d",strtotime("-1 month"));
         $firstday_previous=date("Y-n-j", strtotime("first day of previous month"));
         $lastday_previous=date("Y-n-j", strtotime("last day of previous month"));

            if(Auth::user()->id!=null){
                $id=Auth::user()->id;
                //$ClientUsers=ClientUser::with('clients','clientss')->where('user_id','=',$id)->get();
                //dd($ClientUsers);
                $parent_id= DB::table('clients')->where('parent_id','=',0)->where('id','!=','0')->get();
                foreach($parent_id as $val){
                    $arr[] = $parent_ids= DB::table('clients')->where('parent_id','=',$val->id)->where('id','!=','0')->get();
                }

               $i=0;
                foreach($arr as $va){
                    $j=0;
                    $arr1[$i][]=$parent_id[$i];

                    foreach($va as $ke){
                        $test=array();
                        $arr1[$i][] = $ke;
                        $test[] = DB::table('clients')->where('parent_id','=',$ke->id)->get();
                        foreach($test as $a1){
                            if(count($test)>0){
                                foreach($a1 as $a2){
                                    $arr1[$i][] = $a2;
                                }
                            }  
                        }
                    }
                   $i++; 
                }
                foreach($arr1 as $va){
                    foreach($va as $aa){
                        $array[] = $aa;
                    } 
                }
                $parent_ids = $array;
                
                $users = User::where('last_login', '>', date("Y-m-d", strtotime("-1 months")))->orderby('last_login', 'DESC')->get();
                
                if($id=='1') {
                    $quote =QuoteManagement::with('Username')->groupBy('user_id')->select('user_id', DB::raw('count(*) as total'))->get();
                    $total_Quotes = QuoteManagement::whereBetween('date_created', array($firstday_previous,$lastday_previous))->get();

                    $total_Quotes = QuoteManagement::all();               
                }else{
                    $quote =QuoteManagement::with('Clientname')->where('user_id',$id)->get();
                }
                $userRole = RoleUser::where('user_id' , '=', Auth::user()->id)->first();
                if($userRole->role_id ==1 ){
                    //dd($quote);
                    return view('/dashboard',compact('users','parent_ids','quote','total_Quotes'));
                }else{
                     Toast::error("Authorization  failed:You  don't have permission.", "Error");
                    Auth::logout();
                    return redirect ('/login');
                }
            }
        
        
    }
    
    public function QuoteClientSummary($id=null){
        if ($id!=null) {
             $quote =QuoteManagement::with('Clientname')->where('user_id',$id)->groupBy('client_id')->select('client_id',DB::raw('count(*) as total'))->get();

            if (!empty( $quote)) {
                return view('/QuoteClientSummary',compact('quote'));
            }else{
                return redirect('/admin/dashboard'); 
            }           
        }else{
            return redirect('/admin/dashboard');
        }
    }

   /* public function pagenotfound()
       {
            if ($this->filter($this->permission))
         //   dd($this);
            {
                return view('errors.admincustom');
            }else{

                return view('errors.custom');
            }
        }    
        
            public function redirect()
            {
                if ($this->filter($this->permission))
                {
                    return  redirect('/dashboard');
                }
                else{
                    return redirect ('/admin/dashboard');
                } 

            }
           */
       }

