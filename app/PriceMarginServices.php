<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceMarginServices extends Model
{
	public $table = "price_margin_services";
	protected $fillable = [
	'id',
	'client_id', 
	'type_id',
	'option_id',
	'value',
	'status',
	'country_id'
	];

	public function Clientname(){
		return $this->hasOne('App\Client','id','client_id');
	}
	public function ProductOtionType(){
		return $this->hasOne('App\ProductOption','id','type_id');
	}
	public function ProductOtionServices(){
		return $this->hasOne('App\ProductOption','id','option_id');
	}  
    public function CountryServices() {
        return $this->hasOne('App\Country', 'id', 'country_id');
    }  
}
