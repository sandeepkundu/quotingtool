<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuoteCollectionItem extends Model
{
    protected $table = 'quote_collection_items';
	     protected $fillable = [
	    'id',
	    'quote_id',
	    'collection_id',
	    'quote_item_id',
	    'created_at',
	    'updated_at',
	    'status',
	    
	    ];
}
