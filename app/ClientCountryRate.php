<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientCountryRate extends Model
{
    public $table = "client_country_rates";
	protected $fillable = [
			'id',
			'expires', 
			'inherited', 
			'client_id', 
			'country_id', 
			'exchangerate',
			'created_at',
			'updated_at'
        ];
}
