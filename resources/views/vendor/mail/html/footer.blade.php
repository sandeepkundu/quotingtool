
	<footer class="footer">
	      <div class="container">
	        <div class="row">
	            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
	                <div class="copyright" style="text-align: center;">
	                    {{date("Y")}} &copy; Feilding Technology Pte Ltd.
	                </div>
	            </div>
	        </div>
	      </div>
	</footer>   	 
      
  
