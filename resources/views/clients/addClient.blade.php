@extends('layouts.default')
@section('content')
<style>
	.form-control:focus{
		border: 1px solid #66afe9 !important;
	}
	.bg-warning{
		background-color:#00B1E1 !important;
		border:1px solid #00B1E1 !important;
	}
	input.no-border-right:focus, textarea.no-border-right:focus{
		border: 1px solid #66afe9 !important;
	}

</style>
<div class="body-content animated fadeIn">
	<div class="row">
		<div class="col-lg-12">
			<div class="panel rounded shadow">
				<div class="panel-heading">
					<div class="pull-left">
						<h3 class="panel-title">ADD NEW CLIENT</h3>
					</div>
					<div class="clearfix"></div>
				</div>
				
				<div class="panel-body no-padding">
					<form class="form-horizontal mt-10" id="commentForm" method="post" action="{{URL::to('/')}}/clients/insertClient" >
						{{ csrf_field() }}
						<div class="form-body">
							<div class="form-group">
								<div class="row">
									<div class="col-lg-12" style="margin-bottom: -20px;">
										<div class="col-lg-6">
											<label for="name" class="col-sm-3 control-label">Name<span style="color:red;">*</span></label>
											<div class="col-sm-9">
												<input value="{!!old('name')!!}" class=" form-control" id="name" name="name" minlength="2" type="text" placeholder="Enter client name"> 
												@if ($errors->has('name'))
												<span class="help-block" style="color:red;">
													<strong>{{ $errors->first('name') }}</strong>
												</span>
												@endif
												&nbsp
											</div>
										</div>
										<div class="col-lg-6">
											<label for="client_type" class="col-sm-3 control-label">Client Type<span style="color:red;">*</span></label>
											<div class="col-sm-9" style="padding-bottom:20px;">
												<select id="client_type" name="client_type" class="form-control" >
													<option value="">--Select Client Type--</option>
													<option value="enterprise" {{(old("client_type") == 'enterprise') ? "selected":"" }}>Enterprise</option>
													<option value="store" {{(old("client_type") == 'store') ? "selected":"" }}>Store</option>

												</select>
												@if ($errors->has('client_type'))
												<span class="help-block" style="color:red;">
													<strong>{{ $errors->first('client_type') }}</strong>
												</span>
												@endif
												&nbsp
											</div>
										</div>
									</div>
									
									<div class="col-lg-12" style="margin-bottom: -20px;">
										<div class="col-lg-6">
											<label for="parent_id" class="col-sm-3 control-label">Parent</label>
											<div class="col-sm-9">
												<div class="btn-group hierarchy-select" data-resize="auto" id="example-one">
													<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
														<span class="selected-label pull-left">&nbsp;</span>
														<span class="caret"></span>
														<span class="sr-only">Toggle Dropdown</span>
													</button>
													<div class="dropdown-menu open">
														<div class="hs-searchbox">
															<input type="text" class="form-control" autocomplete="off">
														</div>
														<ul class="dropdown-menu inner enterprise" role="menu">
															<li data-value="0" data-level="1" class="aa" data-default-selected="">
																<a href="#">Select Client</a>
															</li>
															@foreach($parent_ids as $val)
															<li class="par" data-value="{{$val['id']}}" data-level="{{$val['level']}}">
																<a href="#">{{$val['name']}}</a>
															</li>
															@endforeach
														</ul>

													</div>
													<input class="hidden hidden-field" name="parent_id" id="parent_id" aria-hidden="true" type="text"/>
												</div>			
											</div>
										</div>
										<div class="col-lg-6">
											<label for="country" class="col-sm-3 control-label">Country<span style="color:red;">*</span></label>
											<div class="col-sm-9" style="padding-bottom:20px;">
												<select id="country" name="country" class="form-control">
													<option value=""> -- Select Country -- </option>
													@foreach ($countries as $code)
													<option value="{{ $code->id }}"  {{ (old('country') == $code->id) ? "selected":""}}> {{ $code->name }}</option>
													@endforeach             
												</select>
												@if ($errors->has('country'))
												<span class="help-block" style="color:red;">
													<strong>{{ $errors->first('country') }}</strong>
												</span>
												@endif
												&nbsp
											</div>
										</div>
									</div>
									
									
									<div class="col-lg-12">
										<div class="col-lg-6">
											<label for="region" class="col-sm-3 control-label">Region<span style="color:red;">*</span></label>
											<div class="col-sm-9">
												<select id="region" name="region" class="form-control">
													<option value=""> -- Select Country Region -- </option>
												</select>
												@if ($errors->has('region'))
												<span class="help-block" style="color:red;">
													<strong>{{ $errors->first('region') }}</strong>
												</span>
												@endif
												&nbsp
											</div>
										</div>
										
										<div class="col-lg-6">
											<label for="ave_collect_size" class="col-sm-3 control-label">Avg Collect Size</label>
											<div class="col-sm-9">
												<input value="{!!old('ave_collect_size')!!}" type="text" name="ave_collect_size" class="form-control" id="ave_collect_size">
												
												@if ($errors->has('ave_collect_size'))
												<span class="help-block" style="color:red;">
													<strong>{{ $errors->first('ave_collect_size') }}</strong>
												</span>
												@endif
												&nbsp
											</div>
										</div>
									</div>
									
									
									<div class="col-lg-12">
										<div class="col-lg-6">
											<label for="margin_oem_product" class="col-sm-3 control-label">Margin OEM Product</label>
											<div class="col-sm-9">
												<input  value="{!!old('margin_oem_product')!!}" class="form-control" id="margin_oem_product" name="margin_oem_product" type="text"> 
												@if ($errors->has('margin_oem_product'))
												<span class="help-block" style="color:red;">
													<strong>{{ $errors->first('margin_oem_product') }}</strong>
												</span>
												@endif
												&nbsp
											</div>
										</div>
										
										<div class="col-lg-6">
											<label for="margin_oem_logistic" class="col-sm-3 control-label">Margin OEM Logistic</label>
											<div class="col-sm-9">
												<input value="{!!old('margin_oem_logistic')!!}" type="text" name="margin_oem_logistic" class="form-control" id="margin_oem_logistic">
												@if ($errors->has('margin_oem_logistic'))
												<span class="help-block" style="color:red;">
													<strong>{{ $errors->first('margin_oem_logistic') }}</strong>
												</span>
												@endif
												&nbsp
											</div>
										</div>
									</div>
									
									
									<div class="col-lg-12">
										<div class="col-lg-6">
											<label for="margin_oem_service" class="col-sm-3 control-label">Margin OEM Service</label>
											<div class="col-sm-9">
												<input value="{!!old('margin_oem_service')!!}" class="form-control" id="margin_oem_service" name="margin_oem_service" type="text"> 
												
												@if ($errors->has('margin_oem_service'))
												<span class="help-block" style="color:red;">
													<strong>{{ $errors->first('margin_oem_service') }}</strong>
												</span>
												@endif
												&nbsp
											</div>
										</div>
										
										<div class="col-lg-6">
											<label for="status" class="col-sm-3 control-label">Enabled<!-- <br><span>(Active/Not Active)</span> --></label>
												<!--div class="col-sm-9">
													<input name="status" type="radio" value="active" checked='checked'> Active<br>
													<input name="status" type="radio" value="deactive"> Deactive
												</div-->
												<div class="col-sm-9">
													<div class="ckbox ckbox-info">
														<input id="checkbox-info1" checked="checked" type="checkbox" name="status">
														<label for="checkbox-info1"> </label>
													</div>
												</div>
											</div>
										</div>
										
									</div>
								</div>

								<div class="form-group"></div>
							</div>
							<div class="form-footer">
								<div class="col-sm-offset-5">
									<button class="btn btn-color btn-hover" type="submit">Submit</button>
									<a class="btn btn-danger" href="{{URL::to('/')}}/clients/index">Cancel</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--==============================Modal Popup=======================-->
	<div class="modal modal-success fade in" id="modal-bootstrap-tour" tabindex="-1" role="dialog" style="display: none;">
		<div class="modal-dialog" role="document" style="margin: 150px auto;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" id="close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<h4 class="modal-title"><span class="username">Alert!!!</span></h4>
				</div>
				<div class="modal-body">
					You can not make the parent of this store because this is already child of another store.
					
				</div>
			</div>
		</div>
	</div>
	<!--==============================Modal Popup=======================--> 
	<style type="text/css">
		#ave_collect_size{
			border-right:1px solid rgb(221, 221, 221) !important;
		}
		#ave_collect_size:focus{
			border-right:1px solid #66afe9 !important
		}
		#example-one{
			width: 100%;
		}
		.dropdown-menu li.active:hover a, .dropdown-menu li.active:focus a, .dropdown-menu li.active:active a{
			background-color:#00B1E1 !important;
		}
		.dropdown-menu li.active a{
			background-color:#00B1E1 !important;
		}
		.btn-default.dropdown-toggle.btn-default{
			background-color: white;
		}
		.btn-default.active.focus, .btn-default.active:focus, .btn-default.active:hover, .btn-default:active.focus, .btn-default:active:focus, .btn-default:active:hover, .open>.dropdown-toggle.btn-default.focus, .open>.dropdown-toggle.btn-default:focus, .open>.dropdown-toggle.btn-default:hover{
			background-color: white;
		}
		.dropdown-menu li > a:hover:before{
			border-left:3px solid #00B1E1;
		}
		.modal-success .modal-header {
			background-color: #00B1E1 !important;
			border:1px solid #00B1E1;
			border:none;
		}
		.modal-success .modal-header:before {
			content: ""; 
			border: 1px solid #00B1E1 !important; 

		}
	</style>

	
	<link href="{{asset('css/hierarchy-select.min.css')}}" rel="stylesheet">
	@section('js')

	<script src="{{ asset('js/hierarchy-select.min.js') }}"></script>
	<script>
		$(document).ready(function(){

			$('#example-one').hierarchySelect();

			$('#country').on('change', function() {
				var countryId = $(this).val();
				if(countryId != ''){
					$.ajax({
						type:'get',
						url:'country_region',
						data:{'id':countryId},
						dataType:'JSON',
						success:function(obj){
							var option='';
							for (var item in obj) {
								var obj1 = obj[item];
								if (obj1.length > 0) {
									for (var i in obj1) {
										option += '<option value="' + obj1[i].id + '"> ' + obj1[i].name + ' </option>';
									}
								}else{

								/*$('.modal-body').html('<a href="{{URL::to('/')}}/countries/editCountry/'+countryId +'" target="blank" class="btn btn-color">Click</a>');
								$('#modal-bootstrap-tour').modal('show');*/

							}							
						}
						$('#region').html(option);
					},
					error:function(error){
						alert('error block');
					}
				});
				}
			});


			if($("#country").attr("selectedIndex") != 0) {
				var countryId = $("#country option:selected").val();
			//alert(countryId);
			if(countryId != ''){
				$.ajax({
					type:'get',
					url:'country_region',
					data:{'id':countryId},
					dataType:'JSON',
					success:function(obj){
						var option='';
						for (var item in obj) {
							var obj1 = obj[item];
							for (var i in obj1) {
								option += '<option value="' + obj1[i].id + '"> ' + obj1[i].name + ' </option>';
							}
						}
						$('#region').html(option);
					},
					error:function(error){
						alert('error block');
					}
				});
			}
		}
		
	});
</script>

@endsection
<style>
	.tooltip {
		position: relative;
		display: inline-block;
		border-bottom: 1px dotted black;
	}

	.tooltip .tooltiptext {
		visibility: hidden;
		width: 120px;
		background-color: black;
		color: #fff;
		text-align: left;
		border-radius: 6px;
		padding: 5px 0;
		margin: 5px 0;

		/* Position the tooltip */
		position: absolute;
		z-index: 1;
	}

	.tooltip:hover .tooltiptext {
		visibility: visible;
	}
</style>
@endsection
