@extends('layouts.default') 
@section('content')
<style>
    .form-control:focus{
        border: 1px solid #66afe9 !important;
    }
    .bg-warning{
        background-color:#00B1E1 !important;
        border:1px solid #00B1E1 !important;
    }
    input.no-border-right:focus, textarea.no-border-right:focus{
        border: 1px solid #66afe9 !important;
    }
</style>
<div class="body-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">EDIT CLIENTS</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body no-padding">
                    <form class="form-horizontal mt-10" id="commentForm" method="post" action="{{URL::to('/')}}/clients/updateClient">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $clients->id }}">
                        <div class="form-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-12" style="margin-bottom: -20px;">
                                        <div class="col-lg-6">
                                            <label for="name" class="col-sm-3 control-label">Name<span style="color:red;">*</span></label>
                                            <div class="col-sm-9">
                                                <input value="{{ $clients->name }}" class=" form-control" id="name" name="name" minlength="2" type="text" placeholder="Enter client name"> 
                                                @if ($errors->has('name'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="client_type" class="col-sm-3 control-label">Client Type<span style="color:red;">*</span></label>
                                            <div class="col-sm-9" style="padding-bottom:20px;">
                                                <select id="client_type" name="client_type" class="form-control">

                                                    <option value=""> -- Select Client Type -- </option>
                                                    <option value="enterprise" {{($clients->client_type == 'enterprise') ? 'selected' : ''}}>Enterprise</option>
                                                    <option value="store" {{($clients->client_type == 'store') ? 'selected' : ''}}>Store</option>
                                                </select>
                                                @if ($errors->has('client_type'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('client_type') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12" style="margin-bottom: -20px;">
                                        <div class="col-lg-6">
                                            <label for="parent_id" class="col-sm-3 control-label">Parent</label>
                                            <div class="col-sm-9">
                                                <div class="btn-group hierarchy-select" data-resize="auto" id="example-one">
                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <span class="selected-label pull-left">&nbsp;</span>
                                                        <span class="caret"></span>
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                    </button>
                                                    <div class="dropdown-menu open">
                                                        <div class="hs-searchbox">
                                                            <input type="text" class="form-control" autocomplete="off">
                                                        </div>
                                                        <ul class="dropdown-menu inner enterprise" role="menu">
                                                            <li data-value="0" data-level="1" class="aa">
                                                                <a href="#">Select Client</a>
                                                            </li>
                                                            @foreach($parent_ids as $val)				                                           
                                                            <li class="par" data-value="{{$val['id']}}" data-level="{{$val['level']}}" {{ ($val['id'] == $clients->parent_id)? 'data-default-selected=""' : '' }}>
                                                                <a href="#">{{$val['name']}}</a>
                                                            </li>                                                           
                                                            @endforeach
                                                        </ul>

                                                    </div>
                                                    <input class="hidden hidden-field" name="parent_id" readonly="readonly" value="0" aria-hidden="true" type="text"/>
                                                </div>			
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="country" class="col-sm-3 control-label">Country<span style="color:red;">*</span></label>
                                            <div class="col-sm-9" style="padding-bottom:20px;">
                                                <select id="country" name="country" class="form-control">
                                                    <option value=""> -- Select Country -- </option>
                                                    @foreach ($countries as $code)
                                                    
                                                    <option value="{{ $code->id }}" {{($clients->countryClient->id == $code->id) ? 'selected' : ''}}>{{ $code->name }}</option>
                                                    @endforeach             
                                                </select>
                                                @if ($errors->has('country'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('country') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="region" class="col-sm-3 control-label">Region<span style="color:red;">*</span></label>
                                            <div class="col-sm-9">
                                                <select id="region" name="region" class="form-control">
                                                    <option value=""> -- Select Country Region -- </option>
                                                    @foreach ($country_region as $code)
                                                    
                                                    <option value="{{ $code->id }}" {{($clients->region == $code->id) ? 'selected' : ''}}>{{ $code->name }}</option>
                                                    @endforeach 
                                                </select>
                                                @if ($errors->has('region'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('region') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <label for="ave_collect_size" class="col-sm-3 control-label">Avg Collect Size</label>
                                            <div class="col-sm-9">


                                                <input value="{{ $clients->ave_collect_size }}" type="text" name="ave_collect_size" class="form-control" id="ave_collect_size" >

                                                

                                                @if ($errors->has('ave_collect_size'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('ave_collect_size') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="margin_oem_product" class="col-sm-3 control-label">Margin OEM Product</label>
                                            <div class="col-sm-9">
                                                <input value="{{ $clients->margin_oem_product }}" class="form-control" id="margin_oem_product" name="margin_oem_product" type="text" > 

                                                @if ($errors->has('margin_oem_product'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('margin_oem_product') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <label for="margin_oem_logistic" class="col-sm-3 control-label">Margin OEM Logistic</label>
                                            <div class="col-sm-9">
                                                <input value="{{ $clients->margin_oem_logistic }}" type="text" name="margin_oem_logistic" class="form-control" id="margin_oem_logistic"
                                                >
                                                @if ($errors->has('margin_oem_logistic'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('margin_oem_logistic') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="margin_oem_service" class="col-sm-3 control-label">Margin OEM Service</label>
                                            <div class="col-sm-9">
                                                <input  value="{{ $clients->margin_oem_service }}" class="form-control" id="margin_oem_service" name="margin_oem_service" type="text"> 

                                                @if ($errors->has('margin_oem_service'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('margin_oem_service') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <label for="status" class="col-sm-3 control-label">Enabled<!-- <br><span>(Active/Not Active)</span> --></label>
                                            <div class="col-sm-9">
                                                <div class="ckbox ckbox-info">
                                                    <input id="checkbox-info1" {{($clients->status == 'active') ? 'checked' : ''}} type="checkbox" name="status">
                                                    <label for="checkbox-info1"> </label>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <!-- ================ADD Exchange Rate========================== -->
                                    <br/><div class="col-sm-12">
                                    <div class="panel-heading">
                                        <div class="pull-left">
                                            <h3 class="panel-title">ADD EXCHANGE RATE</h3>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div><br/>
                                </div>

                                <div class="col-lg-12">
                                    <div class="col-lg-3 col-sm-3 col-md-3">
                                        <label for="country_ex" style="text-align: left;" class="col-sm-12 control-label">Country<span style="color:red;">*</span></label>
                                        <div class="col-sm-12">
                                            <select id="country_ex" class="form-control">
                                                <option value=""> -- Select Country -- </option>
                                                @foreach ($countries as $code)

                                                <option value="{{ $code->id }}">{{ $code->name }}</option>
                                                @endforeach             
                                            </select>

                                            <span class="help-block" style="color:red;">
                                                <strong id="country_ex_eoor"></strong>
                                            </span>
                                            @if ($errors->has('country_exchange'))
                                            <span class="help-block" style="color:red;">
                                                <strong id="country_ex_error">The Country field is required.</strong>
                                            </span>
                                            @endif
                                            &nbsp
                                        </div>
                                    </div>

                                    <div class="col-lg-3 col-sm-3 col-md-3">
                                        <label for="date_ex" style="text-align: left;" class="col-sm-12 control-label">Expiry Date</label>
                                        <div class="col-sm-12" style="padding-bottom:20px;">

                                        <input type="date" id="date_ex" class="form-control">
                                         <span class="help-block" style="color:red;">
                                            <strong id="date_ex_err"></strong>
                                        </span>
                                        @if ($errors->has('date_ex'))
                                        <span class="help-block" style="color:red;">
                                            <strong id="date_ex_err1">{{ $errors->first('date_ex') }}</strong>
                                        </span>
                                        @endif
                                        &nbsp
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-3 col-md-3">
                                    <label for="value_ex" style="text-align: left;" class="col-sm-12 control-label">Exchange Rate</label>
                                    <div class="col-sm-12" style="padding-bottom:20px;">

                                     <input type="text" id="value_ex" class="form-control" placeholder="Exchange Rate">
                                     <span class="help-block" style="color:red;">
                                        <strong id="value_ex_error"></strong>
                                    </span>
                                    @if ($errors->has('value_ex'))
                                    <span class="help-block" style="color:red;">
                                        <strong id="err2">{{ $errors->first('value_ex') }}</strong>
                                    </span>
                                    @endif
                                    &nbsp
                                </div>
                            </div>
        
							<div class="col-lg-3 col-sm-3 col-md-3">                                  
								<div class="col-sm-12" style="padding-bottom:20px;margin-top: 20px; ">
									<button type="button" id="add_ex_btn" class="btn btn-color btn-hover btn-block">Add</button>
								</div>
							</div>
                    </div>
                    <div class="table-responsive mb-20 col-sm-12">
                        <button type="button" style="margin-bottom: 10px;" class="delete-row btn btn-danger" style="margin-bottom:4px;">Delete Row</button>
                        <table class="table table-striped table-success table-middle table-project-clients region_table" role="grid" aria-describedby="datatable-client-all_info" >
                            <thead>
                                <tr>
                                    <th width="10px;">Select</th>
                                    <th>Country</th>
                                    <th>Date</th>
                                    <th>Value</th>                                    
                                </tr>

                            </thead>
                            <tbody>
							    @if(count($ClientCountryRates) > 0)
                                @foreach($ClientCountryRates as $ClientCountryRate)
                                    <tr>
                                        <td>
                                            <input type='checkbox' name='record'>
                                        </td>

                                        <td id='country_exchange'>{{$ClientCountryRate->country_name}}<input type='hidden' class='country_exchange_hidden' style='width:50%;' name='country_exchange[]'  value='{{$ClientCountryRate->country_id}}'/></td>

                                        <td>{{$ClientCountryRate->expires}}<input type='hidden' name='date_ex[]' class='order_hidden form-control' value='{{$ClientCountryRate->expires}}'/></td>

                                        <td>{{$ClientCountryRate->exchangerate}}<input type='hidden' name='value_ex[]' class='value_ex_hidden form-control' value='{{$ClientCountryRate->exchangerate}}'/></td>

                                    </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>

                    </div>

                    <!-- =======================QUOTE STEP ITEMS========================= -->
                </div>
            </div>

        </div>
        <div class="form-footer">
            <div class="col-sm-offset-5">
                <button class="btn btn-color btn-hover" type="submit">Submit</button>
                <a class="btn btn-danger" href="{{URL::to('/')}}/clients/index">Cancel</a>
            </div>
        </div>

    </form>
</div>
</div>
</div>
</div>
</div>
<!--==============================Modal Popup=======================-->
<div class="modal modal-success fade in" id="modal-bootstrap-tour" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-dialog" role="document" style="margin: 150px auto;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title"><span class="username">Alert!!!</span></h4>
            </div>
            <div class="modal-body">
                You can not make the parent of this store because this is already child of another store.

            </div>
        </div>
    </div>
</div>
<!--==============================Modal Popup=======================--> 
<link href="{{asset('css/hierarchy-select.min.css')}}" rel="stylesheet">
<style type="text/css">
    #ave_collect_size{
        border-right:1px solid rgb(221, 221, 221) !important;
    }
    #ave_collect_size:focus{
        border-right:1px solid #66afe9 !important
    }
    #example-one{
        width: 100%;
    }
    .dropdown-menu li.active:hover a, .dropdown-menu li.active:focus a, .dropdown-menu li.active:active a{
        background-color:#00B1E1 !important;
    }
    .dropdown-menu li.active a{
        background-color:#00B1E1 !important;
    }
    .btn-default.dropdown-toggle.btn-default{
        background-color: white;
    }
    .btn-default.active.focus, .btn-default.active:focus, .btn-default.active:hover, .btn-default:active.focus, .btn-default:active:focus, .btn-default:active:hover, .open>.dropdown-toggle.btn-default.focus, .open>.dropdown-toggle.btn-default:focus, .open>.dropdown-toggle.btn-default:hover{
        background-color: white;
    }
    .dropdown-menu li > a:hover:before{
        border-left:3px solid #00B1E1;
    }
    .modal-success .modal-header {
        background-color: #00B1E1 !important;
        border:1px solid #00B1E1;
        border:none;
    }
    .modal-success .modal-header:before {
        content: ""; 
        border: 1px solid #00B1E1 !important; 

    }
</style>

@section('js')
<script src="{{ asset('js/hierarchy-select.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $('#example-one').hierarchySelect();

        $('#country').on('change', function () {
            var countryId = $(this).val();
            if (countryId != '') {
                $.ajax({
                    type: 'get',
                    url: SITE_URL + '/clients/country_region',
                    data: {'id': countryId},
                    dataType: 'JSON',
                    success: function (obj) {
                        var option = '';
                        for (var item in obj) {
                            var obj1 = obj[item];
                            for (var i in obj1) {
                                option += '<option value="' + obj1[i].id + '"> ' + obj1[i].name + ' </option>';
                            }
                        }
                        $('#region').html(option);
                    },
                    error: function (error) {
                        alert('error block');
                    }
                });
            }
        });
    });

</script>
<script>

        var i = 0;

        $("#add_ex_btn").click(function () {
            i = i + 1;
            var country_ex = $("#country_ex").val();
            var date_ex = $("#date_ex").val();
            var value_ex = $("#value_ex").val();
                    
            var country_ex_text =  $("#country_ex :selected").text();
            var group_code_hidden = $('.country_exchange_hidden').val();

            var row = "<tr><td><input type='checkbox' name='record'></td><td id='country_exchange'>"+country_ex_text+"<input type='hidden' class='country_exchange_hidden' style='width:50%;' name='country_exchange[]'  value='" + country_ex + "'/>" + "</td><td>" + date_ex + "<input type='hidden' name='date_ex[]' class='order_hidden form-control' value='" + date_ex + "'/>" + "</td><td>" + value_ex + "<input type='hidden' name='value_ex[]' class='value_ex_hidden form-control' value='" + value_ex + "'/>" + "</td></tr>";

            var country_ex_array = [];
            $(".country_exchange_hidden").each(function(){
                country_ex_array.push($(this).val());
            });

            if (country_ex == "") {
                $('#country_ex_eoor').text('The country field is required');
                $('#country_ex').focus();
                return false;

            } else if (value_ex != '' && !$.isNumeric(value_ex)) {
                $('#value_ex_error').text('The order field must be numeric');
                $('#value_ex').focus();
                $("#date_ex_err").text('');
                $("#country_ex_eoor").text('');
                return false;

            }else if($.inArray(country_ex , country_ex_array)  !== -1 ){
                $('#country_ex_eoor').text('Country is already exist');
                $('#country_ex').focus();
                 $("#date_ex_err").text('');
                 $("#value_ex").text('');
                return false;

            }else {
                $('.delete-row').show();
                $('.region_table').show();
                $("#date_ex_err").text('');
                $("#country_ex_eoor").text('');
                $('#value_ex_error').text('')
                $("table tbody").append(row);
                $("#order_error").text('');
            };

           /* if (group_code_hidden != '') {
                $("#err1").text('');
            }
            if (order_hidden != '') {
                $("#err2").text('');
            }*/
        });
        // Find and remove selected table rows
        $(".delete-row").click(function () {
            $("table tbody").find('input[name="record"]').each(function () {
                if ($(this).is(":checked")) {
                    $(this).parents("tr").remove();
                }
            });
        });


    $('#checkbox-infoEx').click(function(){
        if($(this).is(":checked")){
            $(this).attr('value','1');
        }else{
            $(this).attr('value','0');            
        }
    })
</script>

@endsection

@endsection
