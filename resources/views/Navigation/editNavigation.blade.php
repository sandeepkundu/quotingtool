@extends('layouts.default') @section('content')
<!-- START @PAGE LEVEL STYLES -->
<link href="{{ asset('css/bower_components/fontawesome/css/font-awesome.min.css')}}" rel="stylesheet">
<link href="{{ asset('css/bower_components/animate.css/animate.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/bower_components/bootstrap-wysihtml5/src/bootstrap-wysihtml5.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/bower_components/summernote/dist/summernote.css')}}" rel="stylesheet">
<link href="{{ asset('css/bower_components/dropzone/downloads/css/dropzone.css') }}" rel="stylesheet">
<link href="{{ asset('css/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css') }}" rel="stylesheet">

<link href="{{ asset('assets/global/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet">
<link href="{{ asset('assets/global/plugins/bower_components/jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/global/plugins/bower_components/chosen_v1.2.0/chosen.min.css') }}" rel="stylesheet">

<style type="text/css">
    .note-editor .note-editable
    {
        background: #fff;
    }
    .note-editor.fullscreen {
        position: fixed;
        top: 69px;
        left: 239px;
        bottom: 20px;
        right: 157px;
        z-index: 1050;
    }
    .fullscreen{
        width: 81% !important;
        height: 80% !important;
    }
    .note-editable{
        height: 80% !important;
    }

</style>
<!--/ END PAGE LEVEL STYLES -->
<div class="body-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">ADD CMS PAGE MANAGEMENT</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body no-padding">
                    <form class="form-horizontal mt-10" id="commentForm" method="post" action="{{URL::to('/')}}/Navigation/updateNavigation" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $Navigation->id }}">
                        <div class="form-body">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="col-sm-6">
                                        <label for="" class="col-sm-3 control-label">Name<span style="color:red;">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" name="name" id="name" value="{{$Navigation->name}}" class="form-control"/ placeholder="Name">

                                                   @if ($errors->has('name'))
                                                   <span class="help-block" style="color:red;">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                            &nbsp;
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="display_name" class="col-sm-3 control-label">Display Name<span style="color:red;">*</span></label>
                                        <div class="col-sm-9">
                                            <input value="{{$Navigation->display_name}}"  type="text" name="display_name" class="form-control" id="display_name" placeholder="Display Name">

                                            @if ($errors->has('display_name'))
                                            <span class="help-block" style="color:red;">
                                                <strong>{{ $errors->first('display_name') }}</strong>
                                            </span>
                                            @endif
                                            &nbsp;
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-6">
                                        <label for="" class="col-sm-3 control-label">Section<span style="color:red;">*</span></label>
                                        <div class="col-sm-9">
                                           
                                                   <select name="section" id="section" class="form-control">
                                                       <option value="">--Select Section--</option>
                                                       <option value="Header" {{($Navigation->section == 'Header') ? "selected":"" }}>Header</option>
                                                       <option value="Footer" {{($Navigation->section == 'Footer') ? "selected":"" }}>Footer</option>
                                            </select>   
                                                   @if ($errors->has('section'))
                                                   <span class="help-block" style="color:red;">
                                                <strong>{{ $errors->first('section') }}</strong>
                                            </span>
                                            @endif
                                            &nbsp;
                                        </div>
                                    </div>
                                     <div class="col-sm-6">
                                        <label for="" class="col-sm-3 control-label">Position<span style="color:red;">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" name="position" id="position" value="{{$Navigation->position}}" class="form-control" placeholder="Position">

                                            @if ($errors->has('position'))
                                            <span class="help-block" style="color:red;">
                                                <strong>{{ $errors->first('position') }}</strong>
                                            </span>
                                            @endif
                                            &nbsp;
                                        </div>
                                    </div>
                                 
                                </div>
                                <div class="col-sm-12">
                                   
                                    <div class="col-lg-6">
                                        <label for="status" class="col-sm-3 control-label">Enabled</label>

                                        <div class="col-sm-9">
                                            <div class="ckbox ckbox-info">
                                                <input id="checkbox-info1" type="checkbox" name="status" {{($Navigation->status == 'Active') ? 'checked' : ''}}>
                                                <label for="checkbox-info1"> </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <label for="description" class="col-sm-2 control-label" style="font-weight:800; text-align: left;">Description</label>
                                        <div class="col-sm-12">
                                            <textarea type="text" name="page_content" class="form-control" id="summernote-textarea"
                                                      >{{$Navigation->page_content}}</textarea>
                                            @if ($errors->has('page_content'))
                                            <span class="help-block" style="color:red;">
                                                <strong>{{ $errors->first('page_content')}}</strong>
                                            </span>
                                            @endif
                                            &nbsp;
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- /.form-group -->
                            <div class="form-group"></div>
                            <!-- /.form-group -->
                        </div>
                        <!-- /.form-body -->
                        <div class="form-footer">
                            <div class="col-sm-offset-5">
                                <button class="btn btn-color btn-hover" type="submit">Submit</button>
                                <a class="btn btn-danger" href="{{URL::to('/')}}/Navigation/index">Cancel</a>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <!-- /.panel -->
            <!--/ End horizontal form -->
        </div>
    </div>
</div>
@section('js')
<!-- START @PAGE LEVEL SCRIPT -->

<script src="{{asset('assets/global/plugins/bower_components/bootstrap-wysihtml5/lib/js/wysihtml5-0.3.0.min.js')}}"></script>
<script src="{{asset('assets/global/plugins/bower_components/bootstrap-wysihtml5/src/bootstrap-wysihtml5.js')}}"></script>
<script src="{{asset('assets/global/plugins/bower_components/summernote/dist/summernote.min.js')}}"></script>
<script type="text/javascript">
var BlankonFormWysiwyg = function () {

    return {
        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            BlankonFormWysiwyg.bootstrapWYSIHTML5();
            BlankonFormWysiwyg.summernote();
        },
        // =========================================================================
        // BOOTSTRAP WYSIHTML5
        // =========================================================================
        bootstrapWYSIHTML5: function () {
            if ($('#wysihtml5-textarea').length) {
                $('#wysihtml5-textarea').wysihtml5();
            }
        },
        // =========================================================================
        // SUMMERNOTE
        // =========================================================================
        summernote: function () {
            if ($('#summernote-textarea').length) {
                $('#summernote-textarea').summernote();
            }
        }

    };

}();

// Call main app init
BlankonFormWysiwyg.init();
</script>
@endsection

@endsection
