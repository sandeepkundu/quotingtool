<style>
    body {
        background:#ffffff!important;
    }
</style>
<form action="{{url('/')}}/Enterprises/saveEnterpriseQuote" method="post">
    {{csrf_field()}}

    <input type="hidden" id="modelid" name="modelid" value="{{isset($Model->id)?$Model->id:0}}" abc="{{isset($Model->id)?$Model->id:0}}">
    
    <section class="search-bar" style="background-color: #fff!important;padding-top: 25px !important;">
        <div class="container">

            <h3 class="title text-center">Device Specification: 
            	@if (isset($Model) && !empty($Model->model))
				({{$Model->model}})
				@endif
            </h3>
            <div class="device-spec-row row-center">

                <div class="card card-selected card-final" <?php if(!isset($Model->id) || $Model->id == 0) { echo 'onclick="itemedit(' . $Product->id . ');"'; } ?>>
                    <div class="brand-img">
                        <img src="{{ URL::to('product/'.$Product->image) }}" class="img-responsive" />
                    </div>
                    <div class="brand-title productname" id="{{$Product->id}}" >{{$Product->name}}</div>
                    <input type="hidden" name="Product" value="{{$Product->id}}">
                    <input type="hidden" name="product_name" value="{{$Product->name}}">
                </div>

                <div class="card card-selected card-final" <?php if(!isset($Model->id) || $Model->id == 0) { echo 'onclick="itemedit(' . $Brand->id . ');"'; } ?>>
                    <div class="selected-brand-wrap brand-img">
                        <div class="selected-brand" style="background-image:url({{ URL::to('product/'.$Brand->image) }})"></div>
                    </div>
                    <div class="brand-title brandname" id="{{$Brand->id}}">{{$Brand->name}}</div>     
                    <input type="hidden" name="Brand" value="{{$Brand->id}}">
                    <input type="hidden" name="brand_name" value="{{$Brand->name}}">         
                </div>
                
                @if(@$Processor->name!='')
                <div class="card card-selected card-final" <?php if(!isset($Model->id) || $Model->id == 0) { echo 'onclick="itemedit(' . $Processor->id . ');"'; } ?>>
                    <div class="brand-img">
                        {{ @$Processor->type }}
                    </div>
                    <div class="brand-title" data-toggle="tooltip" title="{{$Processor->name}}">{{$Processor->name}}</div>
                    <input type="hidden" name="Processor" value="{{$Processor->id}}">
                    <input type="hidden" name="proccessor_name" value="{{$Processor->name}}">
                </div>
                @endif
                
				<?php 
				if(isset($Screen->name)){
					$addOnclick = '';
					foreach ($stepsProgress as $stp){
						if(strpos($stp->group_code, 'Screen') !== false){
							$addOnclick = 'itemedit('.$Screen->id.')';
						}
					}
					?>
					<div class="card card-selected card-final" onclick="{{$addOnclick}}">
                    <div class="brand-img">{{ @$Screen->type }}</div>
                    <div class="brand-title" >{{@$Screen->name}}</div>
                    <input type="hidden" name="Screen" value="{{ @$Screen->id }}">
                    <input type="hidden" name="screen_name" value="{{ @$Screen->name }}">
					</div>
                <?php } ?>

                <?php 
				if(isset($DVD->name)){
					$addOnclick = '';
					foreach ($stepsProgress as $stp){
						if(strpos($stp->group_code, 'DVD') !== false){
							$addOnclick = 'itemedit('.$DVD->id.')';
						}
					}
					?>
					<div class="card card-selected card-final" onclick="{{$addOnclick}}">
                    <div class="brand-img" >{{ @$DVD->type }}</div>
                    <div class="brand-title" >{{$DVD->name}}</div>
                    <input type="hidden" name="DVD" value="{{ @$DVD->id }}">
                    <input type="hidden" name="dvd_name" value="{{ @$DVD->name }}">
					</div>                
                <?php } ?>

                <?php 
				if(isset($HDD->name)){
					$addOnclick = '';
					foreach ($stepsProgress as $stp){
						if(strpos($stp->group_code, 'HDD') !== false){
							$addOnclick = 'itemedit('.$HDD->id.')';
						}
					}
					?>
					<div class="card card-selected card-final" onclick="{{$addOnclick}}">
                    <div class="brand-img">{{ @$HDD->type }}</div>
                    <div class="brand-title" >{{@$HDD->name}}</div>
                    <input type="hidden" name="HDD" value="{{ @$HDD->id }}">
                    <input type="hidden" name="hdd_name" value="{{ @$HDD->name }}">
					</div>
                <?php } ?>

                <?php 
				if(isset($RAM->name)){
					$addOnclick = '';
					foreach ($stepsProgress as $stp){
						if(strpos($stp->group_code, 'RAM') !== false){
							$addOnclick = 'itemedit('.$RAM->id.')';
						}
					}
					?>
					<div class="card card-selected card-final" onclick="{{$addOnclick}}">
                    <div class="brand-img">{{ @$RAM->type }}</div>
                    <div class="brand-title" >{{@$RAM->name}}</div>
                    <input type="hidden" name="RAM" value="{{ @$RAM->id }}">
                    <input type="hidden" name="ram_name" value="{{ @$RAM->name }}">
					</div>                
                <?php } ?>
                
            </div><!-- device-spec-row -->


            @if(isset($Defects) && count($Defects)>0)
            
            @php $alignLeft = (count($Defects)) > 7 ? "row-center row-left": "row-center"; @endphp
            <h3 class="title text-center">Device Condition</h3>
            <div class="defects-selected {{$alignLeft}}">
                @foreach($Defects as $Defect)
                <?php $def[] = $Defect->id; ?>
                <div class="card card-selected card-final" id="{{$Defect->id}}">
                    <div class="checked">
                        <div class="url_img" style="position:relative"><i class="fa fa-check"></i></div>
                    </div>  
                    <div class="Processors">{{ $Defect->name}}</div>
                    <div class="brand-title" style="margin-bottom:3px;">
                        @if($Defect->description != '')
                        <i class="fa fa-question-circle" name="{{$Defect->id}}" style="margin-right: 5px; float: right;"></i>
                        @endif
                    </div>
                    <input type="hidden" name="QuoteItemDetail" value="{{ @$QuoteItemDetail->id }}">
                    <input type="hidden" name="Defects[]" value="{{ @$Defect->id }}">
                    <input type="hidden" name="defects_name[]" value="{{ @$Defect->name }}">    
                </div>

                @endforeach 
            </div>
            @endif
            
            @if(@$Services->name != '' && $isServiceDefault == 0)
            <h3 class="title text-center">Services</h3>
            <div class="row-center">
                <div class="card card-selected card-final">
                    <div class="brand-img">
                        {{@$Services->name}}
                    </div>
                    <div class="brand-title"></div>
                </div>
            </div> 
            @endif 
            @if(@$Services->name != '')
            <input type="hidden" name="Services" value="{{ @$Services->id }}">
            <input type="hidden" name="services_name" value="{{ @$Services->name }}">
           @endif 
           
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2" style="margin-top:20px">
                    <div class="well well-lg">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label for="product_oem" class="col-sm-4 control-label">Product Margin</label>
                                <div class="col-sm-8">
                                    <input type="text" name="product_oem" id="product_oem" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="service_oem" class="col-sm-4 control-label">Service Margin</label>
                                <div class="col-sm-8">
                                    <input type="text" name="service_oem" id="service_oem" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">        
                                <label for="logistic_oem" class="col-sm-4 control-label">Logistics Margin</label>
                                <div class="col-sm-8">
                                    <input type="text" name="logistic_oem" id="logistic_oem" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">                    
                                <label for="" class="col-sm-4 control-label">Quantity</label>
                                <div class="col-sm-8">
                                    <input type="number" min="1" class="form-control Reference" placeholder="Quantity">
                                </div>
                            </div>
                            <div class="form-group" style="margin-bottom:0">    
                                <div class="col-sm-4"></div>
                                <div class="col-sm-8">
                                    <button type="button" id="" class="btn btn-cta btn-block btn-lg AcceptButton saveQuote">Confirm</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
    </section>
</form>

<!--==============================Edit Item=======================-->
<form method="post" class="Quote_Edit_form" action="{{ URL::to('/') }}/enterprises/quote/step">
    {{csrf_field()}}
    <input type="hidden" name="quote_edit_items" id="quote_edit_item" class="quote_edit_item">
    <input type="hidden" name="quote_item_id" id="quote_item_Id" class="quote_item_Id">

</form>
<!--==============================Edit Item=======================-->

<!--==============================Modal Popup=======================-->
<div class="modal modal-success fade in" id="ProductIdNotFound" style="display: none;">
    <div class="modal-dialog" role="document" style="margin: 150px auto;">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #56bdf1;">
            <button type="button" onclick="window.reload();" id="close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title">Warning</h4>
            </div>
            <div class="modal-body" style="height: auto;overflow: auto;">
                <h4 style="color: #00B1E1;">You can not create quote for this client due to no relavent data found in product table !!!</h4>
            </div>
        </div>
    </div>
</div>
<!--==============================Modal Popup=======================-->

<script type="text/javascript">
    $(function(){
        var clientID = $('#clientID').val();
        var quote_id = $('#quote_id').val();
        var quote_item_id = $('#quote_item_id').val();
        var str = $("form").serialize() + '&client_id=' + clientID + '&quote_id=' + quote_id + '&quote_item_id=' + quote_item_id;
        var SITE = $('#siteurl').val();
        console.log(SITE+'/Enterprises/quoteTotal?'+str);
	
        $.ajax({
            type:'post',
            url:SITE+'/Enterprises/quoteTotal',
            data:str,
            dataType:'JSON',
            success:function(obj){
               if(obj.status == 'ProductIdIssue'){
                $('.productIdTrue').hide();
                $('#ProductIdNotFound').modal('show');
            }else{
                if (obj.status == 'success'){
                    $('#getTotal').text(obj.total);
                    $('#setLastTthToHp').text(obj.ttgToHp);
                    $('.setLastClient').text(obj.lastClient);
                    $('.Reference').val(obj.quantity);   
                    $('.saveQuote').attr('id',obj.Quote_Item_Id );  
                    $('#quote_item_Id').val(obj.Quote_Item_Id );
					$('#product_oem').val(obj.product_oem );
					$('#service_oem').val(obj.service_oem );
					$('#logistic_oem').val(obj.logistic_oem );
					$('#quote_id').val(obj.quote_id);
                }
            }
        },
        error:function(error){
            alert('error block');
        }
    });
    });

    $(function(){
        $('.saveQuote').click(function(){

            $('#loadingDiv').css('display', 'block');
 			var SITE = $('#siteurl').val();
			var quote_id = $('#quote_id').val();
			var QuoteItemId = $(this).attr('id');
            var quantity = $('.Reference').val();
            var product_oem = $('#product_oem').val();
            var service_oem = $('#service_oem').val();
            var logistic_oem = $('#logistic_oem').val();

			console.log(SITE+'/Enterprises/quantityUpdate?quote_id='+quote_id+'&quoteItemId='+QuoteItemId+'&quantity='+quantity+'&product_oem='+product_oem+'&service_oem='+service_oem+'&logistic_oem='+logistic_oem);

            $.ajax({
                type:'get',
                url:SITE+'/Enterprises/quantityUpdate',
                data:{'quote_id':quote_id, 'quoteItemId':QuoteItemId, 'quantity':quantity, 'product_oem':product_oem, 'service_oem':service_oem, 'logistic_oem':logistic_oem},
                dataType:'JSON',
                success:function(obj){
                   window.location.href = "/enterprises/index";
               },
               error:function(error){
                $('#loadingDiv').css('display', 'none');
                alert('error block');
            }
        });
		});

    });
    $(document).ready(function(){
       $('[data-toggle="tooltip"]').tooltip(); 
   });

    function itemedit(quote_edit_item){
        $('.quote_edit_item').val(quote_edit_item);
        $('.Quote_Edit_form').submit();
    }


</script>
