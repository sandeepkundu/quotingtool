<?php
use App\Http\Controllers\HpUsersController;
@$Theme = HpUsersController::Theme_Cookie(); 
$ccFilename = 'pdf.css';
$cssfile = asset('css/'.$ccFilename);
$logo = 'f5-logo.png';
$title = 'F5';
if(isset($Theme)){
	if(!empty($Theme->logo)) {
		$logo = @$Theme->logo;
	}
	if(!empty($Theme->theme_name)) {
		$title = @$Theme->theme_name;
	}
	if(!empty($Theme->theme_folder)) {
		if(file_exists(public_path() . '/' . @$Theme->theme_folder . '/' . $ccFilename)){
			$cssfile = asset('/'). @$Theme->theme_folder . '/' . $ccFilename;
		}
	}
}

function displayQuaoteValues($value, $symbol, $rate){
	$rate = ($rate > 0 && strlen($symbol) > 0) ?  $rate : 1;
	$symbol =  (strlen($symbol) > 0) ? $symbol : '$';
	return $symbol . '' . number_format($value * $rate, 0, '.', ',');
}
?>

<link rel="stylesheet" type="text/css" href="{{$cssfile}}">
<img src="{{asset('images').'/'.$logo}}" class="img-logo" />
<div class="page-title">
    <div class="container">
        <div class="row" style="text-align: center;">
            <h3 style="padding-top: 12px;">{{$client['name']}}</h3>
   		</div>
    </div>    
</div>
<table style="margin-bottom: 20px;">
	<tr>
		<thead>
			<th> Product </th>
			<th> Brand </th>
			<th> Processor </th>
			<th> Screen Size </th>
			<th> DVD </th>
			<th> HDD </th>
			<th> RAM </th>
			<th> Service </th>
			<th> Defects </th>
			<th> Quantity </th>
			<th> Value </th>
		</thead>
	</tr>
	@foreach($QuoteItem as $val)
	<tr>
		<td>{{$val['type_name']}}</td>
		<td>{{$val['brand_name']}}</td>
		<td>{{$val['processor_name']}}</td>
		<td>{{$val['screen_name']}}</td>
		<td>{{$val['dvd_name']}}</td>
		<td>{{$val['hdd_name']}}</td>
		<td>{{$val['ram_name']}}</td>
		<td>{{$val['services_name']}}</td>
		<td>
			<ul>
				@foreach($val['quote_items_defect'] as $def)
				<li>{{$def['defect_name']}}</li>
				@endforeach
			</ul>			
		</td>
		<td>{{$val['quantity']}}</td>
		<td>{{displayQuaoteValues($val['total_price'], $Quote['currency_symbol'], $Quote['currency_rate'] )}}</td>
	</tr>
	@endforeach
	<tr>
		<td colspan="9"> Total </td>
		<td>{{$Quote['total_items']}}</td>
		<td>{{displayQuaoteValues($Quote['total_value'], $Quote['currency_symbol'], $Quote['currency_rate'] )}}</td>
	</tr>
</table>

<div class="row footer">
    <div class="footer2">
    	<table>
    		<tr>
    			<td> Status : {{$quote_status['name']}}</td>
    			<td>Quote Name : {{$Quote['name'] !=null ? $Quote['name']:'N/A'}}</td>
    		</tr>
    	</table>
        
    </div>
</div>
