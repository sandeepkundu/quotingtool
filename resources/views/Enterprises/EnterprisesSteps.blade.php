@extends('layouts.hpUser') 
@section('content')

@php  
use App\Http\Controllers\HpUsersController;
@$Theme = HpUsersController::Theme_Cookie(); 
$ccFilename = 'stepsStore.css';
$cssfile = asset('css/'.$ccFilename);
if(isset($Theme)){
	if(!empty($Theme->theme_folder)) {
		if(file_exists(public_path() . '/' . @$Theme->theme_folder . '/' . $ccFilename)){
			$cssfile = asset('/'). @$Theme->theme_folder . '/' . $ccFilename;
		}
	}
}
@endphp
<link rel="stylesheet" type="text/css" href="{{$cssfile}}">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<div class="page-title">
    <div class="container">
        @php 
        $statustext = ' ( In Progress )';
        $total_items = 0;
        if(null !== @$Quote['id']){
        	switch($Quote['status']){
        		case "6": $statustext = " ( Completed )"; break;
        		case "2": $statustext = " ( Accepted )"; break;
        		case "7": $statustext = " ( Canceled )"; break;
        	}
        	$total_items = @$Quote['total_items'];
        }
        @endphp
        <div class="pull-left">
        <span class="page-heading-center"  style="text-align: left;">
        Quote:
        @if(null !== $Quote && !empty($Quote['id']))
        {{@$Quote['id']}} : {{@$Quote['name']}}
        @else
        {{ Session::get('name') }}
        @endif
        {{$statustext}}
        </span>                  
        </div>
        <div class="pull-right hidden-print">
            <a class="btn btn-cta btn-summary" href="/enterprises/index">Quote Summary <span class="badge">{{$total_items}}</span></a>
        </div>            
    </div>    
</div>

<input type="hidden" id="clientID" name="clientid" value='{{@$clinetname->id}}'>
<input type="hidden" id="margin" name="margin" value='{{@$clinetname->margin_oem_product + @$clinetname->margin_oem_product + @$clinetname->margin_oem_product}}'>
<input type="hidden" id="quote_id" name="quote_id" value='{{@$quote_id}}'>
<input type="hidden" id="quote_item_id" name="quote_item_id" value='{{@$quoteItemId}}'>
<div class="mainDiv">
    <section class="search-bar" style="background-color:#d2dee1;">
        <div class="container">
            <div class="row">			
                <div class="col-md-8 col-sm-6 col-sm-offset-3 col-md-offset-2">
                    <p class="text-center" style="font-size: 24px; color: #fff;">Enter Model Number:</p>
                    <div style="position:relative">
                        <div class="close-search"><i class="fa fa-close"></i></div>
                        <input type="search" name="modal" id="tags" class="search-input" placeholder=" Eg: HP Probook 450 G4" style="width:100%;margin-left:0" value="{{@$modal}}">
                        <ul class="list-group search-ul" style="width: 100% !important; position: absolute;z-index:3;margin-top: 3px;border: none;border-radius: 0px;">
                        </ul>
                    </div> 
                    <p class="text-center" style="margin-top:10px;color:#fff">If model search is unsuccessful please enter model specifications below</p>
                    <input type="hidden" name="client_id" value="{{$client_id}}">

                    <br><span style="color: red;">
                        @if(Session::has('error'))
                        {{ Session::get('error') }}
                        @endif
                    </span>

                </div>
            </div>
        </div> 
    </section>
    <section class="device-section">
        <div class="container">
            <h2 class="title text-center">Select Product Type</h2>
            @if(count($Products) > 7)
            <div class="row-center row-left">
            @else
            <div class="row-center">
            @endif 
                @foreach($Products as $product)
                <div class="card {{@($product->id == $QuoteItemDetail->type_id) ? 'selected':''}}" onclick="step01({{ $product->id }})">
                    <div class="brand-img">
                        <img class="p_img" src="{{ URL::to('product/'.$product->image) }}"/>
                    </div>
                    <div class="brand-title">{{$product->name}}</div>
                </div>
                @endforeach
            </div>
        
        </div> 
    </section>
</div>


<input type="hidden" name="siteurl" id="siteurl" value="{{URL::to('/')}}">
<input type="hidden" name="lastId" value="3" id="lastId" class="lastId">
<div id="loadingDiv" style="display:none;">
    <div>
        <div class="loader"></div>
    </div>
</div>
{{csrf_field()}}
@endsection

@section('js')
<script type="text/javascript">
            function summary(){
            $('#addToCard').modal('show');
            }
</script>
<script type=text/javascript src="{{ asset('js/enterprises.js') }}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        if($.cookie('data')){
            var abc =  $.cookie('data');
            var obj = JSON.parse(abc);

            if(obj.brandIfTrue == true){                
                step01(obj.type_id);
            }

            var pro1 = obj.type_id;
            var pro2 = obj.brand_id;
			var model = obj.model_id;
			var pro9 = 0;
			var laststep = obj.EditlastStep;

            if(obj.SearchType == 'Processor'){                
                step02(pro1, pro2);
            }

			var pro3 = obj.processor_id;

            if(obj.SearchType == 'HDD' || obj.SearchType == 'DVD' || obj.SearchType == 'RAM' || obj.SearchType == 'Screen Size'){
                var pro4 = 0;
                var pro5 = 0;
                var pro6 = 0;
                var pro7 = 0;
                var pro8 = 0;	
                setTimeout(function(){  totalSteps(pro1, pro2, pro3, pro4, pro5, pro6, pro7, pro8, pro9, laststep, model) }, 200);
            }

            var pro4 = obj.screen_id;
            var pro5 = obj.dvd_id;
            var pro6 = obj.hdd_id;
            var pro7 = obj.ram_id;

            if(obj.SearchType == 'Defects'){
				var pro8 = 0;
				setTimeout(function(){  totalSteps(pro1, pro2, pro3, pro4, pro5, pro6, pro7, pro8, pro9, laststep, model) }, 200);               
            }

            var pro8 = obj.Defects;            
            if(obj.SearchType == 'Services'){
                setTimeout(function(){  totalSteps(pro1, pro2, pro3, pro4, pro5, pro6, pro7, pro8, pro9, laststep, model) }, 200);                
            }
        }        
    });

</script>
@endsection