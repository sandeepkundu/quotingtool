@extends('layouts.hpUser') 
@section('content')
@php  
use App\Http\Controllers\HpUsersController;
@$Theme = HpUsersController::Theme_Cookie(); 
$ccFilename = 'enterprisesIndex.css';
$cssfile = asset('css/'.$ccFilename);
if(isset($Theme)){
	if(!empty($Theme->theme_folder)) {
		if(file_exists(public_path() . '/' . @$Theme->theme_folder . '/' . $ccFilename)){
			$cssfile = asset('/'). @$Theme->theme_folder . '/' . $ccFilename;
		}
	}
}
@endphp
<link rel="stylesheet" type="text/css" href="{{$cssfile}}">

@if(isset($toast['message']))
{{ $toast['message'] }}
@endif

<div class="page-title">
    <div class="container">
        @php 
        $statustext = '';
        switch($Quotes['status']){
        	case "1": $statustext = " ( In Progress )"; break;
        	case "6": $statustext = " ( Completed )"; break;
        	case "2": $statustext = " ( Accepted )"; break;
        	case "7": $statustext = " ( Canceled )"; break;
        }
        @endphp
        <div class="pull-left">
        <span class="page-heading-center">
        Quote: {{@$Quotes['id']}} : {{@$Quotes['name']}} {{$statustext}}
        </span>                  
        </div>
        <div class="pull-right hidden-print">
            <a class="btn btn-cta btn-summary" href="/enterprises/index">Quote Summary <span class="badge">{{@$Quotes['total_items']}}</span></a>
        </div>         
    </div>
</div>    
<?php 
$ids = array();  

function displayQuaoteValues($value, $symbol, $rate){
	$rate = ($rate > 0 && strlen($symbol) > 0) ?  $rate : 1;
	$symbol =  (strlen($symbol) > 0) ? $symbol : '$';
	return $symbol . '' . number_format($value * $rate, 0, '.', ',');
}
?>
<div class="mainDiv">
	<section class="search-bar" style="background-color:#fff!important; padding: 20px 0px;">
		<div class="container">

			<div class="clearfix" style="margin-bottom:30px">    
    			@if(count($Quotes)==0 || $Quotes['status']==1)
    
    			<form class="pull-left action-bar hidden-print" method="post" action="{{ URL::to('/') }}/enterprises/quote/step">
    				{{csrf_field()}}
    				<input type="hidden" name="client_id" value="{{@$clientDetails->id}}">
    				<input type="hidden" id="quote_id" name="quote_id" value="{{@$Quotes->id}}">
    				<input type="hidden" id="quote_item_id" name="quote_item_id" value="">
    				<input type="submit" class="btn btn-cta btn-sm default" value="Add New Item"/>
    			</form>
    
    			@endif
    			
    			<form method="post" class="pull-left action-bar hidden-print" action="{{ URL::to('/') }}/Enterprises/quote_setting">
    				{{csrf_field()}}
    				<input type="hidden" name="client_id" value="{{@$clientDetails->id}}">
    				<input type="hidden" name="quote_id" value="{{@$Quotes->id}}">
    				<input type="hidden" name="quote_item_id" value="">
    				<input type="hidden" name="note_status" value="{{$Quotes['status']}}">
    				<input type="Submit" class="btn btn-cta btn-sm pull-left" value="Quote Settings" id="Edit_Quote_Setting"/>
    			</form>
    
    		    <button type="button" class="hidden-print action-bar pull-left btn btn-cta btn-sm" onclick="print_quote();">Export CSV</button>
				<!--<div class="hidden-print">
				<select name="exportchange" id="exportchange" class="btn-cta status-select ">
				<option value="">Export</option>
				<option value="print">Print</option>
        		<option value="csv">Export CSV</option>
				</select>
				</div>-->
    
    			@if(count($Quotes)>0 && $Quotes['status']!=1)
    			
    			@endif
    				
    			<div class="pull-right hidden-print">
        			<select name="statuschange" id="statuschange" class="btn-cta status-select pull-right">
        			<option value="">Quote Status</option>
        			@if($Quotes['status'] == 1)	
        			    <option value="6">Complete</option>
        			    <option value="7">Cancel</option>
        			@elseif ($Quotes['status'] == 6)
        			    <option value="1">In-Progress</option>
        				<option value="2">Accept</option>
        			    <option value="7">Cancel</option>
        			@elseif ($Quotes['status'] == 2)
        			    <option value="1">In-Progress</option>
        			    <option value="7">Cancel</option>
        			@elseif ($Quotes['status'] == 7)
        			    <option value="1">In-Progress</option>
        			@endif	
        			</select>
        			<!--<button class="AcceptButton btn pull-right" value="Update Status" disabled="disabled" id="btnupdatestatus">Update Status</button>-->
    			</div>
		    </div><!-- clearfix -->

			@if($role == 2 || ($role == 3 && in_array(1,$permission)) )

			<div class="table-responsive" id="first-table">
				<table class="table table-striped" align="center">
				<tr>
				<td class="border-empty">Country</td>
				<td align="center">{{$Quotes['Country']['name']}}</td>
				<td class="border-empty" width="25px">&nbsp;&nbsp;&nbsp;</td>
				<td align="center" class="theme" style="font-weight: 600; color:#fff">Financial Summary</td>
				<td align="center" class="theme" style="font-weight: 600; color:#fff">Product Value</td>
				<td align="center" class="theme" style="font-weight: 600; color:#fff">Services Fee</td>
				<td align="center" class="theme" style="font-weight: 600; color:#fff">Logistics Fee</td>
				<td align="center" class="theme" style="font-weight: 600; color:#fff">Net Value</td>
				</tr>
				<tr>
				<td class="border-empty">Region</td>
				<td align="center">{{$Quotes['Region']['name']}}</td>
				<td class="border-empty"></td>
				<td>Quote from Partner</td>
				<td align="center">{{displayQuaoteValues($unit_tp, $Quotes['currency_symbol'], $Quotes['currency_rate'] )}}</td>
				<td align="center">{{displayQuaoteValues(@$services_tp, $Quotes['currency_symbol'], $Quotes['currency_rate'] )}}</td>
				<td align="center">{{displayQuaoteValues(@$logistic_tp, $Quotes['currency_symbol'], $Quotes['currency_rate'] )}}</td>
				<td align="center">{{displayQuaoteValues(@$total_tp, $Quotes['currency_symbol'], $Quotes['currency_rate'] )}}</td>
				</tr>
				<tr>
				<td class="border-empty">Currency</td>
				<td align="center">
				<?php
				if(($Quotes['currency_symbol'] == "$" || $Quotes['currency_symbol'] == 'US$') && $Quotes['currency_rate'] == 1){
					echo 'US$';		
				} else {
					echo $Quotes['Country']['currency_name'];
				}
				?>
				</td>
				<td class="border-empty"></td>
				<td>Quote to Customer</td>
				<td align="center">{{displayQuaoteValues($unit_oemtp, $Quotes['currency_symbol'], $Quotes['currency_rate'] )}}</td>
				<td align="center">{{displayQuaoteValues(@$services_oemtp, $Quotes['currency_symbol'], $Quotes['currency_rate'] )}}</td>
				<td align="center">{{displayQuaoteValues(@$logistic_oemtp, $Quotes['currency_symbol'], $Quotes['currency_rate'] )}}</td>
				<td align="center">{{displayQuaoteValues(array_sum(@$total_price), $Quotes['currency_symbol'], $Quotes['currency_rate'] )}}</td>
				</tr>
				<tr>				
				<td class="border-empty">Qty Per Pickup</td>
				<td align="center">{{number_format($Quotes['ave_collect_size'], 0, '.', ',')}}</td>
				<td class="border-empty"></td>
				<td>Margin / Markup $</td>
				<td align="center">{{displayQuaoteValues(($unit_tp - $unit_oemtp), $Quotes['currency_symbol'], $Quotes['currency_rate'] )}} </td>
				<td align="center">{{displayQuaoteValues(($services_oemtp - $services_tp), $Quotes['currency_symbol'], $Quotes['currency_rate'] )}}</td>
				<td align="center">{{displayQuaoteValues(($logistic_oemtp -$logistic_tp), $Quotes['currency_symbol'], $Quotes['currency_rate'] )}}</td>
				<td align="center">{{displayQuaoteValues(($total_tp - $total_oemtp), $Quotes['currency_symbol'], $Quotes['currency_rate'] )}}</td>
				</tr>
				<tr>
				<td class="border-empty">Base Product Margin</td>
				<td align="center">{{number_format($Quotes['margin_oem_product'],0)}}%</td>
				<td class="border-empty"></td>
				<td>Margin / Markup %</td>
				<td align="center">
				@if($unit_tp > 0)	
				{{number_format( ceil((($unit_tp - $unit_oemtp) / $unit_tp) * 100), 0)}}%
				@endif
				</td>
				<td align="center">
				@if($services_tp > 0)	
				{{number_format( ceil((($services_oemtp - $services_tp) / $services_tp) * 100), 0)}}%
				@endif	
				</td>
				<td align="center">
				@if($logistic_tp > 0)	
				{{number_format( ceil((($logistic_oemtp - $logistic_tp) / $logistic_tp) * 100), 0)}}%
				@endif	
				</td>
				<td align="center">
				@if($total_tp > 0)	
				{{number_format( ceil((($total_tp - $total_oemtp) / $total_tp) * 100), 0)}}%				
				@endif	
				</td>
				</tr>
				<tr>				
				<td class="border-empty">Base Services Markup</td>
				<td align="center">{{number_format($Quotes['margin_oem_service'],0)}}%</td>
				<td class="border-empty"></td>
				<td class="border-empty"></td>
				<td class="border-empty" align="center"></td>
				<td class="border-empty" align="center"></td>
				<td class="border-empty" align="center"></td>
				<td class="border-empty" align="center"></td>
				</tr>
				<tr>				
				<td class="border-empty">Base Logistics Markup</td>
				<td align="center">{{number_format($Quotes['margin_oem_logistic'],0)}}%</td>
				<td class="border-empty"></td>
				<td class="border-empty"></td>
				<td class="border-empty" align="center"></td>
				<td class="border-empty" align="center"></td>
				<td class="border-empty" align="center"></td>
				<td class="border-empty" align="center"></td>
				</tr>
				</table>
			</div>
			<p class="lower-txt" style="margin-bottom:15px">Changing Base Margins will overwrite any item -level margin settings.</p>
				
			@endif

			<!-- start third row table	-->

			<div class="table-responsive" id="second-table">
				<table class="table table-striped">
					<thead>
						<tr>
							<th style="text-align:left!important"><b style="font-weight: 700;font-size: 18px">Product Details</b> <span class="fairly-sml">(Click to Expand)</span></th>
							<th align="center">Items</th>
							<th align="center">QTY</th>
							<th align="center">Product<br>Value<br><span class="really-sml">(per unit)</span></th>
							<th align="center">Services<br>Fee<br><span class="really-sml">(per unit)</span></th>
							<th align="center">Logistics<br>Fee<br><span class="really-sml">(per unit)</span></th>
							<th align="center">Net<br>Value<br><span class="really-sml">(per unit)</span></th>
							<th align="center">Total<br>Net<br>Value</th>
							<th align="center">Margin<br>%</th>
							<th align="center">Action</th>
						</tr>
					</thead>
					<tbody>

						@if(count($Quote_Items)>0)

						@foreach($Quote_Items as $key1 => $quotes1)

							@php 
							$qty2 = $unitp2 = $services1 = $logistic1 = $net_price1 = $total_price1 = $total_price1 = $totalmargin1 = $quotes2 = $p_unit = $p_logistics = $p_services = array(); 
							$isSelected = '';
							@endphp
							
							@foreach($quotes1 as $Quote1)
								@php
									if(@$itemSelected == $Quote1[0]['id']){ 
										$isSelected = 'in';
									}
	                                $qty2[]			= $Quote1[0]['quantity'];
	                                $unitp2[]		= $Quote1[0]['unit_f5_price'] * $Quote1[0]['quantity'];
	                                $services1[]	= $Quote1[0]['services_net_price'] * $Quote1[0]['quantity'];
	                                $logistic1[]	= $Quote1[0]['logistics_net_price'] * $Quote1[0]['quantity'];
	                                $net_price1[]	= $Quote1[0]['unit_price'];
	                                $total_price1[] = $Quote1[0]['total_price'];
	                                $totalmargin1[] = $Quote1[0]['totalmargin'];		

									$p_unit[]		= $Quote1[0]['unit_prod_price'] * $Quote1[0]['quantity'];
									$p_logistics[]	= $Quote1[0]['unit_f5_logistics'] * $Quote1[0]['quantity'];
									$p_services[]	= $Quote1[0]['unit_f5_services'] * $Quote1[0]['quantity'];                              
                                @endphp
                       		@endforeach

							@if(array_sum($qty2) != 0 && array_sum($p_unit) != 0)

								@php									
								$net_price = array_sum($p_unit) - array_sum($p_services) - array_sum($p_logistics);
								$ave_margin = ceil( (1 - (array_sum($total_price1) / $net_price)) * 100); 																	
								@endphp

                       								
								<tr>
								<td><a href="javascript:void(0);" class="key-name" data-toggle="collapse" data-target=".{{str_replace(' ', '-',$key1)}}">{{$key1}}</a></td>
								<td align="center">{{count($quotes1)}}</td>
								<td align="center">{{number_format(array_sum($qty2), 0, '.', ',')}}</td>
								<td align="center">{{displayQuaoteValues(array_sum($unitp2) / array_sum($qty2), $Quotes['currency_symbol'], $Quotes['currency_rate'] )}}</td>
								<td align="center">{{displayQuaoteValues(array_sum($services1) / array_sum($qty2), $Quotes['currency_symbol'], $Quotes['currency_rate'] )}}</td>
								<td align="center">{{displayQuaoteValues(array_sum($logistic1) / array_sum($qty2), $Quotes['currency_symbol'], $Quotes['currency_rate'] )}}</td>
								<td align="center">{{displayQuaoteValues(array_sum($total_price1) / array_sum($qty2) , $Quotes['currency_symbol'], $Quotes['currency_rate'] )}}</td>
								<td align="center">{{displayQuaoteValues(array_sum($total_price1), $Quotes['currency_symbol'], $Quotes['currency_rate'] )}}</td>
								<td align="center">{{number_format($ave_margin,0)}}%</td>
								<td></td>									
								</tr>

								@php $i = 1; @endphp
								@foreach($quotes1 as $Quote)
								@php 
								$ids[] = $Quote[0]['id']; 
								$highlight = '';
								if(@$itemSelected == $Quote[0]['id']){ 
									$highlight = 'highlight';
								}
								@endphp
									<tr class="collapse demo {{str_replace(' ', '-',$key1)}} {{$isSelected}}  {{$highlight}} " style="border:1px solid grey !important;">
									<td colspan="2">
    									<div class="row">
    									    
    										<div class="col-sm-6">
    											<ul class="specs-list">
    											@if(!empty($Quote[0]['model_name']))
    											<li><label>Model:</label> {{$Quote[0]['model_name']}}</li>
    											@endif
    
    											@if(!empty($Quote[0]['type_name']))
    											<li><label>Product:</label> {{$Quote[0]['type_name']}}</li>
    											@endif
    
    											@if(!empty(@$Quote[0]['brand_name']))
    											<li><label>Brand:</label> {{@$Quote[0]['brand_name']}}</li>
    											@endif
    
    											@if(!empty(@$Quote[0]['processor_name']))
    											<li><label>Processor:</label> {{@$Quote[0]['processor_name']}}</li>
    											@endif
    
    											@if(!empty(@$Quote[0]['screen_name']))
    											<li><label>Screen:</label> {{@$Quote[0]['screen_name']}}</li>
    											@endif

    											@if(!empty(@$Quote[0]['dvd_name']))
    											<li><label>DVD:</label> {{@$Quote[0]['dvd_name']}}</li>
    											@endif
    
    											@if(!empty(@$Quote[0]['hdd_name']))
    											<li><label>HDD:</label> {{@$Quote[0]['hdd_name']}}</li>
    											@endif
    
    											@if(!empty(@$Quote[0]['ram_name']))
    											<li><label>RAM:</label> {{@$Quote[0]['ram_name']}}</li>
    											@endif
    											
    											@if(!empty(@$Quote[0]['services_name']))
    											<li><label>Services:</label> {{@$Quote[0]['services_name']}}</li>
    											@endif

    											<ul>
    										</div>
    
    										<div class="col-sm-6">
    											<ul class="specs-list">

    											@if(isset($Quote[0]['quote_items_defect']) && count($Quote[0]['quote_items_defect'])>0)
    											<li><strong>Condition(s):</strong><br>
    												@foreach($Quote[0]['quote_items_defect'] as $val)
                                                    <span class="specs-list-defect">{{$val['defect_name']}}</span> 				
    												@endforeach
    											</li>
    											@endif
    											</ul>
    										</div>
    
    									</div>
									</td>									

									@php
									/* get the pre oem pricing for products, logistics and services to work out the actual OEM margin */
									$item_p_total		= ($Quote[0]['unit_prod_price'] - $Quote[0]['unit_f5_logistics'] - $Quote[0]['unit_f5_services']) * $Quote[0]['quantity'];
									$item_m				= ($item_p_total != 0) ? ceil( (1 - ($Quote[0]['total_price'] / $item_p_total)) * 100) : 0;
									@endphp

									<!--
									<td  align="right">{{$i++}}</td>
									-->
									<td align="center">{{number_format($Quote[0]['quantity'], 0, '.', ',')}}</td>
									<td align="center">{{displayQuaoteValues($Quote[0]['unit_f5_price'], $Quotes['currency_symbol'], $Quotes['currency_rate'] )}}</td>
									<td align="center">{{displayQuaoteValues($Quote[0]['services_net_price'], $Quotes['currency_symbol'], $Quotes['currency_rate'] )}}</td>
									<td align="center">{{displayQuaoteValues($Quote[0]['logistics_net_price'], $Quotes['currency_symbol'], $Quotes['currency_rate'] )}}</td>
									<td align="center">{{displayQuaoteValues($Quote[0]['unit_price'], $Quotes['currency_symbol'], $Quotes['currency_rate'] )}}</td>
									<td align="center">{{displayQuaoteValues($Quote[0]['total_price'], $Quotes['currency_symbol'], $Quotes['currency_rate'] )}}</td>
									<td align="center">{{number_format($item_m, 0 )}}%
									@if($role == 2 || ($role == 3 && in_array(1,$permission)) )
									<span class="really-sml">
									<br>P {{number_format($Quote[0]['unit_margin_oem_product'], 0 )}}%
									<br>S {{number_format($Quote[0]['services_oem_margin'], 0 )}}%
									<br>L {{number_format($Quote[0]['logistics_oem_margin'], 0 )}}%
									</span>
									@endif
									</td>
									<td align="center">
									@if(@$Quotes->status==1)
									<a class="hidden-print btn btn-cta btn-action editCart" title="Edit" href="javascript:void(0)" name="{{$Quote[0]['id']}}">
									<i class="fa fa-edit"></i>
									</a>
									@endif
							
									<a class="hidden-print btn btn-cta btn-action" title="Duplicate" href="{{URL('/')}}/Enterprises/clone/{{$Quote[0]['id']}}">
									<i class="fa fa-clone"></i>
									</a>

									<a onclick="return confirm('Are you sure you want to delete?')" title="Delete" class="hidden-print btn btn-action btn-danger" href="{{URL('/')}}/Enterprises/delete/{{$Quote[0]['id']}}">
									<i class="fa fa-trash-o"></i>
									</a>

									</td>
									</tr>
								@endforeach

							@endif 

						@endforeach
						@endif 

						<tr>
						<td class="border-empty"></td>
						<td align="center">{{count($ItemCount)}}</td>
						<td align="center"> {{number_format(array_sum($qty), 0, '.', ',')}}</td>
						<td class="border-empty"></td>
						<td class="border-empty"></td>
						<td class="border-empty"></td>
						<td class="border-empty"></td>
						<td align="center">{{displayQuaoteValues(@$total_oemtp, $Quotes['currency_symbol'], $Quotes['currency_rate'] )}}</td>
						<td align="center">
						@if($total_tp > 0 && $total_tp >= $total_oemtp)	
						{{number_format( ceil((($total_tp - $total_oemtp) / $total_tp) * 100), 0)}}%
						@elseif ($total_tp > 0 && $total_tp < $total_oemtp)
						{{number_format( ceil((($total_oemtp - $total_tp) / $total_tp) * 100), 0)}}%
						@endif	
						</td>		
						<td class="border-empty"></td>							
						</tr>

					</tbody>
				</table>

			</div>

		</div><!--End COntainer-->
	</section>
</div><!--End mainDiv-->

<input type="hidden" name="ids" value="" > 
<input type="hidden" name="{{@$Quotes['status']}}" id="{{implode(',',$ids)}}" class="status">
<input type="hidden" name="siteurl" id="siteurl" value="{{URL::to('/')}}">
<div id="loadingDiv" style="display:none;">
	<div>
		<div class="loader"></div>
	</div>
</div>
{{csrf_field()}}

<!--==============================Modal Popup Edit QuoteItem=======================-->
<div class="modal modal-success fade in" id="modal-bootstrap-edit-quote-item" style="display: none;">
	<div class="modal-dialog" role="document" style="margin: 150px auto;">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #56bdf1;">
				<button type="button" id="close" onclick="reload();" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
				<h4 class="modal-title" id="modal-title"></h4>
			</div>
			<div class="modal-body" style="height: auto;overflow: auto;">
				<form method="post" class="Quote_Edit_form" action="{{ URL::to('/') }}/enterprises/quote/step">
					{{csrf_field()}}
					<div class="col-sm-12">
					<select name="quote_edit_items" id="quote_edit_item" class="form-control">
					</select>
					</div>
					
					<div class="col-sm-12" style="padding:4px">
					<label for="quantity" style="font-weight: 600;color:#767676; padding-top: 5px" class="col-sm-4 control-label">Quantity:</label>
					<div class="col-sm-3" style="padding-left: 0px;">
					<input type="number" min="1" name="quantity" class="form-control Quantity" id="item_quantity" placeholder="Quantity">
					</div>
					</div>

					<div class="col-sm-12" style="padding:3px">
					<label for="product_oem" style="font-weight: 600;color:#767676; padding-top: 5px" class="col-sm-4 control-label">Product Margin</label>
					<div class="col-sm-3" style="padding-left: 0px;">
					<input type="number" min="1" name="product_oem" class="form-control Quantity" id="product_oem" placeholder="Product Margin">
					</div>
					</div>

					<div class="col-sm-12" style="padding:3px">
					<label for="service_oem" style="font-weight: 600;color:#767676; padding-top: 5px" class="col-sm-4 control-label">Services Margin</label>
					<div class="col-sm-3" style="padding-left: 0px;">
					<input type="number" min="1" name="service_oem" class="form-control Quantity" id="service_oem" placeholder="Services Margin">
					</div>
					</div>

					<div class="col-sm-12" style="padding:3px">
					<label for="logistic_oem" style="font-weight: 600;color:#767676; padding-top: 5px" class="col-sm-4 control-label">Logistics Margin</label>
					<div class="col-sm-3" style="padding-left: 0px;">
					<input type="number" min="1" name="logistic_oem" class="form-control Quantity" id="logistic_oem" placeholder="Logistics Margin">
					</div>
					</div>

					
					<input type="hidden" name="client_id" value="{{@$clientDetails->id}}">
					<input type="hidden" name="quote_id" id="quote_id" value="{{@$Quotes->id}}">
					<input type="hidden" name="quote_item_id" id="quote_item_Id" value="">
					<input type="hidden" name="quoteItemId" id="quoteItemId" value="">
				</form>
				<a href="" class="btn" style="height:50px;padding-top: 15px;"></a>
				<div class="pull-right btn btn-default Quote_edit_sub" style="margin-top:11px;"> Submit </div>
			</div>
		</div>
	</div>
</div>
<!--============================== Modal Popup Edit QuoteItem =======================--> 
<style>
	.edit-input {
		display:none;
	}
</style>
@endsection

@section('js')
<script>
$(document).ready(function() {
    $('.Quote_edit_sub').click(function (){
        $('.Quote_Edit_form').submit();
    });
});

$(document).ready(function() {
    $('.editCart').click(function () {
    	var QuoteItem = $(this).attr('name');
    	var SITE = $('#siteurl').val();
    	var option_product='';
		//console.log(SITE + '/Enterprises/EditItem?QuoteItem='+QuoteItem);
    	$.ajax({
    		type: 'get',
    		url: SITE + '/Enterprises/EditItem',
    		data: {'QuoteItem': QuoteItem},
    		dataType: 'JSON',
    		success: function (obj) {

					$('#item_quantity').val(obj.QuoteItem.quantity);
					$('#service_oem').val(obj.QuoteItem.services_oem_margin);
					$('#logistic_oem').val(obj.QuoteItem.logistics_oem_margin);
					$('#product_oem').val(obj.QuoteItem.unit_margin_oem_product);
					$('#quoteItemId').val(obj.QuoteItem.id );
                    $('#quote_item_Id').val(obj.QuoteItem.id );
                    $('#modal-title').text(obj.QuoteItem.type_name);
					option_product += '<option value="-1">Select Step</option>';

                    if(obj.QuoteItem.type_id != 0){
                        option_product += '<option value="'+obj.QuoteItem.type_id+'">Product : '+obj.QuoteItem.type_name+'</option>';
                    }                            
                    if(obj.QuoteItem.brand_id != 0){
                        option_product += '<option value="'+obj.QuoteItem.brand_id+'">Brand : '+obj.QuoteItem.brand_name+'</option>';
                    }     
                       
                    $.each(obj.Steps, function(i, v) {
						if (v.group_code.search(new RegExp(/Processor/i)) != -1) {
							option_product += '<option value="'+obj.QuoteItem.processor_id+'">Processor : '+obj.QuoteItem.processor_name+'</option>';
						}
                    });

                    $.each(obj.Steps, function(i, v) {
						if (v.group_code.search(new RegExp(/Screen/i)) != -1) {
							option_product += '<option value="'+obj.QuoteItem.screen_id+'">Screen : '+obj.QuoteItem.screen_name+'</option>';
						}
                    });                          
                    
					$.each(obj.Steps, function(i, v) {
						if (v.group_code.search(new RegExp(/DVD/i)) != -1) {
							option_product += '<option value="'+obj.QuoteItem.dvd_id+'">DVD : '+obj.QuoteItem.dvd_name+'</option>';
						}
                    });    
                      
                    $.each(obj.Steps, function(i, v) {
						if (v.group_code.search(new RegExp(/HDD/i)) != -1) {
							option_product += '<option value="'+obj.QuoteItem.hdd_id+'">HDD : '+obj.QuoteItem.hdd_name+'</option>';
						}
                    });
                    
					$.each(obj.Steps, function(i, v) {
						if (v.group_code.search(new RegExp(/RAM/i)) != -1) {
							option_product += '<option value="'+obj.QuoteItem.ram_id+'">RAM : '+obj.QuoteItem.ram_name+'</option>';
						}
                    });

					$.each(obj.Steps, function(i, v) {
						if (v.group_code.search(new RegExp(/Defect/i)) != -1) {
							option_product += '<option value="Defects">Device Condition</option>';
						}
					});

					$.each(obj.Steps, function(i, v) {
						if (v.group_code.search(new RegExp(/Service/i)) != -1) {
							if(obj.QuoteItem.services_id != 0){
								option_product += '<option value="'+obj.QuoteItem.services_id+'">Service : '+obj.QuoteItem.services_name+'</option>';
							} else {
								option_product += '<option value="Services">Services</option>';
							}
						}
					});

                    $('#quote_edit_item').html(option_product);

                    $('#modal-bootstrap-edit-quote-item').modal('show');    

                },
                error: function (error) {
                    alert('error block');
                }
            });

    });
});

$(document).ready(function() {	
	$('#statuschange').on('change', function() {
		if(this.value ==''){
			//$('#btnupdatestatus').attr("disabled", "disabled")
		} else {
			//$('#btnupdatestatus').removeAttr("disabled")
			var QuoteId = $('.status').attr('id');
    		var SITE = $('#siteurl').val();
    		var quote_id = $('#quote_id').val();
    		var newstatus = $('#statuschange option:selected').val();    			

			$('#loadingDiv').css('display', 'block');
			$.ajax({
				type: 'get',
				url: SITE + '/Enterprises/quotestatus',
				data: {'status': newstatus, 'name': QuoteId, 'Quote_id':quote_id},
				dataType: 'JSON',
				success: function (obj) {
					location.reload();
				},
				error: function (error) {
					$('#loadingDiv').css('display', 'none');
					alert('error block');
				}
			});
		}	
	})		
});

$(function () {	
    $('.AcceptButton').click(function () {
    	var QuoteId = $('.status').attr('id');
    	var SITE = $('#siteurl').val();
    	var quote_id = $('#quote_id').val();
    	var newstatus = $('#statuschange option:selected').val();    			

		$('#loadingDiv').css('display', 'block');
		$.ajax({
			type: 'get',
			url: SITE + '/Enterprises/quotestatus',
			data: {'status': newstatus, 'name': QuoteId, 'Quote_id':quote_id},
			dataType: 'JSON',
			success: function (obj) {
				location.reload();
			},
			error: function (error) {
				$('#loadingDiv').css('display', 'none');
				alert('error block');
			}
		});
    });
});

$('.close').click(function(){
    location.reload();
})

function print_quote(){
	location.href = '/Enterprises/csv';
    //window.print();
}

$(document).ready(function() {	
	$('#exportchange').on('change', function() {
		if(this.value != ''){
			
		} else {
    		var action = $('#exportchange option:selected').val();    			
			if(action == 'csv'){
				location.href = '/Enterprises/csv';
			} else if(action == 'print'){
				window.print();
			}			
		}	
	})		
});
</script>

    @endsection