<div style="background:#fff">
    <section class="" style="padding-top: 20px;">
        <div class="container">
            <ul class="progressbar">
                <li class="active1" style="width: 20%;">Product</li>
                <li class="" style="width: 20%;">Brand</li>
                <li class="" style="width: 20%;">Processor</li>
                <li class="" style="width: 20%;">Condition</li>
                <li class="" style="width: 20%;">Services</li>
            </ul>           
        </div>
    </section>
    <section class="search-bar" style="padding: 10px 0px; padding-bottom: 20px;background-color:#d2dee1;">
        <div class="container">
            <h3 class="text-center dev-spec-selection">Device Specification:</h3>  
            <div class="device-spec-row row-center">
				<?php
				$event = 'reload()';
				if(null !== @$QuoteItemDetail && @$QuoteItemDetail->id > 0) {
					$event = 'processorSelect('.$product->id.')';
				}
				?>
                <a href="javascript:void(0)" class="card card-selected" title="Back to product type selection" onclick="{{$event}}">
                    <div class="brand-img">
                    @if(!empty($product->image))
                    <img src="{{ URL::to('product/'.$product->image) }}"/>
                    @endif
                    </div>
                    <div class="brand-title">{{$product->name}}</div>
                </a>
            </div>
            <input type="hidden" id="quote_item_id" name="quote_item_id" value='{{@$quoteItemId}}'>
            
        </div> <!-- /container -->   
    </section>
</div>
<section class="device-section">
    <div class="container">
	
        @if(count($results) > 0)		
            <h2 class="title text-center">Select Brand</h2>						
            
            @if(count($results) > 7)
            <div class="row-center row-left">
            @else
            <div class="row-center">
            @endif 
            @foreach($results as $result)
				<?php
				$event = 'step02('.$product->id.', '.$result->id.')';
				if(null !== @$QuoteItemDetail && @$QuoteItemDetail->id > 0) {
					$event = 'BrandSet('.$result->id.')';
				}
				?>
                <div class="card step02 {{@($result->id == $QuoteItemDetail->brand_id) ? 'selected':''}}" id="{{$result->id}}" onclick="{{$event}}">
                    @if(!empty($result->image))
                    <div class="brand-img-wrap" style="background-image:url({{ URL::to('product/'.$result->image) }})"></div>
                    @endif
                </div>
            @endforeach
            </div>
            @else
       
        <script> $('#modal-bootstrap-tour').modal('show');</script>               
      
        @endif     

     </div> <!-- /container -->  

        <p class="text-center">
    
        @if(null !== @$QuoteItemDetail && @$QuoteItemDetail->id > 0)    
        <button class="btn btn-cta btn-lg next" onclick="BrandCheck({{$product->id}});">Save</button>        
        <input type="hidden" id="brand_id" name="brand_id" value='{{$QuoteItemDetail->brand_id}}'> 
        <button onclick="BrandProgress({{$product->id}})" class="btn btn-cta btn-lg next group" style="margin-right:0">Continue</button>
		<script>
		function processorSelect(productId) {
			$('#loadingDiv').css('display', 'block');
			var val = productId;
			var SITE = $('#siteurl').val();
			var quote_item_id = $('#quote_item_id').val();   
			var quote_id = $('#quote_id').val(); 
			var client_id = $('#clientID').val(); 
			var token = $("input[name=_token]").val();

			location.href = SITE + '/EnterprisesSteps?_token='+token+'&quote_item_id='+quote_item_id+'&quoteItemId='+quote_item_id+'&quote_edit_items='+productId;		
		}
		function BrandSet(brandid) {
			var now = $('#brand_id').val();
			$('#brand_id').val(brandid);
			$('#'+brandid).addClass('selected');
			$('#'+now).removeClass('selected');
		}
		function BrandProgress(pro1) {
			var pro2 = $('#brand_id').val();
			step02(pro1, pro2);
		}
		function BrandCheck(pro1) {
		
			$('#loadingDiv').css('display', 'block');
			var token = $("input[name=_token]").val();
			var pro2 = $('#brand_id').val();
			var quote_item_id = $('#quote_item_id').val();
			var SITE = $('#siteurl').val();
			console.log(SITE + '/Enterprises/stepcheck?pro1='+pro1+'&_token='+ token+ '&pro2='+ pro2+ '&lastStep=3&quote_item_id='+ quote_item_id);

			$.ajax({
				type: 'post',
				url: SITE + '/Enterprises/stepcheck',
				data: { 'pro1': pro1, '_token': token, 'pro2': pro2, 'lastStep': 3, 'quote_item_id': quote_item_id },
				dataType: 'JSON',
				success: function (obj) {
					if(obj.status == 'new') {
						BrandProgress(pro1);
					} else if(obj.status == 'equal') {
						window.location.href = "Enterprises/index?qiid=" + quote_item_id;
					}else {
						$('#loadingDiv').css('display', 'none');
						$('#modal-bootstrap-tour').modal('show');
					}
				},
				error: function (error) {
					alert('an error occured getting the the list of processors');
				}
			});
		
		}
		</script>
        @endif
                             
        </p> 
</section>

