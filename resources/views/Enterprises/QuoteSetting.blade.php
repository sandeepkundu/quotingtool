@extends('layouts.hpUser') 
@section('content')
<div class="page-title">
    <div class="container">
        @php 
        $statustext = '';
        switch($Quote['status']){
        	case "1": $statustext = " ( In Progress )"; break;
        	case "4": $statustext = " ( Completed )"; break;
        	case "6": $statustext = " ( Accepted )"; break;
        	case "7": $statustext = " ( Canceled )"; break;
        }
        @endphp
        <div class="pull-left">
            <span class="page-heading-center">
            Quote: {{@$Quote['id']}} : {{@$Quote['name']}} {{$statustext}}
            </span>                  
        </div>
        <div class="pull-right hidden-print">
            <a class="btn btn-cta btn-summary" href="/enterprises/index">Quote Summary <span class="badge">{{@$Quote['total_items']}}</span></a>
        </div>
    </div>    
</div>
<div class="container">


    <form class="form-signin form-horizontal" method="post" action="{{url('/')}}/Enterprises/UpdateQuoteSettings">
        {{ csrf_field() }}
        <input type="hidden" name="quote_id" value="{{$Quote->id}}">
        <input type="hidden" name="quote_status" id="quote_status" value="{{$Quote->status}}">

        <div class="form-group"> 

            <label for="name" class="col-sm-3 control-label">Name / Reference </label>
            <div class="col-sm-9">
				@php 
				/* if there is a quote and use quote values otherwise populate from client */
				$q_ref			= $client->name;
				$q_country		= $client->country;
				$q_region		= $client->region;
				$q_collect		= $client->ave_collect_size;
				$q_margin_prod	= $client->margin_oem_product; 
				$q_margin_serv	= $client->margin_oem_service; 
				$q_margin_logs	= $client->margin_oem_logistics; 
				$q_currency		= $currency->currency_symbol; 
				$q_rate			= 0;

				if(null !== $Quote && !empty($Quote['name'])){
					$q_ref			= $Quote->name;
					$q_country		= $Quote->country;
					$q_region		= $Quote->region;
					$q_collect		= $Quote->ave_collect_size;
					$q_margin_prod	= $Quote->margin_oem_product; 
					$q_margin_serv	= $Quote->margin_oem_service; 
					$q_margin_logs	= $Quote->margin_oem_logistic; 
					$q_currency		= $Quote->currency_symbol; 
					$q_rate			= $Quote->currency_rate; 
				}
				@endphp 
                <input value="{{ $q_ref }}" class=" form-control readonly" id="name" name="name"  type="text"> 
                @if ($errors->has('name'))
                <span class="help-block" style="color:red;">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group">

            <label for="name" class="col-sm-3 control-label" >Country: </label>
            <div class="col-sm-9">
                    <!-- <input value="{!!old('name')!!}" class=" form-control" id="name" name="name"  type="text">  -->
                <select name="country" id="country" class="form-control readonly">
                    @foreach($Countries as $Country)
                    <option value="{{$Country->id}}" {{($q_country == $Country->id) ? 'selected' : ''}}>{{$Country->name}}</option>
                    @endforeach
                </select>
                @if ($errors->has('name'))
                <span class="help-block" style="color:red;">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label for="name" class="col-sm-3 control-label">Region: </label>
            <div class="col-sm-9">
                <select name="region" id="region" class="form-control readonly">
                    @foreach($Regions as $Region)
                    <option value="{{$Region->id}}" {{($q_region == $Region->id) ? 'selected' : ''}}>{{$Region->name}}</option>
                    @endforeach
                </select>
                @if ($errors->has('region'))
                <span class="help-block" style="color:red;">
                    <strong>{{ $errors->first('region') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label for="ave_collect_size" class="col-sm-3 control-label">Collection Size: </label>
            <div class="col-sm-9">
                <input value="{{$q_collect}}" class="form-control readonly" id="collection_size" name="ave_collect_size"  type="text"> 
                @if ($errors->has('ave_collect_size'))
                <span class="help-block" style="color:red;">
                    <strong>{{ $errors->first('collection_size') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label for="name" class="col-sm-3 control-label">Currency: </label>
            <div class="col-sm-9">							
                <select name="currency" id="currency" class="form-control readonly">
                    <option value="{{$currency->currency_symbol}}" {{($q_currency == $currency->currency_symbol) ? 'selected' : ''}}>{{$currency->currency_symbol}}</option>
                    @if($q_country != 13)
					<option value="US$" {{($q_currency == '$' && $q_rate == 1) ? 'selected="selected"' : ''}}>US$</option>
					@endif
                </select>
                @if ($errors->has('name'))
                <span class="help-block" style="color:red;">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
        </div>
        
         @if($role == 2 || ($role == 3 && @in_array(2,$permission)) )
                @php $readonly = ""; @endphp
         @else
                @php $readonly = "readonly"; @endphp
				<input value="{{$q_margin_prod}}" name="org_margin_oem_product"  type="hidden"> 
				<input value="{{$q_margin_serv}}" name="org_margin_oem_services"  type="hidden">
				<input value="{{$q_margin_logs}}" name="org_margin_oem_logistics"  type="hidden">
        @endif

		@if($Quote->status != 1)
			@php $readonly = "readonly"; @endphp
		@endif
				
        <div class="form-group">
            <label for="margin_oem_product" class="col-sm-3 control-label">Product Margin: </label>
            <div class="col-sm-9">
                <input value="{{$q_margin_prod}}" class="form-control readonly" id="margin_oem_product" name="margin_oem_product"  type="text" {{$readonly}}> 
				@if($Quote->status == 1)								
				<span>
				<input type="checkbox" value="1" class="" id="margin_apply_product" name="margin_apply_product"> 
				<small>Check this box to apply above Product Margin to all items.</small>
				</span><br>
				@endif

                @if ($errors->has('margin_oem_product'))
                <span class="help-block" style="color:red;">
                    <strong>{{ $errors->first('product_margin') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label for="margin_oem_service" class="col-sm-3 control-label">Service Margin: </label>
            <div class="col-sm-9">
                <input value="{{$q_margin_serv}}" class="form-control readonly" id="margin_oem_service" name="margin_oem_service" type="text" {{$readonly}}> 
				@if($Quote->status == 1)								
				<span>
				<input type="checkbox" value="1" class="" id="margin_apply_service" name="margin_apply_service"> 
				<small>Check this box to apply above Services Margin to all items.</small>
				</span>
				<br>
				@endif

                @if ($errors->has('margin_oem_service'))
                <span class="help-block" style="color:red;">
                    <strong>{{ $errors->first('margin_oem_service') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label for="margin_oem_logistic" class="col-sm-3 control-label">Logistics Fee: </label>
            <div class="col-sm-9">
                <input value="{{$q_margin_logs}}" class="form-control readonly" id="margin_oem_logistic" name="margin_oem_logistic"  type="text" {{$readonly}}> 
				@if($Quote->status == 1)								
				<span>
				<input type="checkbox" value="1" class="" id="margin_apply_logistic" name="margin_apply_logistic"> 
				<small>Check this box to apply above Logistics Margin to all items.</small>
				</span><br>
				@endif

                @if ($errors->has('margin_oem_logistic'))
                <span class="help-block" style="color:red;">
                    <strong>{{ $errors->first('margin_oem_logistic') }}</strong>
                </span>
                @endif
            </div>
        </div>
										

 

        <div class="form-group">
            <div class="col-sm-3">
            </div>
        
            <div class="col-sm-9">
                <div class="row">
                    <div class="col-sm-6">
                        @if (empty($Quote->status) || $Quote->status == 1)
                		<button type="submit" class="btn btn-lg btn-block btn-cta">Update</button>
                		@endif 
                    </div>                    
                    <div class="col-sm-6">
                        <button class="btn btn-block btn-lg btn-cta add-note-btn" type="button">Add a Note</button>
                        <input type="hidden" name="display_level" id="display_level">
                        <input type="hidden" name="note" id="note">
                    </div>

                </div>
                
                @if(isset($quote_notes) && count($quote_notes) > 0)
					<h3 class="text-center" style="margin-top:25px"><i class="fa fa-list-alt"></i> Notes</h3>
					<div class="table-responsive">
					<table class="table table-notes table-striped table-bordered">
					<thead>
					<tr>
					<th>Date</th>
					<th>User</th>
					<th>Note</th>
					</tr>	
					</thead>
					<tbody>
					@foreach($quote_notes as $quote_note)
						@if(!empty($quote_note->note))
						<tr>
						<td>{{@$quote_note->date_created}}</td>
						<td>{{@$quote_note->username->first_name}} {{@$quote_note->username->last_name}}</td>
						<td>{{@$quote_note->note}}</td>
						</tr>
						@endif
					@endforeach 
					</tbody>
					</table>
					</div> 
				@endif
						

				
            </div>
        </div>
              
    </form>               

</div>
<!--==============================Modal Popup Quote Note=======================-->
<div class="modal modal-success fade in" id="quote-note-modal" tabindex="-1" role="dialog" style="display: none;">
	<form class="form-signin" method="post" action="{{url('/')}}/Enterprises/addnote">
	{{ csrf_field() }}
	<input type="hidden" name="quote_id" value="{{$Quote->id}}">
	<input type="hidden" name="quote_status" id="quote_status" value="{{$Quote->status}}">

    <div class="modal-dialog" role="document" style="margin:150px auto;">
        <div class="modal-content">
            <div class="modal-header">
            <b>Add Quote Note</b>
                <button type="button" id="close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body" style="height: auto;overflow: auto;">
                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
 
				<input type="hidden" name="display_level_pop" id="display_level_pop" value="2"/>

    
                <div class="col-lg-12">                                        
                    <label for="option_id" class="col-sm-3 control-label" style="font-size: 14px;">Note<span style="color:red;">*</span></label>
                    <div class="col-sm-9">
                        
                        <textarea name="note" id="note_pop" class="form-control" placeholder="Add a note"></textarea>
                         <span class="help-block" style="color:red;">
                            <strong class="note_error"></strong>
                        </span>
                        &nbsp
                    </div>
                </div>

                <div class="col-lg-12">                                        
                   <div class="col-sm-3"></div>
                    <div class="col-sm-9">
                      <button class="btn btn-lg btn-cta btn-block btn-add-sub" onclick="myNoteFunction();">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
	</form>   
</div>
</div>
<!--==============================Modal Popup Quote Note=======================--> 
@section('js')
<script>
    $(document).ready(function () {
        $('#country').on('change', function () {
            var countryId = $(this).val();
            var v_token = "{{csrf_token()}}";
            if (countryId != '') {

                $.ajax({
                    type: 'post',
                    url: SITE_URL + '/EnterpriseQuote',
                    data: {id: countryId, _token: v_token},
                    dataType: 'JSON',
                    success: function (response) {
                        var option1 = '';
                        var option = "";
                        if(countryId != 13){
                            option += "<option value='US$'> US$ </option>";
                        }

                        for (var item in response) {
                            var obj1 = response[item];
                            option1 += '<option value="' + obj1.region_id + '"> ' + obj1.region + ' </option>';
                        }
                        option += '<option selected="selected" value="' + obj1.currency_symbol + '"> ' + obj1.currency_symbol + ' </option>';
                        $('#region').html(option1);
                        $('#currency').html(option);
                    },
                    error: function (error) {
                        alert('error block');
                    }
                });
            }
        });

        if($('#quote_status').val() != 1){
            $('.readonly').attr('disabled','')
           // $('.readonly option').attr('disabled','')
        }

    });
    $('.add-note-btn').click(function(){

        $('#quote-note-modal').modal('show');

    })

function myNoteFunction(){
    var display_level_pop = $('#display_level_pop').val();
    var note_pop = $('#note_pop').val();


    if(display_level_pop == ''){
        $('.level_error').text('The display level field is required.');
        return false;
    }else{
       $('.level_error').text(''); 
    }
    if(note_pop == ''){
        $('.note_error').text('The note field is required.');
        return false;
    }else{
       $('.note_error').text(''); 
    }
    
    if(display_level_pop != '' && note_pop != ''){
        $('#display_level').val(display_level_pop);
        $('#note').val(note_pop);
        $('#quote-note-modal').modal('hide');
    }

}   
</script>
@endsection
@endsection
