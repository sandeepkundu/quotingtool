@extends('layouts.hpUser') 
@section('content')
<style>
    
</style>
<div class="page-title">
    <div class="container">
        <div class="pull-left">
            <span class="page-heading-left" style="font-weight:600;border:none;font-size: 24px;">  {{@$client->name}}  </span>
        </div>
        <div class="pull-right">
            <span class="page-heading-center">
                Quote Settings
            </span>
        </div>
    </div>    
</div>
<div class="container">

    <form class="form-horizontal form-signin" method="post" action="{{url('/')}}/Enterprises/SaveEnterpriseQuote">
        {{ csrf_field() }}
        <input type="hidden" name="client_id" value="{{$client->id}}">
        <div class="form-group">
            <label for="name" class="col-sm-3 control-label">Name / Reference:</label>
            <div class="col-sm-9">
                <input value="{{ $client->name }}" class="form-control" id="name" name="name" type="text"> 
                @if ($errors->has('name'))
                <span class="help-block" style="color:red;">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label for="name" class="col-sm-3 control-label" style="">Country: </label>
            <div class="col-sm-9">
                <!-- <input value="{!!old('name')!!}" class=" form-control" id="name" name="name"  type="text">  -->
                <select name="country" id="country" class="form-control">
                    @foreach($Countries as $Country)
                    <option value="{{$Country->id}}" {{($client->country == $Country->id) ? 'selected' : ''}}>{{$Country->name}}</option>
                    @endforeach
                </select>
                @if ($errors->has('name'))
                <span class="help-block" style="color:red;">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label for="region" class="col-sm-3 control-label" style="">Region: </label>
            <div class="col-sm-9">
                <select name="region" id="region" class="form-control">
                    @foreach($Regions as $Region)
                    <option value="{{$Region->id}}" {{($client->region == $Region->id) ? 'selected' : ''}}>{{$Region->name}}</option>
                    @endforeach
                </select>
                @if ($errors->has('region'))
                <span class="help-block" style="color:red;">
                    <strong>{{ $errors->first('region') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label for="collection_size" class="col-sm-3 control-label">Collection Size: </label>
            <div class="col-sm-9">
                <input value="{{$collection_size}}" class=" form-control" id="collection_size" name="collection_size"  type="text"> 
                @if (@$errors->has('collection_size'))
                <span class="help-block" style="color:red;">
                    <strong>{{ @$errors->first('collection_size') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label for="currency" class="col-sm-3 control-label" style="">Currency: </label>
            <div class="col-sm-9">
                <select name="currency" id="currency" class="form-control">

                    <option value="{{@ $currency->currency_symbol}}" {{(@$client->country == @$Country->id) ? 'selected' : ''}}>{{@$currency->currency_symbol}}</option>
                    @if($Country->id != 13)
                    <option value="US$">US$</option>
					@endif
                </select>
                @if ($errors->has('name'))
                <span class="help-block" style="color:red;">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
        </div>
            @if($role == 2 || ($role == 3 && @in_array(2,$permission)) )
               
        <div class="form-group">
            <label for="product_margin" class="col-sm-3 control-label" style="">Product Margin: </label>
            <div class="col-sm-9">
                <input value="{{$product_margin}}" class=" form-control" id="product_margin" name="product_margin" type="text"> 
                @if ($errors->has('product_margin'))
                <span class="help-block" style="color:red;">
                    <strong>{{ $errors->first('product_margin') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label for="service_margin" class="col-sm-3 control-label" style="">Service Margin: </label>
            <div class="col-sm-9">
                <input value="{{$service_margin}}" class=" form-control" id="service_margin" name="service_margin" type="text"> 
                @if ($errors->has('service_margin'))
                <span class="help-block" style="color:red;">
                    <strong>{{ $errors->first('service_margin') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label for="logistic_fee" class="col-sm-3 control-label" style="">Logistics Fee: </label>
            <div class="col-sm-9">
                <input value="{{$logistic_fee}}" class=" form-control" id="logistic_fee" name="logistic_fee"  type="text"> 
                @if ($errors->has('logistic_fee'))
                <span class="help-block" style="color:red;">
                    <strong>{{ $errors->first('logistic_fee') }}</strong>
                </span>
                @endif
            </div>
        </div>
        @endif
                               
        <div class="form-group">
            <label for="" class="col-sm-3 control-label"></label>
            <div class="col-sm-9">
                <button type="submit" class="btn btn-cta btn-block btn-lg">Next</button>
            </div>
        </div>


    </form>


    <form class=">
        
        
        <div class="form-group"> 
            <div class="row"> 




            </div>
        </div>
    </form>               

</div>

<!--==============================Modal Popup Quote Note=======================-->
<div class="modal modal-success fade in" id="quote-note-modal" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-dialog" role="document" style="margin:150px auto;">
        <div class="modal-content">
            <div class="modal-header">
            <b>Add Quote Note</b>
                <button type="button" id="close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body" style="height: auto;overflow: auto;">
                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                 <div class="col-lg-12">
                    <label for="display_level" class="col-sm-3 control-label" style="font-size: 14px;">Display Lavel<span style="color:red;">*</span></label>
                    <div class="col-sm-9">
                        <select name="display_level_pop" id="display_level_pop" class="form-control">
                            <option value="">--Select Display Level--</option>
                            <option value="1">Admin</option>
                            <option value="2">User</option>
                        </select>


                        <span class="help-block" style="color:red;">
                            <strong class="level_error"></strong>
                        </span>

                        &nbsp
                    </div>
                </div>
                <div class="col-lg-12">                                        
                    <label for="option_id" class="col-sm-3 control-label" style="font-size: 14px;">Note<span style="color:red;">*</span></label>
                    <div class="col-sm-9">
                        
                        <textarea name="note" id="note_pop" class="form-control" placeholder="Add a note"></textarea>
                         <span class="help-block" style="color:red;">
                            <strong class="note_error"></strong>
                        </span>
                        &nbsp
                    </div>

                </div>
                <div class="col-lg-12">                                        
                   <div class="col-sm-3"></div>
                    <div class="col-sm-9">
                      <button class="btn btn-block btn-add-sub" onclick="myNoteFunction();">Submit</button>
                        &nbsp
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!--==============================Modal Popup Quote Note=======================--> 

@section('js')
<script>
    $(document).ready(function () {
        $('#country').on('change', function () {
            var countryId = $(this).val();
            var v_token = "{{csrf_token()}}";
            if (countryId != '') {

                $.ajax({
                    type: 'post',
                    url: SITE_URL + '/EnterpriseQuote',
                    data: {id: countryId, _token: v_token},
                    dataType: 'JSON',
                    success: function (response) {
                        var option1 = '';
                        var option = "";
                        if(countryId != 13){
                            option += "<option value='US$'> US$ </option>";
                        }
                        for (var item in response) {
                            var obj1 = response[item];
                            option1 += '<option value="' + obj1.region_id + '"> ' + obj1.region + ' </option>';
                        }
                        option += '<option selected="selected" value="' + obj1.currency_symbol + '"> ' + obj1.currency_symbol + ' </option>';
                        $('#region').html(option1);
                        $('#currency').html(option);
                    },
                    error: function (error) {
                        alert('error block');
                    }
                });
            }
        });
    });
$('.add-note-btn').click(function(){

        $('#quote-note-modal').modal('show');

    })
function myNoteFunction(){
    var display_level_pop = $('#display_level_pop').val();
    var note_pop = $('#note_pop').val();


    if(display_level_pop == ''){
        $('.level_error').text('The display level field is required.');
        return false;
    }else{
       $('.level_error').text(''); 
    }
    if(note_pop == ''){
        $('.note_error').text('The note field is required.');
        return false;
    }else{
       $('.note_error').text(''); 
    }
    
    if(display_level_pop != '' && note_pop != ''){
        $('#display_level').val(display_level_pop);
        $('#note').val(note_pop);
        $('#quote-note-modal').modal('hide');
    }

}
</script>
@endsection
@endsection
