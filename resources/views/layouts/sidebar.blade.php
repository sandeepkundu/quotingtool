<style type="text/css">
  .icon .user:after{
    border:none !important;
  }
</style>
<aside id="sidebar-left" class="sidebar-circle">
    <!-- Start left navigation - profile shortcut -->
    <!-- <div id="tour-8" class="sidebar-content">
        <div class="media">
            <a class="pull-left has-notif avatar" href="{{url('/')}}/users/viewProfile">
                <?php
                    /*if(Auth::user()->image !=''){
                         $image = Auth::user()->image; 
                    }else{
                       $image = 'logo.png'; 
                    }*/ 
                    //echo $image;die;
                ?>
                <img class="logo" src="{{ asset('images/')}}<?php echo '/'//.$image;?>" alt="admin">
                <i class="online"></i>
            </a>
            <div class="media-body">
                <h4 class="media-heading"> 
                    @if(isset(Auth::user()->username))
                    {{ Auth::user()->username }}
                    @endif
                </h4>
                <small></small>
            </div>
        </div>
    </div>--><!-- /.sidebar-content --> 
    <!--/ End left navigation -  profile shortcut -->

    <?php $url = $_SERVER['REQUEST_URI']; ?>
    <!-- Start left navigation - menu -->
    <ul id="tour-9" class="sidebar-menu">
        <?php 
        $a =''; $b=''; $m=''; $s='';
        if (strpos($url, '/users/index') !== false || strpos($url, '/users/addUser') !== false || strpos($url, '/users/editUser') !== false) {
           $a='active';
           $m='active';
           $s='selected';
       } else if (strpos($url, '/clients/index') !== false || strpos($url, '/clients/addClient') !== false ||strpos($url, '/clients/editClient') !== false){
           $b='active';
           $m='active';
           $s='selected';
       }
       ?>
       <!-- =======================Start Clients===================== -->
       <li class="submenu <?php echo $m;?>">
        <a href="javascript:void(0);">
            <span class="icon"><i class="fa fa-user user"></i></span>
            <span class="text">Clients</span>
            <span class="arrow"></span>
            <span class="<?php echo $s; ?>"></span>
           
        </a>
        <ul>
            <li class=" <?php echo $a;?>"><a href="{{URL::to('/')}}/users/index">Users</a></li>
            <li class="<?php echo $b;?>"><a href="{{URL::to('/')}}/clients/index">Clients</a></li>
        </ul>
    </li>
    <!-- =======================================End Clients======================================= --> 


    <!-- =======================================Start Quotes======================================= -->
    <?php 
    $q_m=''; $qm =''; $qc=''; $qs='';  $selected='';
    if (strpos($url, '/QuoteManagements/index') !== false || strpos($url, '/QuoteManagements/QuoteDetails') !== false) {
       $qm='active';
       $q_m='active';
       $selected='<span class="selected"></span>';
   } else if (strpos($url, '/quoteStatus/index') !== false || strpos($url, '/quoteStatus/addQuoteStatus') !== false ||strpos($url, '/quoteStatus/editQuoteStatus') !== false){
       $qs='active';
       $q_m='active';
       $selected='<span class="selected"></span>';
   } else if (strpos($url, '/QuoteCollection/index') !== false || strpos($url, '/QuoteCollection/addQuoteCollection') !== false ||strpos($url, '/QuoteCollection/editQuoteCollection') !== false){
       $qc='active';
       $q_m='active';
       $selected='<span class="selected"></span>';
   }
   ?>

   <li class="submenu <?php echo $q_m;?>">
    <a href="javascript:void(0);">
        <span class="icon"><i class="fa fa-cube"></i></span>
        <span class="text">Quotes</span>
        <span class="arrow"></span>
        <?php echo $selected ; ?>
    </a>
    <ul>
        <li class="submenu <?php echo $qm; ?>"><a href="{{URL::to('/')}}/QuoteManagements/index">Quotes</a>
        </li>
        <li class="submenu <?php echo $qs; ?>"><a href="{{URL::to('/')}}/quoteStatus/index">Quote Status</a>
        </li>
        <li class="<?php echo $qc;?>"><a href="{{URL::to('/')}}/QuoteCollection/index">Collection Requests</a></li>
    </ul>
</li>
<!-- =======================================End Quotes======================================= -->

<!-- =======================================Start Reporting======================================= -->
<li class="submenu <?php //echo $m;?>">
    <a href="javascript:void(0);">
        <span class="icon"><i class="fa fa-cube"></i></span>
        <span class="text">Reporting</span>
        <span class="arrow"></span>
        
    </a>
    <ul>
        <li class="<?php echo $qc;?>"><a href="{{URL::to('/')}}/reporting/logins">Logins</a></li>
    </ul>
</li>
<!-- =======================================Start Reporting======================================= -->

<!-- =======================================Start Product======================================= -->
<?php 
$Pro_m=''; $products =''; $pro_op='';$pro_op_group=''; $selected=''; $DefectDVD=''; $priceBands=''; $priceFactorBands=''; $HDD_RAM=''; $RAM_Config=''; $HDD_Config='';$HDD_Price='';$RAM_Price='';$priceFactorBandsDVD='';$priceFactorBandsDefect='';
if (strpos($url, '/products/index') !== false || strpos($url, '/products/addProduct') !== false ||strpos($url, 'products/editProduct') !== false) {
   $products='active';
   $Pro_m='active';
   $selected='<span class="selected"></span>';
} else if (strpos($url, 'productOptions/index') !== false || strpos($url, 'productOptions/detailProductOptions') !== false ||strpos($url, '/productOptions/addProductOption') !== false ||strpos($url, 'productOptions/editProductOptions') !== false){
   $pro_op='active';
   $Pro_m='active';
   $selected='<span class="selected"></span>';
} 
else if (strpos($url, 'productOptions/index') !== false || strpos($url, 'productOptions/detailProductOptions') !== false ||strpos($url, '/productOptions/addProductOption') !== false ||strpos($url, 'productOptions/editProductOptions') !== false){
   $pro_op='active';
   $Pro_m='active';
   $selected='<span class="selected"></span>';
}
else if (strpos($url, 'poGroups/index') !== false || strpos($url, 'poGroups/detailsPog') !== false ||strpos($url, '/poGroups/addPog') !== false ||strpos($url, 'poGroups/editPog') !== false ||strpos($url, 'poGroups/addNewGroups') !== false){
   $pro_op_group='active';
   $Pro_m='active';
   $selected='<span class="selected"></span>';
}
else if (strpos($url, 'priceBands/index') !== false || strpos($url, 'priceBands/addPriceBands') !== false){
   $DefectDVD='active';
   $Pro_m='active';
   $priceBands='active';
   $selected='<span class="selected"></span>';
}

else if (strpos($url, 'ProductFactors/index?HDD') !== false || strpos($url, 'ProductFactors/addProductFactors?HDD') !== false ||strpos('HDD',key($_GET) ) !== false && strpos($url, 'ProductFactors/editProductFactors') !== false){
   $Pro_m='active'; 
   $HDD_RAM='active';
   $HDD_Config='active';
   $selected='<span class="selected"></span>';
}
else if (strpos($url, 'ProductFactors/index?RAM') !== false || strpos($url, 'ProductFactors/addProductFactors?RAM') !== false ||strpos('RAM', key($_GET)) !== false &&  strpos($url, 'ProductFactors/editProductFactors') !== false ){
   $Pro_m='active';
   $HDD_RAM='active';
   $RAM_Config='active';
   $selected='<span class="selected"></span>';
}
else if (strpos($url, 'PriceFactors/index?HDD') !== false || strpos($url, 'PriceFactors/addPriceFactors?HDD') !== false ||strpos('HDD',  key($_GET)) !== false && strpos($url, 'PriceFactors/editPriceFactors') !== false ||strpos('HDD', isset($_GET['cat']) ? $_GET['cat']:key($_GET)) !== false){
   $Pro_m='active';
   $HDD_RAM='active';
   $HDD_Price='active';
   $selected='<span class="selected"></span>';
}
else if (strpos($url, 'PriceFactors/index?RAM') !== false || strpos($url, 'PriceFactors/addPriceFactors?RAM') !== false ||strpos('RAM',  key($_GET)) !== false && strpos($url, 'PriceFactors/editPriceFactors') !== false){
   $Pro_m='active';
   $HDD_RAM='active';
   $RAM_Price='active';
   $selected='<span class="selected"></span>';
}
else if (strpos($url, 'priceFactorBands/index?Defects') !== false || strpos($url, 'priceFactorBands/addPriceFactorBands?Defects') !== false ||strpos('Defects', isset($_GET['cat']) ? $_GET['cat']:key($_GET)) !== false){
   $Pro_m='active';
   $DefectDVD='active';
   $priceFactorBandsDefect='active';
   $selected = '<span class="selected"></span>';
}

else if (strpos($url, 'priceFactorBands/index?DVD') !== false || strpos($url, 'priceFactorBands/addPriceFactorBands?DVD') !== false ||strpos('DVD', isset($_GET['cat']) ? $_GET['cat']:key($_GET)) !== false){
   $Pro_m='active';
   $DefectDVD='active';
   $priceFactorBandsDVD='active';
   $selected='<span class="selected"></span>';
}

?>
<li class="submenu <?php echo $Pro_m; ?>">
    <a href="javascript:void(0);">
        <span class="icon"><i class="fa fa-dropbox"></i></span>
        <span class="text">Products</span>
        <span class="arrow"></span>
        <?php echo $selected; ?>
    </a>
    <ul>
        <li> <li class="submenu <?php echo $products; ?>"><a href="{{URL::to('/')}}/products/index">Products </a></li>
        <li class="submenu <?php echo $pro_op; ?>"><a href="{{URL::to('/')}}/productOptions/index">Product Parts </a></li>
        <li class="submenu <?php echo $pro_op_group; ?>"><a href="{{URL::to('/')}}/poGroups/index">Product Option Groups</a></li>

        <li class="submenu <?php echo $HDD_RAM ?>">
            <a href="javascript:void(0);">
             <span class="icon"><i></i></span>
             <span class="text">HDD & RAM </span>
             <span class="arrow"></span>
             <?php echo $selected; ?>

         </a>
         <ul>
            <li class=" <?php echo $HDD_Config; ?> "><a href="{{URL::to('/')}}/ProductFactors/index?HDD">HDD Config</a></li>
            <li class=" <?php echo $RAM_Config; ?> "><a href="{{URL::to('/')}}/ProductFactors/index?RAM">RAM Config</a></li>
            <li class=" <?php echo $HDD_Price; ?> "><a href="{{URL::to('/')}}/PriceFactors/index?HDD">HDD Pricing</a></li>
            <li class=" <?php echo $RAM_Price; ?> "><a href="{{URL::to('/')}}/PriceFactors/index?RAM">RAM Pricing</a></li>
        </ul>
    </li>

    <li class="submenu <?php echo $DefectDVD; ?>">
        <a href="javascript:void(0);">
            <span class="icon"><i></i></span>
            <span class="text"> Defects & DVD</span>
            <span class="arrow"></span>
            <?php echo $selected; ?>
        </a>
        <ul>
            <li class=" <?php echo $priceBands; ?> "><a href="{{URL::to('/')}}/priceBands/index">Price Bands</a></li>
            <li class=" <?php  echo $priceFactorBandsDefect; ?> "><a href="{{URL::to('/')}}/priceFactorBands/index?Defects">Defect Pricing</a></li>
            <li class="<?php  echo $priceFactorBandsDVD; ?> "><a href="{{URL::to('/')}}/priceFactorBands/index?DVD">DVD Pricing</a></li>
        </ul>
    </li>
</ul>
</li>
<!-- =======================================End Product======================================= -->

<!-- =======================================Start Pricing======================================= -->
<?php 
$Price_m=''; $pma =''; $ps='';  $selected='';
if (strpos($url, 'PriceMarginAdjustments/index') !== false || strpos($url, '/PriceMarginAdjustments/addPriceMarginAdjustments') !== false ||strpos($url, 'PriceMarginAdjustments/editPriceMarginAdjustments') !== false) {
   $pma='active';
   $Price_m='active';
   $selected='<span class="selected"></span>';
} else if (strpos($url, 'priceServices/index') !== false || strpos($url, 'priceServices/addPriceServices') !== false ||strpos($url, 'priceServices/editPriceServices') !== false ){
   $ps='active';
   $Price_m='active';
   $selected='<span class="selected"></span>';
}
?>
<li class="submenu <?php echo $Price_m; ?>">
    <a href="javascript:void(0);">
        <span class="icon"><i class="fa fa-usd"></i></span>
        <span class="text">Pricing</span>
        <span class="arrow"></span>
        <?php echo $selected;?>
    </a>
    <ul>        

        <li class="submenu <?php echo $pma; ?>"><a href="{{URL::to('/')}}/PriceMarginAdjustments/index">Margin Adj.</a></li>
        <li class="submenu <?php echo $ps;?> "><a href="{{URL::to('/')}}/priceServices/index">Services</a></li>
    </ul>
</li>
<!-- =======================================End Pricing======================================= -->

<!-- =======================================Start Logistic======================================= -->
<?php 
$Log_m=''; $country =''; $lun=''; $price_log='';  $selected='';
if (strpos($url, 'countries/index') !== false || strpos($url, '/countries/addCountry') !== false ||strpos($url, 'countries/editCountry') !== false) {
   $country='active';
   $Log_m='active';
   $selected='<span class="selected"></span>';
} else if (strpos($url, 'LogisticsUnitNumbers/index') !== false || strpos($url, 'LogisticsUnitNumbers/addLogisticsUnitNumbers') !== false ||strpos($url, 'LogisticsUnitNumbers/editLogisticsUnitNumbers') !== false ){
   $lun='active';
   $Log_m='active';
   $selected='<span class="selected"></span>';
}
else if (strpos($url, 'PriceLogistics/index') !== false || strpos($url, 'PriceLogistics/addPriceLogistics') !== false ||strpos($url, 'PriceLogistics/editPriceLogistics') !== false ){
   $price_log='active';
   $Log_m='active';
   $selected='<span class="selected"></span>';
}
?>
<li class="submenu <?php echo $Log_m; ?>">
    <a href="javascript:void(0);">
        <span class="icon"><i class="fa fa-usd"></i></span>
        <span class="text">Logistics</span>
        <span class="arrow"></span>
        <?php echo $selected; ?>
    </a>
    <ul>        
        <li class="submenu <?php echo $country;?> "><a href="{{URL::to('/')}}/countries/index">Countries</a></li>
        <li class="submenu <?php echo $lun; ?>"><a href="{{URL::to('/')}}/LogisticsUnitNumbers/index">Logistic Volumes</a></li>
        <li class="submenu <?php echo $price_log; ?>"><a href="{{URL::to('/')}}/PriceLogistics/index">Pricing </a></li>
    </ul>
</li>
<!-- =======================================End Logistic======================================= -->

<!-- =======================================Start Permission======================================= -->
<?php 
$Permission=''; $permissions =''; $p_role=''; $roles='';  $selected='';
if (strpos($url, 'Permissions/index') !== false || strpos($url, '/Permissions/add') !== false ||strpos($url, 'Permissions/edit') !== false) {
   $permissions='active';
   $Permission='active';
   $selected='<span class="selected"></span>';
} else if (strpos($url, 'PermissionRoles/index') !== false || strpos($url, 'PermissionRoles/add') !== false ||strpos($url, 'PermissionRoles/edit') !== false ){
   $p_role='active';
   $Permission='active';
   $selected='<span class="selected"></span>';
} else if (strpos($url,'Roles/index') !== false || strpos($url, 'Roles/addRoles') !== false || strpos($url, 'Roles/editRoles/') !== false){
   $roles='active';
   $Permission='active';
   $selected='<span class="selected"></span>';
}

?>

<!-- =======================================End Permission======================================= -->

<!-- =======================================Start Settings======================================= -->
<?php 
$setting=''; $navigation=''; $QuoteValid =''; $qs=''; $qsi=''; $qsm='';   $selected=''; $default_services=''; $import_pup=''; $BulkImports=''; $theme=''; $ModelIMport= '';$DvdDefectIMport = '';$HddRamIMport='';$logisticIMport = '';$priceMrznAdustImp='';$servicesIMport = '';
if (strpos($url, 'QuoteValid/index') !== false || strpos($url, '/QuoteValid/addQuoteValid') !== false ||strpos($url, 'QuoteValid/editQuoteValid') !== false) {
   $QuoteValid='active';
   $setting='active';
   $selected='<span class="selected"></span>';
}elseif (strpos($url, 'Navigation/index') !== false || strpos($url, '/Navigation/addNavigation') !== false ||strpos($url, 'Navigation/editNavigation') !== false) {
   $navigation='active';
   $setting='active';
   $selected='<span class="selected"></span>';
}elseif (strpos($url, 'QuoteStepDefaults/index') !== false || strpos($url, '/QuoteStepDefaults/add') !== false ||strpos($url, '/QuoteStepDefaults/edit') !== false) {
   $default_services='active';
   $setting='active';
   $selected='<span class="selected"></span>';
}else if (strpos($url, 'Quotes/listQuoteStep') !== false || strpos($url, 'Quotes/addQuoteStep') !== false ||strpos($url, 'Quotes/editQuoteStep') !== false ){
   $qs='active';
   $qsm='active';
   $setting='active';
   $selected='<span class="selected"></span>';
 //}else if (strpos($url, 'QuotesNotes/index') !== false || strpos($url, 'QuotesNotes/add') !== false ||strpos($url, 'QuotesNotes/edit') !== false ){
   //$q_n='active';
   //$setting='active';
  // $selected='<span class="selected"></span>';
} else if (strpos($url, 'Quotes/addQuote') !== false || strpos($url, 'Quotes/listQuote') !== false || strpos($url, 'Quotes/editQuoteItem/') !== false){
   $qsi='active';
   $qsm='active';
   $setting='active';
   $selected='<span class="selected"></span>';
} else if (strpos($url, 'Theme/index') !== false || strpos($url, 'Theme/add') !== false || strpos($url, 'Theme/edit') !== false){
   $theme='active';
   $setting='active';
   $selected='<span class="selected"></span>';
} else if (strpos($url, 'Variances/BulkModelImports')){
   $BulkImports='active';
   $setting='active';
   $ModelIMport='active';
   $selected='<span class="selected"></span>';

}else if (strpos($url, 'Variances/BulkDvdDefectsImports')){
   $BulkImports='active';
   $setting='active';
   $DvdDefectIMport='active';
   $selected='<span class="selected"></span>';

}
else if (strpos($url, 'Variances/hdd-ram')){
   $BulkImports='active';
   $setting='active';
   $HddRamIMport='active';
   $selected='<span class="selected"></span>';
   
}else if (strpos($url, 'logistic-view')){
   $BulkImports='active';
   $setting='active';
   $logisticIMport='active';
   $selected='<span class="selected"></span>';
   
}else if (strpos($url, 'Variances/price-margin-import-view')){
   $BulkImports='active';
   $setting='active';
   $priceMrznAdustImp='active';
   $selected='<span class="selected"></span>';
   
}else if (strpos($url, 'Variances/import_pup')){
   $import_pup='active';
   $setting='active';
   $selected='<span class="selected"></span>';
}else if (strpos($url, 'Variances/services-import-view')){
   $BulkImports='active';
   $setting='active';
   $ServicesIMport='active';
   $selected='<span class="selected"></span>';
}

?>
<li class="submenu <?php echo $setting; ?>">
    <a href="javascript:void(0);">
        <span class="icon"> <i class="fa fa-cog fa-spin"></i></span>
        <span class="text">System</span>
        <span class="arrow"></span>
        <?php echo $selected; ?>
    </a>
    <ul>
        <li class="submenu <?php echo $QuoteValid;?>"><a href="{{URL::to('/')}}/QuoteValid/index">Quote Valid Duration</a>
        </li>

        <li class="submenu <?php echo $qsm; ?>">
          <a href="{{URL::to('/')}}/Quotes/listQuoteStep">Quote Steps</a>
       </li>
      <!-- <li class="submenu <?php //echo $q_n; ?>">
          <a href="{{URL::to('/')}}/QuotesNotes/index">Quote Notes</a>
       </li>-->

        <li class="submenu <?php echo $navigation; ?>"><a href="{{URL::to('/')}}/Navigation/index">CMS Page Management</a></li>
        <li class="submenu <?php echo $theme; ?>"><a href="{{URL::to('/')}}/Theme/index">Theme Management</a></li>
       <li class="submenu <?php echo $default_services; ?>"><a href="{{URL::to('/')}}/QuoteStepDefaults/index">Default Services</a></li>


        <li class="submenu <?php echo $BulkImports; ?>">
            <a href="javascript:void(0);">
             <span class="icon"><i></i></span>
             <span class="text">Bulk Import </span>
             <span class="arrow"></span>
             <?php echo $selected; ?>

            </a>
         <ul>
            <li class=" <?php echo $ModelIMport; ?>"><a href="{{URL::to('/')}}/Variances/BulkModelImports">Model Imports</a></li>

            <li class=" <?php echo $DvdDefectIMport; ?> "><a href="{{URL::to('/')}}/Variances/BulkDvdDefectsImports">Dvd/Defects Import</a></li>

            <li class=" <?php echo $HddRamIMport; ?> "><a href="{{URL::to('/')}}/Variances/hdd-ram">Hdd/Ram Import</a></li>

            <li class=" <?php echo $logisticIMport; ?> "><a href="{{URL::to('/')}}/Variances/logistic-view">Logistic Import</a></li>

            <li class=" <?php echo $priceMrznAdustImp; ?> "><a href="{{URL::to('/')}}/Variances/price-margin-import-view">Pricing Adjustments</a></li>
            
            <li class=" <?php echo $servicesIMport; ?> "><a href="{{URL::to('/')}}/Variances/services-import-view">Service Pricing Import</a></li>
        </ul>
      </li>

        <li class="submenu <?php echo $import_pup; ?> ">
          <a href="{{URL::to('/')}}/Variances/import_pup">Import Pup</a>
       </li>
       
   </ul>
</li>
<!-- =======================================End Settings======================================= -->
<!--/ End documentation - api documentation -->

</ul><!-- /.sidebar-menu -->

</aside><!-- /#sidebar-left -->