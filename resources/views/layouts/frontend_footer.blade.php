@php 
	$url = $_SERVER['REQUEST_URI']; 
    use App\Http\Controllers\HpUsersController;
    $footers = HpUsersController::Footer();
    $Theme = HpUsersController::Theme(); 
@endphp

<footer class="footer">
    <div class="container">
        <div class="copyright">
            <span class="credit">{{date("Y")}} &copy; Feilding Technology Pte Ltd.</span>
        
            @foreach ($footers as $footer) 
            	<a class="{{  strpos($url, '/'.$footer->name) !== false ? 'abc' : '' }}" href="{{url('/')}}/{{$footer->name}}">{{@$footer->display_name}}</a>
            @endforeach
            
        </div>
    </div><!-- /.container -->
</footer>
<!--<div id="search_overlay"></div>-->