<!-- START @WRAPPER -->
<?php $abc = URL::to('/'); ?>

<script>
var SITE_URL = "<?php echo $abc; ?>";

</script>
<form id="logout-form" action="{{ route('logout') }}" method="POST"
	style="display: none;">{{ csrf_field() }}</form>
<!-- START @HEADER -->
<header id="header">

	<!-- Start header left -->
	<div class="header-left">
		<!-- Start offcanvas left: This menu will take position at the top of template header (mobile only). Make sure that only #header have the `position: relative`, or it may cause unwanted behavior -->
		<div class="navbar-minimize-mobile left">
			<i class="fa fa-bars"></i>
		</div>
		<!--/ End offcanvas left -->

		<!-- Start navbar header -->
		<div class="navbar-header">

			<!-- Start brand -->
			<a id="tour-1" class="navbar-brand" href="{{url('/')}}/admin/dashboard">
			 <img class="logo" src="{{ asset('images/logo.svg')}}" alt="brand logo"
				style="height: 92%; margin-top: 1%; width: 96%;">
				
			</a>
			<!-- /.navbar-brand -->
			<!--/ End brand -->

		</div>
		<!-- /.navbar-header -->
		<!--/ End navbar header -->

		<!-- Start offcanvas right: This menu will take position at the top of template header (mobile only). Make sure that only #header have the `position: relative`, or it may cause unwanted behavior -->
		<div class="navbar-minimize-mobile right">
			<i class="fa fa-cog"></i>
		</div>
		<!--/ End offcanvas right -->

		<div class="clearfix"></div>
	</div>
	<!-- /.header-left -->
	<!--/ End header left -->

	<!-- Start header right -->
	<div class="header-right">
		<!-- Start navbar toolbar -->
		<div class="navbar navbar-toolbar">

			<!-- Start left navigation -->
			<ul class="nav navbar-nav navbar-left">

				<!-- Start sidebar shrink -->
				<li id="tour-2" class="navbar-minimize"><a
					href="javascript:void(0);" title="Minimize sidebar"> <i
						class="fa fa-bars"></i>
				</a></li>
				

			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li id="tour-6" class="dropdown navbar-profile"><a href="#"
					class="dropdown-toggle" data-toggle="dropdown"> <span class="meta">
					<!-- <span class="avatar">
					<?php
                   /* if(Auth::user()->image !=''){
                         $image = Auth::user()->image; 
                    }else{
                       $image = 'logo.png'; 
                    } */
                    //echo $image;die;
                ?>
               <img class="logo" src="{{ asset('images/')}}<?php echo '/'//.$image;?>" alt="admin"> 
                </span> -->
                 <span
							class="text hidden-xs hidden-sm text-muted"><?php if(isset(Auth::user()->username)) { echo Auth::user()->username; } ?></span> <span
							class="caret"></span>
					</span>
				</a> <!-- Start dropdown menu -->
					<ul class="dropdown-menu animated flipInX">
						<li class="dropdown-header">Account</li>
						<li><a href="{{URL::to('/')}}/users/viewProfile"><i class="fa fa-user"></i>View
								profile</a></li>
						
						<li class="divider"></li>
						<li><a href="{{ route('logout') }}"
							onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
								<i class="fa fa-key"></i> Log Out
						</a></li>
					</ul> <!--/ End dropdown menu --></li>
				<!-- /.dropdown navbar-profile -->
				<!--/ End profile -->

				<!-- Start settings -->
				<!-- <li id="tour-7" class="navbar-setting pull-right"><a
					href="javascript:void(0);"><i class="fa fa-cog fa-spin"></i></a></li> -->
				<!-- /.navbar-setting pull-right -->
				<!--/ End settings -->

			</ul>
			<!--/ End right navigation -->

		</div>
		<!-- /.navbar-toolbar -->
		<!--/ End navbar toolbar -->
	</div>
	<!-- /.header-right -->
	<!--/ End header left -->

</header>
<!-- /#header -->