<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->

<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- START @HEAD -->
<head>
    <!-- START @META SECTION -->

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="Quotingtool">
        <meta name="keywords" content="Quotingtool Quoting Quoting tool">
        <meta name="author" content="Djava UI">

        <title>DASHBOARD</title>

       

         <link rel="icon" href="{!! asset('images/logo.svg') !!}"/>
        <!--/ END FAVICONS -->


        <!-- START @FONT STYLES -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Oswald:700,400" rel="stylesheet">
        <!--/ END FONT STYLES -->

        <!-- START @GLOBAL MANDATORY STYLES -->
		<link href="{{ asset('assets/global/plugins/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
        
        <!--/ END GLOBAL MANDATORY STYLES -->
		<link href="{{asset('assets/global/plugins/bower_components/simple-line-icons/css/simple-line-icons.css')}}" rel="stylesheet">
        <link href="{{asset('assets/global/plugins/bower_components/datatables/css/dataTables.bootstrap.css')}}" rel="stylesheet">
        <link href="{{asset('assets/global/plugins/bower_components/datatables/css/datatables.responsive.css')}}" rel="stylesheet">
        <!-- START @PAGE LEVEL STYLES -->
		<link href="{{ asset('assets/global/plugins/bower_components/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/global/plugins/bower_components/animate.css/animate.min.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/global/plugins/bower_components/dropzone/downloads/css/dropzone.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/global/plugins/bower_components/jquery.gritter/css/jquery.gritter.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/global/plugins/bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css') }}" rel="stylesheet">
        <!--/ END PAGE LEVEL STYLES -->

        <!-- START @THEME STYLES -->
        <link href="{{ asset('assets/admin/css/reset.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/admin/css/layout.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/admin/css/components.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/admin/css/plugins.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/admin/css/themes/default.theme.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/admin/css/custom.css') }}" rel="stylesheet">
		
        <!--/ END THEME STYLES -->

        <!-- START @IE SUPPORT -->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="../../../assets/global/plugins/bower_components/html5shiv/dist/html5shiv.min.js"></script>
        <script src="../../../assets/global/plugins/bower_components/respond-minmax/dest/respond.min.js"></script>
        <![endif]-->
        <!--/ END IE SUPPORT -->
        <style type="text/css">
            .pagination>li>a, .pagination>li>span {
                position: initial;
                float: left;
                padding: 6px 12px;
                margin-left: -1px;
                line-height: 1.42857143;
                color: #337ab7;
                text-decoration: none;
                background-color: #fff;
                border: 1px solid #ddd;
            }
            .btn{
                position: initial;
            }

        </style>
    </head>

    <body class="page-session page-sound page-header-fixed page-sidebar-fixed demo-dashboard-session">

        <!--[if lt IE 9]>
        <p class="upgrade-browser">Upps!! You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- START @WRAPPER -->
        <section id="wrapper">

		<!--header start-->
		@include('layouts.header')
		<!--header end-->

		<!--sidebar start-->
		@include('layouts.sidebar')
		<!--sidebar end-->
        <section id="page-content">
  
		@yield('content')

        @include('layouts.footer')
        </section>
    </section>

        <!-- START @CORE PLUGINS -->
		<script type=text/javascript src="{{ asset('js/lib/bower_components/jquery/dist/jquery.min.js')}}"></script>
        <script src="{{ asset('assets/global/plugins/bower_components/jquery-cookie/jquery.cookie.js') }}"></script>
        <script src="{{ asset('assets/global/plugins/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/global/plugins/bower_components/typehead.js/dist/handlebars.js') }}"></script>
        <script src="{{ asset('assets/global/plugins/bower_components/typehead.js/dist/typeahead.bundle.min.js') }}"></script>
        <script src="{{ asset('assets/global/plugins/bower_components/jquery-nicescroll/jquery.nicescroll.min.js') }}"></script>
        <script src="{{ asset('assets/global/plugins/bower_components/jquery.sparkline.min/index.js') }}"></script>
        <script src="{{ asset('assets/global/plugins/bower_components/jquery-easing-original/jquery.easing.1.3.min.js') }}"></script>
        <script src="{{ asset('assets/global/plugins/bower_components/ionsound/js/ion.sound.min.js') }}"></script>
        <script src="{{ asset('assets/global/plugins/bower_components/bootbox/bootbox.js') }}"></script>
        <script src="{{ asset('assets/global/plugins/bower_components/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js')}}"></script>
        <script src="{{ asset('assets/global/plugins/bower_components/flot/jquery.flot.js') }}"></script>
        <script src="{{ asset('assets/global/plugins/bower_components/flot/jquery.flot.spline.min.js') }}"></script>
        <script src="{{ asset('assets/global/plugins/bower_components/flot/jquery.flot.categories.js') }}"></script>
        <script src="{{ asset('assets/global/plugins/bower_components/flot/jquery.flot.tooltip.min.js') }}"></script>
        <script src="{{ asset('assets/global/plugins/bower_components/flot/jquery.flot.resize.js') }}"></script>
        <script src="{{ asset('assets/global/plugins/bower_components/flot/jquery.flot.pie.js') }}"></script>
        <script src="{{ asset('assets/global/plugins/bower_components/dropzone/downloads/dropzone.min.js') }}"></script>
        <script src="{{ asset('assets/global/plugins/bower_components/jquery.gritter/js/jquery.gritter.min.js') }}"></script>
        <script src="{{ asset('assets/global/plugins/bower_components/skycons-html5/skycons.js')}}"></script>        
        <script src="{{ asset('assets/global/plugins/bower_components/chosen_v1.2.0/chosen.jquery.min.js') }}"></script>
        
        
         <!-- START @PAGE LEVEL PLUGINS -->      
        <script src="{{asset('assets/global/plugins/bower_components/jasny-bootstrap-fileinput/js/jasny-bootstrap.fileinput.min.js')}}"></script>
        <!--/ END PAGE LEVEL PLUGINS -->     

        <!-- START @PAGE LEVEL SCRIPTS -->
		<script type=text/javascript src="{{ asset('assets/admin/js/apps.js') }}"></script>
		
		<script type=text/javascript src="{{ asset('assets/admin/js/demo.js') }}"></script>

        <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
	<!--/ END JAVASCRIPT SECTION -->
	<!-- START GOOGLE ANALYTICS -->
	<script>

            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-55892530-1', 'auto');
            ga('send', 'pageview');
        </script>
		<script>
			$('.submenu').click(function() {
				$('.submenu').removeClass('active');
				$(this).addClass('active');
			});
			$(document).ready(function(){
				$(".multiIdCheck").change(function () {
              $(".id").prop('checked', $(this).prop("checked"));
          });
			});
		</script>
		
		@if(Session::has('toasts'))
        	<!-- Messenger http://github.hubspot.com/messenger/ -->
        	

        	<script type="text/javascript">
        		toastr.options = {
        			"closeButton": true,
        			"newestOnTop": true,
        			"positionClass": "toast-top-right"
        		};

        		@foreach(Session::get('toasts') as $toast)
        			toastr["{{ $toast['level'] }}"]("{{ $toast['message'] }}","{{ $toast['title'] }}");
        		@endforeach
        	</script>
        @endif
        @yield('js')

        <style>
            .form-control:focus{
                border: 1px solid #66afe9 !important;
            }
            .bg-warning{
                background-color:#00B1E1 !important;
                border:1px solid #00B1E1 !important;
            }
            input.no-border-right:focus, textarea.no-border-right:focus{
                border: 1px solid #66afe9 !important;
            }
        </style>
        
    </body>
</html>
