@php     
use App\Http\Controllers\HpUsersController;
@$Theme = HpUsersController::Theme_Cookie();

$logo = 'logo.svg';
$title = 'F5';
$folder = '';
$homecss = asset('css/login.css');

if(isset($Theme)){
if(!empty($Theme->logo)) {
$logo = @$Theme->logo;
}
if(!empty($Theme->theme_name)) {
$title = @$Theme->theme_name;
}
if(!empty($Theme->theme_folder)) {
$folder = @$Theme->theme_folder;
if(file_exists(asset('css').'/'. $folder . '/login.css')){
$homecss = asset('css').'/'. $folder . '/login.css';
}
}
}
@endphp
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>{{$title}}  | Login </title>
	<link rel="icon" href="{{ URL::to('/images') }}/{{$logo}}"/>
	<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('css/style.css') }}" rel="stylesheet">
	<link href="{{ $homecss }}" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css"><!-- Tostr css lib -->
	<script type=text/javascript src="{{ asset('js/lib/bower_components/jquery/dist/jquery.min.js')}}"></script>

</head>

<body class="login-page">  
	<div id="login-wrapper">

		<header class="header">
			<div class="container">
				<div class="login-logo">
					<img src="{{ URL::to('/images') }}/{{$logo}}" alt="{{$title}}" id="theme-logo" />  
				</div>
			</div>
		</header>

		@yield('content')

		<footer class="footer">
			<div class="container">
				<div class="copyright" style="text-align: center;">
					{{date("Y")}} &copy; Feilding Technology Pte Ltd.
				</div>
			</div>
		</footer>

	</div>
	<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script><!-- Tostr lib -->
	@if(Session::has('toasts'))
	<!-- Messenger http://github.hubspot.com/messenger/ -->
	<script type="text/javascript">
		toastr.options = {
			"closeButton": true,
			"newestOnTop": true,
			"positionClass": "toast-top-right"
		};

		@foreach(Session::get('toasts') as $toast)
		toastr["{{ $toast['level'] }}"]("{{ $toast['message'] }}","{{ $toast['title'] }}");
		@endforeach
	</script>
	@endif

</body>
</html>

