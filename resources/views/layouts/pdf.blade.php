<html lang="en">
    <head>
        <link href="{{ asset('assets/global/plugins/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    </head>

    <body class="">

       @yield('content')

       <script type=text/javascript src="{{ asset('js/lib/bower_components/jquery/dist/jquery.min.js')}}"></script>
        <script src="{{ asset('assets/global/plugins/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    </body>
</html>
