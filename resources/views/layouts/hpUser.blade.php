@php     
use App\Http\Controllers\HpUsersController;
@$Theme = HpUsersController::Theme_Cookie();
   
$logo = 'f5-logo.png';
$title = 'F5';
$ccFilename = 'style.css';
$ccFile = asset('css/'.$ccFilename);
if(isset($Theme)){
	if(!empty($Theme->logo)) {
		$logo = @$Theme->logo;
	}
	if(!empty($Theme->theme_name)) {
		$title = @$Theme->theme_name;
	}
	if(!empty($Theme->theme_folder)) {
		if(file_exists(public_path() . '/' . @$Theme->theme_folder . '/' . $ccFilename)){
			$ccFile = asset('/'). @$Theme->theme_folder . '/' . $ccFilename;
		}
	}
}
@endphp
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="{!! asset('images').'/'.$logo !!}"/>
	<title>{{$title}} | Dashboard</title>
	<link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
	<link href="{{ $ccFile }}" rel="stylesheet">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	@yield('css')
	<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
	<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script src="{{asset('js/hpdashboard/jquery.min.js')}}"></script>
	
	<!--<script src="{{asset('js/hpdashboard/bootstrap.js')}}"></script>-->
</head>

    <body>
      @include('layouts.frontend_header')
          
      @yield('content')
        
      @include('layouts.frontend_footer')

	<!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>

<!--  <script src="{{asset('js/hpdashboard/jquery.min.js')}}"></script>
  
  <script src="{{asset('js/hpdashboard/bootstrap.js')}}"></script>
-->  <script src="{{ asset('js/bootstrap.js') }}"></script>

 
  <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
  <script type=text/javascript src="{{ asset('js/jquery.cookie.min.js') }}"></script>
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

  @if(Session::has('toasts'))
          <!-- Messenger http://github.hubspot.com/messenger/ -->

          <script type="text/javascript">
            toastr.options = {
              "closeButton": true,
              "newestOnTop": true,
              "positionClass": "toast-top-right"
            };

            @foreach(Session::get('toasts') as $toast)
              toastr["{{ $toast['level'] }}"]("{{ $toast['message'] }}","{{ $toast['title'] }}");
            @endforeach
          </script>
        @endif
  <!--script src="{{ asset('assets/global/plugins/bower_components/ie-emulation-modes-warning.js') }}"></script-->
  <script type="text/javascript">
      $(document).ready(function () {
        $('.NewQuoteTrigger').click(function(){
          $('.new-quate-btn').trigger('click');
        });
     });

  </script>
  @yield('js')
</body>
</html>
