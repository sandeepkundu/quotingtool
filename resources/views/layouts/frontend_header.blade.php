<?php $abc = URL::to('/'); ?>
<?php 
use App\Http\Controllers\HpUsersController;

$all_nav = HpUsersController::Navigation();
$Theme = HpUsersController::Theme(); 

$logo = 'f5-logo.png';
$title = 'F5';
if(isset($Theme)){
	if(!empty($Theme->theme->logo)) {
		$logo = @$Theme->theme->logo;
	}
	if(!empty($Theme->theme_name)) {
		$title = @$Theme->theme->theme_name;
	}
}

?>
<script>
var SITE_URL = "<?php echo $abc; ?>";
</script>

<form id="logout-form" action="{{ route('logout') }}" method="POST"
style="display: none;">{{ csrf_field() }}</form>
<header class="header clearfix" style="height:auto">
    <div class="container">
        <nav class="navbar navbar-custom">


        
            <div class="" id="navbar-topnav">                  
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="{{URL::to('/')}}/dashboard" class="topnav-link"><span>Dashboard</span> <i class="fa fa-dashboard" aria-hidden="true"></i></a></li>                   
                    <?php
                    $ClientUsers= HpUsersController::hpDashboardQuote();
                    ?>

                    @if(count($ClientUsers)>1)
            			<li><a href="javascript:void(0)" onclick="clientsList();" class="topnav-link"><span>Clients</span> <i class="fa fa-users" aria-hidden="true"></i></a></li>                    
                    @else
            			@foreach($ClientUsers as $code)
            				@if($code->clientss->client_type=="enterprise")
            					<form action="{{URL('/')}}/store/client" method="post" class="navbar-form navbar-left clientform">
            					    <div class="form-group">
            					{{csrf_field()}}
            					<input type='hidden' value="{{$code->clientss->id}}" name="client_name" class="clientnames" /><a href="javascript:void(0)" class="clientGos topnav-link"><span>Quote</span> <i class="fa fa-plus" aria-hidden="true"></i></a> 
            					    </div>
            					</form>
            				@elseif($code->clientss->client_type=="store")
            					<form action="{{URL('/')}}/store/client" method="post" class="navbar-form navbar-left clientform">
            					    <div class="form-group">
            					{{csrf_field()}}
            					<input type='hidden' value="{{$code->clientss->id}}" name="client_name" class="clientnames" /><a href="javascript:void(0)" class="clientGos topnav-link"><span>Quote</span> <i class="fa fa-plus" aria-hidden="true"></i></a> 
            					    </div>
            					</form>
            				@endif
            			@endforeach
                    @endif

                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="topnav-link"><span>Log Out</span> <i class="fa fa-sign-out"></i></a></li>
                </ul>
            </div><!-- /.navbar-collapse -->

            <div class="navbar-header">
              <a class="navbar-brand" href="{{url('/dashboard')}}"><img src="/images/{{$logo}}" alt="{{$title}}" id="img-logo" /></a>
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-mainnav" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>

            </div>
        </nav>

        @if(count($all_nav)>1)
              
        <nav class="navbar navbar-conditional">				  

            <div class="collapse navbar-collapse" id="navbar-mainnav">
                
                <?php /* //<!-- demo, please remove --> ?>
                <ul class="nav navbar-nav">
                <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
                <li><a href="#">Link</a></li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#">One more separated link</a></li>
                  </ul>
                </li>
              </ul>
              <?php //<!--demo, please remove --> */ ?>
              
                @if(count($all_nav)>1)
                    <ul class="nav navbar-nav">	
                    <?php $url = $_SERVER['REQUEST_URI']; ?>
                    @foreach ($all_nav as $nav)
                        <li class="{{  strpos($url, '/page/'.$nav->name) !== false ? 'active' : '' }}"><a href="{{url('/')}}/page/{{$nav->name}}">{{$nav->name}}</a></li>
                    @endforeach
                    </ul>	
                @endif                    
            </div><!-- /.navbar-collapse -->				 
        </nav>

    @endif   
    </div>
</header>

<!--==============================Modal Popup New Quote=======================-->
<div class="modal modal-success fade in" id="modal-bootstrap-tour_quote" tabindex="-1" role="dialog" style="display: none;">
<div class="modal-dialog" role="document" style="margin: 150px auto;">
<div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
    <h4 class="modal-title">Select Client</h4>
    </div>

    <div class="modal-body" style="height:100%;">

        <form action="{{URL('/')}}/store/client" method="post" class="clientforms">
        {{csrf_field()}}
        <select name="client_name" id="clientname" class="form-control">
        <option value="">--Select Client-- </option>
        @if(count($ClientUsers) > 0)
			@foreach ($ClientUsers as $ClientUser) 
				@if($ClientUser->clientss != null)
				<option value="{{$ClientUser->clientss->id}}">{{$ClientUser->clientss->name}}</option>
				@endif
            @endforeach
		@endif
        </select>                        
        <span class="error" style="color:red;"></span><br/>
        <p class="text-right">
        <input type="submit" name="" class="btn btn-cta clientGo" value="Go"></p>
        </form>

    </div>
</div>
</div>
</div>
<!--==============================Modal Popup New Quote=======================-->
<script src="{{asset('js/hpdashboard/jquery.min.js')}}"></script>   
<script type="text/javascript">
    function clientsList() {
       $('#modal-bootstrap-tour_quote').modal('show');
   }

   $(document).ready(function () {
       /* $('#clientname').change(function(){
            var client = $(this).val();
           $('#client_hidden').val(client);
       });*/

       $('.clientGo').click(function () {

        var client = $('#clientname').val();

        if (client == '') {
            $('.error').text('Please select client.');
            return false;
        }else{
           $('.error').text('');
       }
   });
       $('.clientGos').click(function () {

        var client = $('.clientnames').val();

        if (client != '') {
          $('.clientform').submit();
      }
  });

       

   });
</script>
