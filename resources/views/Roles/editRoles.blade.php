@extends('layouts.default') @section('content')
<!-- START @PAGE LEVEL STYLES -->
<link href="{{ asset('css/bower_components/fontawesome/css/font-awesome.min.css')}}" rel="stylesheet">
<link href="{{ asset('css/bower_components/animate.css/animate.min.css')}}" rel="stylesheet">

<link href="{{asset('assets/global/plugins/bower_components/bootstrap-wysihtml5/src/bootstrap-wysihtml5.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/bower_components/summernote/dist/summernote.css')}}" rel="stylesheet">
<link href="{{ asset('css/bower_components/dropzone/downloads/css/dropzone.css') }}" rel="stylesheet">
<link href="{{ asset('assets/global/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet">
<link href="{{ asset('assets/global/plugins/bower_components/jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/global/plugins/bower_components/chosen_v1.2.0/chosen.min.css') }}" rel="stylesheet">
<style type="text/css">
  .note-editor .note-editable
  {
    background: #fff;
  }
</style>
<!--/ END PAGE LEVEL STYLES -->
<div class="body-content animated fadeIn">
  <div class="row">
    <div class="col-lg-12">
      <div class="panel rounded shadow">
        <div class="panel-heading">
          <div class="pull-left">
            <h3 class="panel-title">ADD ROLES</h3>
          </div>
          <div class="clearfix"></div>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body no-padding">
          <form class="form-horizontal mt-10" id="commentForm" method="post" action="{{URL::to('/')}}/Roles/updateRoles" enctype="multipart/form-data">
           {{ csrf_field() }}
           <input type="hidden" name="id" value="{{$Role->id}}">
           <div class="form-body">
            <div class="form-group">
              <div class="col-sm-12">
                <div class="col-sm-6">
                 <label for="" class="col-sm-3 control-label">Name<span style="color:red;">*</span></label>
                 <div class="col-sm-9">
                  <input type="text" name="name" id="name" value="{{$Role->name}}" class="form-control"/ placeholder="Name">
                  <input type="hidden" name="hidden_name" value="{{$Role->name}}">
                  
                  @if ($errors->has('name'))
                  <span class="help-block" style="color:red;">
                    <strong>{{ $errors->first('name') }}</strong>
                  </span>
                  @endif
                  &nbsp;
                </div>
              </div>
              <div class="col-sm-6">
                <label for="display_name" class="col-sm-3 control-label">Display Name<!-- <span style="color:red;">*</span> --></label>
                <div class="col-sm-9">
                  <input value="{{$Role->display_name}}"  type="text" name="display_name" class="form-control" id="display_name" placeholder="Display Name">
                  
                  @if ($errors->has('display_name'))
                  <span class="help-block" style="color:red;">
                    <strong>{{ $errors->first('display_name') }}</strong>
                  </span>
                  @endif
                  &nbsp;
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="col-sm-6">
              <label for="description" class="col-sm-2 control-label" style="font-weight: 800; text-align: left;">Permissions</label>
              </div>
           </div>

            <div class="col-sm-12">
                @if(count($permissions)>0)
              @foreach($permissions as $val)
                <label class="col-sm-2 control-label"> {{$val->name}}</label>
                <div class="col-sm-1">
                  <div class="ckbox ckbox-info">
                  <?php 
                    if(in_array($val->id, $a)){
                      $abc = 'checked';
                    }else{
                      $abc = '';
                    }
                  ?>
                    <input id="checkbox-info{{$val->id}}" type="checkbox" name="{{$val->name}}" class="pull-right" {{$abc}}>
                    <label for="checkbox-info{{$val->id}}"></label>
                  </div>
                </div>
              @endforeach
              @endif
            </div>


            <div class="col-sm-12">
              <div class="col-sm-12">
               <label for="description" class="col-sm-2 control-label" style="font-weight: 800; text-align: left;">Description<!-- <span style="color:red;">*</span> --></label>
               <div class="col-sm-12">
                <textarea type="text" name="description" class="form-control" id="summernote-textarea"
                >{{$Role->description}}</textarea>
                @if ($errors->has('description'))
                <span class="help-block" style="color:red;">
                  <strong>{{ $errors->first('description')}}</strong>
                </span>
                @endif
                &nbsp;
              </div>
            </div>
          </div>
        </div>
        <!-- /.form-group -->
        <div class="form-group"></div>
        <!-- /.form-group -->
      </div>
      <!-- /.form-body -->
      <div class="form-footer">
        <div class="col-sm-offset-5">
          <button class="btn btn-color btn-hover" type="submit">Submit</button>
          <a class="btn btn-danger" href="{{URL::to('/')}}/Roles/index">Cancel</a>
        </div>
      </div>

    </form>
  </div>
</div>
<!-- /.panel -->
<!--/ End horizontal form -->
</div>
</div>
</div>
@section('js')
<!-- START @PAGE LEVEL SCRIPT -->
<script type=text/javascript src="{{ asset('js/lib/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js') }}"></script>
<script type=text/javascript src="{{ asset('js/lib/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.min.js') }}"></script>
<script type=text/javascript src="{{ asset('js/pages/blankon.form.advanced.js') }}"></script>
<script src="{{ asset('assets/global/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>
<script src="{{ asset('assets/global/plugins/bower_components/jasny-bootstrap-fileinput/js/jasny-bootstrap.fileinput.min.js')}}"></script>
<script src="{{ asset('assets/global/plugins/bower_components/holderjs/holder.js')}}"></script>
<script src="{{ asset('assets/global/plugins/bower_components/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
<script src="{{ asset('assets/global/plugins/bower_components/jquery-autosize/jquery.autosize.min.js')}}"></script>
<script src="{{ asset('assets/global/plugins/bower_components/chosen_v1.2.0/chosen.jquery.min.js')}}"></script>

<script src="{{asset('assets/global/plugins/bower_components/bootstrap-wysihtml5/lib/js/wysihtml5-0.3.0.min.js')}}"></script>
<script src="{{asset('assets/global/plugins/bower_components/bootstrap-wysihtml5/src/bootstrap-wysihtml5.js')}}"></script>
<script src="{{asset('assets/global/plugins/bower_components/summernote/dist/summernote.min.js')}}"></script>


<!--         <script src="{{asset('assets/admin/js/pages/blankon.form.wysiwyg.js')}}"></script>  -->

<script type="text/javascript">
  var BlankonFormWysiwyg = function () {

    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
          BlankonFormWysiwyg.bootstrapWYSIHTML5();
          BlankonFormWysiwyg.summernote();
        },

        // =========================================================================
        // BOOTSTRAP WYSIHTML5
        // =========================================================================
        bootstrapWYSIHTML5: function () {
          if($('#wysihtml5-textarea').length){
            $('#wysihtml5-textarea').wysihtml5();
          }
        },

        // =========================================================================
        // SUMMERNOTE
        // =========================================================================
        summernote: function () {
          if($('#summernote-textarea').length){
            $('#summernote-textarea').summernote();
          }
        }

      };

    }();

// Call main app init
BlankonFormWysiwyg.init();
</script>
@endsection

@endsection
