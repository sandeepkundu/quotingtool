@extends('layouts.default') @section('content')

@if(isset($toast['message']))
    {{ $toast['message'] }}
@endif
<link href="{{ asset('assets/global/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet">
<link href="{{ asset('assets/global/plugins/bower_components/jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css') }}" rel="stylesheet">

<div class="header-content">
    <h2><i class="fa fa-file-pdf-o"></i> Import - Pricinging Adjustments Data<span></span></h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label"></span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="{{URL::to('/')}}/admin/dashboard">Dashboard</a>
            </li>
            <li class="active"></li>
        </ol>
    </div>
</div>
<div class="body-content animated fadeIn">
    <div class="row">
        <div class="col-md-12">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Import - Pricinging Adjustments Data </h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body no-padding">
                    <form class="form-horizontal mt-10" id="commentForm" method="post" action="{{URL::to('/')}}/Variances/price-margin-import-store" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="col-lg-8">
                                            
                                            <label for="first_name" class="col-sm-3 control-label">
                                                Select CSV file<span style="color:red;">*</span>
                                            </label>
                                            <div class="col-sm-9">
                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                                        <span class="fileinput-filename"></span>
                                                    </div>
                                                    <span class="input-group-addon btn btn-success btn-file" style="background-color: #00B1E1;border-color: #00B1E1;">
                                                        <span class="fileinput-new">
                                                            Select file
                                                        </span>
                                                        <span class="fileinput-exists">
                                                            Change
                                                        </span>
                                                        <input type="file" name="fileToUpload" accept=".csv">
                                                    </span>
                                                    <a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <input type="submit" class="btn btn-success" value="Upload File" style="background-color: #00B1E1;border-color: #00B1E1;">  
                                        
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-12">
                                     
                                           <!--  <input type="button" class="btn btn-success" value="Export" style="background-color: #00B1E1;border-color: #00B1E1;" onclick="location.href='{{URL::to('/')}}/Variances/export_data'"> -->
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </form>
                    <table id="datatable-ajax" class="table table-striped table-success">
                        <thead>
                            <tr role="row">
                                <th>Date</th>
                                <th>Valid Records</th>
                                <th>Invalid Records</th>
                                <th>Import File</th>
                                <th>Export File</th>
                            </tr>
                        </thead>

                        <tbody>

                            @if (isset($IntegrationImports) && count($IntegrationImports) > 0)
                                @foreach ($IntegrationImports as $IntegrationImport)
                                    <tr>
                                        <td>{{$IntegrationImport->date}}</td>
                                        <td>{{$IntegrationImport->records_valid}}</td>
                                        <td>{{$IntegrationImport->records_invalid}}</td>
                                        <td>
                                            <a href="{{URL::to('/')}}/import/{{$IntegrationImport->id}}">
                                                {{$IntegrationImport->fileimported}}
                                            </a>
                                            </td>
                                        <td>
                                            <a href="{{URL::to('/')}}/export/{{$IntegrationImport->id}}">
                                                {{$IntegrationImport->fileexported}}
                                            </a>
                                        </td>
                                        
                                    </tr>
                                @endforeach
                            @else
                                <?php echo '<tr><td colspan="10" style="color:red;"> No record found!!! </td></tr>'; ?>
                            @endif

                        </tbody>

                        <tfoot>
                            <tr role="row">
                                <th>Date</th>
                                <th>Valid Records</th>
                                <th>Invalid Records</th>
                                <th>Import File</th>
                                <th>Export File</th>
                            </tr>
                        </tfoot>
                    </table>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="dataTables_info" id="datatable-client-all_info" role="status" aria-live="polite">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="dataTables_paginate paging_simple_numbers" id="datatable-client-all_paginate">  
                          

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

        
@endsection
@section('js')

<script src="{{ asset('assets/global/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>
<script src="{{ asset('assets/global/plugins/bower_components/jasny-bootstrap-fileinput/js/jasny-bootstrap.fileinput.min.js')}}"></script>
@endsection