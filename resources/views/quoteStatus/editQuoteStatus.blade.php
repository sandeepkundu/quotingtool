@extends('layouts.default') @section('content')

     
 <style>
.form-control:focus{
	border: 1px solid #66afe9 !important;
}
.bg-warning{
	background-color:#00B1E1 !important;
	border:1px solid #00B1E1 !important;
}
input.no-border-right:focus, textarea.no-border-right:focus{
	border: 1px solid #66afe9 !important;
}
</style>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel rounded shadow">
					<div class="panel-heading">
						<div class="pull-left">
							<h3 class="panel-title">EDIT QUOTE STATUS</h3>
						</div>
						
						<div class="clearfix"></div>
					</div>
					<!-- /.panel-heading -->
					<div class="panel-body no-padding">

						<form class="form-horizontal mt-10" id="commentForm" method="post" action="{{URL::to('/')}}/quoteStatus/updateQuoteStatus" >
							{{ csrf_field() }}
							<input type="hidden" name="id" value="{{ $QuoteStatus->id }}">
							<div class="form-body">
								<div class="form-group">
									<div class="row">
										<div class="col-lg-12">
											<div class="col-lg-6">
												<label for="name" class="col-sm-3 control-label"> Name<span style="color:red;">*</span></label>
												<div class="col-sm-9">
													<input value="{{$QuoteStatus->name}}" class=" form-control" id="first_name" name="name" minlength="2" type="text" placeholder=" Name"> 
													@if ($errors->has('name'))
													<span class="help-block" style="color:red;">
														<strong>{{ $errors->first('name') }}</strong>
													</span>
													@endif
													&nbsp
												</div>
											</div>

											<div class="col-lg-6">
												<label for="source" class="col-sm-3 control-label">Source<span style="color:red;">*</span></label>
												<div class="col-sm-9">
													<input value="{{$QuoteStatus->source}}" class=" form-control" id="source" name="source" minlength="2" type="text" placeholder="Source"> 
													@if ($errors->has('source'))
													<span class="help-block" style="color:red;">
														<strong>{{ $errors->first('source') }}</strong>
													</span>
													@endif
													&nbsp
												</div>
											</div>
										</div>
										
									</div>
								</div>
							</div>
							<!-- /.form-group -->
							<div class="form-group"></div>
							<!-- /.form-group -->
						</div>
						<!-- /.form-body -->
						<div class="form-footer">
							<div class="col-sm-offset-5">
								<button class="btn btn-color btn-hover" type="submit">Submit</button>
								<a class="btn btn-danger" href="{{URL::to('/')}}/quoteStatus/index">Cancel</a>
							</div>
						</div>
					</form>

				</div>
			</div>
			<!-- /.panel -->
			<!--/ End horizontal form -->
		</div>
	</div>


@endsection
