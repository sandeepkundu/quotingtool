@extends('layouts.default') @section('content')

<!-- Bootstrap core CSS -->
<!-- <link href="{{ asset('css/hpdashboard/bootstrap.css') }}" rel="stylesheet"> -->
<link href="{{ asset('css/hpdashboard/style.css') }}" rel="stylesheet">
<link href="{{ asset('css/hpdashboard/responsive.css') }}" rel="stylesheet">    
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
 
@if(isset($toast['message']))

        {{ $toast['message'] }}
 @endif

<style>
    .row-one .box-1{
        width:173px;
        margin-bottom: 20px !important;
    }
    .btn-default{
        color: #fff !important;
    }
    #table_panel_head{
        font-weight: 300;
        font-size: 24px;
        text-align: center;
        text-transform: capitalize;
        color: #999999;
    }
    div{
        font-family: "Open Sans", sans-serif;
        color: #636E7B;
        -webkit-font-smoothing: antialiased;
        direction: ltr;
        line-height: 21px;
        font-size: 13px;
    }
    .table-success thead tr th {
        background-color: #00B1E1 !important;
        border-color: #63D3E9 !important;
        color: #fff;
        border-color: #63D3E9;
    }
    .new-quate-btn:hover {
        background: #426ca9;
        color: #ffffff;
    }
    #lavel1{
        height: 31px;
        padding-top: 5px;
        border-bottom: 1px lightgray solid;
    }
    #level2{
        padding-left: 25px;
        height: 31px;
        padding-top: 5px;
        border-bottom: 1px lightgray solid;
    }
    #level3{
        padding-left: 50px;
        height: 31px;
        padding-top: 5px;
        border-bottom: 1px lightgray solid;

    }
</style>
   
<!-- Start page header -->
<div id="tour-11" class="header-content">
    <h2><i class="fa fa-home"></i>Dashboard <span>dashboard & statistics</span></h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label"></span>
        <ol class="breadcrumb">
            <li class="active">Dashboard</li>
        </ol>
    </div>
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->

<div class="body-content animated fadeIn">
    <div id="tour-12" class="row" style="margin-bottom: 10px;">
        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-6">
            <div class="number-quates">
                <h3>number of quotes</h3>
                <div class="icon">
                    <span class="fa fa-chevron-circle-down"></span>
                </div>
                <div class="value-text">
                    <h4>{{count($total_Quotes)}}</h4>
                    <!--<span>Last month :124,120/135,145 </span>-->
                    <span>Till date</span>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-6">
            <div class="value-quote">
                <h3>value of quote</h3>
                <div class="icon">
                    <span class="fa fa-chevron-circle-up"></span>
                </div>
                <div class="value-text">                    
                    <?php 
                        if(!empty($total_Quotes)) {   
                            $arr[]='';              
                            foreach ($total_Quotes as  $quotes) {
                                $arr[]=$quotes->total_value;
                            }
                            ?>
                            <h4>${{array_sum($arr)}}</h4>
                            <!--<span>Last month :124,120/135,145 </span>-->
                            <span>Till date</span>
                            <?php } ?>

                </div>
            </div>
        </div>

    </div><!-- /.row -->

    <div class="two-col-layout-dashboard">
        <div class="row">
            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-6">
                <div class="recent-quoate">
                    <div class="panel panel-default">
                        <div class="panel-heading">recent quotes</div>
                        <div class="panel-body">
                            <table class="table table-striped table-success"> 
                                <thead> 
                                    <tr> 
                                        <th>#</th> 
                                        <th>User Name</th> 
                                        <!--  <th>Client Type</th>  -->
                                        <th>Total Quote</th> 
                                    </tr> 
                                </thead> 
                                <tbody> 
                                    @php
                                    $i=0;
                                    @endphp
                                    @foreach($quote as $val)
                                    @php $i++; @endphp
                                    <tr> 
                                        <th scope="row">{{$i}}</th>
                                        <td>{{@$val->Username->first_name}}</td> 
                                        <td>
                                            <!-- <form action="{{url('/')}}/QuoteClientSummary" method="post">
                                                {{csrf_field()}} 
                                                <input type="hidden" name="id" value="{{@$val->Username->id}}">
                                                <input type="submit" value="{{$val->total}} " >
                                            </form> -->
                                            <a href="{{url('/')}}/QuoteClientSummary/{{@$val->Username->id}}">{{$val->total}}</a>
                                            </td>
                                        </tr>
                                        @endforeach 
                                    </tbody> 
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-xs-12 col-sm-6">
                    <div class="client-panel">
                        <div class="panel panel-default">
                            <div class="panel-heading">clients</div>
                            <div class="panel-body" style="height: 300px; overflow-x: scroll;">

                                <table class="table table-striped table-success">
                                    <thead> 
                                        <tr> 
                                            <th>Client Name</th> 
                                            <th>Client Type</th> 
                                            <th>Action</th> 
                                        </tr> 
                                    </thead> 
                                </table>
                                @foreach ($parent_ids as $ClientUser) 
                                @if($ClientUser->level == 1)
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-6" id="lavel1">
                                    <div class="col-md-5 col-lg-5 col-xs-12 col-sm-6">
                                        {{$ClientUser->name}}
                                    </div>
                                    <div class="col-md-5 col-lg-5 col-xs-12 col-sm-6">
                                        @if ($ClientUser->client_type=='store')
                                        <span class="label label-info text-capitalize">Store</span>
                                        @else
                                        <span class="label label-warning text-capitalize">Enterprise</span>
                                        @endif
                                    </div>  
                                    <div class="col-md-2 col-lg-2 col-xs-12 col-sm-6">
                                     <a class="btn btn-round btn-color btn-hover" href="{{URL::to('/')}}/clients/editClient/{{$ClientUser->id}}" style="padding: 0px 6px">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                </div>
                            </div>
                            @elseif($ClientUser->level == 2)
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-6" id="level2" >
                                <div class="col-md-5 col-lg-5 col-xs-12 col-sm-6">
                                    {{$ClientUser->name}}
                                </div>
                                <div class="col-md-5 col-lg-5 col-xs-12 col-sm-6">
                                    @if ($ClientUser->client_type=='store')
                                    <span class="label label-info text-capitalize">Store</span>
                                    @else
                                    <span class="label label-warning text-capitalize">Enterprise</span>
                                    @endif
                                </div> 
                                <div class="col-md-2 col-lg-2 col-xs-12 col-sm-6" style="padding-left:7px;">
                                 <a class="btn btn-round btn-color btn-hover" href="{{URL::to('/')}}/clients/editClient/{{$ClientUser->id}}" style="padding: 0px 6px">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </div> 
                        </div>
                        @elseif($ClientUser->level == 3)
                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-6" id="level3" >
                            <div class="col-md-5 col-lg-5 col-xs-12 col-sm-5">
                                {{$ClientUser->name}}
                            </div>
                            <div class="col-md-5 col-lg-5 col-xs-12 col-sm-5">

                                @if ($ClientUser->client_type=='store')
                                <span class="label label-info text-capitalize">Store</span>
                                @else
                                <span class="label label-warning text-capitalize">Enterprise</span>
                                @endif
                            </div> 
                            <div class="col-md-2 col-lg-2 col-xs-12 col-sm-6" style="padding-left:4px;">
                             <a class="btn btn-round btn-color btn-hover" href="{{URL::to('/')}}/clients/editClient/{{$ClientUser->id}}" style="padding: 0px 6px">
                                <i class="fa fa-edit"></i>
                            </a>
                        </div> 
                    </div>
                    @endif
                    @endforeach

                </div>
            </div>
        </div>       
    </div> 
</div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading" id="table_panel_head">Recent Login Users</div>
            <div class="panel-body">
                <div class="tab-pane fade in active" id="tab-client-all">
                    <div id="datatable-client-all_wrapper" class="dataTables_wrapper form-inline">
                        <table id="" class="table table-striped table-success table-middle table-project-clients " role="grid" aria-describedby="datatable-client-all_info">
                            <thead>
                                <tr role="row">
                                    <th  data-class="expand" class="expand sorting_asc" tabindex="0" aria-controls="datatable-client-all" rowspan="1" colspan="1" aria-label="Client Name: activate to sort column descending" aria-sort="ascending">Name</th>
                                    <th  data-hide="phone,tablet" class="sorting" tabindex="0" aria-controls="datatable-client-all" rowspan="1" colspan="1" aria-label="Total Balance: activate to sort column ascending">Email Address</th>

                                    <th  data-hide="phone,tablet" class="sorting" tabindex="0" aria-controls="datatable-client-all" rowspan="1" colspan="1" aria-label="Total Balance: activate to sort column ascending">Last Login</th>
                                    <th  data-hide="phone,tablet" class="sorting" tabindex="0" aria-controls="datatable-client-all" rowspan="1" colspan="1" aria-label="Total Balance: activate to sort column ascending">Ip Address</th>

                                </tr>
                            </thead>

                            <tbody>
                              @foreach($users as $user)
                              <tr>
                                <td>{{$user->first_name}} {{$user->last_name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{date('j M Y h:i:s a', $user->last_login)}}</td>
                                <td>{{$user->ip_address}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr role="row">
                                <th  data-class="expand" class="expand sorting_asc" tabindex="0" aria-controls="datatable-client-all" rowspan="1" colspan="1" aria-label="Client Name: activate to sort column descending" aria-sort="ascending">Name</th>
                                <th  data-hide="phone,tablet" class="sorting" tabindex="0" aria-controls="datatable-client-all" rowspan="1" colspan="1" aria-label="Total Balance: activate to sort column ascending">Email Address</th>
                                <th  data-hide="phone,tablet" class="sorting" tabindex="0" aria-controls="datatable-client-all" rowspan="1" colspan="1" aria-label="Total Balance: activate to sort column ascending">Last Login</th>
                                <th  data-hide="phone,tablet" class="sorting" tabindex="0" aria-controls="datatable-client-all" rowspan="1" colspan="1" aria-label="Total Balance: activate to sort column ascending">Ip Address</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
@section('js')

<!--<script src="{{asset('js/hpdashboard/jquery.min.js')}}"></script>   -->
<!-- <script src="{{asset('js/hpdashboard/bootstrap.js')}}"></script> -->
@endsection
@endsection
