@extends('layouts.hpUser') 
@section('content')
<?php 
function displayQuaoteValues($value, $symbol, $rate){
	$rate = ($rate > 0 && strlen($symbol) > 0) ?  $rate : 1;
	$symbol =  (strlen($symbol) > 0) ? $symbol : '$';
	return $symbol . '' . number_format($value * $rate, 0, '.', ',');
}
?>
<link href="{{ asset('assets/global/plugins/bower_components/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset('css/QuoteSummary.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/StoreQuoteSummary.css')}}">
<div class="page-title">
    <div class="container">
        <div class="pull-left">
            <span class="page-heading-center ml7">{{@$Client->name}}</span>
        </div>
    </div>    
</div>

<section class="search-bar" style="background-color: #fff!important;padding-top: 25px !important;">
    <div class="container">

        <h3 class="title text-center">Device Specifications</h3>   
        <div class="device-spec-row row-center">
            @if(isset($type))
            <div class="card card-selected card-final">
                <div class="brand-img">
                    <img src="{{url('/')}}/product/{{$type->image}}" class="img-responsive">
                </div>
                <div class="brand-title productname" id="{{$type->id}}">{{$type->name}}</div>
            </div>
            @endif
            @if(isset($brand))
            <div class="card card-selected card-final">
                <div class="selected-brand-wrap brand-img">
                    <div class="selected-brand" style="background-image:url({{url('/')}}/product/{{$brand->image}})"></div>             
                </div>
                <div class="brand-title brandname" id="4">{{$brand->name}}</div>
                <input type="hidden" name="brand_id" value="4">
                <input type="hidden" name="brandName" value="Acer">
            </div>
            @endif
            @if(isset($processor))
            <div class="card card-selected card-final">
                <div class="brand-img">
                    {{$processor->type}}
                </div>
                <div class="brand-title" data-toggle="tooltip" title="{{$processor->name}}">{{$processor->name}}</div>
                <input type="hidden" name="proccessor" value="122">
                <input type="hidden" name="proccessorName" value="15&quot;">
            </div>
            @endif
            @if(!empty($screen))
            <div class="card card-selected card-final">
                <div class="brand-img">
                    {{$screen->type}}
                </div>
                <div class="brand-title">{{$screen->name}}</div>
                <input type="hidden" name="proccessor" value="122">
                <input type="hidden" name="proccessorName" value="15&quot;">
            </div>
            @endif
            @if(isset($dvd))
            <div class="card card-selected card-final">
                <div class="brand-img">
                    {{$dvd->type}}
                </div>
                <div class="brand-title">{{$dvd->name}}</div>
            </div>
            @endif
            @if(isset($hdd))
            <div class="card card-selected card-final">
                <div class="brand-img">
                    {{$hdd->type}}
                </div>
                <div class="brand-title">{{$hdd->name}}</div>
                <input type="hidden" name="proccessor" value="122">
                <input type="hidden" name="proccessorName" value="15&quot;">
            </div>
            @endif
            @if(isset($ram))
            <div class="card card-selected card-final">
                <div class="brand-img">
                    {{$ram->type}}
                </div>
                <div class="brand-title">{{$ram->name}}</div>
                <input type="hidden" name="proccessor" value="122">
                <input type="hidden" name="proccessorName" value="15&quot;">
            </div>
            @endif
        </div><!-- row-center -->

        @if(count($QuoteItemsDefect)>0)
        
        <h3 class="title text-center">Device Condition</h3>
        <div class="defects-selected row-center">
            @foreach($QuoteItemsDefect as $defect)
            <div class="card card-selected card-final" id="{{$defect->id}}">
                <div class="checked">
                    <div class="url_img" style="position:relative"><i class="fa fa-check"></i></div>
                </div>
                <div class="Processors">{{$defect->defect_name}}</div>
                <div class="brand-title" style="margin-bottom:3px;">
                    @if($defect->description != '')
                    <i class="fa fa-question-circle" name="{{$defect->id}}" style="margin-right: 5px; float: right;">
                    </i>
                    @endif
                </div>
            </div>
            @endforeach
        </div>
        @endif

        @if(count($services) >0 && $isServiceDefault != @$services->id)
        <h3 class="title text-center">Services</h3>
        <div class="row-center">
            <div class="card card-selected card-final">
                <div class="brand-img">
                    {{@$services->name}}
                </div>
                <div class="brand-title" style="font-size:12px"></div>
            </div>
        </div>
        @endif

        @if(@$role == 2 || (@$role == 3 && @in_array(1,$permission)) )
        
        @endif

        <div class="row">
        
            <div class="col-sm-8">
                @if(@$role == 2 || (@$role == 3 && @in_array(1,$permission)) )
                <div class="table-responsive productIdTrue Financial">
        
                    <table class="table table-striped table-bordered table-responsive">
                        <thead>
                            <tr style="background-color: #56bdf1;color:#fff">
                                <th align="center" style="font-weight: 700;font-size: 15px;">Financial Summary</th>
                                <th align="center" style="font-weight: 600;">Product Value</th>
                                <th align="center" style="font-weight: 600;">Services Fee</th>
                                <th align="center" style="font-weight: 600;">Logistics Fee</th>
                                <th align="center" style="font-weight: 600;">Net Value</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Receive from Partner</td>
                                <td align="center">{{displayQuaoteValues($unitp, $Quote['currency_symbol'], $Quote['currency_rate'] )}}</td>
                                <td align="center">{{displayQuaoteValues(@$service, $Quote['currency_symbol'], $Quote['currency_rate'] )}}</td>
                                <td align="center">{{displayQuaoteValues(@$logistic, $Quote['currency_symbol'], $Quote['currency_rate'] )}}</td>
                                <td align="center">{{displayQuaoteValues(@$total_price, $Quote['currency_symbol'], $Quote['currency_rate'] )}}</td>
                            </tr>
                            <tr>
                                <td>Pay to Customer</td>
                                <td align="center">{{displayQuaoteValues($Product_Price_customer, $Quote['currency_symbol'], $Quote['currency_rate'] )}}</td>
                                <td align="center">{{displayQuaoteValues($Service_Price_customer, $Quote['currency_symbol'], $Quote['currency_rate'] )}}</td>
                                <td align="center">{{displayQuaoteValues($Logistic_Price_customer, $Quote['currency_symbol'], $Quote['currency_rate'] )}}</td>
                                <td align="center">{{displayQuaoteValues(@$Sub_total, $Quote['currency_symbol'], $Quote['currency_rate'] )}}</td>
                            </tr>
                            <tr>
                                <td>Margin / Markup $</td>
                                <td align="center">{{displayQuaoteValues($unitp - $Product_Price_customer, $Quote['currency_symbol'], $Quote['currency_rate'] )}} </td>
                                <td align="center">{{displayQuaoteValues($Service_Price_customer- $service, $Quote['currency_symbol'], $Quote['currency_rate'] )}} </td>
                                <td align="center">{{displayQuaoteValues($Logistic_Price_customer- $logistic, $Quote['currency_symbol'], $Quote['currency_rate'] )}} </td>
                                <td align="center">{{displayQuaoteValues($total_price - $Sub_total, $Quote['currency_symbol'], $Quote['currency_rate'] )}} </td>
                            </tr>
                            <tr>
                                <td>Margin / Markup  %</td>
                                <td align="center">{{number_format($product_percent,0)}}%</td>
                                <td align="center">{{number_format($service_percent,0)}}%</td>
                                <td align="center">{{number_format($logistic_percent,0)}}%</td>
                                <td align="center">{{number_format($total_percent,0)}}%</td>
                            </tr>
                        </tbody>    
                    </table>
        
                </div>
                @endif
            </div>
            <div class="col-sm-4">
                <h3>Your device price <b id="getTotal">{{displayQuaoteValues($QuoteItem->total_price, $Quote->currency_symbol, $Quote->currency_rate )}}</b></h3>
        
                <input type="text" class="form-control" style="height:40px;margin-bottom:15px" readonly="" value="{{$QuoteItem->quote_name->name!=null? $QuoteItem->quote_name->name:'N/A'}}" placeholder="Reference">
        
                @if($Quote->Status->id == 1)
                <button type="submit" class="AcceptButton btn btn-cta">{{$Quote->Status->name}}</button>
                @elseif ($Quote->Status->id == 2)
                <button type="submit" class="AcceptButton btn btn-cta">Quote Accepted</button>
                @elseif ($Quote->Status->id == 3)
                <!--<button type="submit" class="AcceptButton btn">Upload Photos / Sr.No</button>-->
                <button type="submit" class="Upload btn btn-cta btn-lg btn-block">Upload Photos</button>
                <button type="submit" class="btn btn-danger btn-block cancel">Cancel</button>
        
                @elseif ($Quote->Status->id == 4)
                @if(@$role == 2 || (@$role == 3 && @in_array(3,$permission)) )
                <button type="submit" class="AcceptButton22 btn">Collection Request</button>
                @endif
                <button type="submit" class="btn cancel" style="width:100%">Cancel</button>
                @elseif ($Quote->Status->id == 5)
                <button type="submit" class="AcceptButton btn">Awaited Collection</button>
                @elseif ($Quote->Status->id == 6)
                <button type="submit" class="AcceptButton btn">Completed</button>
                @elseif ($Quote->Status->id == 7)
                <button type="submit" class="canceled btn">Canceled</button>
                @endif
        
        

                @if($status > 3)

                <h5>Serial Number: </h5> 
                <h6><?php echo $QuoteItem['serial_number']?></h6> 

                <h5>Product Images:</h5>
                @if($image1)

                    <?php  $image1url=URL('/').$image1; ?>
                    <a href="{{$image1url}}" target="_blank" >
                    <img src="<?php echo $image1url?>"  height="100%" width="100%"/>
                    </a>

                @endif
                @if($image2)

                <?php $image1url=URL('/').$image2; ?>
                <a href="{{$image1url}}" target="_blank" >
                <img src="<?php echo $image1url?>"  height="100%" width="100%"/>
                </a>

                @endif
                    
                @if($image3)

                <?php $image1url=URL('/').$image3; ?>
                 <a href="{{$image1url}}" target="_blank" >
                <img src="<?php echo $image1url?>"  height="100%" width="100%"/>
                </a>

                @endif

                @endif                
            </div>
        </div>
        
    </div>
</section>

<input type="hidden" name="{{$Quote->status}}" id="{{$Quote->id}}" class="status">
<input type="hidden" name="siteurl" id="siteurl" value="{{URL::to('/')}}">
<div id="loadingDiv" style="display:none;">
    <div>
        <div class="loader"></div>
    </div>
</div>
<!--==============================Modal Popup=======================-->
<div class="modal modal-success fade in" id="modal-bootstrap-tour" style="display: none;">
    <div class="modal-dialog" role="document" style="margin: 150px auto;">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #56bdf1;">
                <button type="button" id="close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Upload Pictures</h4>
            </div>
            <div class="modal-body" style="height: auto;overflow: auto;">
                <div class="row" style="text-align: center; margin-bottom: 20px;">
                    <a href="javascript:void(0)" class="upload_sub_btn btn" style='' name="" id="uploadimageByWebcam">  
                       <i class="fas fa-video"></i> Upload By Webcam
                   </a>
               </div>
               <span style="color:red;" class="errormsgforimageupload"></span>
               <form action="{{URL('/')}}/UploadImage" method="post" enctype="multipart/form-data" onsubmit=" return imageuploadsubmit()">
                {{ csrf_field() }}
                <input type="hidden" name="quote_id" id="hiddenQuoteId" value="{{$Quote->id}}">
                <table style="width: 100%" class="table">
                    <tr>
                        <td>
                            <div class="uploader" onclick="$('#filePhoto').click()">

                                <img src="{{asset('images/upload1.gif')}}" title="Drag an drop image here" alt="Drag an drop image here" width="100%" height="100%" />
                                <input type="file" name="userprofile_picture"  id="filePhoto" />
                                <input type="hidden" name="image" id="scrId" />
                            </div>
                        </td>
                        
                        <td>
                            <div class="uploader2" onclick="$('#filePhoto2').click()">

                                <img src="{{asset('images/upload1.gif')}}" title="Drag an drop image here" alt="Drag an drop image here"/>
                                <input type="file" name="userprofile_picture2"  id="filePhoto2" />
                                <input type="hidden" name="image2" id="scrId2" />
                            </div>
                        </td>
                        
                        <td>
                            <div class="uploader3" onclick="$('#filePhoto3').click()">

                               <img src="{{asset('images/upload1.gif')}}" alt="drag an drop image here" />
                               <input type="file" name="userprofile_picture3"  id="filePhoto3" />
                               <input type="hidden" name="image3" id="scrId3" />
                           </div>
                       </td>
                   </tr>
                   <tr>
                    <td colspan="3"> 
                        <input type="text" id="serial_numberid" name="serial_number" class="form-control" placeholder="Please enter serial number">
                        <span style="color:red" class="megerrorserialno"></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align:center;">
                        <a href="javascript:void(0)" style='width:49%;display: none' name="{{$Quote->Status->id}}" id="{{$Quote->id}}" class=" btn changeStatus skip">   
                            SKIP
                        </a>
                        <button type="submit" class='upload_sub_btn btn'>SUBMIT</button>
                    </td>
                </tr>    
            </table>
        </form>
    </div>
</div>
</div>
</div>
<!--==============================Modal Popup=======================--> 

<!--==============================Modal Popup=======================-->
<div class="modal modal-success fade in" onload="init();" id="modal-bootstrap-uploadImageByWebcam" style="display: none;">
    <div class="modal-dialog" role="document" style="margin: 150px auto; margin-top: 0px;">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #56bdf1;">
                <button type="button" id="close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Upload Pictures</h4>
            </div>
            <div class="modal-body" style="height: auto;overflow: auto;">
                <div class="camera" style="text-align: center;">
                    <div class="col-md-12">
                        <div class="col-md-8 col-sm-8 col-lg-8 col-md-offset-2">

                         <video onclick="snapshot(this);" style="width: 100%;" id="video" controls autoplay></video>
                        </div>
                    </div>
                    <div class="col-md-12" style="padding:0px;">
                        <div class="col-sm-4 col-md-4 col-lg-4" style="padding-left:2px;padding-right: 0px;">
                            <canvas  id="myCanvas" style="width: 100%;" width="100" height="75"></canvas>  
                            <button id="startbutton" style="width: 100%;" onclick="snapshot();">Take photo 1</button>
                        </div>
                    
                        <div class="col-sm-4 col-md-4 col-lg-4" style="padding-left: 2px;padding-right: 0px;">
                            <canvas  id="myCanvas2" style="width: 100%;" width="100" height="75"></canvas> 
                            <button id="startbutton2" style="width: 100%;" onclick="snapshot2();">Take photo 2</button> 
                        </div>

                        <div class="col-sm-4 col-md-4 col-lg-4" style="padding-left: 2px;padding-right: 0px;">
                            <canvas id="myCanvas3" style="width: 100%;" width="100" height="75"></canvas>
                            <button id="startbutton3" onclick="snapshot3();" style="width: 100%;">Take photo 3</button>
                        </div>
                    </div>
                </div>

            <div class="row" style="text-align: right;">
             <div class="col-sm-12 col-md-12 col-lg-12">
                 <span style="color:red;" class="errormsgforimageuploadwebcam"></span>
                 <form action="{{URL('/')}}/uploadimageByWebcam" onsubmit=" return save_img_webcam()"  method="post" class="form">
                    {{ csrf_field() }}
                    <input type="hidden" name="image1" class="form-control image1">
                    <input type="hidden" name="image2" class="form-control image2">
                    <input type="hidden" name="image3" class="form-control image3">
                    <br>
                    <input type="type" name="serial_number" class="form-control serial_numberid" placeholder="Please enter serial number">
                    <span style="color:red" class="megerrorserialnowebcam"></span>
                    <input type="hidden" name="quote_id" class="" value="" id="quoteId">

                    <button type="submit" class="btn grey save-img" style="margin-top: 15px;"> Save Image </button>
                </form>
            </div>

        </div>

    </div>
</div>
</div>
</div>
<!--==============================Modal Popup=======================-->

@endsection
@section('js')

    <script type="text/javascript" src="{{asset('js/StoreQuoteSummary.js')}}"></script>
    <script src="{{ asset('js/canvas.js')}}"></script>
    <script src="{{ asset('js/QuoteSummary.js')}}"></script>
@endsection