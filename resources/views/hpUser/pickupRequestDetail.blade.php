@extends('layouts.hpUser') 
@section('content')

<style type="text/css">
	
	@media print {
	  body * {
	    visibility: hidden;
	  }
	  #section-to-print, #section-to-print * {
	    visibility: visible;
	  }
	  #section-to-print {
	    position: absolute;
	    left: 0;
	    top: 0;
	  }
	}
	#print{
		cursor: pointer;
		padding: 5px;
		font-size: 28px;
	}

</style>


<div class="mainDiv">
	<section class="device-section">
		<div class="container">
			<div class="row">			
				<div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
					<h2 class="title" style="float: left;">Pickup Request Detail</h2>
					<span style="padding-top: 15px;" class="pull-right" id="print" onclick="myFunction();"><i class="fa fa-print" aria-hidden="true" data-toggle="tooltip" title="Print"></i></span>
				</div>
			</div>
			<div class="row"  id="section-to-print">			
				<div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" style="padding-bottom: 20px; padding-top:50px;">
					<table border="0" cellpadding="10">
						<tr>
							<td>User Name : </td>
							<td>{{ @$quotes[0]->Username->first_name }} {{ @$quotes[0]->Username->last_name }}</td>
						</tr>
						<tr>
							<td>System Reference : </td>
							<td>{{@$quotes[0]->Clientname->name}}</td>
						</tr>
						<tr>
							<td>Customer Name : </td>
							<td>{{@$quotes[0]->Clientname->name}}</td>
						</tr>
					</table>
				</div>
				<div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
					<?php $i=1; ?>
					<table class="table" style="padding-bottom: 50px;"> 
						<tr>
							<th>S.No.</th>
							<th>Quote Name</th>
							<th>Client Name</th>
							<th>Product</th>
							<th>Brand</th>
							<th>Processor</th>
							<th>Screen</th>
							<th>RAM</th>
							<th>DVD</th>
							<th>HDD</th>
							<th>Services</th>
							<th>Date Generated</th>
							<th>Quantity</th>
							<th>Value</th>
						</tr>
						@if(count($quotes) > 0)
						@foreach($quotes as $quote)
							<?php 
								$a[] = @$quote->total_value;
								$Quantity[] = (isset($quote->QuoteItem->quantity))?$quote->QuoteItem->quantity : 0;
							 ?>

							<tr>
								<td>{{ @$i++ }}</td>
								<td>{{ @$quote->name }}</td>
								<td>{{ @$quote->Clientname->name }}</td>
								<td>{{ isset($quote->QuoteItem->type_name)?$quote->QuoteItem->type_name:'N/A' }}</td>
								<td>{{ isset($quote->QuoteItem->brand_name)?$quote->QuoteItem->brand_name:'N/A' }}</td>
								<td>{{ isset($quote->QuoteItem->processor_name)?$quote->QuoteItem->processor_name:'N/A' }}</td>
								<td>{{ isset($quote->QuoteItem->screen_name)?$quote->QuoteItem->screen_name:'N/A' }}</td>
								<td>{{ isset($quote->QuoteItem->ram_name)?$quote->QuoteItem->ram_name:'N/A' }}</td>
								<td>{{ isset($quote->QuoteItem->dvd_name)?$quote->QuoteItem->dvd_name:'N/A' }}</td>
								<td>{{ isset($quote->QuoteItem->hdd_name)?$quote->QuoteItem->hdd_name:'N/A' }}</td>
								<td>{{ isset($quote->QuoteItem->services_name)?$quote->QuoteItem->services_name:'N/A' }}</td>
								<td>{{ isset($quote->QuoteItem->date_created)?$quote->QuoteItem->date_created:'N/A' }}</td>
								<td>{{ isset($quote->QuoteItem->quantity)? $quote->QuoteItem->quantity :'N/A' }}</td>
								<td>{{$quote->QuoteItem->total_price}}</td>
							</tr>
						@endforeach


						<tr style="border-bottom: 1px;">
							<th colspan="12" style="text-align: right;"> Total:</th>
							<th> {{array_sum($Quantity)}}</th>
							<th align="right">${{array_sum($a)}}</th>
						</tr>
						@endif
					</table>
				</div>
			</div>
		</div> 
	</section>
</div>

@section('js')
	<script>
		function myFunction() {
		    window.print();
		}
	</script>
	<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>
@endsection
@endsection