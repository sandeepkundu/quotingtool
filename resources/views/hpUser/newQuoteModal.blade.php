
<!--==============================Modal Popup New Quote=======================-->
<div class="modal modal-success fade in" id="modal-bootstrap-tour" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-dialog" role="document" style="margin: 150px auto;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Select Client</h4>
            </div>
            <div class="modal-body" style="height:100%;">
                <form action="{{URL('/')}}/client" method="get" class="clientform">
                    {{csrf_field()}}
                    <select name="client_name" id="clientname" class="form-control">
                        @if(count($ClientUsers) > 0)
                        @foreach ($ClientUsers as $ClientUser) 
                        @if($ClientUser->clientss != null)
                        <option value="{{$ClientUser->clientss->id}}">
                            {{$ClientUser->clientss->name}}
                        </option>
                        @endif
                        @endforeach
                        @endif
                    </select>
                </form>
                <a href="" class="btn" style="height:50px;padding-top: 15px;"></a>
                <div class="pull-right btn btn-default clientGo" style="margin-top:11px;"> Go</div>
            </div>
        </div>
    </div>
</div>
<!--==============================Modal Popup New Quote=======================-->