@extends('layouts.hpUser') 
@section('content')
<?php 
function displayQuaoteValues($value, $symbol, $rate){
	$rate = ($rate > 0 && strlen($symbol) > 0) ?  $rate : 1;
	$symbol =  (strlen($symbol) > 0) ? $symbol : '$';
	return $symbol . '' . number_format($value * $rate, 0, '.', ',');
}
?>

@php     
use App\Http\Controllers\HpUsersController;
@$Theme = HpUsersController::Theme_Cookie(); 
$cssfile = asset('css/stepsStore.css');
if(isset($Theme)){
	if(!empty($Theme->theme_folder)) {
		$folder = public_path() . '/' . @$Theme->theme_folder . '/stepsStore.css';
		if(file_exists($folder)){
			$cssfile = asset('/'). @$Theme->theme_folder . '/stepsStore.css';
		}
	}
}
@endphp
<link href="{{ $cssfile }}" rel="stylesheet">
<style>
body { background:#fff; }
</style>

<div class="page-title">
    <div class="container">
        <div class="pull-left">
            <span class="page-heading-center">
                @if(!empty($client))
                {{$client->name}}
                @endif
            </span>
        </div>
    </div>    
</div>

<section class="search-bar" style="padding: 0px;">
    <div class="container two-col-layout-dashboard">
        
        <h2 class="text-center">Quote List</h2>

        <div class="clearfix" style="margin-bottom:15px">
            <div class="dataTables_length pull-left" id="datatable-client-all_length">
                <label>
					<select name="datatable-client-all_length" class="form-control input-sm" id="paginationPerPage" style="display: inline-block;">
                        <?php
                        $l = 30;
                        $total = $Quotes->total();
                        $loop = intval($total / $l);

                        if (($total % $l) != 0) {
                            $loop = $loop + 1;
                        }
                        ?>	
                        @for($i=1; $i<=$loop;$i++)
                        <option value="{{$i*$l}}" {{ (($i*$l)==$lim)? 'selected' : '' }}>{{$i*$l}}</option>
                        @endfor

                    </select>
                    Records per page
                </label>
            </div>
			<form action="{{URL::to('/')}}/quote/history_list/{{$id}}" method="get" class="pull-right navbar-form">
			    
				<div class="input-group">
				<select name="status" id="status" class="form-control" onchange="this.form.submit()">
				<option value="">Filter by Status</option>
				<option value="1" {{isset($status) && $status == '1' ?'selected=""' :''}}>In Progress</option>
				<option value="2" {{isset($status) && $status == '2' ?'selected=""' :''}}>Accepted</option>
				<option value="3" {{isset($status) && $status == '3' ?'selected=""' :''}}>Image Upload</option>
				<option value="4" {{isset($status) && $status == '4' ?'selected=""' :''}}>Awaiting Collection</option>
				<option value="6" {{isset($status) && $status == '6' ?'selected=""' :''}}>Completed</option>
				<option value="7" {{isset($status) && $status == '7' ?'selected=""' :''}}>Canceled</option>
				</select>
				</div>  
			    
			    <div class="input-group">
                    <input type="text" name="search" class="form-control" placeholder="Search by name" value="{{isset($txt) ? $txt :''}}">
                    <span class="input-group-btn">
                    <button class="btn btn-cta" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </span>
                </div><!-- /input-group -->
                
			</form>
		</div><!-- /.clearfix -->
			

        <div class="table-responsive">
            <table class="table table-quotelist table-striped table-condensed table-bordered">
                    <thead>
                        <tr>
                            <th>Quote ID</th>
                            <th>Reference</th>
                            <th>Client Name</th>
                            <th>User</th>
                            <th>Created Date</th>
                            <th>Value</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $i=$pages;
                        @endphp
                        @if(count($Quotes)>0)
                        @foreach($Quotes as $Quote)

                        @php
                        $i++;
                        @endphp
                        <tr>
                        <td>{{$Quote->id}}</td>
                        <td><a href="{{url('/')}}/quote/history/{{$Quote->id}}">
                            @if(isset($Quote->name))
                            {{$Quote->name}}
                            @else
                            Name Not Available
                            @endif
                        </a>
                        </td>
                        <td>{{$Quote->Clientname->name}}</td>
                        <td>{{$Quote->Username->first_name}}</td>
                        <td>
                        @php
                        $date = date('d-m-Y', strtotime($Quote->date_created))
                        @endphp
                        {{$date}}
                        </td>
                        <td align="right">{{displayQuaoteValues($Quote->total_value, $Quote['currency_symbol'], $Quote['currency_rate'] )}} </td>
                        <td>{{$Quote->Status->name}}</td>

                    </tr>
                    @endforeach
                    @else
                    <tr><td colspan="7"><div class="alert alert-danger">No records found</div></td></tr>
                    @endif

                </tbody>
			</table>
		</div>

		<div class="row" style="margin-bottom:50px">
            <div class="col-xs-6">
                <div class="dataTables_info" id="datatable-client-all_info" role="status" aria-live="polite">
                    @if($Quotes->total()!=0)
                    Showing {{ $Quotes->firstItem() }} to {{ $Quotes->lastItem() }} of {{ $Quotes->total() }}
                    @endif
                </div>
            </div>
            <div class="col-xs-6" style="text-align: right;">
                <div class="dataTables_paginate paging_simple_numbers" id="datatable-client-all_paginate">
                    {{ $Quotes->links() }}
                </div>
            </div>
        </div>     
        
    </div><!-- /.container -->   
</section>

<form action="{{URL('/')}}/quote/step" id="Quote_Edit_Form" method="post">
    {{csrf_field()}}
    <input type='hidden' id="Quote_Item_Id" name="Quote_Item_Id" value=''>
    <input type='hidden' id='Quote_Id' name="Quote_Id" value=''>
    <input type='hidden' id='client_id' name="client_id" value='{{$client->id}}'>
</form>


<form action="{{URL::to('/')}}/quote/history_list/{{$id}}" method="get" class="navbar-form" id="formPerPage">
    <input type="hidden" value="" name="perPage" id="perPage">
</form>


<div id="loadingDiv" style="display:none;">
    <div>
        <div class="loader"></div>
    </div>
</div>

<input type="hidden" name="siteurl" id="siteurl" value="{{URL::to('/')}}">
@section('js')
<script>
$(document).ready(function () {
    /*==========For pagination==============*/
    $('#paginationPerPage').change(function () {
        $a = $(this).val();
        $('#perPage').val($a);
        $('#formPerPage').submit();
    });
    $('.pagination>li>a').click(function () {
        var a = $(this).attr('href');
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1);
        // alert(hashes);
        var dataArray = '';
        if (hashes.indexOf("page=") >= 0) {
            var dataArray = hashes.slice(7);
            $(this).attr('href', a + '&' + dataArray);
        } else {
            $(this).attr('href', a + '&' + hashes);
        }
    });
});
</script>
@endsection

@endsection
