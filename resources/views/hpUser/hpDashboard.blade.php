@extends('layouts.hpUser')

@section('content')

@php  
use App\Http\Controllers\HpUsersController;
@$Theme = HpUsersController::Theme_Cookie(); 
$ccFilename = 'dashboard.css';
$cssfile = asset('css/'.$ccFilename);
if(isset($Theme)){
	if(!empty($Theme->theme_folder)) {
		if(file_exists(public_path() . '/' . @$Theme->theme_folder . '/' . $ccFilename)){
			$cssfile = asset('/'). @$Theme->theme_folder . '/' . $ccFilename;
		}
	}
}
@endphp

@section('css')
<link href="{{ $cssfile }}" rel="stylesheet">
@endsection

@if(isset($toast['message']))
{{ $toast['message'] }}
@endif
<section class="dashboard">
    <div class="container">
    <div class="row row-panels">
	@if(!empty($quotevalues) && count($quotevalues) > 0)		
	@foreach($quotevalues as $qv)
		@php $sty = ($qv['CurrentMonth'] < $qv['LastMonth']) ? 'number-quates' : 'value-quote'; @endphp 
        <div class="col-sm-6 col-md-4">
		<div class="panel panel-default">

        <div class="panel-heading"><h3 class="panel-title">{{$qv['status']}} Quotes</h3></div>        
		<div class="panel-body {{$sty}}">
			<div class="icon">
			@if($qv['CurrentMonth'] < $qv['LastMonth'])
			<span class="fa fa-chevron-circle-down"></span>
			@else
			<span class="fa fa-chevron-circle-up"></span>
			@endif
			</div>
			<div class="value-text">
			<h4>${{number_format($qv['CurrentMonth'], 0, '.', ',')}}</h4>
			<span>Previous 30 days: ${{number_format($qv['LastMonth'], 0, '.', ',')}}</span>
			</div>
        </div>
		</div>
        </div>
	@endforeach
	@endif
	</div> 
    </div> <!-- /container -->
    
    <div class="cont-theme">
        <div class="container two-col-layout-dashboard">
            <div class="row" style="margin-bottom:20px">
                <div class="col-sm-6">
                    <div class="recent-quoate">
                        <div class="panel panel-default">
                            <div class="panel-heading">Recent Quotes</div>
                            <div class="panel-body">
                                <table class="table"> 
                                    <thead> 
                                        <tr> 
                                            <th>#</th> 
                                            <th>Client Name</th> 
                                            <th>Client Type</th> 
                                            <th>Total Quote</th> 
                                        </tr> 
                                    </thead> 
                                    <tbody> 
                                        @php
                                        $i = 1;
                                        @endphp
                                        @foreach ($quote as $val) 
    
                                            
                                            <tr> 
                                                <th scope="row">{{$i++}}</th>
                                                <td>{{@$val->Clientname->name}}</td> 
                                                <td>
                                                    <?php
                                                    if (@$val->Clientname->client_type == 'store') {
                                                        echo '<span>Store</span>';
                                                    } else {
                                                        echo '<span>Enterprise</span>';
                                                    }
                                                    ?>
                                                </td> 
                                                <td>
                                                    @if(@$val->Clientname->client_type=='store')
                                                     <!-- <span>{{@$val->total}}</span> -->
                                                    <a href="{{url('/')}}/quote/history_list/{{@$val->client_id}}" style="font-weight: 600;">{{@$val->total}}</a>
                                                    @else
                                                    <!-- <span>{{@$val->total}}</span> -->
                                                    <a href="{{url('/')}}/quotes/history_list/{{@$val->client_id}}" style="font-weight: 600;">{{@$val->total}}</a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody> 
                                </table>
                            </div>
                        </div>
                    </div>
    <!-- ======================================Collection Request ======================================= -->
                @if(@$role_id == 2 || (@$role_id == 3 && in_array(3,@$permission)) )
                    <div class="recent-quoate">
                        <div class="panel panel-default">
                            <div class="panel-heading clearfix" style="line-height:1.82857143">Requiring Collection
                                <a href="{{url('/')}}/collection-details" class="btn btn-cta btn-sm btn-colreq">Collection History</a>
                            </div>
                            <div class="panel-body" style="height: 300px; overflow-y:auto;">
                                <table class="table"> 
                                    <thead> 
                                        <tr> 
                                            <th>#</th>
                                            <th>Client Name</th> 
                                            <th>Total</th> 
                                        
                                        </tr> 
                                    </thead> 
                                    <tbody> 
                                       @if(isset($Quotes) && count($Quotes) > 0)
                                       @php $i =1; @endphp
                                            @foreach ($Quotes as $val) 
                                                <tr> 
                                                    <th>{{$i++}}</th>
    
                                                    <td>
                                                        @if(!empty($val->Clientname->name))
                                                            {{$val->Clientname->name}}
                                                         @else
                                                            N/A
                                                        @endif
                                                        
                                                    </td> 
                                                    <td>
                                                        @if(!empty($val->total))
                                                        
                                                            
    
                        <a href="{{url('/')}}/collection_history/{{$val->Clientname->id}}" style="font-weight: 600;" class="coll_sub_btn">
                                                       {{@$val->total}}
                                                     </a>
                                                            
                                                        @else
                                                            N/A
                                                        @endif
                                                    </td>
                                                     
                                                    <!--  
                                                    <td>
                                                    @if(!empty($val->name))
                                                      <a href="{{url('/')}}/hpUser/collection_history/{{$val->Clientname->id}}" style="font-weight: 600;" class="coll_sub_btn">
                                                        {{$val->name}}
                                                     </a>
                                                   
                                                    @else
                                                        N/A
                                                    @endif    
                                                    </td>  -->
                                                    <!-- <td>
                                                        @if(!empty($val->status))
                                                                Collection Request  
                                                            @else
                                                                N/A
                                                        @endif    
                                                    </td>  -->
                                                </tr>
                                           @endforeach
                                           @else
                                           <tr><td colspan="3"><div class="alert alert-danger">No records found</div></td></tr>
                                       @endif
    
                                    </tbody> 
                                </table>
                            </div>
                        </div>
                         
                    </div>
                @endif
     <!-- ================================= End Collection Request ================================== -->
                </div>
                <div class="col-md-6 col-lg-6 col-xs-12 col-sm-6">
                   @if(count(@$ClientUsers) > 1)
                    <div class="client-panel">
                        <div class="panel panel-default">
                            <div class="panel-heading">Clients</div>
                            <div class="panel-body">
                                <table class="table table-condensed">
                                    <tbody>
                                        @if(count(@$ClientUsers) > 1)
                                        @foreach (@$ClientUsers as $ClientUser) 
										<tr>
										<td colspan="2"> {{$ClientUser->clientss->name}}</td>
										<td>
						<form action="{{URL('/')}}/store/client" method="post" class="clientform">
										{{csrf_field()}}
										<input type='hidden' value="{{@$ClientUser->clientss->id}}" name="client_name"/>
										<button class="btn btn-cta btn-sm pull-right">Quote</button>
										</form>
										</td>
										</tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    @endif
                    <!--======================================User Listing=================================-->
                   @if($RoleUser->role_id==2)
                    <div class="client-panel">
                        <div class="panel panel-default">
                            <div class="panel-heading clearfix" style="line-height: 1.82857143;">
                                Users
                                <a  href="{{URL::to('/')}}/add_user" class="btn btn-cta btn-sm pull-right">
                                    Add User
                                </a>
    
                            </div>
                            <div class="panel-body">
                                <table class="table table-bordered table-condensed">
                                    <thead>
                                        <tr>
                                            <th>Full Name</th>
                                            <th>Email</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         @if(isset($users) && count($users) > 0)
                                        @foreach ($users as $User) 
                                        <tr>
                                            <td>{{$User->first_name}} {{$User->last_name}}</td>
                                            <td>{{$User->email}}</td>
                                             <td>
                             <a  href="{{URL::to('/')}}/edit_user/{{$User->id}}" class="btn btn-cta btn-sm">
                                                 <span class="glyphicon glyphicon-pencil" style="cursor: pointer;"></span>
                                             </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                                @if(@count($users)>0)
                                <div class="row">
                                    <div class="col-sm-6">
                                        Showing {{ @$users->firstItem() }} to {{ @$users->lastItem() }} of {{ @$users->total() }}
                                    </div>
                                    <div class="col-sm-6" style="margin: -30px 0px; text-align: right;">
                                        {{ @$users->links() }}
                                    </div>
                                </div>
                                 @endif
                            </div>
                        </div>
                    </div>
                    @endif
                    <!--====================================End User Listing===============================-->
    
                    <div class="profile-panel">
                        <div class="panel panel-default">
                            <div class="panel-heading">Profile</div>
                            <div class="panel-body">
                                <table class="table">
                                    <tbody><tr>
                                            <td><label>Email:</label></td>
                                            <td>{{$user->email}}</td>
                                        </tr>
                                        <tr>
                                            <td><label>Last Login:</label></td>
                                            <td>{{date('d-m-Y H:i:s', $user->last_login)}}</td>
    
                                        </tr> 
                                        <!-- <tr>
                                                <td><label>Device Logged In:</label></td>
                                                <td>xxx-xxxx</td>
                                        </tr> -->
                                        <tr>
                         <td colspan="2">
                     <a  href="{{URL::to('/')}}/edit_profile" class="btn btn-cta btn-sm pull-right">Edit</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>	
            </div>
        </div><!-- /container -->
    </div>
</section>
<!--==============================Modal Popup Pickup Request=======================-->
<div class="modal modal-success fade in" id="Pickup-Request_Modal" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-dialog" role="document" style="margin: 150px auto;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Select Client</h4>
            </div>
            <div class="modal-body" style="height:auto;">
                <form action="{{URL('/')}}/pickupRequest" method="post" class="pickupForm">
                    {{csrf_field()}}
                    <select name="client_names" id="clientnames" class="form-control">
                        @if(count($Quotes) > 0)
                            @foreach ($Quotes as $val) 
                            @if($val->Clientname->client_type == 'store')
                                <option value="{{$val->Clientname->id}}">
                                  {{$val->Clientname->name}} ({{$val->total}})
                                </option>
                            @endif
                            @endforeach
                        @endif
                    </select>
                </form>
                <a href="" class="btn" style="height:50px;padding-top: 15px;"></a>
                <div class="pull-right btn btn-default pickup_btn" style="margin-top:11px;"> Go</div>
            </div>
        </div>
    </div>
</div>
<!--==============================Modal Popup Pickup Request=======================-->
{{csrf_field()}}

@section('js')
<script type="text/javascript">                   
    function clientsLists() {
        $('#Pickup-Request_Modal').modal('show');
    }

    $(document).ready(function () {
        $('.pickup_btn').click(function () {
            var clients = $('#clientnames').val();

            if (clients != '') {
                $('.pickupForm').submit();
            }
        });
        $('.coll_sub_btn').click(function(){
            $('#collection_form').submit();
        })
    });
</script>
@endsection

</body>
@endsection

