@extends('layouts.hpUser') 

@section('content')
@php  
use App\Http\Controllers\HpUsersController;
@$Theme = HpUsersController::Theme_Cookie(); 
$ccFilename = 'stepsStore.css';
$cssfile = asset('css/'.$ccFilename);
if(isset($Theme)){
	if(!empty($Theme->theme_folder)) {
		if(file_exists(public_path() . '/' . @$Theme->theme_folder . '/' . $ccFilename)){
			$cssfile = asset('/'). @$Theme->theme_folder . '/' . $ccFilename;
		}
	}
}
@endphp
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="{{asset('bootstrap-select.min.css')}}" rel="stylesheet">
<link href="{{ $cssfile }}" rel="stylesheet">
<div class="page-title">
    <div class="container">
        <div class="pull-left">
        <span class="page-heading-center ml7" style="text-align: left !important;"> {{@$clinetname->name}}</span>
        </div>
        <input type="hidden" id="clientID" name="clientid" value='{{@$clinetname->id}}'>
        <input type="hidden" id="margin" name="margin" value='{{@$clinetname->margin_oem_product + @$clinetname->margin_oem_product + @$clinetname->margin_oem_product}}'>
        <input type="hidden" id="quote_item_id" name="quote_item_id" value='{{@$QuoteItemDetail->id}}'>
    </div>
</div>

<div class="mainDiv">
    <section class="search-bar" style="background-color:#d2dee1;">
        <div class="container">
            <div class="row">			
                <div class="col-md-8 col-sm-6 col-sm-offset-3 col-md-offset-2">
                    <p class="text-center" style="font-size: 24px; color: #fff;">Enter model number or serial number:</p>
                    <div style="position:relative">
                        <div class="close-search"><i class="fa fa-close"></i></div>
                        <input type="search" name="modal" id="tags" class="search-input" placeholder=" Eg: HP Probook 450 G4" style="width:100%;margin-left:0" value="{{$modal}}">
                        <ul class="list-group search-ul pup" style="width: 100% !important; position: absolute;z-index:3;margin-top: 3px;border: none;border-radius: 0px;">
                    </ul>
                    </div>
                    <p class="text-center" style="margin-top:10px;color:#fff">If model search is unsuccessful please enter model specifications below</p>
                    <input type="hidden" name="client_id" value="{{$client_id}}">

                    <br><span style="color: red;">
                        @if(Session::has('error'))
                        {{ Session::get('error') }}
                        @endif
                    </span>

                </div>
            </div>
        </div> 
    </section>
    <section class="device-section">
        <div class="container">
            <h2 class="title text-center">Select Product Type</h2>
            @if(count($products) > 7)
            <div class="row-center row-left">
            @else
            <div class="row-center">
            @endif 
                @foreach($products as $product)
                <div class="card {{@($product->id == $QuoteItemDetail->type_id) ? 'selected':''}}" onclick="step01({{ $product->id }})">
                    <div class="brand-img">
                        <img class="p_img" src="{{ URL::to('product/'.$product->image) }}"/>
                    </div>
                    <div class="brand-title">{{$product->name}}</div>
                </div>
                @endforeach
            </div>
        
        </div> 
    </section>
</div>
<!--==============================Modal Popup=======================-->
<div class="modal modal-success fade in" id="modal-bootstrap-tour" style="display: none;">
    <div class="modal-dialog" role="document" style="margin: 150px auto;">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #56bdf1;">
                <button type="button" onclick="window.reload();" id="close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title">Warning</h4>
            </div>
            <div class="modal-body" style="height: auto;overflow: auto;">
                <h4 style="color: #00B1E1;">We are not able to purchase this device based on the speficied configuraion you have entered !!</h4>
            </div>
        </div>
    </div>
</div>
<!--==============================Modal Popup=======================--> 
<input type="hidden" name="siteurl" id="siteurl" value="{{URL::to('/')}}">
<div id="loadingDiv" style="display:none;">
    <div>
        <div class="loader"></div>
    </div>
</div>
{{csrf_field()}}
@endsection

@section('js')
<script src="{{asset('bootstrap-select.min.js')}}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('js/stepsStore.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
    if($.cookie('clientdata')){
        var abc =  $.cookie('clientdata');
        var obj = JSON.parse(abc);
        var pro1 = obj.type_id;
        var pro2 = obj.brand_id;
		var model = obj.model_id;
		var pro9 = 0;
		var laststep = obj.EditlastStep;
		var pro3 = obj.processor_id;
		var pupid = obj.pup_id;
		var pupsn = obj.pup_sn;

        if(obj.SearchType == 'HDD' || obj.SearchType == 'DVD' || obj.SearchType == 'RAM' || obj.SearchType == 'Screen Size'){
            var pro4 = 0;
            var pro5 = 0;
            var pro6 = 0;
            var pro7 = 0;
            var pro8 = 0;	
            setTimeout(function(){  nextClientStep(pro1, pro2, pro3, pro4, pro5, pro6, pro7, pro8, pro9, laststep, model, pupid, pupsn) }, 200);
        }

        var pro4 = obj.screen_id;
        var pro5 = obj.dvd_id;
        var pro6 = obj.hdd_id;
        var pro7 = obj.ram_id;

        if(obj.SearchType == 'Defects'){
			var pro8 = 0;
			setTimeout(function(){  nextClientStep(pro1, pro2, pro3, pro4, pro5, pro6, pro7, pro8, pro9, laststep, model, pupid, pupsn) }, 200);               
        }

        var pro8 = obj.Defects;            
        if(obj.SearchType == 'Services'){
            setTimeout(function(){  nextClientStep(pro1, pro2, pro3, pro4, pro5, pro6, pro7, pro8, pro9, laststep, model, pupid, pupsn) }, 200);                
        }
    }        
});

function nextClientStep(pro1, pro2, pro3, pro4, pro5, pro6, pro7, pro8, pro9, lastId, modelid, pupid, pupsn) {
    var token = $("input[name=_token]").val();
    var quote_item_id = $('#quote_item_id').val();
    var SITE = $('#siteurl').val();
    lastId++;
    console.log(SITE + '/hpUser/totalSteps?_token=' + token + '&pro1=' + pro1 + '&pro2=' + pro2 + '&pro3=' + pro3 + '&pro4=' + pro4 + '&pro5=' + pro5 + '&pro6=' + pro6 + '&pro7=' + pro7 + '&pro8=' + pro8 + '&pro9=' + pro9 + '&lastStep=' + lastId + '&quote_item_id=' + quote_item_id + '&modelid=' + modelid + '&pupid=' + pupid + '&pupsn=' + pupsn);
    console.log(SITE + '/dashboard/totalSteps?_token=' + token + '&pro1=' + pro1 + '&pro2=' + pro2 + '&pro3=' + pro3 + '&pro4=' + pro4 + '&pro5=' + pro5 + '&pro6=' + pro6 + '&pro7=' + pro7 + '&pro8=' + pro8 + '&pro9=' + pro9 + '&lastStep=' + lastId + '&quote_item_id=' + quote_item_id + '&modelid=' + modelid);


    $.ajax({
        type: 'post',
        url: SITE + '/store/totalSteps',
        data: {
            '_token': token,
            'pro1': pro1,
            'pro2': pro2, 
            'pro3': pro3, 
            'pro4': pro4, 
            'pro5': pro5,
            'pro6': pro6,
            'pro7': pro7,
            'pro8': pro8,
            'pro9': pro9,
            'lastStep': lastId,
            'quote_item_id': quote_item_id,
            'modelid': modelid,
			'pupid': pupid,
			'pupsn': pupsn
        },
        dataType: 'JSON',
        success: function (obj) {
            $('#lastId').val(lastId);
            if (obj.emptyData == 'empty') {
                $('#loadingDiv').css('display', 'none');
                $('#modal-bootstrap-tour').modal('show');
            } else {
                $('.mainDiv').html(obj.html);
                $('#loadingDiv').css('display', 'none');
                window.scrollTo(0, 0);
            }
        },
        error: function (error) {
            alert('error block');
        }
    });
}
</script>
@endsection