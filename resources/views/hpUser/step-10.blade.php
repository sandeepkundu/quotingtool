@php  
use App\Http\Controllers\HpUsersController;
@$Theme = HpUsersController::Theme_Cookie(); 
$ccFilename = 'StoreStep10.css';
$cssfile = asset('css/'.$ccFilename);
if(isset($Theme)){
    if(!empty($Theme->theme_folder)) {
        if(file_exists(public_path() . '/' . @$Theme->theme_folder . '/' . $ccFilename)){
            $cssfile = asset('/'). @$Theme->theme_folder . '/' . $ccFilename;
        }
    }
}
@endphp
<link rel="stylesheet" type="text/css" href="{{$cssfile}}">

<form action="{{url('/')}}/saveStoreData" method="post" id="formfinalstep">
    {{csrf_field()}}
    
    <input type="hidden" id="modelid" name="modelid" value="{{isset($Model)?$Model->id:0}}" abc="{{@$Model->id}}">
	<input type="hidden" id="pupid" name="pupid" value="{{isset($PUP)?$PUP->id:0}}" abc="{{@$PUP->id}}">
	<input type="hidden" id="pupsn" name="pupsn" value="{{isset($PUP)?$PUP->serialnumber:0}}" abc="{{@$PUP->serialnumber}}">
    
    <section class="search-bar" style="background-color: #fff!important;padding-top: 25px !important;">
        <div class="container">

            <h3 class="title text-center">Device Specifications 
				@if (isset($PUP) && !empty($PUP->serialnumber))
				    (PUP Reference: {{$PUP->serialnumber}})   				
				@elseif (isset($Model) && !empty($Model->model))
				    ({{$Model->model}})
				@endif   
				</h3>
            <div class="device-spec-row row-center">

                <div class="card card-selected card-final">
                    <div class="brand-img">
                        <img src="{{ URL::to('product/'.$Product->image) }}" class="img-responsive" />
                    </div>
                    <div class="brand-title productname" id="{{$Product->id}}" >{{$Product->name}}</div>
                    <input type="hidden" name="Product" value="{{$Product->id}}">
                    <input type="hidden" name="product_name" value="{{$Product->name}}">
                </div>

                @if(@$Brand->name!='')
                <div class="card card-selected card-final">
                    <div class="selected-brand-wrap brand-img">
                        <div class="selected-brand" style="background-image:url({{ URL::to('product/'.@$Brand->image) }})"></div>
                    </div>
                    <div class="brand-title brandname" id="{{@$Brand->id}}">{{@$Brand->name}}</div>     
                    <input type="hidden" name="Brand" value="{{@$Brand->id}}">
                    <input type="hidden" name="brand_name" value="{{@$Brand->name}}">         
                </div>
                @endif
                
                @if(@$Processor->name!='')
                <div class="card card-selected card-final">
                    <div class="brand-img">
                        {{ @$Processor->type }}
                    </div>
                    <div class="brand-title" data-toggle="tooltip" title="{{$Processor->name}}">{{$Processor->name}}</div>
                    <input type="hidden" name="Processor" value="{{$Processor->id}}">
                    <input type="hidden" name="proccessor_name" value="{{$Processor->name}}">
                </div>
                @endif

                @if(@$Screen->name!='')
                <div class="card card-selected card-final">
                    <div class="brand-img">
                        {{ @$Screen->type }}
                    </div>
                    <div class="brand-title" >{{@$Screen->name}}</div>
                    <input type="hidden" name="Screen" value="{{ @$Screen->id }}">
                    <input type="hidden" name="screen_name" value="{{ @$Screen->name }}">
                </div>
                @endif

                @if(@$DVD->name!='')
                <div class="card card-selected card-final">
                    <div class="brand-img" >
                        {{ @$DVD->type }}
                    </div>
                    <div class="brand-title" >{{$DVD->name}}</div>
                    <input type="hidden" name="DVD" value="{{ @$DVD->id }}">
                    <input type="hidden" name="dvd_name" value="{{ @$DVD->name }}">
                </div>
                @endif

                @if(@$HDD->name!='')
                <div class="card card-selected card-final">
                    <div class="brand-img">
                        {{ @$HDD->type }}
                    </div>
                    <div class="brand-title" >{{@$HDD->name}}</div>
                    <input type="hidden" name="HDD" value="{{ @$HDD->id }}">
                    <input type="hidden" name="hdd_name" value="{{ @$HDD->name }}">
                </div>
                @endif

                @if(@$RAM->name!='')
                <div class="card card-selected card-final">
                    <div class="brand-img">
                        {{ @$RAM->type }}
                    </div>
                    <div class="brand-title" >{{@$RAM->name}}</div>
                    <input type="hidden" name="RAM" value="{{ @$RAM->id }}">
                    <input type="hidden" name="ram_name" value="{{ @$RAM->name }}">
                </div>
                @endif

            </div><!-- device-spec-row -->
            
            @if(isset($Defects) && count($Defects)>0)
                @php $alignLeft = (count($Defects)) > 7 ? "row-center row-left": "row-center"; @endphp
                <h3 class="title text-center">Device Condition</h3>
                <div class="defects-selected {{$alignLeft}}">
                    @foreach($Defects as $Defect)
                    <?php $def[] = $Defect->id; ?>
                    <div class="card card-selected card-final" id="{{$Defect->id}}">
                        <div class="checked">
                            <div class="url_img" style="position:relative"><i class="fa fa-check"></i></div>
                        </div>  
                        <div class="Processors">{{ $Defect->name}}</div>
                        <div class="brand-title" style="margin-bottom:3px;">
                            @if($Defect->description != '')
                            <i class="fa fa-question-circle" name="{{$Defect->id}}" style="margin-right: 5px; float: right;"></i>
                            @endif
                        </div>
                        <input type="hidden" name="Defects[]" value="{{ @$Defect->id }}">
                        <input type="hidden" name="defects_name[]" value="{{ @$Defect->name }}">
                        <input type="hidden" name="value[]" value="{{ @$Defect->value }}">    
                    </div>
    
                    @endforeach 
                </div>
            @endif
 
            @if(@$Services->name!='' && $isServiceDefault == 0)
            <h3 class="title text-center">Services</h3>
            <div class="row-center">
                <div class="card card-selected card-final">
                    <div class="brand-img">
                        {{@$Services->name}}
                    </div>
                    <div class="brand-title"></div>
                </div>
            </div>
            @endif
            @if(@$Services->name!='')
            <input type="hidden" name="Services" value="{{ @$Services->id }}">
            <input type="hidden" name="services_name" value="{{ @$Services->name }}">
            @endif
            
            @if(@$role == 2 || (@$role == 3 && @in_array(1,$permission)) )
           <!--  <div class="row" style="padding-top: 70px;">

                <table border="1" class="pull-right table" style="width:100%;font-size: 10px;" >
                    <tr>
                        <th style="width:60%"></th>
                        <th style="text-align: center;width:40%">Total Product Value</th>
                    </tr>
                    <tr>
                        <td style="padding-left: 5px;">TTG to <span class="setLastClient">HP</span></td>
                        <td style="text-align: center;"><span id="setLastTthToHp">17,143</span></td>
                    </tr>
                    <tr>
                        <td style="padding-left: 5px;"><span class="setLastClient">HP</span> to Customer</td>
                        <td style="text-align: center;"><span id="setLastHpToClient">12,000</span></td>
                    </tr>
                    <tr>
                        <td style="padding-left: 5px;">Margin %</td>
                        <td style="text-align: center;" id="marginPercent"></td>
                    </tr>
                </table>

            </div> -->
            @endif   

            <div class="row">   
                <div class="col-sm-8">
                    <div class="table-responsive productIdTrue Financial">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr style="background-color: #56bdf1;color:#fff">
                                    <th align="center" style="font-weight: 700;font-size: 15px;">Financial Summary</td>
                                    <th align="center" style="font-weight: 600;">Product Value</td>
                                    <th align="center" style="font-weight: 600;">Services Fee</td>
                                    <th align="center" style="font-weight: 600;">Logistics Fee</td>
                                    <th align="center" style="font-weight: 600;">Net Value</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>TTG to HP</td>
                                    <td align="center" class="ProductTTGtoHP"></td>
                                    <td align="center" class="ServicesTTGtoHP"></td>
                                    <td align="center" class="LogisticsTTGtoHP"></td>
                                    <td align="center" class="TotalTTGtoHP"></td>
                                </tr>
                                <tr>
                                    <td>HP to Customer</td>
                                    <td align="center" class="ProductHPtoCustomer"></td>
                                    <td align="center" class="ServicesHPtoCustomer"></td>
                                    <td align="center" class="LogisticsHPtoCustomer"></td>
                                    <td align="center" class="TotalHPtoCustomer"></td>
                                </tr>
                                <tr>
                                    <td>Margin / Markup $</td>
                                    <td align="center" class="productMarkup"></td>
                                    <td align="center" class="serviceMarkup"></td>
                                    <td align="center" class="logisticMarkup"></td>
                                    <td align="center" class="totalMarkup"></td>
                                </tr>
                                <tr>
                                    <td>Margin %</td>
                                    <td align="center" class="ProductMargin"></td>
                                    <td align="center" class="ServicesMargin"></td>
                                    <td align="center" class="LogisticsMargin"></td>
                                    <td align="center" class="TotalMargin"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div><!-- table-responsive -->
                </div>
                <div class="col-sm-4">
                    <h3>Your device price <b id="getTotal"></b></h3>
                    <input type="text" name="reference" class="form-control Reference" placeholder="Reference" style="height:40px;margin-bottom: 15px;">
                    <a href="javascript:void(0)" id="2" class='btn btn-cta btn-lg btn-block update_name'>Accept Quote</a>
                </div>                
            </div>
        </div>   
    </section>
    <input type='hidden' id='quote_id' name='quote_id' value='{{@$QuoteItemDetail->quote_id}}'>   
    <input type="hidden" id="quote_item_id" name="quote_item_id" value="{{ @$QuoteItemDetail->id }}">
</form>
<form action="{{URL('/')}}/quote/step" id="Quote_Edit_Form" method="post">
    {{csrf_field()}}
    <input type='hidden' id="Quote_Item_Id" name="Quote_Item_Id" value=''>
    <input type='hidden' id='Quote_Id' name="Quote_Id" value=''>
    <input type='hidden' id='client_id' name="client_id" value=''>
</form>

<form action="{{URL('/')}}/store/updateName" id="Update_Name_Form" method="post">
    {{csrf_field()}}
    <input type='hidden' id='Quote_id' name="Quote_id" value='' >
    <input type='hidden' id='status' name="status" value='' >
    <input type='hidden' class="Reference1" name="reference" value=''>
</form>

<input type="hidden" name="inprogress" value="{{URL('/')}}/quote/history/" class="inprogress">
<input type="hidden" name="siteurl" id="siteurl" value="{{ URL::to('/') }}">   
<script type="text/javascript" src="{{asset('js/StepStore10.js')}}"></script> 

<!--==============================Modal Popup=======================-->
<div class="modal modal-success fade in" id="ProductIdNotFound" style="display: none;">
    <div class="modal-dialog" role="document" style="margin: 150px auto;">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #56bdf1;">
                <button type="button" onclick="window.reload();" id="close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title">Warning</h4>
            </div>
            <div class="modal-body" style="height: auto;overflow: auto;">
                <h4 style="color: #00B1E1;">You can't create a quote for this client due to no relevant data being found in the product table.</h4>
            </div>
        </div>
    </div>
</div>
<!--==============================Modal Popup=======================-->