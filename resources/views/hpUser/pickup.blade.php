<?php //echo "pre"; print_r($quotes);
	$i=0; 
?>
<h2 style="color: #00B1E1;">Pickup Request Detail</h2>	
	<?php $i=1; ?>
<table cellpadding="2" border="1">
	<tr>
		<td>User Name : </td>
		<td>{{ @$quotes[0]->Username->first_name }} {{ @$quotes[0]->Username->last_name }}</td>
	</tr>
	<tr>
		<td>System Reference : </td>
		<td>test</td>
	</tr>
	<tr>
		<td>Customer Name : </td>
		<td>test</td>
	</tr>
</table>

<?php $i=1; $a= array(); ?>
<table width="100%" border="2"> 
	<tr>
		<th>S.No.</th>
		<th>Quote Name</th>
		<th>Client Name</th>
		<th>Product</th>
		<th>Brand</th>
		<th>Processor</th>
		<th>Screen</th>
		<th>RAM</th>
		<th>DVD</th>
		<th>HDD</th>
		<th>Services</th>
		<th>Date Generated</th>
		<th>Quantity</th>
		<th>Value</th>
	</tr>
	@foreach($quotes as $quote)
		<?php $a[] = @$quote->total_value; ?>
		<tr>
			<td>{{ @$i++ }}</td>
			<td>{{ @$quote->name }}</td>
			<td>{{ @$quote->Clientname->name }}</td>
			<td>{{ isset($quote->QuoteItem->type_name)?$quote->QuoteItem->type_name:'N/A' }}</td>
			<td>{{ isset($quote->QuoteItem->brand_name)?$quote->QuoteItem->brand_name:'N/A' }}</td>
			<td>{{ isset($quote->QuoteItem->processor_name)?$quote->QuoteItem->processor_name:'N/A' }}</td>
			<td>{{ isset($quote->QuoteItem->screen_name)?$quote->QuoteItem->screen_name:'N/A' }}</td>
			<td>{{ isset($quote->QuoteItem->ram_name)?$quote->QuoteItem->ram_name:'N/A' }}</td>
			<td>{{ isset($quote->QuoteItem->dvd_name)?$quote->QuoteItem->dvd_name:'N/A' }}</td>
			<td>{{ isset($quote->QuoteItem->hdd_name)?$quote->QuoteItem->hdd_name:'N/A' }}</td>
			<td>{{ isset($quote->QuoteItem->services_name)?$quote->QuoteItem->services_name:'N/A' }}</td>
			<td>{{ isset($quote->QuoteItem->date_created)?$quote->QuoteItem->date_created:'N/A' }}</td>
			<td>{{ isset($quote->QuoteItem->quantity)? $quote->QuoteItem->quantity :'N/A' }}</td>
			<td>{{ isset($quote->QuoteItem->total_price)?$quote->QuoteItem->total_price:'N/A' }}</td>
		</tr>
	@endforeach

	<tr style="border-bottom: 1px;">
		<th colspan="13" style="text-align: left;"> Total Value</th>
		<th>${{array_sum($a)}}</th>
		<td> </td>
	</tr>
</table>
