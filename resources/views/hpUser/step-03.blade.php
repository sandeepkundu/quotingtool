@php  
use App\Http\Controllers\HpUsersController;
@$Theme = HpUsersController::Theme_Cookie(); 
$ccFilename = 'storeStep03.css';
$cssfile = asset('css/'.$ccFilename);
if(isset($Theme)){
	if(!empty($Theme->theme_folder)) {
		if(file_exists(public_path() . '/' . @$Theme->theme_folder . '/' . $ccFilename)){
			$cssfile = asset('/'). @$Theme->theme_folder . '/' . $ccFilename;
		}
	}
}
@endphp
<link rel="stylesheet" type="text/css" href="{{$cssfile}}">

<div style="background:#fff">
    <section class="" style="padding-top: 20px;">
        <div class="container">
            <ul class="progressbar">
                <li class="active1" style="width: {{(100/(count($stepsProgress)+2))}}%;">
                 {{$Product->type}}
                </li>
                <li class="active1" style="width: {{(100/(count($stepsProgress)+2))}}%;">
                {{$Brand->type}}
                </li>
                <?php $i = 2; ?>
                @if(count($stepsProgress)>0)
                @foreach($stepsProgress as $value)
                @if(($lastStep-2)>($i-1))
                <?php $active = 'active1'; ?>
                @else
                <?php $active = ''; ?>
                @endif
                <li class="{{$active}}" style="width: {{(100/(count($stepsProgress)+2))}}%;">
                <?php 
                $abc = preg_replace('/[0-9]+/', '', @$value->group_code ); 
                $abc = str_replace( ',', '/', $abc);
                ?>
                @if($abc == "Defects")
                Condition
                @else
                {{$abc}}
                @endif
                </li>
                <?php $i++; ?>
                @endforeach
                @endif
            </ul>           
        </div> 
    </section>
    <?php $def = array(); ?>
    <input type="hidden" id="quote_item_id" name="quote_item_id" value='{{@$quoteItemId}}'>
    <input type="hidden" id="quote_id" name="quote_id" value='{{@$QuoteItemDetail->quote_id}}'>
    <input type="hidden" name="lastId" id="lastId" value="{{isset($lastStep)? $lastStep:3}}">
    <input type="hidden" id="ProductValue" name="Product" value="{{isset($Product->id)?$Product->id:0}}">
    <input type="hidden" id="BrandValue" name="Brand" value="{{isset($Brand->id)?$Brand->id:0}}">
    <input type="hidden" id="ProcessorValue" name="Processor" value="{{isset($Processor->id)?$Processor->id:0}}" abc="{{@$Processor->current}}">
    <input type="hidden" id="ScreenValue" name="Screen" value="{{isset($Screen->id)?$Screen->id:0}}" abc="{{@$Screen->current}}">
    <input type="hidden" id="DVDValue" name="DVD" value="{{isset($DVD->id)?$DVD->id:0}}" abc="{{@$DVD->current}}">
    <input type="hidden" id="HDDValue" name="HDD" value="{{isset($HDD->id)?$HDD->id:0}}" abc="{{@$HDD->current}}">
    <input type="hidden" id="RAMValue" name="RAM" value="{{isset($RAM->id)?$RAM->id:0}}" abc="{{@$RAM->current}}">
    <input type="hidden" id="ServicesValue" name="Services" value="{{isset($Services->id)?$Services->id:0}}">
    <input type="hidden" id="modelid" name="modelid" value="{{isset($Model)?$Model->id:0}}" abc="{{@$Model->id}}">
   	<input type="hidden" id="pupid" name="pupid" value="{{isset($PUP)?$PUP->id:0}}" abc="{{@$PUP->id}}">
	<input type="hidden" id="pupsn" name="pupsn" value="{{isset($PUP)?$PUP->serialnumber:0}}" abc="{{@$PUP->serialnumber}}">

    <section class="search-bar" style="padding: 10px 0px;padding-bottom: 20px;background-color:#d2dee1;">
        <div class="container">
            <h3 class="text-center dev-spec-selection">Device Specification: 
				@if (isset($PUP) && !empty($PUP->serialnumber))
				(PUP Reference: {{$PUP->serialnumber}})   				
				@elseif (isset($Model) && !empty($Model->model))
				({{$Model->model}})
				@endif   
            </h3> 
            <div class="device-spec-row row-center">
                <a href="javascript:void(0)" class="card card-selected" <?php if(!isset($Model->id) || $Model->id == 0) { ?> title="Back to product type selection" onclick="reload()" <?php } ?>>
                    <div class="brand-img">
                        <img src="{{ URL::to('product/'.$Product->image) }}" />
                    </div>
                    <div class="brand-title">{{$Product->name}}</div>
                </a>
    
                 <a href="javascript:void(0)" class="card card-selected" <?php if(!isset($Model->id) || $Model->id == 0) { ?>title="Back to brand selection" onclick="step01({{ $Product->id }})" <?php } ?>>
                    <div class="selected-brand-wrap brand-img">
                        <div class="selected-brand" style="background-image:url({{ URL::to('product/'.$Brand->image) }})"></div>
                    </div>
                    <div class="brand-title">{{$Brand->name}}</div>
                </a>
    
                @if(isset($Processor->name))
                <a href="javascript:void(0)" class="card card-selected" <?php if(!isset($Model->id) || $Model->id == 0) { ?> title="Back to processor selection" onclick="backSelection({{$Processor->current}})"<?php } ?>>
                    <div class="brand-img">
                        {{ @$Processor->type }}
                    </div>
                    <div class="brand-title" data-toggle="tooltip" title="{{$Processor->name}}">{{$Processor->name}}</div>
                </a>
                @endif

                <?php 
				if(isset($Screen->name)){
					$addOnclick = '';
					foreach ($stepsProgress as $stp){	
						if(strpos($stp->group_code, 'Screen') !== false){
							$addOnclick = 'backSelection('.$Screen->current.')';
						}
					}
					?>
					<a href="javascript:void(0)" class="card card-selected" title="Back to screen size selection" onclick="{{$addOnclick}}">
                    <div class="brand-img">{{@$Screen->type}}</div>
                    <div class="brand-title">{{$Screen->name}}</div>
					</a>
				<?php } ?>
    
                <?php 
				if(isset($DVD->name)){
					$addOnclick = '';
					foreach ($stepsProgress as $stp){
						if(strpos($stp->group_code, 'DVD') !== false){
							$addOnclick = 'backSelection('.$DVD->current.')';
						}
					}
					?>
					<a href="javascript:void(0)" class="card card-selected" title="Back to 'has DVD' selection" onclick="{{$addOnclick}}">
                    <div class="brand-img">{{ @$DVD->type }}</div>
                    <div class="brand-title">{{$DVD->name}}</div>
                </a>                
               <?php } ?>
    
                <?php 
				if(isset($HDD->name)){
					$addOnclick = '';
					foreach ($stepsProgress as $stp){
						if(strpos($stp->group_code, 'HDD') !== false){
							$addOnclick = 'backSelection('.$HDD->current.')';
						}
					}
					?>
					<a href="javascript:void(0)" class="card card-selected" title="Back to HDD size selection" onclick="{{$addOnclick}}">
                    <div class="brand-img"> {{ @$HDD->type }}</div>
                    <div class="brand-title">{{$HDD->name}}</div>
					</a>                
                <?php } ?>
    
                <?php 
				if(isset($RAM->name)){
					$addOnclick = '';
					foreach ($stepsProgress as $stp){
						if(strpos($stp->group_code, 'RAM') !== false){
							$addOnclick = 'backSelection('.$RAM->current.')';
						}
					}
					?>
					<a href="javascript:void(0)" class="card card-selected" title="Back to RAM size selection" onclick="{{$addOnclick}}">
                    <div class="brand-img">{{ @$RAM->type }}</div>
                    <div class="brand-title">{{$RAM->name}}</div>
					</a>                 
                <?php } ?>
                
            </div><!-- device-spec-row -->


            @if(isset($Defects) && count($Defects) > 0)
            <h3 class="text-center dev-spec-selection">Device Condition Selected:</h3>
            @php $alignLeft = (count($Defects)) > 7 ? "row-center row-left": "row-center"; @endphp
            <div class="defects-selected {{$alignLeft}}">
            @foreach($Defects as $Defect)
            <?php $def[] = $Defect->id; ?>

                <div class="card a card-selected" title="Back to reselect conditions" id="{{$Defect->id}}" onclick="backSelection({{$Defects->current}});">
                    <div class="checked">
                        <div class="url_img" style="position:relative"><i class="fa fa-check"></i></div>
                    </div>	
                    <div class="Processors">{{ $Defect->name}}</div>
                    <div class="brand-title">
                        @if($Defect->description != '')
                        <i class="fa fa-question-circle" name="{{$Defect->id}}"></i>
                        @endif
                    </div>
                </div>
            @endforeach
            </div><!-- defects selected -->
            @endif

			@if(isset($Services->name) && $isServiceDefault == 0)
            <div class="card card-selected">
                <div class="brand-img">
                    {{ @$Services->type }}
                </div>
                <div class="brand-title">{{$Services->name}}</div>
            </div>
            @endif

        </div><!-- container -->
    </section>
</div>

<section class="device-section">
    <div class="container">

    @if(isset($stepPro['item'])  && count($stepPro['item'])>0 || count($stepPro) >  0)
        
        @if(isset($stepPro[0]))

        <h2 class="title text-center">Select </h2>
        <div class="row-center">
        @foreach(@$stepPro as $groupStep)

        <div class="card card-selectmenu">

            <div class="brand-title" style="position: unset;padding-top: 25px;">
                @if($groupStep['name']=='Processor')
                @php 
                $flag = 1;
                @endphp
                <b>{{$groupStep['subgroup']}}</b>
                @else
                <b>{{ preg_replace('/[0-9]+/', '', @$groupStep['name'])}}</b>
                @php 
                $flag = 0;
                @endphp
                @endif
            </div>

            @php 
			$isSelected = FALSE;
			@endphp 
			
			@foreach(@$groupStep['item'] as $product1)								
				@if(@($product1->option_id == $QuoteItemDetail->processor_id) ||@($product1->option_id == $QuoteItemDetail->screen_id) ||@($product1->option_id == $QuoteItemDetail->dvd_id) ||@($product1->option_id == $QuoteItemDetail->hdd_id) ||@($product1->option_id == $QuoteItemDetail->ram_id) ||@($product1->option_id == @Session::get('Processor_old')) ||@($product1->option_id == @Session::get('Screen_old')) ||@($product1->option_id == @Session::get('HDD_old')) ||@($product1->option_id == @Session::get('RAM_old')) ||@($product1->option_id == @Session::get('DVD_old')))
					@php 
					$isSelected = TRUE;
					@endphp 
				@endif								
			@endforeach
            <select id="soflow-color" name="{{$groupStep['name']=='Processor'?'Processor':'Screen'}}" group="{{ preg_replace('/[0-9]+/', '', @$groupStep['name'])}}" class="form-control {{$isSelected ? ' option-selected' : '' }} {{$groupStep['name']=='Processor'?'Processor':'Screen'}} {{ preg_replace('/[0-9]+/', '', @$groupStep['name'])}}">

                <option value=""  class="selectname">{{$groupStep['name']=='Processor'?'select version':'Size'}}</option>

                @foreach(@$groupStep['item'] as $product1)

                <option value="{{$product1->option_id}}" {{@($product1->option_id == @Session::get('Processor_old')) ||@($product1->option_id == @Session::get('Screen_old')) ||@($product1->option_id == @Session::get('HDD_old')) ||@($product1->option_id == @Session::get('RAM_old')) ||@($product1->option_id == @Session::get('DVD_old'))? 'selected':''}}>{{ $product1->name }}</option>

                @endforeach                         

            </select>

        </div>


        @endforeach
        </div>
            
        <script>
            // update blue highlight on current processor option
            var $selectprocessor = $( ".form-control.Processor.Processor" );
            $selectprocessor.change(function() {
                $selectprocessor.not(this).prop('selectedIndex',0);
                $selectprocessor.removeClass("option-selected");
                if($(this).val() != ''){
                    $(this).addClass("option-selected");
                }
            });
            // update blue highlight on current screen/dvd/hdd/ram
            var $selectscreen = $( ".form-control.Screen" );
            $selectscreen.change(function() {
                if($(this).val() == ''){
                    $(this).removeClass("option-selected");
                } else {
                    $(this).addClass("option-selected");
                }
            });            
        </script>
        
        <p class="text-center">

            @if(@$flag == 1)
            <button onclick="Processor()" class="btn btn-cta btn-lg next">Continue</button>
            @elseif(@$flag == 0)
            <button onclick="Screen()" class="btn btn-cta btn-lg next_disabled next group" disabled="">Continue</button>
            @endif


        </p>

        <!--=============================x Services x==============================-->
        @elseif(@$stepPro['name'] == 'Services')
        
        <h2 class="title text-center">Select Services</h2>
        <div class="services-list row-center">
            @foreach(@$stepPro['item'] as $product1)
            <div class="card  {{@($product1->option_id == $QuoteItemDetail->services_id) || @($product1->option_id == Session::get('services_')) ? 'selected':''}}" onclick="Services({{$product1->option_id}})">
                <span id="{{$product1->id}}" class="Processors" style="padding: 0px 3px;">{{ $product1->name }}</span>
            </div>
            @endforeach
        </div>
    
        @if(isset($QuoteItemDetail->services_id) && $QuoteItemDetail->services_id != null)
        <button class="pull-left btn-cta btn next_disabled next" onclick="QuoteEditSave();">Save</button>
        @endif
        
    <!--=============================x Services x==============================-->


                <!--=============================x Defects x==============================-->

                @elseif(@$stepPro['name'] == 'Defects')
                
                @php $alignLeft = (count(@$stepPro['item'])) > 7 ? "row-center row-left": "row-center"; @endphp
                <h2 class="title text-center">Specify Device Condition</h2>
                <div class="conditions {{$alignLeft}}">

                @php $df_old = array(); @endphp

                    @if(Session::has('Defects_Old'))

                    @foreach(Session::get('Defects_Old') as $Defect_Old)
                      @php  $df_old[] =$Defect_Old->id; @endphp

                    @endforeach
                    @endif


                @foreach(@$stepPro['item'] as $product1)

                    @if(@$stepPro['name'] == 'Defects')
                        @if($product1->allow != 'not_allow')
    
                        <div class="card a {{@(in_array($product1->option_id,$QID)) || @(in_array($product1->option_id,$df_old))  ? 'activeClass':''}}" id="{{$product1->option_id}}">
                            <div class="checked">
                                <?php /*<img src="{{asset('product/checked-icon.png')}}" style="display:none;position: relative;" class ="url_img">*/ ?>
                                <div class="url_img" style="display:none;position:relative"><i class="fa fa-check"></i></div><?php /*<img src="{{asset('product/checked-icon.png')}}" class ="url_img">*/ ?>
                            </div>  
                            <div class="Processors" style="padding: 0px 3px;">{{ $product1->name}}</div>
                            <div class="brand-title fordefect" style="margin-bottom:3px;">
                                @if($product1->description != '')
                                <i class="fa fa-question-circle" name="{{$product1->option_id}}"></i>
                                @endif
                            </div>
                        </div>
    
                        @endif
    
                        @if($product1->allow == 'not_allow')
    
                        <div class="card a not {{@(in_array($product1->option_id,$QID)) ? 'activeClass':''}}" id="{{$product1->option_id}}" vals="{{$product1->option_id}}" style=""> 
                            <div class="Processors warningmargin" style="padding: 0px 3px;">
                                {{ $product1->name}}
                            </div>
                            
                            <div class="brand-title fordefect" style="margin-bottom:3px;">
    
                                @if($product1->description != '')
    
                                <i class="fa fa-question-circle" name="{{$product1->option_id}}"></i>
                                @endif
                            </div>
    
                            <div class="waringblock content-title">
                               <i class="fa fa-warning" style="font-size:48px;color:red"></i>
                           </div>
                            <div class="waringblock2">
                                 
                                Device ineligible for trade-in
                            </div>
                        </div>
                        @endif
    
                    @endif
                @endforeach

                </div>
            @endif                    
        </div>

        <span style="color: red;" id="errorMessage" style="font-size: 17px;"></span>
        <p class="text-center">
        @if(@$QID != null && @$stepPro['name'] == 'Defects')
        <button class="btn btn-cta next_disabled next" onclick="QuoteEditSave();">Save</button>
        @endif
        @if(@$stepPro['name'] == 'Defects')
        
        <button onclick="stepDefect()" class="btn btn-cta next_disabled next" disabled="">Continue</button>
        @endif
        </p>
    </div>
    @endif
</div>
</div>
</section>

<input type="hidden" id="count" value="">
<input type="hidden" name="multipleDefects" value="{{implode(",", $def)}}" class="Defects" abc="{{@$Defects->current}}">
<input type="hidden" name="multipleDefects_Not" value="{{implode(",", $def)}}" class="Defects_Not">
<!--==============================Modal Popup=======================-->
<div class="modal modal-success fade in" id="modal-bootstrap-question" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-dialog" role="document" style="margin: 150px auto;">
        <div class="modal-content" id="modal-content">
            <div class="modal-header" id="modal-header">
                <button type="button" id="close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</i></span></button>
                <!-- <h4 class="modal-title"><span class="username"></span></h4> -->
            </div>
            <div class="modal-body" style="height: auto;overflow: auto;">
                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                    <div class="col-sm-4 col-md-4 col-sm-offset-4 col-md-offset-4">
                        <div class="row-one">
                            <div class="box-1">
                                <div class="Processors">
                                    Defect type
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12" id="descriptionDefect">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--==============================Modal Popup=======================-->

<!--==============================Modal Popup Validation=======================-->
<div class="modal modal-success fade in" id="modal-bootstrap-validation" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-dialog" role="document" style="margin: 150px auto;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Alert <span class="username"></span></h4>
            </div>
            <div class="modal-body" id="modal-body" style="color:red;">

            </div>
        </div>
    </div>
</div>

<!--==============================Modal Popup Validation=======================-->
<script type="text/javascript" src="{{asset('js/storeStep03.js')}}"></script>
