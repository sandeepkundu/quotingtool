@extends('layouts.hpUser') 
@section('content')
<link href="{{ asset('assets/global/plugins/bower_components/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/global/plugins/bower_components/animate.css/animate.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/global/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet">
<link href="{{ asset('assets/global/plugins/bower_components/jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/global/plugins/bower_components/chosen_v1.2.0/chosen.min.css') }}" rel="stylesheet">
<style>
    .selected-box{
        width: 150px;
        height: 150px;
    }
    .two-col-layout-dashboard .panel-heading {
        font-weight: 300;
        font-size: 24px;
        text-align: center;
        text-transform: capitalize;
        color:#fff;
        background:#01518e;
    }
    thead tr th{
        background: #56bdf1;
        color: #fff;
    }
    .pagination{
        margin:0px;
    }
    div.dataTables_length label {
        font-weight: normal;
        float: left;
        text-align: left;
    }
    select {
        display: inline-block;
        width: auto;
        vertical-align: middle;
    }

    div.dataTables_length select {
        margin-right: 10px;
    }
    label {
        display: inline-block;
        max-width: 100%;
        margin-bottom: 5px;
        font-weight: 700;
    }
    div.dataTables_length select {
        width: auto;
    }
    form .form-group.has-feedback button.form-control-feedback {
        width: 24px;
        height: 24px;
    }
    form .form-group.has-feedback button.form-control-feedback {
        top: 5px;
        right: 5px;
        width: 25px;
        height: 25px;
        line-height: 6px;
        position: absolute;
        pointer-events: auto !important;
    }
    .has-feedback .form-control {
        padding-right: 42.5px;
    }
    .btn-hover:hover {
        background-color: #009dc8 !important; 
        color: white;
    }
    .btn-color {
        background-color: #00B1E1 !important;
        color: #fff;
        font-weight: 400;
        border-color: #00B1E1;
    }
    .form-control:focus {
        border: 1px solid #66afe9 !important;
    }

    .form-control:focus{
         border: 1px solid #56bdf1 !important;
    }
    body{
        background: #f1f4f5 !important;
    }
    .coll_sub_btn,.changeStatuss{
        background-color: #56bdf1 !important;
        border: solid 1px #56bdf1;
        border-radius: 0px;
        color: #fff;
    }
    .coll_sub_btn:hover,.changeStatuss:hover{
        background-color: white !important;
        border: solid 1px #56bdf1;
        border-radius: 0px;
        color: #56bdf1;
    }
    
</style>
<!-- <div class="page-title">
    <div class="container">
        <div class="row">
  <div class="col-md-4 col-lg-4 col-xs-12 col-sm-6">
                <span class="page-heading-left"></span>
            </div> 
            <div class="col-md-4 col-lg-4 col-xs-12 col-sm-6">
                <span class="page-heading-center" style="text-align:left;">
                    @if(!empty($ClientName))
                    {{$ClientName->name}}
                    @endif
                </span>
            </div>
            <div class="col-md-4 col-lg-4 col-xs-12 col-sm-6"></div>
        </div>
    </div>    
</div> -->

<section class="search-bar" style="padding:0px;">
    <div class="container two-col-layout-dashboard">
        <div class="row" style="padding-bottom: 20px;text-align: center;">
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <label class="search-label"></label>    
            </div>
        </div> 
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">

                <div class="client-panel">
                    <div class="panel panel-default">
                        <div class="panel-heading">Collection History List</div>
                        <div class="row" style="padding-left: 15px;padding-top: 5px;">
                            <div class="col-xs-6">
                                <div class="dataTables_length" id="datatable-client-all_length">
                                    <label>
                                        <select name="datatable-client-all_length" class="form-control input-sm" id="paginationPerPage" style="display: inline-block;">
                                            <?php
                                            $l = 30;
                                            $total = $QuoteCollections->total();
                                            $loop = intval($total / $l);

                                            if (($total % $l) != 0) {
                                                $loop = $loop + 1;
                                            }
                                            ?>  
                                            @for($i=1; $i<=$loop;$i++)
                                            <option value="{{$i*$l}}" {{ (($i*$l)==$lim)? 'selected' : '' }}>{{$i*$l}}</option>
                                            @endfor

                                        </select>
                                        Records per page
                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-6">

                                <form action="{{URL::to('/')}}//collection-details" method="get" class="navbar-form">
                                    <div class="form-group has-feedback pull-right" style="margin-top:-10px;">
                                        <input type="text" class="form-control rounded"
                                               placeholder="Search by name" name="search">
                                        <button type="submit" class="btn btn-color  btn-hover btn-theme  form-control-feedback rounded" style="padding: 4px 7px;"><i class="fa fa-search" aria-hidden="true"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="panel-body" style="padding-top: 0px;">
                            <table class="table table-responsive table-bordered table-condensed">
                                <thead>
                                    <tr>
                                        <th>Collection ID</th>
                                        <th>Client</th>
                                        <th>User</th>
                                        <th>Date</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>

                                    @if(count($QuoteCollections)>0)
                                        @foreach($QuoteCollections as $QuoteCollection)

                                        <tr>
                                            
                                            <td>
                                                @if(!empty($QuoteCollection->id))
                                                {{@$QuoteCollection->id}}
                                                @else
                                                N/A
                                                @endif
                                            </td>
                                            <td>
                                                @if(!empty($QuoteCollection->ClientName))
                                                  {{@$QuoteCollection->ClientName}}    
                                                @else
                                                    N/A
                                                @endif
                                              
                                            </td>
                                            <td>
                                                @if(!empty($QuoteCollection->Username))
                                                  {{@$QuoteCollection->Username}}
                                                @else
                                                    N/A
                                                @endif
                                            </td>
                                            <td>
                                                @if(!empty($QuoteCollection->date_collected))
                                                 {{@$QuoteCollection->date_collected}}
                                                @else
                                                    N/A
                                                @endif                                                
                                            </td>
                                            <td>
                                                @if(!empty($QuoteCollection->status))
                                                  <button id="{{$QuoteCollection->quote_id}}" name="{{$QuoteCollection->quote_status}}" value="{{$QuoteCollection->id}}" class="btn changeStatuss"> Awaited for collection</button>
                                                @else
                                                    N/A
                                                @endif   

                                            </td> 
                                            <td><a href="{{url('/')}}/hpUser/pickupRequestDetail?client_id={{$QuoteCollection->client_id}}">View / PDF</a></td>
                                        </tr>
                                        @endforeach
                                    @else
                                        <tr><td colspan="6" style="color:red;">No records found!!!</td></tr>
                                    @endif

                                </tbody>
                            </table>

                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="dataTables_info" id="datatable-client-all_info" role="status" aria-live="polite">
                                        @if($QuoteCollections->total()!=0)
                                        Showing {{ $QuoteCollections->firstItem() }} to {{ $QuoteCollections->lastItem() }} of {{ @$QuoteCollections->total() }}
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xs-6" style="text-align: right;">
                                    <div class="dataTables_paginate paging_simple_numbers" id="datatable-client-all_paginate">
                                        {{ $QuoteCollections->links() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>   
</section>

<form action="{{URL::to('/')}}/collection-details" method="get" class="navbar-form" id="formPerPage">
    <input type="hidden" value="" name="perPage" id="perPage">
</form>
<!--==============================Modal Popup=======================--> 

<div id="loadingDiv" style="display:none;">
    <div>
        <div class="loader"></div>
    </div>
</div>

<input type="hidden" name="siteurl" id="siteurl" value="{{URL::to('/')}}">

<link href="{{ asset('css/stepsStore.css') }}" rel="stylesheet">

@section('js')


<script type="text/javascript">
    $(document).ready(function () {

        /*==========For pagination==============*/
        $('#paginationPerPage').change(function () {
            $a = $(this).val();
            $('#perPage').val($a);
            $('#formPerPage').submit();
        });
        $('.pagination>li>a').click(function () {
            var a = $(this).attr('href');
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1);
            // alert(hashes);
            var dataArray = '';
            if (hashes.indexOf("page=") >= 0) {
                var dataArray = hashes.slice(7);
                $(this).attr('href', a + '&' + dataArray);
            } else {
                $(this).attr('href', a + '&' + hashes);
            }
        });
    });

/*===============================Change status===============================*/
    $(function () {
    $('.changeStatuss').click(function () {

        var QuoteId = $(this).attr('id');
        var statusId = $(this).attr('name');
        var collection_id = $(this).val();

        var SITE = $('#siteurl').val();
        if (statusId < 6) {
            $('#loadingDiv').css('display', 'block');
            $.ajax({
                type: 'get',
                url: SITE + '/hpUser/changeStatus',
                data: {'status': statusId, 'name': QuoteId,'collection_id':collection_id},
                dataType: 'JSON',
                success: function (obj) {
                    location.reload();
                },
                error: function (error) {
                    alert('error block');
                }
            });            
        }
    });

});
</script>
@endsection

@endsection


