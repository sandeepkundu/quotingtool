<div style="background:#fff">
    <section class="" style="padding-top: 20px;">
        <div class="container">
            <ul class="progressbar">
                <li class="active1" style="width: 20%;">Product</li>
                <li class="" style="width: 20%;">Brand</li>
                <li class="" style="width: 20%;">Processor</li>
                <li class="" style="width: 20%;">Condition</li>
                <li class="" style="width: 20%;">Services</li>
            </ul>           
        </div>
    </section>
    <section class="search-bar" style="padding: 10px 0px; padding-bottom: 20px;background-color:#d2dee1;">
        <div class="container">
            <h3 class="text-center dev-spec-selection">Device Specification:</h3>  
            <div class="device-spec-row row-center">
                <a href="javascript:void(0)" class="card card-selected" title="Back to product type selection" onclick="reload()">
                    <div class="brand-img">
                    @if(!empty($product->image))
                    <img src="{{ URL::to('product/'.$product->image) }}"/>
                    @endif
                    </div>
                    <div class="brand-title">{{$product->name}}</div>
                </a>
            </div>
            <input type="hidden" id="quote_item_id" name="quote_item_id" value='{{@$QuoteItemDetail->id}}'>
            
        </div> <!-- /container -->   
    </section>
</div>
<section class="device-section">
    <div class="container">
        @if(count($results) > 0)		
            <h2 class="title text-center">Select Brand</h2>						
            @if(count($results) > 7)
            <div class="row-center row-left">
            @else
            <div class="row-center">
            @endif 
            @foreach($results as $result)
                <div class="card box-1 step02 {{@($result->id == $QuoteItemDetail->brand_id) ? 'selected':''}}" id="{{$result->id}}" onclick="step02({{$product->id}} , {{$result->id}})">
                    @if(!empty($result->image))
                    <div class="brand-img-wrap" style="background-image:url({{ URL::to('product/'.$result->image) }})"></div>
                    @endif
                </div>
                @endforeach
             </div>
            @else
        <script>$('#modal-bootstrap-tour').modal('show');</script>
        @endif     
     </div> <!-- /container -->   
</section>


