@extends('layouts.hpUser')
@section('content')
@if(isset($toast['message']))
{{ $toast['message'] }}
@endif
@section('css')
@php  
use App\Http\Controllers\HpUsersController;
@$Theme = HpUsersController::Theme_Cookie(); 
$ccFilename = 'f5_users.css';
$cssfile = asset('css/'.$ccFilename);
if(isset($Theme)){
	if(!empty($Theme->theme_folder)) {
		if(file_exists(public_path() . '/' . @$Theme->theme_folder . '/' . $ccFilename)){
			$cssfile = asset('/'). @$Theme->theme_folder . '/' . $ccFilename;
		}
	}
}
@endphp
<link href="{{ $cssfile }}" rel="stylesheet">
@endsection
<section class="dashboard" style="padding: 30px 0px; background-image:none!important; background:#f1f4f5;width:100%;height: 100% !important;">
    <div class="container">
        @if(Session::has('message'))
        <div class="alert alert-success" style="padding: 8px; text-align: center;">
            <span style='text-align: center'>{{ Session::get('message') }}</span>
        </div>
        @endif
        
        <h2 class="text-center">Edit Profile</h2>
        <div class="panel rounded shadow">
            <div class="panel-heading">

            </div>
            <div class="panel-body no-padding">
                <form class="form-horizontal mt-10 form-add-edit-user" id="commentForm" method="post" action="{{URL::to('/')}}/updatehpDashboard">
                    {{ csrf_field() }}
                    
                    <div class="form-body">

                        <input class=" form-control" id="name" value="{{ $user->id }}" name="id" minlength="2" type="hidden" required />
                        
                        <div class="row">
                            <div class="col-sm-6">
                                
                                <div class="form-group">
                                    <label for="first_name" class="col-sm-4 control-label">First Name<span style="color:red;">*</span></label>
                                    <div class="col-sm-8">
                                        <input value="{{$user->first_name}}" class=" form-control" id="first_name" name="first_name" minlength="2" type="text" placeholder="First Name">
                                            @if ($errors->has('first_name'))
                                            <span class="help-block" style="color:red;">
                                                <strong>{{ $errors->first('first_name') }}</strong>
                                            </span>
                                            @endif
                                    </div>
                                </div>
                                    
                                <div class="form-group">
                                    <label for="last_name" class="col-sm-4 control-label">Last Name<span style="color:red;">*</span></label>
                                    <div class="col-sm-8">
                                        <input value="{{$user->last_name}}" class=" form-control" id="last_name" name="last_name" minlength="2" type="text" placeholder="Last Name">
                                            @if ($errors->has('last_name'))
                                            <span class="help-block" style="color:red;">
                                                <strong>{{ $errors->first('last_name') }}</strong>
                                            </span>
                                            @endif
                                    </div>
                                    
                                </div>
                                <div class="form-group">

                                    <label for="email" class="col-sm-4 control-label">Email Address<span style="color:red;">*</span></label>
                                    <div class="col-sm-8">
                                        <input value="{{$user->email}}" type="email" name="email" class="form-control" id="email"
                                                   placeholder="Email Address" disabled>
                                            @if ($errors->has('email'))
                                        <span class="help-block" style="color:red;">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="password" class="col-sm-4 control-label">Password</label>
                                    <div class="col-sm-8">
                                            <input  class=" form-control" id="password" name="password" value="{!!old('password') !!}"minlength="2" type="password" placeholder="Password" >
                                            @if ($errors->has('password'))
                                            <span class="help-block" style="color:red;">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                            @endif
                                        
                                    </div>
                                </div>
                                
                                <div class="form-group">

                                    <label for="confirm_password" class="col-sm-4 control-label">Confirm Password</label>
                                    <div class="col-sm-8">
                                            <input type="password"  value="{!!old('confirm_password')!!}"name="confirm_password"class="form-control" id="confirm_password"
                                                   placeholder="Confirm Password">
                                            @if ($errors->has('confirm_password'))
                                            <span class="help-block" style="color:red;">
                                                <strong>{{ $errors->first('confirm_password') }}</strong>
                                            </span>
                                            @endif
                                    </div>
                                </div>

                                    

                            </div><!-- /.col -->
                            
                            <div class="col-sm-6">

                                <div class="form-group">
                                    <label for="company" class="col-sm-4 control-label">Company Name<span style="color:red;">*</span></label>
                                    <div class="col-sm-8">
                                        <input value="{{$user->company}}" type="text" class="form-control" name="company"id="company"
                                                   placeholder="Company Name" >
                                            @if ($errors->has('company'))
                                            <span class="help-block" style="color:red;">
                                                <strong>{{ $errors->first('company') }}</strong>
                                            </span>
                                            @endif
                            
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="phone" class="col-sm-4 control-label">Phone Number</label>
                                    <div class="col-sm-8">
                                            <input value="{{$user->phone }}" type="text" class="form-control" name="phone" id="phone"
                                                   placeholder="Phone Number">
                                            @if ($errors->has('phone'))
                                            <span class="help-block" style="color:red;">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                            @endif
                                    </div>
                                </div>
                                
                                <?php if (isset($clients) && count($clients) > 0) { ?>
                                <div class="form-group">
                                    <label for="client" class="col-sm-4 control-label"> Client <span
                                                style="color: red;">*</span></label>
                                    <div class="col-sm-8">
                                        <select id="client_id" data-placeholder="Choose Client"
                                                    class="form-control" multiple="true" tabindex="4">
                                                @foreach ($clients as $client)
                                                <option data-option-array-index="{{ $client->id }}" {{(in_array($client->id, $CU)) ? 'selected':''}}>{{
													$client->name }}
                                                </option>
                                                @endforeach
                                        </select>
                                        <input class="hidden" name="client_id"  />
                                    </div>
                                </div>
                                @if ($errors->has('client_id'))
                                <span class="help-block"
                                      style="color: red;"> <strong>{{ $errors->first('client_id')
                                        }}</strong>
                                </span>
                                @endif
                                    
                                <?php } ?>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Enabled</label>
                                    <div class="col-sm-8">
                                        <div class="ckbox ckbox-info checkbox">
                                                
                                            <label for="checkbox-info1"><input id="checkbox-info1" {{($user->active == '1') ? 'checked' : ''}} type="checkbox" name="active"></label>
  
                                        </div>
                                    </div>
                                </div>
                                
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div>
                    <div class="form-footer">
                        <div class="col-sm-offset-5">
                            <button class="btn btn-cta" type="submit">Update</button>
                            <a href="{{URL::to('/')}}/dashboard" class="btn btn-danger">Cancel</a>

                        </div>
                    </div>
                </form>
           
            </div>
        </div>
    </div>
</section>
{{csrf_field()}}

@endsection