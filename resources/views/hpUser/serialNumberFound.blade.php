@extends('layouts.hpUser') 
@section('content')

<link href="{{ asset('css/modelfound.css') }}" rel="stylesheet">
<link href="{{ asset('css/stepsStore.css') }}" rel="stylesheet">
<div class="page-title">
    <div class="container">
        <div class="row">
            <!-- <div class="col-md-4 col-lg-4 col-xs-12 col-sm-6">
                <span class="page-heading-left">Build your quote</span>
            </div> -->
            <div class="col-md-4 col-lg-4 col-xs-12 col-sm-6">
                <span class="page-heading-center" style="text-align: left;">{{@$clinetname->name}}</span>
            </div>

            <div class="col-md-4 col-lg-4 col-xs-12 col-sm-6"></div>
        </div>
    </div>    
</div>
<!--<section class="" style="padding-top: 20px;">
    <div class="container">
        <div class="row">
            <ul class="progressbar">
<?php $i = 2; ?>
                @if(count($stepsProgress)>1)
                    @foreach($stepsProgress as $value)
                        @if(($lastStep-2)>($i-1))
<?php $active = 'active1'; ?>
                        @else
<?php $active = ''; ?>
                        @endif

                        <li class="{{$active}}" style="width: {{(100/count($stepsProgress))}}%;">
                            {{preg_replace('/[0-9]+/', '', @$value->group_code)}}
                        </li>
<?php $i++; ?>
                    @endforeach
                @endif
            </ul>           
        </div>
    </div> 
</section>-->
<input type="hidden" id="clientID" name="clientid" value='{{@$clinetname->id}}'>
<input type="hidden" id="lastStep" name="" value="{{@$lastStep}}">
<div class="mainDiv">
    <section class="search-bar" style="padding: 40px 0px;background-color:#d2dee1;">
        <div class="container">
           <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" style="text-align: center;">
               <label class="search-label contol-label" style="float:left;">Device Specification</label> 
           </div>
       </div>
       <form>
        <div class="row">
            {{csrf_field()}}			
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <div class="row-one" style="margin-bottom: 0px;">
                    @if(@$Product->name !==null)
                    <div class="box-1 width">
                        <div class="brand-img">
                            <img src="{{ URL::to('product/'.@$Product->image) }}" height="100%" width="100%"/>
                        </div>
                        <div class="brand-title">{{@$Product->name}}</div>
                        <input type="hidden" name="Product" value="{{@$Product->id}}" class="Product">
                        <input type="hidden" name="product_name" value="{{@$Product->name}}">
                    </div>
                    @endif
                    @if(@$Brand->name != null)
                    <div class="box-1 width">
                        <div class="brand-img">
                            <img src="{{ URL::to('product/'.@$Brand->image) }}" width="100%" height="100%" />
                        </div>
                        <div class="brand-title">{{@$Brand->name}}</div>
                        <input type="hidden" name="Brand" value="{{@$Brand->id}}" class="Brand">
                        <input type="hidden" name="brand_name" value="{{@$Brand->name}}">
                    </div>
                    @endif
                    @if(@$Processor->name != null)
                    <div class="box-1 width">
                        <div class="brand-img b_title">
                            {{ @$Processor->type }}
                        </div>
                        <div class="brand-title">{{@$Processor->name}}</div>
                        <input type="hidden" name="Processor" value="{{@$Processor->id}}" class="Processor">
                        <input type="hidden" name="processor_name" value="{{@$Processor->name}}">
                    </div>
                    @endif
                    @if(@$Screen->name != null)
                    <div class="box-1 width">
                        <div class="brand-img b_title">
                            {{ @$Screen->type }}
                        </div>
                        <div class="brand-title">{{@$Screen->name}}</div>
                        <input type="hidden" name="Screen" value="{{@$Screen->id}}" class="Screen">
                        <input type="hidden" name="screen_name" value="{{@$Screen->name}}">
                    </div>
                    @endif

                    @if(@$DVD->type != null)
                    <div class="box-1 width">
                        <div class="brand-img b_title">
                            {{ @$DVD->type }}
                        </div>
                        <div class="brand-title">{{@$DVD->name}}</div>
                        <input type="hidden" name="DVD" value="{{@$DVD->id}}" class="DVD">
                        <input type="hidden" name="dvd_name" value="{{@$DVD->name}}">
                    </div>
                    @endif

                    @if(@$HDD->name != null)
                    <div class="box-1 width">
                        <div class="brand-img b_title" >
                            {{ @$HDD->type }}
                        </div>
                        <div class="brand-title">{{@$HDD->name}}</div>
                        <input type="hidden" name="HDD" value="{{@$HDD->id}}" class="HDD">
                        <input type="hidden" name="hdd_name" value="{{@$HDD->name}}">
                    </div>
                    @endif
                    @if(@$RAM->name != null)
                    <div class="box-1 width">
                        <div class="brand-img b_title">
                            {{ @$RAM->type }}
                        </div>
                        <div class="brand-title">{{@$RAM->name}}</div>
                        <input type="hidden" name="RAM" value="{{@$RAM->id}}" class="RAM">
                        <input type="hidden" name="ram_name" value="{{@$RAM->name}}">
                    </div>
                    @endif

                </div>
            </div>
        </div> 

        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <div class="row-one defects" style="margin-bottom: 0px;">

                </div>
            </div>
        </div>
    </div>
</section>

<section class="device-section">
    <div class="container">
        <div class="row">
            @if(@$stepPro['name'] == 'Defects')
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" style="padding-left: 0px;">
               <h2 class="title Permit_Not" style="color: #767676 !important;font-weight: 600 !important; font-size: 24px;"> Specify Device Condition</h2>
           </div>
           <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" style="padding-left: 0px;">
               <h2 class="title Permit">Permitted  device conditions </h2>
           </div>
           @endif

           <div class="row">           
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <div class="row-one list" style="    margin-bottom: 0px;">
                    @if(count($stepPro)>0)
                    @php $var = 0;  @endphp
                    @foreach($stepPro['item'] as $val)
                    @if($val->allow == 'allow')

                    <div class="box-1 a" id="{{$val->option_id}}">
                        <div class="checked" style="float:right;">
                            <img src="{{asset('product/checked-icon.png')}}" style="display:none;position:relative;" class ="url_img">
                        </div>  
                        <div class="Processors">{{ $val->name}}</div>
                        <div class="brand-title" style="margin-bottom:3px;">
                            @if($val->description != '')
                            <i class="fa fa-question-circle" name="{{$val->option_id}}"></i>
                            @endif
                        </div>
                    </div> 
                    @else
                    @php $var = 1;  @endphp

                    @endif 
                    @endforeach
                  
                </div>
                @if(@$stepPro['name'] == 'Defects')
                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" style="padding-left: 0px;">
                   @if($var)
                   <h2 class="title Permit_Not">Non Permitted  device conditions </h2>
                   @endif
               </div>
               @endif
               <div class="row-one disable_none">
                @foreach($stepPro['item'] as $val)
                @if($val->allow == 'not_allow')
                <div class="box-1 a not" id="{{$val->option_id}}" style="border:2px solid red;" vals="{{$val->option_id}}">

                    <div class="Processors">{{ $val->name}}</div>
                    <div class="brand-title" style="margin-bottom:3px;">
                        @if($val->description != '')
                        <i class="fa fa-question-circle" name="{{$val->option_id}}"></i>
                        @endif
                    </div>
                </div>
                @endif  
                @endforeach

            </div>
            @else
              <input type="hidden" id="hidden" value="1" name="">
             @endif
            @if(count($stepPro)>0)
            <div class="row-one" style="text-align: right;" id='model_next'>
                <button type="button" class="btn btn-checkout next" style="background-color: #56bdf1; color: #FFF;font-size: 25px; padding: 5px 40px;border-radius: 0px;">NEXT</button>
            </div>
            @endif
        </div>
    </div>
</div>
</div>
</section>
</div>
<input type="hidden" name="Defects" class="Defects">
<input type="hidden" name="Services" class="allServicess">
<input type="hidden" name="siteurl" id="siteurl" value="{{ URL::to('/') }}">
<input type="hidden" name="site" id="site" value="{{ URL::to('product/') }}">
</form>
<!--==============================Modal Popup=======================-->
<div class="modal modal-success fade in" id="modal-bootstrap-question" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-dialog" role="document" style="margin: 150px auto;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <!-- <h4 class="modal-title"><span class="username"></span></h4> -->
            </div>
            <div class="modal-body" style="height: auto;overflow: auto;">
                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                    <div class="col-sm-4 col-md-4 col-sm-offset-4 col-md-offset-4">
                        <div class="row-one">
                            <div class="box-1">
                                <div class="Processors">
                                    Defect type
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12" id="descriptionDefect">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--==============================Modal Popup=======================--> 
<!--==============================Modal Popup Validation=======================-->
<div class="modal modal-success fade in" id="modal-bootstrap-validation" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-dialog" role="document" style="margin: 150px auto;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Alert <span class="username"></span></h4>
            </div>
            <div class="modal-body" id="modal-body" style="color:red;">

            </div>
        </div>
    </div>
</div>
<!--==============================Modal Popup Validation=======================-->

<!--==============================Modal Popup=======================-->
<div class="modal modal-success fade in" id="modal-bootstrap" style="display: none;">
    <div class="modal-dialog" role="document" style="margin: 150px auto;">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #56bdf1;">
                <button type="button" id="close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title">Warning</h4>
            </div>
            <div class="modal-body" style="height: auto;overflow: auto;">
                <h4 style="color: #00B1E1;">We are not able to purchase this device based on the speficied configuraion you have entered !!</h4>
            </div>
        </div>
    </div>
</div>
<!--==============================Modal Popup=======================--> 
@section('js')
<script src="{{ asset('js/serial_number.js') }}"></script>
<script type="text/javascript">
    $('.fa-question-circle').click(function (e) {
        e.stopPropagation();
        $('#loadingDiv').css('display', 'block');
        var id = $(this).attr('name');
        var SITE = $('#siteurl').val();
        $.ajax({
            type: 'get',
            url: SITE + '/hpUser/info',
            data: {'id': id},
            dataType: 'JSON',
            success: function (obj) {
                $('#descriptionDefect').html(obj.description);
                $('#modal-bootstrap-question').modal('show');
                $('#loadingDiv').css('display', 'none');
            },
            error: function (error) {
                $('#descriptionDefect').html('<span style="color:red;">Something went to wrong!!!<span>');
                $('#modal-bootstrap-question').modal('show');
                $('#loadingDiv').css('display', 'none');
            }
        });
    });
    if($('#hidden').val() == 1){
        $('#modal-bootstrap').modal('show');

    }
    $('.close').click(function(){
        history.back();
    })
</script>

@endsection

@endsection

