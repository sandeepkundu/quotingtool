@extends('layouts.hpUser')
@section('content')

<link href="{{ asset('assets/global/plugins/bower_components/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/global/plugins/bower_components/animate.css/animate.min.css' )}}" rel="stylesheet">
<link href="{{ asset('assets/global/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet">
<link href="{{ asset('assets/global/plugins/bower_components/jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/global/plugins/bower_components/chosen_v1.2.0/chosen.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/select.css') }}" rel="stylesheet">
@php  
use App\Http\Controllers\HpUsersController;
@$Theme = HpUsersController::Theme_Cookie(); 
$cssfile = asset('css/stepsStore.css');
if(isset($Theme)){
	if(!empty($Theme->f5_user)) {
		$folder = public_path() . '/' . @$Theme->theme_folder . '/f5_user.css';
		if(file_exists($folder)){
			$cssfile = asset('/'). @$Theme->theme_folder . '/f5_user.css';
		}
	}
}
@endphp

<section class="dashboard" style="padding: 30px 0px; background-image:none!important; background:#f1f4f5;width:100%;height: 100% !important;">
    <div class="container">
        <h2 class="text-center">Edit User: {{$user->first_name}} {{$user->last_name}}</h2>
        
        <div class="panel rounded shadow">
            <div class="panel-heading">

            </div>

            <div class="panel-body no-padding">
                <form class="form-horizontal mt-10 form-add-edit-user" id="commentForm" method="post" action="{{URL::to('/')}}/updateUser" >
                    {{ csrf_field() }}


                    <input class=" form-control" id="name" value="{{ $user->id }}" name="id" minlength="2" type="hidden" required />
                    <div class="form-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="first_name" class="col-sm-4 control-label">First Name<span style="color:red;">*</span></label>
                                    <div class="col-sm-8">
                                        <input value="{{$user->first_name}}" class=" form-control" id="first_name" name="first_name" minlength="2" type="text" placeholder="First Name"> 
                                        @if ($errors->has('first_name'))
                                        <span class="help-block" style="color:red;">
                                            <strong>{{ $errors->first('first_name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="last_name" class="col-sm-4 control-label">Last Name<span style="color:red;">*</span></label>
                                    <div class="col-sm-8">
                                        <input value="{{$user->last_name}}" class=" form-control" id="last_name" name="last_name" minlength="2" type="text" placeholder="Last Name"> 
                                        @if ($errors->has('last_name'))
                                        <span class="help-block" style="color:red;">
                                            <strong>{{ $errors->first('last_name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="company" class="col-sm-4 control-label">Company Name<span style="color:red;">*</span></label>
                                    <div class="col-sm-8">
                                        <input value="{{$user->company}}" type="text" class="form-control" name="company"id="company"
                                        placeholder="Company Name" >
                                        @if ($errors->has('company'))
                                        <span class="help-block" style="color:red;">
                                            <strong>{{ $errors->first('company') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="email" class="col-sm-4 control-label">Email Address<span style="color:red;">*</span></label>
                                    <div class="col-sm-8">
    
                                        <div class="input-group mb-15">
                                            <span class="input-group-addon bg-primary">@</span>
                                            <input value="{{$user->email}}" type="email" name="email" class="form-control" id="email"
                                            placeholder="Email Address" disabled>
                                        </div>
                                        @if ($errors->has('email'))
                                        <span class="help-block" style="color:red;">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password" class="col-sm-4 control-label">Password</label>
                                    <div class="col-sm-8">
                                        <input  class=" form-control" id="password" name="password" value="{!!old('password') !!}"minlength="2" type="password" placeholder="Password" >
                                        @if ($errors->has('password'))
                                        <span class="help-block" style="color:red;">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="phone" class="col-sm-4 control-label">Phone Number<!-- <span style="color:red;">*</span> --></label>
                                    <div class="col-sm-8">
                                        <input value="{{$user->phone }}" type="text" class="form-control" name="phone" id="phone"
                                        placeholder="Phone Number">
                                        @if ($errors->has('phone'))
                                        <span class="help-block" style="color:red;">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>    
                            </div><!-- /.col -->    
                            <div class="col-sm-6">
                                                            
                                <div class="form-group">
                                    <label for="client" class="col-sm-4 control-label">
                                        Clients </label>
                                    <div class="col-sm-8">
                                            
                                        <select id="demo" name="select[]" multiple="multiple">
                                                @foreach($Clients as $Client)
                                                
                                            <option value="{{$Client->id}}" {{in_array($Client->id,$abc) ? "selected":""}}>{{$Client->name}}</option>
                                                @endforeach
                                        </select>
    
                                    </div>
    
                                    @if ($errors->has('select'))
                                    <span class="help-block" style="color:red;">
                                        <strong>{{ $errors->first('client_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="description" class="col-sm-4 control-label">
                                        Permissions
                                        <span style="color:red;">*</span>
                                    </label>
    
                                    <?php 
    
                                        if(!empty($user->permission)){
        
                                         $permission = $user->permission;
        
                                         $arrs = explode(",",$permission);
        
                                         $per = array();
        
                                         foreach($arrs as $arr){
                                            $per[] = $arr;
                                        }
                                    }   
        
                                    ?>
                                
                                    <div class="col-sm-8">
                                
                                        <div class="ckbox ckbox-info checkbox">
                                            <label for="checkbox-info1" style="color: #767676;font-size: 15px;font-weight: 600;"><input id="checkbox-info1" value="1" type="checkbox" name="permission[]" class="pull-right" {{ @(in_array('1',@$per)) ? "checked":""}}> Financial Display</label>
                                        </div>
                                        <div class="ckbox ckbox-info checkbox">
                                            <label for="checkbox-info2" style="color: #767676;font-size: 15px;font-weight: 600;"><input id="checkbox-info2" value="2" type="checkbox" name="permission[]" class="pull-right" {{ @(in_array('2',@$per)) ? "checked":""}}> Financial Management</label>
                                        </div>
                                        <div class="ckbox ckbox-info checkbox">
                                        
                                            <label for="checkbox-info3" style="color: #767676;font-size: 15px;font-weight: 600;"><input id="checkbox-info3"  value="3" type="checkbox" name="permission[]" class="pull-right" {{ @(in_array('3',@$per)) ? "checked":""}}> Collection Request</label>
                                        </div>
    
                                        @if ($errors->has('permission'))
                                        <span class="help-block" style="color:red;">
                                            <strong>{{ $errors->first('permission') }}</strong>
                                        </span>
                                        @endif  
                                        
                                    </div>
                                </div>
                                <div class="form-group">
                                            <label class="col-sm-4 control-label">Enabled
                                            <!-- <br/><span>(Active/Not Active)</span> --></label>
                                            <div class="col-sm-8">
                                                <div class="ckbox ckbox-info checkbox">
                                                    
                                                    <label for="checkbox-info1"><input id="checkbox-info1" {{($user->active == '1') ? 'checked' : ''}} type="checkbox" name="active"></label>
                                                </div>
                                            </div>
                                        </div>
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                        
                    </div><!-- /.form-body -->

                    <div class="form-footer">
                        <div class="col-sm-offset-5">
                            <button class="btn btn-success btn-color btn-hover" type="submit">Submit</button>
                            <a class="btn btn-danger reset" href="{{URL::to('/')}}/dashboard">Cancel</a>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</section>
@section('js')

<script src="{{ asset('assets/global/plugins/bower_components/chosen_v1.2.0/chosen.jquery.min.js') }}"></script>
<script src="{{ asset('assets/global/plugins/bower_components/jquery-autosize/jquery.autosize.min.js') }}"></script>
<script src="{{ asset('assets/global/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('assets/global/plugins/bower_components/jasny-bootstrap-fileinput/js/jasny-bootstrap.fileinput.min.js') }}"></script>
<script src="{{ asset('assets/global/plugins/bower_components/holderjs/holder.js') }}"></script>
<script src="{{ asset('assets/global/plugins/bower_components/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>
<script src="{{ asset('css/password.js') }}"></script>
<script src="{{ asset('js/ClientTree.js') }}"></script>
<script src="{{ asset('js/select.js') }}"></script>

<script type="text/javascript">

    $(document).ready(function () {
       $('#demo').searchableOptionList();
       
       $("#client_id").chosen();
       
       $('#client_id').on('change', function (evt, params) {
        var id = "";
        $('.search-choice a').each(function () {
            id = id + $(this).data('option-array-index') + ",";
        });
        $('[name="client_id"]').val(id);
    });

   });

    window.onload = function () {
        var id = "";
        $('.search-choice a').each(function () {
            id = id + $(this).data('option-array-index') + ",";
        });
        $('[name="client_id"]').val(id);
    };

</script>
<script type="text/javascript">

    $('#password').password({
        // custom messages
        shortPass: 'The password is too short',
        badPass: 'Weak; try combining letters & numbers',
        goodPass: 'Medium; try using special charecters',
        strongPass: 'Strong password',
        containsUsername: 'The password contains the username',
        enterPass: 'Type your password',
        // show percent
        showPercent: true,
        // show text
        showText: true,
        // enable animation
        animate: true,
        animateSpeed: 'fast',
        // link to username
        email: false,
        usernamePartialMatch: true,
        // minimum length
        minimumLength: 1

    });



</script>

@endsection
<style type="text/css">
    .pass-graybar {
        height: 3px;
        background-color: red;
        width: 100%;
        position: relative;
    }

    .pass-colorbar {
        height: 3px;
        background-image: url(passwordstrength.jpg);
        position: absolute;
        background-color: green;
        top: 0;
        left: 0;
    }

    .pass-percent, .pass-text {
        font-size: 1em;
    }

    .pass-percent {
        margin-right: 5px;
    }

</style>

@endsection
