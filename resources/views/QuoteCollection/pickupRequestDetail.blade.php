@extends('layouts.default') @section('content')
<style>
    @media print {
      body * {
        visibility: hidden;
      }
      #section-to-print, #section-to-print * {
        visibility: visible;
      }
      #section-to-print {
        position: absolute;
        left: 0;
        top: 0;
      }
    }
</style>
<!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-table"></i>Pickup Request Detail<span></span></h2>
</div><!--/ End page header -->
<div class="body-content animated fadeIn">
    <!-- Start body content -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-tab panel-tab-double shadow">
                <div class="panel-body no-padding">
                    <div class="panel panel-default shadow no-margin">
                        <div class="panel-heading">
                            <button class=" btn btn-default" id="print" onclick="myFunction();"><i class="fa fa-print" aria-hidden="true" data-toggle="tooltip" title="Print"></i></button> 
                            <div class="clearfix"></div>
                        </div><!--/.panel-heading -->
                        <!--Panel Body-->
                        <div class="panel-body" id="section-to-print">
                        <div class="col-sm-12 col-md-12" style="padding-bottom: 20px;">
                            <table border="0" cellpadding="10">
                            <tr>
                                <td>User Name : </td>
                                <td>{{ @$quotes[0]->Username->first_name }} {{ @$quotes[0]->Username->last_name }}</td>
                            </tr>
                            <tr>
                                <td>Client Name : </td>
                                <td>{{@$quotes[0]->Clientname->name}}</td>
                            </tr>
                        </table>
                        </div>
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="tab-client-all">
                                    <!-- Start datatable -->
                                    <div id="datatable-client-all_wrapper" class="dataTables_wrapper form-inline">
                                        <?php $i=1; ?>
                                        <table id="datatable-ajax" class="table table-striped table-success">
                                            <thead>
                                                <tr>
                                                    <th>S.No.</th>
                                                    <th>Quote Name</th>
                                                    <th>Client Name</th>
                                                    <th>Product</th>
                                                    <th>Brand</th>
                                                    <th>Processor</th>
                                                    <th>Screen</th>
                                                    <th>RAM</th>
                                                    <th>DVD</th>
                                                    <th>HDD</th>
                                                    <th>Services</th>
                                                    <th>Date Generated</th>
                                                    <th>Quantity</th>
                                                    <th>Value</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(count($quotes) > 0)
                                                @foreach($quotes as $quote)
                                                <?php 
                                                $a[] = @$quote->total_value;
                                                $Quantity[] = (isset($quote->QuoteItem->quantity))?$quote->QuoteItem->quantity : 0;
                                                ?>
                                                <tr>
                                                    <td>{{ @$i++ }}</td>
                                                    <td>{{ @$quote->name }}</td>
                                                    <td>{{ @$quote->Clientname->name }}</td>
                                                    <td>{{ isset($quote->QuoteItem->type_name)?$quote->QuoteItem->type_name:'N/A' }}</td>
                                                    <td>{{ isset($quote->QuoteItem->brand_name)?$quote->QuoteItem->brand_name:'N/A' }}</td>
                                                    <td>{{ isset($quote->QuoteItem->processor_name)?$quote->QuoteItem->processor_name:'N/A' }}</td>
                                                    <td>{{ isset($quote->QuoteItem->screen_name)?$quote->QuoteItem->screen_name:'N/A' }}</td>
                                                    <td>{{ isset($quote->QuoteItem->ram_name)?$quote->QuoteItem->ram_name:'N/A' }}</td>
                                                    <td>{{ isset($quote->QuoteItem->dvd_name)?$quote->QuoteItem->dvd_name:'N/A' }}</td>
                                                    <td>{{ isset($quote->QuoteItem->hdd_name)?$quote->QuoteItem->hdd_name:'N/A' }}</td>
                                                    <td>{{ isset($quote->QuoteItem->services_name)?$quote->QuoteItem->services_name:'N/A' }}</td>
                                                    <td>{{ isset($quote->QuoteItem->date_created)?$quote->QuoteItem->date_created:'N/A' }}</td>
                                                    <td>{{ isset($quote->QuoteItem->quantity)? $quote->QuoteItem->quantity :'N/A' }}</td>
                                                    <td>{{$quote->QuoteItem->total_price}}</td>
                                                </tr>
                                                @endforeach


                                                <tr style="border-bottom: 1px;">
                                                    <th colspan="12" style="text-align: right;"> Total:</th>
                                                    <th> {{array_sum($Quantity)}}</th>
                                                    <th align="right">${{array_sum($a)}}</th>
                                                </tr>

                                                @endif
                                            </tbody>
                                          <!--   <tfoot>
                                                <th>S.No.</th>
                                                <th>Quote Name</th>
                                                <th>Client Name</th>
                                                <th>Product</th>
                                                <th>Brand</th>
                                                <th>Processor</th>
                                                <th>Screen</th>
                                                <th>RAM</th>
                                                <th>DVD</th>
                                                <th>HDD</th>
                                                <th>Services</th>
                                                <th>Date Generated</th>
                                                <th>Quantity</th>
                                                <th>Value</th>
                                            </tfoot> -->
                                        </table>
                                    </div>
                                    <!--/ End datatable -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@section('js')
<script>
    function myFunction() {
            window.print();
    }
</script>
@endsection
@endsection

