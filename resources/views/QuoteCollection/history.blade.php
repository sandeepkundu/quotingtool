@extends('layouts.default') @section('content')
<!-- START @PAGE CONTENT -->
@if(isset($toast['message']))
{{ $toast['message'] }}
@endif
@section('css')
<!-- START @PAGE LEVEL STYLES -->
<link href="{{asset('assets/global/plugins/bower_components/datatables/css/dataTables.bootstrap.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/bower_components/datatables/css/datatables.responsive.css')}}" rel="stylesheet">
<!--/ END PAGE LEVEL STYLES -->

@endsection
<style>
    .table-success tbody tr td .sorting_1 {
        background: red !important;
        color: white;
        border-bottom: 1px solid red !important;
    }
    .dataTable thead tr th:first-child {
        min-width: 10px !important;
    }
    .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
        background-color: #178aa9 !important;
        border: 1px solid #178aa9;
    }
    table.dataTable thead th {
        position: relative;
        background-image: none !important;
    }

    table.dataTable thead th.sorting:after,
    table.dataTable thead th.sorting_asc:after,
    table.dataTable thead th.sorting_desc:after {
        position: absolute;
        top: 12px;
        right: 8px;
        display: block;
        font-family: FontAwesome;
    }
    table.dataTable thead th.sorting:after {
        content: "\f0dc";
        color: #ddd;
        font-size: 0.8em;
        padding-top: 0.12em;
    }
    table.dataTable thead th.sorting_asc:after {
        content: "\f0de";
    }
    table.dataTable thead th.sorting_desc:after {
        content: "\f0dd";
    }
        .coll_sub_btn{
        background-color: #56bdf1 !important;
        border: solid 1px #56bdf1;
        border-radius: 0px;
        color: #fff;
    }
    .coll_sub_btn:hover{
        background-color: white !important;
        border: solid 1px #56bdf1;
        border-radius: 0px;
        color: #56bdf1;
    
</style>
<!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-table"></i>@if(!empty($ClientName))
                                {{@$ClientName->name}}
                                @endif  <span></span></h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label"></span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="{{URL::to('/')}}/admin/dashboard">Dashboard</a>
                <!-- <i class="fa fa-angle-right"></i>
            -->
        </li>
        <li>
                <!-- Countries
                <i class="fa fa-angle-right"></i>
                index -->
            </li>
            <li class="active">
            </li>
        </ol>
    </div><!-- /.header-content -->
</div><!--/ End page header -->
<div class="body-content animated fadeIn">
    <!-- Start body content -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-tab panel-tab-double shadow">
                <div class="panel-body no-padding">
                    <div class="panel panel-default shadow no-margin">
                        <div class="panel-heading">
                        <a href="{{URL::to('/')}}/QuoteCollection/index" style="margin-left:5px;margin-right: -4px;" class="btn btn-color btn-hover"><i class="glyphicon glyphicon-chevron-left"></i>&nbsp;Back</a>
                        
                            <a  href="{{URL::to('/')}}/QuoteCollection/history/{{$id}}" class="btn btn-default tooltips" data-toggle="tooltip" data-placement="top" data-title="Reload" data-action="refresh" data-original-title="" title="" id="ml5"><i class="icon-refresh icons"></i></a>
                        
                            <div class="clearfix"></div>
                        </div><!--/.panel-heading -->
                        <!--Panel Body-->
                        <div class="panel-body">
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="tab-client-all">
                                    <!-- Start datatable -->
                                    <div id="datatable-client-all_wrapper" class="dataTables_wrapper form-inline">
                                         <form action="{{URL::to('/') }}/QuoteCollection/collection_status" method="post" class="multipleDeleteForm">

                                        {{ csrf_field() }}
                                        <input type="hidden" name="client_id" value="{{@$ClientName->id}}">
                                           
                                            <table id="datatable-ajax" class="table table-striped table-success">

                                                <thead>
                                                    <tr role="row">
                                                        <th><input type="checkbox" class="multiIdCheck"/></th>
                                                        <th>Image</th>
                                                        <th>Serial Number</th>
                                                        <th>Model Number</th>
                                                        <th>Device</th>
                                                        <th>Brand</th>
                                                        <th>Processor</th>
                                                        <th>Screen</th>
                                                    </tr>
                                                </thead>
                                                <!--tbody section is required-->
                                                <tbody>

                                                  
                                        @if(count($QuoteItems)>0)
                                        @foreach($QuoteItems as $QuoteItem)

                                        <tr>
                                            <td>
                                                <input type="checkbox" class="id" name="ids[]" value="{{$QuoteItem->QI_ID}}">
                                            </td>

                                            <td>@if(!empty($QuoteItem->image1))
                                                <img width="60" height="60" src="{{url('/')}}{{@$QuoteItem->image1}}">
                                                @elseif(!empty($QuoteItem->image2))
                                                 <img width="60" height="60" src="{{url('/')}}{{@$QuoteItem->image2}}">
                                                @elseif(!empty($QuoteItem->image3))
                                                <img width="60" height="60" src="{{url('/')}}{{@$QuoteItem->image3}}">
                                                @elseif(!empty($QuoteItem->m_image))
                                                <img width="60" height="60" src="{{url('/')}}{{@$QuoteItem->m_image}}">
                                                @elseif(!empty($QuoteItem->productImage))
                                                <img width="60" height="60" src="{{url('/')}}/product/{{@$QuoteItem->productImage}}">
                                                @else
                                                N/A
                                                @endif
                                            </td>
                                            <td>
                                                @if(!empty($QuoteItem->serial_number))
                                                {{@$QuoteItem->serial_number}}
                                                @else
                                                N/A
                                                @endif
                                            </td>
                                            <td>
                                                @if(!empty($QuoteItem->model))
                                                  {{@$QuoteItem->model}}    
                                                @else
                                                    N/A
                                                @endif
                                              
                                            </td>
                                            <td>
                                                @if(!empty($QuoteItem->type_name))
                                                  {{@$QuoteItem->type_name}}
                                                @else
                                                    N/A
                                                @endif
                                            </td>
                                            <td>
                                                @if(!empty($QuoteItem->brand_name))
                                                 {{@$QuoteItem->brand_name}}
                                                @else
                                                    N/A
                                                @endif                                                
                                            </td>
                                            <td>
                                                @if(!empty($QuoteItem->processor_name))
                                                   {{@$QuoteItem->processor_name}}
                                                @else
                                                    N/A
                                                @endif   

                                            </td> 
                                            <td>
                                                @if(!empty($QuoteItem->screen_name))
                                                  {{@$QuoteItem->screen_name}}
                                                @else
                                                    N/A
                                                @endif  
                                               
                                            </td>
                                            
                                        </tr>
                                        @endforeach
                                    @else
                                        <tr><td colspan="5" style="color:red;">No records found!!!</td></tr>
                                    @endif
                                            </tbody>
                                            <!--tfoot section is optional-->
                                            <tfoot>
                                                <tr role="row">
                                                    <th><input type="checkbox" class="multiIdCheck"/></th>
                                                    <th>Image</th>
                                                    <th>Serial Number</th>
                                                    <th>Model Number</th>
                                                    <th>Device</th>
                                                    <th>Brand</th>
                                                    <th>Processor</th>
                                                    <th>Screen</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        <div class="col-sm-12">
                                        <span class="error" style="color: red;"></span>
                                            <input type="button" value="submit" onclick="multiId();" class="btn pull-right coll_sub_btn">
                                        </div>
                                    </form>
                                </div>
                                <!--/ End datatable -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@section('js')
<script>
    $(document).ready(function () {
        $('#datatable-ajax').DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1, -7] /* 1st one, start by the right */
            }],
            language: {
                searchPlaceholder: "Search records"
            },
            "lengthMenu": [[30, 60, 90, -1], [30, 60, 90, "All"]],
            "order": [[3, "asc"]],
            "columnDefs": [{
                "targets": 0,
                "orderable": false,
                'min-width': '10px',
                "width": "5%"
            }],
            "bSortClasses": false,
            "searchHighlight": true,
        });

    });

    function multiId() {
        var checkboxcount = jQuery('.id:checked').length;
        if (checkboxcount != 0) {
            jQuery('.multipleDeleteForm').submit();
        } else {
            $('.error').text('Please select a record');
            return false;
        }
    }

</script>
<!-- START @PAGE LEVEL PLUGINS -->
<script src="{{asset('assets/global/plugins/bower_components/datatables/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/global/plugins/bower_components/datatables/js/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('assets/global/plugins/bower_components/datatables/js/datatables.responsive.js')}}"></script>
<!--/ END PAGE LEVEL PLUGINS -->
@endsection
@endsection

