@extends('layouts.default') @section('content')
<!-- START @PAGE CONTENT -->
@if(isset($toast['message']))
{{ $toast['message'] }}
@endif
@section('css')
<!-- START @PAGE LEVEL STYLES -->
<link href="{{asset('assets/global/plugins/bower_components/datatables/css/dataTables.bootstrap.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/bower_components/datatables/css/datatables.responsive.css')}}" rel="stylesheet">
<!--/ END PAGE LEVEL STYLES -->

@endsection
<style>
    .table-success tbody tr td .sorting_1 {
        background: red !important;
        color: white;
        border-bottom: 1px solid red !important;
    }
    .dataTable thead tr th:first-child {
        min-width: 10px !important;
    }
    .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
        background-color: #178aa9 !important;
        border: 1px solid #178aa9;
    }
    table.dataTable thead th {
        position: relative;
        background-image: none !important;
    }

    table.dataTable thead th.sorting:after,
    table.dataTable thead th.sorting_asc:after,
    table.dataTable thead th.sorting_desc:after {
        position: absolute;
        top: 12px;
        right: 8px;
        display: block;
        font-family: FontAwesome;
    }
    table.dataTable thead th.sorting:after {
        content: "\f0dc";
        color: #ddd;
        font-size: 0.8em;
        padding-top: 0.12em;
    }
    table.dataTable thead th.sorting_asc:after {
        content: "\f0de";
    }
    table.dataTable thead th.sorting_desc:after {
        content: "\f0dd";
    }
</style>
<!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-table"></i>COLLECTION REQUEST<span></span></h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label"></span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="{{URL::to('/')}}/admin/dashboard">Dashboard</a>
                <!-- <i class="fa fa-angle-right"></i>
            -->
        </li>
        <li>
                <!-- Countries
                <i class="fa fa-angle-right"></i>
                index -->
            </li>
            <li class="active">
            </li>
        </ol>
    </div><!-- /.header-content -->
</div><!--/ End page header -->
<div class="body-content animated fadeIn">
    <!-- Start body content -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-tab panel-tab-double shadow">
                <div class="panel-body no-padding">
                    <div class="panel panel-default shadow no-margin">
                        <div class="panel-heading">
                            <a  href="{{URL::to('/')}}/QuoteCollection/index" class="btn btn-default tooltips" data-toggle="tooltip" data-placement="top" data-title="Reload" data-action="refresh" data-original-title="" title="" id="ml5"><i class="icon-refresh icons"></i></a>
                            <!-- <a href="javascript:void(0);" class="btn btn-danger tooltips" data-toggle="tooltip" data-placement="top" data-title="Delete" data-original-title="" title="" onclick="return multiId()" ><i class="icon-trash icons"></i></a>
                            <div class="pull-right" id="mt0"> -->
                                <a href="{{URL::to('/')}}/QuoteCollection/CollectionDetails" class="btn btn-color btn-hover" style="float: right;"></i> &nbsp;Collection Request</a>
                            </div>
                            <div class="clearfix"></div>
                        </div><!--/.panel-heading -->
                        <!--Panel Body-->
                        <div class="panel-body">
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="tab-client-all">
                                    <!-- Start datatable -->
                                    <div id="datatable-client-all_wrapper" class="dataTables_wrapper form-inline">
                                        <form action="{{URL::to('/') }}/QuoteCollection/multipleDelete" method="post" class="multipleDeleteForm">
                                            {{ csrf_field() }}
                                            <table id="datatable-ajax" class="table table-striped table-success">

                                                <thead>
                                                    <tr role="row">
                                                        <th style="width:10px !important;text-align:center;"><input type="checkbox" class="multiIdCheck"/></th>
                                                        <th>Client</th> 
                                                        <th>User</th> 
                                                        <th>Date</th> 
                                                        <th># Items</th> 
                                                        <th>Status</th> 
                                                        <th class="" style="width: 90px !important;">Actions</th>
                                                    </tr>
                                                </thead>
                                                <!--tbody section is required-->
                                                <tbody>

                                                  
                                                    @if (isset($Quotes) && count($Quotes) > 0) 
                                                      
                                                        @foreach ($Quotes as $Quote) 
                                                            
                                                            <tr>
                                                                <td style="text-align:center;">
                                                                    <input type="checkbox" class="id" name="ids[]" value="{{$Quote->id}}"/>
                                                                </td>

                                                                <td>
                                                                    @if(!empty($Quote->Clientname->name))
                                                                    {{$Quote->Clientname->name}}
                                                                    @else
                                                                    N/A
                                                                    @endif

                                                                </td> 
                                                                <td>
                                                                    @if(!empty($Quote->Username->first_name))

                                                                        {{@$Quote->Username->first_name}}

                                                                    @else
                                                                    N/A
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                  @if(!empty($Quote->date_modified))

                                                                  {{@$Quote->date_modified}}

                                                                  @else
                                                                  N/A
                                                                  @endif
                                                              </td> 

                                                              <td>
                                                                @if(!empty($Quote->name))
                                                                <!-- <a href="{{url('/')}}/hpUser/collection_history/{{$Quote->Clientname->id}}" style="font-weight: 600;" class="coll_sub_btn">
                                                                    
                                                                </a> -->
                                                                {{$Quote->name}}
                                                                @else
                                                                N/A
                                                                @endif    
                                                            </td> 
                                                            <td>
                                                                @if(!empty($Quote->status))
                                                                Collection Request  
                                                                @else
                                                                N/A
                                                                @endif    
                                                            </td>
                                                            <td>
                                                                <a class="btn btn-round btn-color btn-hover pull-left" href="{{URL::to('/')}}/QuoteCollection/history/{{$Quote->Clientname->id}}" style="padding: 0px 6px;">
                                                                    <i class="fa fa-edit"></i>
                                                                </a>

                                                               <!--  <a onclick="return confirm('Are you sure you want to delete?')" class="btn btn-round btn-danger"  style="padding: 0px 6px;" id="ml2" href="{{URL::to('/')}}/QuoteCollection/delete/{{$Quote->id}}">
                                                                    <i class="fa fa-trash-o"></i>
                                                                </a> -->


                                                            </td>

                                                        </tr>
                                                       
                                                    @endforeach
                                                @else 
                                                    <tr><td colspan="5" style="color:red;"> No records found!!!</td></tr>
                                                @endif

                                            </tbody>
                                            <!--tfoot section is optional-->
                                            <tfoot>
                                                <tr role="row">
                                                    <th style="width:10px !important;text-align:center;"><input type="checkbox" class="multiIdCheck"/></th>
                                                    <th>Client</th> 
                                                    <th>User</th> 
                                                    <th>Date</th> 
                                                    <th># Items</th> 
                                                    <th>Status</th> 
                                                    <th class="" style="width: 90px !important;">Actions</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </form>
                                </div>
                                <!--/ End datatable -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@section('js')
<script>
    $(document).ready(function () {
        $('#datatable-ajax').DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1, -7] /* 1st one, start by the right */
            }],
            language: {
                searchPlaceholder: "Search records"
            },
            "lengthMenu": [[30, 60, 90, -1], [30, 60, 90, "All"]],
            "order": [[3, "asc"]],
            "columnDefs": [{
                "targets": 0,
                "orderable": false,
                'min-width': '10px',
                "width": "5%"
            }],
            "bSortClasses": false,
            "searchHighlight": true,
        });

    });

    function multiId() {
        var checkboxcount = jQuery('.id:checked').length;
        if (checkboxcount != 0) {
            if (confirm("Are you sure you want to delete ?")) {
                jQuery('.multipleDeleteForm').submit();
            }
        } else {
            alert("Please select a record")
            return false;
        }
    }
</script>
<!-- START @PAGE LEVEL PLUGINS -->
<script src="{{asset('assets/global/plugins/bower_components/datatables/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/global/plugins/bower_components/datatables/js/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('assets/global/plugins/bower_components/datatables/js/datatables.responsive.js')}}"></script>
<!--/ END PAGE LEVEL PLUGINS -->
@endsection
@endsection

