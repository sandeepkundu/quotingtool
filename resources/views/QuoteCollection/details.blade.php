@extends('layouts.default') @section('content')
<!-- START @PAGE CONTENT -->
@if(isset($toast['message']))
{{ $toast['message'] }}
@endif
@section('css')
<!-- START @PAGE LEVEL STYLES -->
<link href="{{asset('assets/global/plugins/bower_components/datatables/css/dataTables.bootstrap.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/bower_components/datatables/css/datatables.responsive.css')}}" rel="stylesheet">
<!--/ END PAGE LEVEL STYLES -->

@endsection
<style>
    .table-success tbody tr td .sorting_1 {
        background: red !important;
        color: white;
        border-bottom: 1px solid red !important;
    }
    .dataTable thead tr th:first-child {
        min-width: 10px !important;
    }
    .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
        background-color: #178aa9 !important;
        border: 1px solid #178aa9;
    }
    table.dataTable thead th {
        position: relative;
        background-image: none !important;
    }

    table.dataTable thead th.sorting:after,
    table.dataTable thead th.sorting_asc:after,
    table.dataTable thead th.sorting_desc:after {
        position: absolute;
        top: 12px;
        right: 8px;
        display: block;
        font-family: FontAwesome;
    }
    table.dataTable thead th.sorting:after {
        content: "\f0dc";
        color: #ddd;
        font-size: 0.8em;
        padding-top: 0.12em;
    }
    table.dataTable thead th.sorting_asc:after {
        content: "\f0de";
    }
    table.dataTable thead th.sorting_desc:after {
        content: "\f0dd";
    }
    .coll_sub_btn,.changeStatuss{
    background-color: #56bdf1 !important;
    border: solid 1px #56bdf1;
    border-radius: 0px;
    color: #fff;
    }
    .coll_sub_btn:hover,.changeStatuss:hover{
        background-color: white !important;
        border: solid 1px #56bdf1;
        border-radius: 0px;
        color: #56bdf1;
    }
    
</style>
<!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-table"></i>@if(!empty($ClientName))
                                {{@$ClientName->name}}
                                @endif  <span></span></h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label"></span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="{{URL::to('/')}}/dashboard">Dashboard</a>
                <!-- <i class="fa fa-angle-right"></i>
            -->
        </li>
        <li>
                <!-- Countries
                <i class="fa fa-angle-right"></i>
                index -->
            </li>
            <li class="active">
            </li>
        </ol>
    </div><!-- /.header-content -->
</div><!--/ End page header -->
<div class="body-content animated fadeIn">
    <!-- Start body content -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-tab panel-tab-double shadow">
                <div class="panel-body no-padding">
                    <div class="panel panel-default shadow no-margin">
                        <div class="panel-heading">
                        <!-- <button onclick="history.back();" style="margin-left:5px;margin-right: -4px;" class="btn btn-color btn-hover"><i class="glyphicon glyphicon-chevron-left"></i>&nbsp;Back</button> -->

                            <a  href="{{URL::to('/')}}/QuoteCollection/CollectionDetails" class="btn btn-default tooltips" data-toggle="tooltip" data-placement="top" data-title="Reload" data-action="refresh" data-original-title="" title="" id="ml5"><i class="icon-refresh icons"></i></a>
                        
                            <div class="clearfix"></div>
                        </div><!--/.panel-heading -->
                        <!--Panel Body-->
                        <div class="panel-body">
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="tab-client-all">
                                    <!-- Start datatable -->
                                    <div id="datatable-client-all_wrapper" class="dataTables_wrapper form-inline">
                                            <table id="datatable-ajax" class="table table-striped table-success">

                                                <thead>
                                                    <tr role="row">
                                                        <th><input type="checkbox" class="multiIdCheck"/></th>
                                                        <th>Collection ID</th>
                                                        <th>Client</th>
                                                        <th>User</th>
                                                        <th>Date</th>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <!--tbody section is required-->
                                                <tbody>

                                                  
                                        @if(count($QuoteCollections)>0)
                                        @foreach($QuoteCollections as $QuoteCollection)

                                        <tr>
                                            <td>
                                                <input type="checkbox" class="id" name="ids[]" value="{{$QuoteCollection->id}}"/>
                                             </td>
                                            <td>
                                                @if(!empty($QuoteCollection->id))
                                                {{@$QuoteCollection->id}}
                                                @else
                                                N/A
                                                @endif
                                            </td>
                                            <td>
                                                @if(!empty($QuoteCollection->ClientName))
                                                  {{@$QuoteCollection->ClientName}}    
                                                @else
                                                    N/A
                                                @endif
                                              
                                            </td>
                                            <td>
                                                @if(!empty($QuoteCollection->Username))
                                                  {{@$QuoteCollection->Username}}
                                                @else
                                                    N/A
                                                @endif
                                            </td>
                                            <td>
                                                @if(!empty($QuoteCollection->date_collected))
                                                 {{@$QuoteCollection->date_collected}}
                                                @else
                                                    N/A
                                                @endif                                                
                                            </td>
                                            <td>
                                                @if(!empty($QuoteCollection->status))
                                                  <button id="{{$QuoteCollection->quote_id}}" name="{{$QuoteCollection->quote_status}}" value="{{$QuoteCollection->id}}" class="btn changeStatuss" type="button"> Awaited for collection</button>
                                                @else
                                                    N/A
                                                @endif   

                                            </td> 
                                            <td>
                                                <a href="{{url('/')}}/QuoteCollection/pickupRequestDetail?client_id={{$QuoteCollection->client_id}}">View / PDF</a>
                                                <!-- <a onclick="return confirm('Are you sure you want to delete?')" class="btn btn-round btn-danger"  style="padding: 0px 6px;" id="ml2" href="{{URL::to('/')}}/QuoteCollection/delete/{{$QuoteCollection->id}}">
                                                                    <i class="fa fa-trash-o"></i>
                                                                </a> -->
                                            </td>
                                        </tr>
                                        @endforeach
                                    @else
                                        <tr><td colspan="5" style="color:red;">No records found!!!</td></tr>
                                    @endif

                                            </tbody>
                                            <!--tfoot section is optional-->
                                            <tfoot>
                                                <tr role="row">
                                                    <th><input type="checkbox" class="multiIdCheck"/></th>
                                                    <th>Collection ID</th>
                                                    <th>Client</th>
                                                    <th>User</th>
                                                    <th>Date</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        
                                   
                                </div>
                                <!--/ End datatable -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div id="loadingDiv" style="display:none;">
    <div>
        <div class="loader"></div>
    </div>
</div>

<input type="hidden" name="siteurl" id="siteurl" value="{{URL::to('/')}}">
@section('js')
<script>
    $(document).ready(function () {
        $('#datatable-ajax').DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1, -7] /* 1st one, start by the right */
            }],
            language: {
                searchPlaceholder: "Search records"
            },
            "lengthMenu": [[30, 60, 90, -1], [30, 60, 90, "All"]],
            "order": [[3, "asc"]],
            "columnDefs": [{
                "targets": 0,
                "orderable": false,
                'min-width': '10px',
                "width": "5%"
            }],
            "bSortClasses": false,
            "searchHighlight": true,
        });

    });

    function multiId() {
        var checkboxcount = jQuery('.id:checked').length;
        if (checkboxcount != 0) {
            jQuery('.multipleDeleteForm').submit();
        } else {
            $('.error').text('Please select a record');
            return false;
        }
    }
    $(function () {
        $('.changeStatuss').click(function () {

        var QuoteId = $(this).attr('id');
        var statusId = $(this).attr('name');
        var collection_id = $(this).val();

        var SITE = $('#siteurl').val();
        if (statusId < 6) {
            $('#loadingDiv').css('display', 'block');
            $.ajax({
                type: 'get',
                url: SITE + '/hpUser/changeStatus',
                data: {'status': statusId, 'name': QuoteId,'collection_id':collection_id},
                dataType: 'JSON',
                success: function (obj) {
                    location.reload();
                },
                error: function (error) {
                    alert('error block');
                }
            });            
        }
    });
});
</script>
<!-- START @PAGE LEVEL PLUGINS -->
<script src="{{asset('assets/global/plugins/bower_components/datatables/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/global/plugins/bower_components/datatables/js/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('assets/global/plugins/bower_components/datatables/js/datatables.responsive.js')}}"></script>
<!--/ END PAGE LEVEL PLUGINS -->
@endsection
@endsection

