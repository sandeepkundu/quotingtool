@extends('layouts.default')
@section('content')
<style>
    .form-control:focus{
        border: 1px solid #66afe9 !important;
    }
    .bg-warning{
        background-color:#00B1E1 !important;
        border:1px solid #00B1E1 !important;
    }
    input.no-border-right:focus, textarea.no-border-right:focus{
        border: 1px solid #66afe9 !important;
    }
</style>
<div class="body-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">EDIT COLLECTION REQUEST</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body no-padding">
                    <form class="form-horizontal mt-10" id="commentForm" method="post" action="{{URL::to('/')}}/QuoteCollection/updateQuoteCollection">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{$QuoteCollection->id}}">
                        <div class="form-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="quote_id" class="col-sm-3 control-label">Quote<span style="color:red;">*</span></label>
                                            <div class="col-sm-9" style="padding-bottom:20px;">

                                                <select name="quote_id" class="form-control">
                                                    <option value="">--Select Quote--</option>
                                                    @foreach($Quotes as $Quote)
                                                    <option value="{{$Quote->id}}" {{($QuoteCollection->quote_id == $Quote->id) ? "selected":"" }}>
                                                        @if(isset($Quote->name))
                                                        {{$Quote->name}}
                                                        @else
                                                        Name Not Available
                                                        @endif
                                                    </option>
                                                    @endforeach
                                                </select>

                                                @if ($errors->has('quote_id'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('quote_id') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <label for="variance_to" class="col-sm-3 control-label">User<span style="color:red;">*</span></label>
                                            <div class="col-sm-9" style="padding-bottom:20px;">
                                                <select name="user_id" class="form-control">
                                                    <option value="">--Select User--</option>
                                                    @foreach ($User as $code)
                                                    <option value="{{ $code->id }}" {{($QuoteCollection->user_id == $code->id) ? "selected":"" }}>{{ $code->first_name }}&nbsp {{ $code->last_name }}</option>
                                                    @endforeach 
                                                </select>
                                                @if ($errors->has('user_id'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>The user field is required.</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="parent_id" class="col-sm-3 control-label">Clients</label>
                                            <div class="col-sm-9">
                                                <div class="btn-group hierarchy-select" data-resize="auto" id="example-one">
                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <span class="selected-label pull-left">&nbsp;</span>
                                                        <span class="caret"></span>
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                    </button>
                                                    <div class="dropdown-menu open">
                                                        <div class="hs-searchbox">
                                                            <input type="text" class="form-control" autocomplete="off">
                                                        </div>
                                                        <ul class="dropdown-menu inner store" role="menu">
                                                            <li data-value="0" data-level="1" class="aa">
                                                                <a href="#">Select Client</a>
                                                            </li>
                                                            @foreach($parent_ids as $val)

                                                            
                                                            <li class="par" data-value="{{$val['id']}}" data-level="{{$val['level']}}" {{ ($val['id']==$QuoteCollection->client_id)? 'data-default-selected=""' : '' }} >
                                                                <a href="#">{{$val['name']}}</a>
                                                            </li>
                                                            @endforeach
                                                        </ul>

                                                        <ul class="dropdown-menu inner enterprise" style="display: none;" role="menu" >
                                                            <li data-value="0" data-level="1" class="aa">
                                                                <a href="#">Select Client</a>
                                                            </li>
                                                        </ul>

                                                    </div>
                                                    <input class="hidden hidden-field" name="parent_id" readonly="readonly" value="0" aria-hidden="true" type="text"/>
                                                </div>			
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="Client" class="col-sm-3 control-label">Status<span style="color:red;">*</span></label>
                                            <div class="col-sm-9" style="padding-bottom:20px;">
                                                <select name="status" class="form-control">
                                                    @foreach($quote_status as $code)
                                                    <option value="{{$code->name}}" {{($QuoteCollection->status == $code->name) ? "selected":"" }}>{{$code->name}}</option>
                                                    @endforeach
                                                </select>

                                                @if ($errors->has('client_id'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('client_id') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                                <label for="value" class="col-sm-3 control-label">Email Sent Type<!--  <span style="color:red;">*</span> --></label>
                                            <div class="col-sm-9" style="padding-bottom:20px;">
                                                <input type="text" value="{{$QuoteCollection->email_sent}}" name="email_sent" class="form-control" placeholder="Email Sent"/>
                                                @if ($errors->has('email_sent'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('email_sent') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"></div>
                        </div>
                        <div class="form-footer">
                            <div class="col-sm-offset-5">
                                <button class="btn btn-color btn-hover" type="submit">Submit</button>
                                <a class="btn btn-danger" href="{{URL::to('/')}}/QuoteCollection/index">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<link href="{{asset('css/hierarchy-select.min.css')}}" rel="stylesheet">
<style type="text/css">
    #ave_collect_size{
        border-right:1px solid rgb(221, 221, 221) !important;
    }
    #ave_collect_size:focus{
        border-right:1px solid #66afe9 !important
    }
    #example-one{
        width: 100%;
    }
    .dropdown-menu li.active:hover a, .dropdown-menu li.active:focus a, .dropdown-menu li.active:active a{
        background-color:#00B1E1 !important;
    }
    .dropdown-menu li.active a{
        background-color:#00B1E1 !important;
    }
    .btn-default.dropdown-toggle.btn-default{
        background-color: white;
    }
    .btn-default.active.focus, .btn-default.active:focus, .btn-default.active:hover, .btn-default:active.focus, .btn-default:active:focus, .btn-default:active:hover, .open>.dropdown-toggle.btn-default.focus, .open>.dropdown-toggle.btn-default:focus, .open>.dropdown-toggle.btn-default:hover{
        background-color: white;
    }
    .dropdown-menu li > a:hover:before{
        border-left:3px solid #00B1E1;
    }
    .modal-success .modal-header {
        background-color: #00B1E1 !important;
        border:1px solid #00B1E1;
        border:none;
    }
    .modal-success .modal-header:before {
        content: ""; 
        border: 1px solid #00B1E1 !important;
    }
</style>
@section('js')
<script src="{{ asset('js/hierarchy-select.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function () {
    $('#example-one').hierarchySelect();

});
</script>
@endsection
@endsection