@extends('layouts.default') @section('content')

<?php 
require_once $_SERVER['DOCUMENT_ROOT']. "/app/Http/Controllers/Mobile_Detect.php";
use App\Http\Controllers\ReportController;
$rc = new ReportController;
?>
<style>
    .form-control:focus{
        border: 1px solid #66afe9 !important;
    }

    a:hover{
        text-decoration: none;
    }

</style>
@if(isset($toast['message']))
{{ $toast['message'] }}
@endif
<div class="header-content">
    <h2><i class="fa fa-table"></i>LOGIN REPORT <span></span></h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label"></span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="{{URL::to('/')}}/dashboard">Dashboard</a>
            </li>
            <li class="active">                	
            </li>
        </ol>
    </div><!-- /.header-content -->
</div><!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-tab panel-tab-double shadow">
                <div class="panel-body no-padding">
                    <div class="panel panel-default shadow no-margin">
                        <div class="panel-heading">
                            <a  href="{{URL::to('/')}}/reporting/logins" class="btn btn-default tooltips" data-toggle="tooltip" data-placement="top" data-title="Reload" data-action="refresh" data-original-title="" title="" id="ml5"><i class="icon-refresh icons"></i>
                            <div class="clearfix"></div>
                        </div>

                        <!--Panel Body-->
                        <div class="panel-body">
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="tab-client-all">
                                    <div id="datatable-client-all_wrapper" class="dataTables_wrapper form-inline">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="dataTables_length" id="datatable-client-all_length">
                                                    <label>
                                                        <select name="datatable-client-all_length" class="form-control input-sm" id="paginationPerPage">
                                                            <?php
                                                            $l = 30;
                                                            $total = $totalusers;
                                                            $loop = intval($total / $l);

                                                            if (($total % $l) != 0) {
                                                                $loop = $loop + 1;
                                                            }
                                                            ?>  
                                                            @for($i=1; $i<=$loop;$i++)
                                                            <option value="{{$i*$l}}" {{ (($i*$l)==$lim)? 'selected' : '' }}>{{$i*$l}}</option>
                                                            @endfor
                                                        </select> Records per page
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div id="datatable-client-all_filter" class="dataTables_filter">

                                                </div>
                                            </div>
                                        </div>

                                        <table class="table table-striped table-success table-middle table-project-clients " role="grid" aria-describedby="datatable-client-all_info">

                                            <thead>
                                            <tr role="row">
                                            <th class="sorting">Name</th>
                                            <th class="sorting">Email</th>
											<th class="sorting">Status</th>
                                            <th class="sorting">Login Date</th>
                                            <th class="sorting">Device</th>
											<th class="sorting">OS</th>
											<th class="sorting">Browser</th>
                                            <th class="sorting">IP Address</th>
                                            </tr>
                                            </thead>
                                            <tbody>                                           
                                            <?php
                                            if (count($users) > 0) {
                                                foreach ($users as $user) {
													$name = $user->first_name . ' ' . $user->last_name;
													$email = $user->email;
													$status = ($user->active == 1) ? 1 : 0;

													$anotheruser = 1;

													if (count($user->logins) > 0) {
														foreach ($user->logins as $login) {
															$useragent = $login->browser;
															$user_os        =   $rc->getOS($useragent);
															$user_browser   =   $rc->getBrowser($useragent);
															$device = "Desktop";

															$detect = new Mobile_Detect(null, $useragent); 
															if ( $detect->isMobile() ) { $device = "Mobile"; }
															if( $detect->isTablet() ){ 	$device = "Tablet"; } 

															if($anotheruser == 1){
															?>
															<tr role="row" style="border-top:2px solid black !important; border-bottom:0px !important">
															<td>{{$name}} </td>
															<td>{{$email}}</td>
															@if($status == 1)
															<td style='color:green'><span>&#10003</span></td>
															@else
															<td style='color:red;'><span>&#9747</span></td>
															@endif
															<?php } else {  ?>
															<tr role="row" style="border-bottom:0px !important"><td></td><td></td><td></td>
															<?php  } ?>
															<td>{{date('j M Y h:i:s a', strtotime($login->date_loggedin))}}</td>
															<td>{{$device}}</td>
															<td>{{$user_os}}</td>
															<td>{{$user_browser}}</td>
															<td>{{$login->ip_address}}</td> 
															</tr>
															<?php
															$anotheruser = 0;
														}
													} else {
														// no login info for user
													}
                                                }
                                            } else {
                                                echo '<tr role="row"><td colspan="8" style="color:red;">  No results found!!!</td></tr>';
                                            }
                                            ?>
                                            </tbody>
                                            <tfoot>
                                            <tr role="row">
                                            <th class="sorting">Name</th>
                                            <th class="sorting">Email</th>
											<th class="sorting">Status</th>
                                            <th class="sorting">Login Date</th>
                                            <th class="sorting">Device</th>
											<th class="sorting">OS</th>
											<th class="sorting">Browser</th>
                                            <th class="sorting">IP Address</th>
                                            </tr>
                                            </tfoot>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<form action="{{URL::to('/')}}/reporting/logins" method="get" class="navbar-form" id="formPerPage">
    <input type="hidden" value="" name="perPage" id="perPage">
</form>
<style>
    .pagination{
        float: right;
        margin:0px;
    }
    #tour-3{
        margin-bottom: 0px;
    }
    .padding_bottom{
        padding-bottom: 15px;
    }
</style>

@section('js')
<script type="text/javascript">
    $(document).ready(function () {
        $('#paginationPerPage').change(function () {
            $a = $(this).val();
            $('#perPage').val($a);
            $('#formPerPage').submit();
        });


        $(".statusChanges").change(function () {
            $('#filter').submit();
        });


    });



    function multiId() {
        var checkboxcount = jQuery('.id:checked').length;
        if (checkboxcount != 0) {
            if (confirm("Are you sure you want to delete ?")) {
                jQuery('.multipleDeleteForm').submit();
            }
        } else {
            alert("Please select a record")
            return false;
        }
    }

</script>
@endsection
@endsection