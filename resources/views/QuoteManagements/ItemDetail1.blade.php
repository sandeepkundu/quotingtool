<?php 
	use App\Http\Controllers\QuoteManagementsController;
	$abc = new QuoteManagementsController;
	$price = 0;
?>
<style type="text/css">
	tr td {text-align: center;}
	tr th {text-align: center;}
	tr td:first-child{ width:60%; }
	tr th:first-child{ width:60%; }
	tr td:first-child{ text-align: left !important; }
	tr th:first-child{ text-align: left !important; }
</style>
<table border="1" cellspacing="2" cellpadding="4" width="100%">
	<tr>
		<th>

    											<ul class="specs-list">
    											@if(!empty($quoteitem->model_name))
    											<li><label>Model:</label> {{$quoteitem->model_name}}</li>
    											@endif
    
    											@if(!empty($quoteitem['type_name']))
    											<li><label>Product:</label> {{$quoteitem['type_name']}}</li>
    											@endif
    
    											@if(!empty($quoteitem['brand_name']))
    											<li><label>Brand:</label> {{$quoteitem['brand_name']}}</li>
    											@endif
    
    											@if(!empty($quoteitem['processor_name']))
    											<li><label>Processor:</label> {{$quoteitem['processor_name']}}</li>
    											@endif
    
    											@if(!empty($quoteitem['screen_name']))
    											<li><label>Screen:</label> {{$quoteitem['screen_name']}}</li>
    											@endif

    											@if(!empty($quoteitem['dvd_name']))
    											<li><label>DVD:</label> {{$quoteitem['dvd_name']}}</li>
    											@endif
    
    											@if(!empty($quoteitem['hdd_name']))
    											<li><label>HDD:</label> {{$quoteitem['hdd_name']}}</li>
    											@endif
    
    											@if(!empty($quoteitem['ram_name']))
    											<li><label>RAM:</label> {{$quoteitem['ram_name']}}</li>
    											@endif  
    											<ul>	
</th>
		<th colspan="3"></th>
	</tr>
	<tr>
		<td>Model Price</td>
		<td></td>
		<td><?php if($quoteitem->is_model_price) { echo $abc->formatOutput($quoteitem->unit_price_base); } ?></td>
		<td><?php if($quoteitem->is_model_price) { echo $abc->formatOutput($quoteitem->unit_price_base, $a->currency_rate, $a->currency_symbol); } ?></td>
	</tr>
	<tr>
		<td>CPU Base Price (LCD Monitor default to Nil)</td>
		<td></td>
		<td><?php if(!$quoteitem->is_model_price) { echo $abc->formatOutput($quoteitem->unit_price_base); } ?></td>
		<td><?php if(!$quoteitem->is_model_price) { echo $abc->formatOutput($quoteitem->unit_price_base, $a->currency_rate, $a->currency_symbol); } ?></td>
	</tr>

	<?php $price = $quoteitem->unit_price_base; ?>
	<tr>
		<td>LCD Base Price (Only for LCD Monitor & All in One)</td>
		<td></td>
		<td ><?php echo $abc->formatOutput($quoteitem->unit_price_screen) ?></td>
		<td><?php if( $quoteitem->unit_price_screen != 0 ) {echo $abc->formatOutput($quoteitem->unit_price_screen, $a->currency_rate, $a->currency_symbol); } ?></td>
	</tr>

	<?php $price += $quoteitem->unit_price_screen; ?>
	<tr>
		<th>CPU + LCD Monitor</th>
		<th></th>
		<th><?php echo $abc->formatOutput($price); ?></th>
		<th><?php echo $abc->formatOutput($price, $a->currency_rate, $a->currency_symbol); ?></th>
	</tr>
	
	<?php 
	if( $quoteitem->unit_price_model != 0 ) { 
		$price += $quoteitem->unit_price_model; 
	}else if( $quoteitem->unit_margin_model != 0 ) { 
		$price = $price * ((100 + $quoteitem->unit_margin_model) / 100);
	}
	?>
	<tr>
		<th>Model Margin $</th>
		<th></th>
		<th><?php echo $abc->formatOutput($quoteitem->unit_price_model) ?></th>
		<th><?php  if( $quoteitem->unit_price_model != 0 ) {echo $abc->formatOutput($quoteitem->unit_price_model, $a->currency_rate, $a->currency_symbol); } ?></th>
	</tr>
	<tr>
		<th>Model Margin %</th>
		<th></th>
		<th>{{$quoteitem->unit_margin_model}}</th>
		<th></th>
	</tr>
	<tr>
		<th>Model Margin adjusted price</th>
		<th></th>
		<th><?php echo $abc->formatOutput($price); ?></th>
		<th><?php echo $abc->formatOutput($price, $a->currency_rate, $a->currency_symbol); ?></th>
	</tr>
	<tr>
		<td colspan="3"></td>
	</tr>

	<tr>
		<td>DVD Adjustment</td>
		<td></td>
		<td><?php echo $abc->formatOutput($quoteitem->unit_price_dvd) ?></td>
		<td></td>
	</tr>
	<tr>
		<td>RAM Adjustment</td>
		<td></td>
		<td><?php echo $abc->formatOutput($quoteitem->unit_price_ram) ?></td>
		<td></td>
	</tr>	
	<tr>
		<td>HDD Adjustment</td>
		<td></td>
		<td><?php echo $abc->formatOutput($quoteitem->unit_price_hdd) ?></td>
		<td></td>
	</tr>
	<?php $price += $quoteitem->unit_price_ram + $quoteitem->unit_price_hdd + $quoteitem->unit_price_dvd; ?>
	<tr>
		<th align="left">Base Price Before Margin adjustents</th>
		<th></th>
		<th><?php echo $abc->formatOutput($price); ?></th>
		<th><?php echo $abc->formatOutput($price, $a->currency_rate, $a->currency_symbol); ?></th>
	</tr>
	<tr>
		<td colspan="4"></td>
	</tr>
	<tr>
		<td>Screen Size adjustment</td>
		<td>{{$quoteitem->unit_margin_screen}}%</td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>Brand adjustment</td>
		<td>{{$quoteitem->unit_margin_brand}}%</td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>Country adjustment</td>
		<td>{{$quoteitem->unit_margin_country}}%</td>
		<td></td>
		<td></td>
	</tr>
	<?php 
	$m_cy = ($quoteitem->unit_margin_country != 0) ? ((100 + $quoteitem->unit_margin_country) / 100) : 1;
	$m_c  = ($quoteitem->unit_margin_customer != 0) ? ((100 + $quoteitem->unit_margin_customer) / 100) : 1;
	$m_b  = ($quoteitem->unit_margin_brand != 0) ? ((100 + $quoteitem->unit_margin_brand) / 100) : 1;
	$m_s  = ($quoteitem->unit_margin_screen != 0) ? ((100 + $quoteitem->unit_margin_screen) / 100) : 1;

	$price = $price * $m_cy * $m_c * $m_b * $m_s; ?>
	<tr>
		<td>Client adustment</td>
		<td>{{$quoteitem->unit_margin_customer}}%</td>
		<th></th>
		<th></th>
	</tr>
	<tr>
		<th>Margin Adjustment Price</th>
		<th></th>
		<th><?php echo $abc->formatOutput($price); ?></th>
		<th><?php echo $abc->formatOutput($price, $a->currency_rate, $a->currency_symbol); ?></th>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<th>PUP Tradein</th>
		<th>No</th>
		<th><?php echo $abc->formatOutput($quoteitem->unit_price_committed); ?></th>
		<th><?php echo $abc->formatOutput($quoteitem->unit_price_committed, $a->currency_rate, $a->currency_symbol); ?></th>
	</tr>
	<?php if($quoteitem->use_committed_price) { $price = $quoteitem->unit_price_committed; }  ?>
	<tr>
		<th>Higher of PUP or Calculated</th>
		<th></th>
		<th><?php echo $abc->formatOutput($price);  ?></th>
		<th><?php echo $abc->formatOutput($price, $a->currency_rate, $a->currency_symbol); ?></th>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<th>Defects - All in One	</th>
	</tr>
	@if(count($QuoteItemsDefects) > 0)
		@foreach($QuoteItemsDefects as $QuoteItemsDefect)
		<tr>
			<td>{{@$QuoteItemsDefect['defect_name']}}</td>
			<td>{{$abc->formatOutput($QuoteItemsDefect['defect_value'])}}</td>
			<td></td>
			<td></td>
			
		</tr>
		@endforeach
	@endif
	<tr>
		<th>Total Calculated Defects</th>
		<th></th>
		<th>{{$abc->formatOutput($quoteitem->unit_price_defects)}}</th>
		<th></th>
	</tr>

	<tr>
		<td>Maiximum Defects %</td>
		<td>{{ $quoteitem->unit_margin_defectcap}}%</td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<th>Maximum Defects $</th>
		<th></th>
		<th><?php echo $abc->formatOutput($quoteitem->unit_price_defectcap); ?></th>
		<th></th>
	</tr>
	<?php $def_price = ($quoteitem->unit_price_defects < $quoteitem->unit_price_defectcap) ? $quoteitem->unit_price_defects : $quoteitem->unit_price_defectcap; ?>
	<tr>
		<th colspan="2">Defect to charge (Lesser of calculated or cap)</th>
		<th><?php echo $abc->formatOutput($def_price); ?></th>
		<th><?php echo $abc->formatOutput($def_price, $a->currency_rate, $a->currency_symbol); ?></th>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<th>Net Price Product Price</th>
		<th></th>
		<th></th>
		<th></th>
	</tr>
	<tr>
		<?php $price = $quoteitem->unit_prod_price;  ?>

		<th>Product less Defects before OEM margin</th>
		<th></th>
		<th><?php echo $abc->formatOutput($price); ?></th>
		<th><?php echo $abc->formatOutput($price, $a->currency_rate, $a->currency_symbol); ?></th>

	</tr>
	<tr>
		<td>OEM Product Margin</td>
		<td></td>
		<td>{{ $quoteitem->unit_margin_oem_product}}%</td>
		<td></td>
	</tr>
	<?php $price = $quoteitem->unit_f5_price; ?>
	<tr>
		<th>Product less Defects after OEM margin</th>
		<th></th>
		<th><?php echo $abc->formatOutput($price); ?></th>
		<th><?php echo $abc->formatOutput($price, $a->currency_rate, $a->currency_symbol); ?></th>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<th>Services</th>
		<th></th>
		<th></th>
		<th></th>
	</tr>
	<tr>
		<td>Base Service Charge (Default or Client)</td>
		<td><?php echo $abc->formatOutput($quoteitem->services_base_price); ?></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>Services margin Adjustmanet</td>
		<td>{{$quoteitem->services_margin}}%</td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<th>TTG Service Charge</th>
		<th></th>
		<th><?php echo $abc->formatOutput($quoteitem->unit_f5_services); ?></th>
		<th><?php echo $abc->formatOutput($quoteitem->unit_f5_services, $a->currency_rate, $a->currency_symbol); ?></th>
	</tr>
	<tr>
		<td>OEM Services markup</td>
		<td>{{$quoteitem->services_oem_margin}}%</td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<th>Quoted Services Charge</th>
		<th></th>
		<th><?php echo $abc->formatOutput($quoteitem->services_net_price); ?> </th>
		<th><?php echo $abc->formatOutput($quoteitem->services_net_price, $a->currency_rate, $a->currency_symbol); ?> </th>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<th></th>
	</tr>
	<tr>
		<th>Logistics</th>
		<th></th>
		<th></th>
		<th></th>
	</tr>
	<tr>
		<td>Base Charge:Country,region,Qty </td>
		<td><?php echo $abc->formatOutput($quoteitem->logistics_base_price) ;?></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>Logistic Product adjustment</td>
		<td><?php echo $quoteitem->logistics_product_margin ?>%</td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>General Logistic adjustment</td>
		<td><?php echo $quoteitem->logistics_margin ?>%</td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<th>TTG Logistic Charge</th>
		<th></th>
		<th><?php echo $abc->formatOutput($quoteitem->unit_f5_logistics); ?></th>
		<th><?php echo $abc->formatOutput($quoteitem->unit_f5_logistics, $a->currency_rate, $a->currency_symbol); ?></th>
	</tr>
	<tr>
		<td>OEM Logistic markup</td>
		<td>{{$quoteitem->logistics_oem_margin}}%</td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<th>Quoted Logistic Charge</th>
		<th></th>
		<th><?php echo $abc->formatOutput($quoteitem->unit_price_logistics); ?> </th>
		<th><?php echo $abc->formatOutput($quoteitem->unit_price_logistics, $a->currency_rate, $a->currency_symbol); ?></th>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<th>Net Product Value After Service Fees</th>
		<th></th>
		<th></th>
		<th></th>
	</tr>
	<?php 
	$ttgprice = $quoteitem->unit_prod_price - $quoteitem->unit_f5_services - $quoteitem->unit_f5_logistics;
	$ttgtotalprice = $ttgprice * $quoteitem->quantity;
	?>
	<tr>
		<th>TTG to Client (per unit)</th>
		<th></th>
		<th><?php echo $abc->formatOutput($ttgprice); ?></th>
		<th><?php echo $abc->formatOutput($ttgprice, $a->currency_rate, $a->currency_symbol); ?></th>
	</tr>
	<?php if ($quoteitem->quantity > 1){ ?>
		<tr>
		<th>TTG to Client (per unit)</th>
		<th>Qty = {{$quoteitem->quantity}}</th>
		<th><?php echo $abc->formatOutput($ttgtotalprice); ?></th>
		<th><?php echo $abc->formatOutput($ttgtotalprice, $a->currency_rate, $a->currency_symbol); ?></th>
		</tr>
	<?php } ?>

	<tr>
		<th>Client to Customer (per unit)</th>
		<th></th>
		<th><?php echo $abc->formatOutput($quoteitem->unit_price); ?></th>
		<th><?php echo $abc->formatOutput($quoteitem->unit_price, $a->currency_rate, $a->currency_symbol); ?></th>
	</tr>
	<?php if ($quoteitem->quantity > 1){ ?>
		<tr>
		<th>Client to Customer (total)</th>
		<th>Qty = {{$quoteitem->quantity}}</th>
		<th><?php echo $abc->formatOutput($quoteitem->total_price); ?></th>
		<th><?php echo $abc->formatOutput($quoteitem->total_price, $a->currency_rate, $a->currency_symbol); ?></th>
		</tr>
	<?php } ?>
	
</table>