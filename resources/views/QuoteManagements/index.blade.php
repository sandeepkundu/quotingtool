@extends('layouts.default') @section('content')


<style>
    .form-control:focus{
        border: 1px solid #66afe9 !important;
    }

    a:hover{
        text-decoration: none;
    }

</style>
@if(isset($toast['message']))
{{ $toast['message'] }}
@endif
<div class="header-content">
    <h2><i class="fa fa-table"></i> QUOTE LIST <span></span></h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label"></span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="{{URL::to('/')}}/admin/dashboard">Dashboard</a>
            </li>
            <li class="active">                	
            </li>
        </ol>
    </div><!-- /.header-content -->
</div><!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-tab panel-tab-double shadow">
                <div class="panel-body no-padding">
                    <div class="panel panel-default shadow no-margin">
                        <div class="panel-heading">
                            <a  href="{{URL::to('/')}}/QuoteManagements/index" class="btn btn-default tooltips" data-toggle="tooltip" data-placement="top" data-title="Reload" data-action="refresh" data-original-title="" title="" id="ml5"><i class="icon-refresh icons"></i>
                            </a>
                             <a href="javascript:void(0);" class="btn btn-danger tooltips" data-toggle="tooltip" data-placement="top" data-title="Delete" data-original-title="" title="" onclick="return multiId()" ><i class="icon-trash icons"></i>
                            </a>
                            <div class="clearfix"></div>
                        </div>

                        <!--Panel Body-->
                        <div class="panel-body">
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="tab-client-all">
                                    <div id="datatable-client-all_wrapper" class="dataTables_wrapper form-inline">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="dataTables_length" id="datatable-client-all_length">
                                                    <label>
                                                        <select name="datatable-client-all_length" class="form-control input-sm" id="paginationPerPage">
                                                            <?php
                                                            $l = 30;
                                                            $total = $QuoteManagements->total();
                                                            $loop = intval($total / $l);

                                                            if (($total % $l) != 0) {
                                                                $loop = $loop + 1;
                                                            }
                                                            ?>  
                                                            @for($i=1; $i<=$loop;$i++)
                                                            <option value="{{$i*$l}}" {{ (($i*$l)==$lim)? 'selected' : '' }}>{{$i*$l}}</option>
                                                            @endfor
                                                        </select> Records per page
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div id="datatable-client-all_filter" class="dataTables_filter">
                                                    <form action="{{URL::to('/')}}/QuoteManagements/index" method="get" class="navbar-form" id="filter">

                                                        <div class="form-group has-feedback pull-right" style="margin-right:-15px !important;margin-top:-13px;">
                                                            <select name="status" class="statusChanges form-control input-sm">
                                                                <option value="">Status</option>
                                                                @foreach($QuoteStatus as $val)
                                                                <option value="{{$val->id}}">{{$val->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            <input type="text" class="form-control rounded"
                                                                   placeholder="Search" name="search">
                                                            <button type="submit" class="btn btn-color  btn-hover btn-theme fa fa-search form-control-feedback rounded"></button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                        <table class="table table-striped table-success table-middle table-project-clients " role="grid" aria-describedby="datatable-client-all_info">

                                            <thead>
                                                <tr role="row">
                                                    <th style="width:10px !important;text-align:center;"><input type="checkbox" class="multiIdCheck"/></th>
                                                    <th class="sorting" style="width: 50px;">ID</th>
                                                    <th class="sorting" style="width: 200px;">Reference</th>
                                                    <th class="sorting">Client</th>
                                                    <th class="sorting">User</th>
                                                     <th class="sorting">Value</th>
                                                     <th class="sorting">Created on</th>
                                                    <th class="sorting">Status</th>
                                                    <th style="width: 108px !important;">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <form action="{{URL::to('/') }}/QuoteManagements/multipleDelete" method="post" class="multipleDeleteForm">
                                                {{ csrf_field() }}
                                                <?php
                                                $i = 0;
                                                if (count($QuoteManagements) > 0) {
                                                    foreach ($QuoteManagements as $QuoteManagement) {
                                                        $i++;
                                                        ?>

                                                        <tr role="row">
                                                            <td style="text-align:center;">
                                                                <input type="checkbox" class="id" name="ids[]" value="{{$QuoteManagement->id}}"/>
                                                            </td>
                                                            <td>{{$QuoteManagement->id}}</td>
                                                            <td>
                                                                @if($QuoteManagement->name != '')
                                                                {{$QuoteManagement->ref_name}}
                                                                @else
                                                                N/A
                                                                @endif
                                                                
                                                            </td>
                                                            <td>
                                                                <a href="{{URL::to('/') }}/QuoteManagements/index?client_id={{$QuoteManagement->client_id}}">
                                                                    {{$QuoteManagement->client_name}}
                                                                </a><br>
                                                                @if($QuoteManagement->client_type == "store")
                                                                <span class="label label-info text-capitalize">
                                                                    Store
                                                                </span>
                                                                @else
                                                                <span class="label label-warning text-capitalize">Enterprise</span>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <a href="{{URL::to('/') }}/QuoteManagements/index?user_id={{$QuoteManagement->user_id}}">{{isset($QuoteManagement->user_name)?$QuoteManagement->user_name:''}}
                                                                </a>
                                                            </td>
                                                            <td>${{$QuoteManagement->value}}</td>
                                                             <td>{{date('d/m/Y', strtotime($QuoteManagement->date_created))}}</td>
                                                            <td>
                                                                @if(isset($QuoteManagement->status_name))

                                                                @if($QuoteManagement->client_type=="store")

                                                                @if($QuoteManagement->qs_id == 1)
                                                                <span class="btn btn-color" style="border-radius: 20px; padding: 3px 10px;background-color: #f0ad4e !important;border-color: #f0ad4e;">
                                                                    {{isset($QuoteManagement->status_name) ? $QuoteManagement->status_name:''}}
                                                                </span>
                                                                @elseif ($QuoteManagement->qs_id == 2)
                                                                <span class="btn btn-color" style="border-radius: 20px; padding: 3px 10px;">
                                                                    {{isset($QuoteManagement->status_name) ? $QuoteManagement->status_name:''}}
                                                                </span>
                                                                @elseif ($QuoteManagement->qs_id == 3)
                                                                <span class="btn btn-color" style="border-radius: 20px; padding: 3px 10px;background-color: grey !important; border-color: grey;">
                                                                    {{isset($QuoteManagement->status_name) ? $QuoteManagement->status_name:''}}
                                                                </span>
                                                                @elseif ($QuoteManagement->qs_id == 4)
                                                                <span class="btn btn-color" style="border-radius: 20px; padding: 3px 10px;background-color: grey !important; border-color: grey;">
                                                                    {{isset($QuoteManagement->status_name) ? $QuoteManagement->status_name:''}}
                                                                </span>
                                                                @elseif ($QuoteManagement->qs_id == 5)
                                                                <span class="btn btn-color" style="border-radius: 20px; padding: 3px 10px;background-color: powderblue !important;border-color: powderblue;">
                                                                    {{isset($QuoteManagement->status_name)? $QuoteManagement->status_name:''}}
                                                                </span>
                                                                @elseif ($QuoteManagement->qs_id == 6)
                                                                <span class="btn btn-color" style="border-radius: 20px; padding: 3px 10px;background-color: green !important;border-color: green;"">
                                                                    {{isset($QuoteManagement->status_name) ? $QuoteManagement->status_name:''}}
                                                                </span>
                                                                @elseif ($QuoteManagement->qs_id == 7)
                                                                <span class="btn btn-color" style="border-radius: 20px; padding: 3px 10px;background-color: red !important;border-color: red;">
                                                                    {{isset($QuoteManagement->status_name) ? $QuoteManagement->status_name:''}}
                                                                </span>
                                                                @endif

                                                                @else

                                                                @if($QuoteManagement->qs_id == 1)
                                                                <span class="btn btn-color" style="border-radius: 20px; padding: 3px 10px;background-color: #f0ad4e !important;border-color: #f0ad4e;">
                                                                   In-Progress
                                                                </span>
                                                                @elseif ($QuoteManagement->qs_id == 2)
                                                                <span class="btn btn-color" style="border-radius: 20px; padding: 3px 10px;">
                                                                    Confirm
                                                                </span>
                                                                @elseif ($QuoteManagement->qs_id == 3)
                                                                <span class="btn btn-color" style="border-radius: 20px; padding: 3px 10px;background-color: grey !important; border-color: grey;">
                                                                    Present
                                                                </span>
                                                                @elseif ($QuoteManagement->qs_id == 4)
                                                                <span class="btn btn-color" style="border-radius: 20px; padding: 3px 10px;background-color: grey !important; border-color: grey;">
                                                                    Presented
                                                                </span>
                                                                @elseif ($QuoteManagement->qs_id == 5)
                                                                <span class="btn btn-color" style="border-radius: 20px; padding: 3px 10px;background-color: powderblue !important;border-color: powderblue;">
                                                                    Accept
                                                                </span>
                                                                @elseif ($QuoteManagement->qs_id == 6)
                                                                <span class="btn btn-color" style="border-radius: 20px; padding: 3px 10px;background-color: green !important;border-color: green;"">
                                                                    Completed
                                                                </span>
                                                                @elseif ($QuoteManagement->qs_id == 7)
                                                                <span class="btn btn-color" style="border-radius: 20px; padding: 3px 10px;background-color: red !important;border-color: red;">
                                                                    Canceled
                                                                </span>
                                                                @endif

                                                                @endif
                                                                @endif
                                                            </td>
                                                            <td class="">
                                                                <a class="btn btn-round btn-color btn-hover" href="{{URL::to('/')}}/QuoteManagements/QuoteDetails/{{$QuoteManagement->id}}" style="padding: 0px 6px">
                                                                    <i class="fa fa-edit"></i>
                                                                </a>
                                                                <a onclick="return confirm('Are you sure you want to delete?')" class="btn btn-round btn-danger"  style="padding: 0px 6px" href="{{URL::to('/')}}/QuoteManagements/delete/{{$QuoteManagement->id}}">
                                                                    <i class="fa fa-trash-o"></i>
                                                                </a>
                                                            </td>
                                                        </tr>

                                                    <?php
                                                    }
                                                } else {
                                                    echo '<tr role="row"><td colspan="9" style="color:red;">  No records found!!!</td></tr>';
                                                }
                                                ?>
                                            </form>
                                            </tbody>
                                            <tfoot>
                                                <tr role="row">
                                                    <th style="width: 10px !important; text-align:center;padding: 0px; "><input type="checkbox" class="multiIdCheck"/></th>
                                                    <th class="sorting" style="width: 50px;">ID</th>
                                                    <th class="sorting" style="width: 200px;">Reference</th>
                                                    <th class="sorting">Client</th>
                                                    <th class="sorting">User</th>
                                                    <th class="sorting">Value</th>
                                                    <th class="sorting">Created on</th>
                                                    <th class="sorting">Status</th>
                                                    <th style="width: 108px !important;">Actions</th>
                                                </tr>
                                            </tfoot>
                                        </table>

                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="dataTables_info" id="datatable-client-all_info" role="status" aria-live="polite">  
                                                    @if($QuoteManagements->total()!=0)
                                                    Showing {{ $QuoteManagements->firstItem() }} to {{ $QuoteManagements->lastItem() }} of {{ $QuoteManagements->total() }}
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-xs-6">
                                                <div class="dataTables_paginate paging_simple_numbers" id="datatable-client-all_paginate">
                                                    {{ $QuoteManagements->links() }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<form action="{{URL::to('/')}}/QuoteManagements/index" method="get" class="navbar-form" id="formPerPage">
    <input type="hidden" value="" name="perPage" id="perPage">
</form>
<style>
    .pagination{
        float: right;
        margin:0px;
    }
    #tour-3{
        margin-bottom: 0px;
    }
    .padding_bottom{
        padding-bottom: 15px;
    }
</style>

@section('js')
<script type="text/javascript">
    $(document).ready(function () {
        $('#paginationPerPage').change(function () {
            $a = $(this).val();
            $('#perPage').val($a);
            $('#formPerPage').submit();
        });


        $(".statusChanges").change(function () {
            $('#filter').submit();
        });


    });



    function multiId() {
        var checkboxcount = jQuery('.id:checked').length;
        if (checkboxcount != 0) {
            if (confirm("Are you sure you want to delete ?")) {
                jQuery('.multipleDeleteForm').submit();
            }
        } else {
            alert("Please select a record")
            return false;
        }
    }

</script>
@endsection

@endsection