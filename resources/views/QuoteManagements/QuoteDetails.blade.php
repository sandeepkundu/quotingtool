@extends('layouts.default') @section('content')
<link href="{{ asset('assets/global/plugins/bower_components/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/global/plugins/bower_components/animate.css/animate.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/global/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet">
<link href="{{ asset('assets/global/plugins/bower_components/jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/global/plugins/bower_components/chosen_v1.2.0/chosen.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/stepsStore.css') }}" rel="stylesheet">
<link href="{{ asset('css/QuoteDetails.css') }}" rel="stylesheet">
<div class="body-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">QUOTE DETAILS PAGE FOR {{$Quote->id}}</h3>
                    </div>

                    <div class="clearfix"></div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body no-padding">
                    <div class="form-body">
                        <div class="form-group">

                            <!--=================DETAILS ============-->

                            <div class="row">
                                <div class="col-lg-12">
                                    <form method="post" action="{{url('/')}}/QuoteManagements/updateClientFinancial">
                                        {{csrf_field()}}
                                        <input type="hidden" name="id" value="{{$Quote->id}}">
                                        <table class="table table-striped table-success table-middle table-project-clients">
                                            <thead class="details">
                                                <th width="100%">
                                                    <h4>DETAILS : </h4>
                                                    <span class="pull-right">&#8657;</span>
                                                </th>
                                            </thead>
                                            <tbody id="details" class="">

                                                <tr>
                                                    <td>
                                                        <div class="col-lg-12">
                                                            <div class="col-lg-6" style="padding-bottom: 5px;">
                                                                <label class="col-sm-3 control-label">Client</label>
                                                                <div class="col-sm-9">
                                                                    <input type="hidden" value="{{$Quote->client_id}}" name="client">

                                                                    <select class="form-control" disabled="">
                                                                     <option value="">--Select Client--</option>


                                                                     @foreach($Clients as $Client)
                                                                     <option value="{{$Client->id}}" {{($Client->id == $Quote->client_id) ? 'selected':''}}>{{$Client->name}}</option>
                                                                     @endforeach
                                                                 </select>
                                                             </div>
                                                         </div>
                                                         <div class="col-lg-6" style="padding-bottom: 5px;">
                                                            <label class="col-sm-3 control-label">Reference</label>
                                                            <div class="col-sm-9">
                                                               <input type="text" class="form-control" name="name" value="{{isset($Quote->name)?$Quote->name:'N/A'}}">
                                                           </div>

                                                       </div>
                                                   </div>

                                                   <div class="col-lg-12">
                                                    <div class="col-lg-6" style="padding-bottom: 5px;">
                                                        <label class="col-sm-3 control-label">User</label>
                                                        <div class="col-sm-9">
                                                          <input type="hidden" value="{{$Quote->user_id}}" name="user">
                                                          <select class="form-control" disabled="">
                                                             <option>--Select Client--</option>
                                                             @foreach($Users as $User)
                                                             <option value="{{$User->id}}" {{($User->id == $Quote->user_id) ? 'selected':''}}>{{$User->first_name}}</option>
                                                             @endforeach
                                                         </select>
                                                     </div>
                                                 </div>
                                                 <div class="col-lg-6" style="padding-bottom: 5px;">
                                                    <label class="col-sm-3 control-label">Country</label>
                                                    <div class="col-sm-9">

                                                        <select class="form-control" name="country" id="country">
                                                         <option>--Select Country--</option>
                                                         @foreach($countries as $Country)
                                                         <option value="{{$Country->id}}" {{($Country->id == $Quote->country) ? 'selected':''}}>{{$Country->name}}</option>
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div>
                                         </div>

                                         <div class="col-lg-12">
                                            <div class="col-lg-6" style="padding-bottom: 5px;">
                                                <label class="col-sm-3 control-label">Region</label>
                                                <div class="col-sm-9">

                                                    <select class="form-control" name="region" id="region">
                                                     <option>--Select Region--</option>
                                                     @foreach($regions as $region)
                                                     <option value="{{$region->id}}"  {{($region->id == $Quote->region) ? 'selected':''}}>{{$region->name}}</option>
                                                     @endforeach
                                                 </select>
                                             </div>
                                         </div>
                                         <div class="col-lg-6" style="padding-bottom: 5px;">
                                            <label class="col-sm-3 control-label">Currency</label>
                                            <div class="col-sm-9">
											<select  class="form-control" name="currency_symbol" id="currency" >
											<option value="{{$currency->currency_symbol}}" {{($Quote->currency_symbol == $currency->currency_symbol) ? 'selected' : ''}}>{{$currency->currency_symbol}}</option>
											@if($Quote->country != 13)
											<option value="US$" {{($Quote->currency_symbol == '$' && $Quote->currency_rate == 1) ? 'selected="selected"' : ''}}>US$</option>
											@endif
											</select>       
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="col-lg-6" style="padding-bottom: 5px;">
                                            <label class="col-sm-3 control-label">Exchange Rate</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="currency_rate" value="{{isset($Quote->currency_rate)?$Quote->currency_rate:''}}">
                                            </div>
                                        </div>
                                        <div class="col-lg-6" style="padding-bottom: 5px;">
                                            <label class="col-sm-3 control-label">Margin oem product</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="margin_oem_product" value="{{$Quote->margin_oem_product}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="col-lg-6" style="padding-bottom: 5px;">
                                            <label class="col-sm-3 control-label">Margin oem logistic</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="margin_oem_logistic" value="{{$Quote->margin_oem_logistic}}">
                                            </div>
                                        </div>
                                        <div class="col-lg-6" style="padding-bottom: 5px;">
                                            <label class="col-sm-3 control-label">Margin oem service</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="margin_oem_service" value="{{$Quote->margin_oem_service}}">
                                            </div>
                                        </div>
                                    </div>

									<div class="col-lg-12">
                                        <div class="col-lg-6" style="padding-bottom: 5px;">
                                            <label class="col-sm-3 control-label">Brand Adj </label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="margin_brand" value="">
                                            </div>
                                        </div>
                                        <div class="col-lg-6" style="padding-bottom: 5px;">
                                            <label class="col-sm-3 control-label">Screen Adj </label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="margin_screen" value="">
                                            </div>
                                        </div>
                                    </div>

									<div class="col-lg-12">
                                        <div class="col-lg-6" style="padding-bottom: 5px;">
                                            <label class="col-sm-3 control-label">Customer Adj </label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="margin_customer" value="">
                                            </div>
                                        </div>
                                        <div class="col-lg-6" style="padding-bottom: 5px;">
                                            <label class="col-sm-3 control-label">Country Adj</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="margin_country" value="">
                                            </div>
                                        </div>
                                    </div>

									<div class="col-lg-12">
                                        <div class="col-lg-6" style="padding-bottom: 5px;">
                                            <label class="col-sm-3 control-label">Services Adj</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="margin_Service" value="">
                                            </div>
                                        </div>
                                        <div class="col-lg-6" style="padding-bottom: 5px;">
                                            <label class="col-sm-3 control-label">Logistics Adj</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="margin_logistics" value="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="col-lg-6" style="padding-bottom: 5px;">

											<label class="col-sm-3 control-label">Pricing Locks</label>
											<div class="col-sm-9">
											<select class="form-control" name="lockpricing">
											<option value="0"> </option>
											<option value="1" <?php if($Quote->flag == 1) { echo 'selected="selected"'; } ?>>Lock currency only</option>
											<option value="2" <?php if($Quote->flag == 2) { echo 'selected="selected"'; } ?>>Lock margins (item) only</option>
											<option value="3" <?php if($Quote->flag == 3) { echo 'selected="selected"'; } ?>>Lock margins & currency</option>
											<!--<option value="3" <?php if($Quote->flag == 4) { echo 'selected="selected"'; } ?>>Lock everything</option>-->
											</select>
											</div>
                                   
                                        </div>

                                        <div class="col-lg-6" style="padding-bottom: 5px;">
                                            <label class="col-sm-3 control-label">Logistics Product Adj</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="margin_logisticsp" value="">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-footer">
                                        <div class="col-sm-offset-5">
                                           <input type="submit" name="btn_sub" class="btn btn-color btn-hover">
                                       </div>

                                   </div>
                               </td>
                           </tr>
                       </tbody>

                   </table>
               </form>
           </div>
       </div>

       <!--===========================FINANCIALS==========================-->

       <div class="row" style="margin-top: 20px;">
        <div class="col-lg-12">
           <table class="table table-striped table-success table-middle table-project-clients " align="center">
            <thead class="financials" style="width: 100%">
                <th colspan="5">
                    <h4">FINANCALS : </h4>
                    <span class="pull-right">&#8657;</span>
                </th>
            </thead>
            <tr>
                <td align="left" style="font-weight: 700;font-size: 15px;">Financial Summery</td>
                <td align="center" style="font-weight: 600;">Product Value</td>
                <td align="center" style="font-weight: 600;">Services Fee</td>
                <td align="center" style="font-weight: 600;">Logistics Fee</td>
                <td align="center" style="font-weight: 600;">Net Value</td>
            </tr>

            <tr>
                <td>Quote from Partner</td>
                <td align="center">${{number_format($q_unit_tp, 0)}}</td>
                <td align="center">${{number_format(@$q_services_tp, 0)}}</td>
                <td align="center">${{number_format(@$q_logistic_tp, 0)}}</td>
                <td align="center">${{number_format(@$q_total_tp, 0)}}</td>
            </tr>
            <tr>
                <td>Quote to Customer</td>
                <td align="center">${{number_format($q_unit_oemtp, 0)}}</td>
                <td align="center">${{number_format(@$q_services_oemtp, 0)}}</td>
                <td align="center">${{number_format(@$q_logistic_oemtp, 0)}}</td>
                <td align="center">${{number_format(array_sum(@$total_price), 0)}}</td>
            </tr>
            <tr>
                <td>Margin / Markup $</td>
                <td align="center">${{number_format(($q_unit_tp - $q_unit_oemtp), 0)}} </td>
                <td align="center">${{number_format(($q_services_oemtp - $q_services_tp), 0)}} </td>
                <td align="center">${{number_format(($q_logistic_oemtp - $q_logistic_tp), 0)}} </td>
                <td align="center">${{number_format(($q_total_tp - $q_total_oemtp), 0)}} </td>
            </tr>
            <tr>
                <td>Margin / Markup  %</td>
				<td align="center">
				@if($q_unit_tp > 0)	
				{{number_format( ceil((($q_unit_tp - $q_unit_oemtp) / $q_unit_tp) * 100), 0)}}%
				@endif
				</td>
				<td align="center">
				@if($q_services_tp > 0)	
				{{number_format( ceil((($q_services_oemtp - $q_services_tp) / $q_services_tp) * 100), 0)}}%
				@endif	
				</td>
				<td align="center">
				@if($q_logistic_tp > 0)	
				{{number_format( ceil((($q_logistic_oemtp - $q_logistic_tp) / $q_logistic_tp) * 100), 0)}}%
				@endif	
				</td>
				<td align="center">
				@if($q_total_tp > 0)	
				{{number_format( ceil((($q_total_tp - $q_total_oemtp) / $q_total_tp) * 100), 0)}}%				
				@endif	
				</td>
            </tr>
        </table>
    </div>
</div>   
Financial Summery

<!--===============================NOTES=============================-->

<div class="row" style="margin-top: 20px;">
    <div class="col-lg-12">
        <table class="table table-striped table-success table-middle table-project-clients ">
            <thead class="note">
                <tr>
                    <th width="100%"><h4">NOTES : </h4>
                        <span class="pull-right">
                            <a href="{{URL('/')}}/QuotesNotes/add/{{@$Quote->id}}" class="btn btn-success" style="margin-right: 20px;padding: 2px 12px;border-radius: 20px;">
                                Add Quote Note
                            </a>
                            <span class="pull-right">&#8657;</span>
                        </span>
                    </th>
                </tr>
            </thead>
        </table>
        <table class="table " id="note" style="padding-left: 20px;">
            <thead>
                <tr>
                    <th>DATE</th>
                    <th>DISPLAY LEVEL</th>
                    <th>USER</th>
                    <th>NOTE</th>
                    <th style="width: 85px;">ACTIONS</th>
                </tr>
            </thead>
            <tbody>
                @if(count($QuotesNotes)>0)
                @foreach($QuotesNotes as $val)
                <tr>
                    <td>{{date('d-m-Y', strtotime($val->date_created))}}</td>
                    <td>
                        @if($val->display_level == 1)
                        Admin
                        @else
                        User    
                        @endif
                    </td>

                    <td>{{isset($val->Username->first_name)?$val->Username->first_name : 'N/A'}}</td>
                    <td>@if(!empty($val->note))
                        {{$val->note}}
                        @else
                        --
                        @endif</td>
                        <td>
                            <a class="btn btn-round btn-color btn-hover" href="{{URL::to('/')}}/QuotesNotes/edit/{{$val->id}}" style="padding: 0px 6px">
                                <i class="fa fa-edit"></i>
                            </a>
                            <a onclick="return confirm('Are you sure you want to delete?')" class="btn btn-round btn-danger"  style="padding: 0px 6px" href="{{URL::to('/')}}/QuotesNotes/Delete/{{$val->id}}">
                                <i class="fa fa-trash-o"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="4" style="color: red;">No record found !!</td>
                    </tr> 

                    @endif
                </tbody>
            </table>
        </div>
    </div>   

    <!--================================ITEMS================================-->

    <div class="row" style="margin-top: 20px;">
        <div class="col-lg-12">
            <table class="table table-striped table-success table-middle table-project-clients ">
                <thead class="item">
                    <tr>
                        <th width="100%"><h4">ITEMS : </h4>
                            <span class="pull-right">&#8657;</span>
                        </th>
                    </tr>
                </thead>

            </table>

            <table class="table table-striped table-bordered table-responsive">
                <thead>

                    <tr>
                        <th><b style="font-weight: 700;font-size: 18px">Product Details</b> (Click Product to Expand)</th>
                        <th>Item</th>
                        <th>QTY</th>
                        <th>Unit Price</th>
                        <th>Services</th>
                        <th>Logistics</th>
                        <th>Net Price</th>
                        <th>Total Net</th>
                        <th>Margin %</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                    @if(count($QuoteItems)>0)

                    @foreach($QuoteItems as $key1 => $quotes1)

                    <tr>
                        <td>
                            <a href="javascript:void(0);" style="font-weight: 600;color: #1581bf;font-size: 16px;" data-toggle="collapse" data-target=".{{$key1}}">{{$key1}}</a>

                        </td>
						@php 
						$qty2 = $unitp2 = $services1 = $logistic1 = $net_price1 = $total_price1 = $total_price1 = $totalmargin1 = $quotes2 = $p_unit = $p_logistics = $p_services = array(); 
						@endphp


                        @foreach($quotes1 as $Quote1)
                        @php
	                                $qty2[]			= $Quote1[0]['quantity'];
	                                $unitp2[]		= $Quote1[0]['unit_f5_price'] * $Quote1[0]['quantity'];
	                                $services1[]	= $Quote1[0]['services_net_price'] * $Quote1[0]['quantity'];
	                                $logistic1[]	= $Quote1[0]['logistics_net_price'] * $Quote1[0]['quantity'];
	                                $net_price1[]	= $Quote1[0]['unit_price'];
	                                $total_price1[] = $Quote1[0]['total_price'];
	                                $totalmargin1[] = $Quote1[0]['totalmargin'];		

									$p_unit[]		= $Quote1[0]['unit_prod_price'] * $Quote1[0]['quantity'];
									$p_logistics[]	= $Quote1[0]['unit_f5_logistics'] * $Quote1[0]['quantity'];
									$p_services[]	= $Quote1[0]['unit_f5_services'] * $Quote1[0]['quantity']; 

                        @endphp
                        @endforeach

						@php									
						$net_price = array_sum($p_unit) - array_sum($p_services) - array_sum($p_logistics);
						$ave_margin = ceil( (1 - (array_sum($total_price1) / $net_price)) * 100); 																	
						@endphp

                        <?php $a =count($quotes1); ?>
						<td align="center">{{count($quotes1)}}</td>
						<td align="center">{{number_format(array_sum($qty2), 0, '.', ',')}}</td>
						<td align="center">${{number_format(array_sum($unitp2) / array_sum($qty2), 0 )}}</td>
						<td align="center">${{number_format(array_sum($services1) / array_sum($qty2), 0 )}}</td>
						<td align="center">${{number_format(array_sum($logistic1) / array_sum($qty2), 0 )}}</td>
						<td align="center">${{number_format(array_sum($total_price1) / array_sum($qty2) , 0 )}}</td>
						<td align="center">${{number_format(array_sum($total_price1), 0 )}}</td>
						<td align="center">{{number_format($ave_margin,0)}}%</td>
						<td></td>	
                    </tr>
                    @php $i = 1; @endphp
                    @foreach($quotes1 as $quote)
                    @php $ids[] = $quote[0]['id']; @endphp
                    <tr class="collapse demo {{$key1}}" style="border:1px solid grey !important;">
                        <td>
                            <div class="row" style="padding: 0px;font-size: 7px;">
                                <div class="col-md-2 col-lg-2 col-xs-12 col-sm-2" style="padding: 0px;">
                                    <div class="selected-box" style="width:50px; height: 50px;padding-top: 7px;">
                                        <div class="" style="padding: 0px;">
                                            @if(!empty($quote[0]['model_image']['image']))
                                            @if(!empty($va['model_image']['image']))
                                            <img src="{{ URL::to('/product') }}/{{@$va['model_image']['image']}}" width="100%" height="100%;"/>
                                            @else
                                            N/A
                                            @endif
                                            @elseif(!empty($quote[0]['option_model_image']['image'])) 
                                            <img src="{{ URL::to('/product') }}/{{$quote[0]['option_model_image']['image']}}" width="100%" height="100%;"/>
                                            @else
                                            N/A 
                                            @endif
                                        </div>
                                        <!-- <div class="brand-title" style="background-color: #000; color:#fff; font-size: 7px; margin-bottom: 0px;">hp user</div> -->
                                    </div>
                                </div>
                                <div class="col-md-10 col-lg-10 col-xs-12 col-sm-10" style="padding: 0px;">
                                    <span style="font-size: 10px; font-weight: 600;">{{$quote[0]['quote_name']['name']}}</span><br>
                                    <div class="col-md-6 col-lg-6 col-xs-12 col-sm-6" style="font-size: 12px;">
									    @if(!empty($Quote[0]['model_name']))
    										<div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" style="padding: 0px;">
                                            • Product : {{$Quote[0]['model_name']}}
											</div>
    									@endif

                                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" style="padding: 0px;">
                                            • Product : {{$quote[0]['type_name']}}
                                        </div>
                                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" style="padding: 0px;">
                                            • Brand : {{@$quote[0]['brand_name']}}
                                        </div>
                                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" style="padding: 0px;">
                                            • Processor : {{@$quote[0]['processor_name']}}
                                        </div>
                                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" style="padding: 0px;">
                                            • Screen : {{@$quote[0]['screen_name']}}
                                        </div>
                                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" style="padding: 0px;">
                                            • DVD : {{@$quote[0]['dvd_name']}}
                                        </div>
                                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" style="padding: 0px;">
                                            • HDD : {{@$quote[0]['hdd_name']}}
                                        </div>
                                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" style="padding: 0px;">
                                            • RAM : {{@$quote[0]['ram_name']}}
                                        </div>

                                    </div>

                                    <div class="col-md-6 col-lg-6 col-xs-12 col-sm-6" style="font-size: 12px;">
                                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" style="padding: 0px;">
                                            • Service : {{$quote[0]['services_name']}}
                                        </div>

                                        @if(isset($quote[0]['quote_items_defect']) && count($quote[0]['quote_items_defect'])>0)

                                        <span style="font-size: 10px; font-weight: 600;">Device Condition : </span>
                                        @foreach($quote[0]['quote_items_defect'] as $val)
                                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" style="padding: 0px;">
                                            •  {{$val['defect_name']}}
                                        </div>
                                        @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </td>

                        <td>
                            {{$i++}}
                            
                        </td>

						@php
						/* get the pre oem pricing for products, logistics and services to work out the actual OEM margin */
						$item_p_total		= ($Quote[0]['unit_prod_price'] - $Quote[0]['unit_f5_logistics'] - $Quote[0]['unit_f5_services']) * $Quote[0]['quantity'];
						$item_m				= ($item_p_total != 0) ? ceil( (1 - ($Quote[0]['total_price'] / $item_p_total)) * 100) : 0;
						@endphp

                        <td>
                            @if($Quotes->status==1)
                            <input type="text" value="{{$quote[0]['quantity']}}" class="{{$quote[0]['id']}} quanitity input-quantity" readonly style="width:58px">
                            <a href="javascript:void(0)" class="btn btn-success quantityUpdate" id="{{$quote[0]['id']}}" name="" style='display:none;'>
                                Go
                            </a>
                            @else
                            {{$quote[0]['quantity']}}
                            @endif
                        </td>

                        <td>{{number_format($quote[0]['unit_f5_price'],2)}}</td>
                        <td>{{number_format($quote[0]['services_net_price'],2)}}</td>
                        <td>{{number_format($quote[0]['logistics_net_price'],2)}}</td>
                        <td>{{number_format($quote[0]['unit_price'],2)}}</td>
                        <td>{{number_format($quote[0]['total_price'],2)}}</td>
                        <td>{{number_format($item_m,2)}}</td>
                        <td>
                            @if($Quotes->status==1)
                            <a class="btn btn-round btn-color btn-hover iconclass editCart" href="javascript:void(0)" style="padding: 0px 5px; margin-top:2px;">
                                <i class="fa fa-edit"></i>
                            </a>
                            <a class="btn btn-round btn-info edit_price_margin" style="padding: 0px 8px; margin-top:2px;" href="javascript:void(0)" name="{{$quote[0]['id']}}">
                                <i class="fa fa-info"></i>
                            </a>                            
							<a onclick="return confirm('Are you sure you want to delete?')" class="btn btn-round btn-danger"  style="padding: 0px 5px; margin-top:2px;" href="{{URL::to('/')}}/QuoteManagements/delete_quote_item/{{$quote[0]['id']}}">
                                <i class="fa fa-trash-o"></i>
                            </a>
                            @endif

                            <a class="btn btn-round btn-info detailItem" style="padding: 0px 4px; margin-top:2px;" href="javascript:void(0)" name="{{$quote[0]['id']}}">
                                <i class="fa fa-eye"></i>
                            </a>

                        </tr>
                        @endforeach

                        @endforeach
                        @endif 

                        <tr>
                            <td class="border-empty"></td>
                            <td>{{count($ItemCount)}}</td>
                            <td> {{array_sum($qty)}}</td>
                            <td class="border-empty"></td>
                            <td class="border-empty"></td>
                            <td class="border-empty"></td>
                            <td class="border-empty"></td>
                            <td>{{number_format(@$q_total_oemtp, 2)}}</td>
                            <td>
							@if($q_total_tp > 0 && $q_total_tp >= $q_total_oemtp)	
							{{number_format( ceil((($q_total_tp - $q_total_oemtp) / $q_total_tp) * 100), 0)}}%
							@elseif ($q_total_tp > 0 && $q_total_tp < $q_total_oemtp)
							{{number_format( ceil((($q_total_oemtp - $q_total_tp) / $q_total_tp) * 100), 0)}}%
							@endif	
							</td>

                        </tr>
                    </tbody>
                </table>
            </div>
        </div>   

        <!--=======================================ACTION=====================================-->

        <div class="row" style="margin-top: 20px;">
            <div class="col-lg-12">
                <table class="table table-striped table-success table-middle table-project-clients ">
                    <thead class="action">
                        <tr>
                            <th width="100%"><h4>ACTIONS : </h4>
                                <span class="pull-right">&#8657;</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody id="action" class="">
                        <tr>
                            <td> 

                                <div class="col-lg-12">
                                    <label class="col-sm-3 control-label">Status</label>
                                    <div class="col-lg-9">


                                        @if($Quotes->Clientname->client_type == "store")
                                        @if(@$Quote->Status->id == 1)
                                        <a href="javascript:void(0)" style="float: left;background-color: #f0ad4e;" name="{{$Quote->Status->id}}" id="{{$Quote->id}}" class=" btn changeStatus">	
                                            {{$Quote->Status->name}}
                                        </a>
                                        @elseif ($Quote->Status->id == 2)
                                        <a href="javascript:void(0)" style="float: left;" name="{{$Quote->Status->id}}" id="{{@$Quote->id}}" class=" btn changeStatus">
                                            Quote Accepted
                                        </a>

                                        @elseif ($Quote->Status->id == 3)


                                        <button class="Upload btn" style="float:left;border: 1px #56bdf1 solid;" val="{{@$Quote->id}}">
                                            Upload Photos / Sr.No
                                        </button>

                                        @elseif (@$Quote->Status->id == 4)
                                        <a href="javascript:void(0)" style="float: left; background-color: powderblue;" name="{{@$Quote->Status->id}}" id="{{@$Quote->id}}" class=" btn changeStatus">
                                            Collecton Request
                                        </a>

                                        @elseif ($Quote->Status->id == 5)
                                        <a href="javascript:void(0)" style="float: left; background-color: powderblue;" name="{{@$Quote->Status->id}}" id="{{@$Quote->id}}" class=" btn changeStatus">
                                            Awaited Collection
                                        </a>

                                        @elseif ($Quote->Status->id == 6)
                                        <a href="javascript:void(0)" style="float: left;background-color: #5cb85c" name="{{@$Quote->Status->id}}" id="{{$Quote->id}}" class=" btn changeStatus1">
                                            Completed
                                        </a>

                                        @elseif ($Quote->Status->id == 7)
                                        <a href="javascript:void(0)" style="float: left;background-color: red;" class="btn canceled">
                                            Canceled
                                        </a>
                                        @endif

                                        @elseif($Quotes->Clientname->client_type == 'enterprise')



                                        <!--=======================Enterprise client=================-->

                                        @if($Quotes['status'] == 1)

                                        <button type="button" class="AcceptButton btn">In-Progress 
                                        </button>

                                        @elseif (@$Quotes['status'] == 4)

                                        <button type="button" class="AcceptButton btn">Complete 
                                        </button>

                                        @elseif (@$Quotes['status'] == 6)

                                        <button type="button" class="AcceptButton btn">Accepted</button>

                                        @elseif ($Quotes['status'] == 7)

                                        <button type="button" class="canceled btn" pull-left">Canceled</button>
                                        @endif

                                        @if (@$Quotes['status'] == 4)

                                        <button type="submit" class="btn eedit">Edit</button>

                                        @endif
                                        @if (@$Quotes['status'] == 1 || @$Quotes['status'] == 4 || @$Quotes['status'] == 6 )

                                        <button type="button" class="btn ecancel" >Cancel</button>

                                        @endif
                                        <!--==============Enterprises client end============-->
                                        @endif
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>  

        @if($status > 3)
        <div class="row">
           <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
            <div class="col-md-8">
                <h5 style="float:left;font-weight: 700">Serial Number: </h5> 
                <h6 style="float:left;padding-left: 15px;"><?php echo $QuoteItem['serial_number']?></h6> 
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
            <h5 style="font-weight: 700;padding-left: 15px;">Product Images:</h5>
            @if($image1)
            <div class="col-md-4">
                <?php  $image1url=URL('/').$image1; ?>
                <a href="{{$image1url}}" target="_blank" >
                    <img src="<?php echo $image1url?>"  height="200" width="200"/>
                </a>
            </div>
            @endif
            @if($image2)
            <div class="col-md-4">
                <?php $image1url=URL('/').$image2; ?>
                <a href="{{$image1url}}" target="_blank" >
                    <img src="<?php echo $image1url?>"  height="200" width="200"/>
                </a>
            </div>
            @endif



            @if($image3)
            <div class="col-md-4">
                <?php $image1url=URL('/').$image3; ?>
                <a href="{{$image1url}}" target="_blank" >
                    <img src="<?php echo $image1url?>"  height="200" width="200"/>
                </a>
            </div>
            @endif
        </div>
    </div>
    @endif    
    <!--================================================================-->
</div>
</div>
<div class="form-group"></div>
</div>
</div>
</div>
</div>
</div>


<!--==============================Modal Popup=======================-->
<div class="modal modal-success fade in" id="modal-bootstrap-tour" style="display: none;">
    <div class="modal-dialog" role="document" style="margin: 150px auto;">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #56bdf1;">
                <button type="button" id="close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Upload Pictures</h4>
            </div>
            <div class="modal-body" style="height: auto;overflow: auto;">
                <div class="row" style="text-align: center; margin-bottom: 20px;">
                    <a href="javascript:void(0)" class="upload_sub_btn btn" style='' name="" id="uploadimageByWebcam">  
                     <i class="fas fa-video"></i> Upload By Webcam
                 </a>
             </div>
             <span style="color:red;" class="errormsgforimageupload"></span>
             <form action="{{URL('/')}}/QuoteManagements/UploadImage" method="post" enctype="multipart/form-data" onsubmit=" return imageuploadsubmit()">
                {{ csrf_field() }}
                <input type="hidden" name="quote_id" id="hiddenQuoteId">
                <table style="width: 100%" class="table">
                    <tr>
                        <td>
                            <div class="uploader" onclick="$('#filePhoto').click()">
                               <img src="{{asset('images/upload1.gif')}}" alt="drag an drop image here" />
                               <input type="file" name="userprofile_picture"  id="filePhoto" />
                               <input type="hidden" name="image" id="scrId" />
                           </div>
                       </td>

                       <td>
                        <div class="uploader2" onclick="$('#filePhoto2').click()">

                            <img src="{{asset('images/upload1.gif')}}" alt="drag an drop image here" />
                            <input type="file" name="userprofile_picture2"  id="filePhoto2" />
                            <input type="hidden" name="image2" id="scrId2" />
                        </div>
                    </td>

                    <td>
                        <div class="uploader3" onclick="$('#filePhoto3').click()">
                            <img src="{{asset('images/upload1.gif')}}" alt="drag an drop image here" />
                            <input type="file" name="userprofile_picture3"  id="filePhoto3" />
                            <input type="hidden" name="image3" id="scrId3" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="3"> 
                        <input type="text" id="serial_numberid" name="serial_number" class="form-control" placeholder="Please enter serial number">
                        <span style="color:red" class="megerrorserialno"></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align:center;">
                        <a href="javascript:void(0)" style='width:49%;display: none;' name="{{$Quote->Status->id}}" id="{{$Quote->id}}" class=" btn changeStatus skip">   
                            SKIP
                        </a>
                        <button type="submit" class='upload_sub_btn btn'>SUBMIT</button>

                    </td>
                </tr>    
            </table>
        </form>
    </div>
</div>
</div>
</div>
<form action="{{URL::to('/')}}/quote/history_list/" method="get" class="navbar-form" id="formPerPage">
    <input type="hidden" value="" name="perPage" id="perPage">
</form>
<!--==============================Modal Popup=======================--> 


<!--==============================Modal Popup for Item detail=======================-->
<div class="modal modal-success fade in" id="Itemdetail" style="display: none;">
    <div class="modal-dialog" role="document" style="margin: 67px auto;" id="ItemdetailWidth">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #00B1E1 !important;">
                <button type="button" id="close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Item Calculation Detail</h4>
            </div>
            <div class="modal-body detailPage" style="height: auto;overflow: auto;">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"> Close </button>
            </div>
        </div>
    </div>
</div>
<form action="{{URL::to('/')}}/quote/history_list/" method="get" class="navbar-form" id="formPerPage">
    <input type="hidden" value="" name="perPage" id="perPage">
</form>
<!--==============================Modal Popup for Item detail=======================--> 

<!--==============================Modal Popup Webcam=======================-->
<div class="modal modal-success fade in" id="modal-bootstrap-uploadImageByWebcam" style="display: none;">
    <div class="modal-dialog" role="document" style="margin: 150px auto; margin-top: 0px;">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #56bdf1;">
                <button type="button" id="close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Upload Pictures</h4>
            </div>
            <div class="modal-body" style="height: auto;overflow: auto;">
               <div class="camera" style="text-align: center;">
                <div class="col-md-12">
                    <div class="col-md-8 col-sm-8 col-lg-8 col-md-offset-2">

                       <video onclick="snapshot(this);" style="width: 100%;height: 100%; "id="video" controls autoplay></video>
                   </div>
               </div>
               <div class="col-md-12" style="padding:0px;">
                <div class="col-sm-4 col-md-4 col-lg-4" style="padding-left:2px;padding-right: 0px;">
                    <canvas  id="myCanvas" style="width: 100%;" width="100" height="75"></canvas>  
                    <button id="startbutton" style="width: 100%;" onclick="snapshot();">Take photo 1</button>
                </div>

                <div class="col-sm-4 col-md-4 col-lg-4" style="padding-left: 2px;padding-right: 0px;">
                    <canvas  id="myCanvas2" style="width: 100%;" width="100" height="75"></canvas> 
                    <button id="startbutton2" style="width: 100%;" onclick="snapshot2();">Take photo 2</button> 
                </div>

                <div class="col-sm-4 col-md-4 col-lg-4" style="padding-left: 2px;padding-right: 0px;">
                    <canvas id="myCanvas3" style="width: 100%;" width="100" height="75"></canvas>
                    <button id="startbutton3" onclick="snapshot3();" style="width: 100%;">Take photo 3</button>
                </div>
            </div>
        </div>
        <div class="row" style="text-align: right;">
         <div class="col-sm-12 col-md-12 col-lg-12">
             <span style="color:red;" class="errormsgforimageuploadwebcam"></span>
             <form action="{{URL('/')}}/hpUser/uploadimageByWebcam" onsubmit=" return save_img_webcam()" method="post" class="form">
                {{ csrf_field() }}
                <input type="hidden" name="image1" class="form-control image1">
                <input type="hidden" name="image2" class="form-control image2">
                <input type="hidden" name="image3" class="form-control image3">
                <br>
                <input type="type" name="serial_number" class="form-control serial_numberid" placeholder="Please enter serial number">
                <span style="color:red" class="megerrorserialnowebcam"></span>
                <input type="hidden" name="quote_id" class="" value="" id="quoteId">
                <button type="submit" class="btn grey save-img" style="margin-top: 15px;"> Save Image </button>
            </form>
        </div>

    </div>

</div>
</div>
</div>
</div>
<!--==============================Modal Popup=======================--> 


<!--==============================Update price margin adjustment=======================-->
<div class="modal modal-success fade in" id="update-price-margin-adjustment" style="display: none;">
    <div class="modal-dialog" role="document" style="margin: 150px auto;">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #56bdf1;">
                <button type="button" id="close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Margin Adjust Details</h4>
            </div>
            <div class="modal-body" style="height: auto;overflow: auto;">
                
             <span style="color:red;" class="errormsgforimageupload"></span>
             <form action="{{URL('/')}}/quote-managements/save-price-margin" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" id="item-id" name="item_id">
                <div class="row">

                    <div class="col-sm-12 col-md-12 col-lg-12 form-group">
                        <label class="col-sm-3 control-label">Screen Size Adj :</label>
                        <div class="col-sm-9 col-md-9">
                            <input type="text" name="screen_margin" id="screen-margin" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12 form-group">
                        <label class="col-sm-3 control-label">Brand Adj:</label>
                        <div class="col-sm-9 col-md-9">
                            <input type="text" id="brand-margin" name="brand_margin" class="form-control">
                        </div>
                    </div>    
                    <div class="col-sm-12 col-md-12 col-lg-12 form-group">
                        <label class="col-sm-3 control-label">Country Adj:</label>
                        <div class="col-sm-9 col-md-9">
                            <input type="text" name="country_margin" id="country-margin" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12 form-group">
                        <label class="col-sm-3 control-label">Client Adj:</label>
                        <div class="col-sm-9 col-md-9">
                            <input type="text" name="client_margin" id="client-margin" class="form-control">
                        </div>
                    </div>

					<div class="col-sm-12 col-md-12 col-lg-12 form-group">&nbsp;</div>

                    <div class="col-sm-12 col-md-12 col-lg-12 form-group">
                        <label class="col-sm-3 control-label">Logistics Margin:</label>
                        <div class="col-sm-9 col-md-9">
                            <input type="text" name="logistic_margin" id="logistic-margin" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12 form-group">
                        <label class="col-sm-3 control-label">Logistics Product Margin :</label>
                        <div class="col-sm-9 col-md-9">
                            <input type="text" name="logistic_product_margin" id="logistic-product-margin" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12 form-group">
                        <label class="col-sm-3 control-label">Services Margin:</label>
                        <div class="col-sm-9 col-md-9">
                            <input type="text" name="service_margin" id="service-margin" class="form-control">
                        </div>
                    </div>
   

                    <div class="col-sm-12 col-md-12 col-lg-12 form-group">
                        <div class="col-sm-4 col-md-4 col-sm-offset-4 col-md-offset-4">
                            <input type="Submit" value="Submit" class="btn btn-block" style="background-color:#56bdf1 !important;color:white;">
                        </div>
                    </div>
                </div>
        </form>
    </div>
</div>
</div>
</div>
<form action="{{URL::to('/')}}/quote/history_list/" method="get" class="navbar-form" id="formPerPage">
    <input type="hidden" value="" name="perPage" id="perPage">
</form>
<!--==============================Modal Popup=======================--> 
<input type="hidden" name="{{@$Quotes['status']}}" id="{{implode(',',$ids)}}" class="status">
<input type="hidden" id="quote_id" value="{{@$Quotes['id']}}">
<input type="hidden" name="" value="{{URL('/')}}" id="siteurl">
<div id="loadingDiv" style="display: none;">
    <div>
        <div class="loader"></div>
    </div>
</div>

@section('js')
<script src="{{ asset('js/QuoteDetails.js')}}"></script>
<script src="{{ asset('js/canvas.js')}}"></script>
<script>

    $(document).ready(function () {
        $('#country').on('change', function () {
            var countryId = $(this).val();
            var v_token = "{{csrf_token()}}";
            if (countryId != '') {

                $.ajax({
                    type: 'post',
                    url: SITE_URL + '/EnterpriseQuote',
                    data: {id: countryId, _token: v_token},
                    dataType: 'JSON',
                    success: function (response) {
                        var option1 = '';
                        var option = "";
                        if(countryId != 13){
                            option += "<option value='US$'> US$ </option>";
                        }

                        for (var item in response) {
                            var obj1 = response[item];
                            option1 += '<option value="' + obj1.region_id + '"> ' + obj1.region + ' </option>';
                        }
                        option += '<option selected="selected" value="' + obj1.currency_symbol + '"> ' + obj1.currency_symbol + ' </option>';
                        $('#region').html(option1);
                        $('#currency').html(option);
                    },
                    error: function (error) {
                        alert('error block');
                    }
                });
            }
        });

    });
	</script>
@endsection

@endsection
