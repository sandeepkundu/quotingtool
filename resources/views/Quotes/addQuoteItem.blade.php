@extends('layouts.default') @section('content')
<style>
    .form-control:focus{
        border: 1px solid #66afe9 !important;
    }
    .bg-warning{
        background-color:#00B1E1 !important;
        border:1px solid #00B1E1 !important;
    }
    input.no-border-right:focus, textarea.no-border-right:focus{
        border: 1px solid #66afe9 !important;
    }
</style>
<div class="body-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">ADD NEW QUOTE STEP ITEM</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-body no-padding">
                    <form class="form-horizontal mt-10" id="commentForm" method="post" action="{{URL::to('/')}}/Quotes/insertQuoteStepItem" >
                        {{ csrf_field() }}
                        <input type="hidden" value="{{ $id }}" name="quote_step_id">
                        <div class="form-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="group_code" class="col-sm-3 control-label">Group Code<span style="color:red;">*</span></label>
                                            <div class="col-sm-9">
                                                <input value="{!!old('group_code')!!}" class=" form-control" name="group_code" type="text" placeholder="Enter Quote step Item name"> 
                                                @if ($errors->has('group_code'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('group_code') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="order" class="col-sm-3 control-label">Order<span style="color:red;">*</span></label>
                                            <div class="col-sm-9" style="padding-bottom:20px;">
                                                <input value="{!!old('order')!!}" class=" form-control" name="order" type="text" placeholder="Enter order of Quote step Item"> 
                                                @if ($errors->has('order'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('order') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"></div>
                        </div>
                        <div class="form-footer">
                            <div class="col-sm-offset-5">
                                <button class="btn btn-color btn-hover" type="submit">Submit</button>
                                <a class="btn btn-danger" href="{{URL::to('/')}}/Quotes/listQuote/{{$id}}">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
