@extends('layouts.default') 
@section('content')

@section('css')
<!-- START @PAGE LEVEL STYLES -->
<link href="{{asset('assets/global/plugins/bower_components/datatables/css/dataTables.bootstrap.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/bower_components/datatables/css/datatables.responsive.css')}}" rel="stylesheet">
<!--/ END PAGE LEVEL STYLES -->
@endsection

<style>
  
    .form-control:focus{
        border: 1px solid #66afe9 !important;
    }
    .bg-warning{
        background-color:#00B1E1 !important;
        border:1px solid #00B1E1 !important;
    }
    input.no-border-right:focus, textarea.no-border-right:focus{
        border: 1px solid #66afe9 !important;
    }
    .table-success tbody tr td .sorting_1 {
        background: red !important;
        color: white;
        border-bottom: 1px solid red !important;
    }
    .dataTable thead tr th:first-child {
        min-width: 5px !important;
    }
    .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
        background-color: #178aa9 !important;
        border: 1px solid #178aa9;
    }
    table.dataTable thead th {
        position: relative;
        background-image: none !important;
    }
    table.dataTable thead th.sorting:after,
    table.dataTable thead th.sorting_asc:after,
    table.dataTable thead th.sorting_desc:after {
        position: absolute;
        top: 12px;
        right: 8px;
        display: block;
        font-family: FontAwesome;
    }
    table.dataTable thead th.sorting:after {
        content: "\f0dc";
        color: #ddd;
        font-size: 0.8em;
        padding-top: 0.12em;
    }
    table.dataTable thead th.sorting_asc:after {
        content: "\f0de";
    }
    table.dataTable thead th.sorting_desc:after {
        content: "\f0dd";
    }

</style>
@if(isset($toast['message']))
{{ $toast['message'] }}
@endif
<div class="header-content">
    <h2><i class="fa fa-table"></i>QUOTE STEP<span></span></h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label"></span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="{{URL::to('/')}}/admin/dashboard">Dashboard</a>
            </li>
            <li>

            </li>
        </ol>
    </div>
</div>
<div class="body-content animated fadeIn">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-tab panel-tab-double shadow">
                <div class="panel-body no-padding">
                    <div class="panel panel-default shadow no-margin">
                        <div class="panel-heading">
                            <a  href="{{URL::to('/')}}/Quotes/listQuoteStep" class="btn btn-default tooltips" data-toggle="tooltip" data-placement="top" data-title="Reload" data-action="refresh" data-original-title="" title=""><i class="icon-refresh icons"></i></a>
                            <a href="javascript:void(0);" class="btn btn-danger tooltips" data-toggle="tooltip" data-placement="top" data-title="Delete" data-original-title="" title="" onclick="return multiId()" ><i class="icon-trash icons"></i></a>
                            <div class="pull-right">
                                <a class="btn btn-color btn-hover pull-right" href="{{URL::to('/')}}/Quotes/addQuoteStep"><i class="fa fa-plus"></i>&nbsp;Add new quote step</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="tab-client-all">
                                    <div id="datatable-client-all_wrapper" class="dataTables_wrapper form-inline">

                                        <form action="{{URL::to('/') }}/Quotes/multipleDelete" method="post" class="multipleDeleteForm">
                                            {{ csrf_field() }}
                                            <table id="datatable-ajax" class="table table-striped table-success">
                                                <thead>
                                                    <tr role="row">
                                                        <th style="width:10px !important; text-align:center; "><input type="checkbox" class="multiIdCheck"/></th>

                                                        <th>Client </th>
                                                        <th> Step Name</th>
                                                        <th>Product Type </th>
                                                        <th> Brand</th>
                                                        <th class="text-center" style="width: 108px !important;">Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    @if (count($QuoteType) > 0) 
                                                    @foreach ($QuoteType as $Quote) 
                                                    <tr>
                                                        <td style="text-align:center;">
                                                            <input type="checkbox" class="id" name="ids[]" value="{{$Quote->id}}">
                                                        </td>
                                                        <td>
                                                            @if(!empty($Quote->Client->fullpath))
                                                            {{$Quote->Client->fullpath}}
                                                            @else
                                                            Default
                                                            @endif	
                                                        </td>
                                                        <td>
                                                            @if(!empty($Quote->name))
                                                            {{$Quote->name}}
                                                            @else
                                                            N/A
                                                            @endif
                                                        </td>
                                                        <td>

                                                            @if(!empty($Quote->Type->name))
                                                            {{$Quote->Type->name}}
                                                            @else
                                                            N/A
                                                            @endif
                                                        </td>
                                                        <td>

                                                            @if(!empty($Quote->Brand->name))
                                                            {{$Quote->Brand->name}}
                                                            @else
                                                            N/A
                                                            @endif
                                                        </td>
                                                        <td class="">
                                                            <a class="btn btn-round btn-color btn-hover pull-left" href="{{URL::to('/')}}/Quotes/editQuoteStep/{{$Quote->id}}" style="padding: 2px 8px;">
                                                                <i class="fa fa-edit"></i>
                                                            </a>
                                                            <a onclick="return confirm('Are you sure you want to delete?')" class="btn btn-round btn-danger"  style="padding: 2px 8px;" href="{{URL::to('/')}}/Quotes/Delete/{{$Quote->id}}">
                                                                <i class="fa fa-trash-o"></i>
                                                            </a>
                                                        </td>
                                                    </tr>

                                                    @endforeach
                                                    @else

                                                    <tr><td colspan='8' style='color:red;'> No records found!!!</td></tr>
                                                    @endif


                                                </tbody>
                                                <tfoot>
                                                    <tr role="row">
                                                        <th class="text-center" style="width: 10px !important;"><input type="checkbox" class="multiIdCheck"/></th>
                                                        <th>Client </th>
                                                        <th> Step Name</th>
                                                        <th>Product Type </th>
                                                        <th> Brand</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>	
@section('js')
<script type="text/javascript">
    $(document).ready(function () {
        $('#datatable-ajax').DataTable({
            language: {
                searchPlaceholder: "Search records"
            },
            "aoColumnDefs": [
                {'bSortable': false, 'aTargets': [-1, -6]}
            ],
            "lengthMenu": [[30, 60, 90, -1], [30, 60, 90, "All"]],
          "order": [[1, "asc"]],
            "columnDefs": [{
                    "targets": 0,
                    "orderable": false,
                    'min-width': '10px',
                    "width": "5%"
                }],
            "bSortClasses": false,
            "searchHighlight": true,
        });
    });

    function multiId() {
        var checkboxcount = jQuery('.id:checked').length;
        if (checkboxcount != 0) {
            if (confirm("Are you sure you want to delete ?")) {
                jQuery('.multipleDeleteForm').submit();
            }
        } else {
            alert("Please select a record")
            return false;
        }
    }
</script>
<!-- START @PAGE LEVEL PLUGINS -->
<script src="{{asset('assets/global/plugins/bower_components/datatables/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/global/plugins/bower_components/datatables/js/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('assets/global/plugins/bower_components/datatables/js/datatables.responsive.js')}}"></script>
<!--/ END PAGE LEVEL PLUGINS -->
@endsection
@endsection