@extends('layouts.default') @section('content')
 <style>
.form-control:focus{
	border: 1px solid #66afe9 !important;
}
.bg-warning{
	background-color:#00B1E1 !important;
	border:1px solid #00B1E1 !important;
}
input.no-border-right:focus, textarea.no-border-right:focus{
	border: 1px solid #66afe9 !important;
}
</style>
<div class="body-content animated fadeIn">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel rounded shadow">
					<div class="panel-heading">
						<div class="pull-left">
							<h3 class="panel-title">ADD NEW QUOTE</h3>
						</div>
						<div class="clearfix"></div>
					</div>
					
					<div class="panel-body no-padding">
						<form class="form-horizontal mt-10" id="commentForm" method="post" action="{{URL::to('/')}}/quote/insertQuote" >
							{{ csrf_field() }}
							<div class="form-body">
								<div class="form-group">
									<div class="row">
										
										<div class="col-lg-12">
											<div class="col-lg-6">
												<label for="Product Types" class="col-sm-3 control-label">Product Types<span style="color:red;">*</span></label>
												<div class="col-sm-9" style="padding-bottom:20px;">
											
													<select id="group" name="group" class="form-control" >
														<option value=""> -- Select Product Types -- </option>
														@if(count($QuoteStep)>0)
															@foreach ($QuoteStep as $code)
																<option value="{{ $code->id }}">{{ $code->name }}</option>
															@endforeach
														@endif	
													</select>
												</div>
											</div>
										</div>
										<div class="col-lg-12 brand">
										
										</div>
										
									</div>
								</div>
								<div class="form-group"></div>
							</div>
							<!--div class="form-footer">
								<div class="col-sm-offset-6">
									<button class="btn btn-success" type="submit">Save</button>
									<a class="btn btn-default" href="{{URL::to('/')}}/Quotes/index">Cancel</a>
								</div>
							</div-->
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
<form id="commentForm" method="post" action="{{URL::to('/')}}/quote/listQuote" >
@section('js')
	<script>
		$("#group").change(function(){
			var ProType = $(this).val();
			
			if(ProType!=''){
				window.location = SITE_URL+'/Quotes/listQuote/'+ProType;
			}
		});
	</script>
@endsection
@endsection