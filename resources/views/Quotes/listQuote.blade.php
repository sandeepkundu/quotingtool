@extends('layouts.default') @section('content')

<style>
.form-control:focus{
	border: 1px solid #66afe9 !important;
}
</style>
 <style>
.form-control:focus{
	border: 1px solid #66afe9 !important;
}
.bg-warning{
	background-color:#00B1E1 !important;
	border:1px solid #00B1E1 !important;
}
input.no-border-right:focus, textarea.no-border-right:focus{
	border: 1px solid #66afe9 !important;
}
</style>

  <!-- START @PAGE CONTENT -->

 	 @if(isset($toast['message']))
		  {{ $toast['message'] }}
	@endif
 	<div class="header-content">
        <h2><i class="fa fa-table"></i>Quote Item List For : {{ $Quo->name }} <span></span></h2>
       <div class="breadcrumb-wrapper hidden-xs">
            <span class="label"></span>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="{{URL::to('/')}}/dashboard">Dashboard</a>
                   <!--  <i class="fa fa-angle-right"></i> -->
				</li>
                <li>
                  <!--   Quote Item <i class="fa fa-angle-right"></i> index -->
                </li>
            </ol>
        </div><!-- /.header-content -->
    </div><!--/ End page header -->
<div class="body-content animated fadeIn">
   <!-- Start body content -->
		<div class="row">
			<div class="col-md-12">
                <div class="panel panel-tab panel-tab-double shadow">
                    <div class="panel-body no-padding">
                        <div class="panel panel-default shadow no-margin">
                            <div class="panel-heading">
								<a  href="{{URL::to('/')}}/Quotes/addQuote" class="btn btn-color btn-hover"><i class="glyphicon glyphicon-chevron-left"> </i>&nbsp;Back</a>
								<div class="pull-right" id="mt0">
									<a class="btn btn-color btn-hover pull-right" href="{{URL::to('/')}}/Quotes/addQuoteItem/{{ $Quo->id }}"><i class="fa fa-plus"></i>&nbsp;Add new Quote Item</a>
								</div>
								<div class="clearfix"></div>
                            </div>
							<div class="panel-body">
								<div class="tab-content">
									<div class="tab-pane fade in active" id="tab-client-all">
										<div id="datatable-client-all_wrapper" class="dataTables_wrapper form-inline">
											<!--div class="row">
												<div class="col-xs-6">
													<div class="dataTables_length" id="datatable-client-all_length">
													<label>
													<select name="datatable-client-all_length" class="form-control input-sm" id="paginationPerPage">
														<?php //$pa=array(5,10,15,20,25,30,35,40,45,50); ?>
													</select> Records per page</label></div>
												</div>
												<div class="col-xs-6">
													<div id="datatable-client-all_filter" class="dataTables_filter">
													</div>
													<form action="{{URL::to('/')}}/clients/index" method="get" class="navbar-form">
														<div class="form-group has-feedback pull-right" style="margin-right:-15px !important;margin-top:-13px;">
															<input type="text" class="form-control rounded"
																placeholder="Search by name and type" name="search">
															<button type="submit" class="btn btn-color  btn-hover btn-theme fa fa-search form-control-feedback rounded"></button>
														</div>
													</form>
												</div>
											</div-->
											<table class="table table-striped table-success table-middle table-project-clients " role="grid" aria-describedby="datatable-client-all_info">
												<thead>
													<tr role="row">
														<!-- <th class="text-center" style="width: 55px !important;">Sr. no</th> -->
														<th> Group Code</th>
														<th>Order</th>
														<th style="width: 108px !important;">Actions</th>
													</tr>
												</thead>
												<tbody>
													<?php $i=0;
													if(isset($QuoteStepItem) && count($QuoteStepItem)>0){
														foreach($QuoteStepItem as $user){  $i++; ?>

													<tr role="row">
														<!-- <td scope="row">{{$i}}</td> -->
														<td>{{$user->group_code}}</td>
														<td>{{$user->order}}</td>
														<td class="">
															<a class="btn btn-round btn-color btn-hover" href="{{URL::to('/')}}/Quotes/editQuoteItem/{{$user->id}}/{{ $Quo->id }}" style="padding: 0px 6px">
																<i class="fa fa-edit"></i>
															</a>
															<!--a onclick="return confirm('Are you sure you want to delete?')" class="btn btn-round btn-danger"  style="padding: 0px 6px" href="{{URL::to('/')}}/Quotes/Delete/{{$user->id}}">
																<i class="fa fa-trash-o"></i>
															</a-->
														</td>
													</tr>
													<?php } }else{
														echo '<tr role="row"><td colspan="9" style="color:red;">  No records found!!!</td></tr>';
													} ?>
												</tbody>
												<tfoot>
													<tr role="row">
														<!-- <th class="text-center" style="width: 77px !important">Sr. no</th> -->
														<th> Group Code</th>
														<th>Order</th>
														<th style="width: 108px !important;">Actions</th>
													</tr>
												</tfoot>
											</table>

											<div class="row">
												<div class="col-xs-6">
													<div class="dataTables_info" id="datatable-client-all_info" role="status" aria-live="polite">
														@if($QuoteStepItem->total()!=0)
															Showing {{ $QuoteStepItem->firstItem() }} to {{ $QuoteStepItem->lastItem() }} of {{ $QuoteStepItem->total() }}
														@endif
													</div>
												</div>
												<div class="col-xs-6">
													<div class="dataTables_paginate paging_simple_numbers" id="datatable-client-all_paginate">
													   {{ $QuoteStepItem->links() }}
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	<style>
		.pagination{
			float: right;
			margin:0px;
		}
		#tour-3{
			margin-bottom: 0px;
		}
		.padding_bottom{
			padding-bottom: 15px;
		}
	</style>

@endsection