@extends('layouts.default') @section('content')
<!-- START @PAGE LEVEL STYLES -->
<link href="{{ asset('css/bower_components/fontawesome/css/font-awesome.min.css')}}" rel="stylesheet">
<link href="{{ asset('css/bower_components/animate.css/animate.min.css')}}" rel="stylesheet">
<!--/ END PAGE LEVEL STYLES -->
<style>
    .form-control:focus{
        border: 1px solid #66afe9 !important;
    }
    .bg-warning{
        background-color:#00B1E1 !important;
        border:1px solid #00B1E1 !important;
    }
    input.no-border-right:focus, textarea.no-border-right:focus{
        border: 1px solid #66afe9 !important;
    }
</style>
<div class="body-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">ADD NEW QUOTE STEP</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-body no-padding">
                    <form class="form-horizontal mt-10" id="commentForm" method="post" action="{{URL::to('/')}}/Quotes/insertQuote" >
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-12">

                                        <div class="col-lg-6">
                                            <label for="parent_id" class="col-sm-3 control-label">Clients</label>
                                            <div class="col-sm-9">
                                                <div class="btn-group hierarchy-select" data-resize="auto" id="example-one">
                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <span class="selected-label pull-left">&nbsp;</span>
                                                        <span class="caret"></span>
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                    </button>
                                                    <div class="dropdown-menu open">
                                                        <div class="hs-searchbox">
                                                            <input type="text" class="form-control" autocomplete="off">
                                                        </div>
                                                        <ul class="dropdown-menu inner store" role="menu">
                                                            <li data-value="0" data-level="1" class="aa" data-default-selected="">
                                                                <a href="#">Select Client</a>
                                                            
                                                            </li>
                                                            @foreach($parent_ids as $val)
                                                            <li class="par" data-value="{{$val['id']}}" data-level="{{$val['level']}}">
                                                                <a href="#">{{$val['name']}}</a>
                                                            </li>
                                                            @endforeach
                                                        </ul>

                                                        <ul class="dropdown-menu inner enterprise" style="display: none;" role="menu" >
                                                            <li data-value="0" data-level="1" class="aa">
                                                                <a href="#">Select Client</a>
                                                            </li>
                                                        </ul>

                                                    </div>
                                                    <input class="hidden hidden-field" name="parent_id" id="parent_id" aria-hidden="true" type="text"/>
                                                </div>			
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="name" class="col-sm-3 control-label">Step Name<span style="color:red;">*</span></label>
                                            <div class="col-sm-9">
                                                <input value="{!!old('name')!!}" class=" form-control" id="name" name="name" minlength="2" type="text" placeholder="Enter Quote step name"> 
                                                @if ($errors->has('name'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="type_id" class="col-sm-3 control-label">Type<span style="color:red;">*</span></label>
                                            <div class="col-sm-9" style="padding-bottom:20px;">
                                                <select id="type_id" name="type_id" class="form-control" >
                                                    <option value=""> -- Select Product Type -- </option>
                                                    @if(count($type_id)>0)
                                                    @foreach ($type_id as $code)
                                                    <option value="{{ $code->id }}" {{(old("type_id") == $code->id) ? "selected":"" }}>{{ $code->name }}</option>
                                                    @endforeach
                                                    @endif
                                                </select>

                                                @if ($errors->has('type_id'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('type_id') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="brand_id" class="col-sm-3 control-label">Brand</label>
                                            <div class="col-sm-9">
                                                <select id="brand_id" name="brand_id" class="form-control" >
                                                    <option value=""> -- Select Brand -- </option>
                                                    @if(count($brand_id)>0)
                                                    @foreach ($brand_id as $code)
                                                    <option value="{{ $code->id }}"  {{ (old("brand_id") == $code->id) ? "selected":"" }}>{{ $code->name }}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                                @if ($errors->has('brand_id'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('brand_id') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>

                                    </div>
                                    <!-- ===================================ADD QUOTE STEP ITEMS============================================= -->
                                    <br/><div class="col-sm-12">
                                        <div class="panel-heading">
                                            <div class="pull-left">
                                                <h3 class="panel-title">ADD QUOTE STEP ITEMS</h3>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div><br/>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="col-lg-5">
                                            <label for="group_code" class="col-sm-3 control-label">Group Code<span style="color:red;">*</span></label>
                                            <div class="col-sm-9">
                                                    <!-- <input class=" form-control" id="group_code" type="text" placeholder="Enter Quote step Item name"> --> 
                                                <select id="group_code" name="group_code" class="form-control">
                                                    <option value="">--Select Group Name--</option>
                                                    @foreach($group_codes as $group_code)
                                                    <option value="{{$group_code->group_code}}">{{$group_code->group }} - {{$group_code->group_code}}</option>
                                                    @endforeach	
                                                </select>

                                                <span class="help-block" style="color:red;">
                                                    <strong id="group_code_error"></strong>
                                                </span>
                                                @if ($errors->has('group_code'))
                                                <span class="help-block" style="color:red;">
                                                    <strong id="err1">{{ $errors->first('group_code') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                        <div class="col-lg-5">
                                            <label for="order" class="col-sm-3 control-label">Step Number<span style="color:red;">*</span></label>
                                            <div class="col-sm-9" style="padding-bottom:20px;">

                                                <select id="order" class="form-control">

                                                    <option value="">--Select Step Number--</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>

                                                </select>


                                                <span class="help-block" style="color:red;">
                                                    <strong id="order_error"></strong>
                                                </span>
                                                @if ($errors->has('order'))
                                                <span class="help-block" style="color:red;">
                                                    <strong id="err2">{{ $errors->first('order') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                        <div class="col-lg-2">										
                                            <div class="col-sm-9" style="padding-bottom:20px; ">
                                                <button type="button" id="add_quote_item_btn" class="btn btn-color btn-hover">Add</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table-responsive mb-20 col-sm-12">
                                        <button type="button" style="display:none;margin-bottom: 10px;" class="delete-row btn btn-danger" style="margin-bottom:4px;">Delete Row</button>
                                        <table class="table table-striped table-success table-middle table-project-clients region_table" role="grid" aria-describedby="datatable-client-all_info" style="display: none;">
                                            <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>Group Code</th>
                                                    <th class="text-center" style="width: 108px !important;">Step Number</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>

                                    </div>

                                    <!-- =======================QUOTE STEP ITEMS========================= -->	
                                </div>
                            </div>
                            <div class="form-group"></div>
                        </div>
                        <div class="form-footer">
                            <div class="col-sm-offset-5">
                                <button class="btn btn-color btn-hover" type="submit">Submit</button>
                                <a class="btn btn-danger" href="{{URL::to('/')}}/Quotes/listQuoteStep">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!--==============================Modal Popup=======================-->
<!-- 	<div class="modal modal-success fade in" id="modal-bootstrap-tour" tabindex="-1" role="dialog" style="display: none;">
                <div class="modal-dialog" role="document" style="margin: 150px auto;">
                        <div class="modal-content">
                                <div class="modal-header">
                                        <button type="button" id="close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        <h4 class="modal-title"><span class="username">Warning!!!</span></h4>
                                </div>
                                <div class="modal-body">
                                        You can not make the parent of this store because this is already child of another store.

                                </div>
                        </div>
                </div>
        </div> -->
<!--==============================Modal Popup=======================--> 
<style type="text/css">
    #ave_collect_size{
        border-right:1px solid rgb(221, 221, 221) !important;
    }
    #ave_collect_size:focus{
        border-right:1px solid #66afe9 !important
    }
    #example-one{
        width: 100%;
    }
    .dropdown-menu li.active:hover a, .dropdown-menu li.active:focus a, .dropdown-menu li.active:active a{
        background-color:#00B1E1 !important;
    }
    .dropdown-menu li.active a{
        background-color:#00B1E1 !important;
    }
    .btn-default.dropdown-toggle.btn-default{
        background-color: white;
    }
    .btn-default.active.focus, .btn-default.active:focus, .btn-default.active:hover, .btn-default:active.focus, .btn-default:active:focus, .btn-default:active:hover, .open>.dropdown-toggle.btn-default.focus, .open>.dropdown-toggle.btn-default:focus, .open>.dropdown-toggle.btn-default:hover{
        background-color: white;
    }
    .dropdown-menu li > a:hover:before{
        border-left:3px solid #00B1E1;
    }
    .modal-success .modal-header {
        background-color: #00B1E1 !important;
        border:1px solid #00B1E1;
        border:none;
    }
    .modal-success .modal-header:before {
        content: ""; 
        border: 1px solid #00B1E1 !important; 
    }
</style>		
<link href="{{asset('css/hierarchy-select.min.css')}}" rel="stylesheet">
@section('js')
<script src="{{ asset('js/hierarchy-select.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function () {
    $('#client_type').on('change', function () {
        var type = this.value;
        if (type == 'enterprise') {
            $('.store').css('display', 'none');
            $('.enterprise').css('display', 'block');
        }
        if (type == 'store') {
            $('.enterprise').css('display', 'none');
            $('.store').css('display', 'block');
        }
        if (type == '') {
            $('.enterprise').css('display', 'none');
            $('.store').css('display', 'block');
        }
    });
    $('#example-one').hierarchySelect();
    $('.par').click(function () {
        var a = $(this).attr('data-level');
        if (a == 3) {
            setTimeout(function () {

                $('#modal-bootstrap-tour').modal('show');
                $('.selected-label').text('Select Client');
                $('#parent_id').val('0');

                $("li").removeClass("active");
                $('.aa').addClass("active");

            }, 200);

        }
    });
});
</script>

<script>
    $(document).ready(function () {
        /*$('#group_code').keypress(function(e){
         if(e.which == 13) {
         $("#add_quote_item_btn").click();
         return false;
         }
         });*/
        $("#add_quote_item_btn").click(function () {
            var group_code = $("#group_code").val();
            var order = $("#order").val();

            var group_code_hidden = $('.group_code_hidden').val();
            var order_hidden = $('.order_hidden').val();

            var row = "<tr class='country_rgn'><td><input type='checkbox' name='record'></td><td>" + group_code + "<input type='hidden' name='group_code[]' class='group_code_hidden' value='" + group_code + "'/>" + "</td><td>" + order + "<input type='hidden' name='order[]' class='order_hidden' value='" + order + "'/>" + "</td></tr>";
            var gc = [];
            var go = [];
            $(".group_code_hidden").each(function(){
                gc.push($(this).val());
            });

            $(".order_hidden").each(function(){
                go.push($(this).val());
            });

            if (group_code == "") {
                $('#group_code_error').text('The group field is required');
                $('#group_code').focus();
                return false;

            } else if (order == '') {
                $('#order_error').text('The order field is required');
                $('#order').focus();
                $("#group_code_error").text('');
                return false;
            } else if (!$.isNumeric(order)) {
                $('#order_error').text('The order field must be numeric');
                $('#order').focus();
                $("#group_code_error").text('');

            } else if($.inArray(group_code , gc)  !== -1 ){
                $('#group_code_error').text('Group code is already exist');
                $('#group_code').focus();
                 $('#order_error').text('');
                return false;

            } /*else if($.inArray(order , go)  !== -1 ){
                $('#order_error').text('Order is already exist');
                 $('#order').focus();
                $("#group_code_error").text('');
                return false;

            }*/ else {
                $('.delete-row').show();
                $('.region_table').show();
                $('#c_region').text('');
                $("#group_code").val('');
                $("#order").val('');
                $("table tbody").append(row);
                $("#order_error").text('');
            };

            if (group_code_hidden != '') {
                $("#err1").text('');
            }
            if (order_hidden != '') {
                $("#err2").text('');
            }

        });

        // Find and remove selected table rows
        $(".delete-row").click(function () {
            $("table tbody").find('input[name="record"]').each(function () {
                if ($(this).is(":checked")) {
                    $(this).parents("tr").remove();
                }
            });
        });
    });

</script>
<!-- <script>
        $(document).ready(function(){
                $('#type_id').on('change', function() {

                        var v_token = "{{csrf_token()}}";
                        var type_id = $(this).val();

                        if(type_id != ''){
                                alert(type_id);
                                $.ajax({
                                        type:'post',
                                        url: SITE_URL+'/add_quote_step_items',
                                        data:{id:type_id,_token:v_token},
                                        dataType:'JSON',
                                        success:function(response){
                                                var option='';
                                                for (var item in response) {
                                                        var obj1 = response[item];
                                                        option += '<option value="' + obj1.group_code + '"> ' + obj1.group_code + ' </option>';
                                                }
                                                $('#group_code').html(option);
                                        },
                                        error:function(error){
                                                alert('error block');
                                        }
                                });
                        }
                });
        });
        
</script> -->

@endsection
@endsection
