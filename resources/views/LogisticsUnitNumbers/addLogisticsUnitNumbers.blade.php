@extends('layouts.default')
 @section('content')
 <style>
.form-control:focus{
	border: 1px solid #66afe9 !important;
}
.bg-warning{
	background-color:#00B1E1 !important;
	border:1px solid #00B1E1 !important;
}
input.no-border-right:focus, textarea.no-border-right:focus{
	border: 1px solid #66afe9 !important;
}
</style>
 @if(isset($toast['message']))
		  {{ $toast['message'] }}
	@endif
 <div class="body-content animated fadeIn">
	<div class="row">
			<div class="col-lg-12">
				<div class="panel rounded shadow">
					<div class="panel-heading">
						<div class="pull-left">
							<h3 class="panel-title">ADD LOGISTICS VOLUMES</h3>
						</div>
						<div class="clearfix"></div>
					</div>
					
					<div class="panel-body no-padding">
						<form class="form-horizontal mt-10" id="commentForm" method="post" action="{{URL::to('/')}}/LogisticsUnitNumbers/insertLogisticsUnitNumbers">
							{{ csrf_field() }}
							<div class="form-body">
								<div class="form-group">
									<div class="row">

										<div class="col-lg-12">
											<div class="col-lg-6">
											<label for="ValueFrom" class="col-sm-3 control-label">Value From<span style="color:red;">*</span></label>
												<div class="col-sm-9">
													<input value="{{old('value_from')}}" type="text" name="value_from" class="form-control" id="value_from" placeholder="Value From">
														
													@if ($errors->has('value_from'))
														<span class="help-block" style="color:red;">
															<strong>{{ $errors->first('value_from') }}</strong>
														</span>
													@endif
													&nbsp
												</div>
											</div>
											<div class="col-lg-6">
												<label for="ValueTo" class="col-sm-3 control-label">Value To<span style="color:red;">*</span></label>
												<div class="col-sm-9">
													<input value="{{old('value_to')}}" type="text" name="value_to" class="form-control" id="value_to" placeholder="Value To">
													@if ($errors->has('value_to'))
														<span class="help-block" style="color:red;">
															<strong>{{ $errors->first('value_to') }}</strong>
														</span>
													@endif
													&nbsp
												</div>
												
											</div>
										</div>
									
									</div>
								</div>
								<div class="form-group"></div>
							</div>
							<div class="form-footer">
								<div class="col-sm-offset-6">
									<button class="btn btn-color btn-hover" type="submit">Submit</button>
									<a class="btn btn-danger" href="{{URL::to('/')}}/LogisticsUnitNumbers/index">Cancel</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
</div>
@endsection
