@extends('layouts.default') @section('content')

<link href="{{ asset('assets/global/plugins/bower_components/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/global/plugins/bower_components/animate.css/animate.min.css' )}}" rel="stylesheet">
<link href="{{ asset('assets/global/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet">
<link href="{{ asset('assets/global/plugins/bower_components/jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/global/plugins/bower_components/chosen_v1.2.0/chosen.min.css') }}" rel="stylesheet">

<!--Client Heirarchy CSS File-->
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.common-material.min.css" />
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.material.min.css" />
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.material.mobile.min.css" />
<!--Client Heirarchy CSS File-->
<link rel="stylesheet" href="{{asset('css/usersAdd.css')}}" />

<div class="body-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">ADD NEW USER</h3>
                    </div>

                    <div class="clearfix"></div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body no-padding">

                    <form class="form-horizontal mt-10" id="commentForm" method="post" action="{{URL::to('/')}}/users/insertUser" >
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="first_name" class="col-sm-3 control-label">First Name<span style="color:red;">*</span></label>
                                            <div class="col-sm-9">
                                                <input value="{!!old('first_name')!!}" class=" form-control" id="first_name" name="first_name" minlength="2" type="text" placeholder="First Name" data-toggle="tooltip" title="Name should be at least 2 charecters"> 
                                                @if ($errors->has('first_name'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('first_name') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <label for="last_name" class="col-sm-3 control-label">Last Name<span style="color:red;">*</span></label>
                                            <div class="col-sm-9">
                                                <input value="{!!old('last_name')!!}" class=" form-control" id="last_name" name="last_name" minlength="2" type="text" placeholder="Last Name"> 
                                                @if ($errors->has('last_name'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('last_name') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="company" class="col-sm-3 control-label">Company Name<span style="color:red;">*</span></label>
                                            <div class="col-sm-9">
                                                <input value="{!!old('company')!!}" type="text" class="form-control" name="company"id="company"
                                                placeholder="Company">
                                                @if ($errors->has('company'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('company') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="email" class="col-sm-3 control-label">Email Address<span style="color:red;">*</span></label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon bg-primary">@</span>
                                                    <input value="{!!old('email')!!}" type="email" name="email" class="form-control" id="email"
                                                    placeholder="Email Address">
                                                </div>
                                                @if ($errors->has('email'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="password" class="col-sm-3 control-label">Password<span style="color:red;">*</span></label>
                                            <div class="col-sm-9">

                                                <input class=" form-control" id="password" name="password" minlength="2" type="password" placeholder="Password" >

                                                @if ($errors->has('password'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="phone" class="col-sm-3 control-label">Phone Number<!-- <span style="color:red;">*</span> --></label>
                                            <div class="col-sm-9">
                                                <input value="{!!old('phone')!!}" type="text" class="form-control" name="phone" id="phone"
                                                placeholder="Phone Number">
                                                @if ($errors->has('phone'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('phone') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="country" class="col-sm-3 control-label">
                                                User Role
                                            </label>
                                            <div class="col-sm-9">
                                                <select id="userrole" name="userrole" class="form-control">
                                                    <option value=""> -- Select role -- </option>
                                                    @foreach ($userRoles as $code)
                                                    <option value="{{ $code->id }}"  {{ (old('userrole') == $code->id) ? "selected":""}}> {{ $code->name }}</option>
                                                    @endforeach             
                                                </select>
                                                @if ($errors->has('userrole'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('userrole') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <label for="theme" class="col-sm-3 control-label">
                                                Select Theme<span style="color:red;">*</span>
                                            </label>
                                            <div class="col-sm-9">
                                                <select id="Theme" name="theme" class="form-control">
                                                    <option value=""> -- Select Theme -- </option>
                                                    @if(isset($Themes) && count($Themes) > 0 )

                                                        @foreach($Themes as $Theme)
                                                            <option value="{{$Theme->id}}" {{ (old('theme') == $Theme->id) ? "selected":""}}> {{$Theme->theme_name}} </option>
                                                        @endforeach
                                                    @endif
                                                    
                                                </select>
                                                @if ($errors->has('theme'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('theme') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="client" class="col-sm-3 control-label">
                                                Clients </label>
                                                <div class="col-sm-9">
                                                    <div class="demo-section k-content">
                                                        <div id="dialog">
                                                            <div class="dialogContent">
                                                                <input id="filterText" type="text" placeholder="Search Client" />
                                                                <div class="selectAll" class='com-sm-12'>
                                                                    <input type="checkbox" id="chbAll" class="k-checkbox" onchange="chbAllOnChange()" />
                                                                    <label class="k-checkbox-label" for="chbAll">Select All</label>
                                                                    <span id="result">0 client selected</span>
                                                                </div>
                                                                <div id="treeview"></div>
                                                            </div>
                                                        </div>
                                                        <select id="multiselect"></select>

                                                        <button id="openWindow" class="k-primary btn btn-block btn-color" type="button" style="border:none;">Select Clients</button>
                                                    </div>
                                                </div>

                                                @if ($errors->has('select'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('select') }}</strong>
                                                </span>
                                                @endif
                                            </div>

                                            <div class="col-lg-6">
                                                <label class="col-sm-3 control-label">Send User Notification</label>
                                                <div class="col-sm-9">
                                                    <div class="ckbox ckbox-info">
                                                        <input id="checkbox-info12" checked="checked" type="checkbox" name="SendUserNotification" >
                                                        <label for="checkbox-info12"> Send the new user an email about their account <i class="glyphicon glyphicon-question-sign"  data-toggle="tooltip" title="By selecting this box system will send an email notification to the user"></i></label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-lg-12 col-sm-12 col-md-12" style="padding-top: 23px;">
                                            <div class="col-lg-6">
                                                <label class="col-sm-3 control-label">Enabled</label>
                                                <div class="col-sm-9">
                                                    <div class="ckbox ckbox-info">
                                                        <input id="checkbox-info4" checked="checked" type="checkbox" name="active">
                                                        <label for="checkbox-info4"></label>
                                                    </div>
                                                </div>
                                            </div>
                                            

                                            <div class="col-sm-6 Financial" style="display: none;"> 
                                                    <label for="description" class="col-sm-3 control-label" style="padding-top: 0px;">Public User's Permission</label>

                                                      <?php 
                                                        if(old('permission')){
                                                           $arrs = old('permission');
                                                           $per = array();
                                                           foreach($arrs as $arr){
                                                            $per[] = $arr;
                                                           }
                                                        }   
                                                       ?>
                                                    
                                                    <div class="col-sm-9" style="padding-left: 0px;">
                                                 
                                                        <div class="col-sm-12" style="padding-left: 0px;">
                                                            <div class="col-sm-1">
                                                              <div class="ckbox ckbox-info">
                                                                <input id="checkbox-info1" value="1" type="checkbox" name="permission[]" class="pull-right" {{ @(in_array('1',@$per)) ? "checked":""}}>
                                                                <label for="checkbox-info1"></label>
                                                              </div>
                                                            </div>
                                                            <label class="col-sm-9 control-label" style="text-align:left;">Financial Display</label>
                                                        </div>

                                                        <div class="col-sm-12" style="padding: 0px;">
                                                        
                                                            <div class="col-sm-1">
                                                              <div class="ckbox ckbox-info">
                                                                <input id="checkbox-info2" value="2" type="checkbox" name="permission[]" class="pull-right" {{ @(in_array('2',@$per)) ? "checked":""}}>
                                                                <label for="checkbox-info2"></label>
                                                              </div>
                                                            </div>
                                                             <label class="col-sm-9 control-label" style="text-align:left;">Financial Management</label>
                                                        </div>
                                                     <div class="col-sm-12" style="padding: 0px;">
                                                        
                                                        <div class="col-sm-1">
                                                          <div class="ckbox ckbox-info">
                                                            <input id="checkbox-info3"  value="3" type="checkbox" name="permission[]" class="pull-right" {{ @(in_array('3',@$per)) ? "checked":""}}>
                                                            <label for="checkbox-info3""></label>
                                                          </div>
                                                        </div>
                                                        <label class="col-sm-9 control-label" style="text-align:left;"> Collection Request</label>
                                                    </div>

                                                    @if ($errors->has('permission'))
                                                        <span class="help-block" style="color:red;">
                                                            <strong>{{ $errors->first('permission') }}</strong>
                                                        </span>
                                                    @endif
                                                    &nbsp;
                                                 </div>
                                                   
                                                </div>
                                       
                                 
                                             
                                        </div>

                                    </div>
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group"></div>
                                <!-- /.form-group -->				
                                <!-- /.form-body -->
                                <div class="form-footer">
                                    <div class="col-sm-offset-5">
                                        <!-- <i class="glyphicon glyphicon-question-sign"  data-toggle="tooltip" title="By clicking on this button you will save the form"></i> -->
                                        <button class="btn btn-color btn-hover" type="submit">Submit</button>
                                        <a class="btn btn-danger" href="{{URL::to('/')}}/users/index">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- /.panel -->
                        <!--/ End horizontal form -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="loadingDiv">
        <div>
            <div class="loader"></div>
        </div>
    </div>

    @section('js')
    <script src="{{ asset('assets/global/plugins/bower_components/chosen_v1.2.0/chosen.jquery.min.js') }}"></script>
    <script src="{{ asset('assets/global/plugins/bower_components/jquery-autosize/jquery.autosize.min.js') }}"></script>
    <script src="{{ asset('assets/global/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ asset('assets/global/plugins/bower_components/jasny-bootstrap-fileinput/js/jasny-bootstrap.fileinput.min.js') }}"></script>
    <script src="{{ asset('assets/global/plugins/bower_components/holderjs/holder.js') }}"></script>
    <script src="{{ asset('assets/global/plugins/bower_components/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>
    <script src="{{ asset('css/password.js') }}"></script>
    <script src="{{ asset('js/ClientTree.js') }}"></script>

    <!--=================================Client selection for add user==============================-->
    <script src="http://jonmiles.github.io/bootstrap-treeview/js/bootstrap-treeview.js"></script>

    <!--===========================================================================================-->
    <!--  <script src="{{asset('js/usersAdd.js')}}"></script> -->

    <script type="text/javascript">
        $(document).ready(function(){
            if($('#userrole').val() == 3){
                 $('.Financial').css('display','block');
            }else{
                $('.Financial').css('display','none');
            }
            $('#userrole').change(function(){
                var role = $(this).val();
                if(role == 3){
                    $('.Financial').css('display','block');
                }else{
                    $('.Financial').css('display','none');
                }
            })
        })

        $(".dropdown dt a").on('click', function () {
            $(".dropdown dd ul").slideToggle('fast');
        });

        $(".dropdown dd ul li a").on('click', function () {
            $(".dropdown dd ul").hide();
        });

        function getSelectedValue(id) {
            return $("#" + id).find("dt a span.value").html();
        }

        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown"))
                $(".dropdown dd ul").hide();
        });

        $('.mutliSelect input[type="checkbox"]').on('click', function () {
            var title = $(this).closest('.mutliSelect').find('input[type="checkbox"]').val(),
            title = $(this).attr('class') + ",";

            if ($(this).is(':checked')) {
                var html = '<span title="' + title + '">' + title + '</span>';
                $('.multiSel').append(html);
                $(".hida").hide();
            } else {
                $('span[title="' + title + '"]').remove();
                var ret = $(".hida");
                $('.dropdown dt a').append(ret);
            }
        });


        $('#password').password({
    // custom messages
    shortPass: 'The password is too short',
    badPass: 'Weak; try combining letters & numbers',
    goodPass: 'Medium; try using special charecters',
    strongPass: 'Strong password',
    containsUsername: 'The password contains the username',
    enterPass: 'Type your password',
        // show percent
        showPercent: true,
        // show text
        showText: true,
        // enable animation
        animate: true,
        animateSpeed: 'fast',
        // link to username
        email: false,
        usernamePartialMatch: true,
        // minimum length
        minimumLength: 4

    });



        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();

        });


        $(document).ready(function () {
            $("#client_id").chosen();

            $('#client_id').on('change', function (evt, params) {
                var id = "";
                $('.search-choice a').each(function () {
                    id = id + $(this).data('option-array-index') + ",";
                });
                $('[name="client_id"]').val(id);
            });

        });

	function clientTree(clientArray){
		var c = [];
		var i = 0;
		for (var item in clientArray) {
			var obj = clientArray[item];
			a = [];
            if (obj.items) {
				a = clientTree(obj.items);
			}
			
			c[i] = { id: clientArray[item].id, text: clientArray[item].name, expanded: true, items: a };
            i++;                    
		}
		return c;
	}

        /*==============Client Heirarchy CSS==================*/

        
        $(document).ready(function () {

            var v_token = "{{csrf_token()}}";
            $('#loadingDiv').css('display', 'block');

            $.ajax({
                type: 'post',
                url: SITE_URL + '/ClientSelect',
                data: {_token: v_token},
                dataType: 'JSON',
                success: function (obj) {
                    var s = [];
					/*
                    var a = [];
                    var c = [];
                    var i = 0;
                    for (var item in obj) {
                        j = 0;
                        var obj1 = obj[item];
                        for (var key in obj1.items) {
                            var k = 0;
                            var obj3 = obj1.items[key];
                            for (var element in obj3.items) {
                                c[k] = {id: obj3.items[element].id, text: obj3.items[element].name};
                                k++;
                            }

                            a[j] = {id: obj1.items[key].id, text: obj1.items[key].name, expanded: true, items: c};
                            j++;
                            c = [];
                        }

                        s[i] = {
                            id: obj[item].id, text: obj[item].name, expanded: true, items: a
                        };
                        i++;
                        a = [];
                    }
					*/  
					s = clientTree(obj);

                    var myDataSource = new kendo.data.HierarchicalDataSource({
                        data: s
                    });

                    $("#multiselect").kendoMultiSelect({
                        dataTextField: "text",
                        dataValueField: "id"
                    });

                    $("#treeview").kendoTreeView({
                        loadOnDemand: false,
                        checkboxes: {
                            checkChildren: false
                        },
                        dataSource: myDataSource,
                        check: onCheck,
                        expand: onExpand
                    });

                    var dialog = $("#dialog");
                    var multiSelect = $("#multiselect").data("kendoMultiSelect");
                    $("#openWindow").kendoButton();
                    multiSelect.readonly(false);

                    $("#openWindow").click(function () {
                        dialog.data("kendoDialog").open();
                        $(this).fadeOut();
                    });

                    dialog.kendoDialog({
                        width: "400px",
                        title: "Select Client",
                        visible: false,
                        actions: [
                        {
                            text: 'Cancel',
                            primary: false,
                            action: onCancelClick
                        },
                        {
                            text: 'Ok',
                            primary: true,
                            action: onOkClick
                        }
                        ],
                        close: onClose
        })//.data("kendoDialog").open();


                    function onCancelClick(e) {
                        e.sender.close();
                    }

                    function onOkClick(e) {
                        var checkedNodes = [];
                        var treeView = $("#treeview").data("kendoTreeView");

                        getCheckedNodes(treeView.dataSource.view(), checkedNodes);
                        populateMultiSelect(checkedNodes);

                        e.sender.close();
                    }

                    function onClose() {
                        $("#openWindow").fadeIn();
                    }

                    function populateMultiSelect(checkedNodes) {
                        var multiSelect = $("#multiselect").data("kendoMultiSelect");
                        multiSelect.dataSource.data([]);

                        var multiData = multiSelect.dataSource.data();
                        if (checkedNodes.length > 0) {
                            var array = multiSelect.value().slice();
                            for (var i = 0; i < checkedNodes.length; i++) {
                                multiData.push({text: checkedNodes[i].text, id: checkedNodes[i].id});
                                array.push(checkedNodes[i].id.toString());
                            }

                            multiSelect.dataSource.data(multiData);
                            multiSelect.dataSource.filter({});
                            multiSelect.value(array);
                        }
                    }

                    function checkUncheckAllNodes(nodes, checked) {
                        for (var i = 0; i < nodes.length; i++) {
                            nodes[i].set("checked", checked);

                            if (nodes[i].hasChildren) {
                                checkUncheckAllNodes(nodes[i].children.view(), checked);
                            }
                        }
                    }

                    function chbAllOnChange() {
                        var checkedNodes = [];
                        var treeView = $("#treeview").data("kendoTreeView");
                        var isAllChecked = $('#chbAll').prop("checked");

                        checkUncheckAllNodes(treeView.dataSource.view(), isAllChecked)

                        if (isAllChecked) {
                            setMessage($('#treeview input[type="checkbox"]').length);
                        }
                        else {
                            setMessage(0);
                        }
                    }

                    function getCheckedNodes(nodes, checkedNodes) {
                        var node;

                        for (var i = 0; i < nodes.length; i++) {
                            node = nodes[i];

                            if (node.checked) {
                                checkedNodes.push({text: node.text, id: node.id});
                            }

                            if (node.hasChildren) {
                                getCheckedNodes(node.children.view(), checkedNodes);
                            }
                        }
                    }

                    function onCheck() {
                        var checkedNodes = [];
                        var treeView = $("#treeview").data("kendoTreeView");

                        getCheckedNodes(treeView.dataSource.view(), checkedNodes);
                        setMessage(checkedNodes.length);
                    }

                    function onExpand(e) {
                        if ($("#filterText").val() == "") {
                            $(e.node).find("li").show();
                        }
                    }

                    function setMessage(checkedNodes) {
                        var message;

                        if (checkedNodes > 0) {
                            message = checkedNodes + " clients selected";
                        }
                        else {
                            message = "0 clients selected";
                        }

                        $("#result").html(message);
                    }

                    $("#filterText").keyup(function (e) {
                        var filterText = $(this).val();

                        if (filterText !== "") {
                            $(".selectAll").css("visibility", "hidden");
                            $("#treeview .k-group .k-group .k-in").closest("li").hide();
                            $("#treeview .k-group").closest("li").hide();

                            $("#treeview .k-in:contains(" + filterText + ")").each(function () {
                                $(this).parents("ul, li").each(function () {
                                    var treeView = $("#treeview").data("kendoTreeView");
                                    treeView.expand($(this).parents("li"));
                                    $(this).show();
                                });
                            });

                            $("#treeview .k-group .k-in:contains(" + filterText + ")").each(function () {
                                $(this).parents("ul, li").each(function () {
                                    $(this).show();
                                });
                            });
                        } else {
                            $("#treeview .k-group").find("li").show();
                            var nodes = $("#treeview > .k-group > li");

                            $.each(nodes, function (i, val) {
                                if (nodes[i].getAttribute("data-expanded") == null) {
                                    $(nodes[i]).find("li").hide();
                                }
                            });

                            $(".selectAll").css("visibility", "visible");
                        }
                    });
                    $('#multiselect').attr('name', 'select[]');
        //$('#multiselect option').attr('selected');

        $("#multiselect option").each(function () {
            $(this).attr('selected', 'true');

        });
        $('#loadingDiv').css('display', 'none');
    },
    error: function (error) {
        alert('error block');
    }
});


});

function chbAllOnChange() {
    var checkedNodes = [];
    var treeView = $("#treeview").data("kendoTreeView");
    var isAllChecked = $('#chbAll').prop("checked");

    checkUncheckAllNodes(treeView.dataSource.view(), isAllChecked)

    if (isAllChecked) {
        setMessage($('#treeview input[type="checkbox"]').length);
    }
    else {
        setMessage(0);
    }
}
function checkUncheckAllNodes(nodes, checked) {
    for (var i = 0; i < nodes.length; i++) {
        nodes[i].set("checked", checked);

        if (nodes[i].hasChildren) {
            checkUncheckAllNodes(nodes[i].children.view(), checked);
        }
    }
}
function chbAllOnChange() {
    var checkedNodes = [];
    var treeView = $("#treeview").data("kendoTreeView");
    var isAllChecked = $('#chbAll').prop("checked");

    checkUncheckAllNodes(treeView.dataSource.view(), isAllChecked)

    if (isAllChecked) {
        setMessage($('#treeview input[type="checkbox"]').length);
    }
    else {
        setMessage(0);
    }
}
function onCheck() {
    var checkedNodes = [];
    var treeView = $("#treeview").data("kendoTreeView");

    getCheckedNodes(treeView.dataSource.view(), checkedNodes);
    setMessage(checkedNodes.length);
}

function onExpand(e) {
    if ($("#filterText").val() == "") {
        $(e.node).find("li").show();
    }
}

function setMessage(checkedNodes) {
    var message;

    if (checkedNodes > 0) {
        message = checkedNodes + " clients selected";
    }
    else {
        message = "0 clients selected";
    }

    $("#result").html(message);
}
/*==============Client Heirarchy Js==================*/
</script>
@endsection
@endsection
