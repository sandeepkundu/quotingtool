@extends('layouts.frontendLogin')
@section('title')
Login | upgrade to HP
@endsection

@section('content')
<style>
    .login-title-heading {
        text-align: center;
        font-size: 36px;
        color: #01518e;
        font-weight: 600;
        font-family: Raleway;
        margin-bottom: 55px;
    }
    .footer .footer-link ul li {
        float: left;
        padding: 0px 10px;
        position: relative;
    }
    #lost_pass:hover{
        color: #01518e;
    }
    #lost_pass{
        color: #b6b6b6;
    }

</style>
<div class="container" style="padding-bottom:100px;">
    <div class="row">
        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
            <div class="login-container" style="padding: 0 263px;">
                <form class="form-signin login-form" method="POST" action="{{URL::to('/')}}/users/reset">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <h2 class="login-title-heading">Reset Password</h2>
                    <input type="hidden" name="email" value="{{$user->email}}">
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input id="password" type="password" class="form-control input-sm" name="password" placeholder="Password">
                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <input id="password-confirm" type="password" class="form-control input-sm" name="password_confirmation" placeholder="Confirm Password">
                        @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                        @endif
                    </div>


                    <button type="submit" class="btn btn-login"> Reset Password</button>


                </form>
            </div>
        </div>
    </div>
</div>
@endsection
