@extends('layouts.default') @section('content')
	<div class="header-content">
		<h2><i class="fa fa-male"></i> Profile : {{ Auth::user()->first_name }}</h2>
		<div class="breadcrumb-wrapper hidden-xs">
			<span class="label">You are here:</span>
			<ol class="breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{URL('/')}}">Dashboard</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<!-- <li>
					<a href="#">Pages</a>
					<i class="fa fa-angle-right"></i>
				</li> -->
				<!-- <li class="active">Profile</li> -->
			</ol>
		</div>
	</div>
     @if(isset($toast['message']))
		  {{ $toast['message'] }}
	@endif         
 <div class="body-content animated fadeIn">
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-4">
				<div class="panel rounded shadow">
					<div class="panel-body">
						<div class="inner-all">
							<ul class="list-unstyled">
								<li class="text-center">
									<!-- <img class="img-circle img-bordered-primary" src="http://img.djavaui.com/?create=100x100,4888E1?f=ffffff" alt="Tol Lee"> -->
									@if( Auth::user()->image)
									<img class="img-circle img-bordered-primary" height="100" width="100" src="{{ URL::to('/') }}/images/{{ Auth::user()->image }}" alt="Product" title=" Old Product Image"/>
									@else
									<img class="img-circle img-bordered-primary" height="100" width="100" src="{{ URL::to('/') }}/product/default-user.png" alt="Product" title=" Old Product Image"/>
									@endif

								</li>
								<li class="text-center">
									<h4 class="text-capitalize">{{ Auth::user()->username }}</h4>
									<p class="text-muted text-capitalize"></p>
								</li>
								<li>
									<a style="background-color:#00B1E1 !important; border-color:#00B1E1 !important; " href="{{URL::to('/')}}/users/editProfile" class="btn btn-success text-center btn-block">Edit Profile</a>
								</li>
								<li><br/></li>
								
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-8">
					<div class="table-responsive mb-20">
						<table class="table table-bordered">
							<tbody>
								<tr>
									<th> Name :</th><td> {{ Auth::user()->first_name }}</td>
									<th> Last Name :</th><td>{{ Auth::user()->last_name }}</td>
								</tr>
								<tr>
									<th> Username : </th><td> {{ Auth::user()->username }}</td>
									<th> Email : </th><td>{{ Auth::user()->email }}</td>
								</tr>
								<tr>
									<th> Company : </th><td>{{ Auth::user()->company }}</td>
									<th> Phone : </th><td> {{ Auth::user()->phone }}</td>
								</tr>
							</tbody>
						</table>
					</div>				
				</div>
				<div class="divider"></div>
			</div>
			</div>
			
@endsection