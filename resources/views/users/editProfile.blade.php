@extends('layouts.default')
@section('content')
<link href="{{ asset('css/bower_components/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet">

<link href="{{ asset('css/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet">

<link href="{{ asset('css/bower_components/animate.css/animate.min.css') }}" rel="stylesheet">

<link href="{{ asset('css/bower_components/jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css') }}" rel="stylesheet">

<link href="{{ asset('css/bower_components/chosen_v1.2.0/chosen.min.css') }}" rel="stylesheet">
 <style>
.form-control:focus{
	border: 1px solid #66afe9 !important;
}
.bg-warning{
	background-color:#00B1E1 !important;
	border:1px solid #00B1E1 !important;
}
input.no-border-right:focus, textarea.no-border-right:focus{
	border: 1px solid #66afe9 !important;
}
</style>
   <div class="body-content animated fadeIn">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel rounded shadow">
					<div class="panel-heading">
						<div class="pull-left">
							<h3 class="panel-title">EDIT USER : {{$user->username}}</h3>
						</div>
						
						<div class="clearfix"></div>
					</div>
					
					<div class="panel-body no-padding">
						<form class="form-horizontal mt-10" id="commentForm" method="post" action="{{URL::to('/')}}/users/updateProfile" enctype="multipart/form-data">
							{{ csrf_field() }}
							<div class="form-body">
								<div class="form-group">
									<input class=" form-control" id="name" value="{{ $user->id }}"" name="id" minlength="2" type="hidden" required />
									<div class="row">
										<div class="col-lg-12">
											<div class="col-lg-6">
												<label for="first_name" class="col-sm-3 control-label">First Name<span style="color:red;">*</span></label>
												<div class="col-sm-9">
													<input value="{{$user->first_name}}" class=" form-control" id="first_name" name="first_name" minlength="2" type="text" placeholder="First Name"> 
													@if ($errors->has('first_name'))
													<span class="help-block" style="color:red;">
														<strong>{{ $errors->first('first_name') }}</strong>
													</span>
													@endif
													&nbsp
												</div>
											</div>

											<div class="col-lg-6">
												<label for="last_name" class="col-sm-3 control-label">Last Name<span style="color:red;">*</span></label>
												<div class="col-sm-9">
													<input value="{{$user->last_name}}" class=" form-control" id="last_name" name="last_name" minlength="2" type="text" placeholder="Last Name"> 
													@if ($errors->has('last_name'))
													<span class="help-block" style="color:red;">
														<strong>{{ $errors->first('last_name') }}</strong>
													</span>
													@endif
													&nbsp
												</div>
											</div>
										</div>
										<div class="col-lg-12">
											<div class="col-lg-6">
												<label for="email" class="col-sm-3 control-label">Email Address<span style="color:red;">*</span></label>
												<div class="col-sm-9">
													<input value="{{$user->email}}" type="email" name="email" class="form-control" id="email"
													placeholder="Email Address" disabled>
													@if ($errors->has('email'))
													<span class="help-block" style="color:red;">
														<strong>{{ $errors->first('email') }}</strong>
													</span>
													@endif
													&nbsp
												</div>
											</div>
											<div class="col-lg-6">
												<label for="password" class="col-sm-3 control-label">Password</label>
												<div class="col-sm-9">
													<input  class=" form-control" id="password" name="password" value="{!!old('password') !!}"minlength="2" type="password" placeholder="Password" >
													@if ($errors->has('password'))
													<span class="help-block" style="color:red;">
														<strong>{{ $errors->first('password') }}</strong>
													</span>
													@endif
													&nbsp
												</div>
											</div>
										</div>
										<div class="col-lg-12">
											<div class="col-lg-6">
												<label for="confirm_password" class="col-sm-3 control-label">Confirm Password</label>
												<div class="col-sm-9">
													<input type="password"  value="{!!old('confirm_password')!!}"name="confirm_password"class="form-control" id="confirm_password"
													placeholder="Confirm Password">
													@if ($errors->has('confirm_password'))
													<span class="help-block" style="color:red;">
														<strong>{{ $errors->first('confirm_password') }}</strong>
													</span>
													@endif
													&nbsp
												</div>
											</div>
											<div class="col-lg-6">
												<label for="company" class="col-sm-3 control-label">Company Name<span style="color:red;">*</span></label>
												<div class="col-sm-9">
													<input value="{{$user->company}}" type="text" class="form-control" name="company"id="company"
													placeholder="Company Name" >
													@if ($errors->has('company'))
													<span class="help-block" style="color:red;">
														<strong>{{ $errors->first('company') }}</strong>
													</span>
													@endif
													&nbsp
												</div>
											</div>
										</div>
										<div class="col-lg-12">
											<div class="col-lg-6">
												<label for="phone" class="col-sm-3 control-label">Phone Number<span style="color:red;">*</span></label>
												<div class="col-sm-9">
													<input value="{{$user->phone }}" type="text" class="form-control" name="phone" id="phone"
													placeholder="Phone Number">
													@if ($errors->has('phone'))
													<span class="help-block" style="color:red;">
														<strong>{{ $errors->first('phone') }}</strong>
													</span>
													@endif
													&nbsp
												</div>
											</div>
											<!-- <?php //if(isset($clients) && count($clients)>0){ ?>
											 <div class="col-lg-6">
												<label for="client" class="col-sm-3 control-label"> Client <span
													style="color: red;">*</span></label>
												<div class="col-sm-9">
													<select id="client_id" data-placeholder="Choose Client"
														class="form-control" multiple="true" tabindex="4">
														@foreach ($clients as $client)
															<option data-option-array-index="{{ $client->id }}" {{(in_array($client->id, $CU)) ? 'selected':''}}>{{
																$client->name }}
															</option>
														@endforeach
													</select>
													<input class="hidden" name="client_id"  />
												</div>

												@if ($errors->has('client_id'))
												<span class="help-block"
													style="color: red;"> <strong>{{ $errors->first('client_id')
														}}</strong>
												</span>
												@endif
											</div> 
											<?php  //} ?>  -->
											<div class="col-lg-6">
												<label for="username" class="col-sm-3 control-label">Username<span style="color:red;">*</span></label>
												<div class="col-sm-9">
													<input value="{{$user->username }}" class=" form-control" id="username" name="username" minlength="2" type="text" placeholder="Username"> 
													@if ($errors->has('username'))
													<span class="help-block" style="color:red;">
														<strong>{{ $errors->first('username') }}</strong>
													</span>
													@endif
													&nbsp
												</div>
											</div>
										</div>
										<div class="col-lg-12">
											<div class="col-lg-6">
													<label for="image" class="col-sm-3 control-label">Image<!-- <span style="color:red;">*</span> --></label>
													<div class="col-sm-9">
														<div class="fileinput input-group fileinput-new" data-provides="fileinput">
															<div class="form-control" data-trigger="fileinput">
																<i class="glyphicon glyphicon-file fileinput-exists"></i> 
																<span class="fileinput-filename"></span>
															</div>
															<span class="input-group-addon btn btn-success btn-file" style="border-color:#009dc8 !important; ;background-color:#009dc8 !important;">
																<span class="fileinput-new" style="background-color:#009dc8 !important">Select file</span>
																<span class="fileinput-exists" style="background-color:#009dc8 !important">Change</span>
																<input type="hidden" value="" name="testImg">
																<input type="file" name="image" value="{{ $user->image }}">
															</span>
															<a href="javascript:void(0)" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
														</div>
													</div>
													<div class="col-sm-2">
													@if(!empty($user->image))
														<img height="35" width="35" src="{{ URL::to('/') }}/product/{{ $user->image }}" alt="Product" title=" Old Product Image"/>
														@endif

													</div>
												</div>
												<div class="col-lg-6">
												<label class="col-sm-3 control-label">Enabled</label>
												<div class="col-sm-9">
													<div class="ckbox ckbox-info">
														<input id="checkbox-info1" {{($user->active == '1') ? 'checked' : ''}} type="checkbox" name="active">
														<label for="checkbox-info1"> </label>
													</div>
												</div>
											</div>	
											
										</div>
									</div>
								</div>
								<div class="form-group"></div>
							</div>
							<div class="form-footer">
								<div class="col-sm-offset-5">
									<button class="btn btn-success" type="submit" style="background-color:#00B1E1 !important; border-color:#00B1E1 !important; ">Save</button>
									<a class="btn btn-danger" href="{{URL::to('/')}}/users/viewProfile">Cancel</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		</div>
		
@section('js')

		<script type=text/javascript src="{{ asset('js/lib/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
		<script type=text/javascript src="{{ asset('js/lib/bower_components/jasny-bootstrap-fileinput/js/jasny-bootstrap.fileinput.min.js') }}"></script>
		<script type=text/javascript src="{{ asset('js/lib/bower_components/holderjs/holder.js') }}"></script>
		<script type=text/javascript src="{{ asset('js/lib/bower_components/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>
		<script type=text/javascript src="{{ asset('js/lib/bower_components/jquery-autosize/jquery.autosize.min.js') }}"></script>
		<script type=text/javascript src="{{ asset('js/lib/bower_components/chosen_v1.2.0/chosen.jquery.min.js') }}"></script>
		
		<script type="text/javascript">
			
			$( document ).ready(function() {
				$("#client_id").chosen();

				$('#client_id').on('change', function(evt, params) {
					var id = "";
				    $('.search-choice a').each(function(){
				    	id = id + $(this).data('option-array-index') + "," ;
				    });
				    $('[name="client_id"]').val(id);
				});

			});

			window.onload = function(){
				var id = "";
			    $('.search-choice a').each(function(){
			    	id = id + $(this).data('option-array-index') + "," ;
			    });
			    $('[name="client_id"]').val(id);
			};
 
		</script>
		
	@endsection	

@endsection
