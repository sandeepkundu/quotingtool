<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>

<body>
    <style>
        @media only screen and (max-width: 600px) {
            .inner-body {
                width: 100% !important;
            }

            .footer {
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }
    </style>

@if(!empty($Theme))
<link rel="stylesheet" type="text/css" href="{{asset('css')}}/{{$Theme->css_file}}">
<script type="text/javascript" src="{{asset('css')}}/{{$Theme->js_file}}"></script>
   
@else
<style type="text/css">
    .footer {
        position:fixed;
        bottom: 0;
        width: 100% !important;
        height: 60px;
        background-color: #56bdf1;
    }

    .footer .copyright{color:#fff; font-size:14px; padding:20px 0px;}
    .header{
      background-color: #01518e;
    }

</style>

@endif   

<table class="wrapper" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
            <table class="content" width="100%" cellpadding="0" cellspacing="0">
               <tr>
                <td class="header" style="text-align: center;height: 110px;width: 100%;">

                    <a href="{{url('/')}}">

                      @if(!empty($Theme->id))

                      <img src="{{url('/')}}/images/{{$Theme->logo}}" alt="Hp Login User">
                      @else
                      <img src="{{url('/')}}/images/logo.png" alt="Hp Login User">

                      @endif
                  </a>
              </td>
          </tr>

          <!-- Email Body -->
          <tr>
            <td class="body" width="100%" cellpadding="0" cellspacing="0">
                <table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0">
                    <!-- Body content -->
                    <tr>
                        <td class="content-cell">
                            <h1>Hi {{@$getUser->first_name}},</h1>
                            <p style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#74787e;font-size:16px;line-height:1.5em;margin-top:0;text-align:left">You are receiving this mail as you have successfully been registered with us. To reset your password please click on reset password button below</p>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="center">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <a href="{{URL::to('/')}}/users/resetPassword?token={{@$PasswordReset->token}}" class="" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;border-radius:3px;color:#fff;display:inline-block;text-decoration:none;background-color:#3097d1;border-top:10px solid #3097d1;border-right:18px solid #3097d1;border-bottom:10px solid #3097d1;border-left:18px solid #3097d1">Reset Password</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        
                    </tr>
                    <tr>
                        <td>
                           <br/><br/><p style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#74787e;font-size:16px;line-height:1.5em;margin-top:0;text-align:left">If you don't want to reset your password then no further action is required.</p>
                           
                           <p style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#74787e;font-size:16px;line-height:1.5em;margin-top:0;text-align:left">Regards,<br>Quotingtool</p>
                       </td>
                   </tr>
               </table>
           </table>
           
       </td>
   </tr>

   <footer class="footer" style="width: 100%;background:{{!empty($Theme->foot_back_color)? $Theme->foot_back_color:"#01518e" }}">
      <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <div class="copyright" style="text-align: center;color:{{!empty($Theme->foot_color)? $Theme->foot_color:"#fff" }}; font-size:14px; padding:20px 0px;">
                    {{date("Y")}} &copy; Feilding Technology Pte Ltd.
                </div>
            </div>
        </div>
    </div>
</footer> 
</table>
</td>
</tr>
</table>
</body>
</html>