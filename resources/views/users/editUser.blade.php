@extends('layouts.default') @section('content')

<link href="{{ asset('css/bower_components/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet">

<link href="{{ asset('css/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet">

<link href="{{ asset('css/bower_components/animate.css/animate.min.css') }}" rel="stylesheet">

<link href="{{ asset('css/bower_components/jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css') }}" rel="stylesheet">

<link href="{{ asset('css/bower_components/chosen_v1.2.0/chosen.min.css') }}" rel="stylesheet">

<!--Client Heirarchy CSS File-->
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.common-material.min.css" />
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.material.min.css" />
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.material.mobile.min.css" />
<!--Client Heirarchy CSS File-->
<style>
    .form-control:focus{
        border: 1px solid #66afe9 !important;
    }
    .bg-warning{
        background-color:#00B1E1 !important;
        border:1px solid #00B1E1 !important;
    }
    input.no-border-right:focus, textarea.no-border-right:focus{
        border: 1px solid #66afe9 !important;
    }
    /*==============Client Heirarchy CSS==================*/
    .k-dialog .k-dialog-buttongroup.k-dialog-button-layout-stretched .k-button:last-child{
        background:#00B1E1 !important;
        color:white;
        border:none;

    }
    .k-button, .k-calendar .k-footer, .k-calendar .k-header .k-link {
        text-transform: none;
    }
    #treeview{
        width:100%;
    }
    .k-dialog .k-dialog-buttongroup.k-dialog-button-layout-stretched .k-button:first-child{
        background: #E9573F;
        border-color:#E9573F;
        color: white;
    }
    .k-checkbox:indeterminate+.k-checkbox-label:before {
        border-color: #63D3E9;
    }
    .k-checkbox:indeterminate+.k-checkbox-label:after {
        background-color: #63D3E9;
        background-image: none;
        border-color: #63D3E9;
        border-radius: 0;
    }
    .k-checkbox:checked+.k-checkbox-label:before{
        background-color: #63D3E9 !important;
        border-color: #63D3E9 !important; 
    }
    html .k-dialog .k-window-titlebar {
        padding-left: 17px;
    }

    .k-dialog .k-content {
        padding: 17px;
    }

    #filterText {
        width: 100%;
        box-sizing: border-box;
        padding: 6px;
        border-radius: 3px;
        border: 1px solid #d9d9d9;
    }

    .selectAll {
        margin: 17px 0;
    }

    #result {
        color: #9ca3a6;
        float: right;
    }

    #treeview {
        height: 300px;
        overflow-y: auto;
        border: 1px solid #d9d9d9;
    }

    #openWindow {
        min-width: 180px;
    }

    /*==============Client Heirarchy CSS==================*/
    /*==========================Loader==================*/
    #loadingDiv{
        position:fixed;
        top:0px;
        right:0px;
        width:100%;
        height:100%;
        background-color:#666;
        background-repeat:no-repeat;
        background-position:center;
        z-index:10000000;
        opacity: 0.4;
        filter: alpha(opacity=40); /* For IE8 and earlier */
    }

    .loader {
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #00B1E1;
        border-right: 16px solid #fff;
        border-bottom: 16px solid #00B1E1;
        width: 66px;
        height: 66px;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
        margin-left: auto;
        margin-right: auto;
        margin-top:250px;
    }



    @-webkit-keyframes spin {
        0% { -webkit-transform: rotate(0deg); }
        100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
    /*==========================Loader==================*/
</style>
<div class="body-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">EDIT USER : {{$user->first_name}} {{$user->last_name}}</h3>
                    </div>

                    <div class="clearfix"></div>
                </div>

                <div class="panel-body no-padding">
                    <form class="form-horizontal mt-10" id="commentForm" method="post" action="{{URL::to('/')}}/users/updateUser" >
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <input class=" form-control" id="name" value="{{ $user->id }}" name="id" minlength="2" type="hidden" required />
                                
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="first_name" class="col-sm-3 control-label">First Name<span style="color:red;">*</span></label>
                                            <div class="col-sm-9">
                                                <input value="{{$user->first_name}}" class=" form-control" id="first_name" name="first_name" minlength="2" type="text" placeholder="First Name"> 
                                                @if ($errors->has('first_name'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('first_name') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <label for="last" class="col-sm-3 control-label">Last Name<span style="color:red;">*</span></label>
                                            <div class="col-sm-9">
                                                <input value="{{$user->last}}" class=" form-control" id="last" name="last" minlength="2" type="text" placeholder="Last Name"> 
                                                @if ($errors->has('last'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('last) }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">

                                        <div class="col-lg-6">
                                            <label for="company" class="col-sm-3 control-label">Company Name<span style="color:red;">*</span></label>
                                            <div class="col-sm-9">
                                                <input value="{{$user->company}}" type="text" class="form-control" name="company"id="company"
                                                placeholder="Company Name" >
                                                @if ($errors->has('company'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('company') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <label for="email" class="col-sm-3 control-label">Email Address<span style="color:red;">*</span></label>
                                            <div class="col-sm-9">


                                                <div class="input-group">
                                                    <span class="input-group-addon bg-primary">@</span>
                                                    <input value="{{$user->email}}" type="email" name="email" class="form-control" id="email"
                                                    placeholder="Email Address" disabled>
                                                </div>


                                                @if ($errors->has('email'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="password" class="col-sm-3 control-label">Password</label>
                                            <div class="col-sm-9">
                                                <input  class=" form-control" id="password" name="password" value="{!!old('password') !!}"minlength="2" type="password" placeholder="Password" >
                                                @if ($errors->has('password'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="phone" class="col-sm-3 control-label">Phone Number<!-- <span style="color:red;">*</span> --></label>
                                            <div class="col-sm-9">
                                                <input value="{{$user->phone }}" type="text" class="form-control" name="phone" id="phone"
                                                placeholder="Phone Number">
                                                @if ($errors->has('phone'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('phone') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="country" class="col-sm-3 control-label">
                                                User Role
                                            </label>
                                            <div class="col-sm-9">
                                                <select id="userrole" name="userrole" class="form-control">
                                                    <option value=""> -- Select role -- </option>
                                                    @if($userRoles!='')

                                                    @foreach ($userRoles as $code)

                                                    <option value="{{ $code->id }}"  {{ ($user->UserRole->role_id == $code->id) ? "selected":""}}> {{ $code->name }}</option>
                                                    @endforeach    
                                                    @endif
                                                </select>
                                                @if ($errors->has('userrole'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('userrole') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <label for="theme" class="col-sm-3 control-label">
                                                Select Theme<span style="color:red;">*</span>
                                            </label>
                                            <div class="col-sm-9">
                                                <select id="Theme" name="theme" class="form-control">
                                                    <option value=""> -- Select Theme -- </option>

                                                    @if(isset($Themes) && count($Themes) > 0 )

                                                        @foreach($Themes as $Theme)
                                                            <option value="{{$Theme->id}}" {{ ($user->theme_id == $Theme->id) ? "selected":""}}> {{$Theme->theme_name}} </option>
                                                        @endforeach

                                                    @endif
                                                </select>

                                                @if ($errors->has('theme'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('theme') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">

                                       <div class="col-lg-6">
                                        <label for="client" class="col-sm-3 control-label">
                                            Clients </label>
                                            <div class="col-sm-9" style="padding-left: 0px;">

                                                <div class="demo-section k-content">
                                                    <div id="dialog">
                                                        <div class="dialogContent">
                                                            <input id="filterText" type="text" placeholder="Search Client" />
                                                            <div class="selectAll" class='col-sm-12'>

                                                                <span id="result">0 client selected</span>
                                                            </div>
                                                            <div id="treeview"></div>
                                                        </div>
                                                    </div>
                                                    <select id="multiselect"></select>

                                                    <button id="openWindow" class="k-primary btn btn-block btn-color" type="button" style="border:none;">Select Clients</button>
                                                </div>

                                            </div>

                                            @if ($errors->has('select'))
                                            <span class="help-block" style="color:red;">
                                                <strong>{{ $errors->first('client_id') }}</strong>
                                            </span>
                                            @endif
                                         </div>

                                         <div class="col-lg-6">
                                            <label class="col-sm-3 control-label">Enabled
                                                <!-- <br/><span>(Active/Not Active)</span> --></label>
                                            <div class="col-sm-9">
                                                <div class="ckbox ckbox-info">
                                                    <input id="checkbox-info4" {{($user->active == '1') ? 'checked' : ''}} type="checkbox" name="active">
                                                    <label for="checkbox-info4"> </label>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
									
                                    <div class="col-sm-12 col-md-12 col-lg-12 Financial" style="margin-top: 15px;">
                                        <div class="col-sm-6">
                                                    <label for="description" class="col-sm-3 control-label" style="padding-top: 0px;">Public User's Permission</label>

                                                      <?php 

                                                        if(!empty($user->permission)){

                                                           $permission = $user->permission;

                                                           $arrs = explode(",",$permission);

                                                           $per = array();

                                                           foreach($arrs as $arr){
                                                            $per[] = $arr;
                                                           }
                                                        }   

                                                       ?>
                                                    
                                                    <div class="col-sm-9" style="padding-left: 0px;">
                                                 
                                                        <div class="col-sm-12" style="padding-left: 0px;">
                                                            <div class="col-sm-1">
                                                              <div class="ckbox ckbox-info">
                                                                <input id="checkbox-info1" value="1" type="checkbox" name="permission[]" class="pull-right" {{ @(in_array('1',@$per)) ? "checked":""}}>
                                                                <label for="checkbox-info1"></label>
                                                              </div>
                                                            </div>
                                                            <label class="col-sm-9 control-label" style="text-align:left;">Financial Display</label>
                                                        </div>

                                                        <div class="col-sm-12" style="padding: 0px;">
                                                        
                                                            <div class="col-sm-1">
                                                              <div class="ckbox ckbox-info">
                                                                <input id="checkbox-info2" value="2" type="checkbox" name="permission[]" class="pull-right" {{ @(in_array('2',@$per)) ? "checked":""}}>
                                                                <label for="checkbox-info2"></label>
                                                              </div>
                                                            </div>
                                                             <label class="col-sm-9 control-label" style="text-align:left;">Financial Management</label>
                                                        </div>
                                                     <div class="col-sm-12" style="padding: 0px;">
                                                        
                                                        <div class="col-sm-1">
                                                          <div class="ckbox ckbox-info">
                                                            <input id="checkbox-info3"  value="3" type="checkbox" name="permission[]" class="pull-right" {{ @(in_array('3',@$per)) ? "checked":""}}>
                                                            <label for="checkbox-info3"></label>
                                                          </div>
                                                        </div>
                                                        <label class="col-sm-9 control-label" style="text-align:left;"> Collection Request</label>
                                                    </div>

                                                    @if ($errors->has('permission'))
                                                        <span class="help-block" style="color:red;">
                                                            <strong>{{ $errors->first('permission') }}</strong>
                                                        </span>
                                                    @endif
                                                    &nbsp;
                                                 </div>
                                                   
                                            </div> 
                                    </div>



                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            
                        </div>

                        <div class="form-footer">
                            <div class="col-sm-offset-5">
                                    <button class="btn btn-color btn-hover" type="submit">Submit</button>
                                    <a class="btn btn-danger" href="{{URL::to('/')}}/users/index">Cancel</a>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

        <div id="loadingDiv">
            <div>
                <div class="loader"></div>
            </div>
        </div>
        @section('js')

        <script type=text/javascript src="{{ asset('js/lib/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>

        <script type=text/javascript src="{{ asset('js/lib/bower_components/jasny-bootstrap-fileinput/js/jasny-bootstrap.fileinput.min.js') }}"></script>

        <script type=text/javascript src="{{ asset('js/lib/bower_components/holderjs/holder.js') }}"></script>

        <script type=text/javascript src="{{ asset('js/lib/bower_components/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>

        <script type=text/javascript src="{{ asset('js/lib/bower_components/jquery-autosize/jquery.autosize.min.js') }}"></script>

        <script src="{{ asset('css/password.js') }}"></script>
        <script src="{{ asset('js/ClientTree.js') }}"></script>

        <script type="text/javascript">

            $(document).ready(function () {
                $("#client_id").chosen();

                $('#client_id').on('change', function (evt, params) {
                    var id = "";
                    $('.search-choice a').each(function () {
                        id = id + $(this).data('option-array-index') + ",";
                    });
                    $('[name="client_id"]').val(id);
                });

            });

            window.onload = function () {
                var id = "";
                $('.search-choice a').each(function () {
                    id = id + $(this).data('option-array-index') + ",";
                });
                $('[name="client_id"]').val(id);
            };

        </script>
        <script type="text/javascript">

            $('#password').password({
        // custom messages
        shortPass: 'The password is too short',
        badPass: 'Weak; try combining letters & numbers',
        goodPass: 'Medium; try using special charecters',
        strongPass: 'Strong password',
        containsUsername: 'The password contains the username',
        enterPass: 'Type your password',
        // show percent
        showPercent: true,
        // show text
        showText: true,
        // enable animation
        animate: true,
        animateSpeed: 'fast',
        // link to username
        email: false,
        usernamePartialMatch: true,
        // minimum length
        minimumLength: 1

    });



</script>
<script type="text/javascript">
    $(document).ready(function(){
        if($('#userrole').val() == 3){
        $('.Financial').css('display','block');
        }else{
           $('.Financial').css('display','none'); 
        }
     $('#userrole').change(function(){
        var role = $(this).val();
        if(role == 3){
            $('.Financial').css('display','block');
        }else{
            $('.Financial').css('display','none');
        }
    })
 })

    $(".dropdown dt a").on('click', function () {
        $(".dropdown dd ul").slideToggle('fast');
    });

    $(".dropdown dd ul li a").on('click', function () {
        $(".dropdown dd ul").hide();
    });


	function clientTree(clientArray){
		var c = [];
		var i = 0;
		for (var item in clientArray) {
			var obj = clientArray[item];
			a = [];
            if (obj.items) {
				a = clientTree(obj.items);
			}
			
			c[i] = { id: clientArray[item].id, text: clientArray[item].name, expanded: true, items: a };
            i++;                    
		}
		return c;
	}

    /*==============Client Heirarchy Js==================*/
    $(document).ready(function () {
        var v_token = "{{csrf_token()}}";
        var id = $('#name').val();
        
        $.ajax({
            type: 'post',
            url: SITE_URL + '/ClientSelect',
            data: {_token: v_token, id: id},
            dataType: 'JSON',
            success: function (obj) {
				//console.log(obj);
				//console.log(clientTree(obj));			
                var abcd = obj['ClientUser'];
                delete obj['ClientUser'];				
                var s = [];
				/*
                var a = [];
                var c = [];
                var i = 0;
                for (var item in obj) {
                    j = 0;
                    var obj1 = obj[item];
                    
                    for (var key in obj1.items) {
                        var k = 0;
                        var obj3 = obj1.items[key];
                        for (var element in obj3.items) {
                            c[k] = {id: obj3.items[element].id, text: obj3.items[element].name};
                            k++;
                        }
                        
                        a[j] = {id: obj1.items[key].id, text: obj1.items[key].name, expanded: true, items: c};
                        j++;
                        c = [];
                    }

                    s[i] = {
                        id: obj[item].id, text: obj[item].name, expanded: true, items: a
                    };
                    i++;
                    a = [];
                }
				*/  
				s = clientTree(obj);
              
                var myDataSource = new kendo.data.HierarchicalDataSource({
                    data: s
                });

                $("#multiselect").kendoMultiSelect({
                    dataTextField: "text",
                    dataValueField: "id"
                });

                $("#treeview").kendoTreeView({
                    loadOnDemand: false,
                    checkboxes: {
                        checkChildren: false
                    },
                    dataSource: myDataSource,
                    check: onCheck,
                    expand: onExpand
                });

                $(document).ready(function () {
                    //  $('span').removeClass('k-checkbox-wrapper');
                    var dialog = $("#dialog");
                    var multiSelect = $("#multiselect").data("kendoMultiSelect");
                    $("#openWindow").kendoButton();

                    multiSelect.readonly(false);

                    $("#openWindow").click(function () {
                        dialog.data("kendoDialog").open();
                        $(this).fadeOut();
                    });

                    dialog.kendoDialog({
                        width: "400px",
                        title: "Select Client",
                        visible: false,
                        actions: [
                        {
                            text: 'Cancel',
                            primary: false,
                            action: onCancelClick
                        },
                        {
                            text: 'Ok',
                            primary: true,
                            id: 'test',
                            action: onOkClick
                        }
                        ],
                        close: onClose
                    })//.data("kendoDialog").open();
                });

                function onCancelClick(e) {
                    e.sender.close();
                }

                function onOkClick(e) {
                    var checkedNodes = [];
                    var treeView = $("#treeview").data("kendoTreeView");

                    getCheckedNodes(treeView.dataSource.view(), checkedNodes);
                    populateMultiSelect(checkedNodes);

                    e.sender.close();
                }

                function onClose() {
                    $("#openWindow").fadeIn();
                }

                function populateMultiSelect(checkedNodes) {
                    var multiSelect = $("#multiselect").data("kendoMultiSelect");
                    multiSelect.dataSource.data([]);

                    var multiData = multiSelect.dataSource.data();
                    if (checkedNodes.length > 0) {
                        var array = multiSelect.value().slice();
                        for (var i = 0; i < checkedNodes.length; i++) {
                            multiData.push({text: checkedNodes[i].text, id: checkedNodes[i].id});
                            array.push(checkedNodes[i].id.toString());
                        }

                        multiSelect.dataSource.data(multiData);
                        multiSelect.dataSource.filter({});
                        multiSelect.value(array);
                    }
                }

                function checkUncheckAllNodes(nodes, checked) {
                    for (var i = 0; i < nodes.length; i++) {
                        nodes[i].set("checked", checked);

                        if (nodes[i].hasChildren) {
                            checkUncheckAllNodes(nodes[i].children.view(), checked);
                        }
                    }
                }

                function chbAllOnChange() {
                    var checkedNodes = [];
                    var treeView = $("#treeview").data("kendoTreeView");
                    var isAllChecked = $('#chbAll').prop("checked");

                    checkUncheckAllNodes(treeView.dataSource.view(), isAllChecked)

                    if (isAllChecked) {
                        setMessage($('#treeview input[type="checkbox"]').length);
                    }
                    else {
                        setMessage(0);
                    }
                }

                function getCheckedNodes(nodes, checkedNodes) {
                    var node;

                    for (var i = 0; i < nodes.length; i++) {
                        node = nodes[i];

                        if (node.checked) {
                            checkedNodes.push({text: node.text, id: node.id});
                        }

                        if (node.hasChildren) {
                            getCheckedNodes(node.children.view(), checkedNodes);
                        }
                    }
                }

                function onCheck() {
                    var checkedNodes = [];
                    var treeView = $("#treeview").data("kendoTreeView");

                    getCheckedNodes(treeView.dataSource.view(), checkedNodes);
                    setMessage(checkedNodes.length);
                }

                function onExpand(e) {
                    if ($("#filterText").val() == "") {
                        $(e.node).find("li").show();
                    }
                }

                function setMessage(checkedNodes) {
                    var message;

                    if (checkedNodes > 0) {
                        message = checkedNodes + " clients selected";
                    }
                    else {
                        message = "0 clients selected";
                    }

                    $("#result").html(message);
                }

                $("#filterText").keyup(function (e) {
                    var filterText = $(this).val();

                    if (filterText !== "") {
                        $(".selectAll").css("visibility", "hidden");

                        $("#treeview .k-group .k-group .k-in").closest("li").hide();
                        $("#treeview .k-group").closest("li").hide();
                        $("#treeview .k-in:contains(" + filterText + ")").each(function () {
                            $(this).parents("ul, li").each(function () {
                                var treeView = $("#treeview").data("kendoTreeView");
                                treeView.expand($(this).parents("li"));
                                $(this).show();
                            });
                        });
                        $("#treeview .k-group .k-in:contains(" + filterText + ")").each(function () {
                            $(this).parents("ul, li").each(function () {
                                $(this).show();
                            });
                        });
                    }
                    else {
                        $("#treeview .k-group").find("li").show();
                        var nodes = $("#treeview > .k-group > li");

                        $.each(nodes, function (i, val) {
                            if (nodes[i].getAttribute("data-expanded") == null) {
                                $(nodes[i]).find("li").hide();
                            }
                        });

                        $(".selectAll").css("visibility", "visible");
                    }
                });
                $('#multiselect').attr('name', 'select[]');
                //$('#multiselect option').attr('selected');

                $("#multiselect option").each(function () {
                    $(this).attr('selected', 'true');
                });

                $('#openWindow').trigger('click');
                for (var client in abcd) {
                    $("[aria-label='" + abcd[client].clientss.name + "']").trigger('click');
                }
                $('.k-primary').trigger('click');
                $('#loadingDiv').css('display', 'none');
            },
            error: function (error) {
                alert('error block');
            }
        });

});


/*==============Client Heirarchy js==================*/
</script>
@endsection
<style type="text/css">
    .pass-graybar {
        height: 3px;
        background-color: red;
        width: 100%;
        position: relative;
    }

    .pass-colorbar {
        height: 3px;
        background-image: url(passwordstrength.jpg);
        position: absolute;
        background-color: green;
        top: 0;
        left: 0;
    }

    .pass-percent, .pass-text {
        font-size: 1em;
    }

    .pass-percent {
        margin-right: 5px;
    }

</style>
<style type="text/css">
    AndaleMono, monospace{
        color: #fff;
        padding: 50px;
        width: 300px;
        margin: 0 auto;
        background-color: #374954;
    }

    .dropdown {
        position: absolute;
        top:50%;
        transform: translateY(-50%);
    }

    a {
        color: #fff;
    }

    .dropdown dd,
    .dropdown dt {
        margin: 0px;
        padding: 0px;
    }

    .dropdown ul {
        margin: -1px 0 0 0;
    }

    .dropdown dd {
        position: relative;
    }

    .dropdown a,
    .dropdown a:visited {
        color: #fff;
        text-decoration: none;
        outline: none;
        font-size: 12px;
    }

    .dropdown dt a {
        background-color: #00B1E1;
        display: block;
        padding: 5px 20px 0px 10px;
        min-height: 25px;
        line-height: 24px;
        overflow: hidden;
        border: 0;
    }

    .dropdown dt a span,
    .multiSel span {
        cursor: pointer;
        display: inline-block;
        padding: 0 3px 2px 0;
    }

    .dropdown dd ul {
        background-color: #00B1E1;
        border: 0;
        color: #fff;
        display: none;
        left: 0px;
        padding: 2px 15px 2px 5px;
        position: absolute;
        top: 2px;
        width: 100%;
        list-style: none;
        height: 200px;
        overflow: auto;
        z-index: 9999;
    }

    .dropdown span.value {
        display: none;
    }

    .dropdown dd ul li a {
        padding: 5px;
        display: block;
    }

    .dropdown dd ul li a:hover {
        background-color: #fff;
    }


</style>
@endsection
