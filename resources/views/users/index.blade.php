@extends('layouts.default') @section('content')


@if(isset($toast['message']))
{{ $toast['message'] }}
@endif
@section('css')
<!-- START @PAGE LEVEL STYLES -->
<link href="{{asset('assets/global/plugins/bower_components/datatables/css/dataTables.bootstrap.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/bower_components/datatables/css/datatables.responsive.css')}}" rel="stylesheet">
<!--/ END PAGE LEVEL STYLES -->

@endsection
<style>
    .table-success tbody tr td .sorting_1 {
        background: red !important;
        color: white;
        border-bottom: 1px solid red !important;
    }
    .dataTable thead tr th:first-child {
        min-width: 10px !important;
    }
    .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
        background-color: #178aa9 !important;
        border: 1px solid #178aa9;
    }
    table.dataTable thead th {
        position: relative;
        background-image: none !important;
    }

    table.dataTable thead th.sorting:after,
    table.dataTable thead th.sorting_asc:after,
    table.dataTable thead th.sorting_desc:after {
        position: absolute;
        top: 12px;
        right: 8px;
        display: block;
        font-family: FontAwesome;
    }
    table.dataTable thead th.sorting:after {
        content: "\f0dc";
        color: #ddd;
        font-size: 0.8em;
        padding-top: 0.12em;
    }
    table.dataTable thead th.sorting_asc:after {
        content: "\f0de";
    }
    table.dataTable thead th.sorting_desc:after {
        content: "\f0dd";
    }

</style>
<!-- Start page header -->

<div class="header-content">
    <h2><i class="fa fa-table"></i> USERS<span></span></h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label"></span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="{{URL::to('/')}}/admin/dashboard">Dashboard</a>
                <!-- <i class="fa fa-angle-right"></i> -->
            </li>
            <li> <!-- Users <i class="fa fa-angle-right"></i> index  --></li>
            <li class="active"></li>
        </ol>
    </div>
</div>
<div class="body-content animated fadeIn">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-tab panel-tab-double shadow">
                <div class="panel-body no-padding">
                    <div class="panel panel-default shadow no-margin">
                        <div class="panel-heading">
                            <a  href="{{URL::to('/')}}/users/index" class="btn btn-default tooltips" data-toggle="tooltip" data-placement="top" data-title="Reload" data-action="refresh" data-original-title="" title="" id="ml5"><i class="icon-refresh icons"></i></a>
                            <a href="javascript:void(0);" class="btn btn-danger tooltips" data-toggle="tooltip" data-placement="top" data-title="Delete" data-original-title="" title="" onclick="return multiId()" ><i class="icon-trash icons"></i></a>
                            <div class="pull-right" id="mt0">
                                <a href="{{URL::to('/')}}/users/addUser" class="btn btn-color btn-hover"><i class="fa fa-plus"></i> &nbsp;Add new user</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="tab-client-all">
                                    <div id="datatable-client-all_wrapper" class="dataTables_wrapper form-inline">
                                        <form action="{{URL::to('/') }}/users/multipleDelete" method="post" class="multipleDeleteForm">
                                            {{ csrf_field() }}
                                            <table id="datatable-ajax" class="table table-striped table-success">
                                                <thead>
                                                    <tr role="row">
                                                        <th style="width:10px !important;"><input type="checkbox" class="multiIdCheck"/></th>
                                                        <th>First Name</th>
                                                        <th>Last Name</th>
                                                        <th>Email Address</th>
                                                        <th>Phone</th>
                                                        <th>Clients</th>
                                                        <th>Last Login</th>
                                                        <th>Enabled</th>
                                                        <th class="" style="width: 108px !important;">Actions</th>
                                                    </tr>
                                                </thead>

                                                <tbody>


                                                    <?php
                                                    $i = 0;
                                                    if (count($user) > 0) {
                                                        foreach ($user as $users) {
                                                            $i++;
                                                            ?>

                                                            <tr>
                                                                <td>
                                                                    <input type="checkbox" class="id" name="ids[]" value="{{$users->id}}">
                                                                </td>
                                                                <!-- <td scope="row">{{$i}}</td> -->
                                                                <td>{{$users->first_name}}</td>
                                                                <td>{{$users->last_name}}</td>
                                                                <td>{{$users->email}}</td>
                                                                <td>
                                                                    @if(isset($users->phone))
                                                                    {{$users->phone}}
                                                                    @else
                                                                    N/A
                                                                    @endif
                                                                </td>
                                                                <td><span abc="{{$users->first_name}} {{$users->last_name}}" class="userClient" id="{{$users->id}}" style="cursor: pointer;font-weight: bold;">({{count($users->clientuser)}})</span></td>

                                                                @if ($users->last_login==null) 

                                                                <td style="text-align: center;">-</td>

                                                                @else
                                                                <td>{{date('d-m-Y G:i', $users->last_login)}}</td>

                                                                @endif


                                                                <?php
                                                                if ($users->active == '1') {
                                                                    echo "<td style='color:green'><span>&#10003;</span></td>";
                                                                } else {
                                                                    echo "<td style='color:red;'><span>&#9747;</span></td>";
                                                                }
                                                                ?>
                                                                <td>
                                                                    <a class="btn btn-round btn-color btn-hover" href="{{URL::to('/')}}/users/editUser/{{$users->id}}" style="padding: 0px 6px">
                                                                        <i class="fa fa-edit"></i>
                                                                    </a>
                                                                    <a onclick="return confirm('Are you sure you want to delete?')" class="btn btn-round btn-danger"  style="padding:0px 6px;" href="{{URL::to('/')}}/users/Delete/{{$users->id}}">
                                                                        <i class="fa fa-trash-o"></i>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    } else {
                                                        echo '<tr><td colspan="10" style="color:red;"> No record found !! </td></tr>';
                                                    }
                                                    ?>

                                                </tbody>

                                                <tfoot>
                                                    <tr role="row">
                                                        <th style="width:10px !important;"><input type="checkbox" class="multiIdCheck"/></th>
                                                        <th>First Name</th>
                                                        <th>Last Name</th>
                                                        <th>Email Address</th>
                                                        <th>Phone</th>
                                                        <th>Clients</th>
                                                        <th>Last Login</th>
                                                        <th>Enabled</th>
                                                        <th class="" style="width: 108px !important;">Actions</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="loadingDiv" style="display:none;">
    <div>
        <div class="loader"></div>
    </div>
</div>
<!--==============================Modal Popup=======================-->

<div class="modal modal-success fade in" id="modal-bootstrap-tour" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-dialog" role="document" style="margin: 150px auto;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Client List for <span class="username"></span></h4>
            </div>
            <div class="modal-body">

                <table id="" class="table table-striped table-success table-middle table-project-clients " role="grid" aria-describedby="datatable-client-all_info">
                    <thead><th>S.No.</th><th>Client Name</th><th>Client Type</th></thead>
                    <tbody class="userClintlist"></tbody>
                    <tfoot><th>S.No.</th><th>Client Name</th><th>Client Type</th></tfoot>
                </table>

            </div>
        </div>
    </div>
</div>

<!--==============================Modal Popup=======================--> 

<style>

    #tour-3{
        margin-bottom: 0px;
    }
    .padding_bottom{
        padding-bottom: 15px;
    }
    .form-control:focus{
        border: 1px solid #66afe9 !important;
    }

    .modal-success .modal-header {
        background-color: #00B1E1 !important;
        border-bottom: 1px solid #00B1E1;
    }
</style>

<style>
    /*==========================Loader==================*/
    #loadingDiv{
        position:fixed;
        top:0px;
        right:0px;
        width:100%;
        height:100%;
        background-color:#666;
        background-repeat:no-repeat;
        background-position:center;
        z-index:10000000;
        opacity: 0.4;
        filter: alpha(opacity=40); /* For IE8 and earlier */
    }

    .loader {
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #00B1E1;
        border-right: 16px solid #fff;
        border-bottom: 16px solid #00B1E1;
        width: 66px;
        height: 66px;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
        margin-left: auto;
        margin-right: auto;
        margin-top:250px;
    }



    @-webkit-keyframes spin {
        0% { -webkit-transform: rotate(0deg); }
        100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
    /*==========================Loader==================*/

    .modal-success .modal-header:before {
        display: block;
        position: absolute;
        top: -1px;
        left: -1px;
        right: -1px;
        bottom: -1px;
        content: "";
        border-top: 1px solid #00B1E1 !important; 
        border-left: 1px solid #00B1E1 !important; 
        border-right: 1px solid #00B1E1 !important; 
    }
</style>		

@section('js')
<script type="text/javascript">

    $(document).ready(function () {

        $('#datatable-ajax').DataTable({
            language: {
                searchPlaceholder: "Search records"
            },
            "lengthMenu": [[30, 60, 90, -1], [30, 60, 90, "All"]],
            "order": [[1, "asc"]],
            "columnDefs": [{
                    "targets": 0,
                    "orderable": false,
                    'min-width': '10px',
                    "width": "5%"
                }],
            "bSortClasses": false,
            "searchHighlight": true,
        });

    });
    function multiId() {
        var checkboxcount = jQuery('.id:checked').length;
        if (checkboxcount != 0) {
            if (confirm("Are you sure you want to delete ?")) {
                jQuery('.multipleDeleteForm').submit();
            }
        } else {
            alert("Please select a record")
            return false;
        }
    }
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#client_id").chosen();

        $('#client_id').on('change', function (evt, params) {
            var id = "";
            $('.search-choice a').each(function () {
                id = id + $(this).data('option-array-index') + ", ";
            });
            $('[name="client_id"]').val(id);
        });
    });
</script>


<script>
    $('#modal-bootstrap-tour').modal('hide');
    $(document).ready(function () {
        $('.userClient').click(function () {
            var userId = $(this).attr('id');
            var username = $(this).attr('abc');
            $('#loadingDiv').css('display', 'block');
            $('.username').html(username);
            if (userId != '') {
                $.ajax({
                    type: 'get',
                    url: 'getclients',
                    data: {'userId': userId},
                    dataType: 'JSON',
                    success: function (obj) {
                        var data = obj.clientuser;
                        var abc = '';
                        var i = 1;
                        if (data == '') {
                            abc += '<tr><td colspan="3" style="color:red;">No any client assigned for this user !!</td></tr>';

                        } else {
                            for (x in data) {
                                if (typeof (data[x].clientss) != "undefined" && data[x].clientss !== null) {
                                    abc += '<tr><td>' + i + '</td><td>' + data[x].clientss.name + '</td><td>' + data[x].clientss.client_type + '</td></tr>';
                                    i++;
                                }

                            }
                        }
                        $('.userClintlist').html(abc);
                        $('#modal-bootstrap-tour').modal('show');
                        $('#loadingDiv').css('display', 'none');

                    },
                    error: function (error) {
                        alert('error block');
                    }
                });
            }
        });
    });

</script>
<!-- START @PAGE LEVEL PLUGINS -->
<script src="{{asset('assets/global/plugins/bower_components/datatables/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/global/plugins/bower_components/datatables/js/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('assets/global/plugins/bower_components/datatables/js/datatables.responsive.js')}}"></script>
<!--/ END PAGE LEVEL PLUGINS -->
@endsection
@endsection