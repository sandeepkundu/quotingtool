@extends('layouts.default') @section('content')

 <style>
.form-control:focus{
	border: 1px solid #66afe9 !important;
}
.bg-warning{
	background-color:#00B1E1 !important;
	border:1px solid #00B1E1 !important;
}
input.no-border-right:focus, textarea.no-border-right:focus{
	border: 1px solid #66afe9 !important;
}
</style>
<div class="body-content animated fadeIn">

		<div class="row">
			<div class="col-lg-12">
				<div class="panel rounded shadow">
					<div class="panel-heading">
						<div class="pull-left">
							<h3 class="panel-title">EDIT PRICE MARGIN OEM</h3>
						</div>
						
						<div class="clearfix"></div>
					</div>
					<!-- /.panel-heading -->
					<div class="panel-body no-padding">

						<form class="form-horizontal mt-10" id="commentForm" method="post" action="{{URL::to('/')}}/priceMarginOEM/updatePriceMargin" >
							{{ csrf_field() }}
							<input type="hidden" name="id" value="{{ $oem->id }}">
							<div class="form-body">
								<div class="form-group">
									<div class="row">
										<div class="col-lg-12">
											<div class="col-lg-6">
												<label for="oem_type" class="col-sm-3 control-label">OEM Type<span style="color:red;">*</span></label>
												<div class="col-sm-9">
													<input value="{{$oem->oem_type}}" class=" form-control" id="oem_type" name="oem_type" minlength="2" type="text" placeholder=" OEM Type"> 
													@if ($errors->has('oem_type'))
													<span class="help-block" style="color:red;">
														<strong>{{ $errors->first('oem_type') }}</strong>
													</span>
													@endif
													&nbsp
												</div>
											</div>

											<div class="col-lg-6">
												<label for="value_to" class="col-sm-3 control-label">Value<span style="color:red;">*</span></label>
												<div class="col-sm-9">
													<input value="{{$oem->value}}" class=" form-control" id="value" name="value" minlength="2" type="text" placeholder="Value "> 
													@if ($errors->has('value'))
													<span class="help-block" style="color:red;">
														<strong>{{ $errors->first('value') }}</strong>
													</span>
													@endif
													&nbsp
												</div>
											</div>

											<div class="col-lg-6">
												<label for="status" class="col-sm-3 control-label">Enabled<!-- <span style="color:red;">*</span> --></label>
												<div class="col-sm-9">
													<div class="ckbox ckbox-info">
                                                    <input id="checkbox-info1" {{($oem->status == 'active') ? 'checked' : ''}} type="checkbox" name="status">
                                                    <label for="checkbox-info1"> </label>
												</div>
											</div>
										</div>


											<div class="col-lg-6">
												<label for="client_id" class="col-sm-3 control-label">Client<span style="color:red;">*</span></label>
												<div class="col-sm-9" style="padding-bottom:20px;">
													<select id="client_id" name="client_id" class="form-control">
														<option value=""> -- Select Client -- </option>
														@foreach ($client as $clients)
															<option value="{{ $clients->id }}" {{ $clients->id == $oem->client_id ? 'selected' : '' }}>{{ $clients->name }}</option>
														@endforeach             
													</select>
													@if ($errors->has('client_id'))
														<span class="help-block" style="color:red;">
															<strong>{{ $errors->first('client_id') }}</strong>
														</span>
													@endif
														&nbsp
												</div>
											</div>
										</div>
										
									</div>
								</div>
							</div>
							<!-- /.form-group -->
							<div class="form-group"></div>
							<!-- /.form-group -->
						</div>
						<!-- /.form-body -->
						<div class="form-footer">
							<div class="col-sm-offset-5">
								<button class="btn btn-color btn-hover" type="submit">Submit</button>
								<a class="btn btn-danger" href="{{URL::to('/')}}/priceMarginOEM/index">Cancel</a>
							</div>
						</div>
					</form>

				</div>
			</div>
			<!-- /.panel -->
			<!--/ End horizontal form -->
		</div>
	</div>


@endsection
