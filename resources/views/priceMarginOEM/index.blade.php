@extends('layouts.default') @section('content')


<style>
.form-control:focus{
	border: 1px solid #66afe9 !important;
}
</style>

  <!-- START @PAGE CONTENT -->

 	 @if(isset($toast['message']))
		  {{ $toast['message'] }}
	@endif
 	<div class="header-content">
        <h2><i class="fa fa-table"></i>PRICE MARGIN ADJUSTMENTS <span></span></h2>
       <div class="breadcrumb-wrapper hidden-xs">
            <span class="label"></span>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="{{URL::to('/')}}/admin/dashboard">Dashboard</a>
                    <!-- <i class="fa fa-angle-right"></i> -->


                </li>
                <li>
                    <!-- Clients
                    <i class="fa fa-angle-right"></i>
                     index -->
                    
                </li>
                <li class="active">                	
                </li>
            </ol>
        </div><!-- /.header-content -->
    </div><!--/ End page header -->
	<div class="body-content animated fadeIn">
   <!-- Start body content -->
  
       <div class="row">
           <div class="col-md-12">
                 <div class="panel panel-tab panel-tab-double shadow">
                    <div class="panel-body no-padding">
                        <div class="panel panel-default shadow no-margin">
                             <div class="panel-heading">
                                       <a  href="{{URL::to('/')}}/priceMarginOEM/index" class="btn btn-default tooltips" data-toggle="tooltip" data-placement="top" data-title="Reload" data-action="refresh" data-original-title="" title="" id="ml5"><i class="icon-refresh icons"></i></a>
                                      <!--   <a href="#" class="btn btn-default tooltips" data-toggle="tooltip" data-placement="top" data-title="Print" onclick="window.print();return false;" data-original-title="" title=""><i class="icon-printer icons"></i></a>-->
 											
                                        <a href="javascript:void(0);" class="btn btn-danger tooltips" data-toggle="tooltip" data-placement="top" data-title="Delete" data-original-title="" title="" onclick="return multiId()" ><i class="icon-trash icons"></i></a>

                                <div class="pull-right" id="mt0">
                                   <!--  <button type="button" class="btn btn-success"><i class="icon-user-follow icons"></i> Add client</button>  -->
                                     <a class="btn btn-color btn-hover pull-right" href="{{URL::to('/')}}/priceMarginOEM/addPriceMargin"><i class="fa fa-plus"></i>&nbsp;Add new marginOEM</a>
								</div>

                               <div class="clearfix"></div>
                            </div><!--/.panel-heading -->

                            <!--Panel Body-->
                  <div class="panel-body">
                    <div class="tab-content">
                     <div class="tab-pane fade in active" id="tab-client-all">
                                    <!-- Start datatable -->
                                    <div id="datatable-client-all_wrapper" class="dataTables_wrapper form-inline">
                                    <div class="row">
                                          <div class="col-xs-6">
                                            <div class="dataTables_length" id="datatable-client-all_length">
                                            <label>
                                            <select name="datatable-client-all_length" class="form-control input-sm" id="paginationPerPage">
												<?php $pa=array(10,20,30,40,50); ?>
												@foreach($pa as $p)
													<option value="{{$p}}" {{ ($p==$lim)? 'selected' : '' }}>{{$p}}</option>
												@endforeach

                                            </select> Records per page</label></div>
                                        </div>
                                      <div class="col-xs-6"><div id="datatable-client-all_filter" class="dataTables_filter">
                                           	  

			</div>
	        <form action="{{URL::to('/')}}/priceMarginOEM/index" method="get" class="navbar-form">
					<div class="form-group has-feedback pull-right" style="margin-right:-15px !important;margin-top:-13px;">
						<input type="text" class="form-control rounded"
							placeholder="Search by  OEM  or Type Value" name="search">
						<button type="submit" class="btn btn-color  btn-hover btn-theme fa fa-search form-control-feedback rounded"></button>
					</div>
					 
			</form>
        	 </div>
           </div>

		        <table class="table table-striped table-success table-middle table-project-clients " role="grid" aria-describedby="datatable-client-all_info">
		            
		            <thead>
		            	<tr role="row">
							<th style="width:10px !important; text-align:center; "><input type="checkbox" class="multiIdCheck"/></th>
							<!-- <th class="text-center" style="width: 55px !important;">Sr. no</th> -->
							<th  data-hide="phone,tablet" class=" sorting" tabindex="0" aria-controls="datatable-client-all" rowspan="1" colspan="1" aria-label="Total Balance: activate to sort column ascending">Client</th>
							<th  data-hide="phone,tablet" class=" sorting" tabindex="0" aria-controls="datatable-client-all" rowspan="1" colspan="1" aria-label="Total Balance: activate to sort column ascending">OEM Type</th>
							<th  data-hide="phone,tablet" class=" sorting" tabindex="0" aria-controls="datatable-client-all" rowspan="1" colspan="1" aria-label="Total Balance: activate to sort column ascending">Value</th>
							<th  data-hide="phone,tablet" class=" sorting" tabindex="0" aria-controls="datatable-client-all" rowspan="1" colspan="1">Enabled</th>
							<th style="width: 108px !important;">Actions</th>
		                </tr>
		            </thead>
		            <!--tbody section is required-->
		          <tbody>

                   <form action="{{URL::to('/') }}/priceMarginOEM/multipleDelete" method="post" class="multipleDeleteForm">
                       {{ csrf_field() }}
                     

						<?php $i=0;
							if(count($oem)>0){
							foreach($oem as $oems){  $i++; ?>
							
							<tr role="row">
								<td style="text-align:center;">
                                 <input  type="checkbox" class="id" name="ids[]" value="{{$oems->id}}">
                                 </td>
								<!-- <td scope="row">{{$i}}</td> -->
								<td>{{$oems->name}}</td>
								<td>{{$oems->oem_type}}</td>
								<td>{{$oems->value}}</td>
									<?php 
	                       				if ($oems->status=='active') {

	                       					echo "<td style='color:green'><span>&#10003;</span></td>";
	                       				}else{
	                       					echo "<td style='color:red;'><span>&#9747;</span></td>";
	                       				}
	                       		 ?>
								
								
								<td class="">
									<a class="btn btn-round btn-color btn-hover" href="{{URL::to('/')}}/priceMarginOEM/editPriceMargin/{{$oems->id}}" style="padding: 0px 6px">
										<i class="fa fa-edit"></i>
									</a>
									<a onclick="return confirm('Are you sure you want to delete?')" class="btn btn-round btn-danger"  style="padding: 0px 6px" href="{{URL::to('/')}}/priceMarginOEM/Delete/{{$oems->id}}">
										<i class="fa fa-trash-o"></i>
									</a>
								</td>
							</tr>

							<?php } }else{
								
								echo '<tr role="row"><td colspan="9" style="color:red;">  No records found!!!</td></tr>';
								
							}?>

                       	</form>

                    </tbody>
	  <!--tfoot section is optional-->
	                    <tfoot>
						<tr role="row">
							<th style="width:10px !important; text-align:center; "><input type="checkbox" class="multiIdCheck"/></th>
							<!-- <th class="text-center" style="width: 55px !important;">Sr. no</th> -->
							<th  data-hide="phone,tablet" class=" sorting" tabindex="0" aria-controls="datatable-client-all" rowspan="1" colspan="1" aria-label="Total Balance: activate to sort column ascending">Client</th>
							<th  data-hide="phone,tablet" class=" sorting" tabindex="0" aria-controls="datatable-client-all" rowspan="1" colspan="1" aria-label="Total Balance: activate to sort column ascending">OEM Type</th>
							<th  data-hide="phone,tablet" class=" sorting" tabindex="0" aria-controls="datatable-client-all" rowspan="1" colspan="1" aria-label="Total Balance: activate to sort column ascending">Value</th>
							<th  data-hide="phone,tablet" class=" sorting" tabindex="0" aria-controls="datatable-client-all" rowspan="1" colspan="1">Enabled</th>
							<th style="width: 108px !important;">Actions</th>
		                </tr>
	                    </tfoot>

	                </table>

	                <div class="row">
		                <div class="col-xs-6">
		                	<div class="dataTables_info" id="datatable-client-all_info" role="status" aria-live="polite">  
							@if($oem->total()!=0)
                            Showing {{ $oem->firstItem() }} to {{ $oem->lastItem() }} of {{ $oem->total() }}
                            @endif
				        	</div>
		                </div>

		                <div class="col-xs-6">
			                <div class="dataTables_paginate paging_simple_numbers" id="datatable-client-all_paginate">
				               {{ $oem->links() }}
			                </div>
		                </div>
		                </div>
	                </div>
	                <!--/ End datatable -->
					</div>
					</div>
				 </div>
			  </div>
			</div>
		</div>
	</div>


<form action="{{URL::to('/')}}/priceMarginOEM/index" method="get" class="navbar-form" id="formPerPage">
	<input type="hidden" value="" name="perPage" id="perPage">
</form>
<style>
	.pagination{
		float: right;
		margin:0px;
	}
	#tour-3{
		margin-bottom: 0px;
	}
	.padding_bottom{
		padding-bottom: 15px;
	}
</style>

		


 

 @section('js')
 <script type="text/javascript">
 $(document).ready(function(){
	 $('#paginationPerPage').change(function(){
		$a = $(this).val();
		$('#perPage').val($a);
		$('#formPerPage').submit();
	});
	$('.pagination>li>a').click(function(){
		var a = $(this).attr('href');
		var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1);
		var dataArray='';
		if(hashes.indexOf("page=") >= 0){
			var dataArray = hashes.slice(7);
			$(this).attr('href',a+'&'+dataArray);
		}else{
			$(this).attr('href',a+'&'+hashes);
		}			
	});
 });
	
  function multiId(){
        var checkboxcount = jQuery('.id:checked').length;
         if(checkboxcount != 0 ) {
           if(confirm("Are you sure you want to delete ?")){ 
            jQuery('.multipleDeleteForm').submit();
            }
        }else{
            alert("Please select a record")
            return false;
        }
    }

</script>
@endsection

@endsection