@extends('layouts.default')
@section('content')
<link href="{{ asset('css/bower_components/fontawesome/css/font-awesome.min.css')}}" rel="stylesheet">
<link href="{{ asset('css/bower_components/animate.css/animate.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/bower_components/bootstrap-wysihtml5/src/bootstrap-wysihtml5.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/bower_components/summernote/dist/summernote.css')}}" rel="stylesheet">
<link href="{{ asset('css/bower_components/dropzone/downloads/css/dropzone.css') }}" rel="stylesheet">
<link href="{{ asset('assets/global/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet">
<link href="{{ asset('assets/global/plugins/bower_components/jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/global/plugins/bower_components/chosen_v1.2.0/chosen.min.css') }}" rel="stylesheet">
<style>
    .form-control:focus{
        border: 1px solid #66afe9 !important;
    }
    .bg-warning{
        background-color:#00B1E1 !important;
        border:1px solid #00B1E1 !important;
    }
    input.no-border-right:focus, textarea.no-border-right:focus{
        border: 1px solid #66afe9 !important;
    }
    .note-editor .note-editable
    {
        background: #fff;
    }
    .note-editor.fullscreen {
        position: fixed;
        top: 69px;
        left: 239px;
        bottom: 20px;
        right: 157px;
        z-index: 1050;
    }
    .fullscreen{
        width: 81% !important;
        height: 80% !important;
    }
    .note-editable{
        height: 80% !important;
    }
</style>
<div class="body-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">EDIT THEMES</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-body no-padding">
                    <form class="form-horizontal mt-10" id="commentForm" method="post" action="{{URL::to('/')}}/Theme/update" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{$Theme->id}}">
                        <div class="form-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                        <label for="country_id" class="col-sm-3 control-label">Theme Name<span style="color:red;">*</span></label>
                                            <div class="col-sm-9">

                                                <input type="text" name="theme_name" class="form-control" id="theme_name" placeholder="Theme name" value="{{$Theme->theme_name}}">

                                                @if($errors->has('theme_name'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('theme_name') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                            <div class="col-lg-6">
                                            
                                            <label for="first_name" class="col-sm-3 control-label">
                                                Logo<span style="color:red;">*</span>
                                            </label>
                                            <div class="col-sm-7">
                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                                        <span class="fileinput-filename"></span>
                                                    </div>
                                                    <span class="input-group-addon btn btn-success btn-file" style="background-color: #00B1E1;border-color: #00B1E1;">
                                                        <span class="fileinput-new">
                                                            Select file
                                                        </span>
                                                        <span class="fileinput-exists">
                                                            Change
                                                        </span>
                                                        <input type="file" name="logo" value="{{$Theme->logo}}">
                                                    </span>
                                                    <a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                                @if($errors->has('logo'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('logo') }}</strong>
                                                </span>
                                                @endif

                                                
                                            </div>
                                            <div class="col-sm-2">
                                                @if(!empty($Theme->logo))
                                                <img src="{{asset('/images')}}/{{$Theme->logo}}" height="50" width="100%">
                                               @else
                                               --
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="email" class="col-sm-3 control-label">Email<span style="color:red;">*</span></label>
                                            <div class="col-sm-9" style="padding-bottom:20px;">
                                                <input type="text" value="{{$Theme->email}}" name="email" class="form-control" id="email" placeholder="Email">

                                                @if ($errors->has('email'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp;
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="css_file" class="col-sm-3 control-label">Css File<span style="color:red;">*</span></label>
                                            <div class="col-sm-9">
                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                                        <span class="fileinput-filename"></span>
                                                    </div>
                                                    <span class="input-group-addon btn btn-success btn-file" style="background-color: #00B1E1;border-color: #00B1E1;">
                                                        <span class="fileinput-new">
                                                            Select file
                                                        </span>
                                                        <span class="fileinput-exists">
                                                            Change
                                                        </span>
                                                        <input type="file" name="css_file" value="{{$Theme->css_file}}">
                                                    </span>
                                                    <a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                                @if($errors->has('css_file'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('css_file') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="col-lg-12">
                                   
                                        <div class="col-lg-6">
                                            <label for="head_active_color" class="col-sm-3 control-label">Js File<span style="color:red;">*</span></label>
                                            <div class="col-sm-9">
                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                                        <span class="fileinput-filename"></span>
                                                    </div>
                                                    <span class="input-group-addon btn btn-success btn-file" style="background-color: #00B1E1;border-color: #00B1E1;">
                                                        <span class="fileinput-new">
                                                            Select file
                                                        </span>
                                                        <span class="fileinput-exists">
                                                            Change
                                                        </span>
                                                        <input type="file" name="js_file" value="{{$Theme->js_file}}">
                                                    </span>
                                                    <a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                                @if($errors->has('js_file'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('js_file') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        
                                    </div>

                                    <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <label for="description" class="col-sm-2 control-label" style="font-weight:800; text-align: left;">Email Template</label>
                                        <div class="col-sm-12">
                                            <textarea type="text" name="email_template" class="form-control" id="summernote-textarea">
                                                 {{$Theme->email_template}}
                                            </textarea>
                                            @if ($errors->has('email_template'))
                                            <span class="help-block" style="color:red;">
                                                <strong>{{ $errors->first('email_template')}}</strong>
                                            </span>
                                            @endif
                                            &nbsp;
                                        </div>
                                    </div>
                                </div>


                                </div>
                            </div>
                            <div class="form-group"></div>
                        </div>
                        <div class="form-footer">
                            <div class="col-sm-offset-5">
                                <button class="btn btn-color btn-hover" type="submit">Submit</button>
                                <a class="btn btn-danger" href="{{URL::to('/')}}/Theme/index">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- END @PAGE CONTENT -->
@section('js')
<script src="{{ asset('assets/global/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>
<script src="{{ asset('assets/global/plugins/bower_components/jasny-bootstrap-fileinput/js/jasny-bootstrap.fileinput.min.js')}}"></script>
<script src="{{asset('assets/global/plugins/bower_components/bootstrap-wysihtml5/lib/js/wysihtml5-0.3.0.min.js')}}"></script>
<script src="{{asset('assets/global/plugins/bower_components/bootstrap-wysihtml5/src/bootstrap-wysihtml5.js')}}"></script>
<script src="{{asset('assets/global/plugins/bower_components/summernote/dist/summernote.min.js')}}"></script>
<script type="text/javascript"
<script type="text/javascript">
var BlankonFormWysiwyg = function () {

    return {
        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            BlankonFormWysiwyg.bootstrapWYSIHTML5();
            BlankonFormWysiwyg.summernote();
        },
        // =========================================================================
        // BOOTSTRAP WYSIHTML5
        // =========================================================================
        bootstrapWYSIHTML5: function () {
            if ($('#wysihtml5-textarea').length) {
                $('#wysihtml5-textarea').wysihtml5();
            }
        },
        // =========================================================================
        // SUMMERNOTE
        // =========================================================================
        summernote: function () {
            if ($('#summernote-textarea').length) {
                $('#summernote-textarea').summernote();
            }
        }

    };

}();

// Call main app init
BlankonFormWysiwyg.init();
</script>

@endsection
@endsection