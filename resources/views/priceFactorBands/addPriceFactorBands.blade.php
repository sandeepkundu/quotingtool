@extends('layouts.default') @section('content')


<style>
    .form-control:focus{
        border: 1px solid #66afe9 !important;
    }
    .bg-warning{
        background-color:#00B1E1 !important;
        border:1px solid #00B1E1 !important;
    }
    input.no-border-right:focus, textarea.no-border-right:focus{
        border: 1px solid #66afe9 !important;
    }
</style>
<div class="body-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">ADD <?php echo strtoupper(key($_GET)); ?> PRICING</h3>
                    </div>

                    <div class="clearfix"></div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body no-padding">

                    <form class="form-horizontal mt-10" id="commentForm" method="post" action="{{URL::to('/')}}/priceFactorBands/insertPriceFactorBands" >
                        {{ csrf_field() }}
                        <input type="hidden" name="cat" value="<?php echo key($_GET); ?>">
                        <div class="form-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="parent_id" class="col-sm-3 control-label">Clients</label>
                                            <div class="col-sm-9">
                                                <div class="btn-group hierarchy-select" data-resize="auto" id="example-one">
                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <span class="selected-label pull-left">&nbsp;</span>
                                                        <span class="caret"></span>
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                    </button>
                                                    <div class="dropdown-menu open">
                                                        <div class="hs-searchbox">
                                                            <input type="text" class="form-control" autocomplete="off">
                                                        </div>
                                                        <ul class="dropdown-menu inner store" role="menu">
                                                            <li data-value="0" data-level="1" class="aa" data-default-selected="">
                                                                <a href="#">Select Client</a>
                                                           </li>
                                                            @foreach($parent_ids as $val)
                                                            <li class="par" data-value="{{$val['id']}}" data-level="{{$val['level']}}">
                                                                <a href="#">{{$val['name']}}</a>
                                                            </li>
                                                            @endforeach
                                                        </ul>

                                                        <ul class="dropdown-menu inner enterprise" style="display: none;" role="menu" >
                                                            <li data-value="0" data-level="1" class="aa">
                                                                <a href="#">Select Client</a>
                                                            </li>
                                                        </ul>


                                                    </div>
                                                    <input class="hidden hidden-field" name="parent_id" id="parent_id" aria-hidden="true" type="text"/>
                                                </div>			
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="type_id" class="col-sm-3 control-label">Product  Type<span style="color:red;">*</span></label>
                                            <div class="col-sm-9" style="padding-bottom:20px;">
                                                <select id="type_id" name="type_id" class="form-control">
                                                    <option value=""> -- Select  Product Type -- </option>
                                                    @foreach ($types as $type)
                                                    <option value="{{ $type->id }}"  {{(old("type_id") == $type->id) ? "selected":"" }}>{{ $type->name }}</option>
                                                    @endforeach             
                                                </select>
                                                @if ($errors->has('type_id'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('type_id') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="option_id" class="col-sm-3 control-label"><?php echo key($_GET); ?> Name<span style="color:red;">*</span></label>
                                            <div class="col-sm-9" style="padding-bottom:20px;">
                                                <select id="option_id" name="option_id" class="form-control">
                                                    <option value=""> -- Select Defect Type -- </option>
                                                    @foreach ($option as $options)
                                                    <option value="{{ $options->id }}" {{(old("option_id") == $options->id) ? "selected":"" }}>{{ $options->name }}</option>
                                                    @endforeach             
                                                </select>
                                                @if ($errors->has('option_id'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('option_id') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="factor_name" class="col-sm-3 control-label">Price Factor<span style="color:red;">*</span></label>

                                            <div class="col-sm-9">
                                                <input readonly value="{{key($_GET)}}" class=" form-control" id="factor_name" name="factor_name" minlength="2" type="text" placeholder="Value "> 
                                                @if ($errors->has('factor_name'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('factor_name') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="price_band_id" class="col-sm-3 control-label">Price Bands<span style="color:red;">*</span></label>
                                            <div class="col-sm-9" style="padding-bottom:20px;">
                                                <select id="price_band_id" name="price_band_id" class="form-control">
                                                    <option value=""> -- Select Value from --Value to -- </option>
                                                    @foreach ($priceBands as $priceBand)
                                                    <option value="{{ $priceBand->id }}" {{(old("option_id") == $priceBand->id) ? "selected":"" }}>{{ $priceBand->value_from }} - {{ $priceBand->value_to }}</option>
                                                    @endforeach             
                                                </select>
                                                @if ($errors->has('price_band_id'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('price_band_id') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="value_to" class="col-sm-3 control-label">Value<span style="color:red;">*</span></label>
                                            <div class="col-sm-9">
                                                <input value="{!!old('value')!!}" class=" form-control" id="value" name="value" minlength="2" type="text" placeholder="Value "> 
                                                @if ($errors->has('value'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('value') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <!-- /.form-group -->
                <div class="form-group"></div>
                <!-- /.form-group -->
            </div>
            <!-- /.form-body -->
            <div class="form-footer">
                <div class="col-sm-offset-5">
                    <button class="btn btn-color btn-hover" type="submit">Submit</button>
                    <a class="btn btn-danger" href="{{URL::to('/')}}/priceFactorBands/index?{{key($_GET)}}">Cancel</a>
                </div>
            </div>
            </form>

        </div>
    </div>
    <!-- /.panel -->
    <!--/ End horizontal form -->
</div>
</div>
</div>
<!--==============================Modal Popup=======================-->
<div class="modal modal-success fade in" id="modal-bootstrap-tour" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-dialog" role="document" style="margin: 150px auto;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title"><span class="username">Alert!!!</span></h4>
            </div>
            <div class="modal-body">
                You can not make the parent of this store because this is already child of another store.

            </div>
        </div>
    </div>
</div>
<!--==============================Modal Popup=======================--> 
<style type="text/css">
    #ave_collect_size{
        border-right:1px solid rgb(221, 221, 221) !important;
    }
    #ave_collect_size:focus{
        border-right:1px solid #66afe9 !important
    }
    #example-one{
        width: 100%;
    }
    .dropdown-menu li.active:hover a, .dropdown-menu li.active:focus a, .dropdown-menu li.active:active a{
        background-color:#00B1E1 !important;
    }
    .dropdown-menu li.active a{
        background-color:#00B1E1 !important;
    }
    .btn-default.dropdown-toggle.btn-default{
        background-color: white;
    }
    .btn-default.active.focus, .btn-default.active:focus, .btn-default.active:hover, .btn-default:active.focus, .btn-default:active:focus, .btn-default:active:hover, .open>.dropdown-toggle.btn-default.focus, .open>.dropdown-toggle.btn-default:focus, .open>.dropdown-toggle.btn-default:hover{
        background-color: white;
    }
    .dropdown-menu li > a:hover:before{
        border-left:3px solid #00B1E1;
    }
    .modal-success .modal-header {
        background-color: #00B1E1 !important;
        border:1px solid #00B1E1;
        border:none;
    }
    .modal-success .modal-header:before {
        content: ""; 
        border: 1px solid #00B1E1 !important; 
    }
</style>		
<link href="{{asset('css/hierarchy-select.min.css')}}" rel="stylesheet">
<!-- END @PAGE CONTENT -->
@section('js')
<script src="{{ asset('js/hierarchy-select.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function () {
      $('#example-one').hierarchySelect();
//    $('#client_type').on('change', function () {
//        var type = this.value;
//        if (type == 'enterprise') {
//            $('.store').css('display', 'none');
//            $('.enterprise').css('display', 'block');
//        }
//        if (type == 'store') {
//            $('.enterprise').css('display', 'none');
//            $('.store').css('display', 'block');
//        }
//        if (type == '') {
//            $('.enterprise').css('display', 'none');
//            $('.store').css('display', 'block');
//        }
//    });
//
//    $('.par').click(function () {
//        var a = $(this).attr('data-level');
//        if (a == 3) {
//            setTimeout(function () {
//
//                $('#modal-bootstrap-tour').modal('show');
//                $('.selected-label').text('Select Client');
//                $('#parent_id').val('0');
//
//                $("li").removeClass("active");
//                $('.aa').addClass("active");
//
//            }, 200);
//
//        }
//    });
});
</script>
@endsection
@endsection
