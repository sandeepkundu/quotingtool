@extends('layouts.default')
@section('content')
<style>
	.form-control:focus{
		border: 1px solid #66afe9 !important;
	}
	.bg-warning{
		background-color:#00B1E1 !important;
		border:1px solid #00B1E1 !important;
	}
	input.no-border-right:focus, textarea.no-border-right:focus{
		border: 1px solid #66afe9 !important;
	}
</style>
<div class="body-content animated fadeIn">
	<div class="row">
		<div class="col-lg-12">
			<div class="panel rounded shadow">
				<div class="panel-heading">
					<div class="pull-left">
						<h3 class="panel-title">EDIT QUOTE NOTES</h3>
					</div>
					<div class="clearfix"></div>
				</div>
				
				<div class="panel-body no-padding">
					<form class="form-horizontal mt-10" id="commentForm" method="post" action="{{URL::to('/')}}/QuotesNotes/updateQuotesNotes" >
						{{ csrf_field()}}
						<input type="hidden" name="id" value="{{$QuotesNotes->id}}">
						<input type="hidden" name="user_id" value="{{$QuotesNotes->user_id}}">
						<input type="hidden" name="quote_id" value="{{$QuotesNotes->quote_id}}">
						<div class="form-body">
							<div class="form-group">
								<div class="row">
									<div class="col-lg-12">
									<div class="col-lg-6">
											<label for="display_level" class="col-sm-3 control-label">Display Lavel<span style="color:red;">*</span></label>
											<div class="col-sm-9">
												<!-- <input type="text" value="{{$QuotesNotes->display_level}}" name="display_level" id="display_level"  class="form-control" placeholder="Display level" /> -->
												
												<select name="display_level" class="form-control">
													<option value="">--Select Display Level--</option>
													<option value="1" {{($QuotesNotes->display_level == 1) ? "selected":"" }}>Admin</option>
													<option value="2" {{($QuotesNotes->display_level == 2) ? "selected":"" }}>User</option>
													
												</select>
												@if ($errors->has('display_level'))
												<span class="help-block" style="color:red;">
													<strong>{{ $errors->first('display_level') }}</strong>
												</span>
												@endif
												&nbsp
											</div>
										</div>
										
										<div class="col-lg-6">
										
											<label for="option_id" class="col-sm-3 control-label">Note<span style="color:red;">*</span></label>
											<div class="col-sm-9">
												<input type="text" name="note" value="{{$QuotesNotes->note}}" id="note" class="form-control" placeholder="Note"  />
												@if ($errors->has('note'))
												<span class="help-block" style="color:red;">
													<strong>The note field is required.</strong>
												</span>
												@endif
												&nbsp
											</div>
										</div>
									</div>
									
								</div>
							</div>
							<div class="form-group"></div>
						</div>
						<div class="form-footer">
							<div class="col-sm-offset-5">
								<button class="btn btn-color btn-hover" type="submit">Submit</button>
								<a class="btn btn-danger" href="{{URL::to('/')}}/QuoteManagements/QuoteDetails/{{$QuotesNotes->quote_id}}">Cancel</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
