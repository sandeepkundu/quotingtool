@extends('layouts.default') @section('content')
@section('css')
<!-- START @PAGE LEVEL STYLES
<link href="{{asset('assets/global/plugins/bower_components/datatables/css/dataTables.bootstrap.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/bower_components/datatables/css/datatables.responsive.css')}}" rel="stylesheet"> -->
<!--/ END PAGE LEVEL STYLES -->

@endsection
<style>
    .table-success tbody tr td .sorting_1 {
        background: red !important;
        color: white;
        border-bottom: 1px solid red !important;
    }
    .dataTable thead tr th:first-child {
        min-width: 10px !important;
    }
    .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
        background-color: #178aa9 !important;
        border: 1px solid #178aa9;
    }
    thead th {
        position: relative;
        background-image: none !important;
    }

    thead th.sorting:after,
    thead th.sorting_asc:after,
    thead th.sorting_desc:after {
        position: absolute;
        top: 12px;
        right: 8px;
        display: block;
        font-family: FontAwesome;
    }
    thead th.sorting:after {
        content: "\f0dc";
        color: #ddd;
        font-size: 0.8em;
        padding-top: 0.12em;
    }
    thead th.sorting_asc:after {
        content: "\f0de";
    }
    thead th.sorting_desc:after {
        content: "\f0dd";
    }

</style>
<!-- START @PAGE CONTENT -->

@if(isset($toast['message']))
{{ $toast['message'] }}
@endif

<!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-table"></i>PRODUCTS<span></span></h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label"> </span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="{{URL::to('/')}}/admin/dashboard">Dashboard</a>

            </li>

            <li class="active">                	
            </li>
        </ol>
    </div><!-- /.header-content -->
</div><!--/ End page header -->
<!-- Start body content -->
<div class="body-content animated fadeIn">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-tab panel-tab-double shadow">
                <div class="panel-body no-padding">
                    <div class="panel panel-default shadow no-margin">
                        <div class="panel-heading">
                            <a  href="{{URL::to('/')}}/products/index" class="btn btn-default tooltips" data-toggle="tooltip" data-placement="top" data-title="Reload" data-action="refresh" data-original-title="" title="" id="ml5"><i class="icon-refresh icons"></i></a>

                            <a href="javascript:void(0);" class="btn btn-danger tooltips" data-toggle="tooltip" data-placement="top" data-title="Delete" data-original-title="" title="" onclick="return multiId()" ><i class="icon-trash icons"></i></a>
                            <div class="pull-right" id="mt0">
                                <a href="{{URL::to('/')}}/products/addProduct" class="btn btn-color btn-hover"><i class="fa fa-plus"></i> &nbsp;Add new product</a>
                            </div>
                            <div class="clearfix"></div>
                        </div><!--/.panel-heading -->
                        <!--Panel Body-->
                        <div class="panel-body">
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="tab-client-all">
                                    <!-- Start datatable -->
                                    <div id="datatable-client-all_wrapper" class="dataTables_wrapper form-inline">
                                        <form action="{{URL::to('/') }}/products/multipleDelete" method="post" class="multipleDeleteForm">
                                            {{ csrf_field() }}
                                            <table id="datatable-ajax" class="table table-striped table-success">
                                                <thead>
                                                    <tr role="row">
                                                        <th style="width:10px !important;"><input type="checkbox" class="multiIdCheck"/></th>
														<?php
														$ord = ($order == 'client_f') ?  'client_r' :  'client_f';
														$cls = ($order == 'client_f') ? 'sorting_asc' : ($order == 'client_r') ? 'sorting_desc' : 'sorting';
														?>
														<th class="{{$cls}}" style="width:250px; cursor:hand" onclick="location.href='{{URL::to('/') }}/products/index?order={{$ord}}'">Client</th>
 														<?php
														$ord = ($order == 'country_f') ?  'country_r' :  'country_f';
														$cls = ($order == 'country_f') ? 'sorting_asc' : ($order == 'country_r') ? 'sorting_desc' : 'sorting';
														?>
														<th class="{{$cls}}" onclick="location.href='{{URL::to('/') }}/products/index?order={{$ord}}'">Country</th>
 														<?php
														$ord = ($order == 'model_f') ?  'model_r' :  'model_f';
														$cls = ($order == 'model_f') ? 'sorting_asc' : ($order == 'model_r') ? 'sorting_desc' : 'sorting';
														?>                                                        
														<th class="{{$cls}}" onclick="location.href='{{URL::to('/') }}/products/index?order={{$ord}}'">Model</th>
 														<?php
														$ord = ($order == 'brand_f') ?  'brand_r' :  'brand_f';
														$cls = ($order == 'brand_f') ? 'sorting_asc' : ($order == 'brand_r') ? 'sorting_desc' : 'sorting';
														?>                                                        
														<th class="{{$cls}}" onclick="location.href='{{URL::to('/') }}/products/index?order={{$ord}}'">Brand</th>
 														<?php
														$ord = ($order == 'proc_f') ?  'proc_r' :  'proc_f';
														$cls = ($order == 'proc_f') ? 'sorting_asc' : ($order == 'proc_r') ? 'sorting_desc' : 'sorting';
														?>
                                                        <th class="{{$cls}}" onclick="location.href='{{URL::to('/') }}/products/index?order={{$ord}}'">Processor</th>
 														<?php
														$ord = ($order == 'scr_f') ?  'scr_r' :  'scr_f';
														$cls = ($order == 'scr_f') ? 'sorting_asc' : ($order == 'scr_r') ? 'sorting_desc' : 'sorting';
														?>
														<th class="{{$cls}}" onclick="location.href='{{URL::to('/') }}/products/index?order={{$ord}}'">Screen</th>
 														<?php
														$ord = ($order == 'prod_f') ?  'prod_r' :  'prod_f';
														$cls = ($order == 'prod_f') ? 'sorting_asc' : ($order == 'prod_r') ? 'sorting_desc' : 'sorting';
														?>
                                                        <th class="{{$cls}}" onclick="location.href='{{URL::to('/') }}/products/index?order={{$ord}}'">Product Type </th>

                                                        <th>Value</th>
                                                        <th>Enabled</th>
                                                        <th class="" style="width: 100px !important;">Actions</th>
                                                    </tr>
                                                </thead>
                                                <!--tbody section is required-->
                                                <tbody>

                                                    @if (count($products) > 0) 
                                                    @foreach ($products as $product) 
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox" class="id" name="ids[]" value="{{$product->id}}">
                                                        </td>
                                                        <td>
                                                            @if(isset($product->fullpath))
                                                            {{$product->fullpath}}
                                                            @else
                                                            Default
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if(isset($product->ctry))
                                                            {{$product->ctry}}
                                                            @else
                                                            All
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if(isset($product->model))
                                                            {{$product->model}}
                                                            @else
                                                            N/A
                                                            @endif	
                                                        </td>
                                                        <td>
                                                            @if(isset($product->Brand->name))
                                                            {{$product->Brand->name}}
                                                            @else
                                                            N/A
                                                            @endif	
                                                        </td>
                                                        <td>
                                                            @if(isset($product->Processor->name))
                                                            {{$product->Processor->name}}
                                                            @else
                                                            N/A
                                                            @endif
                                                        </td>
														 <td>
                                                            @if(isset($product->Screen->name))
                                                            {{$product->Screen->name}}
                                                            @else
                                                            N/A
                                                            @endif
                                                        </td>
                                                        <td>

                                                            @if(isset($product->Type->name))
                                                            {{$product->Type->name}}
                                                            @else
                                                            N/A
                                                            @endif
                                                        </td>

                                                        <td>
                                                            @if(isset($product->value))
                                                            @if($product->value_factor=='explicit price')
                                                            <span> ${{$product->value}}</span>
                                                            @elseif($product->value_factor=='margin percent')
                                                            <span style="color:green;"> {{$product->value}}%</span>
                                                            @elseif($product->value_factor=='margin price')
                                                            <span style="color:green;"> ${{$product->value}}</span>
                                                            @endif

                                                            @else
                                                            N/A
                                                            @endif	
                                                        </td>

                                                        @if ($product->status == 'active') 
                                                        <td style='color:green'><span>&#10003</span></td>
                                                        @else 
                                                        <td style='color:red;'><span>&#9747</span></td>
                                                        @endif

                                                        <td>
                                                            <a class="btn btn-round btn-color btn-hover" href="{{URL::to('/')}}/products/editProduct/{{$product->id}}" style="padding: 0px 4px;">
                                                                <i class="fa fa-edit"></i>
                                                            </a>
                                                            <a onclick="return confirm('Are you sure you want to delete?')" class="btn btn-round btn-danger"  style="padding: 0px 4px;" href="{{URL::to('/')}}/products/Delete/{{$product->id}}">
                                                                <i class="fa fa-trash-o"></i>
                                                            </a>
                                                            <a class="btn btn-round btn-color btn-hover" href="{{URL::to('/')}}/products/clones/{{$product->id}}" style="padding: 0px 4px;"><i class="fa fa-clone" aria-hidden="true"></i></a>
                                                        </td>
                                                    </tr>

                                                    @endforeach
                                                    @else  
                                                    <tr><td colspan="11" style="color:red;"> No records found!!!</td> </tr>
                                                    @endif

                                                </tbody>
                                                <!--tfoot section is optional-->
                                                <tfoot>
                                                    <tr role="row">
                                                        <th style="width:10px !important;"><input type="checkbox" class="multiIdCheck"/></th>
                                                        <th style="width:250px;">Client</th>
														<th>Country</th>
                                                        <th>Model</th>
                                                        <th>Brand</th>
                                                        <th >Processor</th>
														<th >Screen</th>
                                                        <th>Product Type </th>
                                                        <th>Value</th>
                                                        <th>Enabled</th>
                                                        <th class="" style="width: 100px !important;">Actions</th>
                                                    </tr>
                                                </tfoot>
                                            </table>


<div class="row" style="margin-bottom:50px">
<div class="col-xs-6">
<div class="dataTables_info" id="datatable-client-all_info" role="status" aria-live="polite">
@if($products->total()!=0)
Showing {{ $products->firstItem() }} to {{ $products->lastItem() }} of {{ $products->total() }}
@endif
</div>
</div>
<div class="col-xs-6" style="text-align: right;">
<div class="dataTables_paginate paging_simple_numbers" id="datatable-client-all_paginate">
{{ $products->links() }}

</div>
</div>
</div> 
                                        </form>
                                    </div>                                    <!--/ End datatable -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .form-control:focus{
        border: 1px solid #66afe9 !important;
    }

</style>
<style>

    .pagination{
        float: right;
        margin:0px;
    }
    #tour-3{
        margin-bottom: 0px;
    }
    .padding_bottom{
        padding-bottom: 15px;
    }
</style>		
@section('js')
<script>
$(document).ready(function () {
    /*==========For pagination==============*/

    $('#paginationPerPage').change(function () {
        $a = $(this).val();
        $('#perPage').val($a);
        $('#formPerPage').submit();
    });
    $('.pagination>li>a').click(function () {
        var a = $(this).attr('href');
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1);
        // alert(hashes);
        var dataArray = '';
        if (hashes.indexOf("page=") >= 0) {
            var dataArray = hashes.slice(7);
            $(this).attr('href', a + '&' + dataArray);
        } else {
            $(this).attr('href', a + '&' + hashes);
        }
    });
});

/*
    $(document).ready(function () {
        $('#datatable-ajax').DataTable({
            "order": [[1, "asc"]],
            "aoColumnDefs": [
                {'bSortable': false, 'aTargets': [-1, -9]}
            ],
            language: {
                searchPlaceholder: "Search records"
            },
            "lengthMenu": [[30, 60, 90, -1], [30, 60, 90, "All"]],
            "columnDefs": [{
                    "targets": 0,
                    "orderable": false,
                    'min-width': '10px',
                    "width": "5%"
                }],
            "bSortClasses": false,
            "searchHighlight": true,
        });

    });
*/
    function multiId() {
        var checkboxcount = jQuery('.id:checked').length;
        if (checkboxcount != 0) {
            if (confirm("Are you sure you want to delete ?")) {
                jQuery('.multipleDeleteForm').submit();

            }
        } else {
            alert("Please select a record")
            return false;
        }
    }

</script>
<!-- START @PAGE LEVEL PLUGINS
<script src="{{asset('assets/global/plugins/bower_components/datatables/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/global/plugins/bower_components/datatables/js/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('assets/global/plugins/bower_components/datatables/js/datatables.responsive.js')}}"></script> -->
<!--/ END PAGE LEVEL PLUGINS -->
@endsection
@endsection