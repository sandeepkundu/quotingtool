@extends('layouts.default') @section('content')
<style>
.form-control:focus{
    border: 1px solid #66afe9 !important;
}
.bg-warning{
    background-color:#00B1E1 !important;
    border:1px solid #00B1E1 !important;
}
input.no-border-right:focus, textarea.no-border-right:focus{
    border: 1px solid #66afe9 !important;
}
.rdio-theme input[type=radio]:checked + label::after {
    border-color: #63D3E9;
    background-color: #63D3E9;
}

#example-one{
    width: 100%;
}
.dropdown-menu li.active:hover a, .dropdown-menu li.active:focus a, .dropdown-menu li.active:active a{
    background-color:#00B1E1 !important;
}
.dropdown-menu li.active a{
    background-color:#00B1E1 !important;
}
.btn-default.dropdown-toggle.btn-default{
    background-color: white;
}
.btn-default.active.focus, .btn-default.active:focus, .btn-default.active:hover, .btn-default:active.focus, .btn-default:active:focus, .btn-default:active:hover, .open>.dropdown-toggle.btn-default.focus, .open>.dropdown-toggle.btn-default:focus, .open>.dropdown-toggle.btn-default:hover{
    background-color: white;
}
.dropdown-menu li > a:hover:before{
    border-left:3px solid #00B1E1;
}
.modal-success .modal-header {
    background-color: #00B1E1 !important;
    border:1px solid #00B1E1;
    border:none;
}
.modal-success .modal-header:before {
    content: ""; 
    border: 1px solid #00B1E1 !important; 

}
</style>

<link href="{{ asset('assets/global/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet">
<link href="{{ asset('assets/global/plugins/bower_components/jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/global/plugins/bower_components/chosen_v1.2.0/chosen.min.css') }}" rel="stylesheet">
<link href="{{asset('css/hierarchy-select.min.css')}}" rel="stylesheet">
<div class="body-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">ADD NEW PRODUCT</h3>

                    </div>

                    <div class="clearfix"></div>
                </div>
                @if ($errors->has('Duplicate'))
                <div class="alert alert-info alert-block">

                    <span>{{ $errors->first('Duplicate') }}</span>
                </div>
                @endif
                <div class="panel-body no-padding">

                    <form class="form-horizontal mt-10" enctype="multipart/form-data" id="commentForm" method="post" action="{{URL::to('/')}}/products/insertProduct" >
                        {{ csrf_field() }}
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <div class="form-body">
                            <div class="form-group">

                                <div class="row">
                                    <div class="col-sm-12 col-lg-12" style="padding-bottom: 35px;">

                                        <div class="col-sm-8 col-sm-offset-2">
                                            <div class="col-sm-4 pull-left" >
                                                <div class="rdio rdio-theme circle">
                                                    <input id="radio-type-rounded1" onclick="radio('Processor');" value="Processor" type="radio" class="a proccessor" name="radiotype1" checked="" id="processor" {{ ('Processor' == old('radiotype1'))? 'checked' : '' }}>
                                                           <label for="radio-type-rounded1">Processor Pricing</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="rdio rdio-theme circle ">
                                                    <input id="radio-type-circle1" onclick="radio('Model');" value="Model" type="radio" name="radiotype1" class="a model" id="model" {{ ('Model' == old('radiotype1'))? 'checked' : '' }}>
                                                           <label for="radio-type-circle1">Model Pricing</label>
                                                </div>
                                            </div>

                                            <div class="col-sm-4">
                                                <div class="rdio rdio-theme circle ">
                                                    <input id="radio-type-circle2" onclick="radio('Lcd');" value="Lcd" type="radio" name="radiotype1" class="a lcd" id="lcd" {{ ('Lcd' == old('radiotype1'))? 'checked' : '' }}>
                                                           <label for="radio-type-circle2">LCD Pricing</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="parent_id" class="col-sm-3 control-label">Client</label>
                                            <div class="col-sm-9">
                                                <div class="btn-group hierarchy-select" data-resize="auto" id="example-one">
                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <span class="selected-label pull-left">&nbsp;</span>
                                                        <span class="caret"></span>
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                    </button>
                                                    <div class="dropdown-menu open">
                                                        <div class="hs-searchbox">
                                                            <input type="text" class="form-control" autocomplete="off">
                                                        </div>
                                                        <ul class="dropdown-menu inner store" role="menu">
                                                            <li data-value="0" data-level="1" class="aa" data-default-selected="">
                                                                <a href="#">Select Client</a>
                                                            </li>
                                                            @foreach($parent_ids as $val)
                                                            <li class="par" data-value="{{$val['id']}}" data-level="{{$val['level']}}">
                                                                <a href="#">{{$val['name']}}</a>
                                                            </li>
                                                            @endforeach
                                                        </ul>

                                                        <ul class="dropdown-menu inner enterprise" style="display: none;" role="menu" >
                                                            <li data-value="0" data-level="1" class="aa">
                                                                <a href="#">Select Client</a>
                                                            </li>
                                                        </ul>


                                                    </div>
                                                    <input class="hidden hidden-field" name="parent_id" id="parent_id" aria-hidden="true" type="text"/>
                                                </div>          
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="model" class="col-sm-3 control-label">Model<span class="Model_star" style="color:red; display: none;">*</span></label>
                                            <div class="col-sm-9">
                                                <input value="{!!old('model')!!}" type="text" name="model" class="form-control processor" id="model"
                                                       placeholder="model">
                                                @if ($errors->has('model'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('model') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="type_id" class="col-sm-3 control-label">Product Type<span style="color:red;">*</span></label>
                                            <div class="col-sm-9">
                                                <select id="type_id" name="type_id" class="form-control product">
                                                    <option value=""> -- Select Type -- </option>
                                                    @if(count($product_options)>0)
                                                    @foreach ($product_options as $code)
                                                    @if($code->type=='Product')
                                                    <option value="{{ $code->id }}" class="{{ ($code->name == 'LCD')? 'lcd-option' : '' }}" {{ ($code->id == old('type_id'))? 'selected' : '' }}>{{ $code->name }}</option>
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                </select>
                                                @if ($errors->has('type_id'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('type_id') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                                <label for="brand_id" class="col-sm-3 control-label">Brand <!-- <span style="color:red;">*</span> --></label>
                                            <div class="col-sm-9">
                                                <input type="hidden" id="bandOldId" value="{{old('brand_id')}}"/> 
                                                <select id="brand_id" name="brand_id" class="form-control product">
                                                    <option value=""> -- Select Brand -- </option>

                                                </select>

                                                @if ($errors->has('brand_id'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('brand_id') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="processor_id" class="col-sm-3 control-label">Processor</label>
                                            <div class="col-sm-9">

                                                <select id="processor_id" name="processor_id" class="form-control">
                                                    <option value=""> -- Select Processor -- </option>


                                                </select>
                                                <input type="hidden" id="processor_old" value="{{old('processor_id')}}"/> 

                                                &nbsp
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <label for="screen_id" class="col-sm-3 control-label">Screen <span style="color:red;" id="screen_star">*</span></label>
                                            <div class="col-sm-9">
                                                <select id="screen_id" name="screen_id" class="form-control processor">
                                                    <option value=""> -- Select Screen Size -- </option>
                                                </select>
                                                <input type="hidden" id="screen_old" value="{{old('screen_id')}}"/>
                                                @if ($errors->has('screen_id'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('screen_id') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                                <label for="dvd_id" class="col-sm-3 control-label">DVD<!-- <span style="color:red;">*</span> --></label>
                                            <div class="col-sm-9">
                                                <select id="dvd_id" name="dvd_id" class="form-control processor">
                                                    <option value=""> -- Select DVD -- </option>
                                                </select>
                                                <input type="hidden" id="dvd_old" value="{{old('dvd_id')}}"/>

                                                @if ($errors->has('dvd_id'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('dvd_id') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                                <label for="hdd_id" class="col-sm-3 control-label">HDD <!-- <span style="color:red;">*</span> --></label>
                                            <div class="col-sm-9">
                                                <select id="hdd_id" name="hdd_id" class="form-control processor">
                                                    <option value=""> -- Select HDD -- </option>
                                                </select>
                                                <input type="hidden" id="hdd_old" value="{{old('hdd_id')}}"/>
                                                @if ($errors->has('hdd_id'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('hdd_id') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                                <label for="ram_id" class="col-sm-3 control-label">RAM<!--  <span style="color:red;">*</span> --></label>
                                            <div class="col-sm-9">
                                                <select id="ram_id" name="ram_id" class="form-control processor">
                                                    <option value=""> -- Select RAM -- </option>
                                                </select>
                                                <input type="hidden" id="ram_old" value="{{old('ram_id')}}"/>
                                                @if ($errors->has('ram_id'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('ram_id') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                        <div class="col-lg-6">

                                            <label for="image" class="col-sm-3 control-label">Image</label>
                                            <div class="col-sm-9">
                                                <div class="fileinput input-group fileinput-new" data-provides="fileinput">
                                                    <div class="form-control image" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                        <span class="fileinput-filename"></span>
                                                    </div>
                                                    <span class="input-group-addon btn btn-success btn-file" style="border-color:#009dc8 !important; ;background-color:#009dc8 !important;">
                                                        <span class="fileinput-new" style="background-color:#009dc8 !important">Select file</span>
                                                        <span class="fileinput-exists" style="background-color:#009dc8 !important">Change</span>
                                                        <input type="hidden" value="" name="testImg">
                                                        <input type="file" name="image" class="hidden" id="file" >
                                                    </span>
                                                    <a href="javascript:void(0)" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                                @if ($errors->has('image'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('image') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="value_factor" class="col-sm-3 control-label">Pricing Type <span class="Model_star" style="color:red; display: none;">*</span></label>
                                            <div class="col-sm-9">  
                                                <select id="value_factor" name="value_factor" class="form-control">

                                                    <option value="explicit price" {{ ('explicit price'== old('value_factor'))? 'selected' : '' }}> Explicit price </option>
                                                    <option value="margin percent" {{ ('margin percent'== old('value_factor'))? 'selected' : '' }}> Margin percent </option>
                                                    <option value="margin price" {{ ('margin price'== old('value_factor'))? 'selected' : '' }}> Margin price </option>
                                                </select>
                                                <!-- <input value="{!!old('value_factor')!!}" class=" form-control" id="value_factor" name="value_factor" minlength="2" type="text">  -->
                                                @if ($errors->has('value_factor'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>The pring type field is required.</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="value" class="col-sm-3 control-label">Value<span style="color:red;">*</span></label>
                                            <div class="col-sm-9">
                                                <input value="{!!old('value')!!}" type="text" name="value" class="form-control" id="value"
                                                       placeholder="Value">
                                                @if ($errors->has('value'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>The price field is required.</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="status" class="col-sm-3 control-label">Enabled </label>
                                            <div class="col-sm-9">
                                                <div class="ckbox ckbox-info">
                                                    <input id="checkbox-info1" checked="checked" type="checkbox" name="status">
                                                    <label for="checkbox-info1"> </label>
                                                </div>
                                            </div>
                                        </div>

										<div class="col-lg-6">
                                            <label for="country_id" class="col-sm-3 control-label">Country</label>
                                            <div class="col-sm-9">
                                                <select id="country_id" name="country_id" class="form-control">
												<option value="0" {{ (old('country_id') == 0 )? 'selected' : '' }}> All Countries </option>
												@foreach($countries as $country)
												<option value="{{$country->id}}" {{ (old('country_id') == $country->id )? 'selected' : '' }}> {{$country->name}} </option>
												@endforeach
                                                </select>
                                                @if ($errors->has('country_id'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('country_id') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"></div>
                        </div>
                        <div class="form-footer">
                            <div class="col-sm-offset-5">
                                <button class="btn btn-color btn-hover" type="submit">Submit</button>
                                <a class="btn btn-danger" href="{{URL::to('/')}}/products/index">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="siteUrl" value="{{url('/')}}">

@section('js')
<script src="{{ asset('assets/global/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>
<script src="{{ asset('assets/global/plugins/bower_components/jasny-bootstrap-fileinput/js/jasny-bootstrap.fileinput.min.js')}}"></script>
<script src="{{ asset('assets/global/plugins/bower_components/chosen_v1.2.0/chosen.jquery.min.js')}}"></script>
<script src="{{ asset('js/hierarchy-select.min.js') }}"></script>
<script src="{{ asset('js/products/products.js') }}"></script>
@endsection

@endsection
