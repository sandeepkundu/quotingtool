@extends('layouts.default') @section('content')
<!-- START @PAGE CONTENT -->
@if(isset($toast['message']))
{{ $toast['message'] }}
@endif
<style>
    .form-control:focus{
        border: 1px solid #66afe9 !important;
    }
    .bg-warning{
        background-color:#00B1E1 !important;
        border:1px solid #00B1E1 !important;
    }
    input.no-border-right:focus, textarea.no-border-right:focus{
        border: 1px solid #66afe9 !important;
    }

</style>
<div class="body-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">ADD PRODUCT OPTION GROUPS</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body no-padding">

                    <form class="form-horizontal mt-10" id="commentForm" method="post" action="{{URL::to('/')}}/poGroups/insertPog" >
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-12"> 
                                        <div class="col-lg-6">
                                                <label for="group_name" class="col-sm-3 control-label">Group<!-- <span style="color:red;">*</span> --></label>
                                            <div class="col-sm-9">
                                                <input value="{{$group}}" class=" form-control" id="group" name="group" type="text" readonly /> 
                                                @if ($errors->has('group_name'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('group_name') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="group_code" class="col-sm-3 control-label">Group Code<span style="color:red;">*</span></label>
                                            <div class="col-sm-9">
                                                <input value="{{$pog->group_code}}" class=" form-control" id="group_code" name="group_code" minlength="2" type="text"  autocomplete="off" placeholder="Group Code" readonly="" /> 

                                                @if ($errors->has('group_code'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('group_code') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="type" class="col-sm-3 control-label">Type<span style="color:red;">*</span></label>
                                            <div class="col-sm-9">

                                                <input type="text" id="type" class="form-control" value="{{$pog->type}}" name="type" readonly="">

                                                @if ($errors->has('type'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('type') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="option_name" class="col-sm-3 control-label">Option Name <span style="color:red;">*</span> </label>
                                            <div class="col-sm-9">

                                                <select class="form-control" name="option_name" id="option_name">
                                                    @if(isset($productOptions))
                                                    @foreach($productOptions as $productOption)
                                                    <option value="{{$productOption->id}}"  {{ $productOption->id == old('option_name') ? 'selected' : '' }} >{{$productOption->name}}</option>
                                                    @endforeach

                                                    @endif
                                                </select>
                                                @if ($errors->has('option_name'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('option_name') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-sm-12">
                                        <div class="col-lg-6">
                                            <label for="parent_id" class="col-sm-3 control-label">Clients</label>
                                            <div class="col-sm-9">
                                                <input type="hidden" class="form-control" id="parent_id" name="parent_id" value="{{$client_name->id}}">

                                                <input type="text" class="form-control" value="{{$client_name->name}}"  readonly="">		
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="order" class="col-sm-3 control-label">Order</label>
                                            <div class="col-sm-9">
                                                <input  value="" class=" form-control" id="order" name="order" type="text" placeholder="Order" /> 
                                                @if ($errors->has('order'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('order') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="status" class="col-sm-3 control-label">Enabled<!-- <br/>
                                                    <span>(Active/Disabled)</span> -->
                                            </label>
                                            <div class="col-sm-9">
                                                <div class="ckbox ckbox-info">
                                                    <input id="checkbox-info1" checked="checked" type="checkbox" name="status">
                                                    <label for="checkbox-info1"> </label>
                                                </div>
                                            </div>
                                        </div>
                                @if($pog->type=="Processor")
                                <div class="col-lg-6 processor_subclass">
                                   <label for="checkbox-info1" class="col-sm-3 control-label">Processor Sub Group<span style="color:red;">*</span>
                                                    <!-- <span>(Active/Disabled)</span> -->
                                            </label>
                                            <div class="col-sm-9">
                                                <div class="sub_group">
                                                <input value="{{$pog->processor_sub_group}}" class=" form-control" id="processor_sub_groupid" name="processor_sub_group" type="text" placeholder="Sub group">
                                                @if ($errors->has('processor_sub_group'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('processor_sub_group') }}</strong>
                                                </span>
                                                @endif
                                                </div>
                                            </div>
                                        </div>
                                @endif
                                    </div>
                                </div>
                                <div class="form-group"></div>
                            </div>
                            <div class="form-footer">
                                <div class="col-sm-offset-5">
                                    <button class="btn btn-color btn-hover" type="submit">Submit</button>
                                    <a class="btn btn-danger" href="{{URL::to('/')}}/poGroups/detailsPog/{{$group}}">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!--==============================Modal Popup=======================-->
<div class="modal modal-success fade in" id="modal-bootstrap-tour" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-dialog" role="document" style="margin: 150px auto;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title"><span class="username">Alert!!!</span></h4>
            </div>
            <div class="modal-body">
                You can not make the parent of this store because this is already child of another store.

            </div>
        </div>
    </div>
</div>
<!--==============================Modal Popup=======================--> 
<style type="text/css">
    #ave_collect_size{
        border-right:1px solid rgb(221, 221, 221) !important;
    }
    #ave_collect_size:focus{
        border-right:1px solid #66afe9 !important
    }
    #example-one{
        width: 100%;
    }
    .dropdown-menu li.active:hover a, .dropdown-menu li.active:focus a, .dropdown-menu li.active:active a{
        background-color:#00B1E1 !important;
    }
    .dropdown-menu li.active a{
        background-color:#00B1E1 !important;
    }
    .btn-default.dropdown-toggle.btn-default{
        background-color: white;
    }
    .btn-default.active.focus, .btn-default.active:focus, .btn-default.active:hover, .btn-default:active.focus, .btn-default:active:focus, .btn-default:active:hover, .open>.dropdown-toggle.btn-default.focus, .open>.dropdown-toggle.btn-default:focus, .open>.dropdown-toggle.btn-default:hover{
        background-color: white;
    }
    .dropdown-menu li > a:hover:before{
        border-left:3px solid #00B1E1;
    }
    .modal-success .modal-header {
        background-color: #00B1E1 !important;
        border:1px solid #00B1E1;
        border:none;
    }
    .modal-success .modal-header:before {
        content: ""; 
        border: 1px solid #00B1E1 !important; 
    }
</style>		
<link href="{{asset('css/hierarchy-select.min.css')}}" rel="stylesheet">
<!-- END @PAGE CONTENT -->
@section('js')
<script src="{{ asset('js/hierarchy-select.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function () {

    $('#client_type').on('change', function () {
        var type = this.value;
        if (type == 'enterprise') {
            $('.store').css('display', 'none');
            $('.enterprise').css('display', 'block');
        }
        if (type == 'store') {
            $('.enterprise').css('display', 'none');
            $('.store').css('display', 'block');
        }
        if (type == '') {
            $('.enterprise').css('display', 'none');
            $('.store').css('display', 'block');
        }
    });
    $('#example-one').hierarchySelect();
    $('.par').click(function () {
        var a = $(this).attr('data-level');
        if (a == 3) {
            setTimeout(function () {

                $('#modal-bootstrap-tour').modal('show');
                $('.selected-label').text('Select Client');
                $('#parent_id').val('0');

                $("li").removeClass("active");
                $('.aa').addClass("active");

            }, 200);

        }
    });
    /*$('#type').on('change',function(){
     var type=$('#type').val();
     var token = "{{csrf_token()}}";
     $.ajax({
     type:"POST",
     url:SITE_URL+"/poGroups/changetype",
     data:{type:type,_token:token},
     success:function(data){
     var option='';
     for(var d in  data ) {
     obj=data[d];
     
     option += '<option value="' + obj.id + '"> ' + obj.name + ' </option>';
     }
     $('#option_name').html(option);
     },
     error:function(error){
     alert('err');
     }
     });
     
     })*/;
});

</script>
@endsection
@endsection