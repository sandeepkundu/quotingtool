@extends('layouts.default') @section('content')
<!-- START @PAGE CONTENT -->
@if(isset($toast['message']))
{{ $toast['message'] }}
@endif
<div class="body-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">ADD NEW GROUPS</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body no-padding">

                    <form class="form-horizontal mt-10" id="commentForm" method="post" action="{{URL::to('/')}}/poGroups/insertNewGroups" >
                        {{ csrf_field() }}

                        <div class="form-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-12"> 
                                        <div class="col-lg-6">
                                            <label for="group" class="col-sm-3 control-label">Group Name<span style="color:red;">*</span></label>
                                            <div class="col-sm-9">
                                                <input value="{!!old('group')!!}" class=" form-control" id="group_code" name="group" minlength="2" type="text"  autocomplete="off" placeholder="Group Name" /> 

                                                @if ($errors->has('group'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('group') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="group_code" class="col-sm-3 control-label">Group Code<span style="color:red;">*</span></label>
                                            <div class="col-sm-9">
                                                <input value="{!!old('group_code')!!}" class=" form-control" id="group_code" name="group_code" minlength="2" type="text" autocomplete="off" placeholder="Group Code" /> 
                                                @if ($errors->has('group_code'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('group_code') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-lg-12"> 
                                        <div class="col-lg-6">
                                            <label for="type" class="col-sm-3 control-label">Product Type<span style="color:red;">*</span></label>
                                            <div class="col-sm-9">
                                                <select class="form-control" name="type" id="type">
                                                    <option value="">-- Select Product Type -- </option>
                                                    @foreach($types as $type)
                                                    <option value="{{$type->type}}" {{ $type->type == old('type') ? 'selected' : '' }}>{{$type->type}}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('type'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('type') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="option_name" class="col-sm-3 control-label">Option Name<span style="color:red;">*</span></label>
                                            <div class="col-sm-9">
                                                <select class="form-control" name="option_name" id="option_name">
                                                    <option value="">-- Select Option Name -- </option>


                                                </select>
                                                @if ($errors->has('option_name'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('option_name') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>										
                                    </div>

                                    <div class="col-lg-12">

                                        <div class="col-lg-6">
                                            <label for="order" class="col-sm-3 control-label">Order</label>
                                            <div class="col-sm-9">
                                                <input  value="" class=" form-control" id="order" name="order"  type="text" placeholder="order" /> 
                                                @if ($errors->has('order'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('order') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="parent_id" class="col-sm-3 control-label">Clients</label>
                                            <div class="col-sm-9">
                                                <div class="btn-group hierarchy-select" data-resize="auto" id="example-one">
                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <span class="selected-label pull-left">&nbsp;</span>
                                                        <span class="caret"></span>
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                    </button>
                                                    <div class="dropdown-menu open">
                                                        <div class="hs-searchbox">
                                                            <input type="text" class="form-control" autocomplete="off">
                                                        </div>
                                                        <ul class="dropdown-menu inner store" role="menu">
                                                            <li data-value="0" data-level="1" class="aa" data-default-selected="">
                                                                <a href="#">Select Client</a>
                                                            </li>
                                                            @foreach($parent_ids as $val)
                                                            <li class="par" data-value="{{$val['id']}}" data-level="{{$val['level']}}">
                                                                <a href="#">{{$val['name']}}</a>
                                                            </li>
                                                            @endforeach
                                                        </ul>

                                                        <ul class="dropdown-menu inner enterprise" style="display: none;" role="menu" >
                                                            <li data-value="0" data-level="1" class="aa">
                                                                <a href="#">Select Client</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <input class="hidden hidden-field" name="parent_id" id="parent_id" aria-hidden="true" type="text"/>
                                                </div>			
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="checkbox-info1" class="col-sm-3 control-label">Enabled
                                                    <!-- <span>(Active/Disabled)</span> -->
                                            </label>
                                            <div class="col-sm-9">
                                                <div class="ckbox ckbox-info">
                                                    <input id="checkbox-info1" checked="checked" type="checkbox" name="status">
                                                    <label for="checkbox-info1"> </label>
                                                </div>
                                            </div>
                                        </div>
                                        @if (old('type'))
                                        <div class="col-lg-6 processor_subclass" style="display: block">
                                        @else
                                        <div class="col-lg-6 processor_subclass" style="display: none">
                                        @endif
                                        <label for="checkbox-info1" class="col-sm-3 control-label">Processor Sub Group<span style="color:red;">*</span>
                                                    <!-- <span>(Active/Disabled)</span> -->
                                            </label>
                                            <div class="col-sm-9">
                                                <div class="sub_group">
                                                <input value="" class=" form-control" id="processor_sub_groupid" name="processor_sub_group" type="text" placeholder="Sub group">
                                                @if ($errors->has('processor_sub_group'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('processor_sub_group') }}</strong>
                                                </span>
                                                @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"></div>
                        </div>
                        <div class="form-footer">
                            <div class="col-sm-offset-5">
                                <button class="btn btn-color btn-hover" type="submit">Submit</button>
                                <a class="btn btn-danger" href="{{URL::to('/')}}/poGroups/index">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!--==============================Modal Popup=======================-->
<!--<div class="modal modal-success fade in" id="modal-bootstrap-tour" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-dialog" role="document" style="margin: 150px auto;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title"><span class="username">Warning!!!</span></h4>
            </div>
            <div class="modal-body">
                You can not make the parent of this store because this is already child of another store.

            </div>
        </div>
    </div>
</div>-->
<!--==============================Modal Popup=======================--> 
<style type="text/css">
    #ave_collect_size{
        border-right:1px solid rgb(221, 221, 221) !important;
    }
    #ave_collect_size:focus{
        border-right:1px solid #66afe9 !important
    }
    #example-one{
        width: 100%;
    }
    .dropdown-menu li.active:hover a, .dropdown-menu li.active:focus a, .dropdown-menu li.active:active a{
        background-color:#00B1E1 !important;
    }
    .dropdown-menu li.active a{
        background-color:#00B1E1 !important;
    }
    .btn-default.dropdown-toggle.btn-default{
        background-color: white;
    }
    .btn-default.active.focus, .btn-default.active:focus, .btn-default.active:hover, .btn-default:active.focus, .btn-default:active:focus, .btn-default:active:hover, .open>.dropdown-toggle.btn-default.focus, .open>.dropdown-toggle.btn-default:focus, .open>.dropdown-toggle.btn-default:hover{
        background-color: white;
    }
    .dropdown-menu li > a:hover:before{
        border-left:3px solid #00B1E1;
    }
    .modal-success .modal-header {
        background-color: #00B1E1 !important;
        border:1px solid #00B1E1;
        border:none;
    }
    .modal-success .modal-header:before {
        content: ""; 
        border: 1px solid #00B1E1 !important; 

    }
</style>


<link href="{{asset('css/hierarchy-select.min.css')}}" rel="stylesheet">

<!-- END @PAGE CONTENT -->
@section('js')
<script src="{{ asset('js/hierarchy-select.min.js') }}"></script>
<script type="text/javascript">

function multiId() {
    var checkboxcount = jQuery('.id:checked').length;
    if (checkboxcount != 0) {
        if (confirm("Are you sure you want to delete ?")) {
            jQuery('.multipleDeleteForm').submit();
        }
    } else {
        alert("Please select a record");
        return false;
    }
}
$('#type').on('change', function () {
    var type = $(this).val();
    if(type=="Processor"){
        $('.processor_subclass').css('display','block');
    }else{
      $('.processor_subclass').css('display','none');  
    }
    var token = "{{csrf_token()}}";
    $.ajax({
        type: "POST",
        url: SITE_URL + "/poGroups/changetype",
        data: {type: type, _token: token},
        success: function (data) {
            var option = '';
            for (var d in  data) {
                obj = data[d];

                option += '<option value="' + obj.id + '"> ' + obj.name + ' </option>';
            }
            $('#option_name').html(option);
        },
        error: function (error) {
            alert('err');
        }
    });

});
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#example-one').hierarchySelect();
//        $('#client_type').on('change', function () {
//            var type = this.value;
//            if (type == 'enterprise') {
//                $('.store').css('display', 'none');
//                $('.enterprise').css('display', 'block');
//            }
//            if (type == 'store') {
//                $('.enterprise').css('display', 'none');
//                $('.store').css('display', 'block');
//            }
//            if (type == '') {
//                $('.enterprise').css('display', 'none');
//                $('.store').css('display', 'block');
//            }
//        });
//        
//        $('.par').click(function () {
//            var a = $(this).attr('data-level');
//            if (a == 3) {
//                setTimeout(function () {
//
//                    $('#modal-bootstrap-tour').modal('show');
//                    $('.selected-label').text('Select Client');
//                    $('#parent_id').val('0');
//
//                    $("li").removeClass("active");
//                    $('.aa').addClass("active");
//
//                }, 200);
//
//            }
//        });
    });
</script>

@endsection
@endsection