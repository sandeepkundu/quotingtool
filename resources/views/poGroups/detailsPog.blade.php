@extends('layouts.default') @section('content')
<!-- START @PAGE CONTENT -->
@if(isset($toast['message']))
{{ $toast['message'] }}
@endif
@section('css')
<!-- START @PAGE LEVEL STYLES -->
<link href="{{asset('assets/global/plugins/bower_components/datatables/css/dataTables.bootstrap.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/bower_components/datatables/css/datatables.responsive.css')}}" rel="stylesheet">
<!--/ END PAGE LEVEL STYLES -->

@endsection
<style>
    .table-success tbody tr td .sorting_1 {
        background: red !important;
        color: white;
        border-bottom: 1px solid red !important;
    }
    .dataTable thead tr th:first-child {
        min-width: 10px !important;
    }
    .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
        background-color: #178aa9 !important;
        border: 1px solid #178aa9;
    }
    table.dataTable thead th {
        position: relative;
        background-image: none !important;
    }

    table.dataTable thead th.sorting:after,
    table.dataTable thead th.sorting_asc:after,
    table.dataTable thead th.sorting_desc:after {
        position: absolute;
        top: 12px;
        right: 8px;
        display: block;
        font-family: FontAwesome;
    }
    table.dataTable thead th.sorting:after {
        content: "\f0dc";
        color: #ddd;
        font-size: 0.8em;
        padding-top: 0.12em;
    }
    table.dataTable thead th.sorting_asc:after {
        content: "\f0de";
    }
    table.dataTable thead th.sorting_desc:after {
        content: "\f0dd";
    }

</style>
<!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-table"></i>{{$group}}<span></span></h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label"> </span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="{{URL::to('/')}}/admin/dashboard">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>

        </ol>
    </div><!-- /.header-content -->
</div><!--/ End page header -->
<!-- Start body content -->
<div class="body-content animated fadeIn">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-tab panel-tab-double shadow">
                <div class="panel-body no-padding">
                    <div class="panel panel-default shadow no-margin">
                        <div class="panel-heading">
                            <a href="{{URL::to('/')}}/poGroups/index" style="margin-left:5px;margin-right: -4px;" class="btn btn-color btn-hover"><i class="glyphicon glyphicon-chevron-left"></i>&nbsp;Back</a>
                            <a  href="{{URL::to('/')}}/poGroups/detailsPog/{{$group}}" class="btn btn-default tooltips" data-toggle="tooltip" data-placement="top" data-title="Reload" data-action="refresh" data-original-title="" title="" id="ml5"><i class="icon-refresh icons"></i></a>
                            <a href="javascript:void(0);" class="btn btn-danger tooltips" data-toggle="tooltip" data-placement="top" data-title="Delete" data-original-title="" title="" onclick="return multiId()" ><i class="icon-trash icons"></i></a>
                            <div class="pull-right" id="mt0">
                                <a href="{{URL::to('/')}}/poGroups/addPog/{{$group}}" class="btn btn-color btn-hover/"><i class="fa fa-plus"></i> &nbsp; Add new product option group</a>
                            </div>
                            <div class="clearfix"></div>
                        </div><!--/.panel-heading -->
                        <!--Panel Body-->
                        <div class="panel-body">
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="tab-client-all">
                                    <!-- Start datatable -->
                                    <div id="datatable-client-all_wrapper" class="dataTables_wrapper form-inline">
                                        <form action="{{URL::to('/') }}/poGroups/multipleDelete" method="post" class="form multipleDeleteForm">
                                            {{csrf_field()}}
                                            <input type="hidden" name='group' value="{{$group}}">
                                            <table id="datatable-ajax" class="table table-striped table-success">
                                                <thead>
                                                    <tr role="row">
                                                        <th style="width:10px !important;text-align:center;"><input type="checkbox" class="multiIdCheck"/></th>
                                                        <th>Client</th>
                                                        <th class="expand sorting_asc" tabindex="0">Product type</th>
                                                        <th class="expand sorting_asc" tabindex="0">Name</th>
                                                        <th class="expand sorting_asc" tabindex="0">Group Code</th>
                                                        <th class="expand sorting_asc" tabindex="0">Processor Sub Group</th>
                                                        <th  class="sorting" tabindex="0">Order</th>
                                                        <th  data-hide="phone,tablet" class=" sorting_disabled">Enabled</th>
                                                        <th class="" style="width: 108px !important;">Actions</th>
                                                    </tr>
                                                </thead>
                                                <!--tbody section is required-->
                                                <tbody id="page-list">

                                                <input type="hidden" value="{{$group}}" name="group">

                                                <?php
                                                if ($pog->count() > 0) {
                                                    foreach ($pog as $pogs) {
												
                                                        ?>
                                                        <tr>
                                                            <td><input type="checkbox" class="id" name="ids[]" value="{{$pogs->id}}"/></td>
                                                            <td>
                                                                @if(!empty($pogs->fullpath))
                                                                {{$pogs->fullpath}}
                                                                @else
                                                                Default
                                                                @endif	
                                                            </td>
                                                            <td>
                                                                @if(!empty($pogs->type))
                                                                {{$pogs->type}}
                                                                @else
                                                                N/A
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if(!empty($pogs->name))
                                                                {{$pogs->name}}
                                                                @else
                                                                N/A
                                                                @endif
                                                            </td>
                                                            <td>{{$pogs->group_code}}</td>
                                                            <td>{{isset($pogs->processor_sub_group) ? $pogs->processor_sub_group : '--'}}</td>
                                                            <td>{{$pogs->order}}</td>

                                                            <?php
                                                            if ($pogs->status == 'active') {
                                                                echo "<td style='color:green'><span>&#10003;</span></td>";
                                                            } else {
                                                                echo "<td style='color:red;'><span>&#9747;</span></td>";
                                                            }
                                                            ?>
                                                        <span> <input type="hidden" class="hidden_status" value="{{$pogs->id}}"/></span>
                                                        <td>
                                                            <a class="btn btn-round btn-color btn-hover pull-left" href="{{URL::to('/')}}/poGroups/editPog/{{$pogs->id}}" style="padding:0px 5px;">
                                                                <i class="fa fa-edit"></i>
                                                            </a>
                                                            <a onclick="return confirm('Are you sure you want to delete?')" class="btn btn-round btn-danger"  style="padding: 0px 6px;" href="{{URL::to('/')}}/poGroups/Delete/{{$pogs->id}}">
                                                                <i class="fa fa-trash-o"></i>
                                                            </a>
                                                        </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                } else {
                                                    ?>
                                                    <tr><td colspan="5" style="color:red;">No records found!!!</td></tr>
                                                <?php } ?>

                                                </tbody>
                                                <!--tfoot section is optional-->
                                                <tfoot>
                                                    <tr role="row">
                                                        <th style="width:10px !important;text-align:center;"><input type="checkbox" class="multiIdCheck"/></th>
                                                        <th class="expand sorting_asc" tabindex="0">Client</th>
    <!--                                                    <th>Full Path </th>-->
                                                        <th class="expand sorting_asc" tabindex="0">Product type</th>
                                                        <th class="expand sorting_asc" tabindex="0">Name</th>
                                                        <th class="expand sorting_asc" tabindex="0">Group Code</th>
                                                        <th class="expand sorting_asc" tabindex="0">Processor Sub Group</th>
                                                        <th data-hide="phone,tablet" class="sorting">Order</th>
                                                        <th data-hide="phone,tablet" class=" sorting_disabled">Enabled</th>
                                                        <th class="" style="width: 108px !important;">Actions</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </form>
                                    </div>
                                    <!--/ End datatable -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<form action="{{URL::to('/')}}/poGroups/detailsPog/{{$group}}" method="get" class="navbar-form" id="formPerPage">
    <input type="hidden" value="" name="perPage" id="perPage">
</form>
<style>
    .pagination{
        float: right;
        margin:0px;
    }
    #tour-3{
        margin-bottom: 0px;
    }
    .padding_bottom{
        padding-bottom: 15px;
    }
</style>
@section('js')
<script>
    var a = $('.chk').val();
    if (a == 'Inactive') {
        $('.chk').removeAttr('checked', 'checked');
    }
</script>

<script>
    $(document).ready(function () {
        $('#datatable-ajax').DataTable({
            'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [-1, -8] /* 1st one, start by the right */
                }],
            language: {
                searchPlaceholder: "Search records"
            },
            "lengthMenu": [[30, 60, 90, -1], [30, 60, 90, "All"]],
            "order": [[1, "asc"]],
            "columnDefs": [{
                    "targets": 0,
                    "orderable": false,
                    'min-width': '10px',
                    "width": "5%"
                }],
            "bSortClasses": false,
            "searchHighlight": true,
        });

    });

    function multiId() {
        var checkboxcount = jQuery('.id:checked').length;
        if (checkboxcount != 0) {
            if (confirm("Are you sure you want to delete ?")) {
                jQuery('.multipleDeleteForm').submit();
            }
        } else {
            alert("Please select a record")
            return false;
        }
    }

</script>
<!-- START @PAGE LEVEL PLUGINS -->
<script src="{{asset('assets/global/plugins/bower_components/datatables/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/global/plugins/bower_components/datatables/js/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('assets/global/plugins/bower_components/datatables/js/datatables.responsive.js')}}"></script>
<!--/ END PAGE LEVEL PLUGINS -->
@endsection
@endsection