@extends('layouts.default')
@section('content')
<style>
    .form-control:focus{
        border: 1px solid #66afe9 !important;
    }
    .bg-warning{
        background-color:#00B1E1 !important;
        border:1px solid #00B1E1 !important;
    }
    input.no-border-right:focus, textarea.no-border-right:focus{
        border: 1px solid #66afe9 !important;
    }
</style>
<div class="body-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">ADD PRICE FACTORS</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-body no-padding">
                    <form class="form-horizontal mt-10" id="commentForm" method="post" action="{{URL::to('/')}}/PriceFactors/insertPriceFactors" >
                        {{ csrf_field() }}
                        <input type="hidden" name="cat" value="<?php if(key($_GET) == 'HDD') {echo key($_GET); } else { echo 'RAM'; }?>">
                        <div class="form-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="parent_id" class="col-sm-3 control-label">Clients</label>
                                            <div class="col-sm-9">
                                                <div class="btn-group hierarchy-select" data-resize="auto" id="example-one">
                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <span class="selected-label pull-left">&nbsp;</span>
                                                        <span class="caret"></span>
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                    </button>
                                                    <div class="dropdown-menu open">
                                                        <div class="hs-searchbox">
                                                            <input type="text" class="form-control" autocomplete="off">
                                                        </div>
                                                        <ul class="dropdown-menu inner store" role="menu">
                                                            <li data-value="0" data-level="1" class="aa" data-default-selected="">
                                                        <a href="#">Select Client</a>
                                                            </li>
                                                            @foreach($parent_ids as $val)
                                                            <li class="par" data-value="{{$val['id']}}" data-level="{{$val['level']}}">
                                                                <a href="#">{{$val['name']}}</a>
                                                            </li>

                                                            @endforeach
                                                        </ul>

                                                        <ul class="dropdown-menu inner enterprise" style="display: none;" role="menu" >
                                                            <li data-value="0" data-level="1" class="aa">
                                                                <a href="#">Select Client</a>
                                                            </li>
                                                        </ul>


                                                    </div>
                                                    <input class="hidden hidden-field" name="parent_id" id="parent_id" aria-hidden="true" type="text"/>
                                                </div>			
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="country_id" class="col-sm-3 control-label">Factor Name<span style="color:red;">*</span></label>
                                            <div class="col-sm-9">

                                                <input value="<?php if(key($_GET) == 'HDD') {echo key($_GET); } else { echo 'RAM'; }?>" readonly list='type' type="text" name="factor_name" class="form-control select" id="name" placeholder="Factor name">

                                                @if ($errors->has('factor_name'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('factor_name') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="variance_from" class="col-sm-3 control-label">Variance From<span style="color:red;">*</span></label>
                                            <div class="col-sm-9">
                                                <input type="text" value="{{old('variance_from')}}" name="variance_from" class="form-control" id="variance_from" placeholder="Variance From">

                                                @if ($errors->has('variance_from'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('variance_from') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="variance_to" class="col-sm-3 control-label">Varience To<span style="color:red;">*</span></label>
                                            <div class="col-sm-9" style="padding-bottom:20px;">
                                                <input type="text" value="{{old('variance_to')}}" name="variance_to" class="form-control" id="variance_to" placeholder="Variance To">
                                                @if ($errors->has('variance_to'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('variance_to') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="value" class="col-sm-3 control-label">Value <span style="color:red;">*</span></label>
                                            <div class="col-sm-9" style="padding-bottom:20px;">
                                                <div class="input-group">
                                                    <span class="input-group-addon">$</span>
                                                    <input type="text" value="{{old('value')}}" name="value" class="form-control" id="value" placeholder="Value">
                                                    <span class="input-group-addon bg-warning">.00</span>
                                                </div>
                                                @if ($errors->has('value'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('value') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>

										<div class="col-lg-6">
                                            <label for="option_id" class="col-sm-3 control-label">Product Type</label>
                                            <div class="col-sm-9">
                                                <select name="type_id" class="form-control">
                                                    <option value="">--Select Option--</option>
                                                    @foreach ($types as $type)                                                   
                                                    <option value="{{ $type->id }}" {{(old('type_id')== $type->id) ? "selected":"" }} >{{ $type->name }}</option>                                                   
                                                    @endforeach  
                                                </select>
                                                @if ($errors->has('type_id'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>Error occurred with the selected value.</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                            <div class="form-group"></div>
                        </div>
                        <div class="form-footer">
                            <div class="col-sm-offset-5">
                                <button class="btn btn-color btn-hover" type="submit">Submit</button>
                                <a class="btn btn-danger" href="{{URL::to('/')}}/PriceFactors/index?<?php echo key($_GET); ?>">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!--==============================Modal Popup=======================-->
<!--<div class="modal modal-success fade in" id="modal-bootstrap-tour" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-dialog" role="document" style="margin: 150px auto;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title"><span class="username">Alert!!!</span></h4>
            </div>
            <div class="modal-body">
                You can not make the parent of this store because this is already child of another store.

            </div>
        </div>
    </div>
</div>-->
<!--==============================Modal Popup=======================--> 
<style type="text/css">
    #ave_collect_size{
        border-right:1px solid rgb(221, 221, 221) !important;
    }
    #ave_collect_size:focus{
        border-right:1px solid #66afe9 !important
    }
    #example-one{
        width: 100%;
    }
    .dropdown-menu li.active:hover a, .dropdown-menu li.active:focus a, .dropdown-menu li.active:active a{
        background-color:#00B1E1 !important;
    }
    .dropdown-menu li.active a{
        background-color:#00B1E1 !important;
    }
    .btn-default.dropdown-toggle.btn-default{
        background-color: white;
    }
    .btn-default.active.focus, .btn-default.active:focus, .btn-default.active:hover, .btn-default:active.focus, .btn-default:active:focus, .btn-default:active:hover, .open>.dropdown-toggle.btn-default.focus, .open>.dropdown-toggle.btn-default:focus, .open>.dropdown-toggle.btn-default:hover{
        background-color: white;
    }
    .dropdown-menu li > a:hover:before{
        border-left:3px solid #00B1E1;
    }
    .modal-success .modal-header {
        background-color: #00B1E1 !important;
        border:1px solid #00B1E1;
        border:none;
    }
    .modal-success .modal-header:before {
        content: ""; 
        border: 1px solid #00B1E1 !important; 
    }
</style>		
<link href="{{asset('css/hierarchy-select.min.css')}}" rel="stylesheet">
<!-- END @PAGE CONTENT -->
@section('js')
<script src="{{ asset('js/hierarchy-select.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function () {
        $('#example-one').hierarchySelect();
//    $('#client_type').on('change', function () {
//        var type = this.value;
//        if (type == 'enterprise') {
//            $('.store').css('display', 'none');
//            $('.enterprise').css('display', 'block');
//        }
//        if (type == 'store') {
//            $('.enterprise').css('display', 'none');
//            $('.store').css('display', 'block');
//        }
//        if (type == '') {
//            $('.enterprise').css('display', 'none');
//            $('.store').css('display', 'block');
//        }
//    });
//
//    $('.par').click(function () {
//        var a = $(this).attr('data-level');
//        if (a == 3) {
//            setTimeout(function () {
//                $('#modal-bootstrap-tour').modal('show');
//                $('.selected-label').text('Select Client');
//                $('#parent_id').val('0');
//
//                $("li").removeClass("active");
//                $('.aa').addClass("active");
//            }, 200);
//
//        }
//    });
});
</script>

@endsection
@endsection