@extends('layouts.default') @section('content')


<style>
	.form-control:focus{
		border: 1px solid #66afe9 !important;
	}
	.bg-warning{
		background-color:#00B1E1 !important;
		border:1px solid #00B1E1 !important;
	}
	input.no-border-right:focus, textarea.no-border-right:focus{
		border: 1px solid #66afe9 !important;
	}
</style>
<div class="body-content animated fadeIn">
<div class="row">
	<div class="col-lg-12">
		<div class="panel rounded shadow">
			<div class="panel-heading">
				<div class="pull-left">
					<h3 class="panel-title">ADD NEW PRICE BANDS</h3>
				</div>

				<div class="clearfix"></div>
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body no-padding">

				<form class="form-horizontal mt-10" id="commentForm" method="post" action="{{URL::to('/')}}/priceBands/insertPriceBands" >
					{{ csrf_field() }}
					<div class="form-body">
						<div class="form-group">
							<div class="row">
								<div class="col-lg-12">
									<div class="col-lg-6">
										<label for="value_from" class="col-sm-3 control-label"> Value From<span style="color:red;">*</span></label>
										<div class="col-sm-9">
											<input value="{!!old('value_from')!!}" class=" form-control" id="value_from" name="value_from" minlength="2" type="text" placeholder=" Value From"> 
											@if ($errors->has('value_from'))
											<span class="help-block" style="color:red;">
												<strong>{{ $errors->first('value_from') }}</strong>
											</span>
											@endif
											&nbsp
										</div>
									</div>

									<div class="col-lg-6">
										<label for="value_to" class="col-sm-3 control-label">Value To<span style="color:red;">*</span></label>
										<div class="col-sm-9">
											<input value="{!!old('value_to')!!}" class=" form-control" id="value_to" name="value_to" minlength="2" type="text" placeholder="Value To"> 
											@if ($errors->has('value_to'))
											<span class="help-block" style="color:red;">
												<strong>{{ $errors->first('value_to') }}</strong>
											</span>
											@endif
											&nbsp
										</div>
									</div>

									<div class="col-lg-6">
										<label for="status" class="col-sm-3 control-label">Enabled</label>

										<div class="col-sm-9">
											<div class="ckbox ckbox-info">
												<input id="checkbox-info1" checked="checked" type="checkbox" name="status">
												<label for="checkbox-info1"> </label>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
					<!-- /.form-group -->
					<div class="form-group"></div>
					<!-- /.form-group -->
				</div>
				<!-- /.form-body -->
				<div class="form-footer">
					<div class="col-sm-offset-5">
						<button class="btn btn-color btn-hover" type="submit">Submit</button>
						<a class="btn btn-danger" href="{{URL::to('/')}}/priceBands/index">Cancel</a>
					</div>
				</div>
			</form>

		</div>
	</div>
	<!-- /.panel -->
	<!--/ End horizontal form -->
</div>
</div>
</div>
@section('js')
	<!-- <script type="text/javascript">
		$(document).ready(function(){
			$('#value_from').keyup(function(){
				//alert('test');
				var value_from=$(this).val();
				$.ajax({
						url: SITE_URL+'/productOptions/updateProductOption/'+hidden_order,
						type: "get",
						data: {order:order},
						success: function(response){
							toastr.success('Record has been updated.','Success');
						},
						error:function(error){
							alert('error block');
						}
					});			
			});
		});
	</script> -->
@endsection
@endsection