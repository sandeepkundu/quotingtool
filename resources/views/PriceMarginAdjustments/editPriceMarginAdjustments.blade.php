@extends('layouts.default')
@section('content')
<style>
    .form-control:focus{
        border: 1px solid #66afe9 !important;
    }
    .bg-warning{
        background-color:#00B1E1 !important;
        border:1px solid #00B1E1 !important;
    }
    input.no-border-right:focus, textarea.no-border-right:focus{
        border: 1px solid #66afe9 !important;
    }
</style>
<div class="body-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">EDIT PRICE MARGIN ADJUSTMENT</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body no-padding">
                    <form class="form-horizontal mt-10" id="commentForm" method="post" action="{{URL::to('/')}}/PriceMarginAdjustments/updatePriceMarginAdjustments/{{$PriceMarginAdjustments->id}}">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="parent_id" class="col-sm-3 control-label">Clients</label>
                                            <div class="col-sm-9">
                                                <div class="btn-group hierarchy-select" data-resize="auto" id="example-one">
                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <span class="selected-label pull-left">&nbsp;</span>
                                                        <span class="caret"></span>
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                    </button>
                                                    <div class="dropdown-menu open">
                                                        <div class="hs-searchbox">
                                                            <input type="text" class="form-control" autocomplete="off">
                                                        </div>
                                                        <ul class="dropdown-menu inner store" role="menu">
                                                            <li data-value="0" data-level="1" class="aa">
                                                                <a href="#">Select Client</a>
                                                            </li>
                                                            @foreach($parent_ids as $val)
                                                           
                                                             <li class="par" data-value="{{$val['id']}}" data-level="{{$val['level']}}" {{ ($val['id']==$PriceMarginAdjustments->client_id)? 'data-default-selected=""' : '' }} >
                                                                <a href="#">{{$val['name']}}</a>
                                                            </li>
                                                            
          
                                                            @endforeach
                                                        </ul>

                                                        <ul class="dropdown-menu inner enterprise" style="display: none;" role="menu" >
                                                            <li data-value="0" data-level="1" class="aa">
                                                                <a href="#">Select Client</a>
                                                            </li>
                                                        </ul>

                                                    </div>
                                                    <input class="hidden hidden-field" name="parent_id" readonly="readonly" value="0" aria-hidden="true" type="text"/>
                                                </div>			
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="country_id" class="col-sm-3 control-label">Country<span style="color:red;">*</span></label>
                                            <div class="col-sm-9">
                                                <select id="country_id" name="country_id" class="form-control">
                                                    <option value=""> -- Select Country -- </option>
                                                    @foreach ($country as $name)
                                                    <option value="{{$name->id
                                                            }}" {{ ($name->id==$PriceMarginAdjustments->country_id) ? 'selected' : ''}}>{{ $name->name }}</option>
                                                    @endforeach             
                                                </select>
                                                @if ($errors->has('country_id'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('country_id') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                    </div>



                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="type_id" class="col-sm-3 control-label">Product Type</label>
                                            <div class="col-sm-9" style="padding-bottom:20px;">
                                                <select  id="type_id" name="type_id" class="form-control">
                                                    <option value=""> --Product Type -- </option>
                                                    @foreach ($product_options as $code)
                                                    @if($code->type=="Product"){
                                                    <option value="{{ $code->id }}" {{ ($code->id==$PriceMarginAdjustments->type_id) ? 'selected' : ''}}>{{ $code->name }}</option>
                                                    @endif
                                                    @endforeach             
                                                </select>


                                                &nbsp
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="region_id" class="col-sm-3 control-label">Brand</label>
                                            <div class="col-sm-9">
                                                <select id="brand_id" name="brand_id" class="form-control">
                                                    <option value=""> -- Select Brand -- </option>
                                                    @foreach ($product_options as $code)
                                                    @if($code->type=="Brand"){
                                                    <option value="{{ $code->id }}" {{ ($code->id==$PriceMarginAdjustments->brand_id) ? 'selected' : ''}}>{{ $code->name }}</option>
                                                    @endif
                                                    @endforeach              
                                                </select>

                                                &nbsp
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="country_id" class="col-sm-3 control-label">Screen Size</label>
                                            <div class="col-sm-9" style="padding-bottom:20px;">
                                                <select id="screen_id" name="screen_id" class="form-control">
                                                    <option value=""> -- Select Screen -- </option>
                                                    @foreach ($product_options as $code)
                                                    @if($code->type=="Screen Size"){
                                                    <option value="{{ $code->id }}" {{ ($code->id==$PriceMarginAdjustments->screen_id) ? 'selected' : ''}}>{{ $code->name }}</option>
                                                    @endif
                                                    @endforeach            
                                                </select>

                                                &nbsp
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="region_id" class="col-sm-3 control-label">Margin Type<span style="color:red;">*</span></label>
                                            <div class="col-sm-9">
                                                <!--<input type="text" value="{{$PriceMarginAdjustments->margin_type}}" class="form-control" name="margin_type" placeholder="Margin Type">-->
                                                <select id="margin_type" name="margin_type" class="form-control">
                                                    <option value=""> -- Select Margin Type -- </option>

                                                    <option value="client" {{$PriceMarginAdjustments->margin_type == 'client' ? "selected":"" }}>Client</option>
                                                    <option value="country" {{($PriceMarginAdjustments->margin_type == 'country') ? "selected":"" }}>Country</option>
                                                    <option value="brand" {{($PriceMarginAdjustments->margin_type == 'brand') ? "selected":"" }}>Brand</option>
                                                    <option value="screen" {{($PriceMarginAdjustments->margin_type == 'screen') ? "selected":"" }}>Screen</option>
                                                    <option value="defectcap" {{($PriceMarginAdjustments->margin_type == 'defectcap') ? "selected":"" }}>Defectcap</option>
                                                    <option value="service" {{($PriceMarginAdjustments->margin_type == 'service') ? "selected":"" }}>Service</option>
                                                    <option value="logistics" {{($PriceMarginAdjustments->margin_type == 'logistics') ? "selected":"" }}>Logistics</option>
                                                    <option value="logistic product" {{($PriceMarginAdjustments->margin_type == 'logistic product') ? "selected":"" }}>Logistic Product</option>

                                                </select>
                                                
                                                @if ($errors->has('margin_type'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('margin_type') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <label for="ave_collect_size" class="col-sm-3 control-label">Value<span style="color:red;">*</span></label>
                                            <div class="col-sm-9">
                                                <input value="{{$PriceMarginAdjustments->value}}" type="text" name="value" class="form-control" id="value">
                                                @if ($errors->has('value'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('value') }}</strong>
                                                </span>
                                                @endif
                                                &nbsp
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                                <label for="status" class="col-sm-3 control-label">Enabled<!-- <br><span>(Active/Not Active)</span> --></label>
                                            <div class="col-sm-9">
                                                <div class="ckbox ckbox-info">
                                                    <input id="checkbox-info1" type="checkbox" name="status" {{($PriceMarginAdjustments->status=='active')? 'checked' : ''}}>
                                                    <label for="checkbox-info1"> </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"></div>
                        </div>
                        <div class="form-footer">
                            <div class="col-sm-offset-5">
                                <button class="btn btn-color btn-hover" type="submit">Submit</button>
                                <a class="btn btn-danger" href="{{URL::to('/')}}/PriceMarginAdjustments/index">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!--==============================Modal Popup=======================-->
<!--<div class="modal modal-success fade in" id="modal-bootstrap-tour" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-dialog" role="document" style="margin: 150px auto;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title"><span class="username">Alert!!!</span></h4>
            </div>
            <div class="modal-body">
                You can not make the parent of this store because this is already child of another store.

            </div>
        </div>
    </div>
</div>-->
<!--==============================Modal Popup=======================--> 
<link href="{{asset('css/hierarchy-select.min.css')}}" rel="stylesheet">
<style type="text/css">
    #ave_collect_size{
        border-right:1px solid rgb(221, 221, 221) !important;
    }
    #ave_collect_size:focus{
        border-right:1px solid #66afe9 !important
    }
    #example-one{
        width: 100%;
    }
    .dropdown-menu li.active:hover a, .dropdown-menu li.active:focus a, .dropdown-menu li.active:active a{
        background-color:#00B1E1 !important;
    }
    .dropdown-menu li.active a{
        background-color:#00B1E1 !important;
    }
    .btn-default.dropdown-toggle.btn-default{
        background-color: white;
    }
    .btn-default.active.focus, .btn-default.active:focus, .btn-default.active:hover, .btn-default:active.focus, .btn-default:active:focus, .btn-default:active:hover, .open>.dropdown-toggle.btn-default.focus, .open>.dropdown-toggle.btn-default:focus, .open>.dropdown-toggle.btn-default:hover{
        background-color: white;
    }
    .dropdown-menu li > a:hover:before{
        border-left:3px solid #00B1E1;
    }
    .modal-success .modal-header {
        background-color: #00B1E1 !important;
        border:1px solid #00B1E1;
        border:none;
    }
    .modal-success .modal-header:before {
        content: ""; 
        border: 1px solid #00B1E1 !important; 

    }
</style>
<!-- END @PAGE CONTENT -->
@section('js')
<script src="{{ asset('js/hierarchy-select.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function () {
    $('#example-one').hierarchySelect();

//    $('#client_type').on('change', function () {
//        var type = this.value;
//        if (type == 'enterprise') {
//            $('.store').css('display', 'none');
//            $('.enterprise').css('display', 'block');
//        }
//        if (type == 'store') {
//            $('.enterprise').css('display', 'none');
//            $('.store').css('display', 'block');
//        }
//        if (type == '') {
//            $('.enterprise').css('display', 'none');
//            $('.store').css('display', 'block');
//        }
//    });
//
//    $('.par').click(function () {
//        var a = $(this).attr('data-level');
//        if (a == 3) {
//            setTimeout(function () {
//                $('#modal-bootstrap-tour').modal('show');
//                $('.selected-label').text('Select Client');
//                $('#parent_id').val('0');
//                $("li").removeClass("active");
//                $('.aa').addClass("active");
//            }, 200);
//        }
//    });
});
</script>

@endsection
@endsection
