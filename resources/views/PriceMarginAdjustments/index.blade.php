@extends('layouts.default') @section('content')
@section('css')
<!-- START @PAGE LEVEL STYLES -->
<link href="{{asset('assets/global/plugins/bower_components/datatables/css/dataTables.bootstrap.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/bower_components/datatables/css/datatables.responsive.css')}}" rel="stylesheet">
<!--/ END PAGE LEVEL STYLES -->

@endsection
<style>
    .table-success tbody tr td .sorting_1 {
        background: red !important;
        color: white;
        border-bottom: 1px solid red !important;
    }
    .dataTable thead tr th:first-child {
        min-width: 10px !important;
    }
    .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
        background-color: #178aa9 !important;
        border: 1px solid #178aa9;
    }
    table.dataTable thead th {
        position: relative;
        background-image: none !important;
    }

    table.dataTable thead th.sorting:after,
    table.dataTable thead th.sorting_asc:after,
    table.dataTable thead th.sorting_desc:after {
        position: absolute;
        top: 12px;
        right: 8px;
        display: block;
        font-family: FontAwesome;
    }
    table.dataTable thead th.sorting:after {
        content: "\f0dc";
        color: #ddd;
        font-size: 0.8em;
        padding-top: 0.12em;
    }
    table.dataTable thead th.sorting_asc:after {
        content: "\f0de";
    }
    table.dataTable thead th.sorting_desc:after {
        content: "\f0dd";
    }

</style>

<style>
    .form-control:focus{
        border: 1px solid #66afe9 !important;
    }
</style>

<!-- START @PAGE CONTENT -->

@if(isset($toast['message']))
{{ $toast['message'] }}
@endif
<div class="header-content">
    <h2><i class="fa fa-table"></i>PRICE MARGIN ADJUSTMENTS <span></span></h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label"></span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="{{URL::to('/')}}/admin/dashboard">Dashboard</a>
            </li>
        </ol>
    </div><!-- /.header-content -->
</div><!--/ End page header -->
<div class="body-content animated fadeIn">
    <!-- Start body content -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-tab panel-tab-double shadow">
                <div class="panel-body no-padding">
                    <div class="panel panel-default shadow no-margin">
                        <div class="panel-heading">
                            <a  href="{{URL::to('/')}}/PriceMarginAdjustments/index" class="btn btn-default tooltips" data-toggle="tooltip" data-placement="top" data-title="Reload" data-action="refresh" data-original-title="" title="" id="ml5"><i class="icon-refresh icons"></i></a>
                           <!--   <a href="#" class="btn btn-default tooltips" data-toggle="tooltip" data-placement="top" data-title="Print" onclick="window.print();return false;" data-original-title="" title=""><i class="icon-printer icons"></i></a>-->

                            <a href="javascript:void(0);" class="btn btn-danger tooltips" data-toggle="tooltip" data-placement="top" data-title="Delete" data-original-title="" title="" onclick="return multiId()" ><i class="icon-trash icons"></i></a>

                            <div class="pull-right" id="mt0">
                               <!--  <button type="button" class="btn btn-success"><i class="icon-user-follow icons"></i> Add client</button>  -->
                                <a class="btn btn-color btn-hover pull-right" href="{{URL::to('/')}}/PriceMarginAdjustments/addPriceMarginAdjustments"><i class="fa fa-plus"></i>&nbsp;Add new adjustment</a>
                            </div>

                            <div class="clearfix"></div>
                        </div><!--/.panel-heading -->

                        <!--Panel Body-->
                        <div class="panel-body">
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="tab-client-all">
                                    <!-- Start datatable -->
                                    <div id="datatable-client-all_wrapper" class="dataTables_wrapper">
                                        <form action="{{URL::to('/') }}/PriceMarginAdjustments/multipleDelete" method="post" class="multipleDeleteForm">
                                            {{ csrf_field() }}
                                            <table id="datatable-ajax" class="table table-responsive table-striped table-success">
                                                <thead>
                                                    <tr role="row">
                                                        <th style="width:10px !important; text-align:center; "><input type="checkbox" class="multiIdCheck"/></th>
                                                        <th>Client</th>
                                                        <th>Country Name</th>
                                                        <th>Product Type</th>
                                                        <th>Brand Name</th>
                                                        <th>Screen Name</th>
                                                        <th>Margin Type </th>
                                                        <th>Value</th>
                                                        <th>Enabled</th>
                                                        <th style="width: 65px !important;">Actions</th>
                                                    </tr>
                                                </thead>
                                                <!--tbody section is required-->

                                                <tbody>

                                                    <?php
                                                    $i = 0;
                                                    if (count($PriceMarginAdjustments) > 0) {
                                                        foreach ($PriceMarginAdjustments as $PriceMarginAdjustment) {
                                                            $i++;
                                                            ?>

                                                            <tr role="row">
                                                                <td style="text-align:center;">
                                                                    <input  type="checkbox" class="id" name="ids[]" value="{{$PriceMarginAdjustment->id}}">
                                                                </td>


                                                                <td>
                                                          <?php
                                                  if (!empty($PriceMarginAdjustment->Clientname->fullpath)) {
                                                        echo $PriceMarginAdjustment->Clientname->fullpath;
                                                                    } else {
                                                                        echo "Default";
                                                                    }
                                                                    ?>

                                                                </td>

                                                                <td> <?php
                                                                    if (isset($PriceMarginAdjustment->Countryname->name)) {
                                                                        echo $PriceMarginAdjustment->Countryname->name;
                                                                    } else {
                                                                        echo "N/A";
                                                                    }
                                                                    ?>
                                                                </td>

                                                                <td>
                                                                    <?php
                                                                    if (isset($PriceMarginAdjustment->ProductOtionType->name)) {
                                                                        echo $PriceMarginAdjustment->ProductOtionType->name;
                                                                    } else {
                                                                        echo "N/A";
                                                                    }
                                                                    ?>

                                                                </td>

                                                                <td>
                                                                    <?php
                                                                    if (isset($PriceMarginAdjustment->ProductOtionBrand->name)) {
                                                                        echo $PriceMarginAdjustment->ProductOtionBrand->name;
                                                                    } else {
                                                                        echo "N/A";
                                                                    }
                                                                    ?>
                                                                </td>

                                                                <td>
                                                                    <?php
                                                                    if (isset($PriceMarginAdjustment->ProductOtionScreen->name)) {
                                                                        echo $PriceMarginAdjustment->ProductOtionScreen->name;
                                                                    } else {
                                                                        echo "N/A";
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <td>{{ $PriceMarginAdjustment->margin_type }}</td>
                                                                <td>{{ $PriceMarginAdjustment->value }}%</td>
                                                                <?php
                                                                if ($PriceMarginAdjustment->status == 'active') {

                                                                    echo "<td style='color:green'><span>&#10003;</span></td>";
                                                                } else {
                                                                    echo "<td style='color:red;'><span>&#9747;</span></td>";
                                                                }
                                                                ?>
                                                                <td class="">
                                                                    <a class="btn btn-round btn-color btn-hover" href="{{URL::to('/')}}/PriceMarginAdjustments/editPriceMarginAdjustments/{{$PriceMarginAdjustment->id}}" style="padding: 0px 6px">
                                                                        <i class="fa fa-edit"></i>
                                                                    </a>
                                                                    <a onclick="return confirm('Are you sure you want to delete?')" class="btn btn-round btn-danger"  style="padding: 0px 6px" href="{{URL::to('/')}}/PriceMarginAdjustments/Delete/{{$PriceMarginAdjustment->id}}">
                                                                        <i class="fa fa-trash-o"></i>
                                                                    </a>
                                                                </td>
                                                            </tr>

                                                            <?php
                                                        }
                                                    } else {

                                                        echo '<tr role="row"><td colspan="9" style="color:red;">  No records found!!!</td></tr>';
                                                    }
                                                    ?>



                                                </tbody>

                                                <!--tfoot section is optional-->
                                                <tfoot>
                                                    <tr role="row">
                                                        <th style="width:10px !important; text-align:center; "><input type="checkbox" class="multiIdCheck"/></th>
                                                        <th>Client</th>
                                                        <th>Country Name</th>
                                                        <th>Product Type</th>
                                                        <th>Brand Name</th>
                                                        <th>Screen Name</th>
                                                        <th>Margin Type </th>
                                                        <th>Value</th>
                                                        <th>Enabled</th>
                                                        <th style="width: 60px !important;">Actions</th>
                                                    </tr>
                                                </tfoot>

                                            </table>
                                        </form>
                                        </form>
                                    </div>
                                    <!--/ End datatable -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @section('js')
    <script>
        $(document).ready(function () {
            $('#datatable-ajax').DataTable({
                "aoColumnDefs": [
                    {'bSortable': false, 'aTargets': [-1, -10]}
                ],
                language: {
                    searchPlaceholder: "Search records"
                },
                "lengthMenu": [[30, 60, 90, -1], [30, 60, 90, "All"]],
                "order": [[1, "asc"]],
                "columnDefs": [{
                        "targets": 0,
                        "orderable": false,
                        'min-width': '10px',
                        "width": "5%"
                    }],
                "bSortClasses": false,
                "searchHighlight": true,
            });

        });

        function multiId() {
            var checkboxcount = jQuery('.id:checked').length;
            if (checkboxcount != 0) {
                if (confirm("Are you sure you want to delete ?")) {
                    jQuery('.multipleDeleteForm').submit();
                }
            } else {
                alert("Please select a record")
                return false;
            }
        }

    </script>
    <!-- START @PAGE LEVEL PLUGINS -->
    <script src="{{asset('assets/global/plugins/bower_components/datatables/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/global/plugins/bower_components/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('assets/global/plugins/bower_components/datatables/js/datatables.responsive.js')}}"></script>
    <!--/ END PAGE LEVEL PLUGINS -->
    @endsection

    @endsection