@extends('layouts.frontendLogin')

@php use App\Http\Controllers\HpUsersController;
$Theme = HpUsersController::Theme_Cookie();
@endphp

@section('title')
Login | {{@$Theme->theme_name}}
@endsection

@if(isset($toast['message']))
{{ $toast['message'] }}
@endif

@section('content')

<style>
.login-title-heading {
    text-align: center;
    font-size: 36px;
    color: #000;
    font-weight: 600;
    font-family: Raleway;
    margin-bottom: 55px;
}
.footer .footer-link ul li {
    float: left;
    padding: 0px 10px;
    position: relative;
}
#lost_pass:hover{
    color: #01518e;
}
#lost_pass{
    color: #b6b6b6;
}
.form-signin .btn-login {
    width: 100%;
    height: 60px;
    /* background: #3085DB !important;*/
    font-size: 24px;
    color: #ffffff;
    font-weight: 600;
}
.login-page{
    background-color: #fff !important;
}
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
            <div class="login-container" style="padding: 0 263px;">
                <form class="form-signin login-form" action="{{ route('login') }}" method="post">
                    {{ csrf_field() }}
                    <h2 class="login-title-heading">Login</h2>
                    @if ($errors->has('active'))
                    <div class="alert alert-info alert-block">
                      
                        <span>{{ $errors->first('active') }}</span>
                    </div>
                    @endif
                    <div class="form-group"> 
                        <input class="form-control" type="email" id="email" class="form-control input-sm" placeholder="Email address " name="email" value="{{ old('email') }}" required style="border-color: E9573F;"> 
                        @if ($errors->has('email')) 
                        <span class="help-block"> 
                            <strong style="color: red">
                                {{ $errors->first('email') }}
                            </strong>
                        </span>
                        @endif                      

                    </div>
                    <div class="form-group">                          
                        <input type="password" class="form-control input-sm" placeholder="Password" name="password" required>
                        @if($errors->has('password')) 
                        <span class="help-block"> 
                            <strong style="color: red">
                                {{ $errors->first('password') }}
                            </strong>
                        </span> 
                        @endif 
                    </div>
                    <button type="submit" class="btn btn-login">Login</button>
                    <div class="col-xs-4 col-sm-4 col-sm-offset-4" style="text-align: center;">
                        <br/><a class="pull-center" id="lost_pass" href="{{url('/')}}/forget">Lost password ?</a>
                    </div>
                </form>               
            </div>          
        </div>
    </div>
</div> <!-- /container --> 

@endsection
