@extends('layouts.frontendLogin')
  @php use App\Http\Controllers\HpUsersController;
        $Theme = HpUsersController::Theme_Cookie();
  @endphp

@section('title')
Password Reset| {{@$Theme->theme_name}}
@endsection

@section('content')
<style>
    .login-title-heading {
        text-align: center;
        font-size: 36px;
        color: #01518e;
        font-weight: 600;
        font-family: Raleway;
        margin-bottom: 55px;
    }
    .footer .footer-link ul li {
        float: left;
        padding: 0px 10px;
        position: relative;
    }
    #lost_pass:hover{
        color: #01518e;
    }
    #lost_pass{
        color: #b6b6b6;
    }

</style>
<div class="container" style="padding-bottom:100px;">
    <div class="row">
        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
            <div class="login-container" style="padding: 0 263px;">

                <!-- Login form -->
                <form class="form-signin login-form" method="POST" action="{{ route('password.request') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="token" value="{{ $token }}">
                    <h2 class="login-title-heading">Reset Password</h2>
                    <!-- /.sign-header -->

                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                        <input id="email" type="email" class="form-control input-sm" name="email" value="{{ $email or old('email') }}" required autofocus placeholder="Email Address" style="border-color: E9573F;">
                        @if ($errors->has('email')) <span class="help-block"> <strong
                                style="color: red">{{ $errors->first('email') }}</strong>
                        </span> @endif
                    </div>
                    <!-- /.form-group -->
                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">

                        <input id="password" type="password" class="form-control" name="password" required placeholder="Password">

                        @if($errors->has('password')) <span class="help-block"> <strong
                                style="color: red">{{ $errors->first('password') }}</strong>
                        </span> @endif 

                    </div>
                    <!-- /.form-group -->
                    <!--.form-group -->
                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">

                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Confirm Password">

                        @if($errors->has('password_confirmation')) <span class="help-block"> <strong
                                style="color: red">{{ $errors->first('password_confirmation') }}</strong>
                        </span> @endif 

                    </div>
                    <!-- /.form-group -->

                    <!-- /.sign-body -->

                    <!-- /.form-group -->

                    <button type="submit" class="btn btn-login" id="login-btn"> Reset Password</button>

                    <div class="col-xs-4 col-sm-4 col-sm-offset-4" style="text-align: center;">
                        <p class="text-muted text-center sign-link">Back to <a href="{{ route('login') }}"> Sign in</a></p>
                    </div>
                    
                    <!-- /.form-group -->


                    <!-- /.sign-footer -->
                </form>
                <!-- /.form-horizontal -->
                <!--/ Login form -->

                <!-- Content text -->

                <!--/ Content text -->
            </div>          
        </div>
    </div>
</div> <!-- /container -->   

@endsection
