@extends('layouts.admin-login') @section('content') 
@if(session('status'))
<div class="alert alert-success">{{ session('status') }}</div>
@endif
<style>
.sign-header{
	background-color:#00B1E1 !important;
	border-color: #63D3E9 !important;
}

.sign-text span{
	background-color:#00B1E1 !important;
}
.btn-theme{
	background-color:#00B1E1 !important;
	border-color: #63D3E9 !important;
}
</style>
<div id="sign-wrapper">

	<div class="brand">
		<img alt="Logo" class="img" src="{{asset('images/logo.png') }}">
	</div>
 

	<form class="form-horizontal" method="POST"
		action="{{ route('password.email') }}">
		{{ csrf_field() }}
		 
		<div class="sign-header">
			<div class="form-group">
				<div class="sign-text">
					<span>Reset your password</span>
				</div>
			</div>
		</div>
		<div class="sign-body">
			<div class="form-group{{ $errors->has('email') ? 'has-error' : '' }}">
				<div class="input-group input-group-lg rounded">
					<input class="form-control" id="email" type="email" name="email"
						value="{{ old('email') }}" required style="border-color: #E9573F;">

					<span class="input-group-addon " style="border-color: #E9573F;"><i
						class="fa fa-envelope"></i></span>
				</div>
				
				@if ($errors->has('email')) <span class="help-block"> <strong
					style="color: red">{{ $errors->first('email') }}</strong>
				</span> @endif
			</div>
		</div>
		<div class="sign-footer">
			<div class="form-group">
				<button type="submit"
					class="btn btn-theme btn-lg btn-block no-margin rounded">Send reset
					email</button>
			</div>
		</div>
	</form>
	<!--/ Lost password form -->

	<!-- Content text -->
	<p class="text-muted text-center sign-link">
		Back to <a href="{{URL::to('/')}}/login"> Sign in</a>
	</p>
	<!--/ Content text -->

</div>
@endsection

