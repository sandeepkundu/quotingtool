@extends('layouts.hpUserLogin')
 @section('title')
Login | upgrade to HP
 @endsection

 @section('content')
<style>
.login-title-heading {
    text-align: center;
    font-size: 36px;
    color: #01518e;
    font-weight: 600;
    font-family: Raleway;
    margin-bottom: 55px;
}
.footer .footer-link ul li {
    float: left;
    padding: 0px 10px;
    position: relative;
}
#lost_pass:hover{
    color: #01518e;
}
#lost_pass{
    color: #b6b6b6;
}

</style>
 <div class="container">
        <div class="row">

        @if(Session::has('status'))
           <div class="alert alert-success">
              <strong>{{ Session::get('status') }}</strong>
            </div>
          @endif
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <div class="login-container" style="padding: 0 263px;">
                            <form class="form-signin login-form" action="{{ route('password.email') }}" method="post">
                            {{ csrf_field() }}
                            <h2 class="login-title-heading">Reset your password</h2>
                            <div class="form-group"> 
                                <input class="form-control" id="email" type="email" name="email" class="form-control input-sm" placeholder="Email address " value="{{ old('email') }}" required style="border-color: E9573F;"> 
                                @if ($errors->has('email')) 
                                    <span class="help-block"> 
                                        <strong style="color: red">
                                            {{ $errors->first('email') }}
                                        </strong>
                                    </span>
                                @endif                      
                                
                            </div>
                            
                            <button type="submit" class="btn btn-login">Send reset email</button>
                            <div class="col-xs-4 col-sm-4 col-sm-offset-4" style="text-align: center;">
                                <br/>
                                <p class="text-muted text-center sign-link">
                                    Back to <a href="{{URL::to('/')}}/login"> Sign in</a>
                                </p>
                            </div>
                          
                </div>          
            </div>
        </div>
    </div> <!-- /container -->   
@endsection