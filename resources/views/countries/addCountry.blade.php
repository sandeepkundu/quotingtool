@extends('layouts.default') @section('content')
<!-- START @PAGE LEVEL STYLES -->
<link href="{{ asset('css/bower_components/fontawesome/css/font-awesome.min.css')}}" rel="stylesheet">
<link href="{{ asset('css/bower_components/animate.css/animate.min.css')}}" rel="stylesheet">
<link href="{{ asset('css/bower_components/dropzone/downloads/css/dropzone.css') }}" rel="stylesheet">
<link href="{{ asset('css/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css') }}" rel="stylesheet">
<!--/ END PAGE LEVEL STYLES -->
<div class="body-content animated fadeIn">
  <div class="row">
    <div class="col-lg-12">
      <div class="panel rounded shadow">
        <div class="panel-heading">
          <div class="pull-left">
            <h3 class="panel-title">ADD NEW COUNTRY</h3>
          </div>
          
          <div class="clearfix"></div>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body no-padding">
          <form class="form-horizontal mt-10" id="commentForm" method="post" action="{{URL::to('/')}}/countries/insertCountry" >
            {{ csrf_field() }}
            <div class="form-body">
              <div class="form-group">

                <label for="username" class="col-sm-2 control-label">Name<span style="color:red;">*</span></label>
                <div class="col-sm-4">
                  <input value="{!!old('name')!!}" class=" form-control" id="countrt_name" name="name" minlength="2" type="text" placeholder="Name">
                  @if ($errors->has('name'))
                  <span class="help-block" style="color:red;">
                    <strong>{{ $errors->first('name') }}</strong>
                  </span>
                  @endif
                  &nbsp;
                </div>

                <label for="currency_name" class="col-sm-2 control-label">Currency Name<span style="color:red;">*</span></label>
                <div class="col-sm-4">
                  <input value="{!!old('currency_name')!!}" type="text" name="currency_name" class="form-control" id="currency_name"
                  placeholder="Currency Name">
                  @if ($errors->has('currency_name'))
                  <span class="help-block" style="color:red;">
                    <strong>{{ $errors->first('currency_name') }}</strong>
                  </span>
                  @endif
                  &nbsp;
                </div>
                <label for="currency_symbol" class="col-sm-2 control-label">Currency Symbol<span style="color:red;">*</span></label>
                <div class="col-sm-4">
                  <input value="{!!old('currency_symbol')!!}"  class=" form-control" id="currency_symbol" name="currency_symbol" type="text" placeholder="Currency Symbol" >
                  @if ($errors->has('currency_symbol'))
                  <span class="help-block" style="color:red;">
                    <strong>{{ $errors->first('currency_symbol') }}</strong>
                  </span>
                  @endif
                  &nbsp;
                </div>
                <label for="currency_rate" class="col-sm-2 control-label">Currency Rate
                  <span style="color:red;">*</span>
                </label>
                <div class="col-sm-4">
                  <input value="{!!old('currency_rate')!!}" type="text" name="currency_rate" class="form-control" id="currency_rate" placeholder="Currency Rate">
                 <!--  <div class="input-group">
                    <span class="input-group-addon">$</span>
                    <input value="{!!old('currency_rate')!!}" type="text" name="currency_rate" class="form-control" id="currency_rate" placeholder="Currency Rate">
                    <span class="input-group-addon bg-warning">.00</span>
                  </div> -->
                  @if ($errors->has('currency_rate'))
                  <span class="help-block" style="color:red;">
                    <strong>{{ $errors->first('currency_rate')}}</strong>
                  </span>
                  @endif
                  &nbsp;
                </div>
                <label class="text-right col-sm-2">Enabled
                 
                </label>
                <div class="col-sm-4">
                  <div class="ckbox ckbox-primary">
                    <input type="checkbox" id="checkbox-primary2" class="chk"  name="country_status" checked="checked">
                    <label for="checkbox-primary2"></label>
                  </div>
                </div>
                <br/><div class="col-sm-12">
                <div class="panel-heading">
                  <div class="pull-left">
                    <h3 class="panel-title">COUNTRY REGION</h3>
                  </div>
                  <div class="clearfix"></div>
                </div><br/>
              </div>
              <div class="col-sm-4 col-sm-offset-2">
                <input type="text" name="country_region" class="form-control" id="country_region"
                placeholder="Country Region">
                <span class="help-block" style="color:red;">
                  <strong id="c_region"></strong>
                </span>

                @if ($errors->has('country_region_name'))
                <span class="help-block" style="color:red;">
                  <strong>{{ $errors->first('country_region_name')}}</strong>
                </span>
                @endif
                &nbsp;
              </div>
              <div class="col-sm-2">
                <button type="button" id="add_country_region" class="btn btn-color btn-hover">Add</button>
              </div>
              <div class="table-responsive mb-20 col-sm-12">
                <button type="button" style="display:none; margin-bottom: 10px;" class="delete-row btn btn-danger" style="margin-bottom:4px;">Delete Row</button>
                <table class="table table-striped table-success table-middle table-project-clients region_table" role="grid" aria-describedby="datatable-client-all_info" style="display:none;">

                  <thead>
                    <tr>
                      <th>Select</th>
                      <th>Country Region</th>
                      <th class="text-center" style="width: 108px !important;">Enabled</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>

              </div>
            </div>
            <!-- /.form-group -->
            <div class="form-group"></div>
            <!-- /.form-group -->
          </div>
          <!-- /.form-body -->
          <div class="form-footer">
            <div class="col-sm-offset-5">
              <button class="btn btn-color btn-hover" type="submit">Submit</button>
              <a class="btn btn-danger" href="{{URL::to('/')}}/countries/index">Cancel</a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- /.panel -->
  <!--/ End horizontal form -->
  </div>
</div>
@section('js')
<!-- START @PAGE LEVEL SCRIPT -->
<script type=text/javascript src="{{ asset('js/lib/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js') }}"></script>
<script type=text/javascript src="{{ asset('js/lib/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.min.js') }}"></script>
<script type=text/javascript src="{{ asset('js/pages/blankon.form.advanced.js') }}"></script>
<!-- START @PAGE LEVEL SCRIPT -->
<script>
  $(document).ready(function(){
    var i=0;
    $('#country_region').keypress(function(e){
      if(e.which == 13) {
        $("#add_country_region").click();
        return false;
      }
    });
    $("#add_country_region").click(function(){
      i=i+1;
      var region_name = $("#country_region").val();
      var region_name_hidden=$(".region_name_hidden").val();
      var row = "<tr class='country_rgn'><td><input type='checkbox'  name='record' name='record'></td><td id='region_name'>"+region_name+"<input type='hidden' name='country_region_name[]' class='region_name_hidden' value='"+region_name+"'/>"+"</td><td> <input type='checkbox' name='region_status[]' checked id="+i+" data-abc="+i+" value='1' data-on-color='teal' onclick='check(this)'> <input type='hidden' name='check[]' value='1' class='"+i+"'></div></td></tr>";
      $("#country_region").val('');
      if (!region_name==""){
        $('.delete-row').show();
        $('.region_table').show();
        $('#c_region').text('');
        var a = ($('input[name^="country_region_name[]"]').length)/8;
        if(a!=0){
          var titles = $('input[name^="country_region_name[]"]').map(function(idx, elem) {
            return $(elem).val();
          }).get();
          var abc=0;
          for(j=0; j<a;j++){
            if(region_name==titles[j]){
              abc = 1;
            }
          }
          if(abc==1){
            $('#c_region').text('Sorry, this values is already exist.');
          }else{
            $("table tbody").append(row);
          }
        }else{
          $("table tbody").append(row);
        }

      }else{
        $('#c_region').text('The country_region field is required.');
      }
    });
  // Find and remove selected table rows
  $(".delete-row").click(function(){
    $("table tbody").find('input[name="record"]').each(function(){
      if($(this).is(":checked")){
        $(this).parents("tr").remove();
      }
    });
  });
  $('.ck').click(function(){
  // alert('test');
  /* if ($('.ck').is(':checked')) {
  $('.a').val('1');
  }
  else{
  $('.a').val('0');
}*/
});
});
  
</script>
<script type="text/javascript">
  function check(abc){
    var a = $(abc).data('abc');
    if($('#'+a).is(":checked")){
      $('.'+a).val('1');
    }else{
      $('.'+a).val('0');
      
    }
  }
</script>>
@endsection
@endsection