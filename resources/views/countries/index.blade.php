@extends('layouts.default') @section('content')
<!-- START @PAGE CONTENT -->
@if(isset($toast['message']))
{{ $toast['message'] }}
@endif
@section('css')
<!-- START @PAGE LEVEL STYLES -->
<link href="{{asset('assets/global/plugins/bower_components/datatables/css/dataTables.bootstrap.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/bower_components/datatables/css/datatables.responsive.css')}}" rel="stylesheet">
<!--/ END PAGE LEVEL STYLES -->

@endsection
<style>
    .table-success tbody tr td .sorting_1 {
        background: red !important;
        color: white;
        border-bottom: 1px solid red !important;
    }
    .dataTable thead tr th:first-child {
        min-width: 10px !important;
    }
    .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
        background-color: #178aa9 !important;
        border: 1px solid #178aa9;
    }
    table.dataTable thead th {
        position: relative;
        background-image: none !important;
    }

    table.dataTable thead th.sorting:after,
    table.dataTable thead th.sorting_asc:after,
    table.dataTable thead th.sorting_desc:after {
        position: absolute;
        top: 12px;
        right: 8px;
        display: block;
        font-family: FontAwesome;
    }
    table.dataTable thead th.sorting:after {
        content: "\f0dc";
        color: #ddd;
        font-size: 0.8em;
        padding-top: 0.12em;
    }
    table.dataTable thead th.sorting_asc:after {
        content: "\f0de";
    }
    table.dataTable thead th.sorting_desc:after {
        content: "\f0dd";
    }

</style>
<!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-table"></i>COUNTRIES<span></span></h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label"></span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="{{URL::to('/')}}/admin/dashboard">Dashboard</a>
                <!-- <i class="fa fa-angle-right"></i>
                -->
            </li>
            <li>
                <!-- Countries
                <i class="fa fa-angle-right"></i>
                index -->
            </li>
            <li class="active">
            </li>
        </ol>
    </div><!-- /.header-content -->
</div><!--/ End page header -->
<div class="body-content animated fadeIn">
    <!-- Start body content -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-tab panel-tab-double shadow">
                <div class="panel-body no-padding">
                    <div class="panel panel-default shadow no-margin">
                        <div class="panel-heading">
                            <a  href="{{URL::to('/')}}/countries/index" class="btn btn-default tooltips" data-toggle="tooltip" data-placement="top" data-title="Reload" data-action="refresh" data-original-title="" title="" id="ml5"><i class="icon-refresh icons"></i></a>
                            <!--   <a href="#" class="btn btn-default tooltips" data-toggle="tooltip" data-placement="top" data-title="Print" onclick="window.print();return false;" data-original-title="" title=""><i class="icon-printer icons"></i></a>-->

                            <a href="javascript:void(0);" class="btn btn-danger tooltips" data-toggle="tooltip" data-placement="top" data-title="Delete" data-original-title="" title="" onclick="return multiId()" ><i class="icon-trash icons"></i></a>

                            <div class="pull-right" id="mt0">
                                    <!--  <button type="button" class="btn btn-success"><i class="icon-user-follow icons"></i> Add client</button>  -->
                                <a href="{{URL::to('/')}}/countries/addCountry" class="btn btn-color btn-hover"><i class="fa fa-plus"></i> &nbsp;Add new country</a>
                            </div>
                            <div class="clearfix"></div>
                        </div><!--/.panel-heading -->
                        <!--Panel Body-->
                        <div class="panel-body">
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="tab-client-all">
                                    <!-- Start datatable -->
                                    <div id="datatable-client-all_wrapper" class="dataTables_wrapper form-inline">
                                        <form action="{{URL::to('/') }}/countries/multipleDelete" method="post" class="multipleDeleteForm">
                                            {{ csrf_field() }}
                                            <table id="datatable-ajax" class="table table-striped table-success">

                                                <thead>
                                                    <tr role="row">
                                                        <th style="width:10px !important;text-align:center;"><input type="checkbox" class="multiIdCheck"/></th>
                                                        <th>Country Name</th>
                                                        <th>Currency Name</th>
                                                        <th>Currency Symbol</th>
                                                        <th>Currency Rate</th>
                                                        <th>Enabled</th>
                                                        <th class="" style="width: 108px !important;">Actions</th>
                                                    </tr>
                                                </thead>
                                                <!--tbody section is required-->
                                                <tbody>

                                                    <?php
                                                    if ($countries->count() > 0) {


                                                        $i = 0;
                                                        foreach ($countries as $country) {
                                                            $i++;
                                                            ?>
                                                            <tr>
                                                                <td style="text-align:center;">
                                                                    <input type="checkbox" class="id" name="ids[]" value="{{$country->id}}"/>
                                                                </td>
                                                                <!-- <td>{{$i}}</td> -->
                                                        <input type="hidden" value="{{$country->name}}" name="country_name">
                                                        <td>{{$country->name}}</td>
                                                        <td>{{$country->currency_name}}</td>
                                                        <td>{{$country->currency_symbol}}</td>
                                                        <td>{{$country->currency_rate}}</td>
                                                        <?php
                                                        if ($country->status == 'active') {
                                                            echo "<td style='color:green'><span>&#10003;</span></td>";
                                                        } else {
                                                            echo "<td style='color:red;'><span>&#9747;</span></td>";
                                                        }
                                                        ?>
                                                        <td>
                                                            <a class="btn btn-round btn-color btn-hover pull-left" href="{{URL::to('/')}}/countries/editCountry/{{$country->id}}" style="padding: 0px 6px;">
                                                                <i class="fa fa-edit"></i>
                                                            </a>

                                                            <a onclick="return confirm('Are you sure you want to delete?')" class="btn btn-round btn-danger"  style="padding: 0px 6px;" id="ml2" href="{{URL::to('/')}}/countries/deleteCountry/{{$country->id}}">
                                                                <i class="fa fa-trash-o"></i>
                                                            </a>

                                                        </td>

                                                        </tr>
                                                        <?php
                                                    }
                                                } else {
                                                    echo '<tr><td colspan="5" style="color:red;">N No records found!!!</td></tr>';
                                                }
                                                ?>

                                                </tbody>
                                                <!--tfoot section is optional-->
                                                <tfoot>
                                                    <tr role="row">
                                                        <th style="width:10px !important;text-align:center;"><input type="checkbox" class="multiIdCheck"/></th>
                                                        <th>Country Name</th>
                                                        <th>Currency Name</th>
                                                        <th>Currency Symbol</th>
                                                        <th>Currency Rate</th>
                                                        <th>Enabled</th>
                                                        <th class="" style="width: 108px !important;">Actions</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </form>
                                    </div>
                                    <!--/ End datatable -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@section('js')
<!-- START @PAGE LEVEL PLUGINS -->
<script src="{{asset('assets/global/plugins/bower_components/datatables/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/global/plugins/bower_components/datatables/js/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('assets/global/plugins/bower_components/datatables/js/datatables.responsive.js')}}"></script>
<!--/ END PAGE LEVEL PLUGINS -->

<script>
                                                                $(document).ready(function () {
                                                                    $('#datatable-ajax').DataTable({
                                                                        "order": [[1, "asc"]],
                                                                        "aoColumnDefs": [
                                                                            {'bSortable': false, 'aTargets': [-1, -7]}
                                                                        ],
                                                                        language: {
                                                                            searchPlaceholder: "Search records"
                                                                        },
                                                                        "lengthMenu": [[30, 60, 90, -1], [30, 60, 90, "All"]],
                                                                        "columnDefs": [{
                                                                                "targets": 0,
                                                                                "orderable": false,
                                                                                'min-width': '10px',
                                                                                "width": "5%"
                                                                            }],
                                                                        "bSortClasses": false,
                                                                        "searchHighlight": true,
                                                                    });

                                                                });

                                                                function multiId() {
                                                                    var checkboxcount = jQuery('.id:checked').length;
                                                                    if (checkboxcount != 0) {
                                                                        if (confirm("Are you sure you want to delete ?")) {
                                                                            jQuery('.multipleDeleteForm').submit();
                                                                        }
                                                                    } else {
                                                                        alert("Please select a record")
                                                                        return false;
                                                                    }
                                                                }

</script>
@endsection
@endsection