@extends('layouts.default') @section('content')
<!-- START @PAGE LEVEL STYLES -->
<link href="{{ asset('css/bower_components/fontawesome/css/font-awesome.min.css')}}" rel="stylesheet">
<link href="{{ asset('css/bower_components/animate.css/animate.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/bower_components/bootstrap-wysihtml5/src/bootstrap-wysihtml5.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/bower_components/summernote/dist/summernote.css')}}" rel="stylesheet">
<link href="{{ asset('css/bower_components/dropzone/downloads/css/dropzone.css') }}" rel="stylesheet">
<link href="{{ asset('assets/global/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet">
<link href="{{ asset('assets/global/plugins/bower_components/jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/global/plugins/bower_components/chosen_v1.2.0/chosen.min.css') }}" rel="stylesheet">
<!--Client Heirarchy CSS File-->
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.common-material.min.css" />
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.material.min.css" />
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.material.mobile.min.css" />
<!--Client Heirarchy CSS File-->
<link rel="stylesheet" href="{{asset('css/usersAdd.css')}}" />
<style type="text/css">
    .note-editor .note-editable
    {
        background: #fff;
    }
    .modal-dialog{
        margin-top: 70px;
    }
    .note-editor.fullscreen {
        position: fixed;
        top: 69px;
        left: 239px;
        bottom: 20px;
        right: 157px;
        z-index: 1050;

    }

    .fullscreen{
        width: 81% !important;
    }
    .note-editable{
        height: 80% !important;
    }


</style>
<!--/ END PAGE LEVEL STYLES -->
<div class="body-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">ADD PRODUCT PARTS</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body no-padding">
                    <form class="form-horizontal mt-10" id="commentForm" method="post" action="{{URL::to('/')}}/productOptions/insertProductOption" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="col-sm-6">
                                        <label for="" class="col-sm-3 control-label">Product Type<span style="color:red;">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="hidden" name="type" id="hidden_type" value="{{$type}}" >
                                            <input value="{{$type}}" disabled="" class="form-control" id="productOptions_type" type="text"> 
                                            @if ($errors->has('type'))
                                            <span class="help-block" style="color:red;">
                                                <strong>{{ $errors->first('type') }}</strong>
                                            </span>
                                            @endif
                                            &nbsp;
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="name" class="col-sm-3 control-label">Name<span style="color:red;">*</span></label>
                                        <div class="col-sm-9">

                                            <input value="{!!old('name')!!}" list='browsers' type="text" name="name" class="form-control select" id="name" placeholder="Name">

                                            @if ($errors->has('name'))
                                            <span class="help-block" style="color:red;">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                            &nbsp;
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    @if($type=='HDD' || $type=='RAM' )
                                    <div class="col-sm-6">
                                        <label for="value" class="col-sm-3 control-label">Value<span style="color:red;">*</span></label>
                                        <div class="col-sm-9">
                                            <input value="{!!old('value')!!}"  class=" form-control" id="value" name="value"  type="text" placeholder="Value" >
                                            @if ($errors->has('value'))
                                            <span class="help-block" style="color:red;">
                                                <strong>{{ $errors->first('value') }}</strong>
                                            </span>
                                            @endif
                                            &nbsp;
                                        </div>
                                    </div>
                                    @endif
                                    <!-- <div class="col-sm-6">
                                     <label for="order" class="col-sm-3 control-label">Order<span style="color:red;">*</span></label>
                                     <div class="col-sm-9">
                                      <input value="{!!old('order')!!}" type="text" name="order" class="form-control" id="order"
                                      placeholder="Order">
                                      @if ($errors->has('order'))
                                      <span class="help-block" style="color:red;">
                                        <strong>{{ $errors->first('order')}}</strong>
                                      </span>
                                      @endif
                                      &nbsp;
                                    </div>
                                  </div> -->
                                </div>
                                <div class="col-sm-12">
                               

                                    <div class="col-sm-6">
                                     <label for="image" class="col-sm-3 control-label">Image<!-- <span style="color:red;">*</span> --></label>
                                        <div class="col-sm-9">
                                         <!-- <input value="{!!old('image')!!}" t    ype="file" name="image" class="form-control" id="image"> -->
                                            <div class="fileinput input-group fileinput-new" data-provides="fileinput">
                                                <div class="form-control" data-trigger="fileinput">
                                                    <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                    <span class="fileinput-filename"></span>
                                                </div>
                                                <span class="input-group-addon btn btn-success btn-file" style="border-color:#009dc8 !important; ;background-color:#009dc8 !important;">
                                                    <span class="fileinput-new" style="background-color:#009dc8 !important">Select file</span>
                                                    <span class="fileinput-exists" style="background-color:#009dc8 !important">Change</span>
                                                    <input type="hidden" value="" name="image">
                                                    <input type="file" name="image" d="image" value="{!!old('image')!!}">
                                                </span>
                                                <a href="javascript:void(0)" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                                            </div> 
                                            @if ($errors->has('image'))
                                            <span class="help-block" style="color:red;">
                                                <strong>{{ $errors->first('image')}}</strong>
                                            </span>
                                            @endif
                                            &nbsp;
                                        </div>
                                    </div>
                                   
                                  <div class="col-sm-6">
                                      <label class="col-sm-3 text-right">Enabled<!-- <br/><span>(Active/Not Active)</span> -->
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="ckbox ckbox-primary">
                                                <input type="checkbox" id="checkbox-primary2" class="chk"  name="status" checked="checked">
                                                <label for="checkbox-primary2"></label>
                                            </div>                                        
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-12">
                                     @if($type == 'Product' || $type == 'Brand')
                               <div class="col-lg-6">
                                            <label for="client" class="col-sm-3 control-label">
                                                Clients </label>
                                                <div class="col-sm-9">
                                                    <div class="demo-section k-content">
                                                        <div id="dialog">
                                                            <div class="dialogContent">
                                                                <input id="filterText" type="text" placeholder="Search Client" />
                                                                <div class="selectAll" class='com-sm-12'>
                                                                    <input type="checkbox" id="chbAll" class="k-checkbox" onchange="chbAllOnChange()" />
                                                                    <label class="k-checkbox-label" for="chbAll">Select All</label>
                                                                    <span id="result">0 client selected</span>
                                                                </div>
                                                                <div id="treeview"></div>
                                                            </div>
                                                        </div>
                                                        <select id="multiselect"></select>

                                                        <button id="openWindow" class="k-primary btn btn-block btn-color" type="button" style="border:none;">Select Clients</button>
                                                    </div>
                                                </div>

                                                @if ($errors->has('select'))
                                                <span class="help-block" style="color:red;">
                                                    <strong>{{ $errors->first('select') }}</strong>
                                                </span>
                                                @endif
                                    </div>
                                    @endif
                                </div>



                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <label for="description" class="col-sm-2 control-label" style="font-weight: 800; text-align: left;">Description</label>
                                        <div class="col-sm-12">
                                            <textarea type="text" name="description" class="form-control" id="summernote-textarea"
                                                      placeholder="Description">{{old('description')}}</textarea>
                                            @if ($errors->has('description'))
                                            <span class="help-block" style="color:red;">
                                                <strong>{{ $errors->first('description')}}</strong>
                                            </span>
                                            @endif
                                            &nbsp;
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.form-group -->
                            <div class="form-group"></div>
                            <!-- /.form-group -->
                        </div>
                        <!-- /.form-body -->
                        <div class="form-footer">
                            <div class="col-sm-offset-5">
                                <button class="btn btn-color btn-hover" type="submit">Submit</button>
                                <a class="btn btn-danger" href="javascript:history.back()">Cancel</a>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <!-- /.panel -->
            <!--/ End horizontal form -->
        </div>
    </div>
</div>
@section('js')
<!-- START @PAGE LEVEL SCRIPT -->

<script src="{{asset('assets/global/plugins/bower_components/bootstrap-wysihtml5/lib/js/wysihtml5-0.3.0.min.js')}}"></script>
<script src="{{asset('assets/global/plugins/bower_components/bootstrap-wysihtml5/src/bootstrap-wysihtml5.js')}}"></script>
<script src="{{asset('assets/global/plugins/bower_components/summernote/dist/summernote.min.js')}}"></script>

<script src="http://jonmiles.github.io/bootstrap-treeview/js/bootstrap-treeview.js"></script>
<!--         <script src="{{asset('assets/admin/js/pages/blankon.form.wysiwyg.js')}}"></script>  -->
<script src="{{ asset('js/ClientTree.js') }}"></script>
<script type="text/javascript">
var BlankonFormWysiwyg = function () {

    return {
        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            BlankonFormWysiwyg.bootstrapWYSIHTML5();
            BlankonFormWysiwyg.summernote();
        },
        // =========================================================================
        // BOOTSTRAP WYSIHTML5
        // =========================================================================
        bootstrapWYSIHTML5: function () {
            if ($('#wysihtml5-textarea').length) {
                $('#wysihtml5-textarea').wysihtml5('');
            }
        },
        // =========================================================================
        // SUMMERNOTE
        // =========================================================================
        summernote: function () {
            if ($('#summernote-textarea').length) {
                $('#summernote-textarea').summernote();
            }
        }

    };

}();

// Call main app init
BlankonFormWysiwyg.init();
</script>
<script type="text/javascript">
    $(document).ready(function () {

                var v_token = "{{csrf_token()}}";
                $('#loadingDiv').css('display', 'block');

                $.ajax({
                    type: 'post',
                    url: SITE_URL + '/ClientSelect',
                    data: {_token: v_token},
                    dataType: 'JSON',
                    success: function (obj) {
                        var s = [];
                        var a = [];
                        var c = [];
                        var i = 0;
                        for (var item in obj) {
                            j = 0;
                            var obj1 = obj[item];
                            for (var key in obj1.items) {
                                var k = 0;
                                var obj3 = obj1.items[key];
                                for (var element in obj3.items) {
                                    c[k] = {id: obj3.items[element].id, text: obj3.items[element].name};
                                    k++;
                                }

                                a[j] = {id: obj1.items[key].id, text: obj1.items[key].name, expanded: true, items: c};
                                j++;
                                c = [];
                            }

                            s[i] = {
                                id: obj[item].id, text: obj[item].name, expanded: true, items: a
                            };
                            i++;
                            a = [];
                        }

                        var myDataSource = new kendo.data.HierarchicalDataSource({
                            data: s
                        });

                        $("#multiselect").kendoMultiSelect({
                            dataTextField: "text",
                            dataValueField: "id"
                        });

                        $("#treeview").kendoTreeView({
                            loadOnDemand: false,
                            checkboxes: {
                                checkChildren: false
                            },
                            dataSource: myDataSource,
                            check: onCheck,
                            expand: onExpand
                        });

                        var dialog = $("#dialog");
                        var multiSelect = $("#multiselect").data("kendoMultiSelect");
                        $("#openWindow").kendoButton();
                        multiSelect.readonly(false);

                        $("#openWindow").click(function () {
                            dialog.data("kendoDialog").open();
                            $(this).fadeOut();
                        });

                        dialog.kendoDialog({
                            width: "400px",
                            title: "Select Client",
                            visible: false,
                            actions: [
                            {
                                text: 'Cancel',
                                primary: false,
                                action: onCancelClick
                            },
                            {
                                text: 'Ok',
                                primary: true,
                                action: onOkClick
                            }
                            ],
                            close: onClose
            })//.data("kendoDialog").open();


                        function onCancelClick(e) {
                            e.sender.close();
                        }

                        function onOkClick(e) {
                            var checkedNodes = [];
                            var treeView = $("#treeview").data("kendoTreeView");

                            getCheckedNodes(treeView.dataSource.view(), checkedNodes);
                            populateMultiSelect(checkedNodes);

                            e.sender.close();
                        }

                        function onClose() {
                            $("#openWindow").fadeIn();
                        }

                        function populateMultiSelect(checkedNodes) {
                            var multiSelect = $("#multiselect").data("kendoMultiSelect");
                            multiSelect.dataSource.data([]);

                            var multiData = multiSelect.dataSource.data();
                            if (checkedNodes.length > 0) {
                                var array = multiSelect.value().slice();
                                for (var i = 0; i < checkedNodes.length; i++) {
                                    multiData.push({text: checkedNodes[i].text, id: checkedNodes[i].id});
                                    array.push(checkedNodes[i].id.toString());
                                }

                                multiSelect.dataSource.data(multiData);
                                multiSelect.dataSource.filter({});
                                multiSelect.value(array);
                            }
                        }

                        function checkUncheckAllNodes(nodes, checked) {
                            for (var i = 0; i < nodes.length; i++) {
                                nodes[i].set("checked", checked);

                                if (nodes[i].hasChildren) {
                                    checkUncheckAllNodes(nodes[i].children.view(), checked);
                                }
                            }
                        }

                        function chbAllOnChange() {
                            var checkedNodes = [];
                            var treeView = $("#treeview").data("kendoTreeView");
                            var isAllChecked = $('#chbAll').prop("checked");

                            checkUncheckAllNodes(treeView.dataSource.view(), isAllChecked)

                            if (isAllChecked) {
                                setMessage($('#treeview input[type="checkbox"]').length);
                            }
                            else {
                                setMessage(0);
                            }
                        }

                        function getCheckedNodes(nodes, checkedNodes) {
                            var node;

                            for (var i = 0; i < nodes.length; i++) {
                                node = nodes[i];

                                if (node.checked) {
                                    checkedNodes.push({text: node.text, id: node.id});
                                }

                                if (node.hasChildren) {
                                    getCheckedNodes(node.children.view(), checkedNodes);
                                }
                            }
                        }

                        function onCheck() {
                            var checkedNodes = [];
                            var treeView = $("#treeview").data("kendoTreeView");

                            getCheckedNodes(treeView.dataSource.view(), checkedNodes);
                            setMessage(checkedNodes.length);
                        }

                        function onExpand(e) {
                            if ($("#filterText").val() == "") {
                                $(e.node).find("li").show();
                            }
                        }

                        function setMessage(checkedNodes) {
                            var message;

                            if (checkedNodes > 0) {
                                message = checkedNodes + " clients selected";
                            }
                            else {
                                message = "0 clients selected";
                            }

                            $("#result").html(message);
                        }

                        $("#filterText").keyup(function (e) {
                            var filterText = $(this).val();

                            if (filterText !== "") {
                                $(".selectAll").css("visibility", "hidden");
                                $("#treeview .k-group .k-group .k-in").closest("li").hide();
                                $("#treeview .k-group").closest("li").hide();

                                $("#treeview .k-in:contains(" + filterText + ")").each(function () {
                                    $(this).parents("ul, li").each(function () {
                                        var treeView = $("#treeview").data("kendoTreeView");
                                        treeView.expand($(this).parents("li"));
                                        $(this).show();
                                    });
                                });

                                $("#treeview .k-group .k-in:contains(" + filterText + ")").each(function () {
                                    $(this).parents("ul, li").each(function () {
                                        $(this).show();
                                    });
                                });
                            } else {
                                $("#treeview .k-group").find("li").show();
                                var nodes = $("#treeview > .k-group > li");

                                $.each(nodes, function (i, val) {
                                    if (nodes[i].getAttribute("data-expanded") == null) {
                                        $(nodes[i]).find("li").hide();
                                    }
                                });

                                $(".selectAll").css("visibility", "visible");
                            }
                        });
                        $('#multiselect').attr('name', 'select[]');
            //$('#multiselect option').attr('selected');

            $("#multiselect option").each(function () {
                $(this).attr('selected', 'true');

            });
            $('#loadingDiv').css('display', 'none');
        },
        error: function (error) {
            alert('error block');
        }
    });


});

function chbAllOnChange() {
    var checkedNodes = [];
    var treeView = $("#treeview").data("kendoTreeView");
    var isAllChecked = $('#chbAll').prop("checked");

    checkUncheckAllNodes(treeView.dataSource.view(), isAllChecked)

    if (isAllChecked) {
        setMessage($('#treeview input[type="checkbox"]').length);
    }
    else {
        setMessage(0);
    }
}
function checkUncheckAllNodes(nodes, checked) {
    for (var i = 0; i < nodes.length; i++) {
        nodes[i].set("checked", checked);

        if (nodes[i].hasChildren) {
            checkUncheckAllNodes(nodes[i].children.view(), checked);
        }
    }
}
function chbAllOnChange() {
    var checkedNodes = [];
    var treeView = $("#treeview").data("kendoTreeView");
    var isAllChecked = $('#chbAll').prop("checked");

    checkUncheckAllNodes(treeView.dataSource.view(), isAllChecked)

    if (isAllChecked) {
        setMessage($('#treeview input[type="checkbox"]').length);
    }
    else {
        setMessage(0);
    }
}
function onCheck() {
    var checkedNodes = [];
    var treeView = $("#treeview").data("kendoTreeView");

    getCheckedNodes(treeView.dataSource.view(), checkedNodes);
    setMessage(checkedNodes.length);
}

function onExpand(e) {
    if ($("#filterText").val() == "") {
        $(e.node).find("li").show();
    }
}
</script>
@endsection

@endsection
