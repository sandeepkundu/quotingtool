@extends('layouts.default') @section('content')
<!-- START @PAGE LEVEL STYLES -->
<link href="{{ asset('css/bower_components/fontawesome/css/font-awesome.min.css')}}" rel="stylesheet">
<link href="{{ asset('css/bower_components/animate.css/animate.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/bower_components/bootstrap-wysihtml5/src/bootstrap-wysihtml5.css')}}" rel="stylesheet">
<link href="{{asset('assets/global/plugins/bower_components/summernote/dist/summernote.css')}}" rel="stylesheet">
<link href="{{ asset('css/bower_components/dropzone/downloads/css/dropzone.css') }}" rel="stylesheet">
<link href="{{ asset('css/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css') }}" rel="stylesheet">

<link href="{{ asset('assets/global/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet">
<link href="{{ asset('assets/global/plugins/bower_components/jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/global/plugins/bower_components/chosen_v1.2.0/chosen.min.css') }}" rel="stylesheet">


<!--Client Heirarchy CSS File-->
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.common-material.min.css" />
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.material.min.css" />
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.material.mobile.min.css" />
<!--Client Heirarchy CSS File-->
<link rel="stylesheet" href="{{asset('css/usersAdd.css')}}" />
<style type="text/css">
    .note-editor .note-editable
    {
        background: #fff;
    }
    .note-editor.fullscreen {
        position: fixed;
        top: 69px;
        left: 239px;
        bottom: 20px;
        right: 157px;
        z-index: 1050;
    }
    .fullscreen{
        width: 81% !important;
        height: 80% !important;
    }
    .note-editable{
        height: 80% !important;
    }

</style>
<!--/ END PAGE LEVEL STYLES -->
<div class="body-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">EDIT PRODUCT PARTS</h3>
                    </div>

                    <div class="clearfix"></div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body no-padding">
                    <form class="form-horizontal mt-10" id="commentForm" method="post" action="{{URL::to('/')}}/productOptions/updateProductOption" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input class=" form-control" id="name" value="{{ $productOptions->id }}" name="id" minlength="2" type="hidden" required />
                        <div class="form-body">
                            <div class="form-group">
                                <input type="hidden" name="ids" value="{{$productOptions->id}}">
                                <div class="col-sm-12">
                                    <div class="col-sm-6">
                                        <label for="" class="col-sm-3 control-label">Product Type<span style="color:red;">*</span></label>
                                        <div class="col-sm-9">
                                            <input value="{{$productOptions->type}}" disabled="" class=" form-control" id="productOptions_type" name="type"  type="text" placeholder="type"> 
                                            @if ($errors->has('type'))
                                            <span class="help-block" style="color:red;">
                                                <strong>{{ $errors->first('type') }}</strong>
                                            </span>
                                            @endif
                                            &nbsp;
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="name" class="col-sm-3 control-label">Name<span style="color:red;">*</span></label>
                                        <div class="col-sm-9">
                                            <input value="{{$productOptions->name}}" list='browsers1' type="text" name="name" class="form-control" id="name"
                                                   placeholder="Name">

                                            @if ($errors->has('name'))
                                            <span class="help-block" style="color:red;">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                            &nbsp;
                                        </div>

                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    @if($productOptions->type=='HDD' || $productOptions->type=='RAM' )
                                    <div class="col-sm-6">
                                        <label for="currency_symbol" class="col-sm-3 control-label">Value<span style="color:red;">*</span></label>
                                        <div class="col-sm-9">
                                            <input value="{{$productOptions->value}}"  class=" form-control" id="currency_symbol" name="value"  type="text" placeholder="Value" >
                                            @if ($errors->has('value'))
                                            <span class="help-block" style="color:red;">
                                                <strong>{{ $errors->first('value') }}</strong>
                                            </span>
                                            @endif
                                            &nbsp;
                                        </div>
                                    </div>
                                    @endif
                                    <!-- <div class="col-sm-6">
                                      <label for="order" class="col-sm-3 control-label">Order<span style="color:red;">*</span></label>
                                      <div class="col-sm-9">
                                        <input value="{{$productOptions->order}}" type="text" name="order" class="form-control" id="order"
                                        placeholder="Order">
                                        @if ($errors->has('order'))
                                        <span class="help-block" style="color:red;">
                                          <strong>{{ $errors->first('order')}}</strong>
                                        </span>
                                        @endif
                                        &nbsp;
                                      </div>
                                    </div> -->
                                </div>

                                <div class="col-sm-12">
                                    <div class="col-sm-6">
                                     <label for="image" class="col-sm-3 control-label">Image<!-- <span style="color:red;">*</span> --></label>
                                        <div class="col-sm-7">
                                         <!--   <input value="{{$productOptions->image}}" type="file" name="image" class="form-control" id="image"> -->
                                            <div class="fileinput input-group fileinput-new" data-provides="fileinput">
                                                <div class="form-control" data-trigger="fileinput">
                                                    <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                    <span class="fileinput-filename"></span>
                                                </div>
                                                <span class="input-group-addon btn btn-success btn-file" style="border-color:#009dc8 !important; ;background-color:#009dc8 !important;">
                                                    <span class="fileinput-new" style="background-color:#009dc8 !important">Select file</span>
                                                    <span class="fileinput-exists" style="background-color:#009dc8 !important">Change</span>
                                                    <input type="hidden" value="" name="image">
                                                    <input type="file" name="image" id="image">
                                                </span>
                                                <a href="javascript:void(0)" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                                            </div> 
                                            @if ($errors->has('image'))
                                            <span class="help-block" style="color:red;">
                                                <strong>{{ $errors->first('image')}}</strong>
                                            </span>
                                            @endif
                                            &nbsp;
                                        </div>
                                        <div class="col-sm-2">
                                            @if(!empty($productOptions->image))
                                            <img height="35" width="35" src="{{ URL::to('/') }}/product/{{ $productOptions->image }}" title=""/>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="col-sm-3 text-right">Enabled</label>
                                        <div class="col-sm-9">
                                            <div class="ckbox ckbox-primary">
                                                <input type="checkbox" id="checkbox-primary2" class="chk"  name="status" {{($productOptions->status == 'active') ? 'checked' : ''}}>
                                                <label for="checkbox-primary2"></label>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-sm-12">
<!--
                                     @if($productOptions->type == 'Product' || $productOptions->type == 'Brand')
                                        <div class="col-lg-6">
                                        <label for="client" class="col-sm-3 control-label">
                                            Clients </label>
                                            <div class="col-sm-9" style="padding-left: 0px;">

                                                <div class="demo-section k-content">
                                                    <div id="dialog">
                                                        <div class="dialogContent">
                                                            <input id="filterText" type="text" placeholder="Search Client" />
                                                            <div class="selectAll" class='col-sm-12'>

                                                                <span id="result">0 client selected</span>
                                                            </div>
                                                            <div id="treeview"></div>
                                                        </div>
                                                    </div>
                                                    <select id="multiselect"></select>

                                                    <button id="openWindow" class="k-primary btn btn-block btn-color" type="button" style="border:none;">Select Clients</button>
                                                </div>

                                            </div>

                                            @if ($errors->has('select'))
                                            <span class="help-block" style="color:red;">
                                                <strong>{{ $errors->first('client_id') }}</strong>
                                            </span>
                                            @endif
                                         </div>
                                    @endif
-->
                                </div>

                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <label for="description" class="col-sm-2 control-label" style="font-weight:800; text-align: left;">Description</label>
                                        <div class="col-sm-12">
                                            <textarea type="text" name="description" class="form-control" id="summernote-textarea"
                                                      >{{$productOptions->description}}</textarea>
                                            @if ($errors->has('description'))
                                            <span class="help-block" style="color:red;">
                                                <strong>{{ $errors->first('description')}}</strong>
                                            </span>
                                            @endif
                                            &nbsp;
                                        </div>
                                    </div>
                                </div>

						@if($productOptions->type == 'Product')
						<!-- ================START Processor Brand Assignment ========================== -->
						<br/><div class="col-sm-12">
						<div class="panel-heading">
						<div class="pull-left">
						<h3 class="panel-title">BRAND ASSIGNMENT</h3>
						</div>
						<div class="clearfix"></div>
						</div><br/>
						</div>

						<div class="col-lg-12">
						<div class="col-lg-9 col-sm-9 col-md-9">
						<label for="country_ex" style="text-align: left;" class="col-sm-12 control-label">Country<span style="color:red;">*</span></label>
						<div class="col-sm-12">
						<select id="brand_proc" class="form-control">
						<option value=""> -- Select Brand -- </option>
						@foreach ($brands as $brand)
						<option value="{{ $brand->id }}">{{ $brand->name }}</option>
						@endforeach             
						</select>

						<span class="help-block" style="color:red;">
						<strong id="brand_proc_eoor"></strong>
						</span>
						@if ($errors->has('brand'))
						<span class="help-block" style="color:red;">
						<strong id="brand_proc_error">A brand selection is  required.</strong>
						</span>
						@endif
						&nbsp
						</div>
						</div>      
						<div class="col-lg-3 col-sm-3 col-md-3">                                  
						<div class="col-sm-12" style="padding-bottom:20px;margin-top: 20px; ">
						<button type="button" id="add_ex_btn" class="btn btn-color btn-hover btn-block">Add</button>
						</div>
						</div>
						</div>
						<div class="table-responsive mb-20 col-sm-12">
						<button type="button" style="margin-bottom: 10px;" class="delete-row btn btn-danger" style="margin-bottom:4px;">Delete Row</button>
						<table class="table table-striped table-success table-middle table-project-clients region_table" role="grid" aria-describedby="datatable-client-all_info" >
						<thead>
						<tr>
						<th width="10px;">Select</th>
						<th>Brand</th>                                 
						</tr>

						</thead>
						<tbody>
						@if(count($processor_brands) > 0)
						@foreach($processor_brands as $brand)
						<tr>
						<td>
						<input type='checkbox' name='record'>
						</td>
						<td id='proc_brand'>{{$brand->name}}<input type='hidden' class='proc_brand_hidden' name='proc_brand[]'  value='{{$brand->id}}'/></td>
						</tr>
						@endforeach
						@endif
						</tbody>
						</table>

						</div>
						<!-- =======================END Processor Brand Assignment ========================= -->
						@endif


                            </div>
                            <!-- /.form-group -->
                            <div class="form-group"></div>
                            <!-- /.form-group -->
                        </div>
                        <!-- /.form-body -->
                        <div class="form-footer">
                            <div class="col-sm-offset-5">
                                <button class="btn btn-color btn-hover" type="submit">Submit</button>
                                <a class="btn btn-danger" href="javascript:history.back()">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.panel -->
            <!--/ End horizontal form -->
        </div>
    </div>
</div>
@section('js')
<!-- START @PAGE LEVEL SCRIPT -->
<script type=text/javascript src="{{ asset('js/lib/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js') }}"></script>
<script type=text/javascript src="{{ asset('js/lib/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.min.js') }}"></script>
<script type=text/javascript src="{{ asset('js/pages/blankon.form.advanced.js') }}"></script>
<script src="{{ asset('assets/global/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>
<script src="{{ asset('assets/global/plugins/bower_components/jasny-bootstrap-fileinput/js/jasny-bootstrap.fileinput.min.js')}}"></script>
<script src="{{ asset('assets/global/plugins/bower_components/holderjs/holder.js')}}"></script>
<script src="{{ asset('assets/global/plugins/bower_components/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
<script src="{{ asset('assets/global/plugins/bower_components/jquery-autosize/jquery.autosize.min.js')}}"></script>
<script src="{{ asset('assets/global/plugins/bower_components/chosen_v1.2.0/chosen.jquery.min.js')}}"></script>

<script src="{{asset('assets/global/plugins/bower_components/bootstrap-wysihtml5/lib/js/wysihtml5-0.3.0.min.js')}}"></script>
<script src="{{asset('assets/global/plugins/bower_components/bootstrap-wysihtml5/src/bootstrap-wysihtml5.js')}}"></script>
<script src="{{asset('assets/global/plugins/bower_components/summernote/dist/summernote.min.js')}}"></script>
<script src="{{ asset('js/ClientTree.js') }}"></script>


<script type="text/javascript">
var i = 0;

$("#add_ex_btn").click(function () {
	i = i + 1;
	var brand_proc = $("#brand_proc").val();                    
	var brand_proc_text =  $("#brand_proc :selected").text();
	var row = "<tr><td><input type='checkbox' name='record'></td><td id='proc_brand'>" + brand_proc_text + "<input type='hidden' name='proc_brand[]'  value='" + brand_proc + "'/></td></tr>";
	var brand_proc_array = [];

	$(".proc_brand_hidden").each(function(){
		brand_proc_array.push($(this).val());
	});

	if (brand_proc == "") {
		$('#brand_proc_eoor').text('The brand field is required');
		$('#brand_proc').focus();
		return false;
	}else {
		$('.delete-row').show();
		$('.region_table').show();
		$("#brand_proc_eoor").text('');
		$("table tbody").append(row);
	};
});

$(".delete-row").click(function () {
	$("table tbody").find('input[name="record"]').each(function () {
		if ($(this).is(":checked")) {
			$(this).parents("tr").remove();
		}
	});
});
</script>
<script type="text/javascript">
var BlankonFormWysiwyg = function () {

    return {
        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            BlankonFormWysiwyg.bootstrapWYSIHTML5();
            BlankonFormWysiwyg.summernote();
        },
        // =========================================================================
        // BOOTSTRAP WYSIHTML5
        // =========================================================================
        bootstrapWYSIHTML5: function () {
            if ($('#wysihtml5-textarea').length) {
                $('#wysihtml5-textarea').wysihtml5();
            }
        },
        // =========================================================================
        // SUMMERNOTE
        // =========================================================================
        summernote: function () {
            if ($('#summernote-textarea').length) {
                $('#summernote-textarea').summernote();
            }
        }

    };

}();

// Call main app init
BlankonFormWysiwyg.init();
</script>
@endsection

@endsection
