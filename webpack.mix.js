let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
		.js(
				[
						'resources/assets/global/plugins/bower_components/jquery/dist/jquery.min.js',
						'resources/assets/global/plugins/bower_components/jquery-cookie/jquery.cookie.js',
						'resources/assets/global/plugins/bower_components/bootstrap/dist/js/bootstrap.min.js',
						'resources/assets/global/plugins/bower_components/jquery-easing-original/jquery.easing.1.3.min.js',
						'resources/assets/global/plugins/bower_components/ionsound/js/ion.sound.min.js',
						'resources/assets/global/plugins/bower_components/jquery-validation/dist/jquery.validate.min.js',
						'resources/assets/admin/js/pages/blankon.sign.js',
						'resources/assets/admin/js/demo.js', ],
				'public/js/admin-login.js')

		.js(
				[
						'resources/assets/global/plugins/bower_components/jquery-1.11.0/dist/jquery.min.js',

				], 'public/js/admin-dashboard.js')
		.js(
				[
						'resources/assets/global/plugins/bower_components/jquery-cookie/jquery.cookie.js',
						'resources/assets/global/plugins/bower_components/bootstrap/dist/js/bootstrap.min.js', ],
				'public/js/admin-dashboard1.js')
				.js(
				[
						'resources/assets/global/plugins/bower_components/jquery-cookie/jquery.cookie.js',
						 ],
				'public/js/jquery-cookie.js')
		.js(
				[
						'resources/assets/global/plugins/bower_components/typehead.js/dist/handlebars.js',
						'resources/assets/global/plugins/bower_components/typehead.js/dist/typeahead.bundle.min.js',
						'resources/assets/global/plugins/bower_components/jquery-nicescroll/jquery.nicescroll.min.js',
						'resources/assets/global/plugins/bower_components/jquery.sparkline.min/index.js',
						'resources/assets/global/plugins/bower_components/jquery-easing-original/jquery.easing.1.3.min.js',
						'resources/assets/global/plugins/bower_components/ionsound/js/ion.sound.min.js',
						'resources/assets/global/plugins/bower_components/bootbox/bootbox.js',
						'resources/assets/global/plugins/bower_components/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js',
						'resources/assets/global/plugins/bower_components/flot/jquery.flot.js',
						'resources/assets/global/plugins/bower_components/flot/jquery.flot.spline.min.js',
						'resources/assets/global/plugins/bower_components/flot/jquery.flot.categories.js',
						'resources/assets/global/plugins/bower_components/flot/jquery.flot.tooltip.min.js',
						'resources/assets/global/plugins/bower_components/flot/jquery.flot.resize.js',
						'resources/assets/global/plugins/bower_components/flot/jquery.flot.pie.js',
						'resources/assets/global/plugins/bower_components/dropzone/downloads/dropzone.min.js',
						'resources/assets/global/plugins/bower_components/jquery.gritter/js/jquery.gritter.min.js',
						'resources/assets/global/plugins/bower_components/skycons-html5/skycons.js',
						'resources/assets/global/plugins/bower_components/waypoints/lib/jquery.waypoints.min.js',
						'resources/assets/global/plugins/bower_components/counter-up/jquery.counterup.min.js',
						'resources/assets/global/plugins/bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js',
						'resources/assets/admin/js/apps.js',
						'resources/assets/admin/js/pages/blankon.dashboard.js',
						'resources/assets/admin/js/demo.js', ],
				'public/js/admin-dashboard2.js')

		.styles(
				[
						'resources/assets/css/bootstrap.css',
						'resources/assets/global/plugins/bower_components/fontawesome/css/font-awesome.min.css',
						'resources/assets/global/plugins/bower_components/animate.css/animate.min.css',
						'resources/assets/admin/css/reset.css',
						'resources/assets/admin/css/layout.css',
						'resources/assets/admin/css/components.css',
						'resources/ssets/admin/css/plugins.css', ],
				'public/css/admin-login.css')
		.styles(
				[

				'resources/assets/admin/css/themes/default.theme.css',
						'resources/assets/admin/css/pages/sign.css',
						'resources/assets/admin/css/custom.css', ],
				'public/css/admin-login2.css')
		.styles(
				[
						'resources/assets/global/plugins/bower_components/bootstrap/dist/css/bootstrap.min.css',
						'resources/assets/global/plugins/bower_components/fontawesome/css/font-awesome.min.css',
						'resources/assets/global/plugins/bower_components/animate.css/animate.min.css',
						'resources/assets/global/plugins/bower_components/dropzone/downloads/css/dropzone.css',
						'resources/assets/global/plugins/bower_components/jquery.gritter/css/jquery.gritter.css',
						'resources/assets/global/plugins/bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css',
						'resources/assets/global/plugins/bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css',
						'resources/assets/admin/css/reset.css',
						'resources/assets/admin/css/layout.css',
						'resources/assets/admin/css/components.css',
						'resources/assets/admin/css/plugins.css',
						'resources/assets/admin/css/themes/default.theme.css',
						'resources/assets/admin/css/custom.css', ],
				'public/css/admin-dashboard.css');